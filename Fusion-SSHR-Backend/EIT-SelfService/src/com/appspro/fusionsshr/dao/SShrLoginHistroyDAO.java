package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;

import com.appspro.fusionsshr.bean.SShrLOginHistroyBean;
import com.appspro.fusionsshr.bean.SummaryReportBean;
import static common.restHelper.RestHelper.getSchema_Name;
import common.restHelper.RestHelper;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class SShrLoginHistroyDAO  extends AppsproConnection {
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    
    public SShrLOginHistroyBean addLoginHistory(SShrLOginHistroyBean sshrLoginHistory
                                        ) {

        try {
            connection = AppsproConnection.getConnection();
          
                String query =
                    "INSERT INTO  " + " " + getSchema_Name() + ".XXX_SSHR_LOGIN_HISTORY(PERSON_NUMBER,LOGIN_DATE,BROWSER_DETAILS,DEVICE_IP) VALUES(?,?,?,?)";
                ps = connection.prepareStatement(query);

                ps.setString(1, sshrLoginHistory.getPersonNumber());
                ps.setString(2, sshrLoginHistory.getLoginDate());
                ps.setString(3, sshrLoginHistory.getBrowserAndDeviceDetails());
                ps.setString(4, sshrLoginHistory.getDeviceIp());
                ps.executeUpdate();
           
        } catch (Exception e) {
        
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return sshrLoginHistory;
    }

    public SShrLOginHistroyBean UpdateLoginHistory(SShrLOginHistroyBean sshrLoginHistory
                                        ) {

        try {
            connection = AppsproConnection.getConnection();
          
                String query =
                    "update  " + " " + getSchema_Name() + ".XXX_SSHR_LOGIN_HISTORY set SIGNOUT_DATE=? where  LOGIN_DATE=? AND BROWSER_DETAILS=?";
                ps = connection.prepareStatement(query);

                ps.setString(1, sshrLoginHistory.getSignOut());
                ps.setString(2, sshrLoginHistory.getLoginDate());
                ps.setString(3, sshrLoginHistory.getBrowserAndDeviceDetails());
                ps.executeUpdate();
           
        } catch (Exception e) {
        
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return sshrLoginHistory;
    }

    
    
}
