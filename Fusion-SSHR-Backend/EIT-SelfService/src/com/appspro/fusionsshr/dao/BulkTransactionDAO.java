/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.dao;


import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.ApprovalManagementBean;
import com.appspro.fusionsshr.bean.BulkTransactionBean;
import com.appspro.fusionsshr.bean.EmployeeBean;
import com.appspro.fusionsshr.bean.PeopleExtraInformationBean;

import common.biPReports.BIPReports;

import common.restHelper.RestHelper;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;


/**
 *
 * @author Shadi Mansi-PC
 */
public class BulkTransactionDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    private EmployeeDetails employeeDetails = new EmployeeDetails();
    private EFFDetails eFFDetails = new EFFDetails();
    private PeopleExtraInformationDAO extraInformationDAO =
        new PeopleExtraInformationDAO();
    private BulkTransactionBean bulkTransactionBean =
        new BulkTransactionBean();
    private PaasBulkTransactionDAO paasBulkTransactionDAO =
        new PaasBulkTransactionDAO();
    private int max_bulk_transaction;

    public PeopleExtraInformationBean executeBulkTransactionByPersonId(String body,
                                                                       String transactionType) {
        
        
        PeopleExtraInformationBean obj ;
        List<JSONObject> beanList = new ArrayList<JSONObject>();
        JSONArray arr = new JSONArray(body);
                JSONObject jsonObjInput;
                String body2 = "";
                for(int i = 0; i < arr.length(); i++){
                    jsonObjInput = arr.getJSONObject(i);    
                    beanList.add(jsonObjInput);
                }
        
        
        
        
        PaasBulkTransactionDAO objMaxTransaction =
            new PaasBulkTransactionDAO();
        max_bulk_transaction = objMaxTransaction.getMaxBulkTransaction();
        EmployeeBean emp = null;
        PeopleExtraInformationBean peopleExtraInformationBean =
            new PeopleExtraInformationBean();
        String result;
           
        try {
            for (JSONObject peopleExtraInformationBean1 :
                 beanList) {
                emp = new EmployeeBean();
                 JSONObject jsonObj = new JSONObject(peopleExtraInformationBean1.get("eit").toString());
                //(jsonObj);
                obj = new PeopleExtraInformationBean();
                //(peopleExtraInformationBean1);
                                         String person_id=jsonObj.get("personId").toString();
                                         //("P"+person_id);
                                         emp = employeeDetails.getEmpDetailsByPersonId(person_id, "", "");
                                         String URL=emp.getEmployeeURL().get(peopleExtraInformationBean1.get("code").toString()).toString();
                                         jsonObj.remove("seq");
                                         jsonObj.remove("personId");
                                         jsonObj.remove("personName");
                                         result = eFFDetails.postEFF_BulkTransaction(jsonObj.toString(), URL);
                                         obj.setCode(peopleExtraInformationBean1.get("code").toString());
                                         obj.setEit(peopleExtraInformationBean1.get("eit").toString());
                                         obj.setStatus(peopleExtraInformationBean1.get("status").toString());
                                         obj.setPerson_number(emp.getPersonNumber());
                                         obj.setLine_manager(emp.getManagerId());
                                         obj.setManage_of_manager(emp.getManagerOfManager());
                                         obj.setUrl(emp.getAbsenceCancelRequestUrl());
                                         
                //                        result = eFFDetails.postEFF_BulkTransaction(peopleExtraInformationBean1.getEit(), emp.getAbsenceCancelRequestUrl());
                                        obj.setStatus(result);
                                        peopleExtraInformationBean = extraInformationDAO.insertOrUpdateXXX_EIT(obj, transactionType,"Default");
                                        bulkTransactionBean.setCreateBy(obj.getCreated_by());
                                        bulkTransactionBean.setPersonExtraInformationID(String.valueOf(obj.getId()));
                                        bulkTransactionBean.setTransactionNumber(Integer.parseInt( peopleExtraInformationBean1.get("reference_num").toString()));
                                        //("test"+peopleExtraInformationBean1.get("reference_num").toString());
                                        paasBulkTransactionDAO.addBulkTransaction(bulkTransactionBean);
            }

        } catch (Exception ex) {
            Logger.getLogger(BulkTransactionDAO.class.getName()).log(Level.SEVERE,
                                                                     null, ex);
        }finally {
             closeResources(connection, ps, rs);
        }
        return peopleExtraInformationBean;
    }

    public PeopleExtraInformationBean executeBulkTransactionByDept(PeopleExtraInformationBean extraInformationBean,
                                                                   String deptId,
                                                                   String transactionType) {

        EmployeeBean emp = new EmployeeBean();
        PeopleExtraInformationBean peopleExtraInformationBean =
            new PeopleExtraInformationBean();
        PaasBulkTransactionDAO objMaxTransaction =
            new PaasBulkTransactionDAO();
        max_bulk_transaction = objMaxTransaction.getMaxBulkTransaction();
        String result = "";
        try {
            BIPReports biPReports = new BIPReports();
            biPReports.setAttributeFormat(BIPReports.ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_XML.getValue());
            biPReports.setAttributeTemplate(BIPReports.ATTRIBUTE_TEMPLATE.ATTRIBUTE_TEMPLATE_XML.getValue());
            biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.EmployeesByDepartmentName.getValue());
            //(biPReports.executeReports());
            String arr = biPReports.executeReports();
            //             JSONObject jsnobject = new JSONObject(readlocationFeed);
            //             JSONArray jsonArray = new JSONArray(biPReports.executeReports());
            //(arr);
            JSONObject xmlJSONObj = XML.toJSONObject(arr);
            //JSONObject jsnobject = new JSONObject(arr);
            //sJSONArray jsonArray = xmlJSONObj.getJSONArray("G_1");

            //(xmlJSONObj);
            JSONArray jsonArray =
                xmlJSONObj.getJSONObject("DATA_DS").getJSONArray("G_1");
            //jsonArray.put(xmlJSONObj);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObj = jsonArray.getJSONObject(i);
                //(jsonObj);
                extraInformationBean.setPerson_id(jsonObj.get("PERSON_ID").toString());
                emp =
employeeDetails.getEmpDetailsByPersonId(extraInformationBean.getPerson_id(),
                                        "", "");
                extraInformationBean.setPerson_number(emp.getPersonNumber());
                extraInformationBean.setLine_manager(emp.getManagerId());
                extraInformationBean.setManage_of_manager(emp.getManagerOfManager());
                if ("XXX_HR_BUSINESS_PAYMENT".equals(extraInformationBean.getCode())) {

                    extraInformationBean.setUrl(emp.getBusinessMissionPaymentUrl());
                    result =
                            eFFDetails.postEFF_BulkTransaction(extraInformationBean.getEit(),
                                                               emp.getBusinessMissionPaymentUrl());
                    extraInformationBean.setStatus(result);
                    peopleExtraInformationBean =
                            extraInformationDAO.insertOrUpdateXXX_EIT(extraInformationBean,
                                                                      transactionType,"Default");
                    bulkTransactionBean.setCreateBy(extraInformationBean.getCreated_by());
                    bulkTransactionBean.setPersonExtraInformationID(String.valueOf(extraInformationBean.getId()));
                    bulkTransactionBean.setTransactionNumber(max_bulk_transaction);
                    paasBulkTransactionDAO.addBulkTransaction(bulkTransactionBean);

                } else if ("XXX_HR_BUSINESS_MISSION".equals(extraInformationBean.getCode())) {
                    extraInformationBean.setUrl(emp.getBusinessMissionRequestUrl());
                    result =
                            eFFDetails.postEFF_BulkTransaction(extraInformationBean.getEit(),
                                                               emp.getBusinessMissionRequestUrl());
                    extraInformationBean.setStatus(result);
                    peopleExtraInformationBean =
                            extraInformationDAO.insertOrUpdateXXX_EIT(extraInformationBean,
                                                                      transactionType,"Default");
                    bulkTransactionBean.setCreateBy(extraInformationBean.getCreated_by());
                    bulkTransactionBean.setPersonExtraInformationID(String.valueOf(extraInformationBean.getId()));
                    bulkTransactionBean.setTransactionNumber(max_bulk_transaction);
                    paasBulkTransactionDAO.addBulkTransaction(bulkTransactionBean);

                } else if ("XXX_HR_OVERTIME_PAYMENT".equals(extraInformationBean.getCode())) {
                    extraInformationBean.setUrl(emp.getOverTimePaymentUrl());
                    result =
                            eFFDetails.postEFF_BulkTransaction(extraInformationBean.getEit(),
                                                               emp.getOverTimePaymentUrl());
                    extraInformationBean.setStatus(result);
                    peopleExtraInformationBean =
                            extraInformationDAO.insertOrUpdateXXX_EIT(extraInformationBean,
                                                                      transactionType,"Default");
                    bulkTransactionBean.setCreateBy(extraInformationBean.getCreated_by());
                    bulkTransactionBean.setPersonExtraInformationID(String.valueOf(extraInformationBean.getId()));
                    bulkTransactionBean.setTransactionNumber(max_bulk_transaction);
                    paasBulkTransactionDAO.addBulkTransaction(bulkTransactionBean);

                } else if ("XXX_OVERTIME_REQUEST".equals(extraInformationBean.getCode())) {
                    extraInformationBean.setUrl(emp.getOverTimeURL());
                    result =
                            eFFDetails.postEFF_BulkTransaction(extraInformationBean.getEit(),
                                                               emp.getOverTimeURL());
                    extraInformationBean.setStatus(result);
                    peopleExtraInformationBean =
                            extraInformationDAO.insertOrUpdateXXX_EIT(extraInformationBean,
                                                                      transactionType,"Default");
                    bulkTransactionBean.setCreateBy(extraInformationBean.getCreated_by());
                    bulkTransactionBean.setPersonExtraInformationID(String.valueOf(extraInformationBean.getId()));
                    bulkTransactionBean.setTransactionNumber(max_bulk_transaction);
                    paasBulkTransactionDAO.addBulkTransaction(bulkTransactionBean);
                    break;
                } else if ("XXX_ABS_UPDATE_DECREE".equals(extraInformationBean.getCode())) {
                    extraInformationBean.setUrl(emp.getAbsenceUpdateRequestUrl());
                    result =
                            eFFDetails.postEFF_BulkTransaction(extraInformationBean.getEit(),
                                                               emp.getAbsenceUpdateRequestUrl());
                    extraInformationBean.setStatus(result);
                    peopleExtraInformationBean =
                            extraInformationDAO.insertOrUpdateXXX_EIT(extraInformationBean,
                                                                      transactionType,"Default");
                    bulkTransactionBean.setCreateBy(extraInformationBean.getCreated_by());
                    bulkTransactionBean.setPersonExtraInformationID(String.valueOf(extraInformationBean.getId()));
                    bulkTransactionBean.setTransactionNumber(max_bulk_transaction);
                    paasBulkTransactionDAO.addBulkTransaction(bulkTransactionBean);
                    break;
                } else if ("XXX_ABS_CREATE_DECREE".equals(extraInformationBean.getCode())) {
                    extraInformationBean.setUrl(emp.getAbsenceRequestUrl());
                    result =
                            eFFDetails.postEFF_BulkTransaction(extraInformationBean.getEit(),
                                                               emp.getAbsenceRequestUrl());
                    extraInformationBean.setStatus(result);
                    peopleExtraInformationBean =
                            extraInformationDAO.insertOrUpdateXXX_EIT(extraInformationBean,
                                                                      transactionType,"Default");
                    bulkTransactionBean.setCreateBy(extraInformationBean.getCreated_by());
                    bulkTransactionBean.setPersonExtraInformationID(String.valueOf(extraInformationBean.getId()));
                    bulkTransactionBean.setTransactionNumber(max_bulk_transaction);
                    paasBulkTransactionDAO.addBulkTransaction(bulkTransactionBean);
                    break;
                } else if ("XXX_EMPLOYMENT_CERTIFICATE".equals(extraInformationBean.getCode())) {
                    extraInformationBean.setUrl(emp.getEmployeeCertificate());
                    result =
                            eFFDetails.postEFF_BulkTransaction(extraInformationBean.getEit(),
                                                               emp.getEmployeeCertificate());
                    extraInformationBean.setStatus(result);
                    peopleExtraInformationBean =
                            extraInformationDAO.insertOrUpdateXXX_EIT(extraInformationBean,
                                                                      transactionType,"Default");
                    bulkTransactionBean.setCreateBy(extraInformationBean.getCreated_by());
                    bulkTransactionBean.setPersonExtraInformationID(String.valueOf(extraInformationBean.getId()));
                    bulkTransactionBean.setTransactionNumber(max_bulk_transaction);
                    paasBulkTransactionDAO.addBulkTransaction(bulkTransactionBean);
                    break;
                } else if ("XXX_ABS_DELETE_DECREE".equals(extraInformationBean.getCode())) {
                    extraInformationBean.setUrl(emp.getAbsenceCancelRequestUrl());
                    result =
                            eFFDetails.postEFF_BulkTransaction(extraInformationBean.getEit(),
                                                               emp.getAbsenceCancelRequestUrl());
                    extraInformationBean.setStatus(result);
                    peopleExtraInformationBean =
                            extraInformationDAO.insertOrUpdateXXX_EIT(extraInformationBean,
                                                                      transactionType,"Default");
                    bulkTransactionBean.setCreateBy(extraInformationBean.getCreated_by());
                    bulkTransactionBean.setPersonExtraInformationID(String.valueOf(extraInformationBean.getId()));
                    bulkTransactionBean.setTransactionNumber(max_bulk_transaction);
                    paasBulkTransactionDAO.addBulkTransaction(bulkTransactionBean);
                }
            }

        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }finally {
             closeResources(connection, ps, rs);
        }

        return peopleExtraInformationBean;
    }

}
