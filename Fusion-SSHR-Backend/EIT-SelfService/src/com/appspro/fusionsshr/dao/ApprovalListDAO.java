/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.ApprovalListBean;
import com.appspro.fusionsshr.bean.ApprovalSetupBean;
import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class ApprovalListDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();

    public ArrayList<ApprovalListBean> getLastStepForApproval(String transactionId, String serviceType) {
        ArrayList<ApprovalListBean> approvalSetupList = new ArrayList<ApprovalListBean>();
        ApprovalListBean bean = null;
        connection = AppsproConnection.getConnection();

        String query = null;
        query = "SELECT * FROM  "+ " " + getSchema_Name() + ".XX_APPROVAL_LIST\n"
                + " WHERE transaction_id = ? AND service_type = ? AND notification_type = 'FYA'\n"
                + " AND step_level = (SELECT MAX(step_level) FROM  "+ " " + getSchema_Name() + ".XX_APPROVAL_LIST\n"
                + " WHERE transaction_id = ? AND service_type = ? AND notification_type = 'FYA')";

        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, transactionId.toString());
            ps.setString(2, serviceType.toString());
            ps.setString(3, transactionId.toString());
            ps.setString(4, serviceType.toString());
            rs = ps.executeQuery();

            while (rs.next()) {
                bean = new ApprovalListBean();
                bean.setId(rs.getString("ID"));
                bean.setStepLeval(rs.getString("STEP_LEVEL"));
                bean.setServiceType(rs.getString("SERVICE_TYPE"));
                bean.setTransActionId(rs.getString("TRANSACTION_ID"));
                bean.setWorkflowId(rs.getString("WORKFLOW_ID"));
                bean.setRolrType(rs.getString("ROLE_TYPE"));
                bean.setRoleId(rs.getString("ROLE_ID"));
                bean.setRequestDatt(rs.getString("REQUEST_DATE"));
                bean.setResponseCode(rs.getString("RESPONSE_CODE"));
                bean.setNotificationType(rs.getString("NOTIFICATION_TYPE"));
                bean.setNote(rs.getString("NOTE"));
                bean.setResponseDate(rs.getString("RESPONSE_DATE"));
                bean.setLineManagerName(rs.getString("LINE_MANAGER_NAME"));
                approvalSetupList.add(bean);
            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return approvalSetupList;

    }

    public ApprovalListBean insertIntoApprovalList(ApprovalListBean bean) {
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query = "insert into  "+ " " + getSchema_Name() + ".XX_APPROVAL_LIST (STEP_LEVEL,SERVICE_TYPE,TRANSACTION_ID,WORKFLOW_ID,ROLE_TYPE,ROLE_ID,REQUEST_DATE,RESPONSE_CODE,NOTIFICATION_TYPE,NOTE,LINE_MANAGER_NAME,EMPLOYEE_NAME,RESPONSE_DATE)"
                    + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,sysDate)";
            ps = connection.prepareStatement(query);
            ps.setString(1, bean.getStepLeval());
            ps.setString(2, bean.getServiceType());
            ps.setString(3, bean.getTransActionId());
            ps.setString(4, bean.getWorkflowId());
            ps.setString(5, bean.getRolrType());
            ps.setString(6, bean.getRoleId());
            ps.setString(7, bean.getRequestDatt());
            ps.setString(8, bean.getResponseCode());
            ps.setString(9, bean.getNotificationType());
            ps.setString(10, bean.getNote());            
            ps.setString(11, bean.getLineManagerName());
            ps.setString(12, bean.getPersonName());
            // ps.setString(11, bean.getResponseDate());
            ps.executeUpdate();

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return bean;
    }

    public ApprovalListBean getRoles(String ID, String ServiceType) {
        
       
        ApprovalListBean bean = new ApprovalListBean();
        try {

            connection = AppsproConnection.getConnection();
            String query = null;
            query = "select ROLE_TYPE, ROLE_ID , NOTIFICATION_TYPE from  "+ " " + getSchema_Name() + ".XX_APPROVAL_LIST Where TRANSACTION_ID =? \n"
                    + "							   And STEP_LEVEL = 1 And SERVICE_TYPE =?";
            ps = connection.prepareStatement(query);
            ps.setString(1, ID);
            ps.setString(2, ServiceType);
            rs = ps.executeQuery();
            while (rs.next()) {
                bean.setRolrType(rs.getString("ROLE_TYPE"));
                bean.setRoleId(rs.getString("ROLE_ID"));
                bean.setNotificationType(rs.getString("NOTIFICATION_TYPE"));
            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return bean;
    }

    public String getMinStepLeval(String transactionId, String serviceType) {
        String minStepLeval = "";
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query = "SELECT MIN(STEP_LEVEL) AS MIN_ID  FROM  "+ " " + getSchema_Name() + ".XX_APPROVAL_LIST "
                    + "WHERE TRANSACTION_ID = ? AND RESPONSE_CODE IS NULL AND SERVICE_TYPE = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, transactionId);
            ps.setString(2, serviceType);
            rs = ps.executeQuery();
            while (rs.next()) {
                minStepLeval = rs.getString("MIN_ID");

            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return minStepLeval;
    }

    public ApprovalListBean getNextApproval(String transactionId, String minStepLeval, String serviceType) {
        ApprovalListBean bean = new ApprovalListBean();
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query = "SELECT * FROM  "+ " " + getSchema_Name() + ".XX_APPROVAL_LIST "
                    + "WHERE TRANSACTION_ID = ? AND RESPONSE_CODE IS NULL "
                    + "AND STEP_LEVEL = ? AND SERVICE_TYPE = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, transactionId);
            ps.setString(2, minStepLeval);
            ps.setString(3, serviceType);
            rs = ps.executeQuery();
            while (rs.next()) {
                bean.setId(rs.getString("ID"));
                bean.setStepLeval(rs.getString("STEP_LEVEL"));
                bean.setServiceType(rs.getString("SERVICE_TYPE"));
                bean.setTransActionId(rs.getString("TRANSACTION_ID"));
                bean.setWorkflowId(rs.getString("WORKFLOW_ID"));
                bean.setRolrType(rs.getString("ROLE_TYPE"));
                bean.setRoleId(rs.getString("ROLE_ID"));
                bean.setRequestDatt(rs.getString("REQUEST_DATE"));
                bean.setResponseCode(rs.getString("RESPONSE_CODE"));
                bean.setNotificationType(rs.getString("NOTIFICATION_TYPE"));
                bean.setNote(rs.getString("NOTE"));
                bean.setResponseDate(rs.getString("RESPONSE_DATE"));
                bean.setLineManagerName(rs.getString("LINE_MANAGER_NAME"));

            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return bean;
    }

    public void updateApprovalList(String RESPONSE_CODE, String transactionId, String stepLeval, String serviceType
                                   ,String reason) {
        try {
            //("ApprovalList ====> " + RESPONSE_CODE);
            //("ApprovalList ====> " + transactionId);
            //("ApprovalList ====> " + stepLeval);
            //("ApprovalList ====> " + serviceType);

            connection = AppsproConnection.getConnection();
            String query = null;
//             query ="UPDATE  "+ " " + getSchema_Name() + ".XX_APPROVAL_LIST \n" +
//"    SET \n" +
//"        RESPONSE_CODE = ? ,\n" +
//"        RESPONSE_DATE = SYSDATE \n" +
//"     WHERE\n" +
//"        TRANSACTION_ID = ?  \n" +
//"        AND STEP_LEVEL = ?  \n" +
//"        AND SERVICE_TYPE = ?";
            query = "UPDATE  "+ " " + getSchema_Name() + ".XX_APPROVAL_LIST SET  RESPONSE_DATE =SYSDATE , RESPONSE_CODE = ?   , REJECT_REASON = ?  "
                    + " WHERE TRANSACTION_ID = ? "
                    + "AND STEP_LEVEL = ?"
                    + " AND SERVICE_TYPE = ?";

            //("ApprovalList ====> " + query);
            ps = connection.prepareStatement(query);
            ps.setString(1, RESPONSE_CODE);
            ps.setString(2, reason);
            // ps.setDate(2, getCurrentDate());
            ps.setString(3, transactionId);
            ps.setString(4, stepLeval);
            ps.setString(5, serviceType);
            //("ApprovalList ====> " + query);
            ps.executeUpdate();
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
    }

    private static java.sql.Date getCurrentDate() {
        java.util.Date today = new java.util.Date();
        return new java.sql.Date(today.getTime());
    }

    public ArrayList<ApprovalListBean> approvalType(String serviceType) {
        ArrayList<ApprovalListBean> approvalSetupList = new ArrayList<ApprovalListBean>();
        ApprovalListBean bean = null;
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query = " SELECT * FROM  "+ " " + getSchema_Name() + ".XX_APPROVAL_LIST WHERE SERVICE_TYPE = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, serviceType);
            rs = ps.executeQuery();

            while (rs.next()) {
                bean = new ApprovalListBean();
                bean.setId(rs.getString("ID"));
                bean.setStepLeval(rs.getString("STEP_LEVEL"));
                bean.setServiceType(rs.getString("SERVICE_TYPE"));
                bean.setTransActionId(rs.getString("TRANSACTION_ID"));
                bean.setWorkflowId(rs.getString("WORKFLOW_ID"));
                bean.setRolrType(rs.getString("ROLE_TYPE"));
                bean.setRoleId(rs.getString("ROLE_ID"));
                bean.setRequestDatt(rs.getString("REQUEST_DATE"));
                bean.setResponseCode(rs.getString("RESPONSE_CODE"));
                bean.setNotificationType(rs.getString("NOTIFICATION_TYPE"));
                bean.setNote(rs.getString("NOTE"));
                bean.setResponseDate(rs.getString("RESPONSE_DATE"));
                bean.setLineManagerName(rs.getString("LINE_MANAGER_NAME"));
                approvalSetupList.add(bean);
            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return approvalSetupList;
    }

    public ArrayList<ApprovalListBean> getApprovalType(String serviceType, String T_id) {
        ArrayList<ApprovalListBean> approvalSetupList = new ArrayList<ApprovalListBean>();
        ApprovalListBean bean = null;
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query = " SELECT * FROM  "+ " " + getSchema_Name() + ".XX_APPROVAL_LIST WHERE SERVICE_TYPE = ? AND TRANSACTION_ID = ? ORDER BY ID ";
            ps = connection.prepareStatement(query);
            ps.setString(1, serviceType);
            ps.setString(2, T_id);
            rs = ps.executeQuery();

            while (rs.next()) {
                bean = new ApprovalListBean();
                bean.setId(rs.getString("ID"));
                bean.setStepLeval(rs.getString("STEP_LEVEL"));
                bean.setServiceType(rs.getString("SERVICE_TYPE"));
                bean.setTransActionId(rs.getString("TRANSACTION_ID"));
                bean.setWorkflowId(rs.getString("WORKFLOW_ID"));
                bean.setRolrType(rs.getString("ROLE_TYPE"));
                bean.setRoleId(rs.getString("ROLE_ID"));
                bean.setRequestDatt(rs.getString("REQUEST_DATE"));
                bean.setResponseCode(rs.getString("RESPONSE_CODE"));
                bean.setNotificationType(rs.getString("NOTIFICATION_TYPE"));
                bean.setNote(rs.getString("NOTE"));
                bean.setResponseDate(rs.getString("RESPONSE_DATE"));
                bean.setLineManagerName(rs.getString("LINE_MANAGER_NAME"));
                bean.setPersonName(rs.getString("EMPLOYEE_NAME"));
                bean.setRejectReason(rs.getString("REJECT_REASON"));
                approvalSetupList.add(bean);
            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return approvalSetupList;
    }

    public ArrayList<ApprovalSetupBean> getApprovalByEITCode(String eitCode) {
        ArrayList<ApprovalSetupBean> approvalSetupList = new ArrayList<ApprovalSetupBean>();
        ApprovalSetupBean approvalSetupObject = null;
        connection = AppsproConnection.getConnection();

        String query = "select  * from  "+ " " + getSchema_Name() + ".XXX_APROVALS_SETUP  where EIT_CODE = ? ORDER BY approval_order ASC";

        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, eitCode.toString());
            rs = ps.executeQuery();

            while (rs.next()) {
                approvalSetupObject = new ApprovalSetupBean();
                approvalSetupObject.setId(rs.getString("ID"));
                approvalSetupObject.setEitCode(rs.getString("EIT_CODE"));
                approvalSetupObject.setApprovalOrder(rs.getString("APPROVAL_ORDER"));
                approvalSetupObject.setApprovalType(rs.getString("APPROVAL_TYPE"));
                approvalSetupObject.setRoleName(rs.getString("ROLE_NAME"));
                approvalSetupObject.setSpecialCase(rs.getString("SPECIALCASE"));
                approvalSetupObject.setNotificationType(rs.getString("NOTIFICATION_TYPE"));
                approvalSetupObject.setCreatedBy(rs.getString("CREATEDBY"));
                approvalSetupObject.setCreationDate(rs.getString("CREATEDDATE"));

                approvalSetupList.add(approvalSetupObject);
            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return approvalSetupList;

    }

    public void updateReqeustDate(String ID) {
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query = " UPDATE  "+ " " + getSchema_Name() + ".XX_APPROVAL_LIST SET REQUEST_DATE = SYSDATE WHERE ID = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, ID);
            ps.executeUpdate();
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
    }
    public String getMaxStepLeval(String transactionId, String serviceType) {
            String maxStepLeval = "";
            int returned=0;
            try {
                connection = AppsproConnection.getConnection();
                String query = null;
                query = "SELECT MAX(STEP_LEVEL) AS MAX_ID  FROM  "+ " " + getSchema_Name() + ".XX_APPROVAL_LIST"
                        + " WHERE TRANSACTION_ID = ? AND RESPONSE_CODE IS NULL AND SERVICE_TYPE = ?";
                ps = connection.prepareStatement(query);
                ps.setString(1, transactionId);
                ps.setString(2, serviceType);
                rs = ps.executeQuery();
                while (rs.next()) {
                    if(rs.getString("MAX_ID")==null){
                        maxStepLeval="1";
                    }else{
                       returned=Integer.parseInt(rs.getString("MAX_ID"))+1;
                        maxStepLeval = String.valueOf(returned);
                    }
                }
            } catch (Exception e) {
                //("Error: ");
               e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            } finally {
                closeResources(connection, ps, rs);
            }
          
         
            return maxStepLeval ;
        }
}
