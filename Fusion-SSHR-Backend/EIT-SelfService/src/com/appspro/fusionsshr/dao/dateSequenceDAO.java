/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.sequenceDateBean;
import com.appspro.fusionsshr.bean.xxx_pass_lookupBean;

import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author lenovo
 */
public class dateSequenceDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();

    public int getDateSequence() {

        String day = null;
        String month = null;
        String year = null;
        int date = 0;
        try {
            connection = AppsproConnection.getConnection();
            String query = "SELECT  (1000 + COUNT(*)) +1 value FROM   "+ " " + getSchema_Name() + ".XXX_PER_PEOPLE_EXTRA_INFO ";
            ps = connection.prepareStatement(query);

            rs = ps.executeQuery();

            while (rs.next()) {

                date = rs.getInt("value");

            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return date;

    }

}
