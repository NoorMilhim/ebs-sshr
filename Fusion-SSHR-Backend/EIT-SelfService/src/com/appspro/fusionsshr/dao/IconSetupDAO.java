package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.IconSetupBean;
import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class IconSetupDAO extends AppsproConnection {
    
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    
    public IconSetupBean insertIcons(IconSetupBean bean) {
        try {
            connection = AppsproConnection.getConnection();

            String query = "INSERT INTO  "+ " " + getSchema_Name() + ".XXX_ICONS(ICONS,NAME,EITCODE) VALUES(?,?,?)";

            ps = connection.prepareStatement(query);
            
            ps.setString(1, bean.getICONS());
            ps.setString(2,bean.getNAME());  
            ps.setString(3,bean.getEITCODE());
            ps.executeUpdate();

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return bean;
      
    }

    public IconSetupBean updateIcons(String body) {
        
        IconSetupBean obj = new IconSetupBean();
        List<JSONObject> beanList = new ArrayList<JSONObject>();
        JSONArray arr = new JSONArray(body);
                JSONObject jsonObjInput;
                for(int i = 0; i < arr.length(); i++){
                    jsonObjInput = arr.getJSONObject(i);    
                    beanList.add(jsonObjInput);
                }

        try {
            connection = AppsproConnection.getConnection();

            String query="DELETE FROM  "+ " " + getSchema_Name() + ".XXX_ICONS WHERE ID=?";
            ps = connection.prepareStatement(query);
            for (JSONObject bean : beanList) {
              ps.setString(1, bean.get("ID").toString());
            }
            ps.executeUpdate();




         query = "INSERT INTO  "+ " " + getSchema_Name() + ".XXX_ICONS(ICONS,NAME,EITCODE) VALUES(?,?,?)";

            ps = connection.prepareStatement(query);
            for (JSONObject bean : beanList) {
                ps.setString(1, bean.getString("ICONS"));
                ps.setString(2,bean.get("NAME").toString()); 
                ps.setString(3,bean.get("EITCODE").toString());; 
                ps.executeUpdate();
            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return obj;
    }
    public ArrayList<IconSetupBean> getIconsTest(String EITCODE) {
           ArrayList<IconSetupBean> beanList = new ArrayList<IconSetupBean>();
       
           try {
               connection = AppsproConnection.getConnection();
               String query = null;
               query = "SELECT * FROM  "+ " " + getSchema_Name() + ".XXX_ICONS WHERE EITCODE=?";
               ps = connection.prepareStatement(query);
               ps.setString(1, EITCODE);
               rs = ps.executeQuery();
               while (rs.next()) {
                   IconSetupBean bean = new IconSetupBean();
                   bean.setEITCODE(rs.getString("EITCODE"));
                   bean.setICONS(rs.getString("ICONS"));
                   bean.setNAME(rs.getString("NAME"));
                   bean.setID(Integer.parseInt(rs.getString("ID")));
                   beanList.add(bean);
               }
           } catch (Exception e) {
               //("Error: ");
              e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
           } finally {
                closeResources(connection, ps, rs);
           }
           return beanList;

       }
    //update exaample 
    public IconSetupBean updateIconsTest(IconSetupBean bean) {
        try {
            connection = AppsproConnection.getConnection();            

         String query = "UPDATE  "+ " " + getSchema_Name() + ".XXX_ICONS set ICONS = ?,NAME=?,EITCODE=? where ID =?";

            ps = connection.prepareStatement(query);
                ps.setString(1, bean.getICONS());
                ps.setString(2,bean.getNAME()); 
                ps.setString(3,bean.getEITCODE());
                ps.setInt(4, bean.getID());
                ps.executeUpdate();

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }
    //end update
    //delete method
    public IconSetupBean deleteIconsTest(String body) {
            
            IconSetupBean obj = new IconSetupBean();
            List<JSONObject> beanList = new ArrayList<JSONObject>();
            JSONArray arr = new JSONArray(body);
                    JSONObject jsonObjInput;
                    for(int i = 0; i < arr.length(); i++){
                        jsonObjInput = arr.getJSONObject(i);    
                        beanList.add(jsonObjInput);
                    }

            try {
                connection = AppsproConnection.getConnection();            

             String query = "DELETE  FROM  "+ " " + getSchema_Name() + ".XXX_ICONS where ID = ?";

                ps = connection.prepareStatement(query);
                for (JSONObject bean : beanList) {
                   ps.setString(1, bean.get("ID").toString());
                    ps.executeUpdate();
                }

            } catch (Exception e) {
                //("Error: ");
               e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            } finally {
                closeResources(connection, ps, rs);
            }
            return obj;
        }
        //end delete
        public ArrayList<IconSetupBean> deleteIconsTests(String eit) {
                   ArrayList<IconSetupBean> beanList = new ArrayList<IconSetupBean>();
               
                   try {
                       connection = AppsproConnection.getConnection();
                       String query = null;
                       query = "DELETE  FROM  "+ " " + getSchema_Name() + ".XXX_ICONS WHERE EITCODE=?";
                       ps = connection.prepareStatement(query);
                       ps.setString(1, eit);
                       ps.executeUpdate();
                      
                   } catch (Exception e) {
                       //("Error: ");
                      e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
                   } finally {
                        closeResources(connection, ps, rs);
                   }
                   return beanList;

               }
        //
        public ArrayList<IconSetupBean> getAllIconsTest() {
               ArrayList<IconSetupBean> beanList = new ArrayList<IconSetupBean>();
           
               try {
                   connection = AppsproConnection.getConnection();
                   String query = null;
                   query = "SELECT * FROM  "+ " " + getSchema_Name() + ".XXX_ICONS";
                   ps = connection.prepareStatement(query);
                   rs = ps.executeQuery();
                   while (rs.next()) {
                       IconSetupBean bean = new IconSetupBean();
                       bean.setEITCODE(rs.getString("EITCODE"));
                       bean.setICONS(rs.getString("ICONs"));
                       bean.setNAME(rs.getString("NAME"));
                       bean.setID(Integer.parseInt(rs.getString("ID")));
                       beanList.add(bean);
                   }
               } catch (Exception e) {
                   //("Error: ");
                  e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
               } finally {
                    closeResources(connection, ps, rs);
               }
               return beanList;

           }

}
