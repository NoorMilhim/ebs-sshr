package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;

import common.biPReports.BIReportModel;

import common.restHelper.RestHelper;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.IOException;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONArray;
import org.json.JSONObject;


public class OTLApplicationDAO  extends RestHelper {
   
    public void callTimeCardWS(String date,String personNumber ) throws IOException {
          
       
        
      
    //            PeopleExtraInformationDAO dd = new PeopleExtraInformationDAO();
    //            PeopleExtraInformationBean pp= dd.getEITById(Integer.toString(id));

//            JSONObject modelJson = new JSONObject(model);
//            
//            String startDate = modelJson.get("requestStartDate").toString();
//            String endDate = modelJson.get("requestEndDate").toString();

//            call ws
            String data =      
            "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:rest=\"http://rest.appspro.com/\">\n" + 
            "   <soapenv:Header/>\n" + 
            "   <soapenv:Body>\n" + 
            "      <rest:ProcessEmployeeTimeCard>\n" + 
            "         <arg0> \n" + 
            "            <punchData>\n" + 
            "               <day>"+date+"</day>\n" + 
            "               <employee_ID>"+ personNumber+"</employee_ID>\n" + 
            "            </punchData>\n" + 
            "         </arg0>\n" + 
            "      </rest:ProcessEmployeeTimeCard>\n" + 
            "   </soapenv:Body>\n" + 
            "</soapenv:Envelope>";
            
            AppsproConnection.LOG.info(data);

            System.setProperty("DUseSunHttpHandler", "true");
            byte[] buffer = new byte[data.length()];
            buffer = data.getBytes();
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            bout.write(buffer);
            byte[] b = bout.toByteArray();
            RestHelper RS = new RestHelper();
            java.net.URL ExeUrl =
                new URL(null, RS.getEmployeeAnalysisUrl(), new sun.net.www.protocol.https.Handler());
            java.net.HttpURLConnection http;
            if (ExeUrl.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                java.net.HttpURLConnection https =
                    (HttpsURLConnection)ExeUrl.openConnection();
                //            System.setProperty("DUseSunHttpHandler", "true");
                //https.setHostnameVerifier(DO_NOT_VERIFY);
                http = https;
            } else {
                http = (HttpURLConnection)ExeUrl.openConnection();
            }
            String SOAPAction = "";
            //            http.setRequestProperty("Content-Length", String.valueOf(b.length));
            http.setRequestProperty("Content-Length",
                                    String.valueOf(b.length));
                            http.setRequestProperty("Content-Type", "text/xml");
            http.setRequestProperty("SOAPAction", SOAPAction);
            //                http.setRequestProperty("Authorization", "Basic " + getAuth());
            http.setRequestMethod("POST");
            http.setDoOutput(true);
            http.setDoInput(true);
            OutputStream out = http.getOutputStream();
            out.write(b);
            AppsproConnection.LOG.info("OTL WS Status : "+ http.getResponseCode());
        }
     
     
    public void timeCardByLocation(String  body ) {
            JSONObject jsonObj = new JSONObject(body);
            String startDate=jsonObj.get("sartDate").toString();
            String endDate=jsonObj.get("endDate").toString();
            String location=jsonObj.get("locationName").toString();
        
            Map<String, String> paramMap = new HashMap<String, String>();
            paramMap.put("LocationName", location);
            JSONObject respone =
                BIReportModel.runReport("All_Employees_in_ specific_location", paramMap);
            JSONObject dataDS = respone.getJSONObject("DATA_DS");
            JSONArray g1 = dataDS.getJSONArray("G_1");
            
            for (int i = 0; i < g1.length(); i++) {      
                
                JSONObject data = (JSONObject)g1.get(i);    
                uploadDataByStartAndEndDate(startDate,endDate, data.get("ASSIGNMENT_NUMBER").toString().substring(1,data.get("ASSIGNMENT_NUMBER").toString().length()));
               
//                callTimeCardWS(startDate, data.get("ASSIGNMENT_NUMBER").toString().substring(1,data.get("ASSIGNMENT_NUMBER").toString().length())); 
                     
            };
    }

    public void uploadDataByStartAndEndDate(String startDate,String endDate,String  personNumber){


        Date startdate = null;
        Date enddate = null;
        
        
       try{
            SimpleDateFormat format  = new SimpleDateFormat("yyyy-MM-dd");
            Calendar c = Calendar.getInstance();
            
            c.setTime(format.parse(startDate));
            c.setTime(format.parse(endDate));
            c.setTime(format.parse(startDate));
            startdate = format.parse(startDate);
            enddate = format.parse(endDate);
           
           long diff = enddate.getTime() - startdate.getTime();
            for (int counter = 0; counter <= (diff / (24 * 60 * 60 * 1000));
                 counter++) {
                if (counter == 0) {
                    c.add(Calendar.DATE, 0);
                } else if (counter > 0) {
                    c.add(Calendar.DATE, 1);
                }
                callTimeCardWS(format.format(c.getTime()).toString(),personNumber);

            }  
       }catch (Exception e) {
         e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }
        
//        for(int counter=startdate;)
//        
//        
        }

   
    public void  timeCardBypersonNumber(String body)  throws IOException{
        
            JSONArray arr = new JSONArray();
            JSONObject jsonObj = new JSONObject(body);
            String date=jsonObj.get("datee").toString();
            String personNumber=null;
            arr=jsonObj.getJSONArray("personNumberArr");
          
            for (int counter=0; counter<arr.length();counter++){
//                 callTimeCardWS(arr.getJSONObject(counter).get("date").toString(),arr.getJSONObject(counter).get("personNumber").toString());
                 uploadDataByStartAndEndDate(arr.getJSONObject(counter).get("sartDate").toString(),arr.getJSONObject(counter).get("endDate").toString(), arr.getJSONObject(counter).get("personNumber").toString());
             }
        
        
        }        
     
}