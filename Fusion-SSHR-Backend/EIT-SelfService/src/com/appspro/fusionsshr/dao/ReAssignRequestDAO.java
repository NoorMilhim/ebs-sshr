package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;

import com.appspro.fusionsshr.bean.ApprovalListBean;
import com.appspro.fusionsshr.bean.NotificationBean;
import com.appspro.fusionsshr.bean.PeopleExtraInformationBean;

import com.appspro.fusionsshr.bean.WorkFlowNotificationBean;

import common.restHelper.RestHelper;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;

import javax.ws.rs.Path;

import static common.restHelper.RestHelper.getSchema_Name;

/**
 *
 * @author Shadi Mansi-PC
 */

public class ReAssignRequestDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    
    public ArrayList<PeopleExtraInformationBean> getPendingSelfServices() {
        PeopleExtraInformationBean bean = null;
        ArrayList<PeopleExtraInformationBean> list = new ArrayList<PeopleExtraInformationBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "select * from  " + " " + getSchema_Name() + ".XXX_PER_PEOPLE_EXTRA_INFO where STATUS = 'PENDING_APPROVED' OR STATUS = 'APPROVED'OR STATUS = 'DRAFT' ORDER BY ID DESC";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                bean = new PeopleExtraInformationBean();
                bean.setId(Integer.parseInt(rs.getString("ID")));
                bean.setCode(rs.getString("EIT_CODE"));
                bean.setEit(rs.getString("EIT"));
                bean.setStatus(rs.getString("STATUS"));
                bean.setPerson_number(rs.getString("PERSON_NUMBER"));
                bean.setPerson_id(rs.getString("PERSON_ID"));
                bean.setCreated_by(rs.getString("CREATED_BY"));
                bean.setCreation_date(rs.getString("CREATION_DATE"));
                bean.setUpdated_by(rs.getString("UPDATED_BY"));
                bean.setUpdated_date(rs.getString("UPDATED_DATE"));
                bean.setLine_manager(rs.getString("LINE_MANAGER"));
                bean.setManage_of_manager(rs.getString("MANAGER_OF_MANAGER"));
                bean.setUrl(rs.getString("URL"));
                bean.setEit_name(rs.getString("EIT_NAME"));
                bean.setPersonName(rs.getString("PERSON_NAME"));
                list.add(bean);
            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return list;
    }
    
    public String deleteSelfServiceRow(String id) {
        int status = 0;

        try {
            connection = AppsproConnection.getConnection();
            // Delete From People Extra Info Table
            
            String queryDeleteFromPeopleExtra = "BEGIN\n" +
                "DELETE FROM  " + " " + getSchema_Name() +
                ".XXX_PER_PEOPLE_EXTRA_INFO\n" +
                "WHERE ID = ?;\n" +
                "COMMIT;\n" +
                "END;";

            ps = connection.prepareStatement(queryDeleteFromPeopleExtra);

            ps.setString(1, id);

            status = ps.executeUpdate();
            
            // Delete From Self Service Table
            
            String queryDeleteFromSelfService = "BEGIN\n" +
                "DELETE FROM  " + " " + getSchema_Name() +
                ".XX_SELF_SERVICE\n" +
                "WHERE TRANSACTION_ID = ?;\n" +
                "COMMIT;\n" +
                "END;";

            ps = connection.prepareStatement(queryDeleteFromSelfService);

            ps.setString(1, id);

            status = ps.executeUpdate();
            
            // Delete From Approval List Table
            
            String queryDeleteFromApprovalList = "BEGIN\n" +
                "DELETE FROM  " + " " + getSchema_Name() +
                ".XX_APPROVAL_LIST\n" +
                "WHERE TRANSACTION_ID = ?;\n" +
                "COMMIT;\n" +
                "END;";

            ps = connection.prepareStatement(queryDeleteFromApprovalList);

            ps.setString(1, id);

            status = ps.executeUpdate();
            
            // Delete From Work Flow Notification Table
            
            String queryDeleteFromWorkFlowNotification = "BEGIN\n" +
                "DELETE FROM  " + " " + getSchema_Name() +
                ".XX_WORKFLOW_NOTIFICATION\n" +
                "WHERE REQUEST_ID = ?;\n" +
                "COMMIT;\n" +
                "END;";

            ps = connection.prepareStatement(queryDeleteFromWorkFlowNotification);

            ps.setString(1, id);

            status = ps.executeUpdate();

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return Integer.toString(status);

    }
    
    public ArrayList<PeopleExtraInformationBean> getPendingSelfServicesByPersonNumber(PeopleExtraInformationBean body) {
        PeopleExtraInformationBean bean = null;
        ArrayList<PeopleExtraInformationBean> list = new ArrayList<PeopleExtraInformationBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "select * from  " + " " + getSchema_Name() + ".XXX_PER_PEOPLE_EXTRA_INFO where STATUS = 'PENDING_APPROVED' AND PERSON_NUMBER = ? ORDER BY ID DESC";
            ps = connection.prepareStatement(query);
            ps.setString(1, body.getPerson_number());
            rs = ps.executeQuery();
            while (rs.next()) {
                bean = new PeopleExtraInformationBean();
                bean.setId(Integer.parseInt(rs.getString("ID")));
                bean.setCode(rs.getString("EIT_CODE"));
                bean.setEit(rs.getString("EIT"));
                bean.setStatus(rs.getString("STATUS"));
                bean.setPerson_number(rs.getString("PERSON_NUMBER"));
                bean.setPerson_id(rs.getString("PERSON_ID"));
                bean.setCreated_by(rs.getString("CREATED_BY"));
                bean.setCreation_date(rs.getString("CREATION_DATE"));
                bean.setUpdated_by(rs.getString("UPDATED_BY"));
                bean.setUpdated_date(rs.getString("UPDATED_DATE"));
                bean.setLine_manager(rs.getString("LINE_MANAGER"));
                bean.setManage_of_manager(rs.getString("MANAGER_OF_MANAGER"));
                bean.setUrl(rs.getString("URL"));
                bean.setEit_name(rs.getString("EIT_NAME"));
                bean.setPersonName(rs.getString("PERSON_NAME"));
                list.add(bean);
            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return list;
    }
    
    public ArrayList<PeopleExtraInformationBean> getPendingAndApprovedSelfServicesByPersonNumber(PeopleExtraInformationBean body) {
        PeopleExtraInformationBean bean = null;
        ArrayList<PeopleExtraInformationBean> list = new ArrayList<PeopleExtraInformationBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "select * from  XXX_PER_PEOPLE_EXTRA_INFO where (STATUS = 'PENDING_APPROVED' OR STATUS = 'APPROVED') And PERSON_NUMBER=? ORDER BY ID DESC";
            ps = connection.prepareStatement(query);
            ps.setString(1, body.getPerson_number());
            rs = ps.executeQuery();
            while (rs.next()) {
                bean = new PeopleExtraInformationBean();
                bean.setId(Integer.parseInt(rs.getString("ID")));
                bean.setCode(rs.getString("EIT_CODE"));
                bean.setEit(rs.getString("EIT"));
                bean.setStatus(rs.getString("STATUS"));
                bean.setPerson_number(rs.getString("PERSON_NUMBER"));
                bean.setPerson_id(rs.getString("PERSON_ID"));
                bean.setCreated_by(rs.getString("CREATED_BY"));
                bean.setCreation_date(rs.getString("CREATION_DATE"));
                bean.setUpdated_by(rs.getString("UPDATED_BY"));
                bean.setUpdated_date(rs.getString("UPDATED_DATE"));
                bean.setLine_manager(rs.getString("LINE_MANAGER"));
                bean.setManage_of_manager(rs.getString("MANAGER_OF_MANAGER"));
                bean.setUrl(rs.getString("URL"));
                bean.setEit_name(rs.getString("EIT_NAME"));
                bean.setPersonName(rs.getString("PERSON_NAME"));
                list.add(bean);
            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return list;
    }
    
    
    
    public ArrayList<ApprovalListBean> getApprovalType(String serviceType, String T_id) {
        ArrayList<ApprovalListBean> approvalSetupList = new ArrayList<ApprovalListBean>();
        ApprovalListBean bean = null;
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query = " SELECT * FROM  "+ " " + getSchema_Name() + ".XX_APPROVAL_LIST WHERE SERVICE_TYPE = ? AND TRANSACTION_ID = ? AND NOTIFICATION_TYPE = 'FYA'";
            ps = connection.prepareStatement(query);
            ps.setString(1, serviceType);
            ps.setString(2, T_id);
            rs = ps.executeQuery();

            while (rs.next()) {
                bean = new ApprovalListBean();
                bean.setId(rs.getString("ID"));
                bean.setStepLeval(rs.getString("STEP_LEVEL"));
                bean.setServiceType(rs.getString("SERVICE_TYPE"));
                bean.setTransActionId(rs.getString("TRANSACTION_ID"));
                bean.setWorkflowId(rs.getString("WORKFLOW_ID"));
                bean.setRolrType(rs.getString("ROLE_TYPE"));
                bean.setRoleId(rs.getString("ROLE_ID"));
                bean.setRequestDatt(rs.getString("REQUEST_DATE"));
                bean.setResponseCode(rs.getString("RESPONSE_CODE"));
                bean.setNotificationType(rs.getString("NOTIFICATION_TYPE"));
                bean.setNote(rs.getString("NOTE"));
                bean.setResponseDate(rs.getString("RESPONSE_DATE"));
                approvalSetupList.add(bean);
            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return approvalSetupList;
    }
    
    public String updateApprovalList(ApprovalListBean bean) {
        int status = 0;
        WorkFlowNotificationBean objNotif = new WorkFlowNotificationBean();
        try {
            
            connection = AppsproConnection.getConnection();
            String query = null;
            query = "UPDATE " + getSchema_Name() + ".XX_APPROVAL_LIST SET ROLE_TYPE = ?, ROLE_ID = ?, REASON = ? WHERE ID = ? ";

            ps = connection.prepareStatement(query);
            ps.setString(1, bean.getRolrType());
            ps.setString(2, bean.getRoleId());
            ps.setString(3, bean.getReasonforReassign());
            ps.setString(4, bean.getId());

            status = ps.executeUpdate();
            
            
            query = "SELECT ID,MSG_TITLE,MSG_BODY,CREATION_DATE,RECEIVER_TYPE,RECEIVER_ID,RESPONSE_PERSON_ID," +
                "TYPE,RESPONSE_DATE,WORKFLOW_ID,REQUEST_ID,STATUS,SELF_TYPE," +
                "PERSON_NAME,NATIONALIDENTITY FROM  "+ getSchema_Name() + ".XX_WORKFLOW_NOTIFICATION WHERE SELF_TYPE = ? AND STATUS = 'OPEN' AND REQUEST_ID = ? AND TYPE = 'FYA'";
            
            ps = connection.prepareStatement(query);
            ps.setString(1, bean.getServiceType());
            ps.setString(2, bean.getResponseCode());
            
            rs = ps.executeQuery();
            
            while (rs.next()) {
            objNotif.setID(rs.getString("ID"));
            objNotif.setMsgTitle(rs.getString("MSG_TITLE"));
            objNotif.setMsgBody(rs.getString("MSG_BODY"));
            objNotif.setCreationDate(rs.getString("CREATION_DATE"));
            objNotif.setReceiverType(rs.getString("RECEIVER_TYPE"));
            objNotif.setReceiverId(rs.getString("RECEIVER_ID"));
            objNotif.setResponsePersonId(rs.getString("RESPONSE_PERSON_ID"));
            objNotif.setType(rs.getString("TYPE"));
            objNotif.setResponseDate(rs.getString("RESPONSE_DATE"));
            objNotif.setWorkflowId(rs.getString("WORKFLOW_ID"));
            objNotif.setRequestId(rs.getString("REQUEST_ID"));
            objNotif.setStatus(rs.getString("STATUS"));
            objNotif.setSelfType(rs.getString("SELF_TYPE"));
            objNotif.setNationalIdentity(rs.getString("NATIONALIDENTITY"));
            objNotif.setPersonName(rs.getString("PERSON_NAME"));
            }
            
            query = "UPDATE "+ getSchema_Name() + ".XX_WORKFLOW_NOTIFICATION SET STATUS = 'CLOSED' WHERE ID = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, objNotif.getID());
            status = ps.executeUpdate();
            
            query =
                    " insert into  " + " " + getSchema_Name() + ".XX_WORKFLOW_NOTIFICATION (MSG_TITLE,MSG_BODY,CREATION_DATE,RECEIVER_TYPE,RECEIVER_ID,RESPONSE_PERSON_ID,TYPE,RESPONSE_DATE,WORKFLOW_ID,REQUEST_ID,STATUS,SELF_TYPE,PERSON_NAME,NATIONALIDENTITY) " +
                    "VALUES(?,?,SYSDATE,?,?,?,?,?,?,?,?,?,?,?) ";
            ps = connection.prepareStatement(query);
            ps.setString(1, objNotif.getMsgTitle());
            ps.setString(2, objNotif.getMsgBody());
            ps.setString(3, bean.getRolrType());
            ps.setString(4, bean.getRoleId());
            ps.setString(5, objNotif.getResponsePersonId());
            ps.setString(6, objNotif.getType());
            ps.setString(7, objNotif.getResponseDate());
            ps.setString(8, objNotif.getWorkflowId());
            ps.setString(9, objNotif.getRequestId());
            ps.setString(10, objNotif.getStatus());
            ps.setString(11, objNotif.getSelfType());
            ps.setString(12, objNotif.getPersonName());
            ps.setString(13, objNotif.getNationalIdentity());
            status = ps.executeUpdate();
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return Integer.toString(status);
    }
    
    
    
}
