/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.dao;


import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.BulkTransactionBean;
import com.appspro.fusionsshr.bean.PeopleExtraInformationBean;

import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;


/**
 *
 * @author lenovo
 */
public class PaasBulkTransactionDAO extends AppsproConnection {

    private int max_bulk_transaction;
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();

    public BulkTransactionBean addBulkTransaction(BulkTransactionBean bulkBean) {

        try {
            connection = AppsproConnection.getConnection();

            String query =
                "insert into XXX_BulkTransaction(CREATEDBY,CREATEDDTE,PERSONEXTRAINFORMATIONID,TRANSACTION_NUMBER)values(?,sysdate,?,?)";

            ps = connection.prepareStatement(query);
            ps.setString(1, bulkBean.getCreateBy());
            ps.setString(2, bulkBean.getPersonExtraInformationID());
            ps.setInt(3, bulkBean.getTransactionNumber());
            ps.executeUpdate();

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return bulkBean;
    }

    //*****************Get Max Bulk Transaction Number Sequence****************

    public int getMaxBulkTransaction() {
        BulkTransactionBean bulkTransactionBean = new BulkTransactionBean();

        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "SELECT NVL(max(transaction_number),0) AS max_bulk_transaction FROM  " +
                    " " + getSchema_Name() + ".XXX_BULKTRANSACTION";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                //bean.setId(rs.getString("NEXTVAL"));
                bulkTransactionBean.setTransactionNumber(Integer.parseInt(rs.getString("max_bulk_transaction")));

            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
             closeResources(connection, ps, rs);
        }
        max_bulk_transaction = bulkTransactionBean.getTransactionNumber();

        max_bulk_transaction++;

        return max_bulk_transaction;
    }

    //********************Get Summary Bulk Transaction*************************

    public ArrayList<PeopleExtraInformationBean> getSummaryBulkTransaction(String eitCode) {
        PeopleExtraInformationBean bean = null;
        ArrayList<PeopleExtraInformationBean> list =
            new ArrayList<PeopleExtraInformationBean>();

        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "SELECT PPEI.ID,PPEI.EIT_CODE ,PPEI.STATUS ,PPEI.PERSON_NUMBER ,PPEI.LINE_MANAGER,BT.CREATEDDTE,\n" +
                    "                    PPEI.MANAGER_OF_MANAGER ,PPEI.URL ,PPEI.EIT_NAME,PPEI.EIT,PPEI.PERSON_ID,PPEI.PEI_INFORMATION1 ,\n" +
                    "                    PPEI.PEI_INFORMATION2 ,PPEI.PEI_INFORMATION3 ,PPEI.PEI_INFORMATION4 ,PPEI.PEI_INFORMATION5 ,\n" +
                    "                    PPEI.PEI_INFORMATION6 ,PPEI.PEI_INFORMATION7,PPEI.PEI_INFORMATION8,PPEI.PEI_INFORMATION9,\n" +
                    "                   PPEI.PEI_INFORMATION10,PPEI.PEI_INFORMATION11,PPEI.PEI_INFORMATION13,PPEI.PEI_INFORMATION_DATE1,bt.transaction_number,\n" +
                    "                    PPEI.PEI_INFORMATION_DATE15\n" +
                    "                   FROM  " + " " + getSchema_Name() +
                    ".XXX_PER_PEOPLE_EXTRA_INFO PPEI\n" +
                    "                    INNER JOIN  " + " " +
                    getSchema_Name() + ".XXX_BULKTRANSACTION BT\n" +
                    "                    ON PPEI.id = bt.personextrainformationid\n" +
                    "                    WHERE ppei.eit_code = ?\n" +
                    "                    order by bt.transaction_number";
            ps = connection.prepareStatement(query);
            ps.setString(1, eitCode);
            rs = ps.executeQuery();
            while (rs.next()) {
                bean = new PeopleExtraInformationBean();
                bean.setId(Integer.parseInt(rs.getString("ID")));
                bean.setCode(rs.getString("EIT_CODE"));
                bean.setEit(rs.getString("EIT"));
                bean.setStatus(rs.getString("STATUS"));
                bean.setPerson_number(rs.getString("PERSON_NUMBER"));
                bean.setPerson_id(rs.getString("PERSON_ID"));
                bean.setCreation_date(rs.getString("CREATEDDTE"));
                bean.setLine_manager(rs.getString("LINE_MANAGER"));
                bean.setManage_of_manager(rs.getString("MANAGER_OF_MANAGER"));
                bean.setUrl(rs.getString("URL"));
                bean.setPEI_INFORMATION1(rs.getString("PEI_INFORMATION1"));
                bean.setPEI_INFORMATION2(rs.getString("PEI_INFORMATION2"));
                bean.setPEI_INFORMATION3(rs.getString("PEI_INFORMATION3"));
                bean.setPEI_INFORMATION4(rs.getString("PEI_INFORMATION4"));
                bean.setPEI_INFORMATION5(rs.getString("PEI_INFORMATION5"));
                bean.setPEI_INFORMATION6(rs.getString("PEI_INFORMATION6"));
                bean.setPEI_INFORMATION7(rs.getString("PEI_INFORMATION7"));
                bean.setPEI_INFORMATION8(rs.getString("PEI_INFORMATION8"));
                bean.setPEI_INFORMATION9(rs.getString("PEI_INFORMATION9"));
                bean.setPEI_INFORMATION10(rs.getString("PEI_INFORMATION10"));
                bean.setPEI_INFORMATION11(rs.getString("PEI_INFORMATION11"));
                bean.setPEI_INFORMATION13(rs.getString("PEI_INFORMATION13"));
                bean.setPEI_INFORMATION_DATE1(rs.getString("PEI_INFORMATION_DATE1"));
                bean.setPEI_INFORMATION_DATE15(rs.getString("PEI_INFORMATION_DATE15"));
                bean.setPEI_ATTRIBUTE1(rs.getString("transaction_number"));
                list.add(bean);
            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
             closeResources(connection, ps, rs);
        }

        //         List<PeopleExtraInformationBean> tempList = new ArrayList<PeopleExtraInformationBean>();
        //
        //         tempList = list;
        //         ArrayList<PeopleExtraInformationBean[]> arrList = new ArrayList<>();
        //         for (PeopleExtraInformationBean peopleExtraInformationBean : tempList) {
        //             if(tempList.get(0).getPei_ATTRIBUTE1() == "1")
        //                 arrList.add(peopleExtraInformationBean)
        //         }
        //         //(tempList.get(0).getPei_ATTRIBUTE1());
        return list;
    }

    public ArrayList<PeopleExtraInformationBean> getSummaryBulkTransactionBYTransactionNumber(String transactionNumber) {
        PeopleExtraInformationBean bean = null;
        ArrayList<PeopleExtraInformationBean> list =
            new ArrayList<PeopleExtraInformationBean>();

        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "SELECT    PPEI.ID,PPEI.EIT_CODE ,PPEI.STATUS ,PPEI.PERSON_NUMBER ,PPEI.LINE_MANAGER,BT.CREATEDDTE,\n" +
                    "                   PPEI.MANAGER_OF_MANAGER ,PPEI.URL ,PPEI.EIT_NAME,PPEI.EIT,PPEI.PERSON_ID,PPEI.PEI_INFORMATION1 ,\n" +
                    "                   PPEI.PEI_INFORMATION2 ,PPEI.PEI_INFORMATION3 ,PPEI.PEI_INFORMATION4 ,PPEI.PEI_INFORMATION5 ,\n" +
                    "                    PPEI.PEI_INFORMATION6 ,PPEI.PEI_INFORMATION7,PPEI.PEI_INFORMATION8,PPEI.PEI_INFORMATION9,\n" +
                    "                  PPEI.PEI_INFORMATION10,PPEI.PEI_INFORMATION11,PPEI.PEI_INFORMATION13,PPEI.PEI_INFORMATION_DATE1,bt.transaction_number,\n" +
                    "                  PPEI.PEI_INFORMATION_DATE15,BT.TRANSACTION_NUMBER\n" +
                    "                  FROM  " + " " + getSchema_Name() +
                    ".XXX_PER_PEOPLE_EXTRA_INFO PPEI\n" +
                    "                   INNER JOIN  " + " " +
                    getSchema_Name() + ".XXX_BULKTRANSACTION BT\n" +
                    "                    ON PPEI.id = bt.personextrainformationid\n" +
                    "                  WHERE bt.TRANSACTION_NUMBER = ?\n" +
                    "                   order by bt.transaction_number";
            ps = connection.prepareStatement(query);
            ps.setString(1, transactionNumber);
            rs = ps.executeQuery();
            while (rs.next()) {
                bean = new PeopleExtraInformationBean();
                bean.setId(Integer.parseInt(rs.getString("ID")));
                bean.setCode(rs.getString("EIT_CODE"));
                bean.setEit(rs.getString("EIT"));
                bean.setStatus(rs.getString("STATUS"));
                bean.setPerson_number(rs.getString("PERSON_NUMBER"));
                bean.setPerson_id(rs.getString("PERSON_ID"));
                bean.setCreation_date(rs.getString("CREATEDDTE"));
                bean.setLine_manager(rs.getString("LINE_MANAGER"));
                bean.setManage_of_manager(rs.getString("MANAGER_OF_MANAGER"));
                bean.setUrl(rs.getString("URL"));
                bean.setPEI_INFORMATION1(rs.getString("PEI_INFORMATION1"));
                bean.setPEI_INFORMATION2(rs.getString("PEI_INFORMATION2"));
                bean.setPEI_INFORMATION3(rs.getString("PEI_INFORMATION3"));
                bean.setPEI_INFORMATION4(rs.getString("PEI_INFORMATION4"));
                bean.setPEI_INFORMATION5(rs.getString("PEI_INFORMATION5"));
                bean.setPEI_INFORMATION6(rs.getString("PEI_INFORMATION6"));
                bean.setPEI_INFORMATION7(rs.getString("PEI_INFORMATION7"));
                bean.setPEI_INFORMATION8(rs.getString("PEI_INFORMATION8"));
                bean.setPEI_INFORMATION9(rs.getString("PEI_INFORMATION9"));
                bean.setPEI_INFORMATION10(rs.getString("PEI_INFORMATION10"));
                bean.setPEI_INFORMATION11(rs.getString("PEI_INFORMATION11"));
                bean.setPEI_INFORMATION13(rs.getString("PEI_INFORMATION13"));
                bean.setPEI_INFORMATION_DATE1(rs.getString("PEI_INFORMATION_DATE1"));
                bean.setPEI_INFORMATION_DATE15(rs.getString("PEI_INFORMATION_DATE15"));
                bean.setPEI_ATTRIBUTE1(rs.getString("transaction_number"));
                list.add(bean);
            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
             closeResources(connection, ps, rs);
        }

        //         List<PeopleExtraInformationBean> tempList = new ArrayList<PeopleExtraInformationBean>();
        //
        //         tempList = list;
        //         ArrayList<PeopleExtraInformationBean[]> arrList = new ArrayList<>();
        //         for (PeopleExtraInformationBean peopleExtraInformationBean : tempList) {
        //             if(tempList.get(0).getPei_ATTRIBUTE1() == "1")
        //                 arrList.add(peopleExtraInformationBean)
        //         }
        //         //(tempList.get(0).getPei_ATTRIBUTE1());
        return list;
    }

    public PeopleExtraInformationBean updateStatusBulkTrasnsaction(PeopleExtraInformationBean extraInformationBean) {
        int res = 0;
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "update XXX_PER_PEOPLE_EXTRA_INFO set STATUS=? where ID=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, extraInformationBean.getStatus());
            ps.setInt(2, extraInformationBean.getId());

            res = ps.executeUpdate();
            //("test update" + res);
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return extraInformationBean;
    }

}
