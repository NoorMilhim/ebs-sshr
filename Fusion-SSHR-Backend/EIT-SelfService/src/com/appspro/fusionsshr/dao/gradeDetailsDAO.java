/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.OrgnizationBean;
import com.appspro.fusionsshr.bean.gradeBean;
import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.trustAllHosts;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author lenovo
 */
public class gradeDetailsDAO extends RestHelper {

    final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

    public ArrayList<gradeBean> getOrgDetails() {
        ArrayList<gradeBean> gradeList = new ArrayList<gradeBean>();
        String finalresponse = "";
        //        String jwttoken = jwt.trim();
        gradeBean org = new gradeBean();

        HttpsURLConnection https = null;
        HttpURLConnection connection = null;

        String serverUrl = getInstanceUrl() + getGradeUrl();
        //getInstanceUrl()+

        String jsonResponse = "";
        try {
            URL url
                    = new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection) url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
            } else {
                connection = (HttpURLConnection) url.openConnection();
            }
            String SOAPAction
                    = getInstanceUrl();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json;");
            connection.setRequestProperty("Accept", "application/json");
            connection.setConnectTimeout(6000000);
            connection.setRequestProperty("SOAPAction", SOAPAction);
            connection.setRequestProperty("charset", "utf-8");
            //connection.setRequestProperty("Authorization","Bearer " + jwttoken);
            connection.setRequestProperty("Authorization",
                    "Basic " + getAuth());

            BufferedReader in
                    = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            JSONObject obj = new JSONObject(response.toString());
            JSONArray arr = obj.getJSONArray("items");

            for (int i = 0; i < arr.length(); i++) {
                gradeBean bean = new gradeBean();
                JSONObject orgObj = arr.getJSONObject(i);
                bean.setGradeId(orgObj.getInt("GradeId"));
                bean.setGradeName(orgObj.getString("GradeName"));

                gradeList.add(bean);
            }

        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }

        if (finalresponse.length() > 1) {
        } else {
            finalresponse = jsonResponse;
        }

        return gradeList;
    }
}
