/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;

import com.appspro.fusionsshr.bean.SecurityAllowanceBean;
import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author amro
 */
public class SecurityAllowanceDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();

    public SecurityAllowanceBean insertOrUpdateSecurityAllowance(SecurityAllowanceBean bean, String transactionType) {

        try {
            connection = AppsproConnection.getConnection();
            if (!bean.getEndDate().isEmpty()) {
                bean.setEndDate(rh.convertToGregorian(bean.getEndDate()));
            }
            if (!bean.getStartDate().isEmpty()) {
                bean.setStartDate(rh.convertToGregorian(bean.getStartDate()));
            }
            if (transactionType.equals("ADD")) {

                String query = "INSERT INTO  "+ " " + getSchema_Name() + ".XXX_SECURITY_ALLOWANCE (START_DATE,END_DATE,FLAG,JOB_NAME ,CREATEDBY,CREATEDDATE)\n"
                        + "                 VALUES (?, ?, ? ,? , ? ,?)";

                ps = connection.prepareStatement(query);
//                ps.setString(1, bean.getId());
                ps.setDate(1, getSQLDateFromString(bean.getStartDate()));
                ps.setDate(2, getSQLDateFromString(bean.getEndDate()));
                ps.setString(3, bean.getFlag());
                ps.setString(4, bean.getJobName());
                ps.setString(5, bean.getCreatedBy());
                ps.setDate(6, getSQLDateFromString(bean.getCreationDate()));
                ps.executeUpdate();
                ArrayList<SecurityAllowanceBean> allowanceBeansList = getMaxID();
                UpdateSecurityAllowanceCode(allowanceBeansList.get(allowanceBeansList.size() - 1));

            } else if (transactionType.equals("EDIT")) {
                String oldFlag = "N";
                String query = "UPDATE  "+ " " + getSchema_Name() + ".XXX_SECURITY_ALLOWANCE \n"
                        + "SET  UPDATEDATE =sysdate \n"
                        + ",END_DATE = sysdate "
                        + ", UPDATEBY = ? "
                        + ",FLAG = ?"
                        + "WHERE id = ? ";
                ps = connection.prepareStatement(query);
                ps.setString(1, bean.getUpdatedBy());
                ps.setString(2, oldFlag);
                ps.setInt(3, Integer.parseInt(bean.getId()));
                ps.executeUpdate();

                query = "INSERT INTO  "+ " " + getSchema_Name() + ".XXX_SECURITY_ALLOWANCE (START_DATE,END_DATE,FLAG,JOB_NAME ,CREATEDBY,CREATEDDATE ,CODE)\n"
                        + "                 VALUES (?, ?, ? ,? , ? ,?,?)";

                ps = connection.prepareStatement(query);
//                ps.setString(1, bean.getId());
                ps.setDate(1, getSQLDateFromString(bean.getStartDate()));
                ps.setDate(2, getSQLDateFromString(bean.getEndDate()));
                ps.setString(3, bean.getFlag());
                ps.setString(4, bean.getJobName());
                ps.setString(5, bean.getCreatedBy());
                ps.setDate(6, getSQLDateFromString(bean.getCreationDate()));
                ps.setString(7, bean.getCode());
//                ps.setString(8, bean.getUpdatedBy());
//                ps.setDate(9, getSQLDateFromString(bean.getUpdatedDate()));
                ps.executeUpdate();

            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }

    public ArrayList<SecurityAllowanceBean> getAllSecurityAllowance() {
        ArrayList<SecurityAllowanceBean> securityAllowanceBeansList = new ArrayList<SecurityAllowanceBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = " select  * from  "+ " " + getSchema_Name() + ".XXX_SECURITY_ALLOWANCE  ";

            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                SecurityAllowanceBean bean = new SecurityAllowanceBean();
                bean.setCreatedBy(rs.getString("CREATEDBY"));
                bean.setCreationDate(rs.getString("CREATEDDATE"));
                bean.setFlag(rs.getString("FLAG"));
                bean.setId(rs.getString("ID"));
                bean.setJobName(rs.getString("JOB_NAME"));
                bean.setUpdatedBy(rs.getString("UPDATEBY"));
                bean.setUpdatedDate(rs.getString("UPDATEDATE"));
                //get hijri start date and end date
                if (rs.getString("END_DATE") != null) {
                    bean.setEndDate(rh.convertToHijri(rs.getString("END_DATE")));
                } else {
                    bean.setEndDate((rs.getString("END_DATE")));
                }
                if (rs.getString("START_DATE") != null) {
                    bean.setStartDate(rh.convertToHijri(rs.getString("START_DATE")));
                } else {
                    bean.setStartDate((rs.getString("START_DATE")));
                }//end 
                securityAllowanceBeansList.add(bean);
            }

        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            //  JsonObject obj = new JsonObject();
            //   obj.addProperty("Error", "Internal Error: " + e.getMessage());
            //   array.add(obj);
        } finally {
            closeResources(connection, ps, rs);
        }

        return securityAllowanceBeansList;
    }

    public ArrayList<SecurityAllowanceBean> getAllSecurityAllowanceByJobName(String jobName) {
        ArrayList<SecurityAllowanceBean> securityAllowanceBeansList = new ArrayList<SecurityAllowanceBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = " select  * from  "+ " " + getSchema_Name() + ".XXX_SECURITY_ALLOWANCE  "
                    + "where JOB_NAME=?"
                    + "AND FLAG = ?";

            ps = connection.prepareStatement(query);
            ps.setString(1, jobName);
            ps.setString(2, "Y");
            rs = ps.executeQuery();
            while (rs.next()) {
                SecurityAllowanceBean bean = new SecurityAllowanceBean();
                bean.setCreatedBy(rs.getString("CREATEDBY"));
                bean.setCreationDate(rs.getString("CREATEDDATE"));
                bean.setFlag(rs.getString("FLAG"));
                bean.setId(rs.getString("ID"));
                bean.setJobName(rs.getString("JOB_NAME"));
                bean.setUpdatedBy(rs.getString("UPDATEBY"));
                bean.setUpdatedDate(rs.getString("UPDATEDATE"));
                bean.setCode(rs.getString("CODE"));
                //get hijri start date and end date
                if (rs.getString("END_DATE") != null) {
                    bean.setEndDate(rh.convertToHijri(rs.getString("END_DATE")));
                } else {
                    bean.setEndDate((rs.getString("END_DATE")));
                }
                if (rs.getString("START_DATE") != null) {
                    bean.setStartDate(rh.convertToHijri(rs.getString("START_DATE")));
                } else {
                    bean.setStartDate((rs.getString("START_DATE")));
                }//end 
                securityAllowanceBeansList.add(bean);
            }

        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            //  JsonObject obj = new JsonObject();
            //   obj.addProperty("Error", "Internal Error: " + e.getMessage());
            //   array.add(obj);
        } finally {
            closeResources(connection, ps, rs);
        }

        return securityAllowanceBeansList;

    }

    public ArrayList<SecurityAllowanceBean> getMaxID() {
        ArrayList<SecurityAllowanceBean> allowanceBeansList = new ArrayList<SecurityAllowanceBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = "    select  * from  "+ " " + getSchema_Name() + ".XXX_SECURITY_ALLOWANCE  \n"
                    + "                WHERE ID = (SELECT MAX(ID) from  "+ " " + getSchema_Name() + ".XXX_SECURITY_ALLOWANCE)";

            ps = connection.prepareStatement(query);

            rs = ps.executeQuery();
            while (rs.next()) {
                SecurityAllowanceBean bean = new SecurityAllowanceBean();
                bean.setCreatedBy(rs.getString("CREATEDBY"));
                bean.setCreationDate(rs.getString("CREATEDDATE"));
                bean.setEndDate(rs.getString("END_DATE"));
                bean.setFlag(rs.getString("FLAG"));
                bean.setId(rs.getString("ID"));
                bean.setJobName(rs.getString("JOB_NAME"));
                bean.setStartDate(rs.getString("START_DATE"));
                bean.setUpdatedBy(rs.getString("UPDATEBY"));
                bean.setUpdatedDate(rs.getString("UPDATEDATE"));

                allowanceBeansList.add(bean);
            }

        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            //  JsonObject obj = new JsonObject();
            //   obj.addProperty("Error", "Internal Error: " + e.getMessage());
            //   array.add(obj);
        } finally {
            closeResources(connection, ps, rs);
        }

        return allowanceBeansList;

    }

    public SecurityAllowanceBean UpdateSecurityAllowanceCode(SecurityAllowanceBean bean) {
        try {
            connection = AppsproConnection.getConnection();
            String code = bean.getId() + bean.getJobName();
            String query = "UPDATE  "+ " " + getSchema_Name() + ".XXX_SECURITY_ALLOWANCE \n"
                    + "SET CODE = ? "
                    + "WHERE id = ? ";
            ps = connection.prepareStatement(query);
            ps.setString(1, code);
            ps.setInt(2, Integer.parseInt(bean.getId()));
            ps.executeUpdate();
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }

    public ArrayList<SecurityAllowanceBean> getAllSecurityAllowanceByCode(String code) {
        ArrayList<SecurityAllowanceBean> securityAllowanceBeansList = new ArrayList<SecurityAllowanceBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = " select  * from  "+ " " + getSchema_Name() + ".XXX_SECURITY_ALLOWANCE  "
                    + "where CODE=?"
                    + "AND FLAG = ?";

            ps = connection.prepareStatement(query);
            ps.setString(1, code);
            ps.setString(2, "Y");
            rs = ps.executeQuery();
            while (rs.next()) {
                SecurityAllowanceBean bean = new SecurityAllowanceBean();
                bean.setCreatedBy(rs.getString("CREATEDBY"));
                bean.setCreationDate(rs.getString("CREATEDDATE"));
                bean.setEndDate(rs.getString("END_DATE"));
                bean.setFlag(rs.getString("FLAG"));
                bean.setId(rs.getString("ID"));
                bean.setJobName(rs.getString("JOB_NAME"));
                bean.setStartDate(rs.getString("START_DATE"));
                bean.setUpdatedBy(rs.getString("UPDATEBY"));
                bean.setUpdatedDate(rs.getString("UPDATEDATE"));

                securityAllowanceBeansList.add(bean);
            }

        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            //  JsonObject obj = new JsonObject();
            //   obj.addProperty("Error", "Internal Error: " + e.getMessage());
            //   array.add(obj);
        } finally {
            closeResources(connection, ps, rs);
        }

        return securityAllowanceBeansList;

    }

    public java.sql.Date getSQLDateFromString(String date) {
        java.sql.Date sqlDate = null;
        try {
            Date utilDate = new SimpleDateFormat("yyyy-MM-dd").parse(date);
            sqlDate = new java.sql.Date(utilDate.getTime());
        } catch (Exception ex) {
           ex.printStackTrace(); AppsproConnection.LOG.error("ERROR", ex);
        }
        return sqlDate;
    }

}
