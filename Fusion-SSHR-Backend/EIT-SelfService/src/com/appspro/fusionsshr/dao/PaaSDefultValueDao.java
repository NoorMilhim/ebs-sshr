/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.dao;


import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.PaaSDefultValueBean;

import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;


/**
 *
 * @author Lenovo
 */
public class PaaSDefultValueDao extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
   // RestHelper rh = new RestHelper();

    public ArrayList<PaaSDefultValueBean> getAllPaaSDefultValue() {
        ArrayList<PaaSDefultValueBean> paaSDefultValueList =
            new ArrayList<PaaSDefultValueBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                " select  * from  " + " " + getSchema_Name() + ".XXX_PAAS_DEFAULT_VALUE ";

            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                PaaSDefultValueBean paaSDefaultValueBean =
                    new PaaSDefultValueBean();
                paaSDefaultValueBean.setEitCode(rs.getString("EIT_CODE"));
                paaSDefaultValueBean.setSegmentName(rs.getString("SEGEMENT_NAME"));
                paaSDefaultValueBean.setQuery(rs.getString("QUERY"));
                paaSDefultValueList.add(paaSDefaultValueBean);
                //("Test "+ rs.getString("SEGEMENT_NAME"));
            }
            //("Test ");

            rs.close();
            ps.close();
            connection.close();
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);

        } finally {
            closeResources(connection, ps, rs);
        }

        return paaSDefultValueList;
    }

}
