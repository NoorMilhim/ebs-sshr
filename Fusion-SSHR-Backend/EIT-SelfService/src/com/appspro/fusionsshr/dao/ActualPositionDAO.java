/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.dao;


import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.ActualPosition;

import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;


/**
 *
 * @author CPBSLV
 */
public class ActualPositionDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();

    public ActualPosition insertOrUpdatePosition(ActualPosition bean,
                                                 String transactionType) {
        try {
            connection = AppsproConnection.getConnection();
            if (!bean.getEndDate().isEmpty()) {
                bean.setEndDate(rh.convertToGregorian(bean.getEndDate()));
            }
            if (!bean.getStartDate().isEmpty()) {
                bean.setStartDate(rh.convertToGregorian(bean.getStartDate()));
            }

            if (transactionType.equals("ADD")) {
                String query =
                    "SELECT  " + " " + getSchema_Name() + ".POSITION_CODE_SEQ.NEXTVAL  from dual";
                ps = connection.prepareStatement(query);
                rs = ps.executeQuery();
                Integer positionCode = 0;
                if (rs.next()) {
                    positionCode = rs.getInt(1);

                }
                query =
                        "INSERT INTO  " + " " + getSchema_Name() + ".XXX_ACTUAL_POSITION (STARTED_DATE,END_DATE,FAMILY_JOB,FAMILY_JOB_CODE,POSITION_NUMBER ,MANAGING,POSITION_NAME,FLAG,CREATEDBY,CREATEDDATE,STATUS)\n" +
                        "                    VALUES ( ?, ? ,? ,?, ? ,?,? ,?,?,?,?)";

                ps = connection.prepareStatement(query);

                ps.setDate(1, getSQLDateFromString(bean.getStartDate()));
                ps.setDate(2, getSQLDateFromString(bean.getEndDate()));
                ps.setString(3, bean.getFamilyJob());
                ps.setString(4, bean.getFamilyJobCode());
                ps.setString(5, bean.getPositionNumber());
                ps.setString(6, bean.getManaging());
                ps.setString(7, bean.getPositionName());
                ps.setString(8, bean.getFlag());
                ps.setString(9, bean.getCreatedBy());

                ps.setDate(10, getSQLDateFromString(bean.getCreatedDate()));

                ps.setString(11, bean.getStatus());

                ps.executeUpdate();

            } else if (transactionType.equals("EDIT")) {
                String oldFlag = "N";
                String query =
                    "UPDATE  " + " " + getSchema_Name() + ".XXX_ACTUAL_POSITION \n" +
                    "SET  UPDATEDATE =sysdate \n" +
                    ",END_DATE = sysdate " + ",FLAG = ?" + ", UPDATEBY = ? " +
                    "WHERE id = ? ";
                ps = connection.prepareStatement(query);
                ps.setString(1, oldFlag);
                ps.setString(2, bean.getUpdatedBy());
                ps.setInt(3, Integer.parseInt(bean.getId()));
                ps.executeUpdate();

                query =
                        "INSERT INTO  " + " " + getSchema_Name() + ".XXX_ACTUAL_POSITION (STARTED_DATE,END_DATE,FAMILY_JOB,FAMILY_JOB_CODE,POSITION_NUMBER ,MANAGING,POSITION_NAME,FLAG,CREATEDBY,CREATEDDATE,STATUS)\n" +
                        "                    VALUES ( ?, ? ,? ,?, ? ,?,? ,?,?,?,?)";

                ps = connection.prepareStatement(query);

                ps.setDate(1, getSQLDateFromString(bean.getStartDate()));
                ps.setDate(2, getSQLDateFromString(bean.getEndDate()));
                ps.setString(3, bean.getFamilyJob());
                ps.setString(4, bean.getFamilyJobCode());
                ps.setString(5, bean.getPositionNumber());
                ps.setString(6, bean.getManaging());
                ps.setString(7, bean.getPositionName());
                ps.setString(8, bean.getFlag());
                ps.setString(9, bean.getCreatedBy());

                ps.setDate(10, getSQLDateFromString(bean.getCreatedDate()));

                ps.setString(11, bean.getStatus());

                ps.executeUpdate();
            } else if (transactionType.equals("CORRECTION")) {
                String oldFlag = "N";
                String query =
                    "UPDATE  " + " " + getSchema_Name() + ".XXX_ACTUAL_POSITION \n" +
                    "SET  UPDATEDATE =sysdate \n" +
                    ",FLAG = ?" + ", UPDATEBY = ? " + ",STARTED_DATE = ?" +
                    ",END_DATE = ?" + ",STATUS = ?" + ",FAMILY_JOB_CODE = ?" +
                    ",POSITION_NUMBER = ?" + ",MANAGING = ?" +
                    ",POSITION_NAME = ?" + ",FAMILY_JOB = ?" + "WHERE id = ? ";
                ps = connection.prepareStatement(query);
                ps.setString(1, bean.getFlag());
                ps.setString(2, bean.getUpdatedBy());
                ps.setDate(3, getSQLDateFromString(bean.getStartDate()));
                ps.setDate(4, getSQLDateFromString(bean.getEndDate()));
                ps.setString(5, bean.getPositionNumber());
                ps.setString(6, bean.getManaging());
                ps.setString(7, bean.getPositionName());
                ps.setString(8, bean.getFamilyJobCode());
                ps.setString(9, bean.getFamilyJob());
                ps.setString(10, bean.getFlag());
                ps.setString(11, bean.getStatus());
                ps.executeUpdate();
            } else if (transactionType.equals("Update_Change_Insert")) {
                updateCahngeInsert(bean);
            } else if (transactionType.equals("UPDATE_OVERRIDE")) {
                updateOverride(bean);
            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }

    public ArrayList<ActualPosition> searchPositions(String familyjob,
                                                     String postionNumber,
                                                     String managing,
                                                     String positionName) {
        ArrayList<ActualPosition> postionsList =
            new ArrayList<ActualPosition>();
        try {
            connection = AppsproConnection.getConnection();

            if (familyjob.equals("undefined")) {
                familyjob = "";
            }
            if (postionNumber.equals("undefined")) {
                postionNumber = "";
            }
            if (managing.equals("undefined")) {
                managing = "";
            }
            if (positionName.equals("undefined")) {
                positionName = "";
            }

            String query =
                "select * from  " + " " + getSchema_Name() + ".XXX_ACTUAL_POSITION positions where positions.FLAG = ? AND(  positions.FAMILY_JOB = NVL (?, positions.FAMILY_JOB) AND positions.POSITION_NUMBER = NVL (?, positions.POSITION_NUMBER) AND positions.MANAGING = NVL (?, positions.MANAGING) AND positions.POSITION_NAME = NVL (?, positions.POSITION_NAME))";

            ps = connection.prepareStatement(query);
            ps.setString(1, "Y");
            ps.setString(2, familyjob);
            ps.setString(3, postionNumber);
            ps.setString(4, managing);
            ps.setString(5, positionName);

            rs = ps.executeQuery();
            while (rs.next()) {
                ActualPosition positionBean = new ActualPosition();
                positionBean.setStatus(rs.getString("STATUS"));
                positionBean.setFamilyJob(rs.getString("FAMILY_JOB"));
                positionBean.setCreatedBy(rs.getString("CREATEDBY"));
                positionBean.setCreatedDate(rs.getString("CREATEDDATE"));
                positionBean.setFlag(rs.getString("FLAG"));
                positionBean.setId(rs.getString("ID"));
                positionBean.setManaging(rs.getString("MANAGING"));
                positionBean.setPositionName(rs.getString("POSITION_NAME"));
                positionBean.setPositionNumber(rs.getString("POSITION_NUMBER"));
                positionBean.setUpdateDate(rs.getString("UPDATEDATE"));
                positionBean.setUpdatedBy(rs.getString("UPDATEBY"));
                positionBean.setFamilyJobCode(rs.getString("FAMILY_JOB_CODE"));

                //get hijri start date and end date
                if (rs.getString("END_DATE") != null) {
                    positionBean.setEndDate(rh.convertToHijri(rs.getString("END_DATE")));
                } else {
                    positionBean.setEndDate((rs.getString("END_DATE")));
                }
                if (rs.getString("STARTED_DATE") != null) {
                    positionBean.setStartDate(rh.convertToHijri(rs.getString("STARTED_DATE")));
                } else {
                    positionBean.setStartDate((rs.getString("STARTED_DATE")));
                } //end
                postionsList.add(positionBean);

            }

        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            //   array.add(obj);
        } finally {
            closeResources(connection, ps, rs);
        }

        return postionsList;
    }

    public ArrayList<ActualPosition> getAllPositions() {
        ArrayList<ActualPosition> postionsList =
            new ArrayList<ActualPosition>();
        try {
            connection = AppsproConnection.getConnection();

            String query =
                " select * from  " + " " + getSchema_Name() + ".XXX_ACTUAL_POSITION actual_positions ";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                ActualPosition actualpositionBean = new ActualPosition();
                actualpositionBean.setCreatedBy(rs.getString("CREATEDBY"));
                actualpositionBean.setCreatedDate(rs.getString("CREATEDDATE"));
                actualpositionBean.setFlag(rs.getString("FLAG"));
                actualpositionBean.setId(rs.getString("ID"));
                actualpositionBean.setManaging(rs.getString("MANAGING"));
                actualpositionBean.setPositionName(rs.getString("POSITION_NAME"));
                actualpositionBean.setPositionNumber(rs.getString("POSITION_NUMBER"));
                actualpositionBean.setUpdateDate(rs.getString("UPDATEDATE"));
                actualpositionBean.setUpdatedBy(rs.getString("UPDATEBY"));
                actualpositionBean.setFamilyJobCode(rs.getString("FAMILY_JOB_CODE"));
                //get hijri start date and end date
                if (rs.getString("END_DATE") != null) {
                    actualpositionBean.setEndDate(rh.convertToHijri(rs.getString("END_DATE")));
                } else {
                    actualpositionBean.setEndDate((rs.getString("END_DATE")));
                }
                if (rs.getString("STARTED_DATE") != null) {
                    actualpositionBean.setStartDate(rh.convertToHijri(rs.getString("STARTED_DATE")));
                } else {
                    actualpositionBean.setStartDate((rs.getString("STARTED_DATE")));
                } //end
                postionsList.add(actualpositionBean);

            }

        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            //   array.add(obj);
        } finally {
            closeResources(connection, ps, rs);
        }

        return postionsList;
    }

    public java.sql.Date getSQLDateFromString(String date) {
        java.sql.Date sqlDate = null;
        try {
            Date utilDate = new SimpleDateFormat("yyyy-MM-dd").parse(date);
            sqlDate = new java.sql.Date(utilDate.getTime());
        } catch (Exception ex) {
           ex.printStackTrace(); AppsproConnection.LOG.error("ERROR", ex);
        }
        return sqlDate;
    }

    public ArrayList<ActualPosition> searchPositionsByCode(String code) {
        ArrayList<ActualPosition> postionsList =
            new ArrayList<ActualPosition>();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "   select * from  " + " " + getSchema_Name() + ".XXX_ACTUAL_POSITION actual_position\n" +
                "                   where  actual_position.ID =  ? ";

            ps = connection.prepareStatement(query);
            ps.setString(1, code);
            rs = ps.executeQuery();
            while (rs.next()) {
                ActualPosition positionBean = new ActualPosition();
                positionBean.setCreatedBy(rs.getString("CREATEDBY"));
                positionBean.setCreatedDate(rs.getString("CREATEDDATE"));
                positionBean.setFlag(rs.getString("FLAG"));
                positionBean.setId(rs.getString("ID"));
                positionBean.setManaging(rs.getString("MANAGING"));
                positionBean.setPositionName(rs.getString("POSITION_NAME"));
                positionBean.setPositionNumber(rs.getString("POSITION_NUMBER"));
                positionBean.setStatus(rs.getString("STATUS"));
                positionBean.setFamilyJobCode(rs.getString("FAMILY_JOB_CODE"));
                positionBean.setUpdateDate(rs.getString("UPDATEDATE"));
                positionBean.setUpdatedBy(rs.getString("UPDATEBY"));
                positionBean.setFamilyJob(rs.getString("FAMILY_JOB"));

                //get hijri start date and end date
                if (rs.getString("END_DATE") != null) {
                    positionBean.setEndDate(rh.convertToHijri(rs.getString("END_DATE")));
                } else {
                    positionBean.setEndDate((rs.getString("END_DATE")));
                }
                if (rs.getString("STARTED_DATE") != null) {
                    positionBean.setStartDate(rh.convertToHijri(rs.getString("STARTED_DATE")));
                } else {
                    positionBean.setStartDate((rs.getString("STARTED_DATE")));
                } //end
                postionsList.add(positionBean);

            }

        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            //   array.add(obj);
        } finally {
            closeResources(connection, ps, rs);
        }

        return postionsList;
    }

    //Get  By Position Name for History Popup

    public ArrayList<ActualPosition> searchPositionsByName(String positionName) {
        ArrayList<ActualPosition> postionsList =
            new ArrayList<ActualPosition>();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                " select * from  " + " " + getSchema_Name() + ".XXX_ACTUAL_POSITION jobs \n" +
                "where   jobs.NAME = ? ";

            ps = connection.prepareStatement(query);
            ps.setString(1, positionName);
            rs = ps.executeQuery();
            while (rs.next()) {
                ActualPosition positionBean = new ActualPosition();
                positionBean.setCreatedBy(rs.getString("CREATEDBY"));
                positionBean.setCreatedDate(rs.getString("CREATEDDATE"));
                positionBean.setFlag(rs.getString("FLAG"));
                positionBean.setId(rs.getString("ID"));
                positionBean.setManaging(rs.getString("MANAGING"));
                positionBean.setPositionName(rs.getString("POSITION_NAME"));
                positionBean.setPositionNumber(rs.getString("POSITION_NUMBER"));
                positionBean.setStatus(rs.getString("STATUS"));
                positionBean.setFamilyJobCode(rs.getString("FAMILY_JOB_CODE"));
                positionBean.setUpdateDate(rs.getString("UPDATEDATE"));
                positionBean.setUpdatedBy(rs.getString("UPDATEBY"));
                positionBean.setFamilyJob(rs.getString("FAMILY_JOB"));
                //get hijri start date and end date
                if (rs.getString("END_DATE") != null) {
                    positionBean.setEndDate(rh.convertToHijri(rs.getString("END_DATE")));
                } else {
                    positionBean.setEndDate((rs.getString("END_DATE")));
                }
                if (rs.getString("STARTED_DATE") != null) {
                    positionBean.setStartDate(rh.convertToHijri(rs.getString("STARTED_DATE")));
                } else {
                    positionBean.setStartDate((rs.getString("STARTED_DATE")));
                } //end
                postionsList.add(positionBean);

            }

            rs.close();
            ps.close();
            connection.close();
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            //   array.add(obj);
        } finally {
            closeResources(connection, ps, rs);
        }

        return postionsList;
    } //endmoh

    public ActualPosition updateCahngeInsert(ActualPosition bean) {
        try {
            //bean.setEndDate(getFutureEndData(bean));
            connection = AppsproConnection.getConnection();
            if (!bean.getEndDate().isEmpty()) {
                bean.setEndDate((bean.getEndDate()));
            }
            if (!bean.getStartDate().isEmpty()) {
                bean.setStartDate(rh.convertToGregorian(bean.getStartDate()));
            }
            String oldFlag = "N";
            String query = "UPDATE XX_MOE.XXX_ACTUAL_POSITION \n" +
                "SET  UPDATEDATE =sysdate \n" +
                ",END_DATE = ? " + ",FLAG = ?" + ", UPDATEBY = ? " +
                "WHERE id = ? ";
            ps = connection.prepareStatement(query);
            ps.setDate(1, getSQLDateFromString(bean.getStartDate()));
            ps.setString(2, oldFlag);
            ps.setString(3, bean.getUpdatedBy());
            ps.setInt(4, Integer.parseInt(bean.getId()));
            ps.executeUpdate();

            String queryInsert =
                "INSERT INTO  " + " " + getSchema_Name() + ".XXX_ACTUAL_POSITION (STARTED_DATE,END_DATE,FAMILY_JOB_CODE,FAMILY_JOB,POSITION_NUMBER ,MANAGING,POSITION_NAME,CREATEDBY,CREATEDDATE, FLAG,STATUS)\n" +
                "                    VALUES ( ?, ? ,?,? ,?,?,?,?,?,?,? )";

            ps = connection.prepareStatement(queryInsert);

            ps.setDate(1, getSQLDateFromString(bean.getStartDate()));
            ps.setDate(2, getSQLDateFromString(bean.getEndDate()));
            ps.setString(3, bean.getFamilyJobCode());
            ps.setString(4, bean.getFamilyJob());
            ps.setString(5, bean.getPositionNumber());
            ps.setString(6, bean.getManaging());
            ps.setString(7, bean.getPositionName());
            ps.setString(8, bean.getCreatedBy());
            ps.setDate(9, getSQLDateFromString(bean.getCreatedDate()));
            ps.setString(10, bean.getFlag());
            ps.setString(11, bean.getStatus());

            ps.executeUpdate();

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return bean;
    }

    public ActualPosition updateOverride(ActualPosition bean) {
        try {
            connection = AppsproConnection.getConnection();
            if (!bean.getEndDate().isEmpty()) {
                bean.setEndDate(rh.convertToGregorian(bean.getEndDate()));
            }
            if (!bean.getStartDate().isEmpty()) {
                bean.setStartDate(rh.convertToGregorian(bean.getStartDate()));
            }

            String query =
                "INSERT INTO  " + " " + getSchema_Name() + ".XXX_ACTUAL_POSITION (STARTED_DATE,END_DATE,FAMILY_JOB_CODE,FAMILY_JOB,POSITION_NUMBER ,MANAGING,POSITION_NAME,CREATEDBY,CREATEDDATE, FLAG,STATUS)\n" +
                "                    VALUES ( ?, ? ,?,? ,?,?,?,?,?,?,? )";

            ps = connection.prepareStatement(query);

            ps.setDate(1, getSQLDateFromString(bean.getStartDate()));
            ps.setDate(2, getSQLDateFromString(bean.getEndDate()));
            ps.setString(3, bean.getFamilyJobCode());
            ps.setString(4, bean.getFamilyJob());
            ps.setString(5, bean.getPositionNumber());
            ps.setString(6, bean.getManaging());
            ps.setString(7, bean.getPositionName());
            ps.setString(8, bean.getCreatedBy());
            ps.setDate(9, getSQLDateFromString(bean.getCreatedDate()));
            ps.setString(10, "Y");
            ps.setString(11, bean.getStatus());

            ps.executeUpdate();

            query =
                    "delete from XX_MOE.XXX_ACTUAL_POSITION where STARTED_DATE > ?";
            ps = connection.prepareStatement(query);
            ps.setDate(1, getSQLDateFromString(bean.getStartDate()));
            ps.executeUpdate();

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return bean;
    }

    public String getFutureEndData(ActualPosition bean) {
        String effEndDate = "";
        try {

            connection = AppsproConnection.getConnection();
            String query =
                "select  STARTED_DATE from  " + " " + getSchema_Name() +
                ".XXX_ACTUAL_POSITION POSITION where FAMILY_JOB_CODE = ? AND POSITION.ID = (select MIN(ID) FROM XX_MOE.XXX_ACTUAL_POSITION where FAMILY_JOB_CODE = ? And STARTED_DATE > ?)";

            ps = connection.prepareStatement(query);
            ps.setString(1, bean.getFamilyJobCode());
            ps.setString(2, bean.getFamilyJobCode());
            ps.setDate(3, getSQLDateFromString(bean.getStartDate()));
            rs = ps.executeQuery();
            while (rs.next()) {
                effEndDate = rs.getString("STARTED_DATE");
                String c = rs.getString(1);
            }

            rs.close();
            ps.close();
            connection.close();
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            //   array.add(obj);
        } finally {
            closeResources(connection, ps, rs);
        }
        return effEndDate;
    }
}
