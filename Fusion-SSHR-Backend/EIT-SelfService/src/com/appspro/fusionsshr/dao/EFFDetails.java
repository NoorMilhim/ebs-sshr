/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.dao;


import com.appspro.db.AppsproConnection;
import com.appspro.db.CommonConfigReader;
import com.appspro.fusionsshr.bean.OrgnizationBean;
import com.appspro.fusionsshr.bean.PeopleExtraInformationBean;
import com.appspro.fusionsshr.bean.TrackServerRequestBean;
import com.appspro.fusionsshr.bean.trackRequestBean;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.sun.codemodel.JOp;

import common.restHelper.RestHelper;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import java.net.HttpURLConnection;
import java.net.URL;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;

import java.util.Date;

import java.util.ArrayList;

import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import javax.swing.JOptionPane;

import org.joda.time.LocalDate;

import org.json.JSONArray;
import org.json.JSONObject;


public class EFFDetails extends RestHelper {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    private static EFFDetails instance = null;

    public static EFFDetails getInstance() {
        if (instance == null)
            instance = new EFFDetails();
        return instance;
    }

    AppsproConnection appsObg = new AppsproConnection();
    final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

    public JSONArray getEFFDetails(String Url) {
        JSONArray EFFLinks = new JSONArray();
        String finalresponse = "";
        //        String jwttoken = jwt.trim();
        OrgnizationBean org = new OrgnizationBean();

        HttpsURLConnection https = null;
        HttpURLConnection connection = null;

        String serverUrl = Url;

        String jsonResponse = "";
        try {
            URL url =
                new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection)url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
            } else {
                connection = (HttpURLConnection)url.openConnection();
            }
            String SOAPAction = getInstanceUrl();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type",
                                          "application/json; charset=UTF-8");
            connection.setRequestProperty("Accept", "application/json");
            connection.setConnectTimeout(6000000);
            connection.setRequestProperty("SOAPAction", SOAPAction);
            //connection.setRequestProperty("Authorization","Bearer " + jwttoken);
            connection.setRequestProperty("Authorization",
                                          "Basic " + getAuth());

            BufferedReader in =
                new BufferedReader(new InputStreamReader(connection.getInputStream(),
                                                         "UTF-8"));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            JSONObject obj = new JSONObject(response.toString());
            // JSONArray arr = obj.getJSONArray("items");
            // //("Items Array "+obj.isNull("items"));
            if (!obj.isNull("items")) {
                // //("Zero "+ obj.isNull("items"));
                // //("length "+obj.getJSONArray("items").length());
                if (obj.getJSONArray("items").length() != 0) {
                    EFFLinks = obj.getJSONArray("items");
                }

            }

        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }

        if (finalresponse.length() > 1) {
        } else {
            finalresponse = jsonResponse;
        }

        return EFFLinks;
    }


    public void postEFF(PeopleExtraInformationBean peiBean) throws Exception {
       TrackServerRequestBean bean = new TrackServerRequestBean();
       bean.setEitCode(peiBean.getCode());
       bean.setProjectName(CommonConfigReader.getValue("projectName"));
       bean.setEitName(peiBean.getEit_name());
       bean.setEitResponseDate(TrackServerRequestBean.getCurrentDateTime());
       try {
           String model = peiBean.getEit();
           JSONObject tempObj;
          
           try{
               tempObj = new JSONObject(model);
           }catch(Exception e){
               ObjectMapper mapper = new ObjectMapper();
               JsonNode jsonNode = mapper.readTree(model);
               tempObj = new JSONObject(jsonNode.asText());
           }
           model = tempObj.toString();
           String Url = peiBean.getUrl();
           String url = peiBean.getUrl();
           URL obj =
               new URL(null, url, new sun.net.www.protocol.https.Handler());
           HttpsURLConnection con = (HttpsURLConnection)obj.openConnection();
           con.setDoInput(true);
           con.setDoOutput(true);
           con.setRequestMethod("POST");
           con.setRequestProperty("Content-Type",
                                  "application/json; charset=UTF-8");
           con.setRequestProperty("Accept", "application/json");
           con.setConnectTimeout(6000000);
           con.setRequestProperty("Authorization", "Basic " + getAuth());
           String urlParameters = new String(model.getBytes());
           bean.setUrl(url);
           bean.setPayload(model);

           OutputStream wr = con.getOutputStream();
           wr.write(model.getBytes("UTF-8"));
           //wr.flush();
           int responseCode = con.getResponseCode();
           InputStream is;
           if (responseCode >= 400) {
               is = con.getErrorStream();
           } else {
               is = con.getInputStream();
           }
           wr.close();
           String msg = "";
           if (is != null) {
               BufferedReader in =
                   new BufferedReader(new InputStreamReader(is));
               StringBuffer response = new StringBuffer();
               while ((msg = in.readLine()) != null) {
                   response.append(msg);
               }
               msg = "=>" + msg;
               in.close();
           }

           
           AppsproConnection.LOG.info(urlParameters);
           String responseDescription = con.getResponseMessage() + msg;
           AppsproConnection.LOG.info("Response Code : " + responseCode);
           AppsproConnection.LOG.info("responseDescription : " + responseDescription);
           //            int requestId = getRequestSequence();
           bean.setEitResponseCode(responseCode + "");
           bean.setEitResponseDesc(responseDescription);
       } catch (Exception e) {
           //("Error: ");
          e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
           bean.setEitResponseCode(500 + "");
           bean.setEitResponseDesc(e.getMessage());
       }
       bean.setRequestId(peiBean.getId() + ""); //todo
       
       if(peiBean.getPEI_ATTRIBUTE1() == "UPDATE")//bean.setPEI_ATTRIBUTE1("UPDATE")
       {
           //code of farouk
            //update
            updateResponseDetails(bean);
       }else{
            InsertResponseDetails(bean);
       }
   }


  


    public void editEFF(String model, String Url) throws Exception {
        String url = Url;
        URL obj = new URL(url);
        HttpsURLConnection con = (HttpsURLConnection)obj.openConnection();
        con.setRequestProperty("X-HTTP-Method-Override", "PATCH");
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json;");
        con.setRequestProperty("Accept", "application/json");
        con.setConnectTimeout(6000000);
        con.setRequestProperty("Authorization", "Basic " + getAuth());

        String urlParameters = model;

        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(model);
        wr.flush();
        wr.close();
        int responseCode = con.getResponseCode();
        //("\nSending 'POST' request to URL : " + url);
        //("Post parameters : " + urlParameters);
        //("Response Code : " + responseCode);
    }

    public String postEFF_BulkTransaction(String model,
                                          String Url) throws Exception {
        String result = "";
        try {
            String url = Url;

            URL obj =
                new URL(null, url, new sun.net.www.protocol.https.Handler());
            HttpsURLConnection con = (HttpsURLConnection)obj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json;");
            con.setRequestProperty("Accept", "application/json");
            con.setConnectTimeout(6000000);
            con.setRequestProperty("Authorization", "Basic " + getAuth());

            String urlParameters = model;

            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(model);
            wr.flush();
            wr.close();
            int responseCode = con.getResponseCode();
            //("\nSending 'POST' request to URL : " + url);
            //("Post parameters : " + urlParameters);
            //("Response Code : " + responseCode);
            if (responseCode == 201) {
                result = "APPROVED";
            } else {
                result = "ERROR";
            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }

        return result;
    }

    public List<TrackServerRequestBean> getLog() {
        List<TrackServerRequestBean> lst = new ArrayList();
        try {
            connection = AppsproConnection.getConnection();
            ps =
 connection.prepareStatement("select * from XXX_TRACK_SERVER_REQUEST ORDER BY ID DESC");
            rs = ps.executeQuery();
            while (rs.next()) {
                TrackServerRequestBean bean = new TrackServerRequestBean();
                bean.setId(rs.getLong("ID"));
                bean.setProjectName(rs.getString("PROJECTNAME"));
                bean.setEitName(rs.getString("EITNAME"));
                bean.setEitCode(rs.getString("EITCODE"));
                bean.setEitResponseCode(rs.getString("EIT_RESPONSE_CODE"));
                bean.setEitResponseDesc(rs.getString("EIT_RESPONSE_DESC"));
                bean.setEitResponseDate(rs.getString("EIT_RESPONSE_DATE"));
                bean.setElementResponseCode(rs.getString("ELEMNT_RESPONSE_CODE"));
                bean.setElementResponseDesc(rs.getString("ELEMNT_RESPONSE_DESC"));
                bean.setElementResponseDate(rs.getString("ELEMNT_RESPONSE_DATE"));
                bean.setRequestId(rs.getString("REQUESTID"));
                bean.setUrl(rs.getString("URL"));
                bean.setPayload(rs.getString("PAYLOAD"));

                lst.add(bean);
            }

        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            appsObg.closeResources(connection, ps, rs);
        }
        return lst;
    }


    public void updateResponseDetails(TrackServerRequestBean bean) {
           if(bean.getEitResponseDesc() != null && bean.getEitResponseDesc().length() > 50)
               bean.setEitResponseDesc(bean.getEitResponseDesc().substring(0, 49));
           try {
               connection = AppsproConnection.getConnection();
               String query =
                   "UPDATE XXX_TRACK_SERVER_REQUEST SET EIT_RESPONSE_CODE=?,EIT_RESPONSE_DESC=? WHERE REQUESTID=?";
               ps = connection.prepareStatement(query);
               ps.setString(1, bean.getEitResponseCode());
               ps.setString(2, bean.getEitResponseDesc());   
               ps.setString(3, bean.getRequestId());
               ps.executeUpdate();
           } catch (Exception e) {
              e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
           } finally {
               appsObg.closeResources(connection, ps, rs);
           }
       }

//eit
    public void InsertResponseDetails(TrackServerRequestBean bean) {
           if(bean.getEitResponseDesc() != null && bean.getEitResponseDesc().length() > 50)
               bean.setEitResponseDesc(bean.getEitResponseDesc().substring(0, 49));
           try {
               connection = AppsproConnection.getConnection();
               String query =
                   "insert into XXX_TRACK_SERVER_REQUEST (PROJECTNAME,EITCODE,EITNAME,EIT_RESPONSE_CODE,EIT_RESPONSE_DESC,EIT_RESPONSE_DATE,ELEMNT_RESPONSE_CODE,ELEMNT_RESPONSE_DESC,ELEMNT_RESPONSE_DATE,URL,PAYLOAD,REQUESTID)\n" +
                   "VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
               ps = connection.prepareStatement(query);
               ps.setString(1, bean.getProjectName());
               ps.setString(2, bean.getEitCode());
               ps.setString(3, bean.getEitName());
               ps.setString(4, bean.getEitResponseCode());
               ps.setString(5, bean.getEitResponseDesc());
               ps.setString(6, bean.getEitResponseDate());
               ps.setString(7, bean.getElementResponseCode());
               ps.setString(8, bean.getElementResponseDesc());
               ps.setString(9, bean.getElementResponseDate());
               ps.setString(10, bean.getUrl());
               ps.setString(11, bean.getPayload());
               ps.setString(12, bean.getRequestId());
               ps.executeUpdate();
           } catch (Exception e) {
              e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
           } finally {
               appsObg.closeResources(connection, ps, rs);
           }
       }


//elemnt entery
    public void editElementResponseDetails(TrackServerRequestBean bean) {
          try {
              if(bean.getElementResponseDesc() != null && bean.getElementResponseDesc().length() > 50)
                  bean.setElementResponseDesc(bean.getElementResponseDesc().substring(0, 49));
              connection = AppsproConnection.getConnection();
              String query =
                  "UPDATE XXX_TRACK_SERVER_REQUEST SET ELEMNT_RESPONSE_CODE = ?, ELEMNT_RESPONSE_DESC = ?, ELEMNT_RESPONSE_DATE = ? WHERE REQUESTID = ? ";
              ps = connection.prepareStatement(query);
              ps.setString(1, bean.getElementResponseCode());
              ps.setString(2, bean.getElementResponseDesc());
              ps.setString(3, bean.getElementResponseDate());
              ps.setString(4, bean.getRequestId());

              int s = ps.executeUpdate();

          } catch (Exception e) {
             e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
          } finally {
              appsObg.closeResources(connection, ps, rs);
          }
      }

    public trackRequestBean getEITNameandEITCode(String restName) {
        trackRequestBean beanOBJ = new trackRequestBean();
        try {
            //   connection = AppsproConnection.getConnection();
            String query =
                "select EITNAME ,EITCODE FROM XXX_DYNAMIC_URL WHERE RESTNAME=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, restName);
            rs = ps.executeQuery();
            while (rs.next()) {

                beanOBJ.setEitCode(rs.getString("EITCODE"));
                beanOBJ.setEitName(rs.getString("EITNAME"));

            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            appsObg.closeResources(connection, ps, rs);
        }


        return beanOBJ;
    }

    public int getRequestSequence() {
        int responseSequence = 0;
        try {

            connection = AppsproConnection.getConnection();
            String query = "SELECT REQUESTIDSEQ.NEXTVAL  from dual";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            if (rs.next()) {
                responseSequence = rs.getInt(1);
            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            appsObg.closeResources(connection, ps, rs);
        }


        return responseSequence;
    }

    public void callExemptionWS(String model, int id) throws IOException {


        //            PeopleExtraInformationDAO dd = new PeopleExtraInformationDAO();
        //            PeopleExtraInformationBean pp= dd.getEITById(Integer.toString(id));

        JSONObject modelJson = new JSONObject(model);

        String startDate = modelJson.get("requestStartDate").toString();
        String endDate = modelJson.get("requestEndDate").toString();
        
        //call ws
        String data =
            "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:rest=\"http://rest.appspro.com/\">\n" +
            "   <soapenv:Header/>\n" +
            "   <soapenv:Body>\n" +
            "      <rest:ProcessEmployeeExemption>\n" +
            "         <!--Optional:-->\n" +
            "         <arg0>\n" +
            "            <exemptionStartDate>" + startDate +
            "</exemptionStartDate>\n" +
            "            <exemptionEndDate>" + endDate +
            "</exemptionEndDate>\n" +
            "            <personNumber>" + id + "</personNumber>\n" +
            //            "            <personNumber>"+pp.getPerson_number()+"</personNumber>\n" +
            "         </arg0>\n" +
            "      </rest:ProcessEmployeeExemption>\n" +
            "   </soapenv:Body>\n" +
            "</soapenv:Envelope>";


        //System.setProperty("DUseSunHttpHandler", "true");
        byte[] buffer = new byte[data.length()];
        buffer = data.getBytes();
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        bout.write(buffer);
        byte[] b = bout.toByteArray();
        java.net.URL ExeUrl =
            new URL(null, "https://hhajcs-a562419.java.em2.oraclecloudapps.com/HHA-OTL/EmployeeTimeAnalysisService",
                    new sun.net.www.protocol.https.Handler());
        java.net.HttpURLConnection http;
        if (ExeUrl.getProtocol().toLowerCase().equals("https")) {
            trustAllHosts();
            java.net.HttpURLConnection https =
                (HttpsURLConnection)ExeUrl.openConnection();
            //            System.setProperty("DUseSunHttpHandler", "true");
            //https.setHostnameVerifier(DO_NOT_VERIFY);
            http = https;
        } else {
            http = (HttpURLConnection)ExeUrl.openConnection();
        }
        String SOAPAction = "";
        //            http.setRequestProperty("Content-Length", String.valueOf(b.length));
        http.setRequestProperty("Content-Length", String.valueOf(b.length));
        http.setRequestProperty("Content-Type", "text/xml");
        http.setRequestProperty("SOAPAction", SOAPAction);
        //                http.setRequestProperty("Authorization", "Basic " + getAuth());
        http.setRequestMethod("POST");
        http.setDoOutput(true);
        http.setDoInput(true);
        OutputStream out = http.getOutputStream();
        out.write(b);
    }


    public static void main(String[] args) {
//        TrackServerRequestBean bean = new TrackServerRequestBean();
//        getInstance().InsertResponseDetails(bean);
        
        
        
        
        TrackServerRequestBean b = new TrackServerRequestBean();
        b.setRequestId("3500");
        b.setElementResponseCode((true ? 200 : 500) + "");
        b.setElementResponseDesc(true ? "Created":"Internal Server Error");
        b.setElementResponseDate(TrackServerRequestBean.getCurrentDateTime());
        EFFDetails.getInstance().editElementResponseDetails(b);
    }
}
