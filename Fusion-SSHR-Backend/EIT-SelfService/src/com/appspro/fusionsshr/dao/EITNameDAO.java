package com.appspro.fusionsshr.dao;
import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.EitName;
import com.appspro.fusionsshr.bean.rest.Assignment;
import com.appspro.fusionsshr.bean.rest.Example;
import com.appspro.fusionsshr.bean.rest.Link;
import com.appspro.fusionsshr.bean.rest.Link_;
import com.appspro.fusionsshr.bean.rest.Photo;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.w3c.dom.NodeList; 
import org.w3c.dom.Node; 
import common.restHelper.RestHelper;

import java.awt.image.BufferedImage;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import java.net.HttpURLConnection;
import java.net.URL;

import java.security.NoSuchAlgorithmException;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Date;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class EITNameDAO {
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    public EITNameDAO() {
        super();
    }
    
    public ArrayList <EitName> eitName() {
        String output = "";
        
        ArrayList<EitName> list = new ArrayList<EitName>();
        EitName eitName = new EitName();
        try {
            AppsproConnection.LOG.info("Invoke service using direct HTTP call with Basic Auth");                    
         String soapRest =   "<soapenv:Envelope xmlns:get=\"http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxrasf_intg_pkg/get_eit_name/\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xxr=\"http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxrasf_intg_pkg/\">\n" + 
            "\n" + 
            "   <soapenv:Header>\n" + 
            "\n" + 
            "      <wsse:Security soapenv:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">\n" + 
            "\n" + 
            "         <wsse:UsernameToken wsu:Id=\"UsernameToken-8EA71DD8F45623F862158499151620314\">\n" + 
            "\n" + 
            "            <wsse:Username>sysadmin</wsse:Username>\n" + 
            "\n" + 
            "            <wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">sysadmin</wsse:Password>\n" + 
            "\n" + 
            "            <wsse:Nonce EncodingType=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary\">ukzBnd1XKAz56TfD1RJocw==</wsse:Nonce>\n" + 
            "\n" + 
            "            <wsu:Created>2020-03-23T19:25:16.203Z</wsu:Created>\n" + 
            "\n" + 
            "         </wsse:UsernameToken>\n" + 
            "\n" + 
            "      </wsse:Security>\n" + 
            "\n" + 
            "      <xxr:SOAHeader></xxr:SOAHeader>\n" + 
            "\n" + 
            "   </soapenv:Header>\n" + 
            "\n" + 
            "   <soapenv:Body>\n" + 
            "\n" + 
            "      <get:InputParameters>\n" + 
            "\n" + 
            "         <!--Optional:-->\n" + 
            "\n" + 
            "         <get:P_LANG>AR</get:P_LANG>\n" + 
            "\n" + 
            "      </get:InputParameters>\n" + 
            "\n" + 
            "   </soapenv:Body>\n" + 
            "\n" + 
            "</soapenv:Envelope>";
            Document doc =
                new RestHelper().httpPost("http://hcmpub.appspro-me.com:8075/webservices/SOAProvider/plsql/xxrasf_intg_pkg/", soapRest);

            doc.getDocumentElement().normalize();
            System.out.println("-----------------" );
            System.out.println( doc.toString());
            System.out.println("-----------------" );
            String data =
                doc.getElementsByTagName("env:Body").item(0).getTextContent();
            if (false) {
    //                output = StringUtils.newStringUtf8(Base64.decodeBase64(data));
            } else {
                output = data;
                System.out.println(output);
                //doc.getElementsByTagName("env:Body").item(0).getTextContent();
                System.out.println(doc.getElementsByTagName("P_NAME_EIT").item(0).getAttributes());
//                System.out.println(doc.getElementsByTagName("P_NAME_EIT").item(0).getChildNodes());
                NodeList  nodeList = doc.getElementsByTagName("P_NAME_EIT_ITEM");  
                System.out.println(nodeList.getLength());
                
                for (int itr = 0; itr < nodeList.getLength(); itr++)   
                {  
                    Node node = nodeList.item(itr); 
                    Element eElement = (Element) node;  
//                    String rb = eElement.getElementsByTagName("DESCRIPTIVE_FLEX_CONTEXT_NAME").item(0).getTextContent();
   
                    eitName.setDESCRIPTIVE_FLEX_CONTEXT_CODE(eElement.getElementsByTagName("DESCRIPTIVE_FLEX_CONTEXT_CODE").item(0).getTextContent());
                    eitName.setDESCRIPTIVE_FLEX_CONTEXT_NAME(eElement.getElementsByTagName("DESCRIPTIVE_FLEX_CONTEXT_NAME").item(0).getTextContent());
//                    rb.getString("").getBytes(), "UTF-8");
//                    rb.ge
//                    new String(eElement.getElementsByTagName("DESCRIPTIVE_FLEX_CONTEXT_NAME"), "UTF-8")
                    list.add(eitName);
                }              
            }

            return list;
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }

        return list;
    }
}
