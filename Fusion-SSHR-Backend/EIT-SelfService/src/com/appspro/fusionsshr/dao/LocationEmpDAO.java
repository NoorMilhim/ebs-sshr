package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.ApprovalConditionBean;
import com.appspro.fusionsshr.bean.ApprovalListBean;
import com.appspro.fusionsshr.bean.AttachmentBean;
import com.appspro.fusionsshr.bean.LocationEmpBean;

import com.bea.wls.ejbgen.Bean;

import common.restHelper.RestHelper;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static common.restHelper.RestHelper.getSchema_Name;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class LocationEmpDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();


    public ArrayList<LocationEmpBean> getData() {
        ArrayList<LocationEmpBean> LocationEmpBeanList =
            new ArrayList<LocationEmpBean>();
        LocationEmpBean bean = null;

        connection = AppsproConnection.getConnection();

        String query = null;
        query =
                "SELECT * FROM  " + " " + getSchema_Name() + ".xx_Location_Employee";

        try {
            ps = connection.prepareStatement(query);

            rs = ps.executeQuery();

            while (rs.next()) {
                bean = new LocationEmpBean();
                bean.setType(rs.getString("type"));
                bean.setDatee(rs.getString("datee"));
                bean.setPersonId(rs.getString("person_Id"));
                bean.setPersonNumber(rs.getString("person_number"));
                bean.setLocationName(rs.getString("location_name"));
                LocationEmpBeanList.add(bean);
            }


        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return LocationEmpBeanList;

    }

    public LocationEmpBean insertlocationEmpList(LocationEmpBean bean) {
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "insert into  " + " " + getSchema_Name() + ".xx_location_employee (type,datee,person_id,person_number,location_name)" +
                    " VALUES(?,?,?,?,?)";
            ps = connection.prepareStatement(query);
            ps.setString(1, bean.getType());
            ps.setString(2, bean.getDatee());
            ps.setString(3, bean.getPersonId());
            ps.setString(4, bean.getPersonNumber());
            ps.setString(5, bean.getLocationName());

            ps.executeUpdate();

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return bean;
    }


    public LocationEmpBean updateLocationEmp(LocationEmpBean LocationEmp) {

        try {
            connection = AppsproConnection.getConnection();

            String query =
                "update  " + " " + getSchema_Name() + ".xx_location_employee set type=? ,datee=?,person_number=?,location_name where person_id=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, LocationEmp.getType());
            ps.setString(2, LocationEmp.getDatee());
            ps.setString(3, LocationEmp.getPersonNumber());
            ps.setString(4, LocationEmp.getLocationName());
            ps.setString(5, LocationEmp.getPersonId());

            ps.executeUpdate();


        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return LocationEmp;
    }


    public String deleteLocationEmp(String body) {

        connection = AppsproConnection.getConnection();
        String query;
        int checkResult = 0;

        try {
            query =
                    "DELETE FROM " + " " + getSchema_Name() + ".xx_location_employee WHERE person_id=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, body);
            checkResult = ps.executeUpdate();

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return Integer.toString(checkResult);
    }
    
    
    
    
    public LocationEmpBean insertEmpList(LocationEmpBean bean) {
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "insert into  " + " " + getSchema_Name() + ".xx_location_employee (type,datee,person_id,person_number,location_name)" +
                    " VALUES(?,?,?,?,?)";
            ps = connection.prepareStatement(query);
            ps.setString(1, bean.getType());
            ps.setString(2, bean.getDatee());
            ps.setString(3, bean.getPersonId());
            ps.setString(4, bean.getPersonNumber());
            ps.setString(5, bean.getLocationName());

            ps.executeUpdate();

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return bean;
    }


}


