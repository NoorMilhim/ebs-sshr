package com.appspro.fusionsshr.dao;


import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.BulkMenuBean;
import com.appspro.fusionsshr.bean.MailTemplateBean;

import com.appspro.fusionsshr.rest.MailTemplateRest;

import static common.restHelper.RestHelper.getSchema_Name;

import java.sql.Connection;

import java.sql.PreparedStatement;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import java.sql.Array;

import org.json.JSONArray;

import static common.restHelper.RestHelper.getSchema_Name;

public class BulkDao extends AppsproConnection {

    private static BulkDao instance = null;
    private static Connection connection;
    private static PreparedStatement ps;
    private static ResultSet rs;


    public List<String> getAll() {
        List<String> list = new ArrayList();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "select * from  " + " " + getSchema_Name() + ".BULK_MENU";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
//                BulkMenuBean bean = new BulkMenuBean();
//                bean.setID(rs.getInt("ID"));
//                bean.setCODE(rs.getString("CODE"));
//                bean.setTITLE_AR(rs.getString(3));
//                bean.setTITLE_EN(rs.getString(4));
//                bean.setCOLOR(rs.getString(5));
//                bean.setICON(rs.getString(6));
//                bean.setVISIBLE(rs.getInt(7));
                list.add(rs.getString("CODE"));
            }
        } catch (SQLException e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return list;
    }

    public static BulkDao getInstance() {
        if (instance == null)
            instance = new BulkDao();
        return instance;
    }

    public JSONObject delete(String  codes) {
        JSONObject response = new JSONObject();
        response.put("status", "Done");
        try {
            connection = AppsproConnection.getConnection();
                                   
            String query =
                "DELETE from " + getSchema_Name() + ".BULK_MENU where CODE in ('"+codes.replace(",", "','")+"')";
            ps = connection.prepareStatement(query);
            int res = ps.executeUpdate();
            if(res ==0) {
                response.put("status", "Error");
                response.put("message", "can't delete");
            }
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            response.put("status", "Error");
            response.put("message", e.getMessage());
        } finally {
            closeResources(connection, ps);
        }
        return response;
    }

    public JSONObject add(JSONArray codes) {
        JSONObject response = new JSONObject();
        response.put("status", "Done");
      
            for(int x=0;x<codes.length();x++){
                try {
                connection = AppsproConnection.getConnection();
                String query =
                    "INSERT INTO " + getSchema_Name() + ".BULK_MENU (CODE) VALUES (?)";
                ps = connection.prepareStatement(query);
                ps.setString(1, codes.get(x).toString());

                int res = ps.executeUpdate();
                 if(res != 1) {
                 }
                } catch (Exception e) {
                e.printStackTrace();
                response.put("status", "Error");
                response.put("message", e.getMessage());
                } finally {
                closeResources(connection, ps);
                }
            }
           
        return response;
    }
    
     public static void main(String[] args) {
        String names="ahmed,hussein,ali";
        names="('"+names.replace(",", "','")+"')";
        System.out.println(names);
    }
}
