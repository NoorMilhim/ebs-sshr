package com.appspro.fusionsshr.dao;


import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.MailTemplateBean;

import com.appspro.fusionsshr.rest.MailTemplateRest;

import java.sql.Connection;

import java.sql.PreparedStatement;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import static common.restHelper.RestHelper.getSchema_Name;

public class MailTemplateDao extends AppsproConnection {

    private static MailTemplateDao instance = null;
    private static Connection connection;
    private static PreparedStatement ps;
    private static ResultSet rs;


    public List<MailTemplateBean> getAll() {
        List<MailTemplateBean> list = new ArrayList();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "select * from  " + " " + getSchema_Name() + ".XXX_EMAIL_TEMPLATE";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                MailTemplateBean bean = new MailTemplateBean();
                bean.setId(rs.getInt(1));
                bean.setTemplateName(rs.getString(2));
                bean.setTemplateLanguage(rs.getString(3));
                bean.setTemplateRequestName(rs.getString(4));
                bean.setTemplateEmailSubject(rs.getString(5));
                bean.setTemplateStatus(rs.getString(6));
                list.add(bean);
            }
        } catch (SQLException e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return list;
    }

    public List<MailTemplateBean> search(String id, String name,
                                         String language) {
        List<MailTemplateBean> list = new ArrayList();
        try {
            connection = AppsproConnection.getConnection();
            String cond = "";
            //and TEMPLATE_ID = ? and TEMPLATE_REQUEST_NAME like %?% and TEMPLATE_LANGUAGE like ?
            try {
                if (id != null && id != "undefined" && id.length() > 0 &&
                    Integer.parseInt(id) > 0)
                    cond += " and TEMPLATE_ID = " + id;
            } catch (Exception e) {

            }

            if (name != null && !name.equalsIgnoreCase("null") &&
                !name.equalsIgnoreCase("undefined") && name.length() > 0)
                cond += " and TEMPLATE_REQUEST_NAME like \'%" + name + "%\' ";

            if (language != null && !language.equalsIgnoreCase("null") &&  !language.equalsIgnoreCase("both") &&
                !language.equalsIgnoreCase("undefined") &&
                language.length() > 0)
                cond += " and TEMPLATE_LANGUAGE like \'%" + language + "%\' ";

            String query =
                "select * from  " + " " + getSchema_Name() + ".XXX_EMAIL_TEMPLATE where 1=1 " +
                cond;


            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                MailTemplateBean bean = new MailTemplateBean();
                bean.setId(rs.getInt(1));
                bean.setTemplateName(rs.getString(2));
                bean.setTemplateLanguage(rs.getString(3));
                bean.setTemplateRequestName(rs.getString(4));
                bean.setTemplateEmailSubject(rs.getString(5));
                bean.setTemplateStatus(rs.getString(6));
                list.add(bean);
            }
        } catch (SQLException e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return list;
    }

    public MailTemplateBean getById(String id) {
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "select * from  " + " " + getSchema_Name() + ".XXX_EMAIL_TEMPLATE WHERE TEMPLATE_ID = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                MailTemplateBean bean = new MailTemplateBean();
                bean.setId(rs.getInt(1));
                bean.setTemplateName(rs.getString(2));
                bean.setTemplateLanguage(rs.getString(3));
                bean.setTemplateRequestName(rs.getString(4));
                bean.setTemplateEmailSubject(rs.getString(5));
                bean.setTemplateStatus(rs.getString(6));
                return bean;
            }
        } catch (SQLException e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return null;
    }


    public static MailTemplateDao getInstance() {
        if (instance == null)
            instance = new MailTemplateDao();
        return instance;
    }

    public boolean deleteById(String id) {
        try{
            connection = AppsproConnection.getConnection();
            String query = "DELETE  from  " + getSchema_Name() + ".XXX_EMAIL_TEMPLATE where TEMPLATE_ID = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, id);
            
            int res = ps.executeUpdate();
            return res == 1;
        }catch(Exception e){
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            }finally{
                closeResources(connection, ps); 
            }
        return false;
    }
}
