/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.dao;


import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.ApprovalListBean;
import com.appspro.fusionsshr.bean.EmployeeBean;
import com.appspro.fusionsshr.bean.NotificationBean;
import com.appspro.fusionsshr.bean.PeopleExtraInformationBean;
import com.appspro.fusionsshr.bean.WorkFlowNotificationBean;
import com.appspro.mail.CredentialStoreClassMail;

import com.hha.appsproExceptionn.AppsProException;

import common.biPReports.BIReportModel;

import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;


/**
 *
 * @author user
 */
public class WorkflowNotificationDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    EmployeeDetails employeeDetails = new EmployeeDetails();
    CredentialStoreClassMail mailObj = new  CredentialStoreClassMail();

    EmployeeBean emp = null;

    public WorkFlowNotificationBean insertIntoWorkflow(WorkFlowNotificationBean bean) {
        try {
            //("Work Flow Notification: ");
            //(new JSONObject (bean));
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    " insert into  " + " " + getSchema_Name() + ".XX_WORKFLOW_NOTIFICATION (MSG_TITLE,MSG_BODY,CREATION_DATE,RECEIVER_TYPE,RECEIVER_ID,RESPONSE_PERSON_ID,TYPE,RESPONSE_DATE,WORKFLOW_ID,REQUEST_ID,STATUS,SELF_TYPE,PERSON_NAME,NATIONALIDENTITY) " +
                    "VALUES(?,?,SYSDATE,?,?,?,?,?,?,?,?,?,?,?) ";
            ps = connection.prepareStatement(query);
            ps.setString(1, bean.getMsgTitle());
            ps.setString(2, bean.getMsgBody());
            ps.setString(3, bean.getReceiverType());
            ps.setString(4, bean.getReceiverId());
            ps.setString(5, bean.getResponsePersonId());
            ps.setString(6, bean.getType());
            ps.setString(7, bean.getResponseDate());
            ps.setString(8, bean.getWorkflowId());
            ps.setString(9, bean.getRequestId());
            ps.setString(10, bean.getStatus());
            ps.setString(11, bean.getSelfType());
            ps.setString(12, bean.getPersonName());
            ps.setString(13, bean.getNationalIdentity());
            ps.executeUpdate();

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return bean;
    }

    public ArrayList getAllNotification(String MANAGER_ID,
                                        String managerOfManager,
                                        String position, String emp,
                                        List<String> userRoles) {

        String rolesIdStr = "";
        int userRolesCount = userRoles.size();

        for (int i = 0; i < userRolesCount; i++) {
            rolesIdStr += ",?";
        }
        rolesIdStr = rolesIdStr.replaceFirst(",", "");
        if(userRolesCount>0){
            rolesIdStr += ")";
        }else{
            userRoles.add("");
            rolesIdStr= "?)";
        }
        userRolesCount = userRoles.size();
      
       

        ArrayList<NotificationBean> notificationList =
            new ArrayList<NotificationBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query = "SELECT * FROM (\n" +
                    "SELECT NT.*,SS.PERSON_ID,SS.PERSON_NUMBER \n" +
                    "    FROM \n" +
                    "         " + " " + getSchema_Name() +
                    ".XX_WORKFLOW_NOTIFICATION NT,\n" +
                    "         " + " " + getSchema_Name() +
                    ".XX_SELF_SERVICE SS\n" +
                    "     WHERE \n" +
                    "            NT.STATUS = 'OPEN' \n" +
                    "            AND SS.TYPE = NT.SELF_TYPE\n" +
                    "            AND SS.TRANSACTION_ID= NT.REQUEST_ID\n" +
                    "            AND ((NT.RECEIVER_TYPE = 'LINE_MANAGER' AND NT.RECEIVER_ID = ?) OR \n" +
                    "                 (NT.RECEIVER_TYPE = 'LINE_MANAGER+1' AND NT.RECEIVER_ID = ?) OR \n" +
                    "(NT.RECEIVER_TYPE = 'LINE_MANAGER+' AND NT.RECEIVER_ID = ?) OR \n" +
                    "(NT.RECEIVER_TYPE = 'AOR' AND NT.RECEIVER_ID = ?)OR \n" +
                    "(NT.RECEIVER_TYPE = 'JOB_LEVEL' AND NT.RECEIVER_ID = ?) OR \n" +
                    "                 (NT.RECEIVER_TYPE = 'POSITION' AND NT.RECEIVER_ID = ?) OR                 \n" +
                    "                (NT.RECEIVER_TYPE = 'EMP' AND NT.RECEIVER_ID = ?)OR" +
                    "(NT.RECEIVER_TYPE = 'Special_Case' AND NT.RECEIVER_ID = ?)OR" +
                    "(NT.RECEIVER_TYPE = 'ROLES' AND NT.RECEIVER_ID IN (" +
                    rolesIdStr + ")" + "                )\n" +
                    "			\n" +
                    "UNION\n" +
                    "    SELECT NT.*,SS.PERSON_ID,SS.PERSON_NUMBER \n" +
                    "    FROM \n" +
                    "         " + " " + getSchema_Name() +
                    ".XX_WORKFLOW_NOTIFICATION NT,\n" +
                    "         " + " " + getSchema_Name() +
                    ".XX_SELF_SERVICE SS \n" +
                    "     WHERE \n" +
                    "            NT.STATUS = 'CLOSED'\n" +
                    "            AND SS.TYPE = NT.SELF_TYPE\n" +
                    "            AND SS.TRANSACTION_ID= NT.REQUEST_ID\n" +
                    "            AND (NT.TYPE = 'FYI' OR( NT.TYPE = 'FYA' and NT.STATUS = 'OPEN')) \n" +
                    "            AND ((NT.RECEIVER_TYPE = 'LINE_MANAGER' AND NT.RECEIVER_ID = ?) OR \n" +
                    "                 (NT.RECEIVER_TYPE = 'POSITION' AND NT.RECEIVER_ID = ?) OR                 \n" +
                    "(NT.RECEIVER_TYPE = 'JOB_LEVEL' AND NT.RECEIVER_ID = ?) OR \n" +
                    "(NT.RECEIVER_TYPE = 'AOR' AND NT.RECEIVER_ID = ?) OR \n" +
                    "				 (NT.RECEIVER_TYPE = 'EMP' AND NT.RECEIVER_ID = ?)\n" +
                    "                )\n" +
                    "			\n" +
                    "				) ORDER BY CREATION_DATE DESC";

            ps = connection.prepareStatement(query);
            ps.setString(1, MANAGER_ID);
            ps.setString(2, MANAGER_ID);
            ps.setString(3, MANAGER_ID);
            ps.setString(4, emp);
            ps.setString(5, MANAGER_ID);
            ps.setString(6, position);
            ps.setString(7, emp);
            ps.setString(8, MANAGER_ID);
            int arrayIndex = 0;
            int psIndex = 9;
            for (int i = 9; i < 9 + userRolesCount; i++) {
                ps.setString(i, userRoles.get(arrayIndex).toString());
                arrayIndex++;
                psIndex++;
            }

            ps.setString(psIndex, MANAGER_ID);
            psIndex++;
            ps.setString(psIndex, position);
            psIndex++;
            ps.setString(psIndex, MANAGER_ID);
            psIndex++;
            ps.setString(psIndex, emp);
            psIndex++;
            ps.setString(psIndex, emp);
            //();
            rs = ps.executeQuery();

            while (rs.next()) {
                NotificationBean obj = new NotificationBean();
                obj.setID(rs.getString("ID"));
                //  obj.put("ID", rs.getString("ID"));
                obj.setMSG_TITLE(rs.getString("MSG_TITLE"));
                obj.setMSG_BODY(rs.getString("MSG_BODY"));
                obj.setCREATION_DATE(rs.getString("CREATION_DATE"));
                obj.setRECEIVER_TYPE(rs.getString("RECEIVER_TYPE"));
                obj.setRECEIVER_ID(rs.getString("RECEIVER_ID"));
                obj.setRESPONSE_PERSON_ID(rs.getString("RESPONSE_PERSON_ID"));
                obj.setTYPE(rs.getString("TYPE"));
                obj.setRESPONSE_DATE(rs.getString("RESPONSE_DATE"));
                obj.setWORKFLOW_ID(rs.getString("WORKFLOW_ID"));
                obj.setREQUEST_ID(rs.getString("REQUEST_ID"));
                obj.setSTATUS(rs.getString("STATUS"));
                obj.setPERSON_NUMBER(rs.getString("PERSON_ID"));
                obj.setSELF_TYPE(rs.getString("SELF_TYPE"));
                obj.setRESPONSE_PERSON_ID(rs.getString("PERSON_NUMBER"));
                obj.setPERSON_NAME(rs.getString("PERSON_NAME"));
                ////(rs.getString("ID"));
                notificationList.add(obj);
                // arr.put(obj);
            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return notificationList;
    }

    public JSONArray getWorkFlowNotification(String MANAGER_ID,
                                             String managerOfManager,
                                             String position, 
                                             String emp,
                                             String self_type,
                                             String requset_id, 
                                             String type) {
        JSONArray arr = new JSONArray();

        if (self_type.equals("undefined")) {
            self_type = "";
        }
        if (requset_id.equals("undefined")) {
            requset_id = "";
        }
        if (type.equals("undefined")) {
            type = "";
        }
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query = "SELECT * FROM \n" +
                    "(SELECT NT.*,SS.PERSON_NUMBER FROM   " + " " +
                    getSchema_Name() + ".XX_WORKFLOW_NOTIFICATION NT, " + " " +
                    getSchema_Name() + ".XX_SELF_SERVICE SS\n" +
                    //NT.STATUS = 'OPEN' AND
                    "WHERE  SS.TYPE = NT.SELF_TYPE AND SS.TRANSACTION_ID= NT.REQUEST_ID AND\n" +
                    "((NT.RECEIVER_TYPE = 'LINE_MANAGER' AND NT.RECEIVER_ID = NVL(?, NT.RECEIVER_ID)) OR\n" +
                    "(NT.RECEIVER_TYPE = 'LINE_MANAGER+1' AND NT.RECEIVER_ID = NVL(?, NT.RECEIVER_ID)) OR \n" +
                    "(NT.RECEIVER_TYPE = 'JOB_LEVEL' AND NT.RECEIVER_ID = NVL(?, NT.RECEIVER_ID)) OR \n" +
                    "(NT.RECEIVER_TYPE = 'POSITION' AND NT.RECEIVER_ID = NVL(?, NT.RECEIVER_ID)) OR\n" +
                    "(NT.RECEIVER_TYPE = 'EMP' AND NT.RECEIVER_ID = NVL(?, NT.RECEIVER_ID)) AND\n" +
                    "(NT.RECEIVER_TYPE = 'AOR' AND NT.RECEIVER_ID = NVL(?, NT.RECEIVER_ID)) AND\n"+
                    "(NT.SELF_TYPE = NVL(?, NT.SELF_TYPE) AND NT.REQUEST_ID = NVL(?, NT.REQUEST_ID) AND NT.TYPE = NVL(?, NT.TYPE)))\n AND NT.STATUS = NVL(?, NT.STATUS)))\n" +
                    "UNION\n" +
                    "SELECT NT.*,SS.PERSON_NUMBER FROM  " + " " +
                    getSchema_Name() + ".XX_WORKFLOW_NOTIFICATION NT, " + " " +
                    getSchema_Name() + ".XX_SELF_SERVICE SS\n" +
                    "WHERE NT.STATUS = 'CLOSED' AND SS.TYPE = NT.SELF_TYPE AND SS.TRANSACTION_ID= NT.REQUEST_ID AND\n" +
                    "NT.TYPE = 'FYI' AND\n" +
                    "((NT.RECEIVER_TYPE = 'LINE_MANAGER' AND NT.RECEIVER_ID = NVL(?, NT.RECEIVER_ID)) OR\n" +
                    "(NT.RECEIVER_TYPE = 'POSITION' AND NT.RECEIVER_ID = NVL(?, NT.RECEIVER_ID)) OR\n" +
                    "(NT.RECEIVER_TYPE = 'JOB_LEVEL' AND NT.RECEIVER_ID = NVL(?, NT.RECEIVER_ID)) OR\n" +
                    "(NT.RECEIVER_TYPE = 'AOR' AND NT.RECEIVER_ID = NVL(?, NT.RECEIVER_ID)) OR\n" +
                    "(NT.RECEIVER_TYPE = 'EMP' AND NT.RECEIVER_ID = NVL(?, NT.RECEIVER_ID)))) \n" +
                    "ORDER BY CREATION_DATE DESC";
            ps = connection.prepareStatement(query);
            ps.setString(1, MANAGER_ID);
            ps.setString(2, MANAGER_ID);
            ps.setString(3, MANAGER_ID);
            ps.setString(4, position);
            ps.setString(5, emp);
            ps.setString(6, emp);
            ps.setString(7, self_type);
            ps.setString(8, requset_id);
            ps.setString(9, type);
            ps.setString(10, MANAGER_ID);
            ps.setString(11, position);
            ps.setString(12, MANAGER_ID);
            ps.setString(13, emp);
            ps.setString(14, emp);
            rs = ps.executeQuery();

            while (rs.next()) {
                JSONObject obj = new JSONObject();
                obj.put("ID", rs.getString("ID"));
                //  obj.put("ID", rs.getString("ID"));
                obj.put("MSG_TITLE", rs.getString("MSG_TITLE"));
                obj.put("MSG_BODY", rs.getString("MSG_BODY"));
                obj.put("CREATION_DATE", rs.getString("CREATION_DATE"));
                obj.put("RECEIVER_TYPE", rs.getString("RECEIVER_TYPE"));
                obj.put("RECEIVER_ID", rs.getString("RECEIVER_ID"));
                obj.put("RESPONSE_PERSON_ID",
                        rs.getString("RESPONSE_PERSON_ID"));
                obj.put("TYPE", rs.getString("TYPE"));
                obj.put("RESPONSE_DATE", rs.getString("RESPONSE_DATE"));
                obj.put("WORKFLOW_ID", rs.getString("WORKFLOW_ID"));
                obj.put("REQUEST_ID", rs.getString("REQUEST_ID"));
                obj.put("STATUS", rs.getString("STATUS"));
                obj.put("PERSON_NUMBER", rs.getString("PERSON_NUMBER"));
                obj.put("SELF_TYPE", rs.getString("SELF_TYPE"));
                ////(rs.getString("ID"));

                arr.put(obj);
            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return arr;
    }

    public void updateWorkflownotification(String PERSON_ID, String STATUS,
                                           String TRS_ID, String ssType,String workFlowId) {
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "UPDATE  " + " " + getSchema_Name() + ".XX_WORKFLOW_NOTIFICATION \n" +
                    "    SET\n" +
                    "        RESPONSE_PERSON_ID = ?,\n" +
                    "        RESPONSE_DATE = SYSDATE,\n" +
                    "        STATUS = ?\n" +
                    "     WHERE\n" +
                    "         REQUEST_ID = ?\n" +
                    "         AND SELF_TYPE = ?\n"+
                    "          AND ID= ?"
                ;
            
            ps = connection.prepareStatement(query);
            ps.setString(1, PERSON_ID);
            ps.setString(2, STATUS);
            ps.setString(3, TRS_ID);
            ps.setString(4, ssType);
            ps.setString(5, workFlowId);
            
            //(" query = " + query);

            ps.executeUpdate();
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
    }

    public void workflowAction(String action) {
        PeopleExtraInformationDAO extraInformation= new PeopleExtraInformationDAO();
        JSONObject jsonObj = new JSONObject(action);
        String v_check = "YES";
        boolean v_check2 = true;
        String V_STEP_LEVEL;
        ApprovalListBean approvalListBean = new ApprovalListBean();
        ApprovalListDAO approvalDao = new ApprovalListDAO();
        SelfServiceDAO selfServiceDao = new SelfServiceDAO();
        
        try {
            V_STEP_LEVEL =
                    approvalDao.getMinStepLeval(jsonObj.getString("TRS_ID"),
                                                jsonObj.getString("ssType"));
            //("Min Step Leval : " + V_STEP_LEVEL);
            approvalListBean =
                    approvalDao.getNextApproval(jsonObj.getString("TRS_ID"),
                                                V_STEP_LEVEL,
                                                jsonObj.getString("ssType"));



            approvalDao.updateApprovalList(jsonObj.getString("RESPONSE_CODE"),
                                           jsonObj.getString("TRS_ID"),
                                           V_STEP_LEVEL,
                                           jsonObj.getString("ssType"),
                                            jsonObj.getString("rejectReason"));
            this.updateWorkflownotification(jsonObj.getString("PERSON_ID"),
                                            "CLOSED",
                                            jsonObj.getString("TRS_ID"),
                                            jsonObj.getString("ssType"),
                                            jsonObj.getString("workflowId") );
            if (jsonObj.getString("RESPONSE_CODE").equals("REJECTED")) {
                selfServiceDao.updateSelfService(jsonObj.getString("RESPONSE_CODE"),
                                                 jsonObj.getString("ssType"),
                                                 jsonObj.getString("TRS_ID"));
               extraInformation.updateStatus("REJECTED",jsonObj.getString("rejectReason"),
                                    Integer.parseInt(jsonObj.getString("TRS_ID")) );
            }
            V_STEP_LEVEL =
                    approvalDao.getMinStepLeval(jsonObj.getString("TRS_ID"),
                                                jsonObj.getString("ssType"));
            //("Next Approval  Step Leval : " + V_STEP_LEVEL);
            approvalListBean =
                    approvalDao.getNextApproval(jsonObj.getString("TRS_ID"),
                                                V_STEP_LEVEL,
                                                jsonObj.getString("ssType"));

            if (approvalListBean.getId() == null) {
                //("Inside Null ");
                selfServiceDao.updateSelfService(jsonObj.getString("RESPONSE_CODE"),
                                                 jsonObj.getString("ssType"),
                                                 jsonObj.getString("TRS_ID"));
                v_check = "NO";
            }
            if (!v_check.equalsIgnoreCase("NO")) {
                if (jsonObj.getString("RESPONSE_CODE").equals("APPROVED")) {
                    if (approvalListBean.getNotificationType().equals("FYI")) {
                        while (v_check2) {
                            if (approvalListBean.getNotificationType().equals("FYI")) {
                                WorkFlowNotificationBean wfnBean =
                                    new WorkFlowNotificationBean();
                                wfnBean.setMsgTitle(jsonObj.getString("MSG_TITLE"));
                                wfnBean.setMsgBody(jsonObj.getString("MSG_BODY"));
                                wfnBean.setReceiverType(approvalListBean.getRolrType());
                                wfnBean.setReceiverId(approvalListBean.getRoleId());
                                wfnBean.setType(approvalListBean.getNotificationType());
                                wfnBean.setRequestId(approvalListBean.getTransActionId());
                                wfnBean.setStatus("OPEN");
                                wfnBean.setPersonName(jsonObj.get("PERSON_NAME").toString());
                                wfnBean.setSelfType(approvalListBean.getServiceType());
                                //("W Notification : ");
                                //(new JSONObject(wfnBean));
                                this.insertIntoWorkflow(wfnBean);
                                sendEmail(approvalListBean.getRolrType(),approvalListBean.getNotificationType(),approvalListBean.getTransActionId(),jsonObj.get("PERSON_NAME").toString() , 
                                                                approvalListBean.getRoleId()  ) ;
                                approvalDao.updateReqeustDate(approvalListBean.getId());
                                approvalDao.getMinStepLeval(approvalListBean.getTransActionId(),
                                                            approvalListBean.getServiceType());
                                approvalDao.updateApprovalList("Delivered",
                                                               approvalListBean.getTransActionId(),
                                                               approvalListBean.getStepLeval(),
                                                               approvalListBean.getServiceType(),
                                                             ""  );
                                this.updateWorkflownotification(jsonObj.getString("PERSON_ID"),
                                                                "CLOSED",
                                                                jsonObj.getString("TRS_ID"),
                                                                jsonObj.getString("ssType"),
                                                                jsonObj.getString("workflowId"));
                                V_STEP_LEVEL =
                                        approvalDao.getMinStepLeval(approvalListBean.getTransActionId(),
                                                                    approvalListBean.getServiceType());
                                approvalListBean =
                                        approvalDao.getNextApproval(approvalListBean.getTransActionId(),
                                                                    V_STEP_LEVEL,
                                                                    approvalListBean.getServiceType());


                                String Str = jsonObj.getString("MSG_BODY");
                                //("Str"+ Str);
                                //(Str.indexOf( ':' ));

                                String personId =
                                    Str.substring(Str.indexOf(':') + 1);
                                String eitCode =
                                    Str.substring(0, Str.indexOf(':'));
                                //("personId"+personId);
                                //("eitCode"+eitCode);
                            
                            
                                if (eitCode.equals("XXX_HR_PERSONAL_LOAN") ) {
                                    
                                    emp =
                           employeeDetails.getEmpDetailsByPersonId(personId.toString(), "", "");
                                    String email = emp.getEmail();
//                                    CredentialStoreClassMail.SEND_MAIL("hosni.tarek@appspro-me.com",
//                                                                       "For your Infromation ","Noor");
                                }


                                if (approvalListBean.getId() == null) {
                                    v_check2 = false;
                                }


                            } else {
                                v_check2 = false;
                                WorkFlowNotificationBean wfnBean =
                                    new WorkFlowNotificationBean();
                                wfnBean.setMsgTitle(jsonObj.getString("MSG_TITLE"));
                                wfnBean.setMsgBody(jsonObj.getString("MSG_BODY"));
                                wfnBean.setReceiverType(approvalListBean.getRolrType());
                                wfnBean.setReceiverId(approvalListBean.getRoleId());
                                wfnBean.setType(approvalListBean.getNotificationType());
                                wfnBean.setRequestId(approvalListBean.getTransActionId());
                                wfnBean.setStatus("OPEN");
                                wfnBean.setPersonName(jsonObj.get("PERSON_NAME").toString());
                                wfnBean.setSelfType(approvalListBean.getServiceType());
                                this.insertIntoWorkflow(wfnBean);
                                sendEmail(approvalListBean.getRolrType(),approvalListBean.getNotificationType(),approvalListBean.getTransActionId(),jsonObj.get("PERSON_NAME").toString() , 
                                                                approvalListBean.getRoleId()  ) ;
                                approvalDao.updateReqeustDate(approvalListBean.getId());
                            }
                        }
                    } else {
                        WorkFlowNotificationBean wfnBean =
                            new WorkFlowNotificationBean();
                        wfnBean.setMsgTitle(jsonObj.getString("MSG_TITLE"));
                        wfnBean.setMsgBody(jsonObj.getString("MSG_BODY"));
                        wfnBean.setReceiverType(approvalListBean.getRolrType());
                        wfnBean.setReceiverId(approvalListBean.getRoleId());
                        wfnBean.setType(approvalListBean.getNotificationType());
                        wfnBean.setRequestId(approvalListBean.getTransActionId());
                        wfnBean.setStatus("OPEN");
                        wfnBean.setPersonName(jsonObj.get("PERSON_NAME").toString());
                        wfnBean.setSelfType(approvalListBean.getServiceType());
                        this.insertIntoWorkflow(wfnBean);
                        sendEmail(approvalListBean.getRolrType(),approvalListBean.getNotificationType(),approvalListBean.getTransActionId(),jsonObj.get("PERSON_NAME").toString() , 
                                approvalListBean.getRoleId()  ) ; 
                        
                        
                        approvalDao.updateReqeustDate(approvalListBean.getId());

                    }
                }
            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

    }

    public ArrayList<WorkFlowNotificationBean> getWorkFlowNotificationByFilter(String self_type,
                                                                               String request_id,
                                                                               String type,
                                                                               String receiver_id) {
        ArrayList<WorkFlowNotificationBean> list =
            new ArrayList<WorkFlowNotificationBean>();

        WorkFlowNotificationBean obj = null;
        try {
            connection = AppsproConnection.getConnection();
            String query = null;

            query =
                    "select NT.*, ss.person_number from  " + " " + getSchema_Name() +
                    ".XX_WORKFLOW_NOTIFICATION NT INNER JOIN  " + " " +
                    getSchema_Name() + ".XX_SELF_SERVICE SS \n" +
                    "ON nt.self_type = ss.type AND nt.request_id = ss.transaction_id\n" +
                    "WHERE NT.SELF_TYPE LIKE ? AND NT.REQUEST_ID LIKE ? AND NT.TYPE LIKE ? AND nt.receiver_id = ? AND nt.status NOT IN ('CLOSED')";

            if (request_id.equals("undefined") || type.equals("undefined")) {
                request_id = "";
                type = "";
            }

            ps = connection.prepareStatement(query);
            ps.setString(1, "%" + self_type + "%");
            ps.setString(2, "%" + request_id + "%");
            ps.setString(3, "%" + type + "%");
            ps.setString(4, receiver_id);
            rs = ps.executeQuery();

            while (rs.next()) {
                obj = new WorkFlowNotificationBean();
                obj.setID(rs.getString("ID"));
                obj.setMsgTitle(rs.getString("MSG_TITLE"));
                obj.setMsgBody(rs.getString("MSG_BODY"));
                obj.setCreationDate(rs.getString("CREATION_DATE"));
                obj.setReceiverType(rs.getString("RECEIVER_TYPE"));
                obj.setReceiverId(rs.getString("RECEIVER_ID"));
                obj.setResponsePersonId(rs.getString("RESPONSE_PERSON_ID"));
                obj.setType(rs.getString("TYPE"));
                obj.setResponseDate(rs.getString("RESPONSE_DATE"));
                obj.setWorkflowId(rs.getString("WORKFLOW_ID"));
                obj.setRequestId(rs.getString("REQUEST_ID"));
                obj.setStatus(rs.getString("STATUS"));
                obj.setPersonNumber(rs.getString("PERSON_NUMBER"));
                obj.setSelfType(rs.getString("SELF_TYPE"));
                ////(rs.getString("ID"));

                list.add(obj);
            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return list;
    }

    //************************Search Notification*******************************

    public ArrayList<NotificationBean> getSearchWorkFlowNotification(String MANAGER_ID,
                                                                     String managerOfManager,
                                                                     String position,
                                                                     String emp,
                                                                     String self_type,
                                                                String personNumber,
                                                                     String requset_id,
                                                                     String type,
                                                                     String nationalIdentity,
                                                                     String status) {
        
        
        
       String ssStatus=null;
          String ntStatus=null;
        ArrayList<NotificationBean> list = new ArrayList<NotificationBean>();
        NotificationBean obj = null;
       String operator=null;
       AppsproConnection.LOG.info(self_type);
        if (self_type.equals("")) {
            self_type = null;
        }
        if (requset_id.equals("")) {
            requset_id = null;
        }
        if (type.equals("")) {
            type = null;
        }
        if(nationalIdentity.isEmpty() || nationalIdentity.equals("") || nationalIdentity.equals("")){
            nationalIdentity=null;
                operator="OR";
        }else{
                operator="AND";
            }
        
        if(("APPROVED").equalsIgnoreCase(status)){
            ssStatus="APPROVED";
            ntStatus="CLOSED";
        }else if(("REJECT").equalsIgnoreCase(status)){
            ssStatus="REJECTED";
            ntStatus="CLOSED";
        }else if(("PENDDING APPROVED").equalsIgnoreCase(status)){
            ssStatus=null;
            ntStatus="OPEN";
        }
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
            "SELECT * FROM (SELECT NT.*,SS.PERSON_ID,SS.PERSON_NUMBER FROM "  +
             " " + getSchema_Name() + ".XX_WORKFLOW_NOTIFICATION NT " +
           " INNER JOIN \n" +
            " " + getSchema_Name() + ".XX_SELF_SERVICE SS\n" +
            "ON SS.TRANSACTION_ID= NT.REQUEST_ID \n" +
            "WHERE     SS.TYPE = NT.SELF_TYPE  AND SS.TRANSACTION_ID= NT.REQUEST_ID AND ((NT.RECEIVER_TYPE = 'LINE_MANAGER' AND NT.RECEIVER_ID = ?) OR\n" +
                        "   (NT.RECEIVER_TYPE = 'LINE_MANAGER+1' AND NT.RECEIVER_ID = ?) OR\n" +
                        "    (NT.RECEIVER_TYPE = 'POSITION' AND NT.RECEIVER_ID = ?) OR\n" +
                          "  (NT.RECEIVER_TYPE = 'LINE_MANAGER+' AND NT.RECEIVER_ID = ?)OR\n" +
                         " (NT.RECEIVER_TYPE = 'JOB_LEVEL' AND NT.RECEIVER_ID = ?) OR\n" +
                            " (NT.RECEIVER_TYPE = 'AOR' AND NT.RECEIVER_ID = ?)OR\n" +
                           "(NT.RECEIVER_TYPE = 'EMP' AND NT.RECEIVER_ID = ?))AND\n" +
             "((NT.SELF_TYPE =? or ? is null) AND (SS.PERSON_NUMBER =? or ? is null) AND (NT.TYPE =? or ? is null) and " +
             "( NT.NATIONALIDENTITY =? or ? is null) AND (SS.STATUS =? OR  ? is null) AND (NT.STATUS =? OR ? is null)))";
            ps = connection.prepareStatement(query);

            ps.setString(1, MANAGER_ID);
            ps.setString(2, MANAGER_ID);
            ps.setString(3, position);
            ps.setString(4, MANAGER_ID);
            ps.setString(5, MANAGER_ID);
            ps.setString(6, emp);
            ps.setString(7, emp);
            ps.setString(8, self_type);
            ps.setString(9, self_type);
            ps.setString(10,personNumber );
            ps.setString(11,personNumber );
            ps.setString(12,type );
            ps.setString(13,type );
            ps.setString(14,nationalIdentity );
            ps.setString(15,nationalIdentity );
            ps.setString(16, ssStatus);
            ps.setString(17, ssStatus);
            ps.setString(18, ntStatus);
            ps.setString(19, ntStatus);

            rs = ps.executeQuery();

            while (rs.next()) {
                obj = new NotificationBean();
                obj.setID(rs.getString("ID"));
                //  obj.put("ID", rs.getString("ID"));
                obj.setMSG_TITLE(rs.getString("MSG_TITLE"));
                obj.setMSG_BODY(rs.getString("MSG_BODY"));
                obj.setCREATION_DATE(rs.getString("CREATION_DATE"));
                obj.setRECEIVER_TYPE(rs.getString("RECEIVER_TYPE"));
                obj.setRECEIVER_ID(rs.getString("RECEIVER_ID"));
                obj.setRESPONSE_PERSON_ID(rs.getString("RESPONSE_PERSON_ID"));
                obj.setTYPE(rs.getString("TYPE"));
                obj.setRESPONSE_DATE(rs.getString("RESPONSE_DATE"));
                obj.setWORKFLOW_ID(rs.getString("WORKFLOW_ID"));
                obj.setREQUEST_ID(rs.getString("REQUEST_ID"));
                obj.setSTATUS(rs.getString("STATUS"));
                obj.setPERSON_NUMBER(rs.getString("PERSON_ID"));
                obj.setSELF_TYPE(rs.getString("SELF_TYPE"));
                obj.setRESPONSE_PERSON_ID(rs.getString("PERSON_NUMBER"));
                 obj.setPERSON_NAME(rs.getString("PERSON_NAME"));
                ////(rs.getString("ID"));

                list.add(obj);
            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return list;
    }
    
    
    public void sendEmail(String approvalType  , String notificationType ,String transactionId ,String personName
                           , String roleId) throws AppsProException {
        PeopleExtraInformationDAO extraInformation= new PeopleExtraInformationDAO();
        PeopleExtraInformationBean reqBean = new PeopleExtraInformationBean();
        reqBean = extraInformation.getEITById(transactionId) ; 
        String selfServiceName = reqBean.getEit_name();
        EmployeeBean empObj = new  EmployeeBean (); 
        //employeeId = mean person you went to send email to 
        //roleid = mean the role id if approval type is role 
        //employee id if line manager or line manager +1 
        //position id if approval type is position 
        if(approvalType.equals("LINE_MANAGER")){
            try{
                
                empObj= employeeDetails.getEmpDetailsByPersonId(roleId,"","");                                   
                  mailObj.SEND_MAIL(empObj.getEmail(),
                                    rh.getEmailSubject(notificationType,selfServiceName,personName)
                                    ,rh.getEmailBody(notificationType, empObj.getDisplayName(),selfServiceName, personName));
            }catch (Exception e) {
                //("Error: ");
               e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            }
        }else if (approvalType.equals("LINE_MANAGER+1")){
            try{
                empObj= employeeDetails.getEmpDetailsByPersonId(roleId,"","");    
                mailObj.SEND_MAIL(empObj.getEmail(),
                                   rh.getEmailSubject(notificationType,selfServiceName,personName),
                                  rh.getEmailBody(notificationType, empObj.getDisplayName(), selfServiceName, personName));
            }catch (Exception e) {
                //("Error: ");
               e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            }
        }else if(approvalType.equals("ROLES")){
            try{
                                                Map<String, String> paramMAp = new HashMap<String, String>();
                                                paramMAp.put("P_ROLEID",roleId);
                                                JSONObject respone =
                                                    BIReportModel.runReport("EmployeeRoleForCustomRoleReport", paramMAp);
                                              
                                                JSONObject dataDS = respone.getJSONObject("DATA_DS");
                                                JSONArray g1 = dataDS.getJSONArray("G_1");
                                                for (int i = 0; i < g1.length(); i++) {                               
                                                    JSONObject data = (JSONObject)g1.get(i);                                                   
                                                    mailObj.SEND_MAIL(data.getString("EMAIL_ADDRESS"),
                                                                      rh.getEmailSubject(notificationType,selfServiceName,personName)
                                                                      ,rh.getEmailBody(notificationType,data.getString("FULL_NAME") , selfServiceName,personName));
                                                }
                                                    
                                                
                                            }catch (Exception e) {
                                                //("Error: ");
                                               e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
                                            }
        }else if (approvalType.equals("POSITION")){
            try{
                                                Map<String, String> paramMAp = new HashMap<String, String>();
                                                paramMAp.put("positionId",roleId);
                                                JSONObject respone =
                                                    BIReportModel.runReport("EmailForPositionsReports", paramMAp);

                                                JSONObject dataDS = respone.getJSONObject("DATA_DS");
                                                JSONArray g1 = dataDS.getJSONArray("G_1");
                                                for (int i = 0; i < g1.length(); i++) {                               
                                                    JSONObject data = (JSONObject)g1.get(i);
                                                   
                                                    mailObj.SEND_MAIL(data.getString("EMAIL_ADDRESS"),
                                                                      rh.getEmailSubject(notificationType,selfServiceName,personName)
                                                                      ,rh.getEmailBody(notificationType,data.getString("FULL_NAME") , selfServiceName,personName));
                                                }
                                                    
                                                
                                            }catch (Exception e) {
                                                //("Error: ");
                                               e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
                                            }
        }
    }
}
