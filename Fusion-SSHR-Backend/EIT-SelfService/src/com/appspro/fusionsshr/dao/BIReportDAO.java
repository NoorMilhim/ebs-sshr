/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.dao;


import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.SummaryReportBean;

import common.biPReports.BIPReports;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.codec.binary.Base64;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 *
 * @author Lenovo
 */
public class BIReportDAO {

    public String getBiReport(JSONObject jObject) {
        StringBuilder crunchifyBuilder = new StringBuilder();

        // JSONObject jObject; // json
        String fresponse = null;
        try {
            //    jObject = new JSONObject(incomingData);
            String reportName = jObject.get("reportName").toString();
            if (!BIPReports.REPORT_NAME.EIT_Name.getValue().equals(reportName)) {
                //                if(!Authentication.restAuth(authString, authorization)) {
                //                    return Response.status(Response.Status.UNAUTHORIZED).build();
                //                }
            }

            BIPReports biPReports = new BIPReports();
            biPReports.setAttributeFormat(BIPReports.ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_XML.getValue());
            biPReports.setAttributeTemplate(BIPReports.ATTRIBUTE_TEMPLATE.ATTRIBUTE_TEMPLATE_XML.getValue());

            if (reportName != null) {

                if (reportName.equals(BIPReports.REPORT_NAME.FUSE_REPORT.getValue())) {

                    String paramName = null;

                    //                    for (int i = 0;
                    //                         i < BIPReports.FUSE_REPORT_PARAM.values().length;
                    //                         i++) {
                    //                        paramName =
                    //                                BIPReports.FUSE_REPORT_PARAM.values()[i].getValue();
                    //                        biPReports.getParamMap().put(paramName,
                    //                                                     jObject.getString(paramName));
                    //                    }
                    biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.FUSE_REPORT.getValue());
                    fresponse = biPReports.executeReports();

                    JSONObject xmlJSONObj =
                        XML.toJSONObject(fresponse.toString());
                    String jsonString =
                        xmlJSONObj.getJSONObject("DATA_DS").getJSONArray("G_1").toString();

                    return jsonString;

                } else if (reportName.equals(BIPReports.REPORT_NAME.GET_ALL_POSITIONS.getValue())) {

                    biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.GET_ALL_POSITIONS.getValue());
                    fresponse = biPReports.executeReports();
                    JSONObject xmlJSONObj =
                        XML.toJSONObject(fresponse.toString());
                    String jsonString =
                        xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                                                             "value").replaceAll("LABEL",
                                                                                                                 "label");

                    return jsonString;

                } else if (reportName.equals(BIPReports.REPORT_NAME.EIT_REPORT.getValue())) {
                    String paramName = null;
                    for (int i = 0;
                         i < BIPReports.EIT_REPORT_PARAM.values().length;
                         i++) {
                        paramName =
                                BIPReports.EIT_REPORT_PARAM.values()[i].getValue();
                        biPReports.getParamMap().put(paramName,
                                                     jObject.get(paramName).toString());
                    }

                    biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.EIT_REPORT.getValue());
                    fresponse = biPReports.executeReports();
                    JSONObject xmlJSONObj =
                        XML.toJSONObject(fresponse.toString());
                    String jsonString =
                        xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                                                             "value").replaceAll("LABEL",
                                                                                                                 "label");

                    return jsonString;

                } else if (reportName.equals(BIPReports.REPORT_NAME.Dynamic_Rrport.getValue())) {
                    String paramName = null;
                    //("Dynamic Report ");
                    for (int i = 0;
                         i < BIPReports.Dynamic_Rrport_PARAM.values().length;
                         i++) {
                        paramName =
                                BIPReports.Dynamic_Rrport_PARAM.values()[i].getValue();
                        biPReports.getParamMap().put(paramName,
                                                     jObject.get(paramName).toString().replaceAll(">",
                                                                                                  "GT").replaceAll("<",
                                                                                                                   "LT"));
                    }
                    //(paramName);
                    //(jObject.get(paramName));
                    //(BIPReports.REPORT_NAME.Dynamic_Rrport.getValue());

                    biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.Dynamic_Rrport.getValue());
                    fresponse = biPReports.executeReports();
                    JSONObject xmlJSONObj =
                        XML.toJSONObject(fresponse.toString());
                    //(fresponse.toString());
                    String jsonString =
                        xmlJSONObj.getJSONObject("ROWSET").get("ROW").toString().replaceAll("VALUE",
                                                                                            "value").replaceAll("LABEL",
                                                                                                                "label");

                    return jsonString;

                } else if (reportName.equals(BIPReports.REPORT_NAME.Dynamic_Validation_Rrport.getValue())) {
                    String paramName = null;
                    //("Dynamic Validation  Report ");
                    for (int i = 0;
                         i < BIPReports.Dynamic_Rrport_PARAM.values().length;
                         i++) {
                        paramName =
                                BIPReports.Dynamic_Rrport_PARAM.values()[i].getValue();
                        biPReports.getParamMap().put(paramName,
                                                     jObject.get(paramName).toString().replaceAll(">",
                                                                                                  "GT").replaceAll("<",
                                                                                                                   "LT"));
                    }
                    //(paramName);
                    //(jObject.get(paramName));
                    //(BIPReports.REPORT_NAME.Dynamic_Rrport.getValue());

                    biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.Dynamic_Rrport.getValue());
                    fresponse = biPReports.executeReports();
                    JSONObject xmlJSONObj =
                        XML.toJSONObject(fresponse.toString());
                    //(fresponse.toString());
                    String jsonString =
                        xmlJSONObj.getJSONObject("ROWSET").get("ROW").toString().replaceAll("VALUE",
                                                                                            "value");

                    //                    return Response.ok(jsonString, MediaType.APPLICATION_JSON).build();
                    JSONObject jsonObj = new JSONObject(jsonString);
                    return jsonString;

                } else if (reportName.contains(BIPReports.REPORT_NAME.ABSENCE_REPORT.getValue())) {
                    String paramName = null;
                    //String subReportName = jObject.getString("reportSubName");
                    //                    //(subReportName);
                    for (int i = 0;
                         i < BIPReports.ABSENCE_REPORT_PARAM.values().length;
                         i++) {
                        paramName =
                                BIPReports.ABSENCE_REPORT_PARAM.values()[i].getValue();
                        biPReports.getParamMap().put(paramName,
                                                     jObject.get(paramName).toString());
                    }

                    biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.ABSENCE_REPORT.getValue());
                    biPReports.setAttributeFormat(BIPReports.ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_PDF.getValue());
                    biPReports.setAttributeTemplate(BIPReports.ATTRIBUTE_TEMPLATE.ATTRIBUTE_TEMPLATE_PDF.getValue());
                    byte[] reportBytes = biPReports.executeReportsPDF();

                    fresponse =
                            javax.xml.bind.DatatypeConverter.printBase64Binary(new Base64().decode(reportBytes));

                    //  //(reportBytes);
                } else if (reportName.equals(BIPReports.REPORT_NAME.get_Query_For_List_Report.getValue())) {
                    String paramName = null;
                    //("get_Query_For_List_Report Report ");
                    for (int i = 0;
                         i < BIPReports.get_Query_For_List_Report_PARAM.values().length;
                         i++) {
                        paramName =
                                BIPReports.get_Query_For_List_Report_PARAM.values()[i].getValue();
                        biPReports.getParamMap().put(paramName,
                                                     jObject.get(paramName).toString());
                        //(BIPReports.get_Query_For_List_Report_PARAM.values()[i].getValue());
                    }

                    biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.get_Query_For_List_Report.getValue());
                    fresponse = biPReports.executeReports();
                    JSONObject xmlJSONObj =
                        XML.toJSONObject(fresponse.toString());
                    String jsonString =
                        xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                                                             "value").replaceAll("LABEL",
                                                                                                                 "label");

                    return jsonString;

                } else if (reportName.equals(BIPReports.REPORT_NAME.EIT_Name.getValue())) {

                    String paramName = null;

                    //                    for (int i = 0;
                    //                         i < BIPReports.FUSE_REPORT_PARAM.values().length;
                    //                         i++) {
                    //                        paramName =
                    //                                BIPReports.FUSE_REPORT_PARAM.values()[i].getValue();
                    //                        biPReports.getParamMap().put(paramName,
                    //                                                     jObject.getString(paramName));
                    //                    }
                    biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.EIT_Name.getValue());
                    fresponse = biPReports.executeReports();

                    JSONObject xmlJSONObj =
                        XML.toJSONObject(fresponse.toString());
                    String jsonString =
                        xmlJSONObj.getJSONObject("DATA_DS").getJSONArray("G_1").toString();

                    return jsonString;

                } else {
                    String paramName;
                    List<String> paramValues = new ArrayList<String>();
                    List<String> paramNameList =
                        Arrays.asList("id", "report_Name", "parameter1",
                                      "parameter2", "parameter3", "parameter4",
                                      "parameter5");

                    SummaryReportDAO summary = new SummaryReportDAO();
                    ArrayList arr = summary.getReportParamSummary(reportName);
                    SummaryReportBean reportBeans =
                        (SummaryReportBean)arr.get(0);

                    //(arr.size());

                    if (reportBeans.getParameter1() != null) {
                        paramName = reportBeans.getParameter1();
                        biPReports.getParamMap().put(paramName,
                                                     jObject.get(paramName).toString());
                    }
                    if (reportBeans.getParameter2() != null) {
                        paramName = reportBeans.getParameter2();
                        biPReports.getParamMap().put(paramName,
                                                     jObject.get(paramName).toString());
                    }
                    if (reportBeans.getParameter3() != null) {
                        paramName = reportBeans.getParameter3();
                        biPReports.getParamMap().put(paramName,
                                                     jObject.get(paramName).toString());
                    }
                    if (reportBeans.getParameter4() != null) {
                        paramName = reportBeans.getParameter4();
                        biPReports.getParamMap().put(paramName,
                                                     jObject.get(paramName).toString());
                    }
                    if (reportBeans.getParameter5() != null) {
                        paramName = reportBeans.getParameter5();
                        biPReports.getParamMap().put(paramName,
                                                     jObject.get(paramName).toString());
                    }

                    biPReports.setReportAbsolutePath(reportBeans.getReport_Name());

                    //                    for (SummaryReportBean reportBeans : arr) {
                    //
                    ////                        paramValues.add(reportBeans.getId());
                    ////                        paramValues.add(reportBeans.getReport_Name());
                    ////                        paramValues.add(reportBeans.getParameter1());
                    ////                        paramValues.add(reportBeans.getParameter2());
                    ////                        paramValues.add(reportBeans.getParameter3());
                    ////                        paramValues.add(reportBeans.getParameter4());
                    ////                        paramValues.add(reportBeans.getParameter5());
                    //
                    ////                        for (int i = 0; i < paramValues.size(); i++) {
                    ////                            paramName = paramNameList.get(i);
                    ////                            biPReports.getParamMap().put(paramName, jObject.getString(paramName));
                    ////                            biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.get_Query_For_List_Report.getValue());
                    ////                        }
                    //
                    //                     }
                    biPReports.setReportAbsolutePath(reportBeans.getReport_Name());

                    fresponse = biPReports.executeReports();
                    JSONObject xmlJSONObj =
                        XML.toJSONObject(fresponse.toString());

                    return xmlJSONObj.toString();
                }
            }

        } catch (JSONException e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            //return Response.status(Response.Status.EXPECTATION_FAILED.getStatusCode(),e.getMessage()).build();
        }

        // return HTTP response 200 in case of success
        return fresponse;
    }

    public static void doSomething(Node node) {
        // do something with the current node instead of System.out
        String xx = node.getNodeName();
        if (node.getNodeName().equals("G_1")) {
            for (int i = 0; i < node.getChildNodes().getLength(); i++) {
                Node currentNode = node.getChildNodes().item(i);

            }
        }

        NodeList nodeList = node.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node currentNode = nodeList.item(i);
            if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
                //calls this method for all the children which is Element
                doSomething(currentNode);
            }
        }
    }

}
