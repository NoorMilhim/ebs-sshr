/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.dao;


import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.EmployeeBean;
import com.appspro.fusionsshr.bean.rest.Assignment;
import com.appspro.fusionsshr.bean.rest.Example;
import com.appspro.fusionsshr.bean.rest.Link;
import com.appspro.fusionsshr.bean.rest.Link_;
import com.appspro.fusionsshr.bean.rest.Photo;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import common.restHelper.RestHelper;

import java.awt.image.BufferedImage;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import java.net.HttpURLConnection;
import java.net.URL;

import java.security.NoSuchAlgorithmException;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Date;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


public class EmployeeDetails extends RestHelper {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();

    public EmployeeDetails() {
        super();
    }


    final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };


    public EmployeeBean getEmpDetails(String username, String jwt, String host,
                                      boolean mainData) {

        EmployeeBean emp = new EmployeeBean();


//        String serverUrl =
//            this.getEmployeeUrl() + "?q=UserName=" + username + "&expand=personExtraInformation,assignments,photo,assignments.peopleGroupKeyFlexfield&onlyData=" +
//            !mainData;
//
//        ObjectMapper jmapper = new ObjectMapper();
//        Example opty = null;
//        try {
//
//
//
//        } catch (JsonParseException e) {
//           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
//        } catch (JsonMappingException e) {
//           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
//        } catch (IOException e) {
//           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
//        }
       
        return emp;
    }
    


    public EmployeeBean getEmpDetailsByPersonId(String personId, String jwt,
                                                String host) {


        String serverUrl =
            this.getEmployeeUrl() + "?q=PersonId=" + personId + "&expand=personExtraInformation,assignments,photo,assignments.peopleGroupKeyFlexfield";

        EmployeeBean emp = new EmployeeBean();
        ObjectMapper jmapper = new ObjectMapper();
        Example opty = null;
        try {
            String response = new RestHelper().callSaaS(serverUrl, "2");
            opty = jmapper.readValue(response, Example.class);


        } catch (JsonParseException e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } catch (JsonMappingException e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } catch (IOException e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }
        emp = getEmpAssignment(null, host, emp, true, opty);
        

        return emp;
    }


    public EmployeeBean getEmpAssignment(String jwttoken, String host,
                                         EmployeeBean emp, boolean mainData,
                                         Example example) {

        try {
           


            return emp;
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }

        return emp;

    }

    public EmployeeBean searchPersonDetails(String name, String nationalId,
                                            String personNumber,
                                            String managerId,
                                            String effectiveAsof, String jwt,
                                            String host) {

        String queryParam = this.getEmployeeUrl() + "?q=";


        if (personNumber != null && !personNumber.isEmpty() &&
            !personNumber.equals("undefined")) {
            queryParam = queryParam + "PersonNumber='" + personNumber + "'";
        }

        if (name != null && !name.isEmpty() && !name.equals("undefined")) {
            if (personNumber != null && !personNumber.isEmpty()) {
                queryParam = queryParam + " and ";
            }
            queryParam =
                    queryParam + "DisplayName='" + name + "'";
        }

        if (nationalId != null && !nationalId.isEmpty() &&
            !nationalId.equals("undefined")) {
            if ((personNumber != null && !personNumber.isEmpty()) ||
                (name != null && !name.isEmpty())) {
                queryParam = queryParam + " and ";
            }
            queryParam =
                    queryParam + "NationalId='" + nationalId + "'";
        }


        if (effectiveAsof != null && !effectiveAsof.isEmpty() &&
            !effectiveAsof.equals("undefined")) {
            if ((personNumber != null && !personNumber.isEmpty()) ||
                (name != null && !name.isEmpty()) ||
                (nationalId != null && !nationalId.isEmpty())) {
                queryParam = queryParam + " and ";
            }
            queryParam =
                    queryParam + "EffectiveStartDate  < = '" + effectiveAsof +
                    "'";
        }

        if (managerId != null && !managerId.isEmpty()) {
            if ((personNumber != null && !personNumber.isEmpty()) ||
                (name != null && !name.isEmpty()) ||
                (nationalId != null && !nationalId.isEmpty()) ||
                (effectiveAsof != null && !effectiveAsof.isEmpty())) {
                queryParam = queryParam + " and ";
            }
            queryParam = queryParam + "assignments.ManagerId =" + managerId;
        }
        queryParam =
                queryParam + "&expand=personExtraInformation,assignments,photo,assignments.peopleGroupKeyFlexfield";
        EmployeeBean emp = new EmployeeBean();
        ObjectMapper jmapper = new ObjectMapper();
        Example opty = null;
        try {
            String response = new RestHelper().callSaaS(queryParam, "2");
            opty = jmapper.readValue(response, Example.class);


        } catch (JsonParseException e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } catch (JsonMappingException e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } catch (IOException e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }

        if (opty.getItems() == null || opty.getItems().size() == 0 ||
            opty.getItems().get(0).getPersonId() == null) {
            return null;
        }
        //            List<EmployeeBean> employees = new ArrayList<EmployeeBean>();
        emp = getEmpAssignment(null, host, emp, true, opty);
        //            for (int i = 0; i < arr.length(); i++) {
        //                EmployeeBean emp = new EmployeeBean();
        //                JSONArray jsonArray = new JSONArray();
        //                jsonArray.put(arr.get(i));
        //                employees.add(getEmpAssignment(null, host, jsonArray, emp));
        //            }
        //            EmployeeBean[] array = new EmployeeBean[employees.size()];
        //            employees.toArray(array);
        
        return emp;

    }






    public EmployeeBean employeeByUserName() {
        String output = "";
        EmployeeBean emp = new EmployeeBean();
        try {
            AppsproConnection.LOG.info("Invoke service using direct HTTP call with Basic Auth");

                        String soapRest ="<soapenv:Envelope xmlns:get=\"http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxrasf_intg_pkg/get_employee_info/\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xxr=\"http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxrasf_intg_pkg/\">\n"+
    "<soapenv:Header>\n"+
      "<wsse:Security soapenv:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">\n"+
         "<wsse:UsernameToken wsu:Id=\"UsernameToken-E875E1FD6EABA4A94B15844589027763\">\n"+
           " <wsse:Username>sysadmin</wsse:Username>\n"+
            "<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">sysadmin</wsse:Password>\n"+
            "<wsse:Nonce EncodingType=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary\">+xiNDBUfASnhJI+r6CiiEQ==</wsse:Nonce>\n"+
            "<wsu:Created>2020-03-17T15:28:22.776Z</wsu:Created>\n"+
       "  </wsse:UsernameToken>\n"+
      "</wsse:Security>\n"+
     " <xxr:SOAHeader> \n"+       
     " </xxr:SOAHeader>\n"+
   " </soapenv:Header> \n"+
    "<soapenv:Body>\n"+
     " <get:InputParameters>\n"+
         
         "<get:P_USER_NAME>MOSTAFA</get:P_USER_NAME>\n"+
         
        " <get:P_LANG>AR</get:P_LANG>\n"+
     " </get:InputParameters>\n"+
    "</soapenv:Body>\n"+
    "</soapenv:Envelope>";
            Document doc =
                new RestHelper().httpPost("http://hcmpub.appspro-me.com:8075/webservices/SOAProvider/plsql/xxrasf_intg_pkg/", soapRest);

            doc.getDocumentElement().normalize();
            System.out.println("-----------------" );
            System.out.println( doc.toString());
            System.out.println("-----------------" );
            String data =
                doc.getElementsByTagName("env:Body").item(0).getTextContent();
            if (false) {
//                output = StringUtils.newStringUtf8(Base64.decodeBase64(data));
            } else {
                output = data;
                System.out.println(output);
                //doc.getElementsByTagName("env:Body").item(0).getTextContent();
                System.out.println(doc.getElementsByTagName("P_PERSON_ID").item(0).getTextContent());
                System.out.println(doc.getElementsByTagName("P_GRADE").item(0).getTextContent());
//                String xx =doc.getElementsByTagName("P_GRADE").item(0).getTextContent()
                 emp.setPersonId(doc.getElementsByTagName("P_PERSON_ID").item(0).getTextContent());
                 emp.setGrade(doc.getElementsByTagName("P_GRADE").item(0).getTextContent());//P_FULL_NAME
                 emp.setDisplayName(doc.getElementsByTagName("P_FULL_NAME").item(0).getTextContent());//P_EMPLOYEE_NUMBER
                 emp.setFirstName(doc.getElementsByTagName("P_FULL_NAME").item(0).getTextContent());
                 
                 emp.setPersonNumber(doc.getElementsByTagName("P_EMPLOYEE_NUMBER").item(0).getTextContent()); 
                emp.setNationalId(doc.getElementsByTagName("P_NATIONAL_IDENTIFIER").item(0).getTextContent()); 
                emp.setDateOfBirth(doc.getElementsByTagName("P_BIRTH_DATE").item(0).getTextContent()); 
                emp.setHireDate(doc.getElementsByTagName("P_HIRE_DATE").item(0).getTextContent()); 
                emp.setGradeId(doc.getElementsByTagName("P_GRADE_ID").item(0).getTextContent());
                emp.setOrganizationName(doc.getElementsByTagName("P_ORG_NAME_AR").item(0).getTextContent());
                emp.setPositionId(doc.getElementsByTagName("P_POSITION_ID").item(0).getTextContent());
                emp.setPositionName(doc.getElementsByTagName("P_NAME").item(0).getTextContent());
                emp.setEmployeeLocationId(doc.getElementsByTagName("P_LOCATION_ID").item(0).getTextContent());
                emp.setEmployeeLocation(doc.getElementsByTagName("P_LOCATION_NAME_AR").item(0).getTextContent());
                emp.setMaritalStatus(doc.getElementsByTagName("P_MARITAL_STATUS").item(0).getTextContent());
                emp.setJobId(doc.getElementsByTagName("P_JOB_ID").item(0).getTextContent());
                emp.setJobName(doc.getElementsByTagName("P_JOB_NAME_AR").item(0).getTextContent());
//                emp.setAssignmentStatus(doc.getElementsByTagName("P_JOB_NAME_AR").item(0).getTextContent());
               
            }

            return emp;
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }

        return emp;
    }

    public static void main(String[] args) throws NoSuchAlgorithmException {
//        new EmployeeDetails().getEmpDetails("6415", null, null, true);
new EmployeeDetails().employeeByUserName();
    }




    public static Map<String, String> getEmployeeDetails(String username) {
        try {
            AppsproConnection.LOG.info("Invoke service using direct HTTP call with Basic Auth");
            String payload =
                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/hcm/people/roles/userDetailsServiceV2/types/\">\n" +
                "   <soapenv:Header/>\n" +
                "   <soapenv:Body>\n" +
                "      <typ:findUserDetailsByUsername>\n" +
                "         <typ:Username>" + username + "</typ:Username>\n" +
                "      </typ:findUserDetailsByUsername>\n" +
                "   </soapenv:Body>\n" +
                "</soapenv:Envelope>";
            Document doc =
                new RestHelper().httpPost(new RestHelper().getInstanceUrl() +
                                          "/hcmService/UserDetailsServiceV2?invoke=",
                                          payload);

            doc.getDocumentElement().normalize();
            Element root = doc.getDocumentElement();
            Map<String, String> map = new HashMap<String, String>();
         if(root!=null){
             if(root.getElementsByTagName("ns1:LegalEntityName").item(0)!=null){
            if(root.getElementsByTagName("ns1:LegalEntityName").item(0).getTextContent()!=null){
                map.put("LegalEntityName",
                        root.getElementsByTagName("ns1:LegalEntityName").item(0).getTextContent());
            }
          
            map.put("GradeName",
                    root.getElementsByTagName("ns1:GradeName").item(0).getTextContent());
            map.put("JobName",
                    root.getElementsByTagName("ns1:JobName").item(0).getTextContent());
            map.put("DepartmentName",
                    root.getElementsByTagName("ns1:DepartmentName").item(0).getTextContent());
            map.put("PositionName",
                    root.getElementsByTagName("ns1:PositionName").item(0).getTextContent());
            map.put("ManagerId",
                    root.getElementsByTagName("ns1:ManagerId").item(0).getTextContent());
            map.put("ManagerName",
                    root.getElementsByTagName("ns1:ManagerName").item(0).getTextContent());
            map.put("LocationName",
                     root.getElementsByTagName("ns1:LocationName").item(0).getTextContent());
            }
            }
            return map;
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }

        return null;
    }

    public static Map<String, String> getEmployeeDetailsByPersonId(String personID) {
        try {
            AppsproConnection.LOG.info("Invoke service using direct HTTP call with Basic Auth");
            String payload =
                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/hcm/people/roles/userDetailsServiceV2/types/\">\n" +
                "   <soapenv:Header/>\n" +
                "   <soapenv:Body>\n" +
                "      <typ:findUserDetailsByPersonId>\n" +
                "         <typ:PersonId>" + personID + "</typ:PersonId>\n" +
                "      </typ:findUserDetailsByPersonId>\n" +
                "   </soapenv:Body>\n" +
                "</soapenv:Envelope>";
            Document doc =
                new RestHelper().httpPost(new RestHelper().getInstanceUrl() +
                                          "/hcmService/UserDetailsServiceV2?invoke=",
                                          payload);

            doc.getDocumentElement().normalize();
            Element root = doc.getDocumentElement();
            Map<String, String> map = new HashMap<String, String>();

            map.put("LegalEntityName",
                    root.getElementsByTagName("ns1:LegalEntityName").item(0).getTextContent());
            map.put("GradeName",
                    root.getElementsByTagName("ns1:GradeName").item(0).getTextContent());
            map.put("JobName",
                    root.getElementsByTagName("ns1:JobName").item(0).getTextContent());
            map.put("DepartmentName",
                    root.getElementsByTagName("ns1:DepartmentName").item(0).getTextContent());
            map.put("PositionName",
                    root.getElementsByTagName("ns1:PositionName").item(0).getTextContent());
            map.put("ManagerId",
                    root.getElementsByTagName("ns1:ManagerId").item(0).getTextContent());
            map.put("ManagerName",
                    root.getElementsByTagName("ns1:ManagerName").item(0).getTextContent());
            map.put("LocationName",
                    root.getElementsByTagName("ns1:LocationName").item(0).getTextContent());

            return map;
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }

        return null;
    }

}
