/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.dao;

//import com.appspro.db.ConnectionPoolNew;


import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.JobBean;

import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


/**
 *
 * @author amro
 */
public class JobDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();

    public JobBean insertOrUpdateJob(JobBean bean, String transactionType) {
        try {
            connection = AppsproConnection.getConnection();
            if (!bean.getEndDate().isEmpty()) {
                bean.setEndDate(rh.convertToGregorian(bean.getEndDate()));
            }
            if (!bean.getStartDate().isEmpty()) {
                bean.setStartDate(rh.convertToGregorian(bean.getStartDate()));
            }
            String newFlag = "Y";

            if (transactionType.equals("ADD")) {

                String query =
                    "SELECT  " + " " + getSchema_Name() + ".JOB_CODE_SEQ.NEXTVAL  from dual";
                ps = connection.prepareStatement(query);
                rs = ps.executeQuery();
                Integer jobCode = 0;
                if (rs.next()) {
                    jobCode = rs.getInt(1);
                    //("jobCode " + jobCode);

                }

                query =
                        "INSERT INTO  " + " " + getSchema_Name() + ".XXX_JOB (NAME,START_DATE,END_DATE,JOB_TYPE,GENERAL_GROUP,GROUP_TYPE ,CODE,SERIES_CATEGORIES,CREATEDBY,CREATEDDATE,FLAG)" +
                        " VALUES (?, ?, ? ,? ,?, ? ,?,? ,?,?,?)";

                ps = connection.prepareStatement(query);
                ps.setString(1, bean.getJobName());
                ps.setDate(2, getSQLDateFromString(bean.getStartDate()));
                ps.setDate(3, getSQLDateFromString(bean.getEndDate()));
                ps.setString(4, bean.getJobClassificationType());
                ps.setString(5, bean.getGeneralGroup());
                ps.setString(6, bean.getGroupType());
                ps.setInt(7, jobCode);
                ps.setString(8, bean.getSeriesCategory());
                ps.setString(9, bean.getCreatedBy());
                ps.setDate(10, getSQLDateFromString(bean.getCreationDate()));
                ps.setString(11, newFlag);
                //                ps.setString(11, bean.getLastUpdatedBy());
                //                ps.setDate(12, getSQLDateFromString(bean.getLastUpdatedDate()));
                ps.executeUpdate();

            } else if (transactionType.equals("EDIT")) {

                String oldFlag = "N";
                String query =
                    "UPDATE  " + " " + getSchema_Name() + ".XXX_JOB \n" +
                    "SET  UPDATEDATE =sysdate \n" +
                    ",END_DATE = sysdate " + ",FLAG = ?" + ", UPDATEBY = ? " +
                    "WHERE id = ? ";
                ps = connection.prepareStatement(query);
                ps.setString(1, oldFlag);
                ps.setString(2, bean.getLastUpdatedBy());
                ps.setInt(3, Integer.parseInt(bean.getJobId()));
                ps.executeUpdate();

                query =
                        "INSERT INTO  " + " " + getSchema_Name() + ".XXX_JOB (NAME,START_DATE,END_DATE,JOB_TYPE,GENERAL_GROUP,GROUP_TYPE ,CODE,SERIES_CATEGORIES,CREATEDBY,CREATEDDATE,FLAG)" +
                        " VALUES ( ? ,? ,?, ? ,?,? ,?,?,?,?,? )";

                ps = connection.prepareStatement(query);
                ps.setString(1, bean.getJobName());
                ps.setDate(2, getSQLDateFromString(bean.getStartDate()));
                ps.setDate(3, getSQLDateFromString(bean.getEndDate()));
                ps.setString(4, bean.getJobClassificationType());
                ps.setString(5, bean.getGeneralGroup());
                ps.setString(6, bean.getGroupType());
                ps.setInt(7, bean.getCode());
                ps.setString(8, bean.getSeriesCategory());
                ps.setString(9, bean.getCreatedBy());
                ps.setDate(10, getSQLDateFromString(bean.getCreationDate()));
                ps.setString(11, newFlag);
                //                ps.setString(11, bean.getLastUpdatedBy());
                //                ps.setDate(12, getSQLDateFromString(bean.getLastUpdatedDate()));
                ps.executeUpdate();

            } else if (transactionType.equals("CORRECTION")) {
                String query =
                    "UPDATE  " + " " + getSchema_Name() + ".XXX_JOB \n" +
                    "SET  UPDATEDATE =sysdate \n" +
                    ",END_DATE = sysdate " + ",FLAG = ?" + ", UPDATEBY = ? " +
                    ", JOB_TYPE = ? " + ", GENERAL_GROUP = ? " +
                    ", GROUP_TYPE = ? " + ", SERIES_CATEGORIES = ? " +
                    "WHERE id = ? ";
                ps = connection.prepareStatement(query);
                ps.setString(1, bean.getFlag());
                ps.setString(2, bean.getLastUpdatedBy());
                ps.setString(3, bean.getJobClassificationType());
                ps.setString(4, bean.getGeneralGroup());
                ps.setString(5, bean.getGroupType());
                ps.setString(6, bean.getSeriesCategory());
                ps.setInt(7, Integer.parseInt(bean.getJobId()));
                ps.executeUpdate();
            } else if (transactionType.equals("Update_Change_Insert")) {
                updateChangeInsert(bean);
            } ///////added///////
            else if (transactionType.equals("UPDATE_OVERRIDE")) {

                updateOverride(bean);

            } //added

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }

    public JobBean updateOverride(JobBean bean) {

        try {
            connection = AppsproConnection.getConnection();
            //            if (!bean.getEndDate().isEmpty()) {
            //                bean.setEndDate(rh.convertToGregorian(bean.getEndDate()));
            //            }
            //            if (!bean.getStartDate().isEmpty()) {
            //                bean.setStartDate(rh.convertToGregorian(bean.getStartDate()));
            //            }
            String query =
                "delete from  " + " " + getSchema_Name() + ".xxx_JOB   where ID!=? and code=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, bean.getJobId());
            ps.setInt(2, bean.getCode());
            ps.executeUpdate();

            query =
                    "INSERT INTO  " + " " + getSchema_Name() + ".XXX_JOB (NAME,START_DATE,END_DATE,JOB_TYPE,GENERAL_GROUP,GROUP_TYPE ,CODE,SERIES_CATEGORIES,CREATEDBY,CREATEDDATE,FLAG)" +
                    " VALUES (?, ?, ? ,? ,?, ? ,?,? ,?,?,? )";

            ps = connection.prepareStatement(query);
            ps.setString(1, bean.getJobName());
            ps.setDate(2, getSQLDateFromString(bean.getStartDate()));
            ps.setDate(3, getSQLDateFromString(bean.getEndDate()));
            ps.setString(4, bean.getJobClassificationType());
            ps.setString(5, bean.getGeneralGroup());
            ps.setString(6, bean.getGroupType());
            ps.setInt(7, bean.getCode());
            ps.setString(8, bean.getSeriesCategory());
            ps.setString(9, bean.getCreatedBy());
            ps.setDate(10, getSQLDateFromString(bean.getCreationDate()));
            ps.setString(11, "Y");
            ps.executeUpdate();

            query =
                    " update  " + " " + getSchema_Name() + ".xxx_JOB  SET End_DATE=? WHERE  ID=?";
            ps = connection.prepareStatement(query);
            String dt = bean.getStartDate(); // Start date
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Calendar ca = Calendar.getInstance();
            ca.setTime(sdf.parse(dt));
            ca.add(Calendar.DATE, -1); // number of days to add
            dt = sdf.format(ca.getTime());
            ps.setDate(1, getSQLDateFromString(dt));
            ps.setString(2, bean.getJobId());
            ps.executeUpdate();

            //              ps.executeUpdate();
            //
            //                 query = "        delete from  "+ " " + getSchema_Name() + ".xxx_JOB JOB\n" +
            //			   " where START_DATE > ?  ";
            //                ps = connection.prepareStatement(query);
            //                ps.setDate(1, getSQLDateFromString(bean.getStartDate()));
            //               ps.executeUpdate();
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return bean;
    }

    public JobBean updateChangeInsert(JobBean bean) {
        try {
            //                   bean.setEndDate(getFutureEndData(bean));
            connection = AppsproConnection.getConnection();
            //             if(!bean.getEndDate().isEmpty()){
            //                  bean.setEndDate(rh.convertToGregorian(bean.getEndDate()));
            //                }
            //                 if(!bean.getStartDate().isEmpty()){
            //                  bean.setStartDate(rh.convertToGregorian(bean.getStartDate()));
            //                }
            int checkcout = 0;
            String query =
                "select count(*) from  " + " " + getSchema_Name() + ".XXX_JOB  where code=? and ID>?";
            ps = connection.prepareStatement(query);
            ps.setInt(1, bean.getCode());
            ps.setString(2, bean.getJobId());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                checkcout = rs.getInt(1);
            }
            if (checkcout == 0) {

            } else {

                //("result of check led to saddasdwedsDSS  continue");
                //("id value ==" + bean.getJobId());
                int jobId = (Integer.parseInt(bean.getJobId())) + 1;

                //("id value ==" + jobId);

                query =
                        "update  " + " " + getSchema_Name() + ".XXX_JOB set START_DATE=? WHERE ID=? ";
                ps = connection.prepareStatement(query);
                String dt = bean.getEndDate(); // Start date
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Calendar ca = Calendar.getInstance();
                ca.setTime(sdf.parse(dt));
                ca.add(Calendar.DATE, 1); // number of days to add
                dt = sdf.format(ca.getTime());
                ps.setDate(1, getSQLDateFromString(dt));

                ps.setInt(2, jobId);
                ps.executeUpdate();
            }

            
            query =
                    "INSERT INTO  " + " " + getSchema_Name() + ".XXX_JOB (NAME,START_DATE,END_DATE,JOB_TYPE,GENERAL_GROUP,GROUP_TYPE ,CODE,SERIES_CATEGORIES,CREATEDBY,CREATEDDATE,FLAG)" +
                    " VALUES (?, ?, ? ,? ,?, ? ,?,? ,?,?,? )";

            ps = connection.prepareStatement(query);
            ps.setString(1, bean.getJobName());
            ps.setDate(2, getSQLDateFromString(bean.getStartDate()));
            ps.setDate(3, getSQLDateFromString(bean.getEndDate()));
            ps.setString(4, bean.getJobClassificationType());
            ps.setString(5, bean.getGeneralGroup());
            ps.setString(6, bean.getGroupType());
            ps.setInt(7, bean.getCode());
            ps.setString(8, bean.getSeriesCategory());
            ps.setString(9, bean.getCreatedBy());
            ps.setDate(10, getSQLDateFromString(bean.getCreationDate()));
            ps.setString(11, "Y");
            //                ps.setString(11, bean.getLastUpdatedBy());
            //                ps.setDate(12, getSQLDateFromString(bean.getLastUpdatedDate()));
            ps.executeUpdate();

            query =
                    " update  " + " " + getSchema_Name() + ".xxx_JOB  SET End_DATE=? WHERE  ID=?";
            ps = connection.prepareStatement(query);
            String dt = bean.getStartDate(); // Start date
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Calendar ca = Calendar.getInstance();
            ca.setTime(sdf.parse(dt));
            ca.add(Calendar.DATE, -1); // number of days to add
            dt = sdf.format(ca.getTime());
            ps.setDate(1, getSQLDateFromString(dt));
            ps.setString(2, bean.getJobId());
            ps.executeUpdate();

        } catch (Exception e) {

            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return bean;
    }

    public String getFutureEndData(JobBean bean) {
        String effEndDate = "";
        String EndDateAfterAdd = "";
        try {

            connection = AppsproConnection.getConnection();
            String query =
                " select  End_DATE from  " + " " + getSchema_Name() +
                ".xxx_JOB \n" +
                "			    where CODE = ? and  ID=? ";

            ps = connection.prepareStatement(query);
            ps.setInt(1, bean.getCode());
            ps.setString(2, bean.getJobId());
            //            ps.setString(3, bean.getStartDate());
            rs = ps.executeQuery();
            while (rs.next()) {
                effEndDate = rs.getString("START_DATE");
                String c = rs.getString(1);
                String dt = effEndDate; // Start date
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Calendar ca = Calendar.getInstance();
                ca.setTime(sdf.parse(dt));
                ca.add(Calendar.DATE, 1); // number of days to add
                dt = sdf.format(ca.getTime());
                EndDateAfterAdd = dt;
            }

            rs.close();
            ps.close();
            connection.close();
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            //   array.add(obj);
        } finally {
            closeResources(connection, ps, rs);
        }
        return EndDateAfterAdd;
    }

    public JobBean updateJob(JobBean bean) {
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "UPDATE  " + " " + getSchema_Name() + ".XXX_JOB \n" +
                "SET START_DATE =? \n" +
                ", END_DATE =? \n" +
                ", JOB_TYPE = ? \n" +
                ", GENERAL_GROUP = ? \n" +
                ", GROUP_TYPE= ? \n" +
                ", SERIES_CATEGORIES = ? \n" +
                "WHERE id = ? ";
            ps = connection.prepareStatement(query);
            ps.setDate(1, getSQLDateFromString(bean.getStartDate()));
            ps.setDate(2, getSQLDateFromString(bean.getEndDate()));
            ps.setString(3, bean.getJobClassificationType());
            ps.setString(4, bean.getGeneralGroup());
            ps.setString(5, bean.getGroupType());
            ps.setString(6, bean.getSeriesCategory());
            ps.setInt(7, Integer.parseInt(bean.getJobId()));
            ps.executeUpdate();

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }

    public JobBean deleteJob(JobBean bean) {
        try {
            connection = AppsproConnection.getConnection();

            String query = "BEGIN\n" +
                "  UPDATE  " + " " + getSchema_Name() + ".ABOUT_ME\n" +
                "                    SET ABOUT_ME = ?,\n" +
                "                    UPDATED_BY = ?,\n" +
                "                    UPDATED_DATE = sysdate\n" +
                "                     WHERE PERSON_ID= ?;\n" +
                "                     COMMIT;\n" +
                "END;";

            ps = connection.prepareStatement(query);
            //            ps.setString(1, aboutMeBean.getAboutMe());
            //            ps.setString(2, aboutMeBean.getPersonId());
            //            ps.setString(3, aboutMeBean.getPersonId());

            rs = ps.executeQuery();

            rs.close();
            ps.close();
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }

    public ArrayList<JobBean> searchJobs(String jobName,
                                         String jobClassificationType,
                                         String generalGroup,
                                         String qualityGroup,
                                         String seriesCategory) {
        ArrayList<JobBean> jobsList = new ArrayList<JobBean>();
        if (jobName.equals("undefined")) {
            jobName = "";
        }
        if (jobClassificationType.equals("undefined")) {
            jobClassificationType = "";
        }
        if (generalGroup.equals("undefined")) {
            generalGroup = "";
        }
        if (qualityGroup.equals("undefined")) {
            qualityGroup = "";
        }
        if (seriesCategory.equals("undefined")) {
            seriesCategory = "";
        }
        try {
            connection = AppsproConnection.getConnection();
            String query =
                " select * from  " + " " + getSchema_Name() + ".XXX_JOB jobs where   jobs.NAME = NVL(?, jobs.NAME) AND\n" +
                "jobs.JOB_TYPE = NVL(?, jobs.JOB_TYPE) AND\n" +
                "jobs.GENERAL_GROUP = NVL(?, jobs.GENERAL_GROUP) AND\n" +
                "jobs.GROUP_TYPE = NVL(?, jobs.GROUP_TYPE) AND\n" +
                "jobs.SERIES_CATEGORIES = NVL(?, jobs.SERIES_CATEGORIES)";

            ps = connection.prepareStatement(query);
            ps.setString(1, jobName);
            ps.setString(2, jobClassificationType);
            ps.setString(3, generalGroup);
            ps.setString(4, qualityGroup);
            ps.setString(5, seriesCategory);
            rs = ps.executeQuery();
            while (rs.next()) {
                JobBean jobBean = new JobBean();
                jobBean.setJobName(rs.getString("NAME"));
                jobBean.setCreatedBy(rs.getString("CREATEDBY"));
                jobBean.setGeneralGroup(rs.getString("GENERAL_GROUP"));
                jobBean.setCode(rs.getInt("CODE"));
                jobBean.setCreationDate(rs.getString("CREATEDDATE"));
                jobBean.setGroupType(rs.getString("GROUP_TYPE"));
                jobBean.setJobClassificationType(rs.getString("JOB_TYPE"));
                jobBean.setJobId(rs.getString("ID"));
                jobBean.setLastUpdatedBy(rs.getString("UPDATEBY"));
                jobBean.setSeriesCategory(rs.getString("SERIES_CATEGORIES"));
                jobBean.setStartDateGregorian(rs.getString("START_DATE"));
                //get hijri start date and end date
                if (rs.getString("END_DATE") != null) {
                    jobBean.setEndDate(rh.convertToHijri(rs.getString("END_DATE")));
                } else {
                    jobBean.setEndDate((rs.getString("END_DATE")));
                }
                if (rs.getString("START_DATE") != null) {
                    jobBean.setStartDate(rh.convertToHijri(rs.getString("START_DATE")));
                } else {
                    jobBean.setStartDate((rs.getString("START_DATE")));
                } //end
                jobBean.setLastUpdatedDate(rs.getString("UPDATEDATE"));
                jobsList.add(jobBean);

            }

            rs.close();
            ps.close();
            connection.close();
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            //   array.add(obj);
        } finally {
            closeResources(connection, ps, rs);
        }

        return jobsList;
    }

    public ArrayList<JobBean> getAllJobs() {
        ArrayList<JobBean> jobsList = new ArrayList<JobBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                " select  * from  " + " " + getSchema_Name() + ".XXX_JOB ";

            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                JobBean jobBean = new JobBean();
                jobBean.setJobName(rs.getString("NAME"));
                jobBean.setCreatedBy(rs.getString("CREATEDBY"));
                jobBean.setGeneralGroup(rs.getString("GENERAL_GROUP"));
                jobBean.setCode(rs.getInt("CODE"));
                jobBean.setCreationDate(rs.getString("CREATEDDATE"));
                jobBean.setGroupType(rs.getString("GROUP_TYPE"));
                jobBean.setJobClassificationType(rs.getString("JOB_TYPE"));
                jobBean.setJobId(rs.getString("ID"));
                jobBean.setLastUpdatedBy(rs.getString("UPDATEBY"));
                jobBean.setSeriesCategory(rs.getString("SERIES_CATEGORIES"));
                jobBean.setLastUpdatedDate(rs.getString("UPDATEDATE"));
                //get hijri start date and end date
                if (rs.getString("END_DATE") != null) {
                    jobBean.setEndDate(rh.convertToHijri(rs.getString("END_DATE")));
                } else {
                    jobBean.setEndDate((rs.getString("END_DATE")));
                }
                if (rs.getString("START_DATE") != null) {
                    jobBean.setStartDate(rh.convertToHijri(rs.getString("START_DATE")));
                } else {
                    jobBean.setStartDate((rs.getString("START_DATE")));
                } //end
                jobsList.add(jobBean);
            }

            rs.close();
            ps.close();
            connection.close();
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            //   array.add(obj);
        } finally {
            closeResources(connection, ps, rs);
        }

        return jobsList;
    }

    public java.sql.Date getSQLDateFromString(String date) {
        java.sql.Date sqlDate = null;

        try {
            Date utilDate = new SimpleDateFormat("yyyy-MM-dd").parse(date);
            sqlDate = new java.sql.Date(utilDate.getTime());
        } catch (Exception ex) {
           ex.printStackTrace(); AppsproConnection.LOG.error("ERROR", ex);
        }
        return sqlDate;
    }

    public int getJobCount(String generalGroup, String groupType,
                           String jobClassificationType, String seriesCategory,
                           int jobId) {
        int count = -1;
        //("jobClassificationType" + jobClassificationType);
        //("generalGroup" + generalGroup);
        //("groupType" + groupType);
        //("seriesCategory" + seriesCategory);
        //("id " + jobId);
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "        select  count(*) from  " + " " + getSchema_Name() +
                ".XXX_JOB JOB\n" +
                "                where JOB.JOB_TYPE =? \n" +
                "                AND JOB.GENERAL_GROUP = ? \n" +
                "                AND JOB.GROUP_TYPE =? \n" +
                "                AND JOB.SERIES_CATEGORIES =? \n" +
                "                AND   JOB.ID <> ? ";
            ps = connection.prepareStatement(query);
            ps.setString(1, jobClassificationType);
            ps.setString(2, generalGroup);
            ps.setString(3, groupType);
            ps.setString(4, seriesCategory);
            ps.setInt(5, jobId);
            rs = ps.executeQuery();
            while (rs.next()) {
                //(rs.getString(1));
                String c = rs.getString(1);
                count = Integer.parseInt(c);
            }

            rs.close();
            ps.close();
            connection.close();
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            //   array.add(obj);
        } finally {
            closeResources(connection, ps, rs);
        }
        return count;
    }

    public int getFutureCount(int code, String startDate) {

        int count = -1;
        //("code????????? " + code);
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "        select  count(*) from  " + " " + getSchema_Name() +
                ".XXX_JOB JOB\n" +
                "                where JOB.CODE =? \n" +
                "AND JOB.START_DATE >? ";
            ps = connection.prepareStatement(query);
            //            ps.setString(1, jobClassificationType);
            //            ps.setString(2, generalGroup);
            //            ps.setString(3, groupType);
            ps.setInt(1, code);
            ps.setDate(2, getSQLDateFromString((startDate)));

            rs = ps.executeQuery();
            while (rs.next()) {
                String c = rs.getString(1);
                count = Integer.parseInt(c);

            }

            rs.close();
            ps.close();
            connection.close();
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            //   array.add(obj);
        } finally {
            closeResources(connection, ps, rs);
        }
        return count;
    }

    public ArrayList<JobBean> searchJobs(String jobName) {
        ArrayList<JobBean> jobsList = new ArrayList<JobBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                " select * from  " + " " + getSchema_Name() + ".XXX_JOB jobs \n" +
                "where   jobs.NAME = ? ";

            ps = connection.prepareStatement(query);
            ps.setString(1, jobName);
            rs = ps.executeQuery();
            while (rs.next()) {
                JobBean jobBean = new JobBean();
                jobBean.setJobName(rs.getString("NAME"));
                jobBean.setCreatedBy(rs.getString("CREATEDBY"));
                jobBean.setGeneralGroup(rs.getString("GENERAL_GROUP"));
                jobBean.setCode(rs.getInt("CODE"));
                jobBean.setCreationDate(rs.getString("CREATEDDATE"));
                jobBean.setGroupType(rs.getString("GROUP_TYPE"));
                jobBean.setJobClassificationType(rs.getString("JOB_TYPE"));
                jobBean.setJobId(rs.getString("ID"));
                jobBean.setLastUpdatedBy(rs.getString("UPDATEBY"));
                jobBean.setSeriesCategory(rs.getString("SERIES_CATEGORIES"));
                jobBean.setLastUpdatedDate(rs.getString("UPDATEDATE"));
                //get hijri start date and end date
                if (rs.getString("END_DATE") != null) {
                    jobBean.setEndDate(rh.convertToHijri(rs.getString("END_DATE")));
                } else {
                    jobBean.setEndDate((rs.getString("END_DATE")));
                }
                if (rs.getString("START_DATE") != null) {
                    jobBean.setStartDate(rh.convertToHijri(rs.getString("START_DATE")));
                } else {
                    jobBean.setStartDate((rs.getString("START_DATE")));
                } //end
                jobsList.add(jobBean);

            }

            rs.close();
            ps.close();
            connection.close();
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            //   array.add(obj);
        } finally {
            closeResources(connection, ps, rs);
        }

        return jobsList;
    }
}
