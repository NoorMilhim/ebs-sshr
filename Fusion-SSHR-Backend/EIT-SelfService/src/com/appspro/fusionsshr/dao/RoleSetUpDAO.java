package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.RoleSetUpBean;
import com.appspro.fusionsshr.bean.SummaryReportBean;

import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class RoleSetUpDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    Integer roleSetupId = 0;
    
    
    
    public void getsequnce(){
            try {
                connection = AppsproConnection.getConnection();
                String query="SELECT  XXX_ROLESETUP_SEQ.NEXTVAL  from dual";
                ps = connection.prepareStatement(query);
                rs = ps.executeQuery();
              
                if (rs.next()) {
                    roleSetupId = rs.getInt(1);
                    //("roleSetupId " + roleSetupId);
                } 
            }
            catch (Exception e) {
                        //("Error: ");
                       e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
                    } finally {
                        closeResources(connection, ps, rs);
                    } 
                
                
        }
    
    

    
    public RoleSetUpBean addRoleSetUp(RoleSetUpBean roleSetupBean) {
     
        int nextNum = getNextNumber("XXX_ROLESETUP", "ID_ROLE");
        try {
            connection = AppsproConnection.getConnection();
            String query="insert into  " + " " + getSchema_Name() + ".XXX_ROLESETUP (ID_ROLE, ROLEDESCRIPTION,ROLENAME)VALUES (?,?,?)";
            ps = connection.prepareStatement(query);
            ps.setInt(1, nextNum);
            ps.setString(2, roleSetupBean.getRoleDescription());
            ps.setString(3, roleSetupBean.getRoleName()); 
            
             int status  = ps.executeUpdate();
            ArrayList<String> arr=roleSetupBean.getEitCode();
            for (int i = 0; i < arr.size() && status == 1; i++) {
                query ="insert into " + getSchema_Name() + ".XXX_ROLESETUPEIT(EITCODE,ROLESETUP_ID) values(?,?)";
                ps = connection.prepareStatement(query);
                ps.setString(1, arr.get(i));
                ps.setInt(2, nextNum);
                ps.executeUpdate();
            }
                  
             
            
           

        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return roleSetupBean;
    }
    
    
    public RoleSetUpBean UpdateRoleSetUp(RoleSetUpBean roleSetupBean) {
     
 
        try {
            connection = AppsproConnection.getConnection();
           String   query="update" + " " + getSchema_Name() + ". XXX_ROLESETUP set ROLEDESCRIPTION=? ,ROLENAME=? where ID_ROLE=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, roleSetupBean.getRoleDescription());
            ps.setString(2, roleSetupBean.getRoleName());
            ps.setString(3, roleSetupBean.getRoleId());
            ps.executeUpdate();
            
            
           query= "delete from " + " " + getSchema_Name() + ".XXX_ROLESETUPEIT where ROLESETUP_ID =?" ;
            ps = connection.prepareStatement(query);
            ps.setString(1, roleSetupBean.getRoleId());
            ps.executeUpdate();
            
            
            ps.executeUpdate();
            ArrayList<String> arr;
            arr=roleSetupBean.getEitCode();
            for (int i = 0; i < arr.size(); i++) {

     
               query =
                       "insert into  " + " " + getSchema_Name() + ".XXX_ROLESETUPEIT(EITCODE,ROLESETUP_ID) values(?,?)";
               ps = connection.prepareStatement(query);
               ps.setString(1, arr.get(i));
               ps.setString(2, roleSetupBean.getRoleId());
               ps.executeUpdate();

            }
            
            

            
           
            
           

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return roleSetupBean;
    }
    
    
    
    
    
    
    
    
    
    
    public ArrayList<RoleSetUpBean> getRoleSetup() {

        ArrayList<RoleSetUpBean> roleSetList =
            new ArrayList<RoleSetUpBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "select ID_ROLE,ROLEDESCRIPTION,ROLENAME  from" + " " + getSchema_Name() + ". XXX_ROLESETUP";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                RoleSetUpBean roleSetup = new RoleSetUpBean();
                roleSetup.setRoleId(rs.getString("ID_ROLE"));
                roleSetup.setRoleDescription(rs.getString("ROLEDESCRIPTION"));
                roleSetup.setRoleName(rs.getString("ROLENAME"));
                roleSetList.add(roleSetup);
 
            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return roleSetList;

    }
 
    public ArrayList<RoleSetUpBean> getAllRoleName() {

        ArrayList<RoleSetUpBean> roleSetupList =
            new ArrayList<RoleSetUpBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "select ID_ROLE ,ROLENAME from" + " " + getSchema_Name() + ".XXX_ROLESETUP";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                RoleSetUpBean roleSetupBean = new RoleSetUpBean();
                roleSetupBean.setId(rs.getString("ID_ROLE"));
                roleSetupBean.setRoleName(rs.getString("ROLENAME"));
                roleSetupList.add(roleSetupBean);

            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return roleSetupList;

    }
    

    public String deleteRoleSetUp(String id) {

    connection = AppsproConnection.getConnection();
    String query;
    int checkResult=0;

    try {
    query = "DELETE FROM" + " " + getSchema_Name() + ". XXX_ROLESETUP WHERE ID_ROLE=?";
    ps = connection.prepareStatement(query);
    ps.setInt(1, Integer.parseInt(id));
    checkResult = ps.executeUpdate();
    
        query = "DElete from" + " " + getSchema_Name() + ". XXX_ROLESETUPEIT WHERE ROLESETUP_ID=?";
        ps = connection.prepareStatement(query);
        ps.setInt(1, Integer.parseInt(id));
        checkResult = ps.executeUpdate();
    
    
    
    
    } catch (Exception e) {
    //("Error: ");
   e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
    } finally {
    closeResources(connection, ps, rs);
    }
    return Integer.toString(checkResult);
    }
    
    public ArrayList<RoleSetUpBean> searchRoleSetup(RoleSetUpBean bean) {
        ArrayList<RoleSetUpBean> roleList =
            new ArrayList<RoleSetUpBean>();
        RoleSetUpBean roleBean = null;
        try {
            connection = AppsproConnection.getConnection();
            String query =
                " SELECT * FROM  " + " " + getSchema_Name() + ".XXX_ROLESETUP reports WHERE LOWER(ROLENAME) LIKE LOWER(?)";

            ps = connection.prepareStatement(query);
            //            ps.setString(1, "%" + eitCode + "%");
            ps.setString(1, "%" + bean.getRoleName() + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                roleBean = new RoleSetUpBean();
                roleBean.setRoleId(rs.getString("ID_ROLE"));
                roleBean.setRoleDescription(rs.getString("ROLEDESCRIPTION"));
                roleBean.setRoleName(rs.getString("ROLENAME"));
                roleList.add(roleBean);

            }

            rs.close();
            ps.close();
            connection.close();
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);

        } finally {
            closeResources(connection, ps, rs);
        }

        return roleList;
    }
    

    public static void main(String[] args) {
        RoleSetUpBean roleSetupBean = new RoleSetUpBean();
        roleSetupBean.setRoleDescription("test1");
        roleSetupBean.setRoleName("test1");
        roleSetupBean.getEitCode().clear();
        roleSetupBean.getEitCode().add("XXX_HR_BUSINESS_TRAVEL");
        roleSetupBean.getEitCode().add("XXX_HR_BUSINESS_PAYMENT");
       new RoleSetUpDAO().addRoleSetUp(roleSetupBean);
    }
       
}
