/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.dao;


import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.SummaryReportBean;
import com.appspro.fusionsshr.bean.ValidationBean;

import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;


/**
 *
 * @author lenovo
 */
public class ValidationDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();

    //       public  ValidationBean addValidation(ValidationBean validationBean){
    //
    //         try {
    //            connection=AppsproConnection.getConnection();
    //
    //             String query = "BEGIN\n"
    //                     + "\n"
    //                     + "INSERT INTO XX_MOE.XXX_VALIDATION(EIT_CODE, FIRST_KEY_TYPE, \n"
    //                     + "FIRST_KEY_VALUE, APPLICATION_VALUE_FAMILY,\n"
    //                     + "APPLICATION_KEY, SAAS_SOURCE, REPORT_NAME, \n"
    //                     + "PARAMETER1, PARAMETER2, PARAMETER3, PARAMETER4,\n"
    //                     + "PARAMETER5, SAAS_QUERY, PAAS_QUERY, OPERATION, \n"
    //                     + "SECOND_FIRST_KEY_TYPE, SECOND_FIRST_KEY_VALUE, \n"
    //                     + "SECOND_APP_VALFAM, SECOND_APPLICATION_KEY, \n"
    //                     + "SECOND_SAAS_SOURCE, SECOND_REPORT_NAME, \n"
    //                     + "SECOND_PARAMETER1, SECOND_PARAMETER2, \n"
    //                     + "SECOND_PARAMETER3, SECOND_PARAMETER4, \n"
    //                     + "SECOND_PARAMETER5, SECOND_SAAS_QUERY, \n"
    //                     + "SECOND_PAAS_QUERY, TYPE_OF_ACTION, \n"
    //                     + "ERROR_MESSAGE_EN, ERROR_MESSAGE_AR, \n"
    //                     + "SAVE_VALUE_OF_EIT, SAVE_RESULT, \n"
    //                     + "VALIDATION_VALUES_NAME, VAL) VALUES \n"
    //                     + "\n"
    //                     + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);\n"
    //                     + "\n"
    //                     + "COMMIT;\n"
    //                     + "END;";
    //            ps = connection.prepareStatement(query);
    //
    //              ps.setString(1, validationBean.getEitCode());
    //              ps.setString(2, validationBean.getFirstKeyType());
    //              ps.setString(3, validationBean.getFirstKeyValue());
    //              ps.setString(4, validationBean.getApplicationValueFamily());
    //              ps.setString(5, validationBean.getApplicationKey());
    //              ps.setString(6, validationBean.getSaaSSource());
    //              ps.setString(7, validationBean.getReportName());
    //              ps.setString(8, validationBean.getParameter1Name());
    //              ps.setString(9, validationBean.getParameter2Name());
    //              ps.setString(10, validationBean.getParameter3Name());
    //              ps.setString(11, validationBean.getParameter4Name());
    //              ps.setString(12, validationBean.getParameter5Name());
    //              ps.setString(13, validationBean.getSaaSQuery());
    //              ps.setString(14, validationBean.getPaaSQuery());
    //              ps.setString(15, validationBean.getOperation());
    //              ps.setString(16, validationBean.getSecondKeyType());
    //              ps.setString(17, validationBean.getSecondKeyValue());
    //              ps.setString(18, validationBean.getSecondapplicationValueFamily());
    //              ps.setString(19, validationBean.getSecondapplicationKey());
    //              ps.setString(20, validationBean.getSecondsaaSSource());
    //              ps.setString(21, validationBean.getSecondreportName());
    //              ps.setString(22, validationBean.getSecondparameter1Name());
    //              ps.setString(23, validationBean.getSecondparameter2Name());
    //              ps.setString(24, validationBean.getSecondparameter3Name());
    //              ps.setString(25, validationBean.getSecondparameter4Name());
    //              ps.setString(26, validationBean.getSecondparameter5Name());
    //              ps.setString(27, validationBean.getSecondsaaSQuery());
    //              ps.setString(28, validationBean.getSecondpaaSQuery());
    //              ps.setString(29, validationBean.getTypeOFAction());
    //              ps.setString(30, validationBean.getErrorMassageEn());
    //              ps.setString(31, validationBean.getErrorMassageAr());
    //              ps.setString(32, validationBean.getSetValueOfEIT());
    //              ps.setString(33, validationBean.getSaveResultToValidationsValues());
    //              ps.setString(34, validationBean.getValidationValuesName());
    //              ps.setString(35, validationBean.getValue());
    //
    //
    //
    //              ps.executeUpdate();
    //
    //        } catch (Exception e) {
    //            //("Error: ");
    //           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
    //        }
    //         return validationBean;
    //    }


    public ValidationBean insertOrUpdateValidation(ValidationBean validationBean,
                                                   String transactionType) {
        try {
            connection = AppsproConnection.getConnection();


            if (transactionType.equals("ADD")) {

                String query = "BEGIN\n" +
                    "\n" +
                    "INSERT INTO  " + " " + getSchema_Name() +
                    ".XXX_VALIDATION(EIT_CODE, FIRST_KEY_TYPE, \n" +
                    "FIRST_KEY_VALUE, APPLICATION_VALUE_FAMILY,\n" +
                    "APPLICATION_KEY, SAAS_SOURCE, REPORT_NAME, \n" +
                    "PARAMETER1, PARAMETER2, PARAMETER3, PARAMETER4,\n" +
                    "PARAMETER5, SAAS_QUERY, PAAS_QUERY, OPERATION, \n" +
                    "SECOND_FIRST_KEY_TYPE, SECOND_FIRST_KEY_VALUE, \n" +
                    "SECOND_APP_VALFAM, SECOND_APPLICATION_KEY, \n" +
                    "SECOND_SAAS_SOURCE, SECOND_REPORT_NAME, \n" +
                    "SECOND_PARAMETER1, SECOND_PARAMETER2, \n" +
                    "SECOND_PARAMETER3, SECOND_PARAMETER4, \n" +
                    "SECOND_PARAMETER5, SECOND_SAAS_QUERY, \n" +
                    "SECOND_PAAS_QUERY, TYPE_OF_ACTION, \n" +
                    "ERROR_MESSAGE_EN, ERROR_MESSAGE_AR, \n" +
                    "SAVE_VALUE_OF_EIT, SAVE_RESULT, \n" +
                    "VALIDATION_VALUES_NAME, VAL,ACTIONTYPE,NEWVALUE,EITSEGMENT,TYPE_OF_VALUE,TYPE_OF_FRIST_VALUE,FRIST_VALUES,SECOND_OPERATION,TYPE_OF_SECOND_VALUE,SECOND_VALUE) VALUES \n" +
                    "\n" +
                    "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?);\n" +
                    "\n" +
                    "COMMIT;\n" +
                    "END;";
                ps = connection.prepareStatement(query);
                ps.setString(1, validationBean.getEitCode());
                ps.setString(2, validationBean.getFirstKeyType());
                ps.setString(3, validationBean.getFirstKeyValue());
                ps.setString(4, validationBean.getApplicationValueFamily());
                ps.setString(5, validationBean.getApplicationKey());
                ps.setString(6, validationBean.getSaaSSource());
                ps.setString(7, validationBean.getReportName());
                ps.setString(8, validationBean.getParameter1Name());
                ps.setString(9, validationBean.getParameter2Name());
                ps.setString(10, validationBean.getParameter3Name());
                ps.setString(11, validationBean.getParameter4Name());
                ps.setString(12, validationBean.getParameter5Name());
                ps.setString(13, validationBean.getSaaSQuery());
                ps.setString(14, validationBean.getPaaSQuery());
                ps.setString(15, validationBean.getOperation());
                ps.setString(16, validationBean.getSecondKeyType());
                ps.setString(17, validationBean.getSecondKeyValue());
                ps.setString(18,
                             validationBean.getSecondapplicationValueFamily());
                ps.setString(19, validationBean.getSecondapplicationKey());
                ps.setString(20, validationBean.getSecondsaaSSource());
                ps.setString(21, validationBean.getSecondreportName());
                ps.setString(22, validationBean.getSecondparameter1Name());
                ps.setString(23, validationBean.getSecondparameter2Name());
                ps.setString(24, validationBean.getSecondparameter3Name());
                ps.setString(25, validationBean.getSecondparameter4Name());
                ps.setString(26, validationBean.getSecondparameter5Name());
                ps.setString(27, validationBean.getSecondsaaSQuery());
                ps.setString(28, validationBean.getSecondpaaSQuery());
                ps.setString(29, validationBean.getTypeOFAction());
                ps.setString(30, validationBean.getErrorMassageEn());
                ps.setString(31, validationBean.getErrorMassageAr());
                ps.setString(32, validationBean.getSetValueOfEIT());
                ps.setString(33,
                             validationBean.getSaveResultToValidationsValues());
                ps.setString(34, validationBean.getValidationValuesName());
                ps.setString(35, validationBean.getValue());
                ps.setString(36, validationBean.getActiontype());
                ps.setString(37, validationBean.getNewValue());
                ps.setString(38, validationBean.getEitSegment());
                ps.setString(39, validationBean.getTypeOfValue());
                ps.setString(40, validationBean.getTypeOfFristValue());
                ps.setString(41, validationBean.getFristValues());
                ps.setString(42, validationBean.getSecondOperation());
                ps.setString(43, validationBean.getTypeOfSecondValue());
                ps.setString(44, validationBean.getSecondValue());
                ps.executeUpdate();

            } else if (transactionType.equals("EDIT")) {


                String query = "BEGIN \n" +
                    "UPDATE  " + " " + getSchema_Name() +
                    ".XXX_VALIDATION SET \n" +
                    "    EIT_CODE = ?,\n" +
                    "	FIRST_KEY_TYPE = ?,\n" +
                    "	FIRST_KEY_VALUE = ?,\n" +
                    "	APPLICATION_VALUE_FAMILY = ?,\n" +
                    "	APPLICATION_KEY = ?,\n" +
                    "	SAAS_SOURCE = ?,\n" +
                    "	REPORT_NAME = ?,\n" +
                    "	PARAMETER1 = ?,\n" +
                    "	PARAMETER2 = ?,\n" +
                    "	PARAMETER3 = ?,\n" +
                    "	PARAMETER4 = ?,\n" +
                    "	PARAMETER5 = ?,\n" +
                    "	SAAS_QUERY = ?,\n" +
                    "	PAAS_QUERY = ?,\n" +
                    "	OPERATION = ?,\n" +
                    "	SECOND_FIRST_KEY_TYPE = ?,\n" +
                    "	SECOND_FIRST_KEY_VALUE = ?,\n" +
                    "	SECOND_APP_VALFAM = ?,\n" +
                    "	SECOND_APPLICATION_KEY = ?,\n" +
                    "	SECOND_SAAS_SOURCE = ?,\n" +
                    "	SECOND_REPORT_NAME = ?,\n" +
                    "	SECOND_PARAMETER1 = ?,\n" +
                    "	SECOND_PARAMETER2 = ?,\n" +
                    "	SECOND_PARAMETER3 = ?,\n" +
                    "	SECOND_PARAMETER4 = ?,\n" +
                    "	SECOND_PARAMETER5 = ?,\n" +
                    "	SECOND_SAAS_QUERY = ?,\n" +
                    "	SECOND_PAAS_QUERY = ?,\n" +
                    "	TYPE_OF_ACTION = ?,\n" +
                    "	ERROR_MESSAGE_EN = ?,\n" +
                    "	ERROR_MESSAGE_AR = ?,\n" +
                    "	SAVE_VALUE_OF_EIT = ?,\n" +
                    "	SAVE_RESULT = ?,\n" +
                    "	VALIDATION_VALUES_NAME = ?,\n" +
                    "    VAL = ?,\n" +
                    "    ACTIONTYPE=?,\n" +
                    "    NEWVALUE=?,\n" +
                    "    EITSEGMENT=?," + "    TYPE_OF_VALUE=?," +
                    "    TYPE_OF_FRIST_VALUE=?," + "    FRIST_VALUES=?," +
                    "    SECOND_OPERATION=?," + "    TYPE_OF_SECOND_VALUE=?," +
                    "    SECOND_VALUE=?\n" +
                    " WHERE ID = ?;\n" +
                    "COMMIT;\n" +
                    "END;";
                ps = connection.prepareStatement(query);

                ps.setString(1, validationBean.getEitCode());
                ps.setString(2, validationBean.getFirstKeyType());
                ps.setString(3, validationBean.getFirstKeyValue());
                ps.setString(4, validationBean.getApplicationValueFamily());
                ps.setString(5, validationBean.getApplicationKey());
                ps.setString(6, validationBean.getSaaSSource());
                ps.setString(7, validationBean.getReportName());
                ps.setString(8, validationBean.getParameter1Name());
                ps.setString(9, validationBean.getParameter2Name());
                ps.setString(10, validationBean.getParameter3Name());
                ps.setString(11, validationBean.getParameter4Name());
                ps.setString(12, validationBean.getParameter5Name());
                ps.setString(13, validationBean.getSaaSQuery());
                ps.setString(14, validationBean.getPaaSQuery());
                ps.setString(15, validationBean.getOperation());
                ps.setString(16, validationBean.getSecondKeyType());
                ps.setString(17, validationBean.getSecondKeyValue());
                ps.setString(18,
                             validationBean.getSecondapplicationValueFamily());
                ps.setString(19, validationBean.getSecondapplicationKey());
                ps.setString(20, validationBean.getSecondsaaSSource());
                ps.setString(21, validationBean.getSecondreportName());
                ps.setString(22, validationBean.getSecondparameter1Name());
                ps.setString(23, validationBean.getSecondparameter2Name());
                ps.setString(24, validationBean.getSecondparameter3Name());
                ps.setString(25, validationBean.getSecondparameter4Name());
                ps.setString(26, validationBean.getSecondparameter5Name());
                ps.setString(27, validationBean.getSecondsaaSQuery());
                ps.setString(28, validationBean.getSecondpaaSQuery());
                ps.setString(29, validationBean.getTypeOFAction());
                ps.setString(30, validationBean.getErrorMassageEn());
                ps.setString(31, validationBean.getErrorMassageAr());
                ps.setString(32, validationBean.getSetValueOfEIT());
                ps.setString(33,
                             validationBean.getSaveResultToValidationsValues());
                ps.setString(34, validationBean.getValidationValuesName());
                ps.setString(35, validationBean.getValue());
                ps.setString(36, validationBean.getActiontype());
                ps.setString(37, validationBean.getNewValue());
                ps.setString(38, validationBean.getEitSegment());
                ps.setString(39, validationBean.getTypeOfValue());
                ps.setString(40, validationBean.getTypeOfFristValue());
                ps.setString(41, validationBean.getFristValues());
                ps.setString(42, validationBean.getSecondValue());
                ps.setString(43, validationBean.getTypeOfSecondValue());
                ps.setString(44, validationBean.getSecondValue());
                ps.setString(45, validationBean.getId());

                ps.executeUpdate();


            }


        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return validationBean;
    }

    public ArrayList<ValidationBean> actionValidation(String eitCode) {
        ArrayList<ValidationBean> validationList =
            new ArrayList<ValidationBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                " SELECT * FROM  " + " " + getSchema_Name() + ".XXX_VALIDATION validations WHERE validations.EIT_CODE LIKE ?";

            ps = connection.prepareStatement(query);
            //            ps.setString(1, "%" + eitCode + "%");
            ps.setString(1, eitCode);
            rs = ps.executeQuery();
            while (rs.next()) {
                ValidationBean validationBean = new ValidationBean();
                //(rs.getString("REPORT_NAME"));
                validationBean.setId(rs.getString("ID"));
                validationBean.setEitCode(rs.getString("EIT_CODE"));
                validationBean.setFirstKeyType(rs.getString("FIRST_KEY_TYPE"));
                validationBean.setFirstKeyValue(rs.getString("FIRST_KEY_VALUE"));
                validationBean.setApplicationValueFamily(rs.getString("APPLICATION_VALUE_FAMILY"));
                validationBean.setApplicationKey(rs.getString("APPLICATION_KEY"));
                validationBean.setSaaSSource(rs.getString("SAAS_SOURCE"));
                validationBean.setReportName(rs.getString("REPORT_NAME"));
                validationBean.setParameter1Name(rs.getString("PARAMETER1"));
                validationBean.setParameter2Name(rs.getString("PARAMETER2"));
                validationBean.setParameter3Name(rs.getString("PARAMETER3"));
                validationBean.setParameter4Name(rs.getString("PARAMETER4"));
                validationBean.setParameter5Name(rs.getString("PARAMETER5"));
                validationBean.setSaaSQuery(rs.getString("SAAS_QUERY"));
                validationBean.setPaaSQuery(rs.getString("PAAS_QUERY"));
                validationBean.setOperation(rs.getString("OPERATION"));
                validationBean.setSecondKeyType(rs.getString("SECOND_FIRST_KEY_TYPE"));
                validationBean.setSecondKeyValue(rs.getString("SECOND_FIRST_KEY_VALUE"));
                validationBean.setSecondapplicationValueFamily(rs.getString("SECOND_APP_VALFAM"));
                validationBean.setSecondapplicationKey(rs.getString("SECOND_APPLICATION_KEY"));
                validationBean.setSecondsaaSSource(rs.getString("SECOND_SAAS_SOURCE"));
                validationBean.setSecondreportName(rs.getString("SECOND_REPORT_NAME"));
                validationBean.setSecondparameter1Name(rs.getString("SECOND_PARAMETER1"));
                validationBean.setSecondparameter2Name(rs.getString("SECOND_PARAMETER2"));
                validationBean.setSecondparameter3Name(rs.getString("SECOND_PARAMETER3"));
                validationBean.setSecondparameter4Name(rs.getString("SECOND_PARAMETER4"));
                validationBean.setSecondparameter5Name(rs.getString("SECOND_PARAMETER5"));
                validationBean.setSecondsaaSQuery(rs.getString("SECOND_SAAS_QUERY"));
                validationBean.setSecondpaaSQuery(rs.getString("SECOND_PAAS_QUERY"));
                validationBean.setTypeOFAction(rs.getString("TYPE_OF_ACTION"));
                validationBean.setErrorMassageEn(rs.getString("ERROR_MESSAGE_EN"));
                validationBean.setErrorMassageAr(rs.getString("ERROR_MESSAGE_AR"));
                validationBean.setSetValueOfEIT(rs.getString("SAVE_VALUE_OF_EIT"));
                validationBean.setSaveResultToValidationsValues(rs.getString("SAVE_RESULT"));
                validationBean.setValidationValuesName(rs.getString("VALIDATION_VALUES_NAME"));
                validationBean.setValue(rs.getString("VAL"));
                validationBean.setActiontype(rs.getString("ACTIONTYPE"));
                validationBean.setNewValue(rs.getString("NEWVALUE"));
                validationBean.setEitSegment(rs.getString("EITSEGMENT"));
                validationBean.setTypeOfValue(rs.getString("TYPE_OF_VALUE"));
                validationBean.setTypeOfFristValue(rs.getString("TYPE_OF_FRIST_VALUE"));
                validationBean.setFristValues(rs.getString("FRIST_VALUES"));
                validationBean.setSecondOperation(rs.getString("SECOND_OPERATION"));
                validationBean.setTypeOfSecondValue(rs.getString("TYPE_OF_SECOND_VALUE"));
                validationBean.setSecondValue(rs.getString("SECOND_VALUE"));
                validationList.add(validationBean);

            }

            rs.close();
            ps.close();
            connection.close();
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            //   array.add(obj);
        }finally {
            closeResources(connection, ps, rs);
        } 

        return validationList;
    }


    public ArrayList<ValidationBean> searchValidation(String eitCode) {
        ArrayList<ValidationBean> validationList =
            new ArrayList<ValidationBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                " SELECT * FROM  "+" "+ getSchema_Name() +".XXX_VALIDATION validations WHERE validations.EIT_CODE LIKE ?";

            ps = connection.prepareStatement(query);
            //            ps.setString(1, "%" + eitCode + "%");
            ps.setString(1, eitCode);
            rs = ps.executeQuery();
            while (rs.next()) {
                ValidationBean validationBean = new ValidationBean();
                //(rs.getString("REPORT_NAME"));
                validationBean.setId(rs.getString("ID"));
                validationBean.setEitCode(rs.getString("EIT_CODE"));
                validationBean.setFirstKeyType(rs.getString("FIRST_KEY_TYPE"));
                validationBean.setFirstKeyValue(rs.getString("FIRST_KEY_VALUE"));
                validationBean.setApplicationValueFamily(rs.getString("APPLICATION_VALUE_FAMILY"));
                validationBean.setApplicationKey(rs.getString("APPLICATION_KEY"));
                validationBean.setSaaSSource(rs.getString("SAAS_SOURCE"));
                validationBean.setReportName(rs.getString("REPORT_NAME"));
                validationBean.setParameter1Name(rs.getString("PARAMETER1"));
                validationBean.setParameter2Name(rs.getString("PARAMETER2"));
                validationBean.setParameter3Name(rs.getString("PARAMETER3"));
                validationBean.setParameter4Name(rs.getString("PARAMETER4"));
                validationBean.setParameter5Name(rs.getString("PARAMETER5"));
                validationBean.setSaaSQuery(rs.getString("SAAS_QUERY"));
                validationBean.setPaaSQuery(rs.getString("PAAS_QUERY"));
                validationBean.setOperation(rs.getString("OPERATION"));
                validationBean.setSecondKeyType(rs.getString("SECOND_FIRST_KEY_TYPE"));
                validationBean.setSecondKeyValue(rs.getString("SECOND_FIRST_KEY_VALUE"));
                validationBean.setSecondapplicationValueFamily(rs.getString("SECOND_APP_VALFAM"));
                validationBean.setSecondapplicationKey(rs.getString("SECOND_APPLICATION_KEY"));
                validationBean.setSecondsaaSSource(rs.getString("SECOND_SAAS_SOURCE"));
                validationBean.setSecondreportName(rs.getString("SECOND_REPORT_NAME"));
                validationBean.setSecondparameter1Name(rs.getString("SECOND_PARAMETER1"));
                validationBean.setSecondparameter2Name(rs.getString("SECOND_PARAMETER2"));
                validationBean.setSecondparameter3Name(rs.getString("SECOND_PARAMETER3"));
                validationBean.setSecondparameter4Name(rs.getString("SECOND_PARAMETER4"));
                validationBean.setSecondparameter5Name(rs.getString("SECOND_PARAMETER5"));
                validationBean.setSecondsaaSQuery(rs.getString("SECOND_SAAS_QUERY"));
                validationBean.setSecondpaaSQuery(rs.getString("SECOND_PAAS_QUERY"));
                validationBean.setTypeOFAction(rs.getString("TYPE_OF_ACTION"));
                validationBean.setErrorMassageEn(rs.getString("ERROR_MESSAGE_EN"));
                validationBean.setErrorMassageAr(rs.getString("ERROR_MESSAGE_AR"));
                validationBean.setSetValueOfEIT(rs.getString("SAVE_VALUE_OF_EIT"));
                validationBean.setSaveResultToValidationsValues(rs.getString("SAVE_RESULT"));
                validationBean.setValidationValuesName(rs.getString("VALIDATION_VALUES_NAME"));
                validationBean.setValue(rs.getString("VAL"));
                validationBean.setActiontype(rs.getString("ACTIONTYPE"));
                validationBean.setNewValue(rs.getString("NEWVALUE"));
                validationBean.setEitSegment(rs.getString("EITSEGMENT"));
                validationBean.setTypeOfValue(rs.getString("TYPE_OF_VALUE"));
                validationBean.setTypeOfFristValue(rs.getString("TYPE_OF_FRIST_VALUE"));
                validationBean.setFristValues(rs.getString("FRIST_VALUES"));
                validationBean.setSecondOperation(rs.getString("SECOND_OPERATION"));
                validationBean.setTypeOfSecondValue(rs.getString("TYPE_OF_SECOND_VALUE"));
                validationBean.setSecondValue(rs.getString("SECOND_VALUE"));
                validationList.add(validationBean);

            }

            rs.close();
            ps.close();
            connection.close();
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            //   array.add(obj);
        } finally {
            closeResources(connection, ps, rs);
        }

        return validationList;
    }


    public ArrayList<SummaryReportBean> getAllReportName() {
        ArrayList<SummaryReportBean> ReportList =
            new ArrayList<SummaryReportBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                " select * from  " + " " + getSchema_Name() + ".XXX_REPORT ";

            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                SummaryReportBean reportBean = new SummaryReportBean();
                reportBean.setId(rs.getString("ID"));
                reportBean.setReport_Name(rs.getString("REPORT_NAME"));
                reportBean.setParameter1(rs.getString("PARAMETER1"));
                reportBean.setParameter2(rs.getString("PARAMETER2"));
                reportBean.setParameter3(rs.getString("PARAMETER3"));
                reportBean.setParameter4(rs.getString("PARAMETER4"));
                reportBean.setParameter5(rs.getString("PARAMETER5"));
                ReportList.add(reportBean);
            }

            rs.close();
            ps.close();
            connection.close();
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            //   array.add(obj);
        } finally {
            closeResources(connection, ps, rs);
        }
        return ReportList;
    }


    public void deleteValidationRow(String id) {

        try {
            connection = AppsproConnection.getConnection();

            String query = "BEGIN\n" +
                "DELETE FROM  " + " " + getSchema_Name() +
                ".XXX_VALIDATION\n" +
                "WHERE ID = ?;\n" +
                "COMMIT;\n" +
                "END;";

            ps = connection.prepareStatement(query);

            ps.setString(1, id);

            ps.executeUpdate();

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

    }
    //  Stat validation

    //    public int getValidationCount(ValidationBean validationBean, String transactionType) {
    //        int count = -1;
    //        try {
    //            connection = AppsproConnection.getConnection();
    //
    //            String query = null;
    //            if (transactionType.equals("Application_Value_Error_Massage_No")) {
    //                System.err.println("Application_Value_Error_Massage_No");
    //                query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?\n"
    //                        + "AND	APPLICATION_VALUE_FAMILY = ?\n"
    //                        + "AND	APPLICATION_KEY = ?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND	SECOND_APP_VALFAM = ?\n"
    //                        + "AND	SECOND_APPLICATION_KEY = ?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND	ERROR_MESSAGE_EN = ?\n"
    //                        + "AND	ERROR_MESSAGE_AR = ?\n"
    //                        + "AND	SAVE_RESULT = ?";
    //
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getApplicationValueFamily());
    //                ps.setString(4, validationBean.getApplicationKey());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getSecondapplicationValueFamily());
    //                ps.setString(8, validationBean.getSecondapplicationKey());
    //                ps.setString(9, validationBean.getTypeOFAction());
    //                ps.setString(10, validationBean.getErrorMassageEn());
    //                ps.setString(11, validationBean.getErrorMassageAr());
    //                ps.setString(12, validationBean.getSaveResultToValidationsValues());
    //                rs = ps.executeQuery();
    //            } else if (transactionType.equals("Application_Value_Error_Massage_Yes")) {
    //                System.err.println("Application_Value_Error_Massage_Yes");
    //                  query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?\n"
    //                        + "AND	APPLICATION_VALUE_FAMILY = ?\n"
    //                        + "AND	APPLICATION_KEY = ?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND	SECOND_APP_VALFAM = ?\n"
    //                        + "AND	SECOND_APPLICATION_KEY = ?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND	ERROR_MESSAGE_EN = ?\n"
    //                        + "AND	ERROR_MESSAGE_AR = ?\n"
    //                        + "AND	SAVE_RESULT = ?\n"
    //                        + "AND  VALIDATION_VALUES_NAME=?";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getApplicationValueFamily());
    //                ps.setString(4, validationBean.getApplicationKey());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getSecondapplicationValueFamily());
    //                ps.setString(8, validationBean.getSecondapplicationKey());
    //                ps.setString(9, validationBean.getTypeOFAction());
    //                ps.setString(10, validationBean.getErrorMassageEn());
    //                ps.setString(11, validationBean.getErrorMassageAr());
    //                ps.setString(12, validationBean.getSaveResultToValidationsValues());
    //                ps.setString(13, validationBean.getValidationValuesName());
    //                rs = ps.executeQuery();
    //
    //            }else if(transactionType.equals("Application_Value_ACTION_USE_TO_ANOTHER_VALIDATION_Yes")){
    //                  System.err.println("Application_Value_ACTION_USE_TO_ANOTHER_VALIDATION_Yes");
    //                 query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?\n"
    //                        + "AND	APPLICATION_VALUE_FAMILY = ?\n"
    //                        + "AND	APPLICATION_KEY = ?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND	SECOND_APP_VALFAM = ?\n"
    //                        + "AND	SECOND_APPLICATION_KEY = ?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND	SAVE_RESULT = ?\n"
    //                        + "AND	VALIDATION_VALUES_NAME =?\n";
    //
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getApplicationValueFamily());
    //                ps.setString(4, validationBean.getApplicationKey());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getSecondapplicationValueFamily());
    //                ps.setString(8, validationBean.getSecondapplicationKey());
    //                ps.setString(9, validationBean.getTypeOFAction());
    //                ps.setString(10, validationBean.getActiontype());
    //                ps.setString(11, validationBean.getSaveResultToValidationsValues());
    //                ps.setString(12, validationBean.getValidationValuesName());
    //                rs = ps.executeQuery();
    //
    //            }else if(transactionType.equals("Application_Value_ACTION_USE_TO_ANOTHER_VALIDATION_No")){
    //                 query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?\n"
    //                        + "AND	APPLICATION_VALUE_FAMILY = ?\n"
    //                        + "AND	APPLICATION_KEY = ?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND	SECOND_APP_VALFAM = ?\n"
    //                        + "AND	SECOND_APPLICATION_KEY = ?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND	SAVE_RESULT = ?\n";
    //
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getApplicationValueFamily());
    //                ps.setString(4, validationBean.getApplicationKey());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getSecondapplicationValueFamily());
    //                ps.setString(8, validationBean.getSecondapplicationKey());
    //                ps.setString(9, validationBean.getTypeOFAction());
    //                ps.setString(10, validationBean.getActiontype());
    //                ps.setString(11, validationBean.getSaveResultToValidationsValues());
    //                rs = ps.executeQuery();
    //            } else if(transactionType.equals("Application_Value_ACTION_Hide_Disabled_No")){
    //
    //                 query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?\n"
    //                        + "AND	APPLICATION_VALUE_FAMILY = ?\n"
    //                        + "AND	APPLICATION_KEY = ?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND	SECOND_APP_VALFAM = ?\n"
    //                        + "AND	SECOND_APPLICATION_KEY = ?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND  EITSEGMENT=?\n"
    //                        + "AND	SAVE_RESULT = ?\n";
    //
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getApplicationValueFamily());
    //                ps.setString(4, validationBean.getApplicationKey());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getSecondapplicationValueFamily());
    //                ps.setString(8, validationBean.getSecondapplicationKey());
    //                ps.setString(9, validationBean.getTypeOFAction());
    //                ps.setString(10, validationBean.getActiontype());
    //                ps.setString(11, validationBean.getEitSegment());
    //                ps.setString(12, validationBean.getSaveResultToValidationsValues());
    //                rs = ps.executeQuery();
    //
    //
    //            }else if(transactionType.equals("Application_Value_ACTION_set_value_static_Yes")){
    //
    //               query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?\n"
    //                        + "AND	APPLICATION_VALUE_FAMILY = ?\n"
    //                        + "AND	APPLICATION_KEY = ?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND	SECOND_APP_VALFAM = ?\n"
    //                        + "AND	SECOND_APPLICATION_KEY = ?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND  EITSEGMENT=?\n"
    //                        + "AND  TYPE_OF_VALUE=?\n"
    //                        + "AND  NEWVALUE=?\n"
    //                        + "AND	SAVE_RESULT = ?\n"
    //                        + "AND  VALIDATION_VALUES_NAME=?\n";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getApplicationValueFamily());
    //                ps.setString(4, validationBean.getApplicationKey());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getSecondapplicationValueFamily());
    //                ps.setString(8, validationBean.getSecondapplicationKey());
    //                ps.setString(9, validationBean.getTypeOFAction());
    //                ps.setString(10, validationBean.getActiontype());
    //                ps.setString(11, validationBean.getEitSegment());
    //                ps.setString(12, validationBean.getTypeOfValue());
    //                ps.setString(13, validationBean.getNewValue());
    //                ps.setString(14, validationBean.getSaveResultToValidationsValues());
    //                ps.setString(15, validationBean.getValidationValuesName());
    //                rs = ps.executeQuery();
    //
    //            }else if(transactionType.equals("Application_Value_ACTION_set_value_static_No")){
    //                query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?\n"
    //                        + "AND	APPLICATION_VALUE_FAMILY = ?\n"
    //                        + "AND	APPLICATION_KEY = ?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND	SECOND_APP_VALFAM = ?\n"
    //                        + "AND	SECOND_APPLICATION_KEY = ?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND  EITSEGMENT=?\n"
    //                        + "AND  TYPE_OF_VALUE=?\n"
    //                        + "AND  NEWVALUE=?\n"
    //                        + "AND	SAVE_RESULT = ?\n";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getApplicationValueFamily());
    //                ps.setString(4, validationBean.getApplicationKey());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getSecondapplicationValueFamily());
    //                ps.setString(8, validationBean.getSecondapplicationKey());
    //                ps.setString(9, validationBean.getTypeOFAction());
    //                ps.setString(10, validationBean.getActiontype());
    //                ps.setString(11, validationBean.getEitSegment());
    //                ps.setString(12,validationBean.getTypeOfValue());
    //                ps.setString(13,validationBean.getNewValue());
    //                ps.setString(14, validationBean.getSaveResultToValidationsValues());
    //
    //                rs = ps.executeQuery();
    //            } else if(transactionType.equals("Application_Value_ACTION_set_value_complex_No")) {
    //                 query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?\n"
    //                        + "AND	APPLICATION_VALUE_FAMILY = ?\n"
    //                        + "AND	APPLICATION_KEY = ?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND	SECOND_APP_VALFAM = ?\n"
    //                        + "AND	SECOND_APPLICATION_KEY = ?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND  EITSEGMENT=?\n"
    //                        + "AND  TYPE_OF_VALUE=?\n"
    //                        + "AND  TYPE_OF_FRIST_VALUE=?\n"
    //                        + "AND  FRIST_VALUES=?\n"
    //                        + "AND  SECOND_OPERATION=?\n"
    //                        + "AND  TYPE_OF_SECOND_VALUE=?\n"
    //                        + "AND SECOND_VALUE=?\n"
    //                        + "AND	SAVE_RESULT = ?\n";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getApplicationValueFamily());
    //                ps.setString(4, validationBean.getApplicationKey());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getSecondapplicationValueFamily());
    //                ps.setString(8, validationBean.getSecondapplicationKey());
    //                ps.setString(9, validationBean.getTypeOFAction());
    //                ps.setString(10, validationBean.getActiontype());
    //                ps.setString(11, validationBean.getEitSegment());
    //                ps.setString(12,validationBean.getTypeOfValue());
    //                ps.setString(13, validationBean.getTypeOfFristValue());
    //                ps.setString(14, validationBean.getFristValues());
    //                ps.setString(15, validationBean.getSecondOperation());
    //                ps.setString(16, validationBean.getTypeOfSecondValue());
    //                ps.setString(17, validationBean.getSecondValue());
    //                ps.setString(18, validationBean.getSaveResultToValidationsValues());
    //
    //                rs = ps.executeQuery();
    //
    //            }else if(transactionType.equals("Application_Value_ACTION_set_value_complex_Yes")){
    //
    //                  query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?\n"
    //                        + "AND	APPLICATION_VALUE_FAMILY = ?\n"
    //                        + "AND	APPLICATION_KEY = ?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND	SECOND_APP_VALFAM = ?\n"
    //                        + "AND	SECOND_APPLICATION_KEY = ?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND  EITSEGMENT=?\n"
    //                        + "AND  TYPE_OF_VALUE=?\n"
    //                        + "AND  TYPE_OF_FRIST_VALUE=?\n"
    //                        + "AND  FRIST_VALUES=?\n"
    //                        + "AND  SECOND_OPERATION=?\n"
    //                        + "AND  TYPE_OF_SECOND_VALUE=?\n"
    //                        + "AND SECOND_VALUE=?\n"
    //                        + "AND	SAVE_RESULT = ?\n"
    //                        + "AND  VALIDATION_VALUES_NAME=?\n";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getApplicationValueFamily());
    //                ps.setString(4, validationBean.getApplicationKey());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getSecondapplicationValueFamily());
    //                ps.setString(8, validationBean.getSecondapplicationKey());
    //                ps.setString(9, validationBean.getTypeOFAction());
    //                ps.setString(10, validationBean.getActiontype());
    //                ps.setString(11, validationBean.getEitSegment());
    //                ps.setString(12,validationBean.getTypeOfValue());
    //                ps.setString(13, validationBean.getTypeOfFristValue());
    //                ps.setString(14, validationBean.getFristValues());
    //                ps.setString(15, validationBean.getSecondOperation());
    //                ps.setString(16, validationBean.getTypeOfSecondValue());
    //                ps.setString(17, validationBean.getSecondValue());
    //                ps.setString(18, validationBean.getSaveResultToValidationsValues());
    //                ps.setString(19, validationBean.getValidationValuesName());
    //                rs = ps.executeQuery();
    //            }else if(transactionType.equals("Static_Error_Massage_No")){
    //                System.err.println("Static_Error_Massage_No");
    //                query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?\n"
    //                        + "AND  FIRST_KEY_VALUE=?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND  SECOND_FIRST_KEY_VALUE=?\n "
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND	ERROR_MESSAGE_EN = ?\n"
    //                        + "AND	ERROR_MESSAGE_AR = ?\n"
    //                        + "AND	SAVE_RESULT = ?";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getFirstKeyValue());
    //                ps.setString(4, validationBean.getOperation());
    //                ps.setString(5, validationBean.getSecondKeyValue());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getTypeOFAction());
    //                ps.setString(8, validationBean.getErrorMassageEn());
    //                ps.setString(9, validationBean.getErrorMassageAr());
    //                ps.setString(10, validationBean.getSaveResultToValidationsValues());
    //                rs = ps.executeQuery();
    //
    //            }else if(transactionType.equals("Static_Error_Massage_Yes")){
    //                System.err.println("Static_Error_Massage_Yes");
    //                query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?\n"
    //                        + "AND  FIRST_KEY_VALUE=?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND  SECOND_FIRST_KEY_VALUE=?\n "
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND	ERROR_MESSAGE_EN = ?\n"
    //                        + "AND	ERROR_MESSAGE_AR = ?\n"
    //                        + "AND	SAVE_RESULT = ?\n"
    //                        + "AND VALIDATION_VALUES_NAME=?";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getFirstKeyValue());
    //                ps.setString(4, validationBean.getOperation());
    //                ps.setString(5, validationBean.getSecondKeyValue());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getTypeOFAction());
    //                ps.setString(8, validationBean.getErrorMassageEn());
    //                ps.setString(9, validationBean.getErrorMassageAr());
    //                ps.setString(10, validationBean.getSaveResultToValidationsValues());
    //                ps.setString(11, validationBean.getValidationValuesName());
    //
    //                rs = ps.executeQuery();
    //
    //
    //             }else if (transactionType.equals("Static_ACTION_USE_TO_ANOTHER_VALIDATION_Yes")){
    //
    //                  query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?\n"
    //                        + "AND  FIRST_KEY_VALUE=?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND  SECOND_FIRST_KEY_VALUE=?\n "
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND	ACTIONTYPE = ?\n"
    //                        + "AND	SAVE_RESULT = ?\n"
    //                        + "AND VALIDATION_VALUES_NAME=?";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getFirstKeyValue());
    //                ps.setString(4, validationBean.getOperation());
    //                ps.setString(5, validationBean.getSecondKeyValue());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getTypeOFAction());
    //                ps.setString(8, validationBean.getActiontype());
    //                ps.setString(9, validationBean.getSaveResultToValidationsValues());
    //                ps.setString(10, validationBean.getValidationValuesName());
    //
    //                rs = ps.executeQuery();
    //
    //            } else if(transactionType.equals("Static_ACTION_USE_TO_ANOTHER_VALIDATION_No")){
    //
    //               query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?\n"
    //                        + "AND  FIRST_KEY_VALUE=?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND  SECOND_FIRST_KEY_VALUE=?\n "
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND	ACTIONTYPE = ?\n"
    //                        + "AND	SAVE_RESULT = ?\n";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getFirstKeyValue());
    //                ps.setString(4, validationBean.getOperation());
    //                ps.setString(5, validationBean.getSecondKeyValue());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getTypeOFAction());
    //                ps.setString(8, validationBean.getActiontype());
    //                ps.setString(9, validationBean.getSaveResultToValidationsValues());
    //
    //                rs = ps.executeQuery();
    //
    //            } else if(transactionType.equals("Static_ACTION_Hide_Disabled_No")){
    //
    //              query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?\n"
    //                        + "AND  FIRST_KEY_VALUE=?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND  SECOND_FIRST_KEY_VALUE=?\n "
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND	ACTIONTYPE = ?\n"
    //                        + "AND  EITSEGMENT=?\n"
    //                        + "AND	SAVE_RESULT = ?\n";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getFirstKeyValue());
    //                ps.setString(4, validationBean.getOperation());
    //                ps.setString(5, validationBean.getSecondKeyValue());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getTypeOFAction());
    //                ps.setString(8, validationBean.getActiontype());
    //                ps.setString(9, validationBean.getEitSegment());
    //                ps.setString(9, validationBean.getSaveResultToValidationsValues());
    //
    //                rs = ps.executeQuery();
    //
    //            }else if(transactionType.equals("Static_ACTION_Hide_Disabled_Yes")){
    //
    //                  query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?\n"
    //                        + "AND  FIRST_KEY_VALUE=?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND  SECOND_FIRST_KEY_VALUE=?\n "
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND	ACTIONTYPE = ?\n"
    //                        + "AND  EITSEGMENT=?\n"
    //                        + "AND	SAVE_RESULT = ?\n"
    //                        + "AND VALIDATION_VALUES_NAME=?";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getFirstKeyValue());
    //                ps.setString(4, validationBean.getOperation());
    //                ps.setString(5, validationBean.getSecondKeyValue());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getTypeOFAction());
    //                ps.setString(8, validationBean.getActiontype());
    //                ps.setString(9, validationBean.getEitSegment());
    //                ps.setString(10, validationBean.getSaveResultToValidationsValues());
    //                ps.setString(11, validationBean.getValidationValuesName());
    //
    //                rs = ps.executeQuery();
    //            }else if(transactionType.equals("Static_ACTION_set_value_static_Yes")){
    //
    //                    query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?\n"
    //                        + "AND  FIRST_KEY_VALUE=?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND  SECOND_FIRST_KEY_VALUE=?\n "
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND	ACTIONTYPE = ?\n"
    //                        + "AND  EITSEGMENT=?\n"
    //                        + "AND  TYPE_OF_VALUE=?\n"
    //                        + "AND  NEWVALUE=?\n"
    //                        + "AND	SAVE_RESULT = ?\n"
    //                        + "AND VALIDATION_VALUES_NAME=?";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getFirstKeyValue());
    //                ps.setString(4, validationBean.getOperation());
    //                ps.setString(5, validationBean.getSecondKeyValue());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getTypeOFAction());
    //                ps.setString(8, validationBean.getActiontype());
    //                ps.setString(9, validationBean.getEitSegment());
    //                ps.setString(10, validationBean.getTypeOfValue());
    //                ps.setString(11, validationBean.getNewValue());
    //                ps.setString(12, validationBean.getSaveResultToValidationsValues());
    //                ps.setString(13, validationBean.getValidationValuesName());
    //
    //                rs = ps.executeQuery();
    //
    //            }else if(transactionType.equals("Static_ACTION_set_value_static_No")){
    //
    //                 query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?\n"
    //                        + "AND  FIRST_KEY_VALUE=?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND  SECOND_FIRST_KEY_VALUE=?\n "
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND	ACTIONTYPE = ?\n"
    //                        + "AND  EITSEGMENT=?\n"
    //                        + "AND  TYPE_OF_VALUE=?\n"
    //                        + "AND  NEWVALUE=?\n"
    //                        + "AND	SAVE_RESULT = ?\n";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getFirstKeyValue());
    //                ps.setString(4, validationBean.getOperation());
    //                ps.setString(5, validationBean.getSecondKeyValue());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getTypeOFAction());
    //                ps.setString(8, validationBean.getActiontype());
    //                ps.setString(9, validationBean.getEitSegment());
    //                ps.setString(10, validationBean.getTypeOfValue());
    //                ps.setString(11, validationBean.getNewValue());
    //                ps.setString(12, validationBean.getSaveResultToValidationsValues());
    //
    //                rs = ps.executeQuery();
    //            }else if(transactionType.equals("Static_ACTION_set_value_complex_No")){
    //
    //                  query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?\n"
    //                        + "AND  FIRST_KEY_VALUE=?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND  SECOND_FIRST_KEY_VALUE=?\n "
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND	ACTIONTYPE = ?\n"
    //                        + "AND  EITSEGMENT=?\n"
    //                        + "AND  TYPE_OF_VALUE=?\n"
    //                        + "AND  TYPE_OF_FRIST_VALUE=?\n"
    //                        + "AND  FRIST_VALUES =?\n"
    //                        + "AND  SECOND_OPERATION=?\n"
    //                        + "AND  TYPE_OF_SECOND_VALUE=?\n"
    //                        + "AND  SECOND_VALUE=?\n"
    //                        + "AND	SAVE_RESULT = ?\n";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getFirstKeyValue());
    //                ps.setString(4, validationBean.getOperation());
    //                ps.setString(5, validationBean.getSecondKeyValue());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getTypeOFAction());
    //                ps.setString(8, validationBean.getActiontype());
    //                ps.setString(9, validationBean.getEitSegment());
    //                ps.setString(10, validationBean.getTypeOfValue());
    //                ps.setString(11, validationBean.getTypeOfFristValue());
    //                ps.setString(12, validationBean.getFristValues());
    //                ps.setString(13, validationBean.getSecondOperation());
    //                ps.setString(14, validationBean.getTypeOfSecondValue());
    //                ps.setString(15, validationBean.getSecondValue());
    //                ps.setString(16, validationBean.getSaveResultToValidationsValues());
    //
    //                rs = ps.executeQuery();
    //
    //
    //            }else if(transactionType.equals("Static_ACTION_set_value_complex_Yes")){
    //             String x="=";
    //                 query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?\n"
    //                        + "AND  FIRST_KEY_VALUE=?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND  SECOND_FIRST_KEY_VALUE=?\n "
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND	ACTIONTYPE = ?\n"
    //                        + "AND  EITSEGMENT=?\n"
    //                        + "AND  TYPE_OF_VALUE=?\n"
    //                        + "AND  TYPE_OF_FRIST_VALUE=?\n"
    //                        + "AND  FRIST_VALUES =?\n"
    //                        + "AND  SECOND_OPERATION=?\n"
    //                        + "AND  TYPE_OF_SECOND_VALUE=?\n"
    //                        + "AND  SECOND_VALUE=?\n"
    //                        + "AND	SAVE_RESULT = ?\n"
    //                        + "AND VALIDATION_VALUES_NAME=?";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getFirstKeyValue());
    //                ps.setString(4, validationBean.getOperation());
    //                ps.setString(5, validationBean.getSecondKeyValue());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getTypeOFAction());
    //                ps.setString(8, validationBean.getActiontype());
    //                ps.setString(9, validationBean.getEitSegment());
    //                ps.setString(10, validationBean.getTypeOfValue());
    //                ps.setString(11, validationBean.getTypeOfFristValue());
    //                ps.setString(12, validationBean.getFristValues());
    //                ps.setString(13, validationBean.getSecondOperation());
    //                ps.setString(14, validationBean.getTypeOfSecondValue());
    //                ps.setString(15, validationBean.getSecondValue());
    //                ps.setString(16, validationBean.getSaveResultToValidationsValues());
    //                ps.setString(17, validationBean.getValidationValuesName());
    //
    //                rs = ps.executeQuery();
    //
    //
    //            }else if(transactionType.equals("PaaS_Error_Massage_Yes")){
    //                query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND  PAAS_QUERY =?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?"
    //                        + "AND  SECOND_PAAS_QUERY=?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND	ERROR_MESSAGE_EN = ?\n"
    //                        + "AND	ERROR_MESSAGE_AR = ?\n"
    //                        + "AND	SAVE_RESULT = ?"
    //                        + "AND VALIDATION_VALUES_NAME=?";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getPaaSQuery());
    //                ps.setString(4, validationBean.getOperation());
    //                ps.setString(5, validationBean.getSecondKeyType());
    //                ps.setString(6, validationBean.getSecondpaaSQuery());
    //                ps.setString(7, validationBean.getTypeOFAction());
    //                ps.setString(8, validationBean.getErrorMassageEn());
    //                ps.setString(9, validationBean.getErrorMassageAr());
    //                ps.setString(10, validationBean.getSaveResultToValidationsValues());
    //                ps.setString(11, validationBean.getValidationValuesName());
    //                rs = ps.executeQuery();
    //
    //            }else if(transactionType.equals("PaaS_Error_Massage_No")){
    //              query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND  PAAS_QUERY =?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?"
    //                        + "AND  SECOND_PAAS_QUERY=?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND	ERROR_MESSAGE_EN = ?\n"
    //                        + "AND	ERROR_MESSAGE_AR = ?\n"
    //                        + "AND	SAVE_RESULT = ?";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getPaaSQuery());
    //                ps.setString(4, validationBean.getOperation());
    //                ps.setString(5, validationBean.getSecondKeyType());
    //                ps.setString(6, validationBean.getSecondpaaSQuery());
    //                ps.setString(7, validationBean.getTypeOFAction());
    //                ps.setString(8, validationBean.getErrorMassageEn());
    //                ps.setString(9, validationBean.getErrorMassageAr());
    //                ps.setString(10, validationBean.getSaveResultToValidationsValues());
    //                rs = ps.executeQuery();
    //            }else if(transactionType.equals("PaaS_ACTION_USE_TO_ANOTHER_VALIDATION_Yes")){
    //
    //                  query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND  PAAS_QUERY =?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?"
    //                        + "AND  SECOND_PAAS_QUERY=?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND	SAVE_RESULT = ?\n"
    //                        + "AND VALIDATION_VALUES_NAME=?";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getPaaSQuery());
    //                ps.setString(4, validationBean.getOperation());
    //                ps.setString(5, validationBean.getSecondKeyType());
    //                ps.setString(6, validationBean.getSecondpaaSQuery());
    //                ps.setString(7, validationBean.getTypeOFAction());
    //                ps.setString(8, validationBean.getActiontype());
    //                ps.setString(9, validationBean.getSaveResultToValidationsValues());
    //                ps.setString(10, validationBean.getValidationValuesName());
    //                rs = ps.executeQuery();
    //
    //            }else if(transactionType.equals("PaaS_ACTION_USE_TO_ANOTHER_VALIDATION_No")){
    //
    //                 query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND  PAAS_QUERY =?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?"
    //                        + "AND  SECOND_PAAS_QUERY=?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND	SAVE_RESULT = ?";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getPaaSQuery());
    //                ps.setString(4, validationBean.getOperation());
    //                ps.setString(5, validationBean.getSecondKeyType());
    //                ps.setString(6, validationBean.getSecondpaaSQuery());
    //                ps.setString(7, validationBean.getTypeOFAction());
    //                ps.setString(8, validationBean.getActiontype());
    //                ps.setString(9, validationBean.getSaveResultToValidationsValues());
    //                rs = ps.executeQuery();
    //
    //            } else if(transactionType.equals("PaaS_Disabled_Hide_Yes")){
    //             query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND  PAAS_QUERY =?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?"
    //                        + "AND  SECOND_PAAS_QUERY=?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND  EITSEGMENT=?\n"
    //                        + "AND	SAVE_RESULT = ?\n"
    //                        + "AND VALIDATION_VALUES_NAME=?";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getPaaSQuery());
    //                ps.setString(4, validationBean.getOperation());
    //                ps.setString(5, validationBean.getSecondKeyType());
    //                ps.setString(6, validationBean.getSecondpaaSQuery());
    //                ps.setString(7, validationBean.getTypeOFAction());
    //                ps.setString(8, validationBean.getActiontype());
    //                ps.setString(9, validationBean.getEitSegment());
    //                ps.setString(10, validationBean.getSaveResultToValidationsValues());
    //                ps.setString(11, validationBean.getValidationValuesName());
    //                rs = ps.executeQuery();
    //
    //            }else if(transactionType.equals("PaaS_Disabled_Hide_No")){
    //
    //                  query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND  PAAS_QUERY =?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?"
    //                        + "AND  SECOND_PAAS_QUERY=?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND  EITSEGMENT=?\n"
    //                        + "AND	SAVE_RESULT = ?\n";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getPaaSQuery());
    //                ps.setString(4, validationBean.getOperation());
    //                ps.setString(5, validationBean.getSecondKeyType());
    //                ps.setString(6, validationBean.getSecondpaaSQuery());
    //                ps.setString(7, validationBean.getTypeOFAction());
    //                ps.setString(8, validationBean.getActiontype());
    //                ps.setString(9, validationBean.getEitSegment());
    //                ps.setString(10, validationBean.getSaveResultToValidationsValues());
    //                rs = ps.executeQuery();
    //            }else if(transactionType.equals("PaaS_ACTION_set_value_static_Yes")){
    //
    //                 query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND  PAAS_QUERY =?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?"
    //                        + "AND  SECOND_PAAS_QUERY=?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND  EITSEGMENT=?"
    //                         +"AND  TYPE_OF_VALUE =? \n"
    //                         +"AND  NEWVALUE=?\n"
    //                        + "AND	SAVE_RESULT = ?\n"
    //                        + "AND VALIDATION_VALUES_NAME=?";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getPaaSQuery());
    //                ps.setString(4, validationBean.getOperation());
    //                ps.setString(5, validationBean.getSecondKeyType());
    //                ps.setString(6, validationBean.getSecondpaaSQuery());
    //                ps.setString(7, validationBean.getTypeOFAction());
    //                ps.setString(8, validationBean.getActiontype());
    //                ps.setString(9, validationBean.getEitSegment());
    //                ps.setString(10, validationBean.getTypeOfValue());
    //                ps.setString(11, validationBean.getNewValue());
    //                ps.setString(12, validationBean.getSaveResultToValidationsValues());
    //                ps.setString(13, validationBean.getValidationValuesName());
    //                rs = ps.executeQuery();
    //
    //
    //            }else if(transactionType.equals("PaaS_ACTION_set_value_static_No")){
    //
    //                 query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND  PAAS_QUERY =?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?"
    //                        + "AND  SECOND_PAAS_QUERY=?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND  EITSEGMENT=?"
    //                         +"AND  TYPE_OF_VALUE =? \n"
    //                         +"AND  NEWVALUE=?\n"
    //                        + "AND	SAVE_RESULT =?";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getPaaSQuery());
    //                ps.setString(4, validationBean.getOperation());
    //                ps.setString(5, validationBean.getSecondKeyType());
    //                ps.setString(6, validationBean.getSecondpaaSQuery());
    //                ps.setString(7, validationBean.getTypeOFAction());
    //                ps.setString(8, validationBean.getActiontype());
    //                ps.setString(9, validationBean.getEitSegment());
    //                ps.setString(10, validationBean.getTypeOfValue());
    //                ps.setString(11, validationBean.getNewValue());
    //                ps.setString(12, validationBean.getSaveResultToValidationsValues());
    //                rs = ps.executeQuery();
    //
    //            }else if(transactionType.equals("PaaS_ACTION_set_value_complex_No")){
    //
    //                query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND  PAAS_QUERY =?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?"
    //                        + "AND  SECOND_PAAS_QUERY=?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND  EITSEGMENT=?\n"
    //                         +"AND  TYPE_OF_VALUE =?\n"
    //                        + "AND  TYPE_OF_FRIST_VALUE=?\n"
    //                        + "AND  FRIST_VALUES =?\n"
    //                        + "AND  SECOND_OPERATION=?\n"
    //                        + "AND  TYPE_OF_SECOND_VALUE=?\n"
    //                        + "AND  SECOND_VALUE=?\n"
    //                        + "AND	SAVE_RESULT = ?\n";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getPaaSQuery());
    //                ps.setString(4, validationBean.getOperation());
    //                ps.setString(5, validationBean.getSecondKeyType());
    //                ps.setString(6, validationBean.getSecondpaaSQuery());
    //                ps.setString(7, validationBean.getTypeOFAction());
    //                ps.setString(8, validationBean.getActiontype());
    //                ps.setString(9, validationBean.getEitSegment());
    //                ps.setString(10, validationBean.getTypeOfValue());
    //                ps.setString(11, validationBean.getTypeOfFristValue());
    //                ps.setString(12, validationBean.getFristValues());
    //                ps.setString(13, validationBean.getSecondOperation());
    //                ps.setString(14, validationBean.getTypeOfSecondValue());
    //                ps.setString(15, validationBean.getSecondValue());
    //                ps.setString(16, validationBean.getSaveResultToValidationsValues());
    //                rs = ps.executeQuery();
    //
    //            }else if(transactionType.equals("PaaS_ACTION_set_value_complex_Yes")){
    //
    //                 query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND  PAAS_QUERY =?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?"
    //                        + "AND  SECOND_PAAS_QUERY=?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND  EITSEGMENT=?\n"
    //                         +"AND  TYPE_OF_VALUE =?\n"
    //                        + "AND  TYPE_OF_FRIST_VALUE=?\n"
    //                        + "AND  FRIST_VALUES =?\n"
    //                        + "AND  SECOND_OPERATION=?\n"
    //                        + "AND  TYPE_OF_SECOND_VALUE=?\n"
    //                        + "AND  SECOND_VALUE=?\n"
    //                        + "AND	SAVE_RESULT = ?\n"
    //                        + "AND VALIDATION_VALUES_NAME=?";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getPaaSQuery());
    //                ps.setString(4, validationBean.getOperation());
    //                ps.setString(5, validationBean.getSecondKeyType());
    //                ps.setString(6, validationBean.getSecondpaaSQuery());
    //                ps.setString(7, validationBean.getTypeOFAction());
    //                ps.setString(8, validationBean.getActiontype());
    //                ps.setString(9, validationBean.getEitSegment());
    //                ps.setString(10, validationBean.getTypeOfValue());
    //                ps.setString(11, validationBean.getTypeOfFristValue());
    //                ps.setString(12, validationBean.getFristValues());
    //                ps.setString(13, validationBean.getSecondOperation());
    //                ps.setString(14, validationBean.getTypeOfSecondValue());
    //                ps.setString(15, validationBean.getSecondValue());
    //                ps.setString(16, validationBean.getSaveResultToValidationsValues());
    //                ps.setString(17, validationBean.getValidationValuesName());
    //                rs = ps.executeQuery();
    //
    //            }else if(transactionType.equals("SaaS_Report_Error_Massage_No")){
    //
    //                query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?\n"
    //                        + "AND	SAAS_SOURCE = ?\n"
    //                        + "AND	REPORT_NAME = ?\n"
    //                        + "AND	PARAMETER1 = ?\n"
    //                        + "AND	PARAMETER2 = ?\n"
    //                        + "AND	PARAMETER3 = ?\n"
    //                        + "AND	PARAMETER4 = ?\n"
    //                        + "AND	PARAMETER5 = ?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND	SECOND_SAAS_SOURCE = ?\n"
    //                        + "AND	SECOND_REPORT_NAME = ?\n"
    //                        + "AND	SECOND_PARAMETER1 = ?\n"
    //                        + "AND	SECOND_PARAMETER2 = ?\n"
    //                        + "AND	SECOND_PARAMETER3 = ?\n"
    //                        + "AND	SECOND_PARAMETER4 = ?\n"
    //                        + "AND	SECOND_PARAMETER5 = ?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND	ERROR_MESSAGE_EN = ?\n"
    //                        + "AND	ERROR_MESSAGE_AR = ?\n"
    //                        + "AND	SAVE_RESULT = ?\n";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getSaaSSource());
    //                ps.setString(4, validationBean.getReportName());
    //                ps.setString(5, validationBean.getParameter1Name());
    //                ps.setString(6, validationBean.getParameter2Name());
    //                ps.setString(7, validationBean.getParameter3Name());
    //                ps.setString(8, validationBean.getParameter4Name());
    //                ps.setString(9, validationBean.getParameter5Name());
    //                ps.setString(10, validationBean.getOperation());
    //                ps.setString(11, validationBean.getSecondKeyType());
    //                ps.setString(12, validationBean.getSecondsaaSSource());
    //                ps.setString(13, validationBean.getSecondreportName());
    //                ps.setString(14, validationBean.getSecondparameter1Name());
    //                ps.setString(15, validationBean.getSecondparameter2Name());
    //                ps.setString(16, validationBean.getSecondparameter3Name());
    //                ps.setString(17, validationBean.getSecondparameter4Name());
    //                ps.setString(18, validationBean.getSecondparameter5Name());
    //                ps.setString(19, validationBean.getTypeOFAction());
    //                ps.setString(20, validationBean.getErrorMassageEn());
    //                ps.setString(21, validationBean.getErrorMassageAr());
    //                ps.setString(22, validationBean.getSaveResultToValidationsValues());
    //                rs = ps.executeQuery();
    //
    //
    //            } else if(transactionType.equals("SaaS_Report_Error_Massage_Yes")){
    //
    //                 query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?\n"
    //                        + "AND	SAAS_SOURCE = ?\n"
    //                        + "AND	REPORT_NAME = ?\n"
    //                        + "AND	PARAMETER1 = ?\n"
    //                        + "AND	PARAMETER2 = ?\n"
    //                        + "AND	PARAMETER3 = ?\n"
    //                        + "AND	PARAMETER4 = ?\n"
    //                        + "AND	PARAMETER5 = ?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND	SECOND_SAAS_SOURCE = ?\n"
    //                        + "AND	SECOND_REPORT_NAME = ?\n"
    //                        + "AND	SECOND_PARAMETER1 = ?\n"
    //                        + "AND	SECOND_PARAMETER2 = ?\n"
    //                        + "AND	SECOND_PARAMETER3 = ?\n"
    //                        + "AND	SECOND_PARAMETER4 = ?\n"
    //                        + "AND	SECOND_PARAMETER5 = ?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND	ERROR_MESSAGE_EN = ?\n"
    //                        + "AND	ERROR_MESSAGE_AR = ?\n"
    //                        + "AND	SAVE_RESULT = ?\n"
    //                        + "AND VALIDATION_VALUES_NAME=?\n";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getSaaSSource());
    //                ps.setString(4, validationBean.getReportName());
    //                ps.setString(5, validationBean.getParameter1Name());
    //                ps.setString(6, validationBean.getParameter2Name());
    //                ps.setString(7, validationBean.getParameter3Name());
    //                ps.setString(8, validationBean.getParameter4Name());
    //                ps.setString(9, validationBean.getParameter5Name());   //go
    //                ps.setString(10, validationBean.getOperation());
    //                ps.setString(11, validationBean.getSecondKeyType());
    //                ps.setString(12, validationBean.getSecondsaaSSource());
    //                ps.setString(13, validationBean.getSecondreportName());
    //                ps.setString(14, validationBean.getSecondparameter1Name());
    //                ps.setString(15, validationBean.getSecondparameter2Name());
    //                ps.setString(16, validationBean.getSecondparameter3Name());
    //                ps.setString(17, validationBean.getSecondparameter4Name());
    //                ps.setString(18, validationBean.getSecondparameter5Name());
    //                ps.setString(19, validationBean.getTypeOFAction());
    //                ps.setString(20, validationBean.getErrorMassageEn());
    //                ps.setString(21, validationBean.getErrorMassageAr());
    //                ps.setString(22, validationBean.getSaveResultToValidationsValues());
    //                ps.setString(23, validationBean.getValidationValuesName());
    //                rs = ps.executeQuery();
    //
    //            } else if(transactionType.equals("SaaS_Report_ACTION_USE_TO_ANOTHER_VALIDATION_Yes")){
    //             query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?\n"
    //                        + "AND	SAAS_SOURCE = ?\n"
    //                        + "AND	REPORT_NAME = ?\n"
    //                        + "AND	PARAMETER1 = ?\n"
    //                        + "AND	PARAMETER2 = ?\n"
    //                        + "AND	PARAMETER3 = ?\n"
    //                        + "AND	PARAMETER4 = ?\n"
    //                        + "AND	PARAMETER5 = ?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND	SECOND_SAAS_SOURCE = ?\n"
    //                        + "AND	SECOND_REPORT_NAME = ?\n"
    //                        + "AND	SECOND_PARAMETER1 = ?\n"
    //                        + "AND	SECOND_PARAMETER2 = ?\n"
    //                        + "AND	SECOND_PARAMETER3 = ?\n"
    //                        + "AND	SECOND_PARAMETER4 = ?\n"
    //                        + "AND	SECOND_PARAMETER5 = ?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND	SAVE_RESULT = ?\n"
    //                        + "AND VALIDATION_VALUES_NAME=?\n";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getSaaSSource());
    //                ps.setString(4, validationBean.getReportName());
    //                ps.setString(5, validationBean.getParameter1Name());
    //                ps.setString(6, validationBean.getParameter2Name());
    //                ps.setString(7, validationBean.getParameter3Name());
    //                ps.setString(8, validationBean.getParameter4Name());
    //                ps.setString(9, validationBean.getParameter5Name());   //go
    //                ps.setString(10, validationBean.getOperation());
    //                ps.setString(11, validationBean.getSecondKeyType());
    //                ps.setString(12, validationBean.getSecondsaaSSource());
    //                ps.setString(13, validationBean.getSecondreportName());
    //                ps.setString(14, validationBean.getSecondparameter1Name());
    //                ps.setString(15, validationBean.getSecondparameter2Name());
    //                ps.setString(16, validationBean.getSecondparameter3Name());
    //                ps.setString(17, validationBean.getSecondparameter4Name());
    //                ps.setString(18, validationBean.getSecondparameter5Name());
    //                ps.setString(19, validationBean.getTypeOFAction());
    //                ps.setString(20, validationBean.getActiontype());
    //                ps.setString(21, validationBean.getSaveResultToValidationsValues());
    //                ps.setString(22, validationBean.getValidationValuesName());
    //                rs = ps.executeQuery();
    //
    //            }else if(transactionType.equals("SaaS_Report_ACTION_USE_TO_ANOTHER_VALIDATION_No")){
    //
    //                query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?\n"
    //                        + "AND	SAAS_SOURCE = ?\n"
    //                        + "AND	REPORT_NAME = ?\n"
    //                        + "AND	PARAMETER1 = ?\n"
    //                        + "AND	PARAMETER2 = ?\n"
    //                        + "AND	PARAMETER3 = ?\n"
    //                        + "AND	PARAMETER4 = ?\n"
    //                        + "AND	PARAMETER5 = ?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND	SECOND_SAAS_SOURCE = ?\n"
    //                        + "AND	SECOND_REPORT_NAME = ?\n"
    //                        + "AND	SECOND_PARAMETER1 = ?\n"
    //                        + "AND	SECOND_PARAMETER2 = ?\n"
    //                        + "AND	SECOND_PARAMETER3 = ?\n"
    //                        + "AND	SECOND_PARAMETER4 = ?\n"
    //                        + "AND	SECOND_PARAMETER5 = ?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND	SAVE_RESULT = ?\n";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getSaaSSource());
    //                ps.setString(4, validationBean.getReportName());
    //                ps.setString(5, validationBean.getParameter1Name());
    //                ps.setString(6, validationBean.getParameter2Name());
    //                ps.setString(7, validationBean.getParameter3Name());
    //                ps.setString(8, validationBean.getParameter4Name());
    //                ps.setString(9, validationBean.getParameter5Name());   //go
    //                ps.setString(10, validationBean.getOperation());
    //                ps.setString(11, validationBean.getSecondKeyType());
    //                ps.setString(12, validationBean.getSecondsaaSSource());
    //                ps.setString(13, validationBean.getSecondreportName());
    //                ps.setString(14, validationBean.getSecondparameter1Name());
    //                ps.setString(15, validationBean.getSecondparameter2Name());
    //                ps.setString(16, validationBean.getSecondparameter3Name());
    //                ps.setString(17, validationBean.getSecondparameter4Name());
    //                ps.setString(18, validationBean.getSecondparameter5Name());
    //                ps.setString(19, validationBean.getTypeOFAction());
    //                ps.setString(20, validationBean.getActiontype());
    //                ps.setString(21, validationBean.getSaveResultToValidationsValues());
    //                rs = ps.executeQuery();
    //            }else if(transactionType.equals("SaaS_Report_ACTION_Hide_Disabled_No")){
    //
    //                  query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?\n"
    //                        + "AND	SAAS_SOURCE = ?\n"
    //                        + "AND	REPORT_NAME = ?\n"
    //                        + "AND	PARAMETER1 = ?\n"
    //                        + "AND	PARAMETER2 = ?\n"
    //                        + "AND	PARAMETER3 = ?\n"
    //                        + "AND	PARAMETER4 = ?\n"
    //                        + "AND	PARAMETER5 = ?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND	SECOND_SAAS_SOURCE = ?\n"
    //                        + "AND	SECOND_REPORT_NAME = ?\n"
    //                        + "AND	SECOND_PARAMETER1 = ?\n"
    //                        + "AND	SECOND_PARAMETER2 = ?\n"
    //                        + "AND	SECOND_PARAMETER3 = ?\n"
    //                        + "AND	SECOND_PARAMETER4 = ?\n"
    //                        + "AND	SECOND_PARAMETER5 = ?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND  EITSEGMENT=?\n"
    //                        + "AND	SAVE_RESULT = ?\n";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getSaaSSource());
    //                ps.setString(4, validationBean.getReportName());
    //                ps.setString(5, validationBean.getParameter1Name());
    //                ps.setString(6, validationBean.getParameter2Name());
    //                ps.setString(7, validationBean.getParameter3Name());
    //                ps.setString(8, validationBean.getParameter4Name());
    //                ps.setString(9, validationBean.getParameter5Name());   //go
    //                ps.setString(10, validationBean.getOperation());
    //                ps.setString(11, validationBean.getSecondKeyType());
    //                ps.setString(12, validationBean.getSecondsaaSSource());
    //                ps.setString(13, validationBean.getSecondreportName());
    //                ps.setString(14, validationBean.getSecondparameter1Name());
    //                ps.setString(15, validationBean.getSecondparameter2Name());
    //                ps.setString(16, validationBean.getSecondparameter3Name());
    //                ps.setString(17, validationBean.getSecondparameter4Name());
    //                ps.setString(18, validationBean.getSecondparameter5Name());
    //                ps.setString(19, validationBean.getTypeOFAction());
    //                ps.setString(20, validationBean.getActiontype());
    //                ps.setString(21, validationBean.getEitSegment());
    //                ps.setString(22, validationBean.getSaveResultToValidationsValues());
    //                rs = ps.executeQuery();
    //
    //            }else if(transactionType.equals("SaaS_Report_ACTION_Hide_Disabled_Yes")){
    //
    //
    //                  query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?\n"
    //                        + "AND	SAAS_SOURCE = ?\n"
    //                        + "AND	REPORT_NAME = ?\n"
    //                        + "AND	PARAMETER1 = ?\n"
    //                        + "AND	PARAMETER2 = ?\n"
    //                        + "AND	PARAMETER3 = ?\n"
    //                        + "AND	PARAMETER4 = ?\n"
    //                        + "AND	PARAMETER5 = ?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND	SECOND_SAAS_SOURCE = ?\n"
    //                        + "AND	SECOND_REPORT_NAME = ?\n"
    //                        + "AND	SECOND_PARAMETER1 = ?\n"
    //                        + "AND	SECOND_PARAMETER2 = ?\n"
    //                        + "AND	SECOND_PARAMETER3 = ?\n"
    //                        + "AND	SECOND_PARAMETER4 = ?\n"
    //                        + "AND	SECOND_PARAMETER5 = ?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND  EITSEGMENT=?\n"
    //                        + "AND	SAVE_RESULT = ?\n"
    //                        + "AND VALIDATION_VALUES_NAME=?\n"  ;
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getSaaSSource());
    //                ps.setString(4, validationBean.getReportName());
    //                ps.setString(5, validationBean.getParameter1Name());
    //                ps.setString(6, validationBean.getParameter2Name());
    //                ps.setString(7, validationBean.getParameter3Name());
    //                ps.setString(8, validationBean.getParameter4Name());
    //                ps.setString(9, validationBean.getParameter5Name());   //go
    //                ps.setString(10, validationBean.getOperation());
    //                ps.setString(11, validationBean.getSecondKeyType());
    //                ps.setString(12, validationBean.getSecondsaaSSource());
    //                ps.setString(13, validationBean.getSecondreportName());
    //                ps.setString(14, validationBean.getSecondparameter1Name());
    //                ps.setString(15, validationBean.getSecondparameter2Name());
    //                ps.setString(16, validationBean.getSecondparameter3Name());
    //                ps.setString(17, validationBean.getSecondparameter4Name());
    //                ps.setString(18, validationBean.getSecondparameter5Name());
    //                ps.setString(19, validationBean.getTypeOFAction());
    //                ps.setString(20, validationBean.getActiontype());
    //                ps.setString(21, validationBean.getEitSegment());
    //                ps.setString(22, validationBean.getSaveResultToValidationsValues());
    //                ps.setString(23, validationBean.getValidationValuesName());
    //                rs = ps.executeQuery();
    //
    //
    //            } else if(transactionType.equals("SaaS_Report_ACTION_set_value_static_Yes")){
    //
    //                 query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?\n"
    //                        + "AND	SAAS_SOURCE = ?\n"
    //                        + "AND	REPORT_NAME = ?\n"
    //                        + "AND	PARAMETER1 = ?\n"
    //                        + "AND	PARAMETER2 = ?\n"
    //                        + "AND	PARAMETER3 = ?\n"
    //                        + "AND	PARAMETER4 = ?\n"
    //                        + "AND	PARAMETER5 = ?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND	SECOND_SAAS_SOURCE = ?\n"
    //                        + "AND	SECOND_REPORT_NAME = ?\n"
    //                        + "AND	SECOND_PARAMETER1 = ?\n"
    //                        + "AND	SECOND_PARAMETER2 = ?\n"
    //                        + "AND	SECOND_PARAMETER3 = ?\n"
    //                        + "AND	SECOND_PARAMETER4 = ?\n"
    //                        + "AND	SECOND_PARAMETER5 = ?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND  EITSEGMENT=?"
    //                        + "AND  TYPE_OF_VALUE=?"
    //                        + "AND  NEWVALUE=?\n"
    //                        + "AND	SAVE_RESULT = ?\n"
    //                        + "AND VALIDATION_VALUES_NAME=?\n"  ;
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getSaaSSource());
    //                ps.setString(4, validationBean.getReportName());
    //                ps.setString(5, validationBean.getParameter1Name());
    //                ps.setString(6, validationBean.getParameter2Name());
    //                ps.setString(7, validationBean.getParameter3Name());
    //                ps.setString(8, validationBean.getParameter4Name());
    //                ps.setString(9, validationBean.getParameter5Name());   //go
    //                ps.setString(10, validationBean.getOperation());
    //                ps.setString(11, validationBean.getSecondKeyType());
    //                ps.setString(12, validationBean.getSecondsaaSSource());
    //                ps.setString(13, validationBean.getSecondreportName());
    //                ps.setString(14, validationBean.getSecondparameter1Name());
    //                ps.setString(15, validationBean.getSecondparameter2Name());
    //                ps.setString(16, validationBean.getSecondparameter3Name());
    //                ps.setString(17, validationBean.getSecondparameter4Name());
    //                ps.setString(18, validationBean.getSecondparameter5Name());
    //                ps.setString(19, validationBean.getTypeOFAction());
    //                ps.setString(20, validationBean.getActiontype());
    //                ps.setString(21, validationBean.getEitSegment());
    //                ps.setString(22, validationBean.getTypeOfValue());
    //                ps.setString(23, validationBean.getNewValue());
    //                ps.setString(24, validationBean.getSaveResultToValidationsValues());
    //                ps.setString(25, validationBean.getValidationValuesName());
    //                rs = ps.executeQuery();
    //
    //
    //            }else if (transactionType.equals("SaaS_Report_ACTION_set_value_static_No")){
    //              query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?\n"
    //                        + "AND	SAAS_SOURCE = ?\n"
    //                        + "AND	REPORT_NAME = ?\n"
    //                        + "AND	PARAMETER1 = ?\n"
    //                        + "AND	PARAMETER2 = ?\n"
    //                        + "AND	PARAMETER3 = ?\n"
    //                        + "AND	PARAMETER4 = ?\n"
    //                        + "AND	PARAMETER5 = ?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND	SECOND_SAAS_SOURCE = ?\n"
    //                        + "AND	SECOND_REPORT_NAME = ?\n"
    //                        + "AND	SECOND_PARAMETER1 = ?\n"
    //                        + "AND	SECOND_PARAMETER2 = ?\n"
    //                        + "AND	SECOND_PARAMETER3 = ?\n"
    //                        + "AND	SECOND_PARAMETER4 = ?\n"
    //                        + "AND	SECOND_PARAMETER5 = ?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND  EITSEGMENT=?"
    //                        + "AND  TYPE_OF_VALUE=?"
    //                        + "AND  NEWVALUE=?\n"
    //                        + "AND	SAVE_RESULT = ?\n";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getSaaSSource());
    //                ps.setString(4, validationBean.getReportName());
    //                ps.setString(5, validationBean.getParameter1Name());
    //                ps.setString(6, validationBean.getParameter2Name());
    //                ps.setString(7, validationBean.getParameter3Name());
    //                ps.setString(8, validationBean.getParameter4Name());
    //                ps.setString(9, validationBean.getParameter5Name());   //go
    //                ps.setString(10, validationBean.getOperation());
    //                ps.setString(11, validationBean.getSecondKeyType());
    //                ps.setString(12, validationBean.getSecondsaaSSource());
    //                ps.setString(13, validationBean.getSecondreportName());
    //                ps.setString(14, validationBean.getSecondparameter1Name());
    //                ps.setString(15, validationBean.getSecondparameter2Name());
    //                ps.setString(16, validationBean.getSecondparameter3Name());
    //                ps.setString(17, validationBean.getSecondparameter4Name());
    //                ps.setString(18, validationBean.getSecondparameter5Name());
    //                ps.setString(19, validationBean.getTypeOFAction());
    //                ps.setString(20, validationBean.getActiontype());
    //                ps.setString(21, validationBean.getEitSegment());
    //                ps.setString(22, validationBean.getTypeOfValue());
    //                ps.setString(23, validationBean.getNewValue());
    //                ps.setString(24, validationBean.getSaveResultToValidationsValues());
    //                rs = ps.executeQuery();
    //
    //            } else if(transactionType.equals("SaaS_Report_ACTION_set_value_complex_Yes")){
    //
    //                  query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?\n"
    //                        + "AND	SAAS_SOURCE = ?\n"
    //                        + "AND	REPORT_NAME = ?\n"
    //                        + "AND	PARAMETER1 = ?\n"
    //                        + "AND	PARAMETER2 = ?\n"
    //                        + "AND	PARAMETER3 = ?\n"
    //                        + "AND	PARAMETER4 = ?\n"
    //                        + "AND	PARAMETER5 = ?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND	SECOND_SAAS_SOURCE = ?\n"
    //                        + "AND	SECOND_REPORT_NAME = ?\n"
    //                        + "AND	SECOND_PARAMETER1 = ?\n"
    //                        + "AND	SECOND_PARAMETER2 = ?\n"
    //                        + "AND	SECOND_PARAMETER3 = ?\n"
    //                        + "AND	SECOND_PARAMETER4 = ?\n"
    //                        + "AND	SECOND_PARAMETER5 = ?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND  EITSEGMENT=?"
    //                        + "AND  TYPE_OF_VALUE=?"
    //                        + "AND  TYPE_OF_FRIST_VALUE=?\n"
    //                        + "AND  FRIST_VALUES =?\n"
    //                        + "AND  SECOND_OPERATION=?\n"
    //                        + "AND  TYPE_OF_SECOND_VALUE=?\n"
    //                        + "AND  SECOND_VALUE=?\n"
    //                        + "AND	SAVE_RESULT = ?\n"
    //                        + "AND VALIDATION_VALUES_NAME=?\n"  ;
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getSaaSSource());
    //                ps.setString(4, validationBean.getReportName());
    //                ps.setString(5, validationBean.getParameter1Name());
    //                ps.setString(6, validationBean.getParameter2Name());
    //                ps.setString(7, validationBean.getParameter3Name());
    //                ps.setString(8, validationBean.getParameter4Name());
    //                ps.setString(9, validationBean.getParameter5Name());   //go
    //                ps.setString(10, validationBean.getOperation());
    //                ps.setString(11, validationBean.getSecondKeyType());
    //                ps.setString(12, validationBean.getSecondsaaSSource());
    //                ps.setString(13, validationBean.getSecondreportName());
    //                ps.setString(14, validationBean.getSecondparameter1Name());
    //                ps.setString(15, validationBean.getSecondparameter2Name());
    //                ps.setString(16, validationBean.getSecondparameter3Name());
    //                ps.setString(17, validationBean.getSecondparameter4Name());
    //                ps.setString(18, validationBean.getSecondparameter5Name());
    //                ps.setString(19, validationBean.getTypeOFAction());
    //                ps.setString(20, validationBean.getActiontype());
    //                ps.setString(21, validationBean.getEitSegment());
    //                ps.setString(22, validationBean.getTypeOfValue());
    //                ps.setString(23, validationBean.getTypeOfFristValue());
    //                ps.setString(24, validationBean.getFristValues());
    //                ps.setString(25, validationBean.getSecondOperation());
    //                ps.setString(26, validationBean.getTypeOfSecondValue() );
    //                ps.setString(27, validationBean.getSecondValue());
    //                ps.setString(28, validationBean.getSaveResultToValidationsValues());
    //                ps.setString(29, validationBean.getValidationValuesName());
    //                rs = ps.executeQuery();
    //
    //            }else if(transactionType.equals("SaaS_Report_ACTION_set_value_complex_No")){
    //
    //                 query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?\n"
    //                        + "AND	SAAS_SOURCE = ?\n"
    //                        + "AND	REPORT_NAME = ?\n"
    //                        + "AND	PARAMETER1 = ?\n"
    //                        + "AND	PARAMETER2 = ?\n"
    //                        + "AND	PARAMETER3 = ?\n"
    //                        + "AND	PARAMETER4 = ?\n"
    //                        + "AND	PARAMETER5 = ?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND	SECOND_SAAS_SOURCE = ?\n"
    //                        + "AND	SECOND_REPORT_NAME = ?\n"
    //                        + "AND	SECOND_PARAMETER1 = ?\n"
    //                        + "AND	SECOND_PARAMETER2 = ?\n"
    //                        + "AND	SECOND_PARAMETER3 = ?\n"
    //                        + "AND	SECOND_PARAMETER4 = ?\n"
    //                        + "AND	SECOND_PARAMETER5 = ?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND  EITSEGMENT=?"
    //                        + "AND  TYPE_OF_VALUE=?"
    //                        + "AND  TYPE_OF_FRIST_VALUE=?\n"
    //                        + "AND  FRIST_VALUES =?\n"
    //                        + "AND  SECOND_OPERATION=?\n"
    //                        + "AND  TYPE_OF_SECOND_VALUE=?\n"
    //                        + "AND  SECOND_VALUE=?\n"
    //                        + "AND	SAVE_RESULT = ?\n";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getSaaSSource());
    //                ps.setString(4, validationBean.getReportName());
    //                ps.setString(5, validationBean.getParameter1Name());
    //                ps.setString(6, validationBean.getParameter2Name());
    //                ps.setString(7, validationBean.getParameter3Name());
    //                ps.setString(8, validationBean.getParameter4Name());
    //                ps.setString(9, validationBean.getParameter5Name());   //go
    //                ps.setString(10, validationBean.getOperation());
    //                ps.setString(11, validationBean.getSecondKeyType());
    //                ps.setString(12, validationBean.getSecondsaaSSource());
    //                ps.setString(13, validationBean.getSecondreportName());
    //                ps.setString(14, validationBean.getSecondparameter1Name());
    //                ps.setString(15, validationBean.getSecondparameter2Name());
    //                ps.setString(16, validationBean.getSecondparameter3Name());
    //                ps.setString(17, validationBean.getSecondparameter4Name());
    //                ps.setString(18, validationBean.getSecondparameter5Name());
    //                ps.setString(19, validationBean.getTypeOFAction());
    //                ps.setString(20, validationBean.getActiontype());
    //                ps.setString(21, validationBean.getEitSegment());
    //                ps.setString(22, validationBean.getTypeOfValue());
    //                ps.setString(23, validationBean.getTypeOfFristValue());
    //                ps.setString(24, validationBean.getFristValues());
    //                ps.setString(25, validationBean.getSecondOperation());
    //                ps.setString(26, validationBean.getTypeOfSecondValue() );
    //                ps.setString(27, validationBean.getSecondValue());
    //                ps.setString(28, validationBean.getSaveResultToValidationsValues());
    //                rs = ps.executeQuery();
    //
    //            }else if(transactionType.equals("SaaS_Query_Error_Massage_Yes")){
    //
    //              query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND SAAS_SOURCE =?\n"
    //                        + "AND SAAS_QUERY=?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND  SECOND_SAAS_SOURCE=?\n"
    //                        + "AND  SECOND_SAAS_QUERY=?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND	ERROR_MESSAGE_EN = ?\n"
    //                        + "AND	ERROR_MESSAGE_AR = ?\n"
    //                        + "AND	SAVE_RESULT = ?\n"
    //                        + "AND VALIDATION_VALUES_NAME=?";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getSaaSSource());
    //                ps.setString(4, validationBean.getSaaSQuery());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getSecondsaaSSource());
    //                ps.setString(8, validationBean.getSecondsaaSQuery());
    //                ps.setString(9, validationBean.getTypeOFAction());
    //                ps.setString(10, validationBean.getErrorMassageEn());
    //                ps.setString(11, validationBean.getErrorMassageAr());
    //                ps.setString(12, validationBean.getSaveResultToValidationsValues());
    //                ps.setString(13, validationBean.getValidationValuesName());
    //                rs = ps.executeQuery();
    //
    //
    //            }else if(transactionType.equals("SaaS_Query_Error_Massage_No")){
    //
    //                  query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND SAAS_SOURCE =?\n"
    //                        + "AND SAAS_QUERY=?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND  SECOND_SAAS_SOURCE=?\n"
    //                        + "AND  SECOND_SAAS_QUERY=?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND	ERROR_MESSAGE_EN = ?\n"
    //                        + "AND	ERROR_MESSAGE_AR = ?\n"
    //                        + "AND	SAVE_RESULT = ?";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getSaaSSource());
    //                ps.setString(4, validationBean.getSaaSQuery());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getSecondsaaSSource());
    //                ps.setString(8, validationBean.getSecondsaaSQuery());
    //                ps.setString(9, validationBean.getTypeOFAction());
    //                ps.setString(10, validationBean.getErrorMassageEn());
    //                ps.setString(11, validationBean.getErrorMassageAr());
    //                ps.setString(12, validationBean.getSaveResultToValidationsValues());
    //                rs = ps.executeQuery();
    //
    //            } else if(transactionType.equals("SaaS_Query_ACTION_USE_TO_ANOTHER_VALIDATION_Yes")){
    //
    //                 query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND SAAS_SOURCE =?\n"
    //                        + "AND SAAS_QUERY=?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND  SECOND_SAAS_SOURCE=?\n"
    //                        + "AND  SECOND_SAAS_QUERY=?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND	SAVE_RESULT = ?\n"
    //                        + "AND VALIDATION_VALUES_NAME=?";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getSaaSSource());
    //                ps.setString(4, validationBean.getSaaSQuery());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getSecondsaaSSource());
    //                ps.setString(8, validationBean.getSecondsaaSQuery());
    //                ps.setString(9, validationBean.getTypeOFAction());
    //                ps.setString(10, validationBean.getActiontype());
    //                ps.setString(11, validationBean.getSaveResultToValidationsValues());
    //                ps.setString(12, validationBean.getValidationValuesName());
    //                rs = ps.executeQuery();
    //
    //            }else if(transactionType.equals("SaaS_Query_ACTION_USE_TO_ANOTHER_VALIDATION_No")){
    //
    //                 query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND SAAS_SOURCE =?\n"
    //                        + "AND SAAS_QUERY=?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND  SECOND_SAAS_SOURCE=?\n"
    //                        + "AND  SECOND_SAAS_QUERY=?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND	SAVE_RESULT = ?\n";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getSaaSSource());
    //                ps.setString(4, validationBean.getSaaSQuery());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getSecondsaaSSource());
    //                ps.setString(8, validationBean.getSecondsaaSQuery());
    //                ps.setString(9, validationBean.getTypeOFAction());
    //                ps.setString(10, validationBean.getActiontype());
    //                ps.setString(11, validationBean.getSaveResultToValidationsValues());
    //                rs = ps.executeQuery();
    //            }else if(transactionType.equals("SaaS_Query_ACTION_Hide_Disabled_Yes")){
    //
    //                query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND  SAAS_SOURCE =?\n"
    //                        + "AND  SAAS_QUERY=?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND  SECOND_SAAS_SOURCE=?\n"
    //                        + "AND  SECOND_SAAS_QUERY=?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND  EITSEGMENT=?\n"
    //                        + "AND	SAVE_RESULT = ?\n"
    //                        + "AND  VALIDATION_VALUES_NAME=?";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getSaaSSource());
    //                ps.setString(4, validationBean.getSaaSQuery());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getSecondsaaSSource());
    //                ps.setString(8, validationBean.getSecondsaaSQuery());
    //                ps.setString(9, validationBean.getTypeOFAction());
    //                ps.setString(10, validationBean.getActiontype());
    //                ps.setString(11, validationBean.getEitSegment());
    //                ps.setString(12, validationBean.getSaveResultToValidationsValues());
    //                ps.setString(13, validationBean.getValidationValuesName());
    //                rs = ps.executeQuery();
    //
    //            }else if(transactionType.equals("SaaS_Query_ACTION_Hide_Disabled_No")){
    //
    //                query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND  SAAS_SOURCE =?\n"
    //                        + "AND  SAAS_QUERY=?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND  SECOND_SAAS_SOURCE=?\n"
    //                        + "AND  SECOND_SAAS_QUERY=?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND  EITSEGMENT=?\n"
    //                        + "AND	SAVE_RESULT = ?\n";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getSaaSSource());
    //                ps.setString(4, validationBean.getSaaSQuery());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getSecondsaaSSource());
    //                ps.setString(8, validationBean.getSecondsaaSQuery());
    //                ps.setString(9, validationBean.getTypeOFAction());
    //                ps.setString(10, validationBean.getActiontype());
    //                ps.setString(11, validationBean.getEitSegment());
    //                ps.setString(12, validationBean.getSaveResultToValidationsValues());
    //                rs = ps.executeQuery();
    //
    //            }else if(transactionType.equals("SaaS_Query_ACTION_set_value_static_Yes")){
    //
    //                 query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND  SAAS_SOURCE =?\n"
    //                        + "AND  SAAS_QUERY=?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND  SECOND_SAAS_SOURCE=?\n"
    //                        + "AND  SECOND_SAAS_QUERY=?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND  ACTIONTYPE=?"
    //                        + "AND  EITSEGMENT=?\n"
    //                        + "AND  TYPE_OF_VALUE=?\n"
    //                        + "AND  NEWVALUE=?\n"
    //                        + "AND	SAVE_RESULT = ?\n"
    //                        + "AND  VALIDATION_VALUES_NAME=?";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getSaaSSource());
    //                ps.setString(4, validationBean.getSaaSQuery());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getSecondsaaSSource());
    //                ps.setString(8, validationBean.getSecondsaaSQuery());
    //                ps.setString(9, validationBean.getTypeOFAction());
    //                ps.setString(10, validationBean.getActiontype());
    //                ps.setString(11, validationBean.getEitSegment());
    //                ps.setString(12, validationBean.getTypeOfValue());
    //                ps.setString(13, validationBean.getNewValue());
    //                ps.setString(14, validationBean.getSaveResultToValidationsValues());
    //                ps.setString(15, validationBean.getValidationValuesName());
    //                rs = ps.executeQuery();
    //
    //            }else if(transactionType.equals("SaaS_Query_ACTION_set_value_static_No")){
    //
    //                query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND  SAAS_SOURCE =?\n"
    //                        + "AND  SAAS_QUERY=?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND  SECOND_SAAS_SOURCE=?\n"
    //                        + "AND  SECOND_SAAS_QUERY=?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND  ACTIONTYPE=?"
    //                        + "AND  EITSEGMENT=?\n"
    //                        + "AND  TYPE_OF_VALUE=?\n"
    //                        + "AND  NEWVALUE=?\n"
    //                        + "AND	SAVE_RESULT = ?\n";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getSaaSSource());
    //                ps.setString(4, validationBean.getSaaSQuery());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getSecondsaaSSource());
    //                ps.setString(8, validationBean.getSecondsaaSQuery());
    //                ps.setString(9, validationBean.getTypeOFAction());
    //                ps.setString(10, validationBean.getActiontype());
    //                ps.setString(11, validationBean.getEitSegment());
    //                ps.setString(12, validationBean.getTypeOfValue());
    //                ps.setString(13, validationBean.getNewValue());
    //                ps.setString(14, validationBean.getSaveResultToValidationsValues());
    //                rs = ps.executeQuery();
    //
    //            }else if(transactionType.equals("SaaS_Query_ACTION_set_value_complex_Yes")){
    //
    //                query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND  SAAS_SOURCE =?\n"
    //                        + "AND  SAAS_QUERY=?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND  SECOND_SAAS_SOURCE=?\n"
    //                        + "AND  SECOND_SAAS_QUERY=?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND  ACTIONTYPE=?"
    //                        + "AND  EITSEGMENT=?\n"
    //                        + "AND  TYPE_OF_VALUE=?\n"
    //                        + "AND  TYPE_OF_FRIST_VALUE=?\n"
    //                        + "AND  FRIST_VALUES =?\n"
    //                        + "AND  SECOND_OPERATION=?\n"
    //                        + "AND  TYPE_OF_SECOND_VALUE=?\n"
    //                        + "AND  SECOND_VALUE=?\n"
    //                        + "AND	SAVE_RESULT = ?\n"
    //                        + "AND  VALIDATION_VALUES_NAME=?";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getSaaSSource());
    //                ps.setString(4, validationBean.getSaaSQuery());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getSecondsaaSSource());
    //                ps.setString(8, validationBean.getSecondsaaSQuery());
    //                ps.setString(9, validationBean.getTypeOFAction());
    //                ps.setString(10, validationBean.getActiontype());
    //                ps.setString(11, validationBean.getEitSegment());
    //                ps.setString(12, validationBean.getTypeOfValue());
    //                ps.setString(13, validationBean.getTypeOfFristValue());
    //                ps.setString(14, validationBean.getFristValues());
    //                ps.setString(15, validationBean.getSecondOperation());
    //                ps.setString(16, validationBean.getTypeOfSecondValue());
    //                ps.setString(17, validationBean.getSecondValue());
    //                ps.setString(18, validationBean.getSaveResultToValidationsValues());
    //                ps.setString(19, validationBean.getValidationValuesName());
    //                rs = ps.executeQuery();
    //
    //            }else if(transactionType.equals("SaaS_Query_ACTION_set_value_complex_No")){
    //
    //
    //                query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND  SAAS_SOURCE =?\n"
    //                        + "AND  SAAS_QUERY=?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND  SECOND_SAAS_SOURCE=?\n"
    //                        + "AND  SECOND_SAAS_QUERY=?\n"
    //                        + "AND	TYPE_OF_ACTION = ?\n"
    //                        + "AND  ACTIONTYPE=?"
    //                        + "AND  EITSEGMENT=?\n"
    //                        + "AND  TYPE_OF_VALUE=?\n"
    //                        + "AND  TYPE_OF_FRIST_VALUE=?\n"
    //                        + "AND  FRIST_VALUES =?\n"
    //                        + "AND  SECOND_OPERATION=?\n"
    //                        + "AND  TYPE_OF_SECOND_VALUE=?\n"
    //                        + "AND  SECOND_VALUE=?\n"
    //                        + "AND	SAVE_RESULT = ?\n";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getSaaSSource());
    //                ps.setString(4, validationBean.getSaaSQuery());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getSecondsaaSSource());
    //                ps.setString(8, validationBean.getSecondsaaSQuery());
    //                ps.setString(9, validationBean.getTypeOFAction());
    //                ps.setString(10, validationBean.getActiontype());
    //                ps.setString(11, validationBean.getEitSegment());
    //                ps.setString(12, validationBean.getTypeOfValue());
    //                ps.setString(13, validationBean.getTypeOfFristValue());
    //                ps.setString(14, validationBean.getFristValues());
    //                ps.setString(15, validationBean.getSecondOperation());
    //                ps.setString(16, validationBean.getTypeOfSecondValue());
    //                ps.setString(17, validationBean.getSecondValue());
    //                ps.setString(18, validationBean.getSaveResultToValidationsValues());
    //                rs = ps.executeQuery();
    //
    //            }else if(transactionType.equals("Application_Value_Static_Error_Massage_Yes")){
    //
    //              query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND  APPLICATION_VALUE_FAMILY =?\n"
    //                        + "AND  APPLICATION_KEY =?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND  SECOND_FIRST_KEY_VALUE=?\n"
    //                        + "AND	TYPE_OF_ACTION =?\n"
    //                        + "AND  ERROR_MESSAGE_EN =?\n"
    //                        + "AND  ERROR_MESSAGE_AR=?\n"
    //                        + "AND	SAVE_RESULT = ?\n"
    //                        + "AND  VALIDATION_VALUES_NAME=?";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getApplicationValueFamily());
    //                ps.setString(4, validationBean.getApplicationKey());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getSecondKeyValue());
    //                ps.setString(8, validationBean.getTypeOFAction());
    //                ps.setString(9, validationBean.getErrorMassageEn());
    //                ps.setString(10, validationBean.getErrorMassageAr());
    //                ps.setString(11, validationBean.getSaveResultToValidationsValues());
    //                ps.setString(12, validationBean.getValidationValuesName());
    //                rs = ps.executeQuery();
    //
    //
    //            }else if(transactionType.equals("Application_Value_Static_Error_Massage_No") ){
    //
    //                query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND  APPLICATION_VALUE_FAMILY =?\n"
    //                        + "AND  APPLICATION_KEY =?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND  SECOND_FIRST_KEY_VALUE=?\n"
    //                        + "AND	TYPE_OF_ACTION =?\n"
    //                        + "AND  ERROR_MESSAGE_EN =?\n"
    //                        + "AND  ERROR_MESSAGE_AR=?\n"
    //                        + "AND	SAVE_RESULT = ?\n";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getApplicationValueFamily());
    //                ps.setString(4, validationBean.getApplicationKey());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getSecondKeyValue());
    //                ps.setString(8, validationBean.getTypeOFAction());
    //                ps.setString(9, validationBean.getErrorMassageEn());
    //                ps.setString(10, validationBean.getErrorMassageAr());
    //                ps.setString(11, validationBean.getSaveResultToValidationsValues());
    //                rs = ps.executeQuery();
    //            }else if (transactionType.equals("Application_Value_Static_ACTION_USE_TO_ANOTHER_VALIDATION_Yes")){
    //               query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND  APPLICATION_VALUE_FAMILY =?\n"
    //                        + "AND  APPLICATION_KEY =?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND  SECOND_FIRST_KEY_VALUE=?\n"
    //                        + "AND	TYPE_OF_ACTION =?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND	SAVE_RESULT = ?\n"
    //                        + "AND  VALIDATION_VALUES_NAME=?";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getApplicationValueFamily());
    //                ps.setString(4, validationBean.getApplicationKey());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getSecondKeyValue());
    //                ps.setString(8, validationBean.getTypeOFAction());
    //                ps.setString(9, validationBean.getActiontype());
    //                ps.setString(10, validationBean.getSaveResultToValidationsValues());
    //                ps.setString(11, validationBean.getValidationValuesName());
    //                rs = ps.executeQuery();
    //
    //            }else if(transactionType.equals("Application_Value_Static_ACTION_USE_TO_ANOTHER_VALIDATION_No")){
    //
    //                query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND  APPLICATION_VALUE_FAMILY =?\n"
    //                        + "AND  APPLICATION_KEY =?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND  SECOND_FIRST_KEY_VALUE=?\n"
    //                        + "AND	TYPE_OF_ACTION =?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND	SAVE_RESULT = ?\n";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getApplicationValueFamily());
    //                ps.setString(4, validationBean.getApplicationKey());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getSecondKeyValue());
    //                ps.setString(8, validationBean.getTypeOFAction());
    //                ps.setString(9, validationBean.getActiontype());
    //                ps.setString(10, validationBean.getSaveResultToValidationsValues());
    //                rs = ps.executeQuery();
    //
    //            }else if(transactionType.equals("Application_Value_Static_Hide_Disabled_Yes")){
    //
    //                  query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND  APPLICATION_VALUE_FAMILY =?\n"
    //                        + "AND  APPLICATION_KEY =?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND  SECOND_FIRST_KEY_VALUE=?\n"
    //                        + "AND	TYPE_OF_ACTION =?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND  EITSEGMENT=?\n"
    //                        + "AND	SAVE_RESULT = ?\n"
    //                        + "AND  VALIDATION_VALUES_NAME=?";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getApplicationValueFamily());
    //                ps.setString(4, validationBean.getApplicationKey());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getSecondKeyValue());
    //                ps.setString(8, validationBean.getTypeOFAction());
    //                ps.setString(9, validationBean.getActiontype());
    //                ps.setString(10, validationBean.getEitSegment());
    //                ps.setString(11, validationBean.getSaveResultToValidationsValues());
    //                ps.setString(12, validationBean.getValidationValuesName());
    //                rs = ps.executeQuery();
    //
    //            }else if(transactionType.equals("Application_Value_Static_Hide_Disabled_No")) {
    //
    //                   query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND  APPLICATION_VALUE_FAMILY =?\n"
    //                        + "AND  APPLICATION_KEY =?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND  SECOND_FIRST_KEY_VALUE=?\n"
    //                        + "AND	TYPE_OF_ACTION =?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND  EITSEGMENT=?\n"
    //                        + "AND	SAVE_RESULT = ?\n";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getApplicationValueFamily());
    //                ps.setString(4, validationBean.getApplicationKey());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getSecondKeyValue());
    //                ps.setString(8, validationBean.getTypeOFAction());
    //                ps.setString(9, validationBean.getActiontype());
    //                ps.setString(10, validationBean.getEitSegment());
    //                ps.setString(11, validationBean.getSaveResultToValidationsValues());
    //                rs = ps.executeQuery();
    //
    //            } else if(transactionType.equals("Application_Value_Static_ACTION_set_value_static_Yes")){
    //
    //                query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND  APPLICATION_VALUE_FAMILY =?\n"
    //                        + "AND  APPLICATION_KEY =?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND  SECOND_FIRST_KEY_VALUE=?\n"
    //                        + "AND	TYPE_OF_ACTION =?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND  EITSEGMENT=?\n"
    //                        + "AND  TYPE_OF_VALUE=?\n"
    //                        + "AND  NEWVALUE =?\n"
    //                        + "AND	SAVE_RESULT = ?\n"
    //                        + "AND  VALIDATION_VALUES_NAME=?";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getApplicationValueFamily());
    //                ps.setString(4, validationBean.getApplicationKey());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getSecondKeyValue());
    //                ps.setString(8, validationBean.getTypeOFAction());
    //                ps.setString(9, validationBean.getActiontype());
    //                ps.setString(10, validationBean.getEitSegment());
    //                ps.setString(11, validationBean.getTypeOfValue());
    //                ps.setString(12, validationBean.getNewValue());
    //                ps.setString(13, validationBean.getSaveResultToValidationsValues());
    //                ps.setString(14, validationBean.getValidationValuesName());
    //                rs = ps.executeQuery();
    //
    //            }else if(transactionType.equals("Application_Value_Static_ACTION_set_value_static_No")){
    //
    //                   query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND  APPLICATION_VALUE_FAMILY =?\n"
    //                        + "AND  APPLICATION_KEY =?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND  SECOND_FIRST_KEY_VALUE=?\n"
    //                        + "AND	TYPE_OF_ACTION =?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND  EITSEGMENT=?\n"
    //                        + "AND  TYPE_OF_VALUE=?\n"
    //                        + "AND  NEWVALUE =?\n"
    //                        + "AND	SAVE_RESULT = ?\n";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getApplicationValueFamily());
    //                ps.setString(4, validationBean.getApplicationKey());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getSecondKeyValue());
    //                ps.setString(8, validationBean.getTypeOFAction());
    //                ps.setString(9, validationBean.getActiontype());
    //                ps.setString(10, validationBean.getEitSegment());
    //                ps.setString(11, validationBean.getTypeOfValue());
    //                ps.setString(12, validationBean.getNewValue());
    //                ps.setString(13, validationBean.getSaveResultToValidationsValues());
    //                rs = ps.executeQuery();
    //
    //            }else if(transactionType.equals("Application_Value_Static_ACTION_set_value_complex_Yes")){
    //                query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND  APPLICATION_VALUE_FAMILY =?\n"
    //                        + "AND  APPLICATION_KEY =?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND  SECOND_FIRST_KEY_VALUE=?\n"
    //                        + "AND	TYPE_OF_ACTION =?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND  EITSEGMENT=?\n"
    //                        + "AND  TYPE_OF_VALUE=?\n"
    //                        + "AND  TYPE_OF_FRIST_VALUE=?\n"
    //                        + "AND  FRIST_VALUES =?\n"
    //                        + "AND  SECOND_OPERATION=?\n"
    //                        + "AND  TYPE_OF_SECOND_VALUE=?\n"
    //                        + "AND  SECOND_VALUE=?\n"
    //                        + "AND	SAVE_RESULT = ?\n"
    //                        + "AND  VALIDATION_VALUES_NAME=?";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getApplicationValueFamily());
    //                ps.setString(4, validationBean.getApplicationKey());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getSecondKeyValue());
    //                ps.setString(8, validationBean.getTypeOFAction());
    //                ps.setString(9, validationBean.getActiontype());
    //                ps.setString(10, validationBean.getEitSegment());
    //                ps.setString(11, validationBean.getTypeOfValue());
    //                ps.setString(12, validationBean.getTypeOfFristValue());
    //                ps.setString(13, validationBean.getFristValues());
    //                ps.setString(14, validationBean.getSecondOperation());
    //                ps.setString(15, validationBean.getTypeOfSecondValue());
    //                ps.setString(16, validationBean.getSecondValue());
    //                ps.setString(17, validationBean.getSaveResultToValidationsValues());
    //                ps.setString(18, validationBean.getValidationValuesName());
    //                rs = ps.executeQuery();
    //            }else if (transactionType.equals("Application_Value_Static_ACTION_set_value_complex_No")){
    //
    //                query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND  APPLICATION_VALUE_FAMILY =?\n"
    //                        + "AND  APPLICATION_KEY =?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND	SECOND_FIRST_KEY_TYPE = ?\n"
    //                        + "AND  SECOND_FIRST_KEY_VALUE=?\n"
    //                        + "AND	TYPE_OF_ACTION =?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND  EITSEGMENT=?\n"
    //                        + "AND  TYPE_OF_VALUE=?\n"
    //                        + "AND  TYPE_OF_FRIST_VALUE=?\n"
    //                        + "AND  FRIST_VALUES =?\n"
    //                        + "AND  SECOND_OPERATION=?\n"
    //                        + "AND  TYPE_OF_SECOND_VALUE=?\n"
    //                        + "AND  SECOND_VALUE=?\n"
    //                        + "AND	SAVE_RESULT = ?\n";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getApplicationValueFamily());
    //                ps.setString(4, validationBean.getApplicationKey());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getSecondKeyValue());
    //                ps.setString(8, validationBean.getTypeOFAction());
    //                ps.setString(9, validationBean.getActiontype());
    //                ps.setString(10, validationBean.getEitSegment());
    //                ps.setString(11, validationBean.getTypeOfValue());
    //                ps.setString(12, validationBean.getTypeOfFristValue());
    //                ps.setString(13, validationBean.getFristValues());
    //                ps.setString(14, validationBean.getSecondOperation());
    //                ps.setString(15, validationBean.getTypeOfSecondValue());
    //                ps.setString(16, validationBean.getSecondValue());
    //                ps.setString(17, validationBean.getSaveResultToValidationsValues());
    //                rs = ps.executeQuery();
    //
    //            } else if(transactionType.equals("Application_Value_SaaS_Report_Error_Massage_Yes")){
    //
    //                query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND  APPLICATION_VALUE_FAMILY =?\n"
    //                        + "AND  APPLICATION_KEY =?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND  SECOND_SAAS_SOURCE=?\n"
    //                        + "AND  SECOND_SAAS_QUERY=?\n"
    //                        + "AND  SECOND_REPORT_NAME=?\n"
    //                        + "AND	SECOND_PARAMETER1 = ?\n"
    //                        + "AND	SECOND_PARAMETER2 = ?\n"
    //                        + "AND	SECOND_PARAMETER3 = ?\n"
    //                        + "AND	SECOND_PARAMETER4 = ?\n"
    //                        + "AND	SECOND_PARAMETER5 = ?\n"
    //                        + "AND  TYPE_OF_ACTION =?\n"
    //                        + "AND  ERROR_MESSAGE_EN =?\n"
    //                        + "AND  ERROR_MESSAGE_AR=?\n"
    //                        + "AND  SAVE_RESULT=?\n"
    //                        + "AND  VALIDATION_VALUES_NAME=?\n";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getApplicationValueFamily());
    //                ps.setString(4, validationBean.getApplicationKey());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondsaaSSource());
    //                ps.setString(7, validationBean.getSecondsaaSQuery());
    //                ps.setString(8, validationBean.getSecondreportName());
    //                ps.setString(9, validationBean.getSecondparameter1Name());
    //                ps.setString(10, validationBean.getSecondparameter2Name());
    //                ps.setString(11, validationBean.getSecondparameter3Name());
    //                ps.setString(12, validationBean.getSecondparameter4Name());
    //                ps.setString(13, validationBean.getSecondparameter5Name());
    //                ps.setString(14, validationBean.getTypeOFAction());
    //                ps.setString(15, validationBean.getErrorMassageEn());
    //                ps.setString(16, validationBean.getErrorMassageAr());
    //                ps.setString(17, validationBean.getSaveResultToValidationsValues());
    //                ps.setString(18, validationBean.getValidationValuesName());
    //
    //                rs = ps.executeQuery();
    //
    //            }else if(transactionType.equals("Application_Value_SaaS_Report_Error_Massage_No")){
    //
    //              query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND  APPLICATION_VALUE_FAMILY =?\n"
    //                        + "AND  APPLICATION_KEY =?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND  SECOND_SAAS_SOURCE=?\n"
    //                        + "AND  SECOND_SAAS_QUERY=?\n"
    //                        + "AND  SECOND_REPORT_NAME=?\n"
    //                        + "AND	SECOND_PARAMETER1 = ?\n"
    //                        + "AND	SECOND_PARAMETER2 = ?\n"
    //                        + "AND	SECOND_PARAMETER3 = ?\n"
    //                        + "AND	SECOND_PARAMETER4 = ?\n"
    //                        + "AND	SECOND_PARAMETER5 = ?\n"
    //                        + "AND  TYPE_OF_ACTION =?\n"
    //                        + "AND  ERROR_MESSAGE_EN =?\n"
    //                        + "AND  ERROR_MESSAGE_AR=?\n"
    //                        + "AND  SAVE_RESULT=?\n";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getApplicationValueFamily());
    //                ps.setString(4, validationBean.getApplicationKey());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondsaaSSource());
    //                ps.setString(7, validationBean.getSecondsaaSQuery());
    //                ps.setString(8, validationBean.getSecondreportName());
    //                ps.setString(9, validationBean.getSecondparameter1Name());
    //                ps.setString(10, validationBean.getSecondparameter2Name());
    //                ps.setString(11, validationBean.getSecondparameter3Name());
    //                ps.setString(12, validationBean.getSecondparameter4Name());
    //                ps.setString(13, validationBean.getSecondparameter5Name());
    //                ps.setString(14, validationBean.getTypeOFAction());
    //                ps.setString(15, validationBean.getErrorMassageEn());
    //                ps.setString(16, validationBean.getErrorMassageAr());
    //                ps.setString(17, validationBean.getSaveResultToValidationsValues());
    //
    //                rs = ps.executeQuery();
    //            }else if(transactionType.equals("Application_Value_SaaS_Report_ACTION_USE_TO_ANOTHER_VALIDATION_Yes")){
    //
    //                 query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND  APPLICATION_VALUE_FAMILY =?\n"
    //                        + "AND  APPLICATION_KEY =?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND  SECOND_FIRST_KEY_TYPE=?\n"
    //                        + "AND  SECOND_SAAS_SOURCE=?\n"
    //                        + "AND  SECOND_REPORT_NAME=?\n"
    //                        + "AND	SECOND_PARAMETER1 = ?\n"
    //                        + "AND	SECOND_PARAMETER2 = ?\n"
    //                        + "AND	SECOND_PARAMETER3 = ?\n"
    //                        + "AND	SECOND_PARAMETER4 = ?\n"
    //                        + "AND	SECOND_PARAMETER5 = ?\n"
    //                        + "AND  TYPE_OF_ACTION =?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND  SAVE_RESULT=?\n"
    //                        + "AND  VALIDATION_VALUES_NAME=?\n";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getApplicationValueFamily());
    //                ps.setString(4, validationBean.getApplicationKey());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getSecondsaaSSource());
    //                ps.setString(8, validationBean.getSecondreportName());
    //                ps.setString(9, validationBean.getSecondparameter1Name());
    //                ps.setString(10, validationBean.getSecondparameter2Name());
    //                ps.setString(11, validationBean.getSecondparameter3Name());
    //                ps.setString(12, validationBean.getSecondparameter4Name());
    //                ps.setString(13, validationBean.getSecondparameter5Name());
    //                ps.setString(14, validationBean.getTypeOFAction());
    //                ps.setString(15, validationBean.getActiontype());
    //                ps.setString(16, validationBean.getSaveResultToValidationsValues());
    //                ps.setString(17, validationBean.getValidationValuesName());
    //
    //                rs = ps.executeQuery();
    //            }else if(transactionType.equals("Application_Value_SaaS_Report_ACTION_USE_TO_ANOTHER_VALIDATION_No")){
    //
    //                  query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND  APPLICATION_VALUE_FAMILY =?\n"
    //                        + "AND  APPLICATION_KEY =?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND  SECOND_FIRST_KEY_TYPE=?\n"
    //                        + "AND  SECOND_SAAS_SOURCE=?\n"
    //                        + "AND  SECOND_REPORT_NAME=?\n"
    //                        + "AND	SECOND_PARAMETER1 = ?\n"
    //                        + "AND	SECOND_PARAMETER2 = ?\n"
    //                        + "AND	SECOND_PARAMETER3 = ?\n"
    //                        + "AND	SECOND_PARAMETER4 = ?\n"
    //                        + "AND	SECOND_PARAMETER5 = ?\n"
    //                        + "AND  TYPE_OF_ACTION =?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND  SAVE_RESULT=?\n";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getApplicationValueFamily());
    //                ps.setString(4, validationBean.getApplicationKey());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getSecondsaaSSource());
    //                ps.setString(8, validationBean.getSecondreportName());
    //                ps.setString(9, validationBean.getSecondparameter1Name());
    //                ps.setString(10, validationBean.getSecondparameter2Name());
    //                ps.setString(11, validationBean.getSecondparameter3Name());
    //                ps.setString(12, validationBean.getSecondparameter4Name());
    //                ps.setString(13, validationBean.getSecondparameter5Name());
    //                ps.setString(14, validationBean.getTypeOFAction());
    //                ps.setString(15, validationBean.getActiontype());
    //                ps.setString(16, validationBean.getSaveResultToValidationsValues());
    //
    //                rs = ps.executeQuery();
    //            }else if(transactionType.equals("Application_Value_SaaS_Report_ACTION_Hide_Disable_Yes")){
    //                query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND  APPLICATION_VALUE_FAMILY =?\n"
    //                        + "AND  APPLICATION_KEY =?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND  SECOND_FIRST_KEY_TYPE=?\n"
    //                        + "AND  SECOND_SAAS_SOURCE=?\n"
    //                        + "AND  SECOND_REPORT_NAME=?\n"
    //                        + "AND	SECOND_PARAMETER1 = ?\n"
    //                        + "AND	SECOND_PARAMETER2 = ?\n"
    //                        + "AND	SECOND_PARAMETER3 = ?\n"
    //                        + "AND	SECOND_PARAMETER4 = ?\n"
    //                        + "AND	SECOND_PARAMETER5 = ?\n"
    //                        + "AND  TYPE_OF_ACTION =?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND  EITSEGMENT=?\n"
    //                        + "AND  SAVE_RESULT=?\n"
    //                        + "AND  VALIDATION_VALUES_NAME=?\n";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getApplicationValueFamily());
    //                ps.setString(4, validationBean.getApplicationKey());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getSecondsaaSSource());
    //                ps.setString(8, validationBean.getSecondreportName());
    //                ps.setString(9, validationBean.getSecondparameter1Name());
    //                ps.setString(10, validationBean.getSecondparameter2Name());
    //                ps.setString(11, validationBean.getSecondparameter3Name());
    //                ps.setString(12, validationBean.getSecondparameter4Name());
    //                ps.setString(13, validationBean.getSecondparameter5Name());
    //                ps.setString(14, validationBean.getTypeOFAction());
    //                ps.setString(15, validationBean.getActiontype());
    //                ps.setString(16, validationBean.getEitSegment());
    //                ps.setString(17, validationBean.getSaveResultToValidationsValues());
    //                ps.setString(18, validationBean.getValidationValuesName());
    //
    //                rs = ps.executeQuery();
    //            }else if(transactionType.equals("Application_Value_SaaS_Report_ACTION_Hide_Disable_No")){
    //
    //                  query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND  APPLICATION_VALUE_FAMILY =?\n"
    //                        + "AND  APPLICATION_KEY =?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND  SECOND_FIRST_KEY_TYPE=?\n"
    //                        + "AND  SECOND_SAAS_SOURCE=?\n"
    //                        + "AND  SECOND_REPORT_NAME=?\n"
    //                        + "AND	SECOND_PARAMETER1 = ?\n"
    //                        + "AND	SECOND_PARAMETER2 = ?\n"
    //                        + "AND	SECOND_PARAMETER3 = ?\n"
    //                        + "AND	SECOND_PARAMETER4 = ?\n"
    //                        + "AND	SECOND_PARAMETER5 = ?\n"
    //                        + "AND  TYPE_OF_ACTION =?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND  EITSEGMENT=?\n"
    //                        + "AND  SAVE_RESULT=?\n";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getApplicationValueFamily());
    //                ps.setString(4, validationBean.getApplicationKey());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getSecondsaaSSource());
    //                ps.setString(8, validationBean.getSecondreportName());
    //                ps.setString(9, validationBean.getSecondparameter1Name());
    //                ps.setString(10, validationBean.getSecondparameter2Name());
    //                ps.setString(11, validationBean.getSecondparameter3Name());
    //                ps.setString(12, validationBean.getSecondparameter4Name());
    //                ps.setString(13, validationBean.getSecondparameter5Name());
    //                ps.setString(14, validationBean.getTypeOFAction());
    //                ps.setString(15, validationBean.getActiontype());
    //                ps.setString(16, validationBean.getEitSegment());
    //                ps.setString(17, validationBean.getSaveResultToValidationsValues());
    //
    //                rs = ps.executeQuery();
    //
    //            }else if(transactionType.equals("Application_Value_SaaS_Report_ACTION_set_value_static_Yes")){
    //
    //                 query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND  APPLICATION_VALUE_FAMILY =?\n"
    //                        + "AND  APPLICATION_KEY =?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND  SECOND_FIRST_KEY_TYPE=?\n"
    //                        + "AND  SECOND_SAAS_SOURCE=?\n"
    //                        + "AND  SECOND_REPORT_NAME=?\n"
    //                        + "AND	SECOND_PARAMETER1 = ?\n"
    //                        + "AND	SECOND_PARAMETER2 = ?\n"
    //                        + "AND	SECOND_PARAMETER3 = ?\n"
    //                        + "AND	SECOND_PARAMETER4 = ?\n"
    //                        + "AND	SECOND_PARAMETER5 = ?\n"
    //                        + "AND  TYPE_OF_ACTION =?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND  EITSEGMENT=?\n"
    //                        + "AND  TYPE_OF_VALUE=?\n"
    //                        + "AND  NEWVALUE=?\n"
    //                        + "AND  SAVE_RESULT=?\n"
    //                        + "AND  VALIDATION_VALUES_NAME=?\n";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getApplicationValueFamily());
    //                ps.setString(4, validationBean.getApplicationKey());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getSecondsaaSSource());
    //                ps.setString(8, validationBean.getSecondreportName());
    //                ps.setString(9, validationBean.getSecondparameter1Name());
    //                ps.setString(10, validationBean.getSecondparameter2Name());
    //                ps.setString(11, validationBean.getSecondparameter3Name());
    //                ps.setString(12, validationBean.getSecondparameter4Name());
    //                ps.setString(13, validationBean.getSecondparameter5Name());
    //                ps.setString(14, validationBean.getTypeOFAction());
    //                ps.setString(15, validationBean.getActiontype());
    //                ps.setString(16, validationBean.getEitSegment());
    //                ps.setString(17, validationBean.getTypeOfValue());
    //                ps.setString(18, validationBean.getNewValue());
    //                ps.setString(19, validationBean.getSaveResultToValidationsValues());
    //                ps.setString(20, validationBean.getValidationValuesName());
    //
    //                rs = ps.executeQuery();
    //
    //            }else if(transactionType.equals("Application_Value_SaaS_Report_ACTION_set_value_static_No")){
    //
    //                  query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND  APPLICATION_VALUE_FAMILY =?\n"
    //                        + "AND  APPLICATION_KEY =?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND  SECOND_FIRST_KEY_TYPE=?\n"
    //                        + "AND  SECOND_SAAS_SOURCE=?\n"
    //                        + "AND  SECOND_REPORT_NAME=?\n"
    //                        + "AND	SECOND_PARAMETER1 = ?\n"
    //                        + "AND	SECOND_PARAMETER2 = ?\n"
    //                        + "AND	SECOND_PARAMETER3 = ?\n"
    //                        + "AND	SECOND_PARAMETER4 = ?\n"
    //                        + "AND	SECOND_PARAMETER5 = ?\n"
    //                        + "AND  TYPE_OF_ACTION =?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND  EITSEGMENT=?\n"
    //                        + "AND  TYPE_OF_VALUE=?\n"
    //                        + "AND  NEWVALUE=?\n"
    //                        + "AND  SAVE_RESULT=?\n";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getApplicationValueFamily());
    //                ps.setString(4, validationBean.getApplicationKey());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getSecondsaaSSource());
    //                ps.setString(8, validationBean.getSecondreportName());
    //                ps.setString(9, validationBean.getSecondparameter1Name());
    //                ps.setString(10, validationBean.getSecondparameter2Name());
    //                ps.setString(11, validationBean.getSecondparameter3Name());
    //                ps.setString(12, validationBean.getSecondparameter4Name());
    //                ps.setString(13, validationBean.getSecondparameter5Name());
    //                ps.setString(14, validationBean.getTypeOFAction());
    //                ps.setString(15, validationBean.getActiontype());
    //                ps.setString(16, validationBean.getEitSegment());
    //                ps.setString(17, validationBean.getTypeOfValue());
    //                ps.setString(18, validationBean.getNewValue());
    //                ps.setString(19, validationBean.getSaveResultToValidationsValues());
    //
    //                rs = ps.executeQuery();
    //            }else if(transactionType.equals("Application_Value_SaaS_Report_ACTION_set_value_complex_Yes")){
    //              query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND  APPLICATION_VALUE_FAMILY =?\n"
    //                        + "AND  APPLICATION_KEY =?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND  SECOND_FIRST_KEY_TYPE=?\n"
    //                        + "AND  SECOND_SAAS_SOURCE=?\n"
    //                        + "AND  SECOND_REPORT_NAME=?\n"
    //                        + "AND	SECOND_PARAMETER1 = ?\n"
    //                        + "AND	SECOND_PARAMETER2 = ?\n"
    //                        + "AND	SECOND_PARAMETER3 = ?\n"
    //                        + "AND	SECOND_PARAMETER4 = ?\n"
    //                        + "AND	SECOND_PARAMETER5 = ?\n"
    //                        + "AND  TYPE_OF_ACTION =?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND  EITSEGMENT=?\n"
    //                        + "AND  TYPE_OF_VALUE=?\n"
    //                        + "AND  TYPE_OF_FRIST_VALUE=?\n"
    //                        + "AND  FRIST_VALUES=?\n"
    //                        + "AND  SECOND_OPERATION=?\n"
    //                        + "AND  TYPE_OF_SECOND_VALUE=?\n"
    //                        + "AND  SECOND_VALUE=?\n"
    //                        + "AND  SAVE_RESULT=?\n"
    //                        + "AND  VALIDATION_VALUES_NAME=?\n";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getApplicationValueFamily());
    //                ps.setString(4, validationBean.getApplicationKey());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getSecondsaaSSource());
    //                ps.setString(8, validationBean.getSecondreportName());
    //                ps.setString(9, validationBean.getSecondparameter1Name());
    //                ps.setString(10, validationBean.getSecondparameter2Name());
    //                ps.setString(11, validationBean.getSecondparameter3Name());
    //                ps.setString(12, validationBean.getSecondparameter4Name());
    //                ps.setString(13, validationBean.getSecondparameter5Name());
    //                ps.setString(14, validationBean.getTypeOFAction());
    //                ps.setString(15, validationBean.getActiontype());
    //                ps.setString(16, validationBean.getEitSegment());
    //                ps.setString(17, validationBean.getTypeOfValue());
    //                ps.setString(18, validationBean.getTypeOfFristValue());
    //                ps.setString(19, validationBean.getFristValues());
    //                ps.setString(20, validationBean.getSecondOperation());
    //                ps.setString(21, validationBean.getTypeOfSecondValue());
    //                ps.setString(22, validationBean.getSecondValue());
    //                ps.setString(23, validationBean.getSaveResultToValidationsValues());
    //                ps.setString(24, validationBean.getValidationValuesName());
    //
    //                rs = ps.executeQuery();
    //            }else if (transactionType.equals("Application_Value_SaaS_Report_ACTION_set_value_complex_No")){
    //                query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?"
    //                        + "AND  APPLICATION_VALUE_FAMILY =?\n"
    //                        + "AND  APPLICATION_KEY =?\n"
    //                        + "AND	OPERATION = ?\n"
    //                        + "AND  SECOND_FIRST_KEY_TYPE=?\n"
    //                        + "AND  SECOND_SAAS_SOURCE=?\n"
    //                        + "AND  SECOND_REPORT_NAME=?\n"
    //                        + "AND	SECOND_PARAMETER1 = ?\n"
    //                        + "AND	SECOND_PARAMETER2 = ?\n"
    //                        + "AND	SECOND_PARAMETER3 = ?\n"
    //                        + "AND	SECOND_PARAMETER4 = ?\n"
    //                        + "AND	SECOND_PARAMETER5 = ?\n"
    //                        + "AND  TYPE_OF_ACTION =?\n"
    //                        + "AND  ACTIONTYPE=?\n"
    //                        + "AND  EITSEGMENT=?\n"
    //                        + "AND  TYPE_OF_VALUE=?\n"
    //                        + "AND  TYPE_OF_FRIST_VALUE=?\n"
    //                        + "AND  FRIST_VALUES=?\n"
    //                        + "AND  SECOND_OPERATION=?\n"
    //                        + "AND  TYPE_OF_SECOND_VALUE=?\n"
    //                        + "AND  SECOND_VALUE=?\n"
    //                        + "AND  SAVE_RESULT=?\n";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                ps.setString(3, validationBean.getApplicationValueFamily());
    //                ps.setString(4, validationBean.getApplicationKey());
    //                ps.setString(5, validationBean.getOperation());
    //                ps.setString(6, validationBean.getSecondKeyType());
    //                ps.setString(7, validationBean.getSecondsaaSSource());
    //                ps.setString(8, validationBean.getSecondreportName());
    //                ps.setString(9, validationBean.getSecondparameter1Name());
    //                ps.setString(10, validationBean.getSecondparameter2Name());
    //                ps.setString(11, validationBean.getSecondparameter3Name());
    //                ps.setString(12, validationBean.getSecondparameter4Name());
    //                ps.setString(13, validationBean.getSecondparameter5Name());
    //                ps.setString(14, validationBean.getTypeOFAction());
    //                ps.setString(15, validationBean.getActiontype());
    //                ps.setString(16, validationBean.getEitSegment());
    //                ps.setString(17, validationBean.getTypeOfValue());
    //                ps.setString(18, validationBean.getTypeOfFristValue());
    //                ps.setString(19, validationBean.getFristValues());
    //                ps.setString(20, validationBean.getSecondOperation());
    //                ps.setString(21, validationBean.getTypeOfSecondValue());
    //                ps.setString(22, validationBean.getSecondValue());
    //                ps.setString(23, validationBean.getSaveResultToValidationsValues());
    //
    //                rs = ps.executeQuery();
    //            }
    //
    //
    //            while (rs.next()) {
    //                // //(rs.getString(1));
    //                String c = rs.getString("count(*)");
    //                //("Count ==========>" + c);
    //                count = Integer.parseInt(c);
    //            }
    //
    //            rs.close();
    //            ps.close();
    //            connection.close();
    //        } catch (Exception e) {
    //           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
    //            JsonObject obj = new JsonObject();
    //            obj.addProperty("Error", "Internal Error: " + e.getMessage());
    //            //   array.add(obj);
    //        }
    //        return count;
    //    }
    //End Validation

    public ArrayList<ValidationBean> validationValueName(String eitCode) {
        ArrayList<ValidationBean> validationList =
            new ArrayList<ValidationBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                " SELECT VALIDATION_VALUES_NAME FROM  " + " " + getSchema_Name() +
                ".XXX_VALIDATION validations WHERE validations.EIT_CODE LIKE ? AND SAVE_RESULT = 'Yes'";

            ps = connection.prepareStatement(query);
            //
            ps.setString(1, eitCode);
            rs = ps.executeQuery();
            while (rs.next()) {
                //(rs.getString("VALIDATION_VALUES_NAME"));
                ValidationBean validationBean = new ValidationBean();
                validationBean.setValidationValuesName(rs.getString("VALIDATION_VALUES_NAME"));
                validationList.add(validationBean);

            }

            rs.close();
            ps.close();
            connection.close();
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            //   array.add(obj);
        } finally {
            closeResources(connection, ps, rs);
        }

        return validationList;
    }


    //  public int getValidationCounttest(ValidationBean validationBean) {
    //  int count =0;
    //
    //      try {
    //
    //        String    query = " select count(*) from  "+ " " + getSchema_Name() + ".XXX_VALIDATION WHERE\n"
    //                        + "EIT_CODE = ?\n"
    //                        + "AND	FIRST_KEY_TYPE = ?\n";
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, validationBean.getEitCode());
    //                ps.setString(2, validationBean.getFirstKeyType());
    //                rs = ps.executeQuery();
    //
    //
    //      } catch (Exception e) {
    //      e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
    //      }

    //  return count;
    //  }

}
