/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.XXX_EITBeen;
import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author lenovo
 */
public class XXX_EITDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();

    public XXX_EITBeen insertOrUpdateXXX_EIT(XXX_EITBeen XXX_eitBean, String transactionType) {
        try {

            connection = AppsproConnection.getConnection();
            String query = null;

            if (transactionType.equals("ADD")) {

                query = "insert into   "+ " " + getSchema_Name() + ".XXX_EIT(CODE,EIT_CODE,STATUS,PERSON_NUMBER,PERSON_ID,CREATED_BY,CREATION_DATE,UPDATED_BY,UPDATED_DATE,LINE_MANAGER,MANAGER_OF_MANAGER,\n"
                        + "PEI_INFORMATION1,PEI_INFORMATION2,PEI_INFORMATION3,PEI_INFORMATION4,PEI_INFORMATION5,PEI_INFORMATION6,PEI_INFORMATION7,PEI_INFORMATION8,\n"
                        + "PEI_INFORMATION9,PEI_INFORMATION10,PEI_INFORMATION11,PEI_INFORMATION12,PEI_INFORMATION13,PEI_INFORMATION14,PEI_INFORMATION15,\n"
                        + "PEI_INFORMATION16,PEI_INFORMATION17,PEI_INFORMATION18,PEI_INFORMATION19,PEI_INFORMATION20,PEI_INFORMATION21,\n"
                        + "PEI_INFORMATION22,PEI_INFORMATION23,PEI_INFORMATION24,PEI_INFORMATION25,PEI_INFORMATION26,PEI_INFORMATION27,PEI_INFORMATION28,PEI_INFORMATION29,PEI_INFORMATION30) \n"
                        + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

                ps = connection.prepareStatement(query);
                ps.setString(1, XXX_eitBean.getCode());
                ps.setString(2, XXX_eitBean.getEit_code());
                ps.setString(3, XXX_eitBean.getStatus());
                ps.setString(4, XXX_eitBean.getPerson_number());
                ps.setString(5, XXX_eitBean.getPerson_id());
                ps.setString(6, XXX_eitBean.getCreated_by());
                ps.setString(7, XXX_eitBean.getCreation_date());
                ps.setString(8, XXX_eitBean.getUpdated_by());
                ps.setString(9, XXX_eitBean.getUpdated_date());
                ps.setString(10, XXX_eitBean.getLine_manager());
                ps.setString(11, XXX_eitBean.getManage_of_manager());
                ps.setString(12, XXX_eitBean.getPei_information1());
                ps.setString(13, XXX_eitBean.getPei_information2());
                ps.setString(14, XXX_eitBean.getPei_information3());
                ps.setString(15, XXX_eitBean.getPei_information4());
                ps.setString(16, XXX_eitBean.getPei_information5());
                ps.setString(17, XXX_eitBean.getPei_information6());
                ps.setString(18, XXX_eitBean.getPei_information7());
                ps.setString(19, XXX_eitBean.getPei_information8());
                ps.setString(20, XXX_eitBean.getPei_information9());
                ps.setString(21, XXX_eitBean.getPei_information10());
                ps.setString(22, XXX_eitBean.getPei_information11());
                ps.setString(23, XXX_eitBean.getPei_information12());
                ps.setString(24, XXX_eitBean.getPei_information13());
                ps.setString(25, XXX_eitBean.getPei_information14());
                ps.setString(26, XXX_eitBean.getPei_information15());
                ps.setString(27, XXX_eitBean.getPei_information16());
                ps.setString(28, XXX_eitBean.getPei_information17());
                ps.setString(29, XXX_eitBean.getPei_information18());
                ps.setString(30, XXX_eitBean.getPei_information19());
                ps.setString(31, XXX_eitBean.getPei_information20());
                ps.setString(32, XXX_eitBean.getPei_information21());
                ps.setString(33, XXX_eitBean.getPei_information22());
                ps.setString(34, XXX_eitBean.getPei_information23());
                ps.setString(35, XXX_eitBean.getPei_information24());
                ps.setString(36, XXX_eitBean.getPei_information25());
                ps.setString(37, XXX_eitBean.getPei_information26());
                ps.setString(38, XXX_eitBean.getPei_information27());
                ps.setString(39, XXX_eitBean.getPei_information28());
                ps.setString(40, XXX_eitBean.getPei_information29());
                ps.setString(41, XXX_eitBean.getPei_information30());

            } else if (transactionType.equals("EDIT")) {
                query = "UPDATE  "+ " " + getSchema_Name() + ".XXX_EIT set CODE=?,EIT_CODE=?,STATUS=?,PERSON_NUMBER=?,PERSON_ID=?,CREATED_By=?,CREATION_DATE=?,\n"
                        + " UPDATED_BY=?,UPDATED_DATE=?,LINE_MANAGER=?,MANAGER_OF_MANAGER=?,PEI_INFORMATION1=?,PEI_INFORMATION2=?,PEI_INFORMATION3=?,\n"
                        + " PEI_INFORMATION4=?,PEI_INFORMATION5=?,PEI_INFORMATION6=?,PEI_INFORMATION7=?,PEI_INFORMATION8=?,PEI_INFORMATION9=?,PEI_INFORMATION10=?\n"
                        + " ,PEI_INFORMATION11=?,PEI_INFORMATION12=?,PEI_INFORMATION13=?,PEI_INFORMATION14=?,PEI_INFORMATION15=?,PEI_INFORMATION16=?,\n"
                        + " PEI_INFORMATION17=?,PEI_INFORMATION18=?,PEI_INFORMATION19=?,PEI_INFORMATION20=?,PEI_INFORMATION21=?,PEI_INFORMATION22=?\n"
                        + " ,PEI_INFORMATION23=?,PEI_INFORMATION24=?,PEI_INFORMATION25=?,PEI_INFORMATION26=?,PEI_INFORMATION27=?,PEI_INFORMATION28=?\n"
                        + " ,PEI_INFORMATION29=?,PEI_INFORMATION30=? where ID=?";
                ps = connection.prepareStatement(query);
                ps.setString(1, XXX_eitBean.getCode());
                ps.setString(2, XXX_eitBean.getEit_code());
                ps.setString(3, XXX_eitBean.getStatus());
                ps.setString(4, XXX_eitBean.getPerson_number());
                ps.setString(5, XXX_eitBean.getPerson_id());
                ps.setString(6, XXX_eitBean.getCreated_by());
                ps.setString(7, XXX_eitBean.getCreation_date());
                ps.setString(8, XXX_eitBean.getUpdated_by());
                ps.setString(9, XXX_eitBean.getUpdated_date());
                ps.setString(10, XXX_eitBean.getLine_manager());
                ps.setString(11, XXX_eitBean.getManage_of_manager());
                ps.setString(12, XXX_eitBean.getPei_information1());
                ps.setString(13, XXX_eitBean.getPei_information2());
                ps.setString(14, XXX_eitBean.getPei_information3());
                ps.setString(15, XXX_eitBean.getPei_information4());
                ps.setString(16, XXX_eitBean.getPei_information5());
                ps.setString(17, XXX_eitBean.getPei_information6());
                ps.setString(18, XXX_eitBean.getPei_information7());
                ps.setString(19, XXX_eitBean.getPei_information8());
                ps.setString(20, XXX_eitBean.getPei_information9());
                ps.setString(21, XXX_eitBean.getPei_information10());
                ps.setString(22, XXX_eitBean.getPei_information11());
                ps.setString(23, XXX_eitBean.getPei_information12());
                ps.setString(24, XXX_eitBean.getPei_information13());
                ps.setString(25, XXX_eitBean.getPei_information14());
                ps.setString(26, XXX_eitBean.getPei_information15());
                ps.setString(27, XXX_eitBean.getPei_information16());
                ps.setString(28, XXX_eitBean.getPei_information17());
                ps.setString(29, XXX_eitBean.getPei_information18());
                ps.setString(30, XXX_eitBean.getPei_information19());
                ps.setString(31, XXX_eitBean.getPei_information20());
                ps.setString(32, XXX_eitBean.getPei_information21());
                ps.setString(33, XXX_eitBean.getPei_information22());
                ps.setString(34, XXX_eitBean.getPei_information23());
                ps.setString(35, XXX_eitBean.getPei_information24());
                ps.setString(36, XXX_eitBean.getPei_information25());
                ps.setString(37, XXX_eitBean.getPei_information26());
                ps.setString(38, XXX_eitBean.getPei_information27());
                ps.setString(39, XXX_eitBean.getPei_information28());
                ps.setString(40, XXX_eitBean.getPei_information29());
                ps.setString(41, XXX_eitBean.getPei_information30());
                ps.setString(42, XXX_eitBean.getId());
            }

            ps.executeUpdate();

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return XXX_eitBean;
    }

    public ArrayList<XXX_EITBeen> getXXX_EIT(String eitId) {
        ArrayList<XXX_EITBeen> XXX_EITList = new ArrayList<XXX_EITBeen>();

        try {
            connection = AppsproConnection.getConnection();
            String query = " select CODE,EIT_CODE,STATUS,PERSON_NUMBER,PERSON_ID,CREATED_BY,CREATION_DATE,UPDATED_BY,UPDATED_DATE,LINE_MANAGER,MANAGER_OF_MANAGER,\n"
                    + "PEI_INFORMATION1,PEI_INFORMATION2,PEI_INFORMATION3,PEI_INFORMATION4,PEI_INFORMATION5,PEI_INFORMATION6,PEI_INFORMATION7,PEI_INFORMATION8,\n"
                    + "PEI_INFORMATION9,PEI_INFORMATION10,PEI_INFORMATION11,PEI_INFORMATION12,PEI_INFORMATION13,PEI_INFORMATION14,PEI_INFORMATION15,\n"
                    + "PEI_INFORMATION16,PEI_INFORMATION17,PEI_INFORMATION18,PEI_INFORMATION19,PEI_INFORMATION20,PEI_INFORMATION21,\n"
                    + "PEI_INFORMATION22,PEI_INFORMATION23,PEI_INFORMATION24,PEI_INFORMATION25,PEI_INFORMATION26,PEI_INFORMATION27,PEI_INFORMATION28,PEI_INFORMATION29,PEI_INFORMATION30 from  "+ " " + getSchema_Name() + ".XXX_EIT WHERE ID=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, eitId);
            rs = ps.executeQuery();
            while (rs.next()) {
                XXX_EITBeen xxx_bean = new XXX_EITBeen();
                xxx_bean.setCode(rs.getString(1));
                xxx_bean.setEit_code(rs.getString(2));
                xxx_bean.setStatus(rs.getString(3));
                xxx_bean.setPerson_number(rs.getString(4));
                xxx_bean.setPerson_id(rs.getString(5));
                xxx_bean.setCreated_by(rs.getString(6));
                xxx_bean.setCreation_date(rs.getString(7));
                xxx_bean.setUpdated_by(rs.getString(8));
                xxx_bean.setUpdated_date(rs.getString(9));
                xxx_bean.setLine_manager(rs.getString(10));
                xxx_bean.setManage_of_manager(rs.getString(11));
                xxx_bean.setPei_information1(rs.getString(12));
                xxx_bean.setPei_information2(rs.getString(13));
                xxx_bean.setPei_information3(rs.getString(14));
                xxx_bean.setPei_information4(rs.getString(15));
                xxx_bean.setPei_information5(rs.getString(16));
                xxx_bean.setPei_information6(rs.getString(17));
                xxx_bean.setPei_information7(rs.getString(18));
                xxx_bean.setPei_information8(rs.getString(19));
                xxx_bean.setPei_information9(rs.getString(20));
                xxx_bean.setPei_information10(rs.getString(21));
                xxx_bean.setPei_information11(rs.getString(22));
                xxx_bean.setPei_information12(rs.getString(23));
                xxx_bean.setPei_information13(rs.getString(24));
                xxx_bean.setPei_information14(rs.getString(25));
                xxx_bean.setPei_information15(rs.getString(26));
                xxx_bean.setPei_information16(rs.getString(27));
                xxx_bean.setPei_information17(rs.getString(28));
                xxx_bean.setPei_information18(rs.getString(29));
                xxx_bean.setPei_information19(rs.getString(30));
                xxx_bean.setPei_information20(rs.getString(31));
                xxx_bean.setPei_information21(rs.getString(32));
                xxx_bean.setPei_information22(rs.getString(33));
                xxx_bean.setPei_information23(rs.getString(34));
                xxx_bean.setPei_information24(rs.getString(35));
                xxx_bean.setPei_information25(rs.getString(36));
                xxx_bean.setPei_information26(rs.getString(37));
                xxx_bean.setPei_information27(rs.getString(38));
                xxx_bean.setPei_information28(rs.getString(39));
                xxx_bean.setPei_information29(rs.getString(40));
                xxx_bean.setPei_information30(rs.getString(41));

                XXX_EITList.add(xxx_bean);

            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return XXX_EITList;

    }

}
