package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.SummaryReportBean;
import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.CommitmentAndDisciplineBean;
import com.appspro.fusionsshr.bean.PeopleExtraInformationBean;
import com.appspro.fusionsshr.bean.SummaryReportBean;

import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class CommitmentAndDisciplineDAO extends AppsproConnection{
   
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    public CommitmentAndDisciplineBean UpdateCommitmentAndDiscipliney(CommitmentAndDisciplineBean bean) {

        try {
            connection = AppsproConnection.getConnection();
 
                String query="update  " + " " + getSchema_Name() + ".COMMITMENTANDDISCIPLINE set PARAMETERAR1=?,PARAMETEREN2=? WHERE ID=?";
                ps = connection.prepareStatement(query);

                ps.setString(1, bean.getParameterAR());
                ps.setString(2, bean.getParamterEn());
                ps.setString(3, bean.getId());
               
                ps.executeUpdate();

            

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }
    
    public CommitmentAndDisciplineBean executeCommitmentAndDiscipline(String body){
            CommitmentAndDisciplineBean bean=new CommitmentAndDisciplineBean();
            CommitmentAndDisciplineBean obj ;
            List<JSONObject> beanList = new ArrayList<JSONObject>();
            JSONArray arr = new JSONArray(body);
                    JSONObject jsonObjInput;
                    String body2 = "";
                    for(int i = 0; i < arr.length(); i++){
                        jsonObjInput = arr.getJSONObject(i);    
                        beanList.add(jsonObjInput);
                    }
                    try{
                        
                        for (JSONObject commitmentAndDisciplineBean1 :
                             beanList)  {
                                connection = AppsproConnection.getConnection();
                                String ar=commitmentAndDisciplineBean1.get("parameterAR").toString();
                                String en=commitmentAndDisciplineBean1.get("paramterEn").toString();
                                //(ar+en);
                               String query="INSERT INTO  " + " " + getSchema_Name() + ".COMMITMENTANDDISCIPLINE(PARAMETERAR1,PARAMETEREN2)values(?,?)";
                                //(query);
                                ps = connection.prepareStatement(query);
                                ps.setString(1, ar);
                                ps.setString(2,en);
                                //(query);
                                ps.executeUpdate();
                            
                            
                            
                            } 
                        
                        
                        
                        
                    }catch(Exception ex){
                        
                        
                              }finally {
            closeResources(connection, ps, rs);
        }
          return bean;
        }
    
    public ArrayList<CommitmentAndDisciplineBean> getCommitmentAndDiscipline() {

        ArrayList<CommitmentAndDisciplineBean> CommitmentAndDisciplineList =
            new ArrayList<CommitmentAndDisciplineBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "select * from" + "  " + " " + getSchema_Name() + ".COMMITMENTANDDISCIPLINE";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                CommitmentAndDisciplineBean bean = new CommitmentAndDisciplineBean();
                bean.setId(rs.getString("ID"));
                bean.setParameterAR(rs.getString("PARAMETERAR1"));
                bean.setParamterEn(rs.getString("PARAMETEREN2"));
                
                CommitmentAndDisciplineList.add(bean);

            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return CommitmentAndDisciplineList;

    }


}
