/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.dao;


import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.PositionBean;

import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;


/**
 *
 * @author amro
 */
public class PositionDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();

    public PositionBean insertOrUpdatePosition(PositionBean bean,
                                               String transactionType) {
        try {
            connection = AppsproConnection.getConnection();
            if (!bean.getEndDate().isEmpty()) {
                bean.setEndDate(rh.convertToGregorian(bean.getEndDate()));
            }
            if (!bean.getStartDate().isEmpty()) {
                bean.setStartDate(rh.convertToGregorian(bean.getStartDate()));
            }

            if (transactionType.equals("ADD")) {
                String query =
                    "SELECT  " + " " + getSchema_Name() + ".POSITION_CODE_SEQ.NEXTVAL  from dual";
                ps = connection.prepareStatement(query);
                rs = ps.executeQuery();
                Integer positionCode = 0;
                if (rs.next()) {
                    positionCode = rs.getInt(1);

                }
                query =
                        "INSERT INTO  " + " " + getSchema_Name() + ".XXX_POSITION (START_DATE,END_DATE,TAXONOMIC_CODE,RANKED,POSITION_NUMBER ,MANAGING,POSITION_NAME,CODE,ACTION,CREATEDBY,CODE_OF_TAXONOMIC_CODE,CREATEDDATE,FLAG)\n" +
                        "                    VALUES ( ?, ? ,? ,?, ? ,?,? ,?,?,?,?,?,?)";

                ps = connection.prepareStatement(query);

                ps.setDate(1, getSQLDateFromString(bean.getStartDate()));
                ps.setDate(2, getSQLDateFromString(bean.getEndDate()));
                ps.setString(3, bean.getTaxonomicCode());
                ps.setString(4, bean.getRanked());
                ps.setString(5, bean.getPositionNumber());
                ps.setString(6, bean.getManaging());
                ps.setString(7, bean.getPositionName());
                ps.setInt(8, positionCode);
                ps.setString(9, bean.getAction());
                ps.setString(10, bean.getCreatedBy());
                ps.setString(11, bean.getCodeOfTaxonomicCode());
                ps.setDate(12, getSQLDateFromString(bean.getCreatedDate()));
                ps.setString(13, bean.getFlag());
                ps.executeUpdate();

            } else if (transactionType.equals("EDIT")) {
                String oldFlag = "N";
                String query =
                    "UPDATE  " + " " + getSchema_Name() + ".XXX_POSITION \n" +
                    "SET  UPDATEDATE =sysdate \n" +
                    ",END_DATE = sysdate " + ",FLAG = ?" + ", UPDATEBY = ? " +
                    "WHERE id = ? ";
                ps = connection.prepareStatement(query);
                ps.setString(1, oldFlag);
                ps.setString(2, bean.getUpdatedBy());
                ps.setInt(3, Integer.parseInt(bean.getId()));
                ps.executeUpdate();

                query =
                        "INSERT INTO  " + " " + getSchema_Name() + ".XXX_POSITION (START_DATE,END_DATE,TAXONOMIC_CODE,RANKED,POSITION_NUMBER ,MANAGING,POSITION_NAME,CODE,ACTION,CREATEDBY,CREATEDDATE, FLAG,CODE_OF_TAXONOMIC_CODE)\n" +
                        "                    VALUES ( ?, ? ,? ,?, ? ,?,? ,?,?,?,?,?,? )";

                ps = connection.prepareStatement(query);

                ps.setDate(1, getSQLDateFromString(bean.getStartDate()));
                ps.setDate(2, getSQLDateFromString(bean.getEndDate()));
                ps.setString(3, bean.getTaxonomicCode());
                ps.setString(4, bean.getRanked());
                ps.setString(5, bean.getPositionNumber());
                ps.setString(6, bean.getManaging());
                ps.setString(7, bean.getPositionName());
                ps.setInt(8, bean.getCode());
                ps.setString(9, bean.getAction());
                ps.setString(10, bean.getCreatedBy());
                ps.setDate(11, getSQLDateFromString(bean.getCreatedDate()));
                ps.setString(12, bean.getFlag());
                ps.setString(13, bean.getCodeOfTaxonomicCode());

                ps.executeUpdate();
            } else if (transactionType.equals("CORRECTION")) {
                String oldFlag = "N";
                String query =
                    "UPDATE  " + " " + getSchema_Name() + ".XXX_POSITION \n" +
                    "SET  UPDATEDATE =sysdate \n" +
                    ",FLAG = ?" + ", UPDATEBY = ? " + ",START_DATE = ?" +
                    ",END_DATE = ?" + ",TAXONOMIC_CODE = ?" + ",RANKED = ?" +
                    ",POSITION_NUMBER = ?" + ",MANAGING = ?" +
                    ",POSITION_NAME = ?" + ",CODE = ?" + ",ACTION = ?" +
                    "WHERE id = ? ";
                ps = connection.prepareStatement(query);
                ps.setString(1, bean.getFlag());
                ps.setString(2, bean.getUpdatedBy());
                ps.setDate(3, getSQLDateFromString(bean.getStartDate()));
                ps.setDate(4, getSQLDateFromString(bean.getEndDate()));
                ps.setString(5, bean.getTaxonomicCode());
                ps.setString(6, bean.getRanked());
                ps.setString(7, bean.getPositionNumber());
                ps.setString(8, bean.getManaging());
                ps.setString(9, bean.getPositionName());
                ps.setInt(10, bean.getCode());
                ps.setString(11, bean.getAction());
                ps.setInt(12, Integer.parseInt(bean.getId()));
                ps.executeUpdate();
            } else if (transactionType.equals("Update_Change_Insert")) {
                updateCahngeInsert(bean);
            } else if (transactionType.equals("UPDATE_OVERRIDE")) {
                updateOverride(bean);
            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }

    public ArrayList<PositionBean> searchPositions(String taxonomicCode,
                                                   String ranked,
                                                   String postionNumber,
                                                   String managing,
                                                   String positionName) {
        ArrayList<PositionBean> postionsList = new ArrayList<PositionBean>();
        try {
            connection = AppsproConnection.getConnection();
            if (taxonomicCode.equals("undefined")) {
                taxonomicCode = "";
            }
            if (ranked.equals("undefined")) {
                ranked = "";
            }
            if (postionNumber.equals("undefined")) {
                postionNumber = "";
            }
            if (managing.equals("undefined")) {
                managing = "";
            }
            if (positionName.equals("undefined")) {
                positionName = "";
            }

            String query =
                "select * from  " + " " + getSchema_Name() + ".XXX_POSITION positions where positions.FLAG = ? AND(  positions.TAXONOMIC_CODE = NVL (?, positions.TAXONOMIC_CODE) AND positions.RANKED = NVL (?, positions.RANKED) AND positions.POSITION_NUMBER = NVL (?, positions.POSITION_NUMBER) AND positions.MANAGING = NVL (?, positions.MANAGING) AND positions.POSITION_NAME = NVL (?, positions.POSITION_NAME))";

            ps = connection.prepareStatement(query);
            ps.setString(1, "Y");
            ps.setString(2, taxonomicCode);
            ps.setString(3, ranked);
            ps.setString(4, postionNumber);
            ps.setString(5, managing);
            ps.setString(6, positionName);

            rs = ps.executeQuery();
            while (rs.next()) {
                PositionBean positionBean = new PositionBean();
                positionBean.setAction(rs.getString("ACTION"));
                positionBean.setCode(rs.getInt("CODE"));
                positionBean.setCreatedBy(rs.getString("CREATEDBY"));
                positionBean.setCreatedDate(rs.getString("CREATEDDATE"));
                positionBean.setFlag(rs.getString("FLAG"));
                positionBean.setId(rs.getString("ID"));
                positionBean.setManaging(rs.getString("MANAGING"));
                positionBean.setPositionName(rs.getString("POSITION_NAME"));
                positionBean.setPositionNumber(rs.getString("POSITION_NUMBER"));
                positionBean.setRanked(rs.getString("RANKED"));
                positionBean.setTaxonomicCode(rs.getString("TAXONOMIC_CODE"));
                positionBean.setUpdateDate(rs.getString("UPDATEDATE"));
                positionBean.setUpdatedBy(rs.getString("UPDATEBY"));
                positionBean.setCodeOfTaxonomicCode(rs.getString("CODE_OF_TAXONOMIC_CODE"));
                positionBean.setStartDateGregorian(rs.getString("START_DATE"));
                //get hijri start date and end date
                if (rs.getString("END_DATE") != null) {
                    positionBean.setEndDate(rh.convertToHijri(rs.getString("END_DATE")));
                } else {
                    positionBean.setEndDate((rs.getString("END_DATE")));
                }
                if (rs.getString("START_DATE") != null) {
                    positionBean.setStartDate(rh.convertToHijri(rs.getString("START_DATE")));
                } else {
                    positionBean.setStartDate((rs.getString("START_DATE")));
                } //end
                postionsList.add(positionBean);

            }

        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);

            //   array.add(obj);
        } finally {
            closeResources(connection, ps, rs);
        }

        return postionsList;
    }

    public ArrayList<PositionBean> getAllPositions() {
        ArrayList<PositionBean> postionsList = new ArrayList<PositionBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                " select * from  " + " " + getSchema_Name() + ".XXX_POSITION positions ";

            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                PositionBean positionBean = new PositionBean();
                positionBean.setAction(rs.getString("ACTION"));
                positionBean.setCode(rs.getInt("CODE"));
                positionBean.setCreatedBy(rs.getString("CREATEDBY"));
                positionBean.setCreatedDate(rs.getString("CREATEDDATE"));
                positionBean.setFlag(rs.getString("FLAG"));
                positionBean.setId(rs.getString("ID"));
                positionBean.setManaging(rs.getString("MANAGING"));
                positionBean.setPositionName(rs.getString("POSITION_NAME"));
                positionBean.setPositionNumber(rs.getString("POSITION_NUMBER"));
                positionBean.setRanked(rs.getString("RANKED"));
                positionBean.setTaxonomicCode(rs.getString("TAXONOMIC_CODE"));
                positionBean.setUpdateDate(rs.getString("UPDATEDATE"));
                positionBean.setUpdatedBy(rs.getString("UPDATEBY"));
                positionBean.setCodeOfTaxonomicCode(rs.getString("CODE_OF_TAXONOMIC_CODE"));
                //get hijri start date and end date
                if (rs.getString("END_DATE") != null) {
                    positionBean.setEndDate(rh.convertToHijri(rs.getString("END_DATE")));
                } else {
                    positionBean.setEndDate((rs.getString("END_DATE")));
                }
                if (rs.getString("START_DATE") != null) {
                    positionBean.setStartDate(rh.convertToHijri(rs.getString("START_DATE")));
                } else {
                    positionBean.setStartDate((rs.getString("START_DATE")));
                } //end
                postionsList.add(positionBean);

            }

        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);

            //   array.add(obj);
        } finally {
            closeResources(connection, ps, rs);
        }

        return postionsList;
    }

    public java.sql.Date getSQLDateFromString(String date) {
        java.sql.Date sqlDate = null;
        try {
            Date utilDate = new SimpleDateFormat("yyyy-MM-dd").parse(date);
            sqlDate = new java.sql.Date(utilDate.getTime());
        } catch (Exception ex) {
           ex.printStackTrace(); AppsproConnection.LOG.error("ERROR", ex);
        }
        return sqlDate;
    }

    public ArrayList<PositionBean> searchPositionsByCode(String code) {
        ArrayList<PositionBean> postionsList = new ArrayList<PositionBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "   select rownum,positions.* from  " + " " + getSchema_Name() +
                ".XXX_POSITION positions\n" +
                "                   where   positions.CODE =  ? ";

            ps = connection.prepareStatement(query);
            ps.setString(1, code);
            rs = ps.executeQuery();
            while (rs.next()) {
                PositionBean positionBean = new PositionBean();
                positionBean.setAction(rs.getString("ACTION"));
                positionBean.setCode(rs.getInt("CODE"));
                positionBean.setCreatedBy(rs.getString("CREATEDBY"));
                positionBean.setCreatedDate(rs.getString("CREATEDDATE"));
                positionBean.setFlag(rs.getString("FLAG"));
                positionBean.setId(rs.getString("ID"));
                positionBean.setManaging(rs.getString("MANAGING"));
                positionBean.setPositionName(rs.getString("POSITION_NAME"));
                positionBean.setPositionNumber(rs.getString("POSITION_NUMBER"));
                positionBean.setRanked(rs.getString("RANKED"));
                positionBean.setTaxonomicCode(rs.getString("TAXONOMIC_CODE"));
                positionBean.setUpdateDate(rs.getString("UPDATEDATE"));
                positionBean.setUpdatedBy(rs.getString("UPDATEBY"));
                positionBean.setCodeOfTaxonomicCode(rs.getString("CODE_OF_TAXONOMIC_CODE"));
                positionBean.setSequence(rs.getString("rownum"));
                //get hijri start date and end date
                if (rs.getString("END_DATE") != null) {
                    positionBean.setEndDate(rh.convertToHijri(rs.getString("END_DATE")));
                } else {
                    positionBean.setEndDate((rs.getString("END_DATE")));
                }
                if (rs.getString("START_DATE") != null) {
                    positionBean.setStartDate(rh.convertToHijri(rs.getString("START_DATE")));
                } else {
                    positionBean.setStartDate((rs.getString("START_DATE")));
                } //end
                postionsList.add(positionBean);

            }

        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);

            //   array.add(obj);
        } finally {
            closeResources(connection, ps, rs);
        }

        return postionsList;
    }

    //Get  By Position Name for History Popup

    public ArrayList<PositionBean> searchPositionsByName(String positionName) {
        ArrayList<PositionBean> postionsList = new ArrayList<PositionBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                " select * from  " + " " + getSchema_Name() + ".XXX_POSITION jobs \n" +
                "where   jobs.NAME = ? ";

            ps = connection.prepareStatement(query);
            ps.setString(1, positionName);
            rs = ps.executeQuery();
            while (rs.next()) {
                PositionBean positionBean = new PositionBean();
                positionBean.setAction(rs.getString("ACTION"));
                positionBean.setCode(rs.getInt("CODE"));
                positionBean.setCreatedBy(rs.getString("CREATEDBY"));
                positionBean.setCreatedDate(rs.getString("CREATEDDATE"));
                positionBean.setFlag(rs.getString("FLAG"));
                positionBean.setId(rs.getString("ID"));
                positionBean.setManaging(rs.getString("MANAGING"));
                positionBean.setPositionName(rs.getString("POSITION_NAME"));
                positionBean.setPositionNumber(rs.getString("POSITION_NUMBER"));
                positionBean.setRanked(rs.getString("RANKED"));
                positionBean.setTaxonomicCode(rs.getString("TAXONOMIC_CODE"));
                positionBean.setUpdateDate(rs.getString("UPDATEDATE"));
                positionBean.setUpdatedBy(rs.getString("UPDATEBY"));
                positionBean.setCodeOfTaxonomicCode(rs.getString("CODE_OF_TAXONOMIC_CODE"));
                //get hijri start date and end date
                if (rs.getString("END_DATE") != null) {
                    positionBean.setEndDate(rh.convertToHijri(rs.getString("END_DATE")));
                } else {
                    positionBean.setEndDate((rs.getString("END_DATE")));
                }
                if (rs.getString("START_DATE") != null) {
                    positionBean.setStartDate(rh.convertToHijri(rs.getString("START_DATE")));
                } else {
                    positionBean.setStartDate((rs.getString("START_DATE")));
                } //end
                postionsList.add(positionBean);

            }

            rs.close();
            ps.close();
            connection.close();
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            //   array.add(obj);
        } finally {
            closeResources(connection, ps, rs);
        }

        return postionsList;
    } //endmoh

    public PositionBean updateCahngeInsert(PositionBean bean) {
        try {
            //bean.setEndDate(getFutureEndData(bean));
            connection = AppsproConnection.getConnection();
            if (!bean.getEndDate().isEmpty()) {
                bean.setEndDate((bean.getEndDate()));
            }
            if (!bean.getStartDate().isEmpty()) {
                bean.setStartDate(rh.convertToGregorian(bean.getStartDate()));
            }
            String oldFlag = "N";
            String query =
                "UPDATE  " + " " + getSchema_Name() + ".XXX_POSITION \n" +
                "SET  UPDATEDATE =sysdate \n" +
                ",END_DATE = ? " + ",FLAG = ?" + ", UPDATEBY = ? " +
                "WHERE id = ? ";
            ps = connection.prepareStatement(query);
            ps.setDate(1, getSQLDateFromString(bean.getStartDate()));
            ps.setString(2, oldFlag);
            ps.setString(3, bean.getUpdatedBy());
            ps.setInt(4, Integer.parseInt(bean.getId()));
            ps.executeUpdate();

            String queryInsert =
                "INSERT INTO  " + " " + getSchema_Name() + ".XXX_POSITION (START_DATE,END_DATE,TAXONOMIC_CODE,RANKED,POSITION_NUMBER ,MANAGING,POSITION_NAME,CODE,ACTION,CREATEDBY,CREATEDDATE, FLAG,CODE_OF_TAXONOMIC_CODE)\n" +
                "                    VALUES ( ?, ? ,? ,?, ? ,?,? ,?,?,?,?,?,? )";

            ps = connection.prepareStatement(queryInsert);

            ps.setDate(1, getSQLDateFromString(bean.getStartDate()));
            ps.setDate(2, getSQLDateFromString(bean.getEndDate()));
            ps.setString(3, bean.getTaxonomicCode());
            ps.setString(4, bean.getRanked());
            ps.setString(5, bean.getPositionNumber());
            ps.setString(6, bean.getManaging());
            ps.setString(7, bean.getPositionName());
            ps.setInt(8, bean.getCode());
            ps.setString(9, bean.getAction());
            ps.setString(10, bean.getCreatedBy());
            ps.setDate(11, getSQLDateFromString(bean.getCreatedDate()));
            ps.setString(12, bean.getFlag());
            ps.setString(13, bean.getCodeOfTaxonomicCode());

            ps.executeUpdate();

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return bean;
    }

    public PositionBean updateOverride(PositionBean bean) {
        try {
            connection = AppsproConnection.getConnection();
            if (!bean.getEndDate().isEmpty()) {
                bean.setEndDate(rh.convertToGregorian(bean.getEndDate()));
            }
            if (!bean.getStartDate().isEmpty()) {
                bean.setStartDate(rh.convertToGregorian(bean.getStartDate()));
            }

            String query =
                "INSERT INTO  " + " " + getSchema_Name() + ".XXX_POSITION (START_DATE,END_DATE,TAXONOMIC_CODE,RANKED,POSITION_NUMBER ,MANAGING,POSITION_NAME,CODE,ACTION,CREATEDBY,CREATEDDATE, FLAG,CODE_OF_TAXONOMIC_CODE)\n" +
                "                    VALUES ( ?, ? ,? ,?, ? ,?,? ,?,?,?,?,?,? )";

            ps = connection.prepareStatement(query);

            ps.setDate(1, getSQLDateFromString(bean.getStartDate()));
            ps.setDate(2, getSQLDateFromString(bean.getEndDate()));
            ps.setString(3, bean.getTaxonomicCode());
            ps.setString(4, bean.getRanked());
            ps.setString(5, bean.getPositionNumber());
            ps.setString(6, bean.getManaging());
            ps.setString(7, bean.getPositionName());
            ps.setInt(8, bean.getCode());
            ps.setString(9, bean.getAction());
            ps.setString(10, bean.getCreatedBy());
            ps.setDate(11, getSQLDateFromString(bean.getCreatedDate()));
            ps.setString(12, "Y");
            ps.setString(13, bean.getCodeOfTaxonomicCode());

            ps.executeUpdate();

            query =
                    "delete from  " + " " + getSchema_Name() + ".XXX_POSITION where START_DATE > ?";
            ps = connection.prepareStatement(query);
            ps.setDate(1, getSQLDateFromString(bean.getStartDate()));
            ps.executeUpdate();

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return bean;
    }

    public String getFutureEndData(PositionBean bean) {
        String effEndDate = "";
        try {

            connection = AppsproConnection.getConnection();
            String query =
                "select  START_DATE from  " + " " + getSchema_Name() +
                ".XXX_POSITION POSITION where CODE = ? AND POSITION.ID = (select MIN(ID) FROM  " +
                " " + getSchema_Name() +
                ".XXX_POSITION where CODE = ? And START_DATE > ?)";

            ps = connection.prepareStatement(query);
            ps.setInt(1, bean.getCode());
            ps.setInt(2, bean.getCode());
            ps.setDate(3, getSQLDateFromString(bean.getStartDate()));
            rs = ps.executeQuery();
            while (rs.next()) {
                effEndDate = rs.getString("START_DATE");
                String c = rs.getString(1);
            }

            rs.close();
            ps.close();
            connection.close();
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            //   array.add(obj);
        } finally {
            closeResources(connection, ps, rs);
        }
        return effEndDate;
    }

}
