/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.dao;


import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.ApprovalListBean;
import com.appspro.fusionsshr.bean.ApprovalSetupBean;
import com.appspro.fusionsshr.bean.EmployeeBean;
import com.appspro.fusionsshr.bean.CountEITRequest;
import com.appspro.fusionsshr.bean.DelegationBean;
import com.appspro.fusionsshr.bean.PeopleExtraInformationBean;
import com.appspro.fusionsshr.bean.SelfServiceBean;
import com.appspro.fusionsshr.bean.SummaryReportBean;
import com.appspro.fusionsshr.bean.WorkFlowNotificationBean;

import com.appspro.mail.CredentialStoreClassMail;

import com.hha.appsproExceptionn.AppsProException;

import common.biPReports.BIReportModel;

import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import java.text.DateFormat;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.joda.time.LocalDateTime;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;

import utils.system;


/**
 *
 * @author Shadi Mansi-PC
 */
public class PeopleExtraInformationDAO extends AppsproConnection {

    private static PeopleExtraInformationDAO instance = null;
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    EFFDetails efd;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    EmployeeDetails employeeDetails = new EmployeeDetails();
    CredentialStoreClassMail mailObj = new CredentialStoreClassMail();
    SelfServiceDAO selfServiceDao = new SelfServiceDAO();
    SelfServiceBean sfBean = new SelfServiceBean();
    ApprovalSetupDAO approvalSetup = new ApprovalSetupDAO();
    ArrayList<ApprovalSetupBean> approvalSetupList =
        new ArrayList<ApprovalSetupBean>();
    ApprovalListDAO approvalListDao = new ApprovalListDAO();
    ApprovalListBean alBean = new ApprovalListBean();
    WorkflowNotificationDAO workflowNotificationDao =
        new WorkflowNotificationDAO();
    WorkFlowNotificationBean wfn = new WorkFlowNotificationBean();

    public static PeopleExtraInformationDAO getInstance() {
        if (instance == null)
            instance = new PeopleExtraInformationDAO();
        return instance;
    }
    //******************Start Insert Or Update PeopleExtraInfo******************

    public synchronized PeopleExtraInformationBean insertOrUpdateXXX_EIT(PeopleExtraInformationBean extraInformationBean,
                                                                         String transactionType,
                                                                         String conditionCode) throws AppsProException {
        try {

            connection = AppsproConnection.getConnection();
            String query = null;

            if (transactionType.equals("ADD")) {

                extraInformationBean.setId(getMaxId(connection));
                query =
                        "insert into   " + " " + getSchema_Name() + ".XXX_PER_PEOPLE_EXTRA_INFO(\n" +
                        "ID,EIT_CODE,EIT,STATUS,PERSON_NUMBER,PERSON_ID,CREATION_DATE,CREATED_BY,UPDATED_BY,UPDATED_DATE,LINE_MANAGER\n" +
                        ",MANAGER_OF_MANAGER,URL,EIT_NAME,PERSON_EXTRA_INFO_ID,EFFECTIVE_START_DATE,EFFECTIVE_END_DATE,INFORMATION_TYPE,PEI_ATTRIBUTE_CATEGORY,PEI_ATTRIBUTE1,PEI_ATTRIBUTE2\n" +
                        ",PEI_ATTRIBUTE3,PEI_ATTRIBUTE4,PEI_ATTRIBUTE5,PEI_ATTRIBUTE6,PEI_ATTRIBUTE7,PEI_ATTRIBUTE8,PEI_ATTRIBUTE9,PEI_ATTRIBUTE10,PEI_ATTRIBUTE11,PEI_ATTRIBUTE12\n" +
                        ",PEI_ATTRIBUTE13,PEI_ATTRIBUTE14,PEI_ATTRIBUTE15,PEI_ATTRIBUTE16,PEI_ATTRIBUTE17,PEI_ATTRIBUTE18,PEI_ATTRIBUTE19,PEI_ATTRIBUTE20,PEI_ATTRIBUTE21,PEI_ATTRIBUTE22\n" +
                        ",PEI_ATTRIBUTE23,PEI_ATTRIBUTE24,PEI_ATTRIBUTE25,PEI_ATTRIBUTE26,PEI_ATTRIBUTE27,PEI_ATTRIBUTE28,PEI_ATTRIBUTE29,PEI_ATTRIBUTE30,PEI_INFORMATION_CATEGORY,PEI_INFORMATION1\n" +
                        ",PEI_INFORMATION2,PEI_INFORMATION3,PEI_INFORMATION4,PEI_INFORMATION5,PEI_INFORMATION6,PEI_INFORMATION7,PEI_INFORMATION8,PEI_INFORMATION9,PEI_INFORMATION10,PEI_INFORMATION11\n" +
                        ",PEI_INFORMATION12,PEI_INFORMATION13,PEI_INFORMATION14,PEI_INFORMATION15,PEI_INFORMATION16,PEI_INFORMATION17,PEI_INFORMATION18,PEI_INFORMATION19,PEI_INFORMATION20,PEI_INFORMATION21\n" +
                        ",PEI_INFORMATION22,PEI_INFORMATION23,PEI_INFORMATION24,PEI_INFORMATION25,PEI_INFORMATION26,PEI_INFORMATION27,PEI_INFORMATION28,PEI_INFORMATION29,PEI_INFORMATION30,PEI_INFORMATION_NUMBER1,PEI_INFORMATION_NUMBER2,PEI_INFORMATION_NUMBER3,PEI_INFORMATION_NUMBER4,PEI_INFORMATION_NUMBER5,PEI_INFORMATION_NUMBER6,PEI_INFORMATION_NUMBER7,PEI_INFORMATION_NUMBER8\n" +
                        ",PEI_INFORMATION_NUMBER9,PEI_INFORMATION_NUMBER10,PEI_INFORMATION_NUMBER11,PEI_INFORMATION_NUMBER12,PEI_INFORMATION_NUMBER13,PEI_INFORMATION_NUMBER14,PEI_INFORMATION_NUMBER15\n" +
                        ",PEI_INFORMATION_NUMBER16,PEI_INFORMATION_NUMBER17,PEI_INFORMATION_NUMBER18,PEI_INFORMATION_NUMBER19,PEI_INFORMATION_NUMBER20,PEI_INFORMATION_DATE1,PEI_INFORMATION_DATE2\n" +
                        ",PEI_INFORMATION_DATE3,PEI_INFORMATION_DATE4,PEI_INFORMATION_DATE5,PEI_INFORMATION_DATE6,PEI_INFORMATION_DATE7,PEI_INFORMATION_DATE8,PEI_INFORMATION_DATE9,PEI_INFORMATION_DATE10\n" +
                        ",PEI_INFORMATION_DATE11,PEI_INFORMATION_DATE12,PEI_INFORMATION_DATE13,PEI_INFORMATION_DATE14,PEI_INFORMATION_DATE15,OBJECT_VERSION_NUMBER,LAST_UPDATE_LOGIN,EITLBL,PERSON_NAME)\n" +
                        "VALUES(\n" +
                        "?,?,?,?,?,?,sysdate,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?\n" +
                        ",?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?\n" +
                        ",?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?\n" +
                        ",?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?\n" +
                        ",?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?\n" +
                        ",?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                // String X = extraInformationBean.getEit().toString();
                ps = connection.prepareStatement(query);
                ps.setInt(1, extraInformationBean.getId());
                ps.setString(2, extraInformationBean.getCode());
                ps.setString(3, extraInformationBean.getEit());
                ps.setString(4, extraInformationBean.getStatus());
                ps.setString(5, extraInformationBean.getPerson_number());
                ps.setString(6, extraInformationBean.getPerson_id());
                //                ps.setString(7, extraInformationBean.getCreated_by());
                ps.setString(7, extraInformationBean.getCreation_date());
                ps.setString(8, extraInformationBean.getUpdated_by());
                ps.setString(9, extraInformationBean.getUpdated_date());
                ps.setString(10, extraInformationBean.getLine_manager());
                ps.setString(11, extraInformationBean.getManage_of_manager());
                ps.setString(12, extraInformationBean.getUrl());
                ps.setString(13, extraInformationBean.getEit_name());
                //New
                ps.setString(14,
                             extraInformationBean.getPERSON_EXTRA_INFO_ID());
                ps.setString(15,
                             extraInformationBean.getEFFECTIVE_START_DATE());
                ps.setString(16, extraInformationBean.getEFFECTIVE_END_DATE());
                ps.setString(17, extraInformationBean.getINFORMATION_TYPE());
                ps.setString(18,
                             extraInformationBean.getPEI_ATTRIBUTE_CATEGORY());
                //PEI_Attribute
                ps.setString(19, extraInformationBean.getPEI_ATTRIBUTE1());
                ps.setString(20, extraInformationBean.getPEI_ATTRIBUTE2());
                ps.setString(21, extraInformationBean.getPEI_ATTRIBUTE3());
                ps.setString(22, extraInformationBean.getPEI_ATTRIBUTE4());
                ps.setString(23, extraInformationBean.getPEI_ATTRIBUTE5());
                ps.setString(24, extraInformationBean.getPEI_ATTRIBUTE6());
                ps.setString(25, extraInformationBean.getPEI_ATTRIBUTE7());
                ps.setString(26, extraInformationBean.getPEI_ATTRIBUTE8());
                ps.setString(27, extraInformationBean.getPEI_ATTRIBUTE9());
                ps.setString(28, extraInformationBean.getPEI_ATTRIBUTE10());
                ps.setString(29, extraInformationBean.getPEI_ATTRIBUTE11());
                ps.setString(30, extraInformationBean.getPEI_ATTRIBUTE12());
                ps.setString(31, extraInformationBean.getPEI_ATTRIBUTE13());
                ps.setString(32, extraInformationBean.getPEI_ATTRIBUTE14());
                ps.setString(33, extraInformationBean.getPEI_ATTRIBUTE15());
                ps.setString(34, extraInformationBean.getPEI_ATTRIBUTE16());
                ps.setString(35, extraInformationBean.getPEI_ATTRIBUTE17());
                ps.setString(36, extraInformationBean.getPEI_ATTRIBUTE18());
                ps.setString(37, extraInformationBean.getPEI_ATTRIBUTE19());
                ps.setString(38, extraInformationBean.getPEI_ATTRIBUTE20());
                ps.setString(39, extraInformationBean.getPEI_ATTRIBUTE21());
                ps.setString(40, extraInformationBean.getPEI_ATTRIBUTE22());
                ps.setString(41, extraInformationBean.getPEI_ATTRIBUTE23());
                ps.setString(42, extraInformationBean.getPEI_ATTRIBUTE24());
                ps.setString(43, extraInformationBean.getPEI_ATTRIBUTE25());
                ps.setString(44, extraInformationBean.getPEI_ATTRIBUTE26());
                ps.setString(45, extraInformationBean.getPEI_ATTRIBUTE27());
                ps.setString(46, extraInformationBean.getPEI_ATTRIBUTE28());
                ps.setString(47, extraInformationBean.getPEI_ATTRIBUTE29());
                ps.setString(48, extraInformationBean.getPEI_ATTRIBUTE30());
                //
                ps.setString(49,
                             extraInformationBean.getPEI_INFORMATION_CATEGORY());
                //Pei_INFORMATION
                //extraInformationBean.setPEI_INFORMATION8(rh.convertToGregorian(extraInformationBean.getPEI_INFORMATION8()));
                ps.setString(50, extraInformationBean.getPEI_INFORMATION1());
                ps.setString(51, extraInformationBean.getPEI_INFORMATION2());
                ps.setString(52, extraInformationBean.getPEI_INFORMATION3());
                ps.setString(53, extraInformationBean.getPEI_INFORMATION4());
                ps.setString(54, extraInformationBean.getPEI_INFORMATION5());
                ps.setString(55, extraInformationBean.getPEI_INFORMATION6());
                ps.setString(56, extraInformationBean.getPEI_INFORMATION7());
                ps.setString(57, extraInformationBean.getPEI_INFORMATION8());
                ps.setString(58, extraInformationBean.getPEI_INFORMATION9());
                ps.setString(59, extraInformationBean.getPEI_INFORMATION10());
                ps.setString(60, extraInformationBean.getPEI_INFORMATION11());
                ps.setString(61, extraInformationBean.getPEI_INFORMATION12());
                ps.setString(62, extraInformationBean.getPEI_INFORMATION13());
                ps.setString(63, extraInformationBean.getPEI_INFORMATION14());
                ps.setString(64, extraInformationBean.getPEI_INFORMATION15());
                ps.setString(65, extraInformationBean.getPEI_INFORMATION16());
                ps.setString(66, extraInformationBean.getPEI_INFORMATION17());
                ps.setString(67, extraInformationBean.getPEI_INFORMATION18());
                ps.setString(68, extraInformationBean.getPEI_INFORMATION19());
                ps.setString(69, extraInformationBean.getPEI_INFORMATION20());
                ps.setString(70, extraInformationBean.getPEI_INFORMATION21());
                ps.setString(71, extraInformationBean.getPEI_INFORMATION22());
                ps.setString(72, extraInformationBean.getPEI_INFORMATION23());
                ps.setString(73, extraInformationBean.getPEI_INFORMATION24());
                ps.setString(74, extraInformationBean.getPEI_INFORMATION25());
                ps.setString(75, extraInformationBean.getPEI_INFORMATION26());
                ps.setString(76, extraInformationBean.getPEI_INFORMATION27());
                ps.setString(77, extraInformationBean.getPEI_INFORMATION28());
                ps.setString(78, extraInformationBean.getPEI_INFORMATION29());
                ps.setString(79, extraInformationBean.getPEI_INFORMATION30());
                //Pei_INFORMATION_NUMBER
                ps.setString(80,
                             extraInformationBean.getPEI_INFORMATION_NUMBER1());
                ps.setString(81,
                             extraInformationBean.getPEI_INFORMATION_NUMBER2());
                ps.setString(82,
                             extraInformationBean.getPEI_INFORMATION_NUMBER3());
                ps.setString(83,
                             extraInformationBean.getPEI_INFORMATION_NUMBER4());
                ps.setString(84,
                             extraInformationBean.getPEI_INFORMATION_NUMBER5());
                ps.setString(85,
                             extraInformationBean.getPEI_INFORMATION_NUMBER6());
                ps.setString(86,
                             extraInformationBean.getPEI_INFORMATION_NUMBER7());
                ps.setString(87,
                             extraInformationBean.getPEI_INFORMATION_NUMBER8());
                ps.setString(88,
                             extraInformationBean.getPEI_INFORMATION_NUMBER9());
                ps.setString(89,
                             extraInformationBean.getPEI_INFORMATION_NUMBER10());
                ps.setString(90,
                             extraInformationBean.getPEI_INFORMATION_NUMBER11());
                ps.setString(91,
                             extraInformationBean.getPEI_INFORMATION_NUMBER12());
                ps.setString(92,
                             extraInformationBean.getPEI_INFORMATION_NUMBER13());
                ps.setString(93,
                             extraInformationBean.getPEI_INFORMATION_NUMBER14());
                ps.setString(94,
                             extraInformationBean.getPEI_INFORMATION_NUMBER15());
                ps.setString(95,
                             extraInformationBean.getPEI_INFORMATION_NUMBER16());
                ps.setString(96,
                             extraInformationBean.getPEI_INFORMATION_NUMBER17());
                ps.setString(97,
                             extraInformationBean.getPEI_INFORMATION_NUMBER18());
                ps.setString(98,
                             extraInformationBean.getPEI_INFORMATION_NUMBER19());
                ps.setString(99,
                             extraInformationBean.getPEI_INFORMATION_NUMBER20());
                //Pei_INFORMATION_DATE
                //extraInformationBean.setPEI_INFORMATION_DATE1(rh.convertToGregorian(extraInformationBean.getPEI_INFORMATION_DATE1()));
                //extraInformationBean.setPEI_INFORMATION_DATE2(rh.convertToGregorian(extraInformationBean.getPEI_INFORMATION_DATE2()));
                ps.setString(100,
                             extraInformationBean.getPEI_INFORMATION_DATE1());
                ps.setString(101,
                             extraInformationBean.getPEI_INFORMATION_DATE2());
                ps.setString(102,
                             extraInformationBean.getPEI_INFORMATION_DATE3());
                ps.setString(103,
                             extraInformationBean.getPEI_INFORMATION_DATE4());
                ps.setString(104,
                             extraInformationBean.getPEI_INFORMATION_DATE5());
                ps.setString(105,
                             extraInformationBean.getPEI_INFORMATION_DATE6());
                ps.setString(106,
                             extraInformationBean.getPEI_INFORMATION_DATE7());
                ps.setString(107,
                             extraInformationBean.getPEI_INFORMATION_DATE8());
                ps.setString(108,
                             extraInformationBean.getPEI_INFORMATION_DATE9());
                ps.setString(109,
                             extraInformationBean.getPEI_INFORMATION_DATE10());
                ps.setString(110,
                             extraInformationBean.getPEI_INFORMATION_DATE11());
                ps.setString(111,
                             extraInformationBean.getPEI_INFORMATION_DATE12());
                ps.setString(112,
                             extraInformationBean.getPEI_INFORMATION_DATE13());
                ps.setString(113,
                             extraInformationBean.getPEI_INFORMATION_DATE14());
                ps.setString(114,
                             extraInformationBean.getPEI_INFORMATION_DATE15());

                ps.setString(115,
                             extraInformationBean.getOBJECT_VERSION_NUMBER());
                ps.setString(116, extraInformationBean.getLAST_UPDATE_LOGIN());
                ps.setString(117, extraInformationBean.getEitLbl());
                ps.setString(118, extraInformationBean.getPersonName());
                ps.executeUpdate();
                sfBean.setType(extraInformationBean.getCode());
                sfBean.setTypeTableName("XXX_PER_PEOPLE_EXTRA_INFO");
                sfBean.setTransactionId(Integer.toString(extraInformationBean.getId()));
                sfBean.setPresonId(extraInformationBean.getPerson_id());
                sfBean.setCreated_by(extraInformationBean.getCreated_by());
                sfBean.setPersonNumber(extraInformationBean.getPerson_number());
                selfServiceDao.insertIntoSelfService(sfBean);
                processApproval(extraInformationBean, conditionCode);

            } else if (transactionType.equals("EDIT")) {
                query =
                        "UPDATE  " + " " + getSchema_Name() + ".XXX_PER_PEOPLE_EXTRA_INFO SET\n" +
                        "EIT_CODE =NVL(?,EIT_CODE),EIT =NVL(?,EIT),STATUS =NVL(?,STATUS),PERSON_NUMBER =NVL(?,PERSON_NUMBER),PERSON_ID =NVL(?,PERSON_ID),CREATED_BY =NVL(?,CREATED_BY),UPDATED_BY =NVL(?,UPDATED_BY),UPDATED_DATE =sysdate,LINE_MANAGER =NVL(?,LINE_MANAGER),MANAGER_OF_MANAGER =NVL(?,MANAGER_OF_MANAGER),URL =NVL(?,URL)\n" +
                        ",EIT_NAME =NVL(?,EIT_NAME),PERSON_EXTRA_INFO_ID =NVL(?,PERSON_EXTRA_INFO_ID),EFFECTIVE_START_DATE =NVL(?,EFFECTIVE_START_DATE),EFFECTIVE_END_DATE =NVL(?,EFFECTIVE_END_DATE),INFORMATION_TYPE =NVL(?,INFORMATION_TYPE),PEI_ATTRIBUTE_CATEGORY =NVL(?,PEI_ATTRIBUTE_CATEGORY),PEI_ATTRIBUTE1 =NVL(?,PEI_ATTRIBUTE1),PEI_ATTRIBUTE2 =NVL(?,PEI_ATTRIBUTE2)\n" +
                        ",PEI_ATTRIBUTE3 =NVL(?,PEI_ATTRIBUTE3),PEI_ATTRIBUTE4 =NVL(?,PEI_ATTRIBUTE4),PEI_ATTRIBUTE5 =NVL(?,PEI_ATTRIBUTE5),PEI_ATTRIBUTE6 =NVL(?,PEI_ATTRIBUTE6),PEI_ATTRIBUTE7 =NVL(?,PEI_ATTRIBUTE7),PEI_ATTRIBUTE8 =NVL(?,PEI_ATTRIBUTE8),PEI_ATTRIBUTE9 =NVL(?,PEI_ATTRIBUTE9),PEI_ATTRIBUTE10 =NVL(?,PEI_ATTRIBUTE10),PEI_ATTRIBUTE11 =NVL(?,PEI_ATTRIBUTE11)\n" +
                        ",PEI_ATTRIBUTE12 =NVL(?,PEI_ATTRIBUTE12),PEI_ATTRIBUTE13 =NVL(?,PEI_ATTRIBUTE13),PEI_ATTRIBUTE14 =NVL(?,PEI_ATTRIBUTE14),PEI_ATTRIBUTE15 =NVL(?,PEI_ATTRIBUTE15),PEI_ATTRIBUTE16 =NVL(?,PEI_ATTRIBUTE16),PEI_ATTRIBUTE17 =NVL(?,PEI_ATTRIBUTE17),PEI_ATTRIBUTE18 =NVL(?,PEI_ATTRIBUTE18),PEI_ATTRIBUTE19 =NVL(?,PEI_ATTRIBUTE19),PEI_ATTRIBUTE20 =NVL(?,PEI_ATTRIBUTE20)\n" +
                        ",PEI_ATTRIBUTE21 =NVL(?,PEI_ATTRIBUTE21),PEI_ATTRIBUTE22 =NVL(?,PEI_ATTRIBUTE22),PEI_ATTRIBUTE23 =NVL(?,PEI_ATTRIBUTE23),PEI_ATTRIBUTE24 =NVL(?,PEI_ATTRIBUTE24),PEI_ATTRIBUTE25 =NVL(?,PEI_ATTRIBUTE25),PEI_ATTRIBUTE26 =NVL(?,PEI_ATTRIBUTE26),PEI_ATTRIBUTE27 =NVL(?,PEI_ATTRIBUTE27),PEI_ATTRIBUTE28 =NVL(?,PEI_ATTRIBUTE28),PEI_ATTRIBUTE29 =NVL(?,PEI_ATTRIBUTE29)\n" +
                        ",PEI_ATTRIBUTE30 =NVL(?,PEI_ATTRIBUTE30),PEI_INFORMATION_CATEGORY =NVL(?,PEI_INFORMATION_CATEGORY),PEI_INFORMATION1 =NVL(?,PEI_INFORMATION1),PEI_INFORMATION2 =NVL(?,PEI_INFORMATION2),PEI_INFORMATION3 =NVL(?,PEI_INFORMATION3),PEI_INFORMATION4 =NVL(?,PEI_INFORMATION4),PEI_INFORMATION5 =NVL(?,PEI_INFORMATION5),PEI_INFORMATION6 =NVL(?,PEI_INFORMATION6)\n" +
                        ",PEI_INFORMATION7 =NVL(?,PEI_INFORMATION7),PEI_INFORMATION8 =NVL(?,PEI_INFORMATION8),PEI_INFORMATION9 =NVL(?,PEI_INFORMATION9),PEI_INFORMATION10 =NVL(?,PEI_INFORMATION10),PEI_INFORMATION11 =NVL(?,PEI_INFORMATION11),PEI_INFORMATION12 =NVL(?,PEI_INFORMATION12),PEI_INFORMATION13 =NVL(?,PEI_INFORMATION13),PEI_INFORMATION14 =NVL(?,PEI_INFORMATION14)\n" +
                        ",PEI_INFORMATION15 =NVL(?,PEI_INFORMATION15),PEI_INFORMATION16 =NVL(?,PEI_INFORMATION16),PEI_INFORMATION17 =NVL(?,PEI_INFORMATION17),PEI_INFORMATION18 =NVL(?,PEI_INFORMATION18),PEI_INFORMATION19 =NVL(?,PEI_INFORMATION19),PEI_INFORMATION20 =NVL(?,PEI_INFORMATION20),PEI_INFORMATION21 =NVL(?,PEI_INFORMATION21),PEI_INFORMATION22 =NVL(?,PEI_INFORMATION22)\n" +
                        ",PEI_INFORMATION23 =NVL(?,PEI_INFORMATION23),PEI_INFORMATION24 =NVL(?,PEI_INFORMATION24),PEI_INFORMATION25 =NVL(?,PEI_INFORMATION25),PEI_INFORMATION26 =NVL(?,PEI_INFORMATION26),PEI_INFORMATION27 =NVL(?,PEI_INFORMATION27),PEI_INFORMATION28 =NVL(?,PEI_INFORMATION28),PEI_INFORMATION29 =NVL(?,PEI_INFORMATION29),PEI_INFORMATION30 =NVL(?,PEI_INFORMATION30)\n" +
                        ",PEI_INFORMATION_NUMBER1 =NVL(?,PEI_INFORMATION_NUMBER1),PEI_INFORMATION_NUMBER2 =NVL(?,PEI_INFORMATION_NUMBER2),PEI_INFORMATION_NUMBER3 =NVL(?,PEI_INFORMATION_NUMBER3),PEI_INFORMATION_NUMBER4 =NVL(?,PEI_INFORMATION_NUMBER4),PEI_INFORMATION_NUMBER5 =NVL(?,PEI_INFORMATION_NUMBER5),PEI_INFORMATION_NUMBER6 =NVL(?,PEI_INFORMATION_NUMBER6)\n" +
                        ",PEI_INFORMATION_NUMBER7 =NVL(?,PEI_INFORMATION_NUMBER7),PEI_INFORMATION_NUMBER8 =NVL(?,PEI_INFORMATION_NUMBER8),PEI_INFORMATION_NUMBER9 =NVL(?,PEI_INFORMATION_NUMBER9),PEI_INFORMATION_NUMBER10 =NVL(?,PEI_INFORMATION_NUMBER10),PEI_INFORMATION_NUMBER11 =NVL(?,PEI_INFORMATION_NUMBER11),PEI_INFORMATION_NUMBER12 =NVL(?,PEI_INFORMATION_NUMBER12)\n" +
                        ",PEI_INFORMATION_NUMBER13 =NVL(?,PEI_INFORMATION_NUMBER13),PEI_INFORMATION_NUMBER14 =NVL(?,PEI_INFORMATION_NUMBER14),PEI_INFORMATION_NUMBER15 =NVL(?,PEI_INFORMATION_NUMBER15),PEI_INFORMATION_NUMBER16 =NVL(?,PEI_INFORMATION_NUMBER16),PEI_INFORMATION_NUMBER17 =NVL(?,PEI_INFORMATION_NUMBER17),PEI_INFORMATION_NUMBER18 =NVL(?,PEI_INFORMATION_NUMBER18)\n" +
                        ",PEI_INFORMATION_NUMBER19 =NVL(?,PEI_INFORMATION_NUMBER19),PEI_INFORMATION_NUMBER20 =NVL(?,PEI_INFORMATION_NUMBER20),PEI_INFORMATION_DATE1 =NVL(?,PEI_INFORMATION_DATE1),PEI_INFORMATION_DATE2 =NVL(?,PEI_INFORMATION_DATE2),PEI_INFORMATION_DATE3 =NVL(?,PEI_INFORMATION_DATE3),PEI_INFORMATION_DATE4 =NVL(?,PEI_INFORMATION_DATE4),PEI_INFORMATION_DATE5 =NVL(?,PEI_INFORMATION_DATE5)\n" +
                        ",PEI_INFORMATION_DATE6 =NVL(?,PEI_INFORMATION_DATE6),PEI_INFORMATION_DATE7 =NVL(?,PEI_INFORMATION_DATE7),PEI_INFORMATION_DATE8 =NVL(?,PEI_INFORMATION_DATE8),PEI_INFORMATION_DATE9 =NVL(?,PEI_INFORMATION_DATE9),PEI_INFORMATION_DATE10 =NVL(?,PEI_INFORMATION_DATE10),PEI_INFORMATION_DATE11 =NVL(?,PEI_INFORMATION_DATE11),PEI_INFORMATION_DATE12 =NVL(?,PEI_INFORMATION_DATE12)\n" +
                        ",PEI_INFORMATION_DATE13 =NVL(?,PEI_INFORMATION_DATE13),PEI_INFORMATION_DATE14 =NVL(?,PEI_INFORMATION_DATE14),PEI_INFORMATION_DATE15 =NVL(?,PEI_INFORMATION_DATE15),OBJECT_VERSION_NUMBER  =NVL(?,OBJECT_VERSION_NUMBER),LAST_UPDATE_LOGIN =NVL(?,LAST_UPDATE_LOGIN),EITLBL=NVL(?,EITLBL),PERSON_NAME=NVL(?,PERSON_NAME) WHERE ID = ?";

                ps = connection.prepareStatement(query);

                ps.setString(1, extraInformationBean.getCode());
                ps.setString(2, extraInformationBean.getEit());
                ps.setString(3, extraInformationBean.getStatus());
                ps.setString(4, extraInformationBean.getPerson_number());
                ps.setString(5, extraInformationBean.getPerson_id());
                ps.setString(6, extraInformationBean.getCreated_by());
                //ps.setString(7, extraInformationBean.getCreation_date());
                ps.setString(7, extraInformationBean.getUpdated_by());
                ps.setString(8, extraInformationBean.getLine_manager());
                ps.setString(9, extraInformationBean.getManage_of_manager());
                ps.setString(10, extraInformationBean.getUrl());
                ps.setString(11, extraInformationBean.getEit_name());
                //New
                ps.setString(12,
                             extraInformationBean.getPERSON_EXTRA_INFO_ID());
                ps.setString(13,
                             extraInformationBean.getEFFECTIVE_START_DATE());
                ps.setString(14, extraInformationBean.getEFFECTIVE_END_DATE());
                ps.setString(15, extraInformationBean.getINFORMATION_TYPE());
                ps.setString(16,
                             extraInformationBean.getPEI_ATTRIBUTE_CATEGORY());
                //PEI_Attribute
                ps.setString(17, extraInformationBean.getPEI_ATTRIBUTE1());
                ps.setString(18, extraInformationBean.getPEI_ATTRIBUTE2());
                ps.setString(19, extraInformationBean.getPEI_ATTRIBUTE3());
                ps.setString(20, extraInformationBean.getPEI_ATTRIBUTE4());
                ps.setString(21, extraInformationBean.getPEI_ATTRIBUTE5());
                ps.setString(22, extraInformationBean.getPEI_ATTRIBUTE6());
                ps.setString(23, extraInformationBean.getPEI_ATTRIBUTE7());
                ps.setString(24, extraInformationBean.getPEI_ATTRIBUTE8());
                ps.setString(25, extraInformationBean.getPEI_ATTRIBUTE9());
                ps.setString(26, extraInformationBean.getPEI_ATTRIBUTE10());
                ps.setString(27, extraInformationBean.getPEI_ATTRIBUTE11());
                ps.setString(28, extraInformationBean.getPEI_ATTRIBUTE12());
                ps.setString(29, extraInformationBean.getPEI_ATTRIBUTE13());
                ps.setString(30, extraInformationBean.getPEI_ATTRIBUTE14());
                ps.setString(31, extraInformationBean.getPEI_ATTRIBUTE15());
                ps.setString(32, extraInformationBean.getPEI_ATTRIBUTE16());
                ps.setString(33, extraInformationBean.getPEI_ATTRIBUTE17());
                ps.setString(34, extraInformationBean.getPEI_ATTRIBUTE18());
                ps.setString(35, extraInformationBean.getPEI_ATTRIBUTE19());
                ps.setString(36, extraInformationBean.getPEI_ATTRIBUTE20());
                ps.setString(37, extraInformationBean.getPEI_ATTRIBUTE21());
                ps.setString(38, extraInformationBean.getPEI_ATTRIBUTE22());
                ps.setString(39, extraInformationBean.getPEI_ATTRIBUTE23());
                ps.setString(40, extraInformationBean.getPEI_ATTRIBUTE24());
                ps.setString(41, extraInformationBean.getPEI_ATTRIBUTE25());
                ps.setString(42, extraInformationBean.getPEI_ATTRIBUTE26());
                ps.setString(43, extraInformationBean.getPEI_ATTRIBUTE27());
                ps.setString(44, extraInformationBean.getPEI_ATTRIBUTE28());
                ps.setString(45, extraInformationBean.getPEI_ATTRIBUTE29());
                ps.setString(46, extraInformationBean.getPEI_ATTRIBUTE30());
                //
                ps.setString(47,
                             extraInformationBean.getPEI_INFORMATION_CATEGORY());
                //Pei_INFORMATION
                ps.setString(48, extraInformationBean.getPEI_INFORMATION1());
                ps.setString(49, extraInformationBean.getPEI_INFORMATION2());
                ps.setString(50, extraInformationBean.getPEI_INFORMATION3());
                ps.setString(51, extraInformationBean.getPEI_INFORMATION4());
                ps.setString(52, extraInformationBean.getPEI_INFORMATION5());
                ps.setString(53, extraInformationBean.getPEI_INFORMATION6());
                ps.setString(54, extraInformationBean.getPEI_INFORMATION7());
                ps.setString(55, extraInformationBean.getPEI_INFORMATION8());
                ps.setString(56, extraInformationBean.getPEI_INFORMATION9());
                ps.setString(57, extraInformationBean.getPEI_INFORMATION10());
                ps.setString(58, extraInformationBean.getPEI_INFORMATION11());
                ps.setString(59, extraInformationBean.getPEI_INFORMATION12());
                ps.setString(60, extraInformationBean.getPEI_INFORMATION13());
                ps.setString(61, extraInformationBean.getPEI_INFORMATION14());
                ps.setString(62, extraInformationBean.getPEI_INFORMATION15());
                ps.setString(63, extraInformationBean.getPEI_INFORMATION16());
                ps.setString(64, extraInformationBean.getPEI_INFORMATION17());
                ps.setString(65, extraInformationBean.getPEI_INFORMATION18());
                ps.setString(66, extraInformationBean.getPEI_INFORMATION19());
                ps.setString(67, extraInformationBean.getPEI_INFORMATION20());
                ps.setString(68, extraInformationBean.getPEI_INFORMATION21());
                ps.setString(69, extraInformationBean.getPEI_INFORMATION22());
                ps.setString(70, extraInformationBean.getPEI_INFORMATION23());
                ps.setString(71, extraInformationBean.getPEI_INFORMATION24());
                ps.setString(72, extraInformationBean.getPEI_INFORMATION25());
                ps.setString(73, extraInformationBean.getPEI_INFORMATION26());
                ps.setString(74, extraInformationBean.getPEI_INFORMATION27());
                ps.setString(75, extraInformationBean.getPEI_INFORMATION28());
                ps.setString(76, extraInformationBean.getPEI_INFORMATION29());
                ps.setString(77, extraInformationBean.getPEI_INFORMATION30());
                //Pei_INFORMATION_NUMBER
                ps.setString(78,
                             extraInformationBean.getPEI_INFORMATION_NUMBER1());
                ps.setString(79,
                             extraInformationBean.getPEI_INFORMATION_NUMBER2());
                ps.setString(80,
                             extraInformationBean.getPEI_INFORMATION_NUMBER3());
                ps.setString(81,
                             extraInformationBean.getPEI_INFORMATION_NUMBER4());
                ps.setString(82,
                             extraInformationBean.getPEI_INFORMATION_NUMBER5());
                ps.setString(83,
                             extraInformationBean.getPEI_INFORMATION_NUMBER6());
                ps.setString(84,
                             extraInformationBean.getPEI_INFORMATION_NUMBER7());
                ps.setString(85,
                             extraInformationBean.getPEI_INFORMATION_NUMBER8());
                ps.setString(86,
                             extraInformationBean.getPEI_INFORMATION_NUMBER9());
                ps.setString(87,
                             extraInformationBean.getPEI_INFORMATION_NUMBER10());
                ps.setString(88,
                             extraInformationBean.getPEI_INFORMATION_NUMBER11());
                ps.setString(89,
                             extraInformationBean.getPEI_INFORMATION_NUMBER12());
                ps.setString(90,
                             extraInformationBean.getPEI_INFORMATION_NUMBER13());
                ps.setString(91,
                             extraInformationBean.getPEI_INFORMATION_NUMBER14());
                ps.setString(92,
                             extraInformationBean.getPEI_INFORMATION_NUMBER15());
                ps.setString(93,
                             extraInformationBean.getPEI_INFORMATION_NUMBER16());
                ps.setString(94,
                             extraInformationBean.getPEI_INFORMATION_NUMBER17());
                ps.setString(95,
                             extraInformationBean.getPEI_INFORMATION_NUMBER18());
                ps.setString(96,
                             extraInformationBean.getPEI_INFORMATION_NUMBER19());
                ps.setString(97,
                             extraInformationBean.getPEI_INFORMATION_NUMBER20());
                //Pei_INFORMATION_DATE
                ps.setString(98,
                             extraInformationBean.getPEI_INFORMATION_DATE1());
                ps.setString(99,
                             extraInformationBean.getPEI_INFORMATION_DATE2());
                ps.setString(100,
                             extraInformationBean.getPEI_INFORMATION_DATE3());
                ps.setString(101,
                             extraInformationBean.getPEI_INFORMATION_DATE4());
                ps.setString(102,
                             extraInformationBean.getPEI_INFORMATION_DATE5());
                ps.setString(103,
                             extraInformationBean.getPEI_INFORMATION_DATE6());
                ps.setString(104,
                             extraInformationBean.getPEI_INFORMATION_DATE7());
                ps.setString(105,
                             extraInformationBean.getPEI_INFORMATION_DATE8());
                ps.setString(106,
                             extraInformationBean.getPEI_INFORMATION_DATE9());
                ps.setString(107,
                             extraInformationBean.getPEI_INFORMATION_DATE10());
                ps.setString(108,
                             extraInformationBean.getPEI_INFORMATION_DATE11());
                ps.setString(109,
                             extraInformationBean.getPEI_INFORMATION_DATE12());
                ps.setString(110,
                             extraInformationBean.getPEI_INFORMATION_DATE13());
                ps.setString(111,
                             extraInformationBean.getPEI_INFORMATION_DATE14());
                ps.setString(112,
                             extraInformationBean.getPEI_INFORMATION_DATE15());

                ps.setString(113,
                             extraInformationBean.getOBJECT_VERSION_NUMBER());
                ps.setString(114, extraInformationBean.getLAST_UPDATE_LOGIN());
                ps.setString(115, extraInformationBean.getEitLbl());
                ps.setString(116, extraInformationBean.getPersonName());
                ps.setInt(117, extraInformationBean.getId());
                ps.executeUpdate();
            } else if (transactionType.equals("draft")) {
                query =
                        "UPDATE  " + getSchema_Name() + ".XXX_PER_PEOPLE_EXTRA_INFO SET\n" +
                        "EIT_CODE =NVL(?,EIT_CODE),EIT =NVL(?,EIT),STATUS =NVL(?,STATUS),PERSON_NUMBER =NVL(?,PERSON_NUMBER),PERSON_ID =NVL(?,PERSON_ID),CREATED_BY =NVL(?,CREATED_BY),UPDATED_BY =NVL(?,UPDATED_BY),UPDATED_DATE =sysdate,LINE_MANAGER =NVL(?,LINE_MANAGER),MANAGER_OF_MANAGER =NVL(?,MANAGER_OF_MANAGER),URL =NVL(?,URL)\n" +
                        ",EIT_NAME =NVL(?,EIT_NAME),PERSON_EXTRA_INFO_ID =NVL(?,PERSON_EXTRA_INFO_ID),EFFECTIVE_START_DATE =NVL(?,EFFECTIVE_START_DATE),EFFECTIVE_END_DATE =NVL(?,EFFECTIVE_END_DATE),INFORMATION_TYPE =NVL(?,INFORMATION_TYPE),PEI_ATTRIBUTE_CATEGORY =NVL(?,PEI_ATTRIBUTE_CATEGORY),PEI_ATTRIBUTE1 =NVL(?,PEI_ATTRIBUTE1),PEI_ATTRIBUTE2 =NVL(?,PEI_ATTRIBUTE2)\n" +
                        ",PEI_ATTRIBUTE3 =NVL(?,PEI_ATTRIBUTE3),PEI_ATTRIBUTE4 =NVL(?,PEI_ATTRIBUTE4),PEI_ATTRIBUTE5 =NVL(?,PEI_ATTRIBUTE5),PEI_ATTRIBUTE6 =NVL(?,PEI_ATTRIBUTE6),PEI_ATTRIBUTE7 =NVL(?,PEI_ATTRIBUTE7),PEI_ATTRIBUTE8 =NVL(?,PEI_ATTRIBUTE8),PEI_ATTRIBUTE9 =NVL(?,PEI_ATTRIBUTE9),PEI_ATTRIBUTE10 =NVL(?,PEI_ATTRIBUTE10),PEI_ATTRIBUTE11 =NVL(?,PEI_ATTRIBUTE11)\n" +
                        ",PEI_ATTRIBUTE12 =NVL(?,PEI_ATTRIBUTE12),PEI_ATTRIBUTE13 =NVL(?,PEI_ATTRIBUTE13),PEI_ATTRIBUTE14 =NVL(?,PEI_ATTRIBUTE14),PEI_ATTRIBUTE15 =NVL(?,PEI_ATTRIBUTE15),PEI_ATTRIBUTE16 =NVL(?,PEI_ATTRIBUTE16),PEI_ATTRIBUTE17 =NVL(?,PEI_ATTRIBUTE17),PEI_ATTRIBUTE18 =NVL(?,PEI_ATTRIBUTE18),PEI_ATTRIBUTE19 =NVL(?,PEI_ATTRIBUTE19),PEI_ATTRIBUTE20 =NVL(?,PEI_ATTRIBUTE20)\n" +
                        ",PEI_ATTRIBUTE21 =NVL(?,PEI_ATTRIBUTE21),PEI_ATTRIBUTE22 =NVL(?,PEI_ATTRIBUTE22),PEI_ATTRIBUTE23 =NVL(?,PEI_ATTRIBUTE23),PEI_ATTRIBUTE24 =NVL(?,PEI_ATTRIBUTE24),PEI_ATTRIBUTE25 =NVL(?,PEI_ATTRIBUTE25),PEI_ATTRIBUTE26 =NVL(?,PEI_ATTRIBUTE26),PEI_ATTRIBUTE27 =NVL(?,PEI_ATTRIBUTE27),PEI_ATTRIBUTE28 =NVL(?,PEI_ATTRIBUTE28),PEI_ATTRIBUTE29 =NVL(?,PEI_ATTRIBUTE29)\n" +
                        ",PEI_ATTRIBUTE30 =NVL(?,PEI_ATTRIBUTE30),PEI_INFORMATION_CATEGORY =NVL(?,PEI_INFORMATION_CATEGORY),PEI_INFORMATION1 =NVL(?,PEI_INFORMATION1),PEI_INFORMATION2 =NVL(?,PEI_INFORMATION2),PEI_INFORMATION3 =NVL(?,PEI_INFORMATION3),PEI_INFORMATION4 =NVL(?,PEI_INFORMATION4),PEI_INFORMATION5 =NVL(?,PEI_INFORMATION5),PEI_INFORMATION6 =NVL(?,PEI_INFORMATION6)\n" +
                        ",PEI_INFORMATION7 =NVL(?,PEI_INFORMATION7),PEI_INFORMATION8 =NVL(?,PEI_INFORMATION8),PEI_INFORMATION9 =NVL(?,PEI_INFORMATION9),PEI_INFORMATION10 =NVL(?,PEI_INFORMATION10),PEI_INFORMATION11 =NVL(?,PEI_INFORMATION11),PEI_INFORMATION12 =NVL(?,PEI_INFORMATION12),PEI_INFORMATION13 =NVL(?,PEI_INFORMATION13),PEI_INFORMATION14 =NVL(?,PEI_INFORMATION14)\n" +
                        ",PEI_INFORMATION15 =NVL(?,PEI_INFORMATION15),PEI_INFORMATION16 =NVL(?,PEI_INFORMATION16),PEI_INFORMATION17 =NVL(?,PEI_INFORMATION17),PEI_INFORMATION18 =NVL(?,PEI_INFORMATION18),PEI_INFORMATION19 =NVL(?,PEI_INFORMATION19),PEI_INFORMATION20 =NVL(?,PEI_INFORMATION20),PEI_INFORMATION21 =NVL(?,PEI_INFORMATION21),PEI_INFORMATION22 =NVL(?,PEI_INFORMATION22)\n" +
                        ",PEI_INFORMATION23 =NVL(?,PEI_INFORMATION23),PEI_INFORMATION24 =NVL(?,PEI_INFORMATION24),PEI_INFORMATION25 =NVL(?,PEI_INFORMATION25),PEI_INFORMATION26 =NVL(?,PEI_INFORMATION26),PEI_INFORMATION27 =NVL(?,PEI_INFORMATION27),PEI_INFORMATION28 =NVL(?,PEI_INFORMATION28),PEI_INFORMATION29 =NVL(?,PEI_INFORMATION29),PEI_INFORMATION30 =NVL(?,PEI_INFORMATION30)\n" +
                        ",PEI_INFORMATION_NUMBER1 =NVL(?,PEI_INFORMATION_NUMBER1),PEI_INFORMATION_NUMBER2 =NVL(?,PEI_INFORMATION_NUMBER2),PEI_INFORMATION_NUMBER3 =NVL(?,PEI_INFORMATION_NUMBER3),PEI_INFORMATION_NUMBER4 =NVL(?,PEI_INFORMATION_NUMBER4),PEI_INFORMATION_NUMBER5 =NVL(?,PEI_INFORMATION_NUMBER5),PEI_INFORMATION_NUMBER6 =NVL(?,PEI_INFORMATION_NUMBER6)\n" +
                        ",PEI_INFORMATION_NUMBER7 =NVL(?,PEI_INFORMATION_NUMBER7),PEI_INFORMATION_NUMBER8 =NVL(?,PEI_INFORMATION_NUMBER8),PEI_INFORMATION_NUMBER9 =NVL(?,PEI_INFORMATION_NUMBER9),PEI_INFORMATION_NUMBER10 =NVL(?,PEI_INFORMATION_NUMBER10),PEI_INFORMATION_NUMBER11 =NVL(?,PEI_INFORMATION_NUMBER11),PEI_INFORMATION_NUMBER12 =NVL(?,PEI_INFORMATION_NUMBER12)\n" +
                        ",PEI_INFORMATION_NUMBER13 =NVL(?,PEI_INFORMATION_NUMBER13),PEI_INFORMATION_NUMBER14 =NVL(?,PEI_INFORMATION_NUMBER14),PEI_INFORMATION_NUMBER15 =NVL(?,PEI_INFORMATION_NUMBER15),PEI_INFORMATION_NUMBER16 =NVL(?,PEI_INFORMATION_NUMBER16),PEI_INFORMATION_NUMBER17 =NVL(?,PEI_INFORMATION_NUMBER17),PEI_INFORMATION_NUMBER18 =NVL(?,PEI_INFORMATION_NUMBER18)\n" +
                        ",PEI_INFORMATION_NUMBER19 =NVL(?,PEI_INFORMATION_NUMBER19),PEI_INFORMATION_NUMBER20 =NVL(?,PEI_INFORMATION_NUMBER20),PEI_INFORMATION_DATE1 =NVL(?,PEI_INFORMATION_DATE1),PEI_INFORMATION_DATE2 =NVL(?,PEI_INFORMATION_DATE2),PEI_INFORMATION_DATE3 =NVL(?,PEI_INFORMATION_DATE3),PEI_INFORMATION_DATE4 =NVL(?,PEI_INFORMATION_DATE4),PEI_INFORMATION_DATE5 =NVL(?,PEI_INFORMATION_DATE5)\n" +
                        ",PEI_INFORMATION_DATE6 =NVL(?,PEI_INFORMATION_DATE6),PEI_INFORMATION_DATE7 =NVL(?,PEI_INFORMATION_DATE7),PEI_INFORMATION_DATE8 =NVL(?,PEI_INFORMATION_DATE8),PEI_INFORMATION_DATE9 =NVL(?,PEI_INFORMATION_DATE9),PEI_INFORMATION_DATE10 =NVL(?,PEI_INFORMATION_DATE10),PEI_INFORMATION_DATE11 =NVL(?,PEI_INFORMATION_DATE11),PEI_INFORMATION_DATE12 =NVL(?,PEI_INFORMATION_DATE12)\n" +
                        ",PEI_INFORMATION_DATE13 =NVL(?,PEI_INFORMATION_DATE13),PEI_INFORMATION_DATE14 =NVL(?,PEI_INFORMATION_DATE14),PEI_INFORMATION_DATE15 =NVL(?,PEI_INFORMATION_DATE15),OBJECT_VERSION_NUMBER  =NVL(?,OBJECT_VERSION_NUMBER),LAST_UPDATE_LOGIN =NVL(?,LAST_UPDATE_LOGIN),EITLBL=NVL(?,EITLBL),PERSON_NAME=NVL(?,PERSON_NAME) WHERE ID = ?";


                ps = connection.prepareStatement(query);


                ps.setString(1, extraInformationBean.getCode());
                ps.setString(2, extraInformationBean.getEit());
                ps.setString(3, extraInformationBean.getStatus());
                ps.setString(4, extraInformationBean.getPerson_number());
                ps.setString(5, extraInformationBean.getPerson_id());
                ps.setString(6, extraInformationBean.getCreated_by());
                //ps.setString(7, extraInformationBean.getCreation_date());
                ps.setString(7, extraInformationBean.getUpdated_by());
                ps.setString(8, extraInformationBean.getLine_manager());
                ps.setString(9, extraInformationBean.getManage_of_manager());
                ps.setString(10, extraInformationBean.getUrl());
                ps.setString(11, extraInformationBean.getEit_name());
                //New
                ps.setString(12,
                             extraInformationBean.getPERSON_EXTRA_INFO_ID());
                ps.setString(13,
                             extraInformationBean.getEFFECTIVE_START_DATE());
                ps.setString(14, extraInformationBean.getEFFECTIVE_END_DATE());
                ps.setString(15, extraInformationBean.getINFORMATION_TYPE());
                ps.setString(16,
                             extraInformationBean.getPEI_ATTRIBUTE_CATEGORY());
                //PEI_Attribute
                ps.setString(17, extraInformationBean.getPEI_ATTRIBUTE1());
                ps.setString(18, extraInformationBean.getPEI_ATTRIBUTE2());
                ps.setString(19, extraInformationBean.getPEI_ATTRIBUTE3());
                ps.setString(20, extraInformationBean.getPEI_ATTRIBUTE4());
                ps.setString(21, extraInformationBean.getPEI_ATTRIBUTE5());
                ps.setString(22, extraInformationBean.getPEI_ATTRIBUTE6());
                ps.setString(23, extraInformationBean.getPEI_ATTRIBUTE7());
                ps.setString(24, extraInformationBean.getPEI_ATTRIBUTE8());
                ps.setString(25, extraInformationBean.getPEI_ATTRIBUTE9());
                ps.setString(26, extraInformationBean.getPEI_ATTRIBUTE10());
                ps.setString(27, extraInformationBean.getPEI_ATTRIBUTE11());
                ps.setString(28, extraInformationBean.getPEI_ATTRIBUTE12());
                ps.setString(29, extraInformationBean.getPEI_ATTRIBUTE13());
                ps.setString(30, extraInformationBean.getPEI_ATTRIBUTE14());
                ps.setString(31, extraInformationBean.getPEI_ATTRIBUTE15());
                ps.setString(32, extraInformationBean.getPEI_ATTRIBUTE16());
                ps.setString(33, extraInformationBean.getPEI_ATTRIBUTE17());
                ps.setString(34, extraInformationBean.getPEI_ATTRIBUTE18());
                ps.setString(35, extraInformationBean.getPEI_ATTRIBUTE19());
                ps.setString(36, extraInformationBean.getPEI_ATTRIBUTE20());
                ps.setString(37, extraInformationBean.getPEI_ATTRIBUTE21());
                ps.setString(38, extraInformationBean.getPEI_ATTRIBUTE22());
                ps.setString(39, extraInformationBean.getPEI_ATTRIBUTE23());
                ps.setString(40, extraInformationBean.getPEI_ATTRIBUTE24());
                ps.setString(41, extraInformationBean.getPEI_ATTRIBUTE25());
                ps.setString(42, extraInformationBean.getPEI_ATTRIBUTE26());
                ps.setString(43, extraInformationBean.getPEI_ATTRIBUTE27());
                ps.setString(44, extraInformationBean.getPEI_ATTRIBUTE28());
                ps.setString(45, extraInformationBean.getPEI_ATTRIBUTE29());
                ps.setString(46, extraInformationBean.getPEI_ATTRIBUTE30());
                //
                ps.setString(47,
                             extraInformationBean.getPEI_INFORMATION_CATEGORY());
                //Pei_INFORMATION
                ps.setString(48, extraInformationBean.getPEI_INFORMATION1());
                ps.setString(49, extraInformationBean.getPEI_INFORMATION2());
                ps.setString(50, extraInformationBean.getPEI_INFORMATION3());
                ps.setString(51, extraInformationBean.getPEI_INFORMATION4());
                ps.setString(52, extraInformationBean.getPEI_INFORMATION5());
                ps.setString(53, extraInformationBean.getPEI_INFORMATION6());
                ps.setString(54, extraInformationBean.getPEI_INFORMATION7());
                ps.setString(55, extraInformationBean.getPEI_INFORMATION8());
                ps.setString(56, extraInformationBean.getPEI_INFORMATION9());
                ps.setString(57, extraInformationBean.getPEI_INFORMATION10());
                ps.setString(58, extraInformationBean.getPEI_INFORMATION11());
                ps.setString(59, extraInformationBean.getPEI_INFORMATION12());
                ps.setString(60, extraInformationBean.getPEI_INFORMATION13());
                ps.setString(61, extraInformationBean.getPEI_INFORMATION14());
                ps.setString(62, extraInformationBean.getPEI_INFORMATION15());
                ps.setString(63, extraInformationBean.getPEI_INFORMATION16());
                ps.setString(64, extraInformationBean.getPEI_INFORMATION17());
                ps.setString(65, extraInformationBean.getPEI_INFORMATION18());
                ps.setString(66, extraInformationBean.getPEI_INFORMATION19());
                ps.setString(67, extraInformationBean.getPEI_INFORMATION20());
                ps.setString(68, extraInformationBean.getPEI_INFORMATION21());
                ps.setString(69, extraInformationBean.getPEI_INFORMATION22());
                ps.setString(70, extraInformationBean.getPEI_INFORMATION23());
                ps.setString(71, extraInformationBean.getPEI_INFORMATION24());
                ps.setString(72, extraInformationBean.getPEI_INFORMATION25());
                ps.setString(73, extraInformationBean.getPEI_INFORMATION26());
                ps.setString(74, extraInformationBean.getPEI_INFORMATION27());
                ps.setString(75, extraInformationBean.getPEI_INFORMATION28());
                ps.setString(76, extraInformationBean.getPEI_INFORMATION29());
                ps.setString(77, extraInformationBean.getPEI_INFORMATION30());
                //Pei_INFORMATION_NUMBER
                ps.setString(78,
                             extraInformationBean.getPEI_INFORMATION_NUMBER1());
                ps.setString(79,
                             extraInformationBean.getPEI_INFORMATION_NUMBER2());
                ps.setString(80,
                             extraInformationBean.getPEI_INFORMATION_NUMBER3());
                ps.setString(81,
                             extraInformationBean.getPEI_INFORMATION_NUMBER4());
                ps.setString(82,
                             extraInformationBean.getPEI_INFORMATION_NUMBER5());
                ps.setString(83,
                             extraInformationBean.getPEI_INFORMATION_NUMBER6());
                ps.setString(84,
                             extraInformationBean.getPEI_INFORMATION_NUMBER7());
                ps.setString(85,
                             extraInformationBean.getPEI_INFORMATION_NUMBER8());
                ps.setString(86,
                             extraInformationBean.getPEI_INFORMATION_NUMBER9());
                ps.setString(87,
                             extraInformationBean.getPEI_INFORMATION_NUMBER10());
                ps.setString(88,
                             extraInformationBean.getPEI_INFORMATION_NUMBER11());
                ps.setString(89,
                             extraInformationBean.getPEI_INFORMATION_NUMBER12());
                ps.setString(90,
                             extraInformationBean.getPEI_INFORMATION_NUMBER13());
                ps.setString(91,
                             extraInformationBean.getPEI_INFORMATION_NUMBER14());
                ps.setString(92,
                             extraInformationBean.getPEI_INFORMATION_NUMBER15());
                ps.setString(93,
                             extraInformationBean.getPEI_INFORMATION_NUMBER16());
                ps.setString(94,
                             extraInformationBean.getPEI_INFORMATION_NUMBER17());
                ps.setString(95,
                             extraInformationBean.getPEI_INFORMATION_NUMBER18());
                ps.setString(96,
                             extraInformationBean.getPEI_INFORMATION_NUMBER19());
                ps.setString(97,
                             extraInformationBean.getPEI_INFORMATION_NUMBER20());
                //Pei_INFORMATION_DATE
                ps.setString(98,
                             extraInformationBean.getPEI_INFORMATION_DATE1());
                ps.setString(99,
                             extraInformationBean.getPEI_INFORMATION_DATE2());
                ps.setString(100,
                             extraInformationBean.getPEI_INFORMATION_DATE3());
                ps.setString(101,
                             extraInformationBean.getPEI_INFORMATION_DATE4());
                ps.setString(102,
                             extraInformationBean.getPEI_INFORMATION_DATE5());
                ps.setString(103,
                             extraInformationBean.getPEI_INFORMATION_DATE6());
                ps.setString(104,
                             extraInformationBean.getPEI_INFORMATION_DATE7());
                ps.setString(105,
                             extraInformationBean.getPEI_INFORMATION_DATE8());
                ps.setString(106,
                             extraInformationBean.getPEI_INFORMATION_DATE9());
                ps.setString(107,
                             extraInformationBean.getPEI_INFORMATION_DATE10());
                ps.setString(108,
                             extraInformationBean.getPEI_INFORMATION_DATE11());
                ps.setString(109,
                             extraInformationBean.getPEI_INFORMATION_DATE12());
                ps.setString(110,
                             extraInformationBean.getPEI_INFORMATION_DATE13());
                ps.setString(111,
                             extraInformationBean.getPEI_INFORMATION_DATE14());
                ps.setString(112,
                             extraInformationBean.getPEI_INFORMATION_DATE15());

                ps.setString(113,
                             extraInformationBean.getOBJECT_VERSION_NUMBER());
                ps.setString(114, extraInformationBean.getLAST_UPDATE_LOGIN());
                ps.setString(115, extraInformationBean.getEitLbl());
                ps.setString(116, extraInformationBean.getPersonName());
                ps.setInt(117, extraInformationBean.getId());
                ps.executeUpdate();

                processApproval(extraInformationBean, conditionCode);
                //                approvalSetupList =
                //                        approvalSetup.getApprovalByEITCodeAndConditionCode(extraInformationBean.getCode(),
                //                                                                           conditionCode);
                //                alBean.setStepLeval("0");
                //                alBean.setServiceType(extraInformationBean.getCode());
                //                alBean.setTransActionId(Integer.toString(extraInformationBean.getId()));
                //                alBean.setWorkflowId("");
                //                alBean.setRolrType("EMP");
                //                alBean.setRoleId(extraInformationBean.getPerson_id());
                //                alBean.setResponseCode("SUBMIT");
                //                alBean.setNotificationType("FYI");
                //
                //                approvalListDao.insertIntoApprovalList(alBean);
                //                alBean = null;
                //                ApprovalSetupBean ItratorBean;
                //                Iterator<ApprovalSetupBean> approvalSetupListIterator =
                //                    approvalSetupList.iterator();
                //                while (approvalSetupListIterator.hasNext()) {
                //                    alBean = new ApprovalListBean();
                //                    ItratorBean = approvalSetupListIterator.next();
                //
                //                    if (ItratorBean.getApprovalType().equals("JOB_LEVEL")) {
                //                        alBean.setServiceType(extraInformationBean.getCode());
                //                        alBean.setTransActionId(Integer.toString(extraInformationBean.getId()));
                //                        alBean.setWorkflowId("");
                //                        alBean.setRolrType(ItratorBean.getApprovalType());
                //                        alBean.setNotificationType(ItratorBean.getNotificationType());
                //                        /**************** call job level report *******************/
                //                        JSONObject dataDS = null;
                //                        JSONArray g1 = null;
                //                        BIReportDAO BIReportDAO = new BIReportDAO();
                //                        JSONObject obj = new JSONObject();
                //                        obj.put("reportName", "GET_JOB_LEVEL_REPORT");
                //                        obj.put("P_PERSON_NUMBER",
                //                                extraInformationBean.getPerson_number());
                //                        JSONObject jsonObj =
                //                            new JSONObject(BIReportDAO.getBiReport(obj));
                //                        if (!jsonObj.getJSONObject("DATA_DS").isNull("G_1")) {
                //                            dataDS = jsonObj.getJSONObject("DATA_DS");
                //                            g1 = dataDS.getJSONArray("G_1");
                //                        }
                //
                //                        for (int i = 0; i < g1.length(); i++) {
                //                            JSONObject data = (JSONObject)g1.get(i);
                //                            String stpLevel =
                //                                approvalListDao.getMaxStepLeval(String.valueOf(extraInformationBean.getId()),
                //                                                                String.valueOf(extraInformationBean.getCode()));
                //                            if (ItratorBean.getOperation().equals(">=")) {
                //                                if (Integer.parseInt(data.get("APPROVAL_AUTHORITY").toString()) >=
                //                                    Integer.parseInt(ItratorBean.getManagerLeval())) {
                //                                    alBean.setRoleId(data.get("MANAGER_ID").toString());
                //                                    alBean.setStepLeval(stpLevel.equalsIgnoreCase("null") ?
                //                                                        data.get("APPROVAL_LEVEL").toString() :
                //                                                        stpLevel);
                //                                    alBean.setLineManagerName(data.get("FULL_NAME").toString());
                //                                }
                //                            } else if (ItratorBean.getOperation().equals(">")) {
                //                                if (Integer.parseInt(data.get("APPROVAL_AUTHORITY").toString()) >
                //                                    Integer.parseInt(ItratorBean.getManagerLeval())) {
                //                                    alBean.setRoleId(data.get("MANAGER_ID").toString());
                //                                    alBean.setStepLeval(stpLevel.equalsIgnoreCase("null") ?
                //                                                        data.get("APPROVAL_LEVEL").toString() :
                //                                                        stpLevel);
                //                                    alBean.setLineManagerName(data.get("FULL_NAME").toString());
                //                                }
                //                            } else if (ItratorBean.getOperation().equals("<")) {
                //                                if (Integer.parseInt(data.get("APPROVAL_AUTHORITY").toString()) <
                //                                    Integer.parseInt(ItratorBean.getManagerLeval())) {
                //                                    alBean.setRoleId(data.get("MANAGER_ID").toString());
                //                                    alBean.setStepLeval(stpLevel.equalsIgnoreCase("null") ?
                //                                                        data.get("APPROVAL_LEVEL").toString() :
                //                                                        stpLevel);
                //                                    alBean.setLineManagerName(data.get("FULL_NAME").toString());
                //                                }
                //                            } else if (ItratorBean.getOperation().equals("<=")) {
                //                                if (Integer.parseInt(data.get("APPROVAL_AUTHORITY").toString()) <=
                //                                    Integer.parseInt(ItratorBean.getManagerLeval())) {
                //                                    alBean.setRoleId(data.get("MANAGER_ID").toString());
                //                                    alBean.setStepLeval(stpLevel.equalsIgnoreCase("null") ?
                //                                                        data.get("APPROVAL_LEVEL").toString() :
                //                                                        stpLevel);
                //                                    alBean.setLineManagerName(data.get("FULL_NAME").toString());
                //                                }
                //                            } else if (ItratorBean.getOperation().equals("==") ||
                //                                       ItratorBean.getOperation().equals("===")) {
                //                                if (Integer.parseInt(data.get("APPROVAL_AUTHORITY").toString()) ==
                //                                    Integer.parseInt(ItratorBean.getManagerLeval())) {
                //                                    alBean.setRoleId(data.get("MANAGER_ID").toString());
                //                                    alBean.setStepLeval(stpLevel.equalsIgnoreCase("null") ?
                //                                                        data.get("APPROVAL_LEVEL").toString() :
                //                                                        stpLevel);
                //                                    alBean.setLineManagerName(data.get("FULL_NAME").toString());
                //                                }
                //                            } else if (ItratorBean.getOperation().equals("!=")) {
                //                                if (Integer.parseInt(data.get("APPROVAL_AUTHORITY").toString()) !=
                //                                    Integer.parseInt(ItratorBean.getManagerLeval())) {
                //                                    alBean.setRoleId(data.get("MANAGER_ID").toString());
                //                                    alBean.setStepLeval(stpLevel.equalsIgnoreCase("null") ?
                //                                                        data.get("APPROVAL_LEVEL").toString() :
                //                                                        stpLevel);
                //                                    alBean.setLineManagerName(data.get("FULL_NAME").toString());
                //                                }
                //                            }
                //                            approvalListDao.insertIntoApprovalList(alBean);
                //                        }
                //                    } else {
                //                        String stpLevel =
                //                            approvalListDao.getMaxStepLeval(String.valueOf(extraInformationBean.getId()),
                //                                                            String.valueOf(extraInformationBean.getCode()));
                //                        alBean.setStepLeval(stpLevel.equalsIgnoreCase("null") ?
                //                                            ItratorBean.getApprovalOrder() :
                //                                            stpLevel);
                //                        alBean.setServiceType(extraInformationBean.getCode());
                //                        alBean.setTransActionId(Integer.toString(extraInformationBean.getId()));
                //                        alBean.setWorkflowId("");
                //                        alBean.setRolrType(ItratorBean.getApprovalType());
                //                        if (!alBean.getRolrType().equals("POSITION")) {
                //                            if (alBean.getRolrType().equals("EMP")) {
                //                                alBean.setRoleId(extraInformationBean.getPerson_id());
                //                            } else if (alBean.getRolrType().equals("LINE_MANAGER") &&
                //                                       !extraInformationBean.getLine_manager().isEmpty()) {
                //                                alBean.setRoleId(extraInformationBean.getLine_manager());
                //                            } else if (alBean.getRolrType().equals("LINE_MANAGER+1") &&
                //                                       !extraInformationBean.getManage_of_manager().isEmpty()) {
                //                                alBean.setRoleId(extraInformationBean.getManage_of_manager());
                //                            } else if (alBean.getRolrType().equals("AOR")) {
                //                                /**************** get AOR report  *******************/
                //                                                   extraInformationBean.getPerson_number());
                //                                BIReportDAO bIReportDAO = new BIReportDAO();
                //                                JSONObject obj = new JSONObject();
                //                                obj.put("reportName", "GET_AOR_REP");
                //                                obj.put("P_PERSON_NUMBER",
                //                                        extraInformationBean.getPerson_number());
                //
                //                                JSONObject data =
                //                                    new JSONObject(bIReportDAO.getBiReport(obj));
                //                                if (!data.getJSONObject("DATA_DS").isNull("G_1")) {
                //                                    JSONObject dataDS =
                //                                        data.getJSONObject("DATA_DS");
                //                                    JSONObject g1 =
                //                                        dataDS.getJSONObject("G_1");
                //                                    alBean.setRoleId(g1.get("PERSON_ID").toString());
                //                                    alBean.setLineManagerName(g1.get("FULL_NAME").toString());
                //                                }
                //                            }
                //
                //                        } else if (alBean.getRolrType().equals("POSITION")) {
                //                            alBean.setRoleId(ItratorBean.getRoleName());
                //                        }
                //                        alBean.setNotificationType(ItratorBean.getNotificationType());
                //                        approvalListDao.insertIntoApprovalList(alBean);
                //                    }
                //                    alBean = null;
                //                }
                //                if (!approvalSetupList.isEmpty()) {
                //                    wfn.setMsgTitle(extraInformationBean.getCode() +
                //                                    "Request");
                //                    wfn.setMsgBody(extraInformationBean.getCode() + ":" +
                //                                   extraInformationBean.getPerson_id());
                //                    ApprovalListBean rolesBean =
                //                        approvalListDao.getRoles(Integer.toString(extraInformationBean.getId()),
                //                                                 extraInformationBean.getCode());
                //                    wfn.setReceiverType(rolesBean.getRolrType());
                //                    wfn.setReceiverId(rolesBean.getRoleId());
                //                    wfn.setType("FYA");
                //                    wfn.setRequestId(Integer.toString(extraInformationBean.getId()));
                //                    wfn.setStatus("OPEN");
                //                    wfn.setSelfType(extraInformationBean.getCode());
                //                    workflowNotificationDao.insertIntoWorkflow(wfn);
                //
                //                } else {
                //                    efd = new EFFDetails();
                //                    efd.postEFF(extraInformationBean);
                //                    this.updateStatus("APPROVED", "",
                //                                      extraInformationBean.getId());
                //
                //                    selfServiceDao.updateSelfService("APPROVED",
                //                                                     sfBean.getType(),
                //                                                     sfBean.getTransactionId());
                //                }


            } else if (transactionType.equals("BULK")) {
                extraInformationBean.setId(getMaxId(connection));
                query =
                        "insert into   " + " " + getSchema_Name() + ".XXX_PER_PEOPLE_EXTRA_INFO(\n" +
                        "ID,EIT_CODE,EIT,STATUS,PERSON_NUMBER,PERSON_ID,CREATED_BY,CREATION_DATE,UPDATED_BY,UPDATED_DATE,LINE_MANAGER\n" +
                        ",MANAGER_OF_MANAGER,URL,EIT_NAME,PERSON_EXTRA_INFO_ID,EFFECTIVE_START_DATE,EFFECTIVE_END_DATE,INFORMATION_TYPE,PEI_ATTRIBUTE_CATEGORY,PEI_ATTRIBUTE1,PEI_ATTRIBUTE2\n" +
                        ",PEI_ATTRIBUTE3,PEI_ATTRIBUTE4,PEI_ATTRIBUTE5,PEI_ATTRIBUTE6,PEI_ATTRIBUTE7,PEI_ATTRIBUTE8,PEI_ATTRIBUTE9,PEI_ATTRIBUTE10,PEI_ATTRIBUTE11,PEI_ATTRIBUTE12\n" +
                        ",PEI_ATTRIBUTE13,PEI_ATTRIBUTE14,PEI_ATTRIBUTE15,PEI_ATTRIBUTE16,PEI_ATTRIBUTE17,PEI_ATTRIBUTE18,PEI_ATTRIBUTE19,PEI_ATTRIBUTE20,PEI_ATTRIBUTE21,PEI_ATTRIBUTE22\n" +
                        ",PEI_ATTRIBUTE23,PEI_ATTRIBUTE24,PEI_ATTRIBUTE25,PEI_ATTRIBUTE26,PEI_ATTRIBUTE27,PEI_ATTRIBUTE28,PEI_ATTRIBUTE29,PEI_ATTRIBUTE30,PEI_INFORMATION_CATEGORY,PEI_INFORMATION1\n" +
                        ",PEI_INFORMATION2,PEI_INFORMATION3,PEI_INFORMATION4,PEI_INFORMATION5,PEI_INFORMATION6,PEI_INFORMATION7,PEI_INFORMATION8,PEI_INFORMATION9,PEI_INFORMATION10,PEI_INFORMATION11\n" +
                        ",PEI_INFORMATION12,PEI_INFORMATION13,PEI_INFORMATION14,PEI_INFORMATION15,PEI_INFORMATION16,PEI_INFORMATION17,PEI_INFORMATION18,PEI_INFORMATION19,PEI_INFORMATION20,PEI_INFORMATION21\n" +
                        ",PEI_INFORMATION22,PEI_INFORMATION23,PEI_INFORMATION24,PEI_INFORMATION25,PEI_INFORMATION26,PEI_INFORMATION27,PEI_INFORMATION28,PEI_INFORMATION29,PEI_INFORMATION30,PEI_INFORMATION_NUMBER1,PEI_INFORMATION_NUMBER2,PEI_INFORMATION_NUMBER3,PEI_INFORMATION_NUMBER4,PEI_INFORMATION_NUMBER5,PEI_INFORMATION_NUMBER6,PEI_INFORMATION_NUMBER7,PEI_INFORMATION_NUMBER8\n" +
                        ",PEI_INFORMATION_NUMBER9,PEI_INFORMATION_NUMBER10,PEI_INFORMATION_NUMBER11,PEI_INFORMATION_NUMBER12,PEI_INFORMATION_NUMBER13,PEI_INFORMATION_NUMBER14,PEI_INFORMATION_NUMBER15\n" +
                        ",PEI_INFORMATION_NUMBER16,PEI_INFORMATION_NUMBER17,PEI_INFORMATION_NUMBER18,PEI_INFORMATION_NUMBER19,PEI_INFORMATION_NUMBER20,PEI_INFORMATION_DATE1,PEI_INFORMATION_DATE2\n" +
                        ",PEI_INFORMATION_DATE3,PEI_INFORMATION_DATE4,PEI_INFORMATION_DATE5,PEI_INFORMATION_DATE6,PEI_INFORMATION_DATE7,PEI_INFORMATION_DATE8,PEI_INFORMATION_DATE9,PEI_INFORMATION_DATE10\n" +
                        ",PEI_INFORMATION_DATE11,PEI_INFORMATION_DATE12,PEI_INFORMATION_DATE13,PEI_INFORMATION_DATE14,PEI_INFORMATION_DATE15,OBJECT_VERSION_NUMBER,LAST_UPDATE_LOGIN,EITLBL,PERSON_NAME)\n" +
                        "VALUES(\n" +
                        "?,?,?,?,?,?,?,sysdate,?,?,?,?,?,?,?,?,?,?,?,?,?\n" +
                        ",?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?\n" +
                        ",?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?\n" +
                        ",?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?\n" +
                        ",?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?\n" +
                        ",?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                // String X = extraInformationBean.getEit().toString();

                ps = connection.prepareStatement(query);
                ps.setInt(1, extraInformationBean.getId());
                ps.setString(2, extraInformationBean.getCode());
                ps.setString(3, extraInformationBean.getEit());
                ps.setString(4, extraInformationBean.getStatus());
                ps.setString(5, extraInformationBean.getPerson_number());
                ps.setString(6, extraInformationBean.getPerson_id());
                ps.setString(7, extraInformationBean.getCreated_by());
                //ps.setString(8, extraInformationBean.getCreation_date());
                ps.setString(8, extraInformationBean.getUpdated_by());
                ps.setString(9, extraInformationBean.getUpdated_date());
                ps.setString(10, extraInformationBean.getLine_manager());
                ps.setString(11, extraInformationBean.getManage_of_manager());
                ps.setString(12, extraInformationBean.getUrl());
                ps.setString(13, extraInformationBean.getEit_name());
                //New
                ps.setString(14,
                             extraInformationBean.getPERSON_EXTRA_INFO_ID());
                ps.setString(15,
                             extraInformationBean.getEFFECTIVE_START_DATE());
                ps.setString(16, extraInformationBean.getEFFECTIVE_END_DATE());
                ps.setString(17, extraInformationBean.getINFORMATION_TYPE());
                ps.setString(18,
                             extraInformationBean.getPEI_ATTRIBUTE_CATEGORY());
                //PEI_Attribute
                ps.setString(19, extraInformationBean.getPEI_ATTRIBUTE1());
                ps.setString(20, extraInformationBean.getPEI_ATTRIBUTE2());
                ps.setString(21, extraInformationBean.getPEI_ATTRIBUTE3());
                ps.setString(22, extraInformationBean.getPEI_ATTRIBUTE4());
                ps.setString(23, extraInformationBean.getPEI_ATTRIBUTE5());
                ps.setString(24, extraInformationBean.getPEI_ATTRIBUTE6());
                ps.setString(25, extraInformationBean.getPEI_ATTRIBUTE7());
                ps.setString(26, extraInformationBean.getPEI_ATTRIBUTE8());
                ps.setString(27, extraInformationBean.getPEI_ATTRIBUTE9());
                ps.setString(28, extraInformationBean.getPEI_ATTRIBUTE10());
                ps.setString(29, extraInformationBean.getPEI_ATTRIBUTE11());
                ps.setString(30, extraInformationBean.getPEI_ATTRIBUTE12());
                ps.setString(31, extraInformationBean.getPEI_ATTRIBUTE13());
                ps.setString(32, extraInformationBean.getPEI_ATTRIBUTE14());
                ps.setString(33, extraInformationBean.getPEI_ATTRIBUTE15());
                ps.setString(34, extraInformationBean.getPEI_ATTRIBUTE16());
                ps.setString(35, extraInformationBean.getPEI_ATTRIBUTE17());
                ps.setString(36, extraInformationBean.getPEI_ATTRIBUTE18());
                ps.setString(37, extraInformationBean.getPEI_ATTRIBUTE19());
                ps.setString(38, extraInformationBean.getPEI_ATTRIBUTE20());
                ps.setString(39, extraInformationBean.getPEI_ATTRIBUTE21());
                ps.setString(40, extraInformationBean.getPEI_ATTRIBUTE22());
                ps.setString(41, extraInformationBean.getPEI_ATTRIBUTE23());
                ps.setString(42, extraInformationBean.getPEI_ATTRIBUTE24());
                ps.setString(43, extraInformationBean.getPEI_ATTRIBUTE25());
                ps.setString(44, extraInformationBean.getPEI_ATTRIBUTE26());
                ps.setString(45, extraInformationBean.getPEI_ATTRIBUTE27());
                ps.setString(46, extraInformationBean.getPEI_ATTRIBUTE28());
                ps.setString(47, extraInformationBean.getPEI_ATTRIBUTE29());
                ps.setString(48, extraInformationBean.getPEI_ATTRIBUTE30());
                //
                ps.setString(49,
                             extraInformationBean.getPEI_INFORMATION_CATEGORY());
                //Pei_INFORMATION
                ps.setString(50, extraInformationBean.getPEI_INFORMATION1());
                ps.setString(51, extraInformationBean.getPEI_INFORMATION2());
                ps.setString(52, extraInformationBean.getPEI_INFORMATION3());
                ps.setString(53, extraInformationBean.getPEI_INFORMATION4());
                ps.setString(54, extraInformationBean.getPEI_INFORMATION5());
                ps.setString(55, extraInformationBean.getPEI_INFORMATION6());
                ps.setString(56, extraInformationBean.getPEI_INFORMATION7());
                ps.setString(57, extraInformationBean.getPEI_INFORMATION8());
                ps.setString(58, extraInformationBean.getPEI_INFORMATION9());
                ps.setString(59, extraInformationBean.getPEI_INFORMATION10());
                ps.setString(60, extraInformationBean.getPEI_INFORMATION11());
                ps.setString(61, extraInformationBean.getPEI_INFORMATION12());
                ps.setString(62, extraInformationBean.getPEI_INFORMATION13());
                ps.setString(63, extraInformationBean.getPEI_INFORMATION14());
                ps.setString(64, extraInformationBean.getPEI_INFORMATION15());
                ps.setString(65, extraInformationBean.getPEI_INFORMATION16());
                ps.setString(66, extraInformationBean.getPEI_INFORMATION17());
                ps.setString(67, extraInformationBean.getPEI_INFORMATION18());
                ps.setString(68, extraInformationBean.getPEI_INFORMATION19());
                ps.setString(69, extraInformationBean.getPEI_INFORMATION20());
                ps.setString(70, extraInformationBean.getPEI_INFORMATION21());
                ps.setString(71, extraInformationBean.getPEI_INFORMATION22());
                ps.setString(72, extraInformationBean.getPEI_INFORMATION23());
                ps.setString(73, extraInformationBean.getPEI_INFORMATION24());
                ps.setString(74, extraInformationBean.getPEI_INFORMATION25());
                ps.setString(75, extraInformationBean.getPEI_INFORMATION26());
                ps.setString(76, extraInformationBean.getPEI_INFORMATION27());
                ps.setString(77, extraInformationBean.getPEI_INFORMATION28());
                ps.setString(78, extraInformationBean.getPEI_INFORMATION29());
                ps.setString(79, extraInformationBean.getPEI_INFORMATION30());
                //Pei_INFORMATION_NUMBER
                ps.setString(80,
                             extraInformationBean.getPEI_INFORMATION_NUMBER1());
                ps.setString(81,
                             extraInformationBean.getPEI_INFORMATION_NUMBER2());
                ps.setString(82,
                             extraInformationBean.getPEI_INFORMATION_NUMBER3());
                ps.setString(83,
                             extraInformationBean.getPEI_INFORMATION_NUMBER4());
                ps.setString(84,
                             extraInformationBean.getPEI_INFORMATION_NUMBER5());
                ps.setString(85,
                             extraInformationBean.getPEI_INFORMATION_NUMBER6());
                ps.setString(86,
                             extraInformationBean.getPEI_INFORMATION_NUMBER7());
                ps.setString(87,
                             extraInformationBean.getPEI_INFORMATION_NUMBER8());
                ps.setString(88,
                             extraInformationBean.getPEI_INFORMATION_NUMBER9());
                ps.setString(89,
                             extraInformationBean.getPEI_INFORMATION_NUMBER10());
                ps.setString(90,
                             extraInformationBean.getPEI_INFORMATION_NUMBER11());
                ps.setString(91,
                             extraInformationBean.getPEI_INFORMATION_NUMBER12());
                ps.setString(92,
                             extraInformationBean.getPEI_INFORMATION_NUMBER13());
                ps.setString(93,
                             extraInformationBean.getPEI_INFORMATION_NUMBER14());
                ps.setString(94,
                             extraInformationBean.getPEI_INFORMATION_NUMBER15());
                ps.setString(95,
                             extraInformationBean.getPEI_INFORMATION_NUMBER16());
                ps.setString(96,
                             extraInformationBean.getPEI_INFORMATION_NUMBER17());
                ps.setString(97,
                             extraInformationBean.getPEI_INFORMATION_NUMBER18());
                ps.setString(98,
                             extraInformationBean.getPEI_INFORMATION_NUMBER19());
                ps.setString(99,
                             extraInformationBean.getPEI_INFORMATION_NUMBER20());
                //PEI_INFORMATION_DATE
                ps.setString(100,
                             extraInformationBean.getPEI_INFORMATION_DATE1());
                ps.setString(101,
                             extraInformationBean.getPEI_INFORMATION_DATE2());
                ps.setString(102,
                             extraInformationBean.getPEI_INFORMATION_DATE3());
                ps.setString(103,
                             extraInformationBean.getPEI_INFORMATION_DATE4());
                ps.setString(104,
                             extraInformationBean.getPEI_INFORMATION_DATE5());
                ps.setString(105,
                             extraInformationBean.getPEI_INFORMATION_DATE6());
                ps.setString(106,
                             extraInformationBean.getPEI_INFORMATION_DATE7());
                ps.setString(107,
                             extraInformationBean.getPEI_INFORMATION_DATE8());
                ps.setString(108,
                             extraInformationBean.getPEI_INFORMATION_DATE9());
                ps.setString(109,
                             extraInformationBean.getPEI_INFORMATION_DATE10());
                ps.setString(110,
                             extraInformationBean.getPEI_INFORMATION_DATE11());
                ps.setString(111,
                             extraInformationBean.getPEI_INFORMATION_DATE12());
                ps.setString(112,
                             extraInformationBean.getPEI_INFORMATION_DATE13());
                ps.setString(113,
                             extraInformationBean.getPEI_INFORMATION_DATE14());
                ps.setString(114,
                             extraInformationBean.getPEI_INFORMATION_DATE15());

                ps.setString(115,
                             extraInformationBean.getOBJECT_VERSION_NUMBER());
                ps.setString(116, extraInformationBean.getLAST_UPDATE_LOGIN());
                ps.setString(117, extraInformationBean.getEitLbl());
                ps.setString(118, extraInformationBean.getPersonName());
                int status = ps.executeUpdate();
                EFFDetails.getInstance().postEFF(extraInformationBean);
                this.updateStatus("APPROVED", "",
                                  extraInformationBean.getId());
            }

        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            throw new AppsProException(e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return extraInformationBean;
    }
    //******************End Insert Or Update PeopleExtraInfo********************

    //******************Start Update Status*************************************

    public void updateStatus(String RESPONSE_CODE, String rejectReason,
                             int ID) throws AppsProException{
        try {
            connection = AppsproConnection.getConnection();
            String query = null;

            query =
                    "UPDATE   " + " " + getSchema_Name() + ".XXX_PER_PEOPLE_EXTRA_INFO  SET STATUS = ? ,REJECT_REASON=? WHERE ID = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, RESPONSE_CODE);
            ps.setString(2, rejectReason);
            ps.setInt(3, ID);
            ps.executeUpdate();
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            throw new AppsProException(e);

        } finally {
            closeResources(connection, ps, rs);
        }
    }
    //******************End Update Status***************************************

    //******************Start Get Max ID****************************************

    public synchronized int getMaxId(Connection connection) throws AppsProException {
        int id = 0;
        try {

            //  connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "SELECT  " + " " + getSchema_Name() + ".XXX_PER_PEOPLE_EXTRA_INFO_SEQ.NEXTVAL AS NEXT_ID FROM dual";
            AppsproConnection.LOG.info(query);
            ps = connection.prepareStatement(query);
            // ps.executeUpdate();
            rs = ps.executeQuery();
            while (rs.next()) {
                id = Integer.parseInt(rs.getString("NEXT_ID"));
            }
        } catch (Exception e) {
            //("Error: ");

           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            throw new AppsProException(e);
        } finally {
            // closeResources(connection, ps, rs);
        }
        return id;
    }
    //****************** End Get Max ID ****************************************

    //******************Start Get People Extra Info By Id***********************

    public PeopleExtraInformationBean getEITById(String Id) throws AppsProException{
        PeopleExtraInformationBean bean = new PeopleExtraInformationBean();
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "select * from  " + " " + getSchema_Name() + ".XXX_PER_PEOPLE_EXTRA_INFO where ID=?";
            ps = connection.prepareStatement(query);
            ps.setLong(1, Long.parseLong(Id));
            rs = ps.executeQuery();
            while (rs.next()) {
                //bean.setId(rs.getString("NEXTVAL"));
                bean.setId(Integer.parseInt(rs.getString("ID")));
                bean.setCode(rs.getString("EIT_CODE"));
                bean.setEit(rs.getString("EIT"));
                bean.setStatus(rs.getString("STATUS"));
                bean.setPerson_number(rs.getString("PERSON_NUMBER"));
                bean.setPerson_id(rs.getString("PERSON_ID"));
                bean.setCreated_by(rs.getString("CREATED_BY"));
                bean.setCreation_date(rs.getString("CREATION_DATE"));
                bean.setUpdated_by(rs.getString("UPDATED_BY"));
                bean.setUpdated_date(rs.getString("UPDATED_DATE"));
                bean.setLine_manager(rs.getString("LINE_MANAGER"));
                bean.setManage_of_manager(rs.getString("MANAGER_OF_MANAGER"));
                bean.setUrl(rs.getString("URL"));
                bean.setPerson_number(rs.getString("PERSON_NUMBER"));
            }
        } catch (Exception e) {
           
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            throw new AppsProException(e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }
    //******************End Get People Extra Info By Id*************************

    //******************Start Get People Extra Info By Person Id****************

    public ArrayList<PeopleExtraInformationBean> getEITsByPersonId(String eitCode,
                                                                   String personId) throws AppsProException {
        ArrayList<PeopleExtraInformationBean> eitsBeanList =
            new ArrayList<PeopleExtraInformationBean>();
        PeopleExtraInformationBean bean = null;
        connection = AppsproConnection.getConnection();

        String query = null;

        try {
            connection = AppsproConnection.getConnection();
            query = null;
            query =
                    "select * from  " + " " + getSchema_Name() + ".XXX_PER_PEOPLE_EXTRA_INFO where EIT_CODE = ? AND PERSON_ID = ? ORDER BY ID DESC ";

            ps = connection.prepareStatement(query);
            ps.setString(1, eitCode);
            ps.setString(2, personId);
            rs = ps.executeQuery();

            while (rs.next()) {
                bean = new PeopleExtraInformationBean();
                bean.setId(Integer.parseInt(rs.getString("ID")));
                bean.setCode(rs.getString("EIT_CODE"));
                bean.setEit(rs.getString("EIT"));
                bean.setEitLbl(rs.getString("EITLBL"));
                bean.setStatus(rs.getString("STATUS"));
                bean.setPerson_number(rs.getString("PERSON_NUMBER"));
                bean.setPerson_id(rs.getString("PERSON_ID"));
                bean.setCreated_by(rs.getString("CREATED_BY"));
                bean.setCreation_date(rs.getString("CREATION_DATE"));
                bean.setUpdated_by(rs.getString("UPDATED_BY"));
                bean.setUpdated_date(rs.getString("UPDATED_DATE"));
                bean.setLine_manager(rs.getString("LINE_MANAGER"));
                bean.setManage_of_manager(rs.getString("MANAGER_OF_MANAGER"));
                bean.setUrl(rs.getString("URL"));
                eitsBeanList.add(bean);
            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            throw new AppsProException(e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return eitsBeanList;

    }
    //******************End Get People Extra Info By Person Id******************

    public String getDynamicQuery(String body) throws AppsProException {
        JSONObject jsonObj = null;
        ArrayList<JSONObject> list = new ArrayList<JSONObject>();
        try {

            connection = AppsproConnection.getConnection();
            String query = null;
            query = body;
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            int columnCount = rs.getMetaData().getColumnCount();
            ResultSetMetaData rsmd = rs.getMetaData();

            while (rs.next()) {
                jsonObj = new JSONObject();
                for (int i = 1; i <= columnCount; i++) {
                    jsonObj.put(rsmd.getColumnName(i), rs.getString(i));
                }
                list.add(jsonObj);

            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            throw new AppsProException(e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return list.toString();
    }

    public CountEITRequest getAllCountRequestByPersonId(String personId) throws AppsProException {
        CountEITRequest bean = null;
        connection = AppsproConnection.getConnection();

        String query =
            "SELECT COUNT(CASE WHEN STATUS IS NULL AND PERSON_ID = ? THEN ID END) countPendingApproved" +
            ",COUNT(CASE WHEN STATUS = 'APPROVED' AND PERSON_ID = ? THEN ID END) countApproved" +
            ",COUNT(CASE WHEN STATUS = 'REJECTED' AND PERSON_ID = ? THEN ID END) countRejected " +
            "FROM " + getSchema_Name() + ".XX_SELF_SERVICE";


        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, personId.toString());
            ps.setString(2, personId.toString());
            ps.setString(3, personId.toString());

            rs = ps.executeQuery();

            while (rs.next()) {
                bean = new CountEITRequest();
                bean.setCountPendingApproved(rs.getString("countPendingApproved"));
                bean.setCountApproved(rs.getString("countApproved"));
                bean.setCountRejected(rs.getString("countRejected"));
            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            throw new AppsProException(e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }


    public ArrayList<PeopleExtraInformationBean> getRejectReason(String id) throws AppsProException {
        String rejectReson = null;
        PeopleExtraInformationBean bean = null;
        connection = AppsproConnection.getConnection();
        ArrayList<PeopleExtraInformationBean> list =
            new ArrayList<PeopleExtraInformationBean>();
        String query =
            "select REJECT_REASON FROM " + getSchema_Name() + ".XX_APPROVAL_LIST where  TRANSACTION_ID=?";
        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, id);
            rs = ps.executeQuery();

            while (rs.next()) {
                bean = new PeopleExtraInformationBean();
                bean.setRejectReason(rs.getString("REJECT_REASON"));
                list.add(bean);
            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            throw new AppsProException(e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return list;

    }


    public static void main(String[] args) {
        PeopleExtraInformationDAO obj = new PeopleExtraInformationDAO();
        //(obj.getDynamicQuery("SELECT * FROM XXX_EITS"));

    }


    private void processApproval(PeopleExtraInformationBean extraInformationBean,
                                 String conditionCode) throws AppsProException,Exception {
        String s = extraInformationBean.getStatus();
        boolean isInsertApprovalList=false;
        if (!s.equals("DRAFT")) {
            approvalSetupList =
                    approvalSetup.getApprovalByEITCodeAndConditionCode(extraInformationBean.getCode(),
                                                                       conditionCode);
            alBean.setStepLeval("0");
            alBean.setServiceType(extraInformationBean.getCode());
            alBean.setTransActionId(Integer.toString(extraInformationBean.getId()));
            alBean.setWorkflowId("");
            alBean.setRolrType("EMP");
            alBean.setRoleId(extraInformationBean.getPerson_id());
            alBean.setPersonName(extraInformationBean.getPersonName());
            alBean.setResponseCode("SUBMIT");
            alBean.setNotificationType("FYI");

            approvalListDao.insertIntoApprovalList(alBean);
            // String emailBody= "You Have "+ extraInformationBean.getEit_name()+ " Notification For your kind Approval" ;
            String emailBody = "";
            EmployeeBean empObj = new EmployeeBean();
            String emailSupject = "";
            try {
                empObj =
                        employeeDetails.getEmpDetailsByPersonId(extraInformationBean.getPerson_id(),
                                                                "", "");
                emailBody =
                        rh.getEmailBody("FYI", empObj.getDisplayName(), extraInformationBean.getEit_name(),
                                        empObj.getDisplayName());
                emailSupject =
                        rh.getEmailSubject("FYI", extraInformationBean.getEit_name(),
                                           empObj.getDisplayName());
                mailObj.SEND_MAIL(empObj.getEmail(), emailSupject, emailBody);
            } catch (Exception e) {
                //("Error: ");
               e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
                throw new AppsProException(e);
            }

            alBean.setRoleId(extraInformationBean.getLine_manager());
            alBean = null;
            ApprovalSetupBean ItratorBean;
            Iterator<ApprovalSetupBean> approvalSetupListIterator =
                approvalSetupList.iterator();
            while (approvalSetupListIterator.hasNext()) {
                alBean = new ApprovalListBean();
                ItratorBean = approvalSetupListIterator.next();
                if (ItratorBean.getApprovalType().equals("JOB_LEVEL")) {
                    alBean.setServiceType(extraInformationBean.getCode());
                    alBean.setTransActionId(Integer.toString(extraInformationBean.getId()));
                    alBean.setWorkflowId("");
                    alBean.setRolrType(ItratorBean.getApprovalType());
                    alBean.setNotificationType(ItratorBean.getNotificationType());
                    /**************** call job level report *******************/
                    JSONObject dataDS = null;
                    JSONArray g1 = null;
                    BIReportDAO BIReportDAO = new BIReportDAO();
                    JSONObject obj = new JSONObject();
                    obj.put("reportName", "GET_JOB_LEVEL_REPORT");
                    obj.put("P_PERSON_NUMBER",
                            extraInformationBean.getPerson_number());
                    JSONObject jsonObj =
                        new JSONObject(BIReportDAO.getBiReport(obj));
                    if (!jsonObj.getJSONObject("DATA_DS").isNull("G_1")) {
                        dataDS = jsonObj.getJSONObject("DATA_DS");
                        g1 = dataDS.getJSONArray("G_1");
                    }

                    for (int i = 0; i < g1.length(); i++) {
                        JSONObject data = (JSONObject)g1.get(i);
                        String stpLevel =
                            approvalListDao.getMaxStepLeval(String.valueOf(extraInformationBean.getId()),
                                                            String.valueOf(extraInformationBean.getCode()));
                        if (ItratorBean.getOperation().equals(">=")) {
                            if (Integer.parseInt(data.get("APPROVAL_AUTHORITY").toString()) >=
                                Integer.parseInt(ItratorBean.getManagerLeval())) {
                                alBean.setRoleId(data.get("MANAGER_ID").toString());
                                alBean.setStepLeval(stpLevel.equalsIgnoreCase("null") ?
                                                    data.get("APPROVAL_LEVEL").toString() :
                                                    stpLevel);
                                alBean.setLineManagerName(data.get("FULL_NAME").toString());
                            }
                        } else if (ItratorBean.getOperation().equals(">")) {
                            if (Integer.parseInt(data.get("APPROVAL_AUTHORITY").toString()) >
                                Integer.parseInt(ItratorBean.getManagerLeval())) {
                                alBean.setRoleId(data.get("MANAGER_ID").toString());
                                alBean.setStepLeval(stpLevel.equalsIgnoreCase("null") ?
                                                    data.get("APPROVAL_LEVEL").toString() :
                                                    stpLevel);
                                alBean.setLineManagerName(data.get("FULL_NAME").toString());
                            }
                        } else if (ItratorBean.getOperation().equals("<")) {
                            if (Integer.parseInt(data.get("APPROVAL_AUTHORITY").toString()) <
                                Integer.parseInt(ItratorBean.getManagerLeval())) {
                                alBean.setRoleId(data.get("MANAGER_ID").toString());
                                alBean.setStepLeval(stpLevel.equalsIgnoreCase("null") ?
                                                    data.get("APPROVAL_LEVEL").toString() :
                                                    stpLevel);
                                alBean.setLineManagerName(data.get("FULL_NAME").toString());
                            }
                        } else if (ItratorBean.getOperation().equals("<=")) {
                            if (Integer.parseInt(data.get("APPROVAL_AUTHORITY").toString()) <=
                                Integer.parseInt(ItratorBean.getManagerLeval())) {
                                alBean.setRoleId(data.get("MANAGER_ID").toString());
                                alBean.setStepLeval(stpLevel.equalsIgnoreCase("null") ?
                                                    data.get("APPROVAL_LEVEL").toString() :
                                                    stpLevel);
                            }
                        } else if (ItratorBean.getOperation().equals("==") ||
                                   ItratorBean.getOperation().equals("===")) {
                            if (Integer.parseInt(data.get("APPROVAL_AUTHORITY").toString()) ==
                                Integer.parseInt(ItratorBean.getManagerLeval())) {
                                alBean.setRoleId(data.get("MANAGER_ID").toString());
                                alBean.setStepLeval(stpLevel.equalsIgnoreCase("null") ?
                                                    data.get("APPROVAL_LEVEL").toString() :
                                                    stpLevel);
                                alBean.setLineManagerName(data.get("FULL_NAME").toString());
                            }
                        } else if (ItratorBean.getOperation().equals("!=")) {
                            if (Integer.parseInt(data.get("APPROVAL_AUTHORITY").toString()) !=
                                Integer.parseInt(ItratorBean.getManagerLeval())) {
                                alBean.setRoleId(data.get("MANAGER_ID").toString());
                                alBean.setStepLeval(stpLevel.equalsIgnoreCase("null") ?
                                                    data.get("APPROVAL_LEVEL").toString() :
                                                    stpLevel);
                                alBean.setLineManagerName(data.get("FULL_NAME").toString());
                            }
                        }
                        approvalListDao.insertIntoApprovalList(alBean);
                    }
                } else {
                    System.out.println("ID  : "+ extraInformationBean.getId() );
                    System.out.println("Code  : "+ extraInformationBean.getCode() );
                    String stpLevel =
                        approvalListDao.getMaxStepLeval(String.valueOf(extraInformationBean.getId()),
                                                        String.valueOf(extraInformationBean.getCode()));
                    System.out.println("stpLevel : "+ stpLevel );
                    alBean.setStepLeval(stpLevel.equalsIgnoreCase("null") ?
                                        ItratorBean.getApprovalOrder() :
                                        stpLevel);
                    alBean.setServiceType(extraInformationBean.getCode());
                    alBean.setTransActionId(Integer.toString(extraInformationBean.getId()));
                    alBean.setWorkflowId("");
                    alBean.setRolrType(ItratorBean.getApprovalType());
                    ArrayList<DelegationBean> listArr =
                        new ArrayList<DelegationBean>();
                    DelegationDAO serviceDAO = new DelegationDAO();
                    listArr = serviceDAO.getAllSummary();
                    java.util.Date currentDate = new java.util.Date();
                    SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");


                    if (!alBean.getRolrType().equals("POSITION")) {
                        alBean.setRoleId("");
                        for (int i = 0; i < listArr.size(); i++) {
                            //                                    if(!listArr.get(i).getPersonId().equals(extraInformationBean.getPerson_id())){

                            if (listArr.get(i).getCreatedByPersonId().equals(extraInformationBean.getPerson_id()) &&
                                ft.format(currentDate).compareTo(listArr.get(i).getEffectiveEndDate()) <=
                                0) {
                                alBean.setRoleId(listArr.get(i).getPersonId());
                            } else if (listArr.get(i).getCreatedByPersonId().equals(extraInformationBean.getLine_manager()) &&
                                       ft.format(currentDate).compareTo(listArr.get(i).getEffectiveEndDate()) <=
                                       0) {
                                alBean.setRoleId(listArr.get(i).getPersonId());
                            } else if (listArr.get(i).getCreatedByPersonId().equals(extraInformationBean.getManage_of_manager()) &&
                                       ft.format(currentDate).compareTo(listArr.get(i).getEffectiveEndDate()) <=
                                       0) {
                                alBean.setRoleId(listArr.get(i).getPersonId());
                            }
                        }
                        //                                    }

                        if (alBean.getRoleId().isEmpty() ||
                            alBean.getRoleId().equals(null) ||
                            alBean.getRoleId() == "") {
                            if (alBean.getRolrType().equals("EMP")) {
                                alBean.setRoleId(extraInformationBean.getPerson_id());
                            } else if (alBean.getRolrType().equals("LINE_MANAGER") &&
                                 extraInformationBean.getLine_manager()!=null &&
                                       !extraInformationBean.getLine_manager().isEmpty()) {
                                alBean.setRoleId(extraInformationBean.getLine_manager());
                                alBean.setPersonName(extraInformationBean.getManagerName());

                            } else if (alBean.getRolrType().equals("LINE_MANAGER+1") &&
                                       extraInformationBean.getManage_of_manager()!= null
                                       &&!extraInformationBean.getManage_of_manager().isEmpty()) {
                                alBean.setRoleId(extraInformationBean.getManage_of_manager());
                                alBean.setPersonName(extraInformationBean.getManagerOfManagerName());
                                //  EmployeeBean obj = new EmployeeBean();


                            }
                        }


                        if (alBean.getRolrType().equals("Special_Case")) {

                            //Caall Dynamic Report Validation -- arg
                            BIReportDAO bIReportDAO = new BIReportDAO();
                            JSONObject obj = new JSONObject();
                            obj.put("reportName", "DynamicValidationReport");
                            obj.put("str", ItratorBean.getSpecialCase());

                            JSONObject jsonObj =
                                new JSONObject(bIReportDAO.getBiReport(obj));

                            alBean.setRoleId(jsonObj.get("value").toString());
                        } else if (alBean.getRolrType().equals("ROLES")) {
                            alBean.setRoleId(ItratorBean.getRoleName());
                            //("Role Name : ");
                            //(ItratorBean.getRoleName());
                        } else if (alBean.getRolrType().equals("AOR")) {
                            /**************** get AOR report  *******************/

                            BIReportDAO bIReportDAO = new BIReportDAO();
                            JSONObject obj = new JSONObject();
                            obj.put("reportName", "GET_AOR_REP");
                            obj.put("P_PERSON_NUMBER",
                                    extraInformationBean.getPerson_number());

                            JSONObject data =
                                new JSONObject(bIReportDAO.getBiReport(obj));
                            if (!data.getJSONObject("DATA_DS").isNull("G_1")) {
                                JSONObject dataDS =
                                    data.getJSONObject("DATA_DS");
                                JSONObject g1 = dataDS.getJSONObject("G_1");
                                alBean.setRoleId(g1.get("PERSON_ID").toString());
                                alBean.setLineManagerName(g1.get("FULL_NAME").toString());
                            }
                        }

                    } else if (alBean.getRolrType().equals("POSITION")) {
                        alBean.setRoleId(ItratorBean.getRoleName());
                    }
                    alBean.setNotificationType(ItratorBean.getNotificationType());
                    if( alBean.getRoleId()!= null&&!alBean.getRoleId().isEmpty()){
                        
                        approvalListDao.insertIntoApprovalList(alBean);
                        isInsertApprovalList=true;
                    }
                   
                }
                alBean = null;
            }
            if (isInsertApprovalList) {
           
                wfn.setMsgTitle(extraInformationBean.getCode() + " Request");
                wfn.setMsgBody(extraInformationBean.getCode() + ":" +
                               extraInformationBean.getPerson_id());

                ApprovalListBean rolesBean =
                    approvalListDao.getRoles(Integer.toString(extraInformationBean.getId()),
                                             extraInformationBean.getCode());
                System.out.println( "Notification Type :"+ rolesBean.getNotificationType());
                //1617 XXX_HR_MEDICAL_INSUR_FOR_HR
                if(rolesBean.getNotificationType().equals("FYI")){
                    this.updateStatus("APPROVED", "",
                                      extraInformationBean.getId());

                    selfServiceDao.updateSelfService("APPROVED", sfBean.getType(),
                                                     sfBean.getTransactionId());
                }

                //null LINE_MANAGER

                wfn.setReceiverType(rolesBean.getRolrType());
                wfn.setReceiverId(rolesBean.getRoleId());
                wfn.setType(rolesBean.getNotificationType());
                wfn.setRequestId(Integer.toString(extraInformationBean.getId()));
                wfn.setStatus("OPEN");
                wfn.setSelfType(extraInformationBean.getCode());
                wfn.setPersonName(extraInformationBean.getPersonName());
                wfn.setNationalIdentity(extraInformationBean.getNationalIdentity());
                wfn.setResponsePersonId(rolesBean.getRoleId());
                workflowNotificationDao.insertIntoWorkflow(wfn);

                 System.out.println("Role Type : " +rolesBean.getRolrType() );
                if (rolesBean.getRolrType().equals("LINE_MANAGER")) {
                    try {
                        empObj =
                                employeeDetails.getEmpDetailsByPersonId(extraInformationBean.getLine_manager(),
                                                                        "",
                                                                        "");
                        mailObj.SEND_MAIL(empObj.getEmail(),
                                          rh.getEmailSubject(rolesBean.getNotificationType(),
                                                             extraInformationBean.getEit_name(),
                                                             extraInformationBean.getPersonName()),
                                          rh.getEmailBody(rolesBean.getNotificationType(),
                                                          empObj.getDisplayName(),
                                                          extraInformationBean.getEit_name(),
                                                          extraInformationBean.getPersonName()));
                    } catch (Exception e) {
                        //("Error: ");
                       e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
                        throw new AppsProException(e);
                    }
                } else if (rolesBean.getRolrType().equals("LINE_MANAGER+1")) {
                    try {
                        empObj =
                                employeeDetails.getEmpDetailsByPersonId(extraInformationBean.getManage_of_manager(),
                                                                        "",
                                                                        "");
                        mailObj.SEND_MAIL(empObj.getEmail(),
                                          rh.getEmailSubject(rolesBean.getNotificationType(),
                                                             extraInformationBean.getEit_name(),
                                                             extraInformationBean.getPersonName()),
                                          rh.getEmailBody(rolesBean.getNotificationType(),
                                                          empObj.getDisplayName(),
                                                          extraInformationBean.getEit_name(),
                                                          extraInformationBean.getPersonName()));
                    } catch (Exception e) {
                        //("Error: ");
                       e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
                        throw new AppsProException(e);
                    }
                } else if (rolesBean.getRolrType().equals("ROLES")) {
                    try {
                        Map<String, String> paramMAp =
                            new HashMap<String, String>();
                        paramMAp.put("P_ROLEID", rolesBean.getRoleId());
                        JSONObject respone =
                            BIReportModel.runReport("EmployeeRoleForCustomRoleReport",
                                                    paramMAp);

                        JSONObject dataDS = respone.getJSONObject("DATA_DS");
                        JSONArray g1 = dataDS.getJSONArray("G_1");
                        for (int i = 0; i < g1.length(); i++) {
                            JSONObject data = (JSONObject)g1.get(i);

                            mailObj.SEND_MAIL(data.getString("EMAIL_ADDRESS"),
                                              rh.getEmailSubject(rolesBean.getNotificationType(),
                                                                 extraInformationBean.getEit_name(),
                                                                 extraInformationBean.getPersonName()),
                                              rh.getEmailBody(rolesBean.getNotificationType(),
                                                              data.getString("FULL_NAME"),
                                                              extraInformationBean.getEit_name(),
                                                              extraInformationBean.getPersonName()));
                        }


                    } catch (Exception e) {
                        //("Error: ");
                       e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
                        throw new AppsProException(e);
                    }
                } else if (rolesBean.getRolrType().equals("POSITION")) {
                    try {

                        Map<String, String> paramMAp =
                            new HashMap<String, String>();
                        paramMAp.put("positionId", rolesBean.getRoleId());
                        JSONObject respone =
                            BIReportModel.runReport("EmailForPositionsReport",
                                                    paramMAp);

                        JSONObject dataDS = respone.getJSONObject("DATA_DS");
                        JSONArray g1 = dataDS.getJSONArray("G_1");
                        for (int i = 0; i < g1.length(); i++) {
                            JSONObject data = (JSONObject)g1.get(i);

                            mailObj.SEND_MAIL(data.getString("EMAIL_ADDRESS"),
                                              rh.getEmailSubject(rolesBean.getNotificationType(),
                                                                 extraInformationBean.getEit_name(),
                                                                 extraInformationBean.getPersonName()),
                                              rh.getEmailBody(rolesBean.getNotificationType(),
                                                              data.getString("FULL_NAME"),
                                                              extraInformationBean.getEit_name(),
                                                              extraInformationBean.getPersonName()));
                        }


                    } catch (Exception e) {
                        //("Error: ");
                       e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
                        throw new AppsProException(e);
                    }
                }

            } else {
                efd = new EFFDetails();
                String eitString = extraInformationBean.getEit().toString();
                String eitToString1 = eitString.replaceAll("\\\\", "");
                String eitToString2 =
                    eitToString1.substring(1, eitToString1.length() - 1);
                efd.postEFF(extraInformationBean);
                this.updateStatus("APPROVED", "",
                                  extraInformationBean.getId());

                selfServiceDao.updateSelfService("APPROVED", sfBean.getType(),
                                                 sfBean.getTransactionId());
            }
        }
    }
    
    public ArrayList<PeopleExtraInformationBean> getNotAttachedElement(String q) throws AppsProException {
        String rejectReson = null;
        PeopleExtraInformationBean bean = null;
        connection = AppsproConnection.getConnection();
        ArrayList<PeopleExtraInformationBean> list =
            new ArrayList<PeopleExtraInformationBean>();
        String query =
            "Select ID , status , eit_code ,PERSON_ID, PERSON_NUMBER , CREATION_DATE,EIT FROM "+getSchema_Name() +" .XXX_PER_PEOPLE_EXTRA_INFO \n" + 
            "Where 1=1 \n" + 
            "AND  STATUS ='APPROVED' \n" + 
            "AND EIT_CODE IN ('XXX_HR_SCHOOL_FEES','XXX_HR_MARRIAGE_ALLOWANCE','XXX_HR_RELOCATION_ALLOWANCE','XXX_HR_GOVERN_RELATIONSHIPS','XXX_HR_STOP_LOAN_DEDUCTION','XXX_HR_BUSINESS_PAYMENT'\n" + 
            ",'XXX_HR_OVERTIME_PAYMENT','XXX_HR_MEDICAL_INSUR_FOR_HR')"+q +"  ORDER BY ID";
        
        try {
            ps = connection.prepareStatement(query);
  
            rs = ps.executeQuery();

            while (rs.next()) {
                bean = new PeopleExtraInformationBean();
                bean.setId(Integer.parseInt(rs.getString("ID")));
                bean.setPerson_id(rs.getString("PERSON_ID"));
                bean.setStatus(rs.getString("STATUS"));
                bean.setCode(rs.getString("EIT_CODE"));
                bean.setPerson_number(rs.getString("PERSON_NUMBER"));
                bean.setCreation_date(rs.getString("CREATION_DATE"));
                bean.setEit(rs.getString("EIT"));
                list.add(bean);
            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            throw new AppsProException(e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return list;

    }
    
    
}
