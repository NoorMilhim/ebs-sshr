package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;

import com.appspro.fusionsshr.bean.ProbationaryPeriodEvaluationBean;
import com.appspro.fusionsshr.bean.SummaryReportBean;

import common.restHelper.RestHelper;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.SummaryReportBean;

import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;


public class ProbationaryPeriodEvaluationDAO  extends AppsproConnection{
   
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    private String probationperiodEvaluationID;
    public ArrayList<ProbationaryPeriodEvaluationBean> getAllSumary(String personNumber) {

        ArrayList<ProbationaryPeriodEvaluationBean> SummaryList =
            new ArrayList<ProbationaryPeriodEvaluationBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "select * from" + "  " + " " + getSchema_Name() + ".XXX_PROBATIONARYPERIOD where JOBNUMBER=? order by ID DESC";
            ps = connection.prepareStatement(query);
            ps.setString(1, personNumber);
            rs = ps.executeQuery();
            while (rs.next()) {
                ProbationaryPeriodEvaluationBean summaryReport = new ProbationaryPeriodEvaluationBean();
                summaryReport.setId(rs.getString("ID"));
                summaryReport.setCommitmentanddisciplineq1(rs.getString("COMMITMENTANDDISCIPLINEQ1"));
                summaryReport.setCommitmentanddisciplineq2(rs.getString("COMMITMENTANDDISCIPLINEQ2"));
                summaryReport.setCommitmentanddisciplineq3(rs.getString("COMMITMENTANDDISCIPLINEQ3"));
                summaryReport.setCommitmentanddisciplineq4(rs.getString("COMMITMENTANDDISCIPLINEQ4"));
                summaryReport.setCommitmentanddisciplineq5(rs.getString("COMMITMENTANDDISCIPLINEQ5"));
                summaryReport.setTotalcommitmentanddisciplineq(rs.getString("TOTALCOMMITMENTANDDISCIPLINEQ"));
                
                summaryReport.setAppearanceandbehavior1(rs.getString("APPEARANCEANDBEHAVIOR1"));
                summaryReport.setAppearanceandbehavior2(rs.getString("APPEARANCEANDBEHAVIOR2"));
                summaryReport.setAppearanceandbehavior3(rs.getString("APPEARANCEANDBEHAVIOR3"));
                summaryReport.setAppearanceandbehavior4(rs.getString("APPEARANCEANDBEHAVIOR4"));
                summaryReport.setAppearanceandbehavior5(rs.getString("APPEARANCEANDBEHAVIOR5"));
                summaryReport.setTotalappearanceandbehavior(rs.getString("TOTALAPPEARANCEANDBEHAVIOR"));
                
                
                summaryReport.setInteractionwithothers1(rs.getString("INTERACTIONWITHOTHERS1"));
                summaryReport.setInteractionwithothers2(rs.getString("INTERACTIONWITHOTHERS2"));
                summaryReport.setInteractionwithothers3(rs.getString("INTERACTIONWITHOTHERS3"));
                summaryReport.setInteractionwithothers4(rs.getString("INTERACTIONWITHOTHERS4"));
                summaryReport.setInteractionwithothers5(rs.getString("INTERACTIONWITHOTHERS5"));
                summaryReport.setTotalinteractionwithothers(rs.getString("TOTALINTERACTIONWITHOTHERS"));
                
                
                
                summaryReport.setQualityofcommunication1(rs.getString("QUALITYOFCOMMUNICATION1"));
                summaryReport.setQualityofcommunication2(rs.getString("QUALITYOFCOMMUNICATION2"));
                summaryReport.setQualityofcommunication3(rs.getString("QUALITYOFCOMMUNICATION3"));
                summaryReport.setQualityofcommunication4(rs.getString("QUALITYOFCOMMUNICATION4"));
                summaryReport.setQualityofcommunication5(rs.getString("QUALITYOFCOMMUNICATION5"));
                summaryReport.setTotalqualityofcommunication(rs.getString("TOTALQUALITYOFCOMMUNICATION"));
                summaryReport.setEmployeename(rs.getString("EMPLOYEENAME"));
                summaryReport.setJobname(rs.getString("JOBNUMBER"));
                summaryReport.setJobname(rs.getString("JOBNAME"));
                summaryReport.setHiredate(rs.getString("HIREDATE"));
                summaryReport.setGradecandidate(rs.getString("GRADECANDIDATE"));
                summaryReport.setLocation(rs.getString("LOCATION"));
                summaryReport.setLetterName(rs.getString("LETTERNAME"));
                summaryReport.setFinalEvaluationSummary(rs.getString("FINAL_EVALUATION_SUMMARY"));
                summaryReport.setStatus(rs.getString("STATUS"));
                summaryReport.setFinaldecision(rs.getString("FINALDECISION"));
                summaryReport.setDepartment(rs.getString("DEPARTMENT"));
                summaryReport.setUrl(rs.getString("URL"));
                
               
                SummaryList.add(summaryReport);

            }

        } catch (Exception e) {
            AppsproConnection.LOG.info("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return SummaryList;

    }
    
    
    
    public ArrayList<ProbationaryPeriodEvaluationBean> getAllSumaryById(String id ) {

        ArrayList<ProbationaryPeriodEvaluationBean> SummaryList =
            new ArrayList<ProbationaryPeriodEvaluationBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "select * from" + "  " + " " + getSchema_Name() + ".XXX_PROBATIONARYPERIOD where ID=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, id);
            rs = ps.executeQuery();
            
            while (rs.next()) {
                ProbationaryPeriodEvaluationBean summaryReport = new ProbationaryPeriodEvaluationBean();
                summaryReport.setId(rs.getString("ID"));
                summaryReport.setCommitmentanddisciplineq1(rs.getString("COMMITMENTANDDISCIPLINEQ1"));
                summaryReport.setCommitmentanddisciplineq2(rs.getString("COMMITMENTANDDISCIPLINEQ2"));
                summaryReport.setCommitmentanddisciplineq3(rs.getString("COMMITMENTANDDISCIPLINEQ3"));
                summaryReport.setCommitmentanddisciplineq4(rs.getString("COMMITMENTANDDISCIPLINEQ4"));
                summaryReport.setCommitmentanddisciplineq5(rs.getString("COMMITMENTANDDISCIPLINEQ5"));
                summaryReport.setTotalcommitmentanddisciplineq(rs.getString("TOTALCOMMITMENTANDDISCIPLINEQ"));
                
                summaryReport.setAppearanceandbehavior1(rs.getString("APPEARANCEANDBEHAVIOR1"));
                summaryReport.setAppearanceandbehavior2(rs.getString("APPEARANCEANDBEHAVIOR2"));
                summaryReport.setAppearanceandbehavior3(rs.getString("APPEARANCEANDBEHAVIOR3"));
                summaryReport.setAppearanceandbehavior4(rs.getString("APPEARANCEANDBEHAVIOR4"));
                summaryReport.setAppearanceandbehavior5(rs.getString("APPEARANCEANDBEHAVIOR5"));
                summaryReport.setTotalappearanceandbehavior(rs.getString("TOTALAPPEARANCEANDBEHAVIOR"));
                
                
                summaryReport.setInteractionwithothers1(rs.getString("INTERACTIONWITHOTHERS1"));
                summaryReport.setInteractionwithothers2(rs.getString("INTERACTIONWITHOTHERS2"));
                summaryReport.setInteractionwithothers3(rs.getString("INTERACTIONWITHOTHERS3"));
                summaryReport.setInteractionwithothers4(rs.getString("INTERACTIONWITHOTHERS4"));
                summaryReport.setInteractionwithothers5(rs.getString("INTERACTIONWITHOTHERS5"));
                summaryReport.setTotalinteractionwithothers(rs.getString("TOTALINTERACTIONWITHOTHERS"));
                
                
                
                summaryReport.setQualityofcommunication1(rs.getString("QUALITYOFCOMMUNICATION1"));
                summaryReport.setQualityofcommunication2(rs.getString("QUALITYOFCOMMUNICATION2"));
                summaryReport.setQualityofcommunication3(rs.getString("QUALITYOFCOMMUNICATION3"));
                summaryReport.setQualityofcommunication4(rs.getString("QUALITYOFCOMMUNICATION4"));
                summaryReport.setQualityofcommunication5(rs.getString("QUALITYOFCOMMUNICATION5"));
                summaryReport.setTotalqualityofcommunication(rs.getString("TOTALQUALITYOFCOMMUNICATION"));
                summaryReport.setEmployeename(rs.getString("EMPLOYEENAME"));
                summaryReport.setJobname(rs.getString("JOBNUMBER"));
                summaryReport.setJobname(rs.getString("JOBNAME"));
                summaryReport.setHiredate(rs.getString("HIREDATE"));
                summaryReport.setGradecandidate(rs.getString("GRADECANDIDATE"));
                summaryReport.setLocation(rs.getString("LOCATION"));
                summaryReport.setLetterName(rs.getString("LETTERNAME"));
                summaryReport.setFinalEvaluationSummary(rs.getString("FINAL_EVALUATION_SUMMARY"));
                summaryReport.setStatus(rs.getString("STATUS"));
                summaryReport.setFinaldecision(rs.getString("FINALDECISION"));
                summaryReport.setDepartment(rs.getString("DEPARTMENT"));
                summaryReport.setUrl(rs.getString("URL"));
                
               
                SummaryList.add(summaryReport);

            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return SummaryList;

    }
   
   
   
    
    public ProbationaryPeriodEvaluationBean addProbationPerformanceEvaluation(ProbationaryPeriodEvaluationBean probationEvaluationBean ) {

        try {
            connection = AppsproConnection.getConnection();
           
                String query =
                    "INSERT INTO  " + " " + getSchema_Name() + ".XXX_PROBATIONARYPERIOD (COMMITMENTANDDISCIPLINEQ1,COMMITMENTANDDISCIPLINEQ2,COMMITMENTANDDISCIPLINEQ3\n" + 
                    ",COMMITMENTANDDISCIPLINEQ4,COMMITMENTANDDISCIPLINEQ5,TOTALCOMMITMENTANDDISCIPLINEQ,APPEARANCEANDBEHAVIOR1,APPEARANCEANDBEHAVIOR2\n" + 
                    ",APPEARANCEANDBEHAVIOR3,APPEARANCEANDBEHAVIOR4,APPEARANCEANDBEHAVIOR5,TOTALAPPEARANCEANDBEHAVIOR,INTERACTIONWITHOTHERS1,INTERACTIONWITHOTHERS2\n" + 
                    ",INTERACTIONWITHOTHERS3,INTERACTIONWITHOTHERS4,INTERACTIONWITHOTHERS5,TOTALINTERACTIONWITHOTHERS,QUALITYOFCOMMUNICATION1,QUALITYOFCOMMUNICATION2,\n" + 
                    "QUALITYOFCOMMUNICATION3,QUALITYOFCOMMUNICATION4,QUALITYOFCOMMUNICATION5,TOTALQUALITYOFCOMMUNICATION,EMPLOYEENAME,JOBNUMBER,JOBNAME,\n" + 
                    "HIREDATE,GRADECANDIDATE,LOCATION,LETTERNAME,FINAL_EVALUATION_SUMMARY,STATUS,FINALDECISION,DEPARTMENT,URL\n" + 
                    ")VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                ps = connection.prepareStatement(query);

                ps.setString(1, probationEvaluationBean.getCommitmentanddisciplineq1());
                ps.setString(2, probationEvaluationBean.getCommitmentanddisciplineq2());
                ps.setString(3 ,probationEvaluationBean.getCommitmentanddisciplineq3());
                ps.setString(4, probationEvaluationBean.getCommitmentanddisciplineq4());
                ps.setString(5, probationEvaluationBean.getCommitmentanddisciplineq5());
                ps.setString(6,probationEvaluationBean.getTotalcommitmentanddisciplineq());
                ps.setString(7, probationEvaluationBean.getAppearanceandbehavior1());
                ps.setString(8, probationEvaluationBean.getAppearanceandbehavior2());
                ps.setString(9, probationEvaluationBean.getAppearanceandbehavior3());
                ps.setString(10, probationEvaluationBean.getAppearanceandbehavior4());
                ps.setString(11, probationEvaluationBean.getAppearanceandbehavior5());
                ps.setString(12, probationEvaluationBean.getTotalappearanceandbehavior());
                ps.setString(13, probationEvaluationBean.getInteractionwithothers1());
                ps.setString(14, probationEvaluationBean.getInteractionwithothers2());
                ps.setString(15, probationEvaluationBean.getInteractionwithothers3());
                ps.setString(16, probationEvaluationBean.getInteractionwithothers4());
                ps.setString(17, probationEvaluationBean.getInteractionwithothers5());
                ps.setString(18, probationEvaluationBean.getTotalinteractionwithothers());
                ps.setString(19, probationEvaluationBean.getQualityofcommunication1());
                ps.setString(20, probationEvaluationBean.getQualityofcommunication2());
                ps.setString(21, probationEvaluationBean.getQualityofcommunication3());
                ps.setString(22, probationEvaluationBean.getQualityofcommunication4());
                ps.setString(23, probationEvaluationBean.getQualityofcommunication5());
                ps.setString(24, probationEvaluationBean.getTotalqualityofcommunication());
                ps.setString(25, probationEvaluationBean.getEmployeename());
                ps.setString(26, probationEvaluationBean.getJobnumber());
                ps.setString(27,probationEvaluationBean.getJobname());                
                ps.setString(28, probationEvaluationBean.getHiredate());
                ps.setString(29, probationEvaluationBean.getGradecandidate());
                ps.setString(30, probationEvaluationBean.getLocation());
                ps.setString(31, probationEvaluationBean.getLetterName());
                ps.setString(32, probationEvaluationBean.getFinalEvaluationSummary());
                ps.setString(33, probationEvaluationBean.getStatus());
                ps.setString(34, probationEvaluationBean.getFinaldecision()); 
                ps.setString(35, probationEvaluationBean.getDepartment());
                ps.setString(36, probationEvaluationBean.getUrl());
                
                
                ps.executeUpdate();
            

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return probationEvaluationBean;
    }
   
   
    public ProbationaryPeriodEvaluationBean UpdateProbationPerformanceEvaluation(ProbationaryPeriodEvaluationBean probationEvaluationBean) {

        try {
            connection = AppsproConnection.getConnection();
        
                String query =
                    "update " + " " + getSchema_Name() + ".XXX_PROBATIONARYPERIOD  set COMMITMENTANDDISCIPLINEQ1=?,COMMITMENTANDDISCIPLINEQ2=?,COMMITMENTANDDISCIPLINEQ3=?,\n" + 
                    "COMMITMENTANDDISCIPLINEQ4=?,COMMITMENTANDDISCIPLINEQ5=?,TOTALCOMMITMENTANDDISCIPLINEQ=?,APPEARANCEANDBEHAVIOR1=?,\n" + 
                    "APPEARANCEANDBEHAVIOR2=?,APPEARANCEANDBEHAVIOR3=?,APPEARANCEANDBEHAVIOR4=?,APPEARANCEANDBEHAVIOR5=?,TOTALAPPEARANCEANDBEHAVIOR=?,\n" + 
                    "INTERACTIONWITHOTHERS1=?,INTERACTIONWITHOTHERS2=?,INTERACTIONWITHOTHERS3=?,INTERACTIONWITHOTHERS4=?,INTERACTIONWITHOTHERS5=?,\n" + 
                    "TOTALINTERACTIONWITHOTHERS=?,QUALITYOFCOMMUNICATION1=?,QUALITYOFCOMMUNICATION2=?,QUALITYOFCOMMUNICATION3=?,\n" + 
                    "QUALITYOFCOMMUNICATION4=?,QUALITYOFCOMMUNICATION5=?,TOTALQUALITYOFCOMMUNICATION=?,EMPLOYEENAME=?,JOBNUMBER=?,\n" + 
                    "JOBNAME=?,HIREDATE=?,GRADECANDIDATE=?,LOCATION=?,LETTERNAME=? ,FINAL_EVALUATION_SUMMARY=?,FINALDECISION=?,DEPARTMENT=?  where ID=?";
                ps = connection.prepareStatement(query);

                ps.setString(1, probationEvaluationBean.getCommitmentanddisciplineq1());
                ps.setString(2, probationEvaluationBean.getCommitmentanddisciplineq2());
                ps.setString(3 ,probationEvaluationBean.getCommitmentanddisciplineq3());
                ps.setString(4, probationEvaluationBean.getCommitmentanddisciplineq4());
                ps.setString(5, probationEvaluationBean.getCommitmentanddisciplineq5());
                ps.setString(6,probationEvaluationBean.getTotalcommitmentanddisciplineq());
            
                ps.setString(7, probationEvaluationBean.getAppearanceandbehavior1());
                ps.setString(8, probationEvaluationBean.getAppearanceandbehavior2());
                ps.setString(9, probationEvaluationBean.getAppearanceandbehavior3());
                ps.setString(10, probationEvaluationBean.getAppearanceandbehavior4());
                ps.setString(11, probationEvaluationBean.getAppearanceandbehavior5());
                ps.setString(12, probationEvaluationBean.getTotalappearanceandbehavior());
            
                ps.setString(13, probationEvaluationBean.getInteractionwithothers1());
                ps.setString(14, probationEvaluationBean.getInteractionwithothers2());
                ps.setString(15, probationEvaluationBean.getInteractionwithothers3());
                ps.setString(16, probationEvaluationBean.getInteractionwithothers4());
                ps.setString(17, probationEvaluationBean.getInteractionwithothers5());
                ps.setString(18, probationEvaluationBean.getTotalinteractionwithothers());
            
                ps.setString(19, probationEvaluationBean.getQualityofcommunication1());
                ps.setString(20, probationEvaluationBean.getQualityofcommunication2());
                ps.setString(21, probationEvaluationBean.getQualityofcommunication3());
                ps.setString(22, probationEvaluationBean.getQualityofcommunication4());
                ps.setString(23, probationEvaluationBean.getQualityofcommunication5());
                ps.setString(24, probationEvaluationBean.getTotalqualityofcommunication());
            
                ps.setString(25, probationEvaluationBean.getEmployeename());
                 ps.setString(26, probationEvaluationBean.getJobnumber());
                ps.setString(27,probationEvaluationBean.getJobname());
                
                ps.setString(28, probationEvaluationBean.getHiredate());
                ps.setString(29, probationEvaluationBean.getGradecandidate());
                ps.setString(30, probationEvaluationBean.getLocation());
                ps.setString(31, probationEvaluationBean.getLetterName());
                ps.setString(32, probationEvaluationBean.getFinalEvaluationSummary());
                ps.setString(33, probationEvaluationBean.getFinaldecision());
                ps.setString(34, probationEvaluationBean.getDepartment());
                ps.setString(35, probationEvaluationBean.getId());
                
                
                ps.executeUpdate();
           

        } catch (Exception e) {
            AppsproConnection.LOG.info("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return probationEvaluationBean;
    }
    
    public ProbationaryPeriodEvaluationBean UpdateStatus(ProbationaryPeriodEvaluationBean probationEvaluationBean) {

        try {
            connection = AppsproConnection.getConnection();
        
                String query =
                    "update " + " " + getSchema_Name() + ".XXX_PROBATIONARYPERIOD  set STATUS=? where ID=?";
                ps = connection.prepareStatement(query);

                ps.setString(1, probationEvaluationBean.getStatus()); 
                ps.setString(2, probationEvaluationBean.getId());
                ps.executeUpdate();
           

        } catch (Exception e) {
           AppsproConnection.LOG.info("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return probationEvaluationBean;
    }
    
   
    public String getMaxprobationperiodEvaluation() {
            try {
                connection = AppsproConnection.getConnection();
                String query = null;
                query = "SELECT  MAX(ID) AS max_ID FROM "+getSchema_Name() +".XXX_PROBATIONARYPERIOD";
                ps = connection.prepareStatement(query);
                rs = ps.executeQuery();
                while (rs.next()) {
                    probationperiodEvaluationID = rs.getString("max_ID");
                }
            } catch (Exception e) {
                AppsproConnection.LOG.info("Error: ");
               e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            } finally {
                 closeResources(connection, ps, rs);
            }
            return probationperiodEvaluationID;
        }
   
   
   
    public static void main(String[] args) {
          
            ProbationaryPeriodEvaluationDAO probationperiodEvaluation = new ProbationaryPeriodEvaluationDAO();
            String xx=probationperiodEvaluation.getMaxprobationperiodEvaluation();
            AppsproConnection.LOG.info(xx);
          
          
        }
   
   
   
   
   
   
}
