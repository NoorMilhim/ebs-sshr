/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.ApprovalListBean;
import com.appspro.fusionsshr.bean.ApprovalSetupBean;
import com.appspro.fusionsshr.bean.SelfServiceBean;

import com.appspro.fusionsshr.bean.WorkFlowNotificationBean;

import com.hha.appsproExceptionn.AppsProException;

import com.oracle.vmm.client.provider.ovm22.ws.sps.ArrayList;

import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Iterator;


import java.util.TimeZone;

import org.json.JSONObject;
public class SelfServiceDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();

    public SelfServiceBean insertIntoSelfService(SelfServiceBean bean) {
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query = "insert into  "+ " " + getSchema_Name() + ".XX_SELF_SERVICE (TYPE,TYPE_TABLE_NAME,TRANSACTION_ID,PERSON_ID,CREATED_BY,PERSON_NUMBER,CREATION_DATE) "
                    + "VALUES(?,?,?,?,?,?,sysdate)";
            ps = connection.prepareStatement(query);
            ps.setString(1, bean.getType());
            ps.setString(2, bean.getTypeTableName());
            ps.setString(3, bean.getTransactionId());
            ps.setString(4, bean.getPresonId());
            ps.setString(5, bean.getPresonId());
            // ps.setString(6, "sysdate");
            ps.setString(6, bean.getPersonNumber());
            ps.executeUpdate();
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return bean;
    }

    public void updateSelfService(String RESPONSE_CODE, String ssType, String transactionId) {
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            //(transactionId);
            query = "UPDATE   "+ " " + getSchema_Name() + ".XX_SELF_SERVICE  "
                    + "SET STATUS = ?"
                    + "WHERE TYPE = ? AND TRANSACTION_ID = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, RESPONSE_CODE);
            ps.setString(2, ssType);
            ps.setString(3, transactionId);
            ps.executeUpdate();
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
    }
    
    public String approvalpropation(SelfServiceBean bean) throws AppsProException {
                SelfServiceBean sfBean = new SelfServiceBean();

                java.util.ArrayList<ApprovalSetupBean> approvalSetupList = new java.util.ArrayList<ApprovalSetupBean>();
                
                
                ApprovalSetupDAO approvalSetup = new ApprovalSetupDAO();
                ApprovalListBean alBean = new ApprovalListBean();
                ApprovalListDAO approvalListDao = new ApprovalListDAO();
                WorkflowNotificationDAO workflowNotificationDao =
                    new WorkflowNotificationDAO();
                WorkFlowNotificationBean wfn = new WorkFlowNotificationBean();
                PeopleExtraInformationDAO peopleExtra = new PeopleExtraInformationDAO();
                ProbationaryPeriodEvaluationDAO probationperiodEvaluation = new ProbationaryPeriodEvaluationDAO();
                int requestID = Integer.parseInt(probationperiodEvaluation.getMaxprobationperiodEvaluation());
                
                sfBean.setType("XXX_HR_PROBATION_PERFORMANCE");
                sfBean.setTypeTableName("XXX_HR_PROBATION_PERFORMANCE");
                sfBean.setTransactionId(Integer.toString(requestID));
                sfBean.setPresonId(bean.getPresonId());
                sfBean.setCreated_by(bean.getCreated_by());
                sfBean.setPersonNumber(bean.getPersonNumber());
                insertIntoSelfService(sfBean);
                String s = "TEST";
                if (!s.equals("DRAFT")) {
                    approvalSetupList =
                            approvalSetup.getApprovalByEITCode(bean.getType());
                 
                    
                    alBean.setStepLeval("0");
                    alBean.setServiceType(bean.getType());
                    alBean.setTransActionId(Integer.toString(requestID));
                    alBean.setWorkflowId("");
                    alBean.setRolrType("EMP");
                    alBean.setRoleId(bean.getPresonId());
                    alBean.setPersonName(bean.getPersonName());
                    alBean.setResponseCode("SUBMIT");
                    alBean.setNotificationType("FYI");

                    approvalListDao.insertIntoApprovalList(alBean);
                    alBean = null;
                    ApprovalSetupBean ItratorBean;
                    Iterator<ApprovalSetupBean> approvalSetupListIterator =
                        approvalSetupList.iterator();
                    while (approvalSetupListIterator.hasNext()) {
                        alBean = new ApprovalListBean();
                        ItratorBean = approvalSetupListIterator.next();
                        alBean.setStepLeval(ItratorBean.getApprovalOrder());
                        alBean.setServiceType(bean.getType());
                        alBean.setTransActionId(Integer.toString(requestID));
                        alBean.setWorkflowId("");
                        alBean.setRolrType(ItratorBean.getApprovalType());
                        if (!alBean.getRolrType().equals("POSITION")) {
                            if (alBean.getRolrType().equals("EMP")) {
                                alBean.setRoleId(bean.getPresonId());
                            } else if (alBean.getRolrType().equals("LINE_MANAGER")) {
                                alBean.setRoleId(bean.getManagerId());
                                alBean.setLineManagerName(bean.getManagerName());
                            } else if (alBean.getRolrType().equals("LINE_MANAGER+1") ) {
                                alBean.setRoleId(bean.getManagerOfManager());
                                alBean.setLineManagerName(bean.getManagerOfMnagerName());
                            } else if (alBean.getRolrType().equals("ROLES")) {
                                    alBean.setRoleId(ItratorBean.getRoleName());
                             }else if (alBean.getRolrType().equals("Special_Case")) {
     
                                //Caall Dynamic Report Validation -- arg
                                BIReportDAO bIReportDAO = new BIReportDAO();
                                JSONObject obj = new JSONObject();
                                obj.put("reportName", "DynamicValidationReport");
                                obj.put("str", ItratorBean.getSpecialCase());

                                JSONObject jsonObj =
                                    new JSONObject(bIReportDAO.getBiReport(obj));

                                //Value - return
                                AppsproConnection.LOG.info("VALUE ====== " +
                                                   jsonObj.get("value").toString());
                                alBean.setRoleId(jsonObj.get("value").toString());
                            }

                        } else if (alBean.getRolrType().equals("POSITION")) {
                            alBean.setRoleId(ItratorBean.getRoleName());
                        }

                        alBean.setNotificationType(ItratorBean.getNotificationType());

                        approvalListDao.insertIntoApprovalList(alBean);
                        alBean = null;
                    }
                    if (!approvalSetupList.isEmpty()) {
                        wfn.setMsgTitle(bean.getType() + "Request");
                        wfn.setMsgBody(bean.getType() + "Request for" +
                                       bean.getPresonId());
                        wfn.setPersonName(bean.getPersonName());
                        ApprovalListBean rolesBean =
                            approvalListDao.getRoles(Integer.toString(requestID),
                                                     bean.getType());
                        
                        wfn.setReceiverType(rolesBean.getRolrType());
                        wfn.setReceiverId(rolesBean.getRoleId());
                        wfn.setType(rolesBean.getNotificationType());
                        wfn.setRequestId(Integer.toString(requestID));
                        wfn.setStatus("OPEN");
                        wfn.setSelfType(bean.getType());
                        workflowNotificationDao.insertIntoWorkflow(wfn);

                    } else {
                        peopleExtra.updateStatus("APPROVED","", requestID);

                        updateSelfService("APPROVED", sfBean.getType(),
                                          sfBean.getTransactionId());
                    }

                }
                return "";
            }
    
    
    
   
    

}
