/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.AbsenceBean;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.trustAllHosts;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class AbsenceDAO extends RestHelper {

    public void postAbsence(String bean) throws Exception {
        try {

            String url = this.getInstanceUrl() + this.getAbsenceUrl();

            URL obj = new URL(null, url, new sun.net.www.protocol.https.Handler());
            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json;");
            con.setRequestProperty("Accept", "application/json");
            con.setConnectTimeout(6000000);
            con.setRequestProperty("Authorization",
                   "Basic " + getAuth());
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String json = ow.writeValueAsString(bean);
            String urlParameters = json;

            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(bean);
            wr.flush();
            wr.close();
            int responseCode = con.getResponseCode();
           AppsproConnection.LOG.info("\nSending 'POST' request to URL : " + url);
            AppsproConnection.LOG.info("Post parameters : " + urlParameters);
            AppsproConnection.LOG.info("Response Code : " + responseCode);
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }

    }
    
    

        public static void main(String[] args) {
            String Str="MSG_BODYXXX_HR_EMP_DEBIT_SETTLEMENTRequest for:100000003273720";
            //(Str.indexOf( ':' ));
            //(Str.substring(47));
            //(Str.substring(1,47));            
        }
    
    
    
    
    
}
