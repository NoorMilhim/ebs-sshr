package com.appspro.fusionsshr.dao;


import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.EmployeeTimeAttandanceBean;

import common.restHelper.RestHelper;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import static common.restHelper.RestHelper.getSchema_Name;


public class EmployeeTimeAttandanceDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    Statement stmt = null;
    RestHelper rh = new RestHelper();



    public ArrayList<EmployeeTimeAttandanceBean> getAllEmployeeTimeAtandance(EmployeeTimeAttandanceBean  empAttandance) throws Exception {
        ArrayList<EmployeeTimeAttandanceBean> list = new ArrayList<EmployeeTimeAttandanceBean>();
        ArrayList<EmployeeTimeAttandanceBean> _list = new ArrayList<EmployeeTimeAttandanceBean>();
        int   size = empAttandance.getID() < 100 ? 100 : empAttandance.getID();
        AppsproConnection.LOG.info("getAllEmployeeTimeAtandance(size)=>>"+size);

        int id = 0;
        do{
            _list.clear();
            _list = getAllEmployeeTimeAtandance(empAttandance.getStartDate(),empAttandance.getEndDate(),id,size);
            if(_list.size() > 0){
                list.addAll(_list);
                id = _list.get(_list.size()  -1).getID();
            }
        }while(_list.size() > 0 && id != 0);
       AppsproConnection.LOG.info("getAllEmployeeTimeAtandance(finished)=>>"+list.size());
        return list;
    }


    private ArrayList<EmployeeTimeAttandanceBean> getAllEmployeeTimeAtandance(String startDate,String endDate, int id,int size) throws Exception {
        ArrayList<EmployeeTimeAttandanceBean> empTimeAttandanceList =
            new ArrayList<EmployeeTimeAttandanceBean>();
        try {
                connection = AppsproConnection.getConnection();
            AppsproConnection.LOG.info("getAllEmployeeTimeAtandance(id)=>>"+id);

                String query = 
                    "   select * from ( SELECT ID, PER_TIME.PUNCH_DATE,PER_TIME.PERSON_NUMBER,PER_TIME.PERSON_NAME,PER_TIME.SR_NUMBER,PER_TIME.PUNCH_TIME AS TODAY_PUNCH,DEVICE_NAME, \n" + 
                    "                   (SELECT TO_CHAR(MAX(TO_DATE (PUNCH_TIME, 'HH24:MI')),'HH24:MI') \n" + 
                    "                        FROM XX_PERSON_TIME_CARD\n" + 
                    "                     WHERE TO_DATE (PUNCH_DATE, 'YYYY-MM-DD') = \n" + 
                    "                                (TO_DATE (PER_TIME.PUNCH_DATE, 'YYYY-MM-DD') - 1) AND PERSON_NUMBER = PER_TIME.PERSON_NUMBER)\n" + 
                    "                         AS PERVIOUS_DATE_PUNCH_OUT,\n" + 
                    "                        (SELECT TO_CHAR(MAX(TO_DATE (PUNCH_DATE, 'YYYY-MM-DD')),'YYYY-mm-dd')\n" + 
                    "                         FROM XX_PERSON_TIME_CARD\n" + 
                    "                        WHERE TO_DATE (PUNCH_DATE, 'YYYY-MM-DD') =\n" + 
                    "                                 (TO_DATE (PER_TIME.PUNCH_DATE, 'YYYY-MM-DD') - 1) AND PERSON_NUMBER = PER_TIME.PERSON_NUMBER)\n" + 
                    "                         AS PERVIOUS_DATE_DATE,\n" + 
                    "                        (SELECT TO_CHAR(MIN(TO_DATE (PUNCH_TIME, 'HH24:MI')),'HH24:MI')\n" + 
                    "                        FROM XX_PERSON_TIME_CARD\n" + 
                    "                       WHERE TO_DATE (PUNCH_DATE, 'YYYY-MM-DD') = \n" + 
                    "                              (TO_DATE (PER_TIME.PUNCH_DATE, 'YYYY-MM-DD') - 1) AND PERSON_NUMBER = PER_TIME.PERSON_NUMBER)\n" + 
                    "                        AS PERVIOUS_DATE_PUNCH_IN\n" + 
                    "                 FROM XX_PERSON_TIME_CARD PER_TIME \n" + 
                    "                 WHERE  PUNCH_DATE >=  ? and  PUNCH_DATE <= ? and ID > ?" + 
                    "             ORDER BY ID ASC ) where rownum <= ?";
                ps = connection.prepareStatement(query);
            ps.setString(1, startDate);
            ps.setString(2, endDate);
            ps.setInt(3, id);
            ps.setInt(4, size);
            rs = ps.executeQuery();
            
                while (rs.next()) {
                    EmployeeTimeAttandanceBean empTimeAttandance =
                        new EmployeeTimeAttandanceBean();
                    empTimeAttandance.setID(rs.getInt("ID"));
                    empTimeAttandance.setPunch_date(rs.getString("PUNCH_DATE")== null ?"Absent So Far" :rs.getString("PUNCH_DATE"));
                    empTimeAttandance.setPersonNumber(rs.getString("PERSON_NUMBER")== null ?"Absent So Far" :rs.getString("PERSON_NUMBER"));
                    empTimeAttandance.setPersonName(rs.getString("PERSON_NAME")== null ?"Absent So Far" :rs.getString("PERSON_NAME"));
                    empTimeAttandance.setSr_number(rs.getString("SR_NUMBER")== null ?"Absent So Far" :rs.getString("SR_NUMBER"));
                    empTimeAttandance.setTodaytDate(rs.getString("TODAY_PUNCH")== null ?"Absent So Far" :rs.getString("TODAY_PUNCH"));
                    empTimeAttandance.setDeviceName(rs.getString("DEVICE_NAME")== null ?"Absent So Far" :rs.getString("DEVICE_NAME"));
                    empTimeAttandance.setPerviousDatePunchOut(rs.getString("PERVIOUS_DATE_PUNCH_OUT")== null ?"Absent So Far" :rs.getString("PERVIOUS_DATE_PUNCH_OUT"));
                    empTimeAttandance.setPerviousDateDate(rs.getString("PERVIOUS_DATE_DATE")== null ?"Absent So Far" :rs.getString("PERVIOUS_DATE_DATE"));
                    empTimeAttandance.setPerviousDatePunchIn(rs.getString("PERVIOUS_DATE_PUNCH_IN")== null ?"Absent So Far" :rs.getString("PERVIOUS_DATE_PUNCH_IN"));
                    empTimeAttandanceList.add(empTimeAttandance);
                }    
        } catch (Exception e) {
            AppsproConnection.LOG.info("getAllEmployeeTimeAtandance=>>"+e.getMessage());
            throw new Exception(e.getMessage());
        } finally {
            closeResources(connection, ps, rs);
        }
        return empTimeAttandanceList;

    }


    public ArrayList<EmployeeTimeAttandanceBean> getAllEmployeeTimeAtandanceByPersonNumber(EmployeeTimeAttandanceBean empAttandance,String employeeNumbers,String employeeNumbers2,String employeeNumbers3) {

        ArrayList<EmployeeTimeAttandanceBean> empTimeAttandanceList =
            new ArrayList<EmployeeTimeAttandanceBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "/* Formatted on 3/3/2019 5:21:37 PM (QP5 v5.256.13226.35510) */\n" +
                "  SELECT PER_TIME.PUNCH_DATE,PER_TIME.PERSON_NUMBER,PER_TIME.PERSON_NAME,PER_TIME.SR_NUMBER,PER_TIME.PUNCH_TIME AS TODAY_PUNCH,DEVICE_NAME,\n" +
                "         (SELECT TO_CHAR(MAX(TO_DATE (PUNCH_TIME, 'HH24:MI')),'HH24:MI')\n" +
                "            FROM XX_PERSON_TIME_CARD\n" +
                "           WHERE TO_DATE (PUNCH_DATE, 'YYYY-MM-DD') =\n" +
                "                    (TO_DATE (PER_TIME.PUNCH_DATE, 'YYYY-MM-DD') - 1) AND PERSON_NUMBER = PER_TIME.PERSON_NUMBER)\n" +
                "            AS PERVIOUS_DATE_PUNCH_OUT,\n" +
                "            (SELECT TO_CHAR(MAX(TO_DATE (PUNCH_DATE, 'YYYY-MM-DD')),'YYYY-mm-dd')\n" +
                "            FROM XX_PERSON_TIME_CARD\n" +
                "           WHERE TO_DATE (PUNCH_DATE, 'YYYY-MM-DD') =\n" +
                "                    (TO_DATE (PER_TIME.PUNCH_DATE, 'YYYY-MM-DD') - 1) AND PERSON_NUMBER = PER_TIME.PERSON_NUMBER)\n" +
                "            AS PERVIOUS_DATE_DATE,\n" +
                "            (SELECT TO_CHAR(MIN(TO_DATE (PUNCH_TIME, 'HH24:MI')),'HH24:MI')\n" +
                "            FROM XX_PERSON_TIME_CARD\n" +
                "           WHERE TO_DATE (PUNCH_DATE, 'YYYY-MM-DD') =\n" +
                "                    (TO_DATE (PER_TIME.PUNCH_DATE, 'YYYY-MM-DD') - 1) AND PERSON_NUMBER = PER_TIME.PERSON_NUMBER)\n" +
                "            AS PERVIOUS_DATE_PUNCH_IN\n" +
                "    FROM XX_PERSON_TIME_CARD PER_TIME\n" +
                "    WHERE  (PERSON_NUMBER=NVL(?,PERSON_NUMBER) AND " +
                "(PERSON_NUMBER IN "+employeeNumbers+"";
                if(employeeNumbers2.length() > 1){
                    query += "OR PERSON_NUMBER IN " +employeeNumbers2;
                }
            if(employeeNumbers3.length() > 1){
                query += "OR PERSON_NUMBER IN " +employeeNumbers3;
            }
            
               query += "))" +
                "AND PUNCH_DATE  between ? and ? \n" +
                "ORDER BY PERSON_NUMBER,TO_DATE (PER_TIME.PUNCH_DATE, 'YYYY-MM-DD') DESC";
            ps = connection.prepareStatement(query);
            ps.setString(1, empAttandance.getPersonNumber().isEmpty() ? null : empAttandance.getPersonNumber());
            ps.setString(2, empAttandance.getStartDate());
            ps.setString(3, empAttandance.getEndDate());
            rs = ps.executeQuery();
            while (rs.next()) {
                EmployeeTimeAttandanceBean empTimeAttandance =
                    new EmployeeTimeAttandanceBean();
                empTimeAttandance.setPunch_date(rs.getString("PUNCH_DATE") ==
                                                null ? "Absent So Far" :
                                                rs.getString("PUNCH_DATE"));
                empTimeAttandance.setPersonNumber(rs.getString("PERSON_NUMBER") ==
                                                  null ? "Absent So Far" :
                                                  rs.getString("PERSON_NUMBER"));
                empTimeAttandance.setPersonName(rs.getString("PERSON_NAME") ==
                                                null ? "Absent So Far" :
                                                rs.getString("PERSON_NAME"));
                empTimeAttandance.setSr_number(rs.getString("SR_NUMBER") ==
                                               null ? "Absent So Far" :
                                               rs.getString("SR_NUMBER"));
                empTimeAttandance.setTodaytDate(rs.getString("TODAY_PUNCH") ==
                                                null ? "Absent So Far" :
                                                rs.getString("TODAY_PUNCH"));
                empTimeAttandance.setDeviceName(rs.getString("DEVICE_NAME") ==
                                                null ? "Absent So Far" :
                                                rs.getString("DEVICE_NAME"));
                empTimeAttandance.setPerviousDatePunchOut(rs.getString("PERVIOUS_DATE_PUNCH_OUT") ==
                                                          null ?
                                                          "Absent So Far" :
                                                          rs.getString("PERVIOUS_DATE_PUNCH_OUT"));
                empTimeAttandance.setPerviousDateDate(rs.getString("PERVIOUS_DATE_DATE") ==
                                                      null ? "Absent So Far" :
                                                      rs.getString("PERVIOUS_DATE_DATE"));
                empTimeAttandance.setPerviousDatePunchIn(rs.getString("PERVIOUS_DATE_PUNCH_IN") ==
                                                         null ?
                                                         "Absent So Far" :
                                                         rs.getString("PERVIOUS_DATE_PUNCH_IN"));


                empTimeAttandanceList.add(empTimeAttandance);

            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return empTimeAttandanceList;

    }
    
    //*********************new *****//
    public ArrayList<EmployeeTimeAttandanceBean> getEmployeeTimeAtandanceBySearch(EmployeeTimeAttandanceBean employeeBean ) {
          String personNumber=null;
          String status=null;
          String date=null;
        if(employeeBean.getPersonNumber().isEmpty()){
               personNumber="";
        }else{
          personNumber=employeeBean.getPersonNumber();   
             
        }
        if(employeeBean.getStatus().isEmpty()){
               status="";
        }else{
          status=employeeBean.getStatus();   
             
        }
        if(employeeBean.getDate().isEmpty()){
               date="";
        }else{
          date='%'+ employeeBean.getDate()+'%';   
             
        }
        
         
        ArrayList<EmployeeTimeAttandanceBean> empTimeAttandanceList =
            new ArrayList<EmployeeTimeAttandanceBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "select * from XX_PERSON_SAAS_TC WHERE PERSON_NUMBER=? OR STATUS=? OR(FIRST_PUNCH_IN_TIME like ? OR FIRST_PUNCH_OUT_TIME\n" + 
                "like ? OR SECOND_PUNCH_OUT_TIME like ? OR SECOND_PUNCH_IN_TIME like ?)";
            ps = connection.prepareStatement(query);
        
            ps.setString(1 , personNumber );
            ps.setString(2 ,status);
            ps.setString(3 ,date );
            ps.setString(4 , date);
            ps.setString(5 ,date);
            ps.setString(6 , date);
           

            rs = ps.executeQuery();
            while (rs.next()) {
                EmployeeTimeAttandanceBean empTimeAttandance =
                    new EmployeeTimeAttandanceBean();
                empTimeAttandance.setPunch_date(rs.getString("PUNCH_DATE"));
                empTimeAttandance.setPersonNumber(rs.getString("PERSON_NUMBER"));
                empTimeAttandance.setFirstPunchInTime(rs.getString("FIRST_PUNCH_IN_TIME"));
                empTimeAttandance.setFirstPunchOutTime(rs.getString("FIRST_PUNCH_OUT_TIME"));
                empTimeAttandance.setSecondPunchInTime(rs.getString("SECOND_PUNCH_IN_TIME"));
                empTimeAttandance.setSecondPunchOutTime(rs.getString("SECOND_PUNCH_OUT_TIME"));
                empTimeAttandance.setStatus(rs.getString("STATUS"));
                empTimeAttandance.setTransactionType(rs.getString("TRS_TYPE"));
                empTimeAttandance.setMeasure(rs.getString("MEASURE"));
                empTimeAttandance.setResponseDescription(rs.getString("RESPONE_DESC"));
                empTimeAttandance.setRequestBody(rs.getString("REQUEST_BODY"));

                empTimeAttandanceList.add(empTimeAttandance);

            }

        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return empTimeAttandanceList;

    }
    
    public static void main(String[] args) {
        try {
            new EmployeeTimeAttandanceDAO().test();
        } catch (SQLException e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }
    }

    private void test() throws SQLException {
            connection = AppsproConnection.getConnection();
            String query =
//                "    SELECT ID, PER_TIME.PUNCH_DATE,PER_TIME.PERSON_NUMBER,PER_TIME.PERSON_NAME,PER_TIME.SR_NUMBER,PER_TIME.PUNCH_TIME AS TODAY_PUNCH,DEVICE_NAME, \n" + 
//                "                   (SELECT TO_CHAR(MAX(TO_DATE (PUNCH_TIME, 'HH24:MI')),'HH24:MI') \n" + 
//                "                        FROM XX_PERSON_TIME_CARD\n" + 
//                "                     WHERE TO_DATE (PUNCH_DATE, 'YYYY-MM-DD') = \n" + 
//                "                                (TO_DATE (PER_TIME.PUNCH_DATE, 'YYYY-MM-DD') - 1) AND PERSON_NUMBER = PER_TIME.PERSON_NUMBER)\n" + 
//                "                         AS PERVIOUS_DATE_PUNCH_OUT,\n" + 
//                "                        (SELECT TO_CHAR(MAX(TO_DATE (PUNCH_DATE, 'YYYY-MM-DD')),'YYYY-mm-dd')\n" + 
//                "                         FROM XX_PERSON_TIME_CARD\n" + 
//                "                        WHERE TO_DATE (PUNCH_DATE, 'YYYY-MM-DD') =\n" + 
//                "                                 (TO_DATE (PER_TIME.PUNCH_DATE, 'YYYY-MM-DD') - 1) AND PERSON_NUMBER = PER_TIME.PERSON_NUMBER)\n" + 
//                "                         AS PERVIOUS_DATE_DATE,\n" + 
//                "                        (SELECT TO_CHAR(MIN(TO_DATE (PUNCH_TIME, 'HH24:MI')),'HH24:MI')\n" + 
//                "                        FROM XX_PERSON_TIME_CARD\n" + 
//                "                       WHERE TO_DATE (PUNCH_DATE, 'YYYY-MM-DD') = \n" + 
//                "                              (TO_DATE (PER_TIME.PUNCH_DATE, 'YYYY-MM-DD') - 1) AND PERSON_NUMBER = PER_TIME.PERSON_NUMBER)\n" + 
//                "                        AS PERVIOUS_DATE_PUNCH_IN\n" + 
                "                 SELECT * FROM XX_PERSON_TIME_CARD ORDER BY ID ASC";
//                "                 WHERE  PUNCH_DATE >=  '2019-05-08' and  PUNCH_DATE <= '2019-10-08' "+ 
//                "             ORDER BY PERSON_NUMBER,TO_DATE (PER_TIME.PUNCH_DATE, 'YYYY-MM-DD') DESC";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            int count = 0;
            while (rs.next()) {
                ++count;
            }
            AppsproConnection.LOG.info(count);
        }
}
