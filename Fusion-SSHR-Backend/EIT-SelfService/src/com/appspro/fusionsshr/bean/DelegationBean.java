package com.appspro.fusionsshr.bean;

public class DelegationBean {
    private String id;
    private String effectiveStartDate;
    private String effectiveEndDate;
    private String personId;
    private String personNumber;
    private String createdByPersonId;
    private String createByPersonNumber;
    


    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setEffectiveStartDate(String effectiveStartDate) {
        this.effectiveStartDate = effectiveStartDate;
    }

    public String getEffectiveStartDate() {
        return effectiveStartDate;
    }

    public void setEffectiveEndDate(String effectiveEndDate) {
        this.effectiveEndDate = effectiveEndDate;
    }

    public String getEffectiveEndDate() {
        return effectiveEndDate;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonNumber(String personNumber) {
        this.personNumber = personNumber;
    }

    public String getPersonNumber() {
        return personNumber;
    }

    public void setCreatedByPersonId(String createdByPersonId) {
        this.createdByPersonId = createdByPersonId;
    }

    public String getCreatedByPersonId() {
        return createdByPersonId;
    }

    public void setCreateByPersonNumber(String createByPersonNumber) {
        this.createByPersonNumber = createByPersonNumber;
    }

    public String getCreateByPersonNumber() {
        return createByPersonNumber;
    }
}
