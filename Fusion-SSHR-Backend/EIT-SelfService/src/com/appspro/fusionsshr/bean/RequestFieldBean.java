/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.bean;

/**
 *
 * @author amro
 */
public class RequestFieldBean {

    private String id;
    private String requestCode;
    private String requestName;
    private String applicationColumnName;
    private String endUserColumnName;
    private String lastUpdateDate;
    private String lastUpdateBy;
    private String creationDate;
    private String createdBy;
    private String columnSEQNum;
    private String enabledFlag;
    private String requiredFlag;
    private String displayFlag;
    private String displaySize;
    private String maxDescLeng;
    private String concatDescLeng;
    private String flexValueSetId;
    private String defaultType;
    private String defaultValue;

    public RequestFieldBean() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRequestCode() {
        return requestCode;
    }

    public void setRequestCode(String requestCode) {
        this.requestCode = requestCode;
    }

    public String getRequestName() {
        return requestName;
    }

    public void setRequestName(String requestName) {
        this.requestName = requestName;
    }

    public String getApplicationColumnName() {
        return applicationColumnName;
    }

    public void setApplicationColumnName(String applicationColumnName) {
        this.applicationColumnName = applicationColumnName;
    }

    public String getEndUserColumnName() {
        return endUserColumnName;
    }

    public void setEndUserColumnName(String endUserColumnName) {
        this.endUserColumnName = endUserColumnName;
    }

    public String getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(String lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public String getLastUpdateBy() {
        return lastUpdateBy;
    }

    public void setLastUpdateBy(String lastUpdateBy) {
        this.lastUpdateBy = lastUpdateBy;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getColumnSEQNum() {
        return columnSEQNum;
    }

    public void setColumnSEQNum(String columnSEQNum) {
        this.columnSEQNum = columnSEQNum;
    }

    public String getEnabledFlag() {
        return enabledFlag;
    }

    public void setEnabledFlag(String enabledFlag) {
        this.enabledFlag = enabledFlag;
    }

    public String getRequiredFlag() {
        return requiredFlag;
    }

    public void setRequiredFlag(String requiredFlag) {
        this.requiredFlag = requiredFlag;
    }

    public String getDisplayFlag() {
        return displayFlag;
    }

    public void setDisplayFlag(String displayFlag) {
        this.displayFlag = displayFlag;
    }

    public String getDisplaySize() {
        return displaySize;
    }

    public void setDisplaySize(String displaySize) {
        this.displaySize = displaySize;
    }

    public String getMaxDescLeng() {
        return maxDescLeng;
    }

    public void setMaxDescLeng(String maxDescLeng) {
        this.maxDescLeng = maxDescLeng;
    }

    public String getConcatDescLeng() {
        return concatDescLeng;
    }

    public void setConcatDescLeng(String concatDescLeng) {
        this.concatDescLeng = concatDescLeng;
    }

    public String getFlexValueSetId() {
        return flexValueSetId;
    }

    public void setFlexValueSetId(String flexValueSetId) {
        this.flexValueSetId = flexValueSetId;
    }

    public String getDefaultType() {
        return defaultType;
    }

    public void setDefaultType(String defaultType) {
        this.defaultType = defaultType;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Override
    public String toString() {
        return "RequestFieldBean{" + "id=" + id + ", requestCode=" + requestCode + ", requestName=" + requestName + ", applicationColumnName=" + applicationColumnName + ", endUserColumnName=" + endUserColumnName + ", lastUpdateDate=" + lastUpdateDate + ", lastUpdateBy=" + lastUpdateBy + ", creationDate=" + creationDate + ", createdBy=" + createdBy + ", columnSEQNum=" + columnSEQNum + ", enabledFlag=" + enabledFlag + ", requiredFlag=" + requiredFlag + ", displayFlag=" + displayFlag + ", displaySize=" + displaySize + ", maxDescLeng=" + maxDescLeng + ", concatDescLeng=" + concatDescLeng + ", flexValueSetId=" + flexValueSetId + ", defaultType=" + defaultType + ", defaultValue=" + defaultValue + '}';
    }

}
