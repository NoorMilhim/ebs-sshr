/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.bean;

/**
 *
 * @author user
 */
public class OrgnizationBean {

    private String effectiveStartDate;
    private String effectiveEndDate;
    private String name;
    private String classificationCode;
    private String internalAddressLine;
    private String locationId;
    private String organizationCode;
    private String status;
    private String creationDate;
    private String orgCode;
    private String personName;
    

    public OrgnizationBean(String effectiveStartDate, String effectiveEndDate, String name, String classificationCode, String internalAddressLine, String locationId, String organizationCode, String status, String creationDate, String orgCode) {
        this.effectiveStartDate = effectiveStartDate;
        this.effectiveEndDate = effectiveEndDate;
        this.name = name;
        this.classificationCode = classificationCode;
        this.internalAddressLine = internalAddressLine;
        this.locationId = locationId;
        this.organizationCode = organizationCode;
        this.status = status;
        this.creationDate = creationDate;
        this.orgCode = orgCode;
    }

    public OrgnizationBean() {

    }

    public String getEffectiveStartDate() {
        return effectiveStartDate;
    }

    public String getEffectiveEndDate() {
        return effectiveEndDate;
    }

    public String getName() {
        return name;
    }

    public String getClassificationCode() {
        return classificationCode;
    }

    public String getInternalAddressLine() {
        return internalAddressLine;
    }

    public String getLocationId() {
        return locationId;
    }

    public String getOrganizationCode() {
        return organizationCode;
    }

    public String getStatus() {
        return status;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setEffectiveStartDate(String effectiveStartDate) {
        this.effectiveStartDate = effectiveStartDate;
    }

    public void setEffectiveEndDate(String effectiveEndDate) {
        this.effectiveEndDate = effectiveEndDate;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setClassificationCode(String classificationCode) {
        this.classificationCode = classificationCode;
    }

    public void setInternalAddressLine(String internalAddressLine) {
        this.internalAddressLine = internalAddressLine;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public void setOrganizationCode(String organizationCode) {
        this.organizationCode = organizationCode;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    @Override
    public String toString() {
        return "OrgnizationBean{" + "effectiveStartDate=" + effectiveStartDate + ", effectiveEndDate=" + effectiveEndDate + ", name=" + name + ", classificationCode=" + classificationCode + ", internalAddressLine=" + internalAddressLine + ", locationId=" + locationId + ", organizationCode=" + organizationCode + ", syatus=" + status + ", creationDate=" + creationDate + ", orgCode=" + orgCode + '}';
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPersonName() {
        return personName;
    }
}
