package com.appspro.fusionsshr.bean;

public class BulkMenuBean {
    private int ID;
    private String CODE;
    private String TITLE_AR;
    private String TITLE_EN;
    private String COLOR;
    private String ICON;
    private int VISIBLE;

    public BulkMenuBean(){
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getID() {
        return ID;
    }

    public void setCODE(String CODE) {
        this.CODE = CODE;
    }

    public String getCODE() {
        return CODE;
    }

    public void setTITLE_AR(String TITLE_AR) {
        this.TITLE_AR = TITLE_AR;
    }

    public String getTITLE_AR() {
        return TITLE_AR;
    }

    public void setTITLE_EN(String TITLE_EN) {
        this.TITLE_EN = TITLE_EN;
    }

    public String getTITLE_EN() {
        return TITLE_EN;
    }

    public void setCOLOR(String COLOR) {
        this.COLOR = COLOR;
    }

    public String getCOLOR() {
        return COLOR;
    }

    public void setICON(String ICON) {
        this.ICON = ICON;
    }

    public String getICON() {
        return ICON;
    }

    public void setVISIBLE(int VISIBLE) {
        this.VISIBLE = VISIBLE;
    }

    public int getVISIBLE() {
        return VISIBLE;
    }
}
