package com.appspro.fusionsshr.bean.rest;

public class resendBean {
    
    private String url;
    private String payload;

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public String getPayload() {
        return payload;
    }
}
