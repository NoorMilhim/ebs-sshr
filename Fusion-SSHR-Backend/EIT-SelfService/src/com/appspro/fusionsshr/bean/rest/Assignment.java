
package com.appspro.fusionsshr.bean.rest;


import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder( { "AssignmentName", "PersonTypeId", "ProposedPersonTypeId",
                      "ProjectedStartDate", "BusinessUnitId", "LocationId",
                      "JobId", "GradeId", "DepartmentId", "WorkerCategory",
                      "AssignmentCategory", "WorkingAtHome",
                      "WorkingAsManager", "SalaryCode", "WorkingHours",
                      "Frequency", "StartTime", "EndTime", "SalaryAmount",
                      "SalaryBasisId", "ActionCode", "ActionReasonCode",
                      "AssignmentStatus", "WorkTaxAddressId", "AssignmentId",
                      "EffectiveStartDate", "EffectiveEndDate", "PositionId",
                      "TermsEffectiveStartDate", "ManagerId",
                      "ManagerAssignmentId", "ManagerType", "AssignmentNumber",
                      "OriginalHireDate", "AssignmentStatusTypeId",
                      "PrimaryAssignmentFlag", "ProbationPeriodEndDate",
                      "ProbationPeriodLength", "ProbationPeriodUnitOfMeasure",
                      "AssignmentProjectedEndDate", "ActualTerminationDate",
                      "LegalEntityId", "PrimaryWorkRelationFlag",
                      "PrimaryWorkTermsFlag", "CreationDate", "LastUpdateDate",
                      "PeriodOfServiceId", "FullPartTime", "RegularTemporary",
                      "GradeLadderId", "peopleGroupKeyFlexfield" })
public class Assignment {

    @JsonProperty("AssignmentName")
    private String assignmentName;
    @JsonProperty("PersonTypeId")
    private Long personTypeId;
    @JsonProperty("ProposedPersonTypeId")
    private Object proposedPersonTypeId;
    @JsonProperty("ProjectedStartDate")
    private Object projectedStartDate;
    @JsonProperty("BusinessUnitId")
    private Long businessUnitId;
    @JsonProperty("LocationId")
    private Long locationId;
    @JsonProperty("JobId")
    private Long jobId;
    @JsonProperty("GradeId")
    private Long gradeId;
    @JsonProperty("DepartmentId")
    private Long departmentId;
    @JsonProperty("WorkerCategory")
    private Object workerCategory;
    @JsonProperty("AssignmentCategory")
    private String assignmentCategory;
    @JsonProperty("WorkingAtHome")
    private String workingAtHome;
    @JsonProperty("WorkingAsManager")
    private String workingAsManager;
    @JsonProperty("SalaryCode")
    private String salaryCode;
    @JsonProperty("WorkingHours")
    private Object workingHours;
    @JsonProperty("Frequency")
    private Object frequency;
    @JsonProperty("StartTime")
    private Object startTime;
    @JsonProperty("EndTime")
    private Object endTime;
    @JsonProperty("SalaryAmount")
    private Long salaryAmount;
    @JsonProperty("SalaryBasisId")
    private Long salaryBasisId;
    @JsonProperty("ActionCode")
    private String actionCode;
    @JsonProperty("ActionReasonCode")
    private String actionReasonCode;
    @JsonProperty("AssignmentStatus")
    private String assignmentStatus;
    @JsonProperty("WorkTaxAddressId")
    private Object workTaxAddressId;
    @JsonProperty("AssignmentId")
    private Long assignmentId;
    @JsonProperty("EffectiveStartDate")
    private String effectiveStartDate;
    @JsonProperty("EffectiveEndDate")
    private String effectiveEndDate;
    @JsonProperty("PositionId")
    private Long positionId;
    @JsonProperty("TermsEffectiveStartDate")
    private String termsEffectiveStartDate;
    @JsonProperty("ManagerId")
    private Object managerId;
    @JsonProperty("ManagerAssignmentId")
    private Object managerAssignmentId;
    @JsonProperty("ManagerType")
    private Object managerType;
    @JsonProperty("AssignmentNumber")
    private String assignmentNumber;
    @JsonProperty("OriginalHireDate")
    private Object originalHireDate;
    @JsonProperty("AssignmentStatusTypeId")
    private Long assignmentStatusTypeId;
    @JsonProperty("PrimaryAssignmentFlag")
    private Boolean primaryAssignmentFlag;
    @JsonProperty("ProbationPeriodEndDate")
    private Object probationPeriodEndDate;
    @JsonProperty("ProbationPeriodLength")
    private Object probationPeriodLength;
    @JsonProperty("ProbationPeriodUnitOfMeasure")
    private Object probationPeriodUnitOfMeasure;
    @JsonProperty("AssignmentProjectedEndDate")
    private Object assignmentProjectedEndDate;
    @JsonProperty("ActualTerminationDate")
    private Object actualTerminationDate;
    @JsonProperty("LegalEntityId")
    private Long legalEntityId;
    @JsonProperty("PrimaryWorkRelationFlag")
    private Boolean primaryWorkRelationFlag;
    @JsonProperty("PrimaryWorkTermsFlag")
    private Boolean primaryWorkTermsFlag;
    @JsonProperty("CreationDate")
    private String creationDate;
    @JsonProperty("LastUpdateDate")
    private String lastUpdateDate;
    @JsonProperty("PeriodOfServiceId")
    private Long periodOfServiceId;
    @JsonProperty("FullPartTime")
    private String fullPartTime;
    @JsonProperty("RegularTemporary")
    private String regularTemporary;
    @JsonProperty("GradeLadderId")
    private Object gradeLadderId;
    @JsonProperty("peopleGroupKeyFlexfield")
    private List<Object> peopleGroupKeyFlexfield = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties =
        new HashMap<String, Object>();

    @JsonProperty("AssignmentName")
    public String getAssignmentName() {
        return assignmentName;
    }

    @JsonProperty("AssignmentName")
    public void setAssignmentName(String assignmentName) {
        this.assignmentName = assignmentName;
    }

    @JsonProperty("PersonTypeId")
    public Long getPersonTypeId() {
        return personTypeId;
    }

    @JsonProperty("PersonTypeId")
    public void setPersonTypeId(Long personTypeId) {
        this.personTypeId = personTypeId;
    }

    @JsonProperty("ProposedPersonTypeId")
    public Object getProposedPersonTypeId() {
        return proposedPersonTypeId;
    }

    @JsonProperty("ProposedPersonTypeId")
    public void setProposedPersonTypeId(Object proposedPersonTypeId) {
        this.proposedPersonTypeId = proposedPersonTypeId;
    }

    @JsonProperty("ProjectedStartDate")
    public Object getProjectedStartDate() {
        return projectedStartDate;
    }

    @JsonProperty("ProjectedStartDate")
    public void setProjectedStartDate(Object projectedStartDate) {
        this.projectedStartDate = projectedStartDate;
    }

    @JsonProperty("BusinessUnitId")
    public Long getBusinessUnitId() {
        return businessUnitId;
    }

    @JsonProperty("BusinessUnitId")
    public void setBusinessUnitId(Long businessUnitId) {
        this.businessUnitId = businessUnitId;
    }

    @JsonProperty("LocationId")
    public Long getLocationId() {
        return locationId;
    }

    @JsonProperty("LocationId")
    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    @JsonProperty("JobId")
    public Long getJobId() {
        return jobId;
    }

    @JsonProperty("JobId")
    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    @JsonProperty("GradeId")
    public Long getGradeId() {
        return gradeId;
    }

    @JsonProperty("GradeId")
    public void setGradeId(Long gradeId) {
        this.gradeId = gradeId;
    }

    @JsonProperty("DepartmentId")
    public Long getDepartmentId() {
        return departmentId;
    }

    @JsonProperty("DepartmentId")
    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    @JsonProperty("WorkerCategory")
    public Object getWorkerCategory() {
        return workerCategory;
    }

    @JsonProperty("WorkerCategory")
    public void setWorkerCategory(Object workerCategory) {
        this.workerCategory = workerCategory;
    }

    @JsonProperty("AssignmentCategory")
    public String getAssignmentCategory() {
        return assignmentCategory;
    }

    @JsonProperty("AssignmentCategory")
    public void setAssignmentCategory(String assignmentCategory) {
        this.assignmentCategory = assignmentCategory;
    }

    @JsonProperty("WorkingAtHome")
    public String getWorkingAtHome() {
        return workingAtHome;
    }

    @JsonProperty("WorkingAtHome")
    public void setWorkingAtHome(String workingAtHome) {
        this.workingAtHome = workingAtHome;
    }

    @JsonProperty("WorkingAsManager")
    public String getWorkingAsManager() {
        return workingAsManager;
    }

    @JsonProperty("WorkingAsManager")
    public void setWorkingAsManager(String workingAsManager) {
        this.workingAsManager = workingAsManager;
    }

    @JsonProperty("SalaryCode")
    public String getSalaryCode() {
        return salaryCode;
    }

    @JsonProperty("SalaryCode")
    public void setSalaryCode(String salaryCode) {
        this.salaryCode = salaryCode;
    }

    @JsonProperty("WorkingHours")
    public Object getWorkingHours() {
        return workingHours;
    }

    @JsonProperty("WorkingHours")
    public void setWorkingHours(Object workingHours) {
        this.workingHours = workingHours;
    }

    @JsonProperty("Frequency")
    public Object getFrequency() {
        return frequency;
    }

    @JsonProperty("Frequency")
    public void setFrequency(Object frequency) {
        this.frequency = frequency;
    }

    @JsonProperty("StartTime")
    public Object getStartTime() {
        return startTime;
    }

    @JsonProperty("StartTime")
    public void setStartTime(Object startTime) {
        this.startTime = startTime;
    }

    @JsonProperty("EndTime")
    public Object getEndTime() {
        return endTime;
    }

    @JsonProperty("EndTime")
    public void setEndTime(Object endTime) {
        this.endTime = endTime;
    }

    @JsonProperty("SalaryAmount")
    public Long getSalaryAmount() {
        return salaryAmount;
    }

    @JsonProperty("SalaryAmount")
    public void setSalaryAmount(Long salaryAmount) {
        this.salaryAmount = salaryAmount;
    }

    @JsonProperty("SalaryBasisId")
    public Long getSalaryBasisId() {
        return salaryBasisId;
    }

    @JsonProperty("SalaryBasisId")
    public void setSalaryBasisId(Long salaryBasisId) {
        this.salaryBasisId = salaryBasisId;
    }

    @JsonProperty("ActionCode")
    public String getActionCode() {
        return actionCode;
    }

    @JsonProperty("ActionCode")
    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    @JsonProperty("ActionReasonCode")
    public String getActionReasonCode() {
        return actionReasonCode;
    }

    @JsonProperty("ActionReasonCode")
    public void setActionReasonCode(String actionReasonCode) {
        this.actionReasonCode = actionReasonCode;
    }

    @JsonProperty("AssignmentStatus")
    public String getAssignmentStatus() {
        return assignmentStatus;
    }

    @JsonProperty("AssignmentStatus")
    public void setAssignmentStatus(String assignmentStatus) {
        this.assignmentStatus = assignmentStatus;
    }

    @JsonProperty("WorkTaxAddressId")
    public Object getWorkTaxAddressId() {
        return workTaxAddressId;
    }

    @JsonProperty("WorkTaxAddressId")
    public void setWorkTaxAddressId(Object workTaxAddressId) {
        this.workTaxAddressId = workTaxAddressId;
    }

    @JsonProperty("AssignmentId")
    public Long getAssignmentId() {
        return assignmentId;
    }

    @JsonProperty("AssignmentId")
    public void setAssignmentId(Long assignmentId) {
        this.assignmentId = assignmentId;
    }

    @JsonProperty("EffectiveStartDate")
    public String getEffectiveStartDate() {
        return effectiveStartDate;
    }

    @JsonProperty("EffectiveStartDate")
    public void setEffectiveStartDate(String effectiveStartDate) {
        this.effectiveStartDate = effectiveStartDate;
    }

    @JsonProperty("EffectiveEndDate")
    public String getEffectiveEndDate() {
        return effectiveEndDate;
    }

    @JsonProperty("EffectiveEndDate")
    public void setEffectiveEndDate(String effectiveEndDate) {
        this.effectiveEndDate = effectiveEndDate;
    }

    @JsonProperty("PositionId")
    public Long getPositionId() {
        return positionId;
    }

    @JsonProperty("PositionId")
    public void setPositionId(Long positionId) {
        this.positionId = positionId;
    }

    @JsonProperty("TermsEffectiveStartDate")
    public String getTermsEffectiveStartDate() {
        return termsEffectiveStartDate;
    }

    @JsonProperty("TermsEffectiveStartDate")
    public void setTermsEffectiveStartDate(String termsEffectiveStartDate) {
        this.termsEffectiveStartDate = termsEffectiveStartDate;
    }

    @JsonProperty("ManagerId")
    public Object getManagerId() {
        return managerId;
    }

    @JsonProperty("ManagerId")
    public void setManagerId(Object managerId) {
        this.managerId = managerId;
    }

    @JsonProperty("ManagerAssignmentId")
    public Object getManagerAssignmentId() {
        return managerAssignmentId;
    }

    @JsonProperty("ManagerAssignmentId")
    public void setManagerAssignmentId(Object managerAssignmentId) {
        this.managerAssignmentId = managerAssignmentId;
    }

    @JsonProperty("ManagerType")
    public Object getManagerType() {
        return managerType;
    }

    @JsonProperty("ManagerType")
    public void setManagerType(Object managerType) {
        this.managerType = managerType;
    }

    @JsonProperty("AssignmentNumber")
    public String getAssignmentNumber() {
        return assignmentNumber;
    }

    @JsonProperty("AssignmentNumber")
    public void setAssignmentNumber(String assignmentNumber) {
        this.assignmentNumber = assignmentNumber;
    }

    @JsonProperty("OriginalHireDate")
    public Object getOriginalHireDate() {
        return originalHireDate;
    }

    @JsonProperty("OriginalHireDate")
    public void setOriginalHireDate(Object originalHireDate) {
        this.originalHireDate = originalHireDate;
    }

    @JsonProperty("AssignmentStatusTypeId")
    public Long getAssignmentStatusTypeId() {
        return assignmentStatusTypeId;
    }

    @JsonProperty("AssignmentStatusTypeId")
    public void setAssignmentStatusTypeId(Long assignmentStatusTypeId) {
        this.assignmentStatusTypeId = assignmentStatusTypeId;
    }

    @JsonProperty("PrimaryAssignmentFlag")
    public Boolean getPrimaryAssignmentFlag() {
        return primaryAssignmentFlag;
    }

    @JsonProperty("PrimaryAssignmentFlag")
    public void setPrimaryAssignmentFlag(Boolean primaryAssignmentFlag) {
        this.primaryAssignmentFlag = primaryAssignmentFlag;
    }

    @JsonProperty("ProbationPeriodEndDate")
    public Object getProbationPeriodEndDate() {
        return probationPeriodEndDate;
    }

    @JsonProperty("ProbationPeriodEndDate")
    public void setProbationPeriodEndDate(Object probationPeriodEndDate) {
        this.probationPeriodEndDate = probationPeriodEndDate;
    }

    @JsonProperty("ProbationPeriodLength")
    public Object getProbationPeriodLength() {
        return probationPeriodLength;
    }

    @JsonProperty("ProbationPeriodLength")
    public void setProbationPeriodLength(Object probationPeriodLength) {
        this.probationPeriodLength = probationPeriodLength;
    }

    @JsonProperty("ProbationPeriodUnitOfMeasure")
    public Object getProbationPeriodUnitOfMeasure() {
        return probationPeriodUnitOfMeasure;
    }

    @JsonProperty("ProbationPeriodUnitOfMeasure")
    public void setProbationPeriodUnitOfMeasure(Object probationPeriodUnitOfMeasure) {
        this.probationPeriodUnitOfMeasure = probationPeriodUnitOfMeasure;
    }

    @JsonProperty("AssignmentProjectedEndDate")
    public Object getAssignmentProjectedEndDate() {
        return assignmentProjectedEndDate;
    }

    @JsonProperty("AssignmentProjectedEndDate")
    public void setAssignmentProjectedEndDate(Object assignmentProjectedEndDate) {
        this.assignmentProjectedEndDate = assignmentProjectedEndDate;
    }

    @JsonProperty("ActualTerminationDate")
    public Object getActualTerminationDate() {
        return actualTerminationDate;
    }

    @JsonProperty("ActualTerminationDate")
    public void setActualTerminationDate(Object actualTerminationDate) {
        this.actualTerminationDate = actualTerminationDate;
    }

    @JsonProperty("LegalEntityId")
    public Long getLegalEntityId() {
        return legalEntityId;
    }

    @JsonProperty("LegalEntityId")
    public void setLegalEntityId(Long legalEntityId) {
        this.legalEntityId = legalEntityId;
    }

    @JsonProperty("PrimaryWorkRelationFlag")
    public Boolean getPrimaryWorkRelationFlag() {
        return primaryWorkRelationFlag;
    }

    @JsonProperty("PrimaryWorkRelationFlag")
    public void setPrimaryWorkRelationFlag(Boolean primaryWorkRelationFlag) {
        this.primaryWorkRelationFlag = primaryWorkRelationFlag;
    }

    @JsonProperty("PrimaryWorkTermsFlag")
    public Boolean getPrimaryWorkTermsFlag() {
        return primaryWorkTermsFlag;
    }

    @JsonProperty("PrimaryWorkTermsFlag")
    public void setPrimaryWorkTermsFlag(Boolean primaryWorkTermsFlag) {
        this.primaryWorkTermsFlag = primaryWorkTermsFlag;
    }

    @JsonProperty("CreationDate")
    public String getCreationDate() {
        return creationDate;
    }

    @JsonProperty("CreationDate")
    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    @JsonProperty("LastUpdateDate")
    public String getLastUpdateDate() {
        return lastUpdateDate;
    }

    @JsonProperty("LastUpdateDate")
    public void setLastUpdateDate(String lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    @JsonProperty("PeriodOfServiceId")
    public Long getPeriodOfServiceId() {
        return periodOfServiceId;
    }

    @JsonProperty("PeriodOfServiceId")
    public void setPeriodOfServiceId(Long periodOfServiceId) {
        this.periodOfServiceId = periodOfServiceId;
    }

    @JsonProperty("FullPartTime")
    public String getFullPartTime() {
        return fullPartTime;
    }

    @JsonProperty("FullPartTime")
    public void setFullPartTime(String fullPartTime) {
        this.fullPartTime = fullPartTime;
    }

    @JsonProperty("RegularTemporary")
    public String getRegularTemporary() {
        return regularTemporary;
    }

    @JsonProperty("RegularTemporary")
    public void setRegularTemporary(String regularTemporary) {
        this.regularTemporary = regularTemporary;
    }

    @JsonProperty("GradeLadderId")
    public Object getGradeLadderId() {
        return gradeLadderId;
    }

    @JsonProperty("GradeLadderId")
    public void setGradeLadderId(Object gradeLadderId) {
        this.gradeLadderId = gradeLadderId;
    }

    @JsonProperty("peopleGroupKeyFlexfield")
    public List<Object> getPeopleGroupKeyFlexfield() {
        return peopleGroupKeyFlexfield;
    }

    @JsonProperty("peopleGroupKeyFlexfield")
    public void setPeopleGroupKeyFlexfield(List<Object> peopleGroupKeyFlexfield) {
        this.peopleGroupKeyFlexfield = peopleGroupKeyFlexfield;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
