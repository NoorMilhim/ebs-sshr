package com.appspro.fusionsshr.bean;

public class LogsBean {


    private String userId;
    private String dated;
    private String logger;
    private String level;
    private String message;


    public LogsBean() {
        super();
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setDated(String dated) {
        this.dated = dated;
    }

    public String getDated() {
        return dated;
    }

    public void setLogger(String logger) {
        this.logger = logger;
    }

    public String getLogger() {
        return logger;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getLevel() {
        return level;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
