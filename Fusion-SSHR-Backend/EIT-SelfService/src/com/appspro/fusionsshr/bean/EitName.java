package com.appspro.fusionsshr.bean;

public class EitName {
    private String DESCRIPTIVE_FLEX_CONTEXT_CODE ; 
    private String DESCRIPTIVE_FLEX_CONTEXT_NAME ; 
    
    public EitName() {
        super();
    }

    public void setDESCRIPTIVE_FLEX_CONTEXT_CODE(String DESCRIPTIVE_FLEX_CONTEXT_CODE) {
        this.DESCRIPTIVE_FLEX_CONTEXT_CODE = DESCRIPTIVE_FLEX_CONTEXT_CODE;
    }

    public String getDESCRIPTIVE_FLEX_CONTEXT_CODE() {
        return DESCRIPTIVE_FLEX_CONTEXT_CODE;
    }

    public void setDESCRIPTIVE_FLEX_CONTEXT_NAME(String DESCRIPTIVE_FLEX_CONTEXT_NAME) {
        this.DESCRIPTIVE_FLEX_CONTEXT_NAME = DESCRIPTIVE_FLEX_CONTEXT_NAME;
    }

    public String getDESCRIPTIVE_FLEX_CONTEXT_NAME() {
        return DESCRIPTIVE_FLEX_CONTEXT_NAME;
    }
}
