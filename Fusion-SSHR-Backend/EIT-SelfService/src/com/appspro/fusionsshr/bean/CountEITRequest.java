package com.appspro.fusionsshr.bean;

public class CountEITRequest {
    
    private String countPendingApproved;
    private String countApproved;
    private String countRejected;


    public void setCountPendingApproved(String countPendingApproved) {
        this.countPendingApproved = countPendingApproved;
    }

    public String getCountPendingApproved() {
        return countPendingApproved;
    }

    public void setCountApproved(String countApproved) {
        this.countApproved = countApproved;
    }

    public String getCountApproved() {
        return countApproved;
    }

    public void setCountRejected(String countRejected) {
        this.countRejected = countRejected;
    }

    public String getCountRejected() {
        return countRejected;
    }
}
