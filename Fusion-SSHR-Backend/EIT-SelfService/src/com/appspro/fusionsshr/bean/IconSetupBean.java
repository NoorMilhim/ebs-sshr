package com.appspro.fusionsshr.bean;

public class IconSetupBean {
    int ID;
    String EITCODE;
    String NAME;
    String ICONS;

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getID() {
        return ID;
    }

    public void setEITCODE(String EITCODE) {
        this.EITCODE = EITCODE;
    }

    public String getEITCODE() {
        return EITCODE;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getNAME() {
        return NAME;
    }

    public void setICONS(String ICONS) {
        this.ICONS = ICONS;
    }

    public String getICONS() {
        return ICONS;
    }
}
