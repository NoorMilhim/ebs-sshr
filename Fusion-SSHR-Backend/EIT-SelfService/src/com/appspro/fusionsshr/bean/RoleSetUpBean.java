package com.appspro.fusionsshr.bean;

import java.util.ArrayList;

public class RoleSetUpBean {
  
  private String id ;
  private String roleName;
  private String roleDescription;
  private String roleIdForegin;
  private  ArrayList<String>  eitCode = new ArrayList();//to avoid null pointer exception
  private String roleSetup_ID;
  private String roleId;
  private String eitCodes;





    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleDescription(String roleDescription) {
        this.roleDescription = roleDescription;
    }

    public String getRoleDescription() {
        return roleDescription;
    }

    public void setRoleIdForegin(String roleIdForegin) {
        this.roleIdForegin = roleIdForegin;
    }

    public String getRoleIdForegin() {
        return roleIdForegin;
    }



    public void setRoleSetup_ID(String roleSetup_ID) {
        this.roleSetup_ID = roleSetup_ID;
    }

    public String getRoleSetup_ID() {
        return roleSetup_ID;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setEitCode(ArrayList<String> eitCode) {
        this.eitCode = eitCode;
    }

    public ArrayList<String> getEitCode() {
        return eitCode;
    }

    public void setEitCodes(String eitCodes) {
        this.eitCodes = eitCodes;
    }

    public String getEitCodes() {
        return eitCodes;
    }
}
