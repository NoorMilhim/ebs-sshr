/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.bean;

/**
 *
 * @author lenovo
 */
public class BulkTransactionBean {

    private String id;
    private String createBy;
    private String createDate;
    private String personExtraInformationID;
    private int transactionNumber;

    public int getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(int transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public BulkTransactionBean(String id, String createBy, String createDate, String personExtraInformationID) {
        this.id = id;
        this.createBy = createBy;
        this.createDate = createDate;
        this.personExtraInformationID = personExtraInformationID;
    }

    public BulkTransactionBean() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getPersonExtraInformationID() {
        return personExtraInformationID;
    }

    public void setPersonExtraInformationID(String personExtraInformationID) {
        this.personExtraInformationID = personExtraInformationID;
    }

    @Override
    public String toString() {
        return "BulkTransactionBean{" + "id=" + id + ", createBy=" + createBy + ", createDate=" + createDate + ", personExtraInformationID=" + personExtraInformationID + '}';
    }

}
