package com.appspro.fusionsshr.bean;

public class CommitmentAndDisciplineBean {
  
    private String id;
    private String parameterAR;
    private String paramterEn;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setParameterAR(String parameterAR) {
        this.parameterAR = parameterAR;
    }

    public String getParameterAR() {
        return parameterAR;
    }

    public void setParamterEn(String paramterEn) {
        this.paramterEn = paramterEn;
    }

    public String getParamterEn() {
        return paramterEn;
    }
}
