/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.bean;

/**
 *
 * @author user
 */
public class SaaSPositionBean {

    private String positionId;
    private String name;

    public void setPositionId(String positionId) {
        this.positionId = positionId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPositionId() {
        return positionId;
    }

    public String getName() {
        return name;
    }

}
