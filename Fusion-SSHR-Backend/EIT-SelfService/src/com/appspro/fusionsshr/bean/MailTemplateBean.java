package com.appspro.fusionsshr.bean;

public class MailTemplateBean {
    private int templateId;
    private String templateName;
    private String templateLanguage;
    private String templateRequestName;
    private String templateEmailSubject;
    private String templateStatus;

    public MailTemplateBean() {

    }

    public MailTemplateBean(int id, String templateName,
                            String templateLanguage,
                            String templateRequestName,
                            String templateEmailSubject,
                            String templateStatus) {
        this.templateId = id;
        this.templateName = templateName;
        this.templateLanguage = templateLanguage;
        this.templateRequestName = templateRequestName;
        this.templateEmailSubject = templateEmailSubject;
        this.templateStatus = templateStatus;
    }


    public void setId(int id) {
        this.templateId = id;
    }

    public int getId() {
        return templateId;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateLanguage(String templateLanguage) {
        this.templateLanguage = templateLanguage;
    }

    public String getTemplateLanguage() {
        return templateLanguage;
    }

    public void setTemplateRequestName(String templateRequestName) {
        this.templateRequestName = templateRequestName;
    }

    public String getTemplateRequestName() {
        return templateRequestName;
    }

    public void setTemplateEmailSubject(String templateEmailSubject) {
        this.templateEmailSubject = templateEmailSubject;
    }

    public String getTemplateEmailSubject() {
        return templateEmailSubject;
    }

    public void setTemplateStatus(String templateStatus) {
        this.templateStatus = templateStatus;
    }

    public String getTemplateStatus() {
        return templateStatus;
    }

    public String toString() {
        return String.format("{id:{0},name:{1},language:{2},requestName:{3},subject:{4},status:{5}",
                             templateId, templateName, templateLanguage,
                             templateRequestName, templateEmailSubject,
                             templateStatus);
    }
}
