package com.appspro.fusionsshr.bean;

public class roleSetupValidationBean {
    private String  id;
    private String  rolename;
    private String  roleid;
    private String  personInfromation;
    private String  operation;
    private String  staticValue;
    private String eitCode;



    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

    public String getRolename() {
        return rolename;
    }

    public void setRoleid(String roleid) {
        this.roleid = roleid;
    }

    public String getRoleid() {
        return roleid;
    }

    public void setPersonInfromation(String personInfromation) {
        this.personInfromation = personInfromation;
    }

    public String getPersonInfromation() {
        return personInfromation;
    }

    public void setStaticValue(String staticValue) {
        this.staticValue = staticValue;
    }

    public String getStaticValue() {
        return staticValue;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getOperation() {
        return operation;
    }

    public void setEitCode(String eitCode) {
        this.eitCode = eitCode;
    }

    public String getEitCode() {
        return eitCode;
    }
}
