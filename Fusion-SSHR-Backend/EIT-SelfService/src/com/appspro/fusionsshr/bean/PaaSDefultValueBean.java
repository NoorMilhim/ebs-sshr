/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.bean;

/**
 *
 * @author Lenovo
 */
public class PaaSDefultValueBean {

    private String id;
    private String eitCode;
    private String segmentName;
    private String query;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEitCode() {
        return eitCode;
    }

    public void setEitCode(String eitCode) {
        this.eitCode = eitCode;
    }

    public String getSegmentName() {
        return segmentName;
    }

    public void setSegmentName(String segmentName) {
        this.segmentName = segmentName;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    @Override
    public String toString() {
        return "PaaSDefultValueBean{" + "id=" + id + ", eitCode=" + eitCode + ", segmentName=" + segmentName + ", query=" + query + '}';
    }

}
