package com.appspro.fusionsshr.bean;

public class LocationEmpBean {

    private String type;
    private String Datee;
  
    private String personId;
    private String personNumber;
    private String locationName;
    private String startDate;
    private String endDate;
    
    
    

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }


    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonNumber(String personNumber) {
        this.personNumber = personNumber;
    }

    public String getPersonNumber() {
        return personNumber;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setDatee(String Datee) {
        this.Datee = Datee;
    }

    public String getDatee() {
        return Datee;
    }
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartDate() {
        return startDate;
    }
}

