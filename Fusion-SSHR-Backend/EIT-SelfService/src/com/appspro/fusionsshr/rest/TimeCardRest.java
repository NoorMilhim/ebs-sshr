package com.appspro.fusionsshr.rest;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.dao.TimeCardDao;

import common.biPReports.BIReportModel;

import java.io.IOException;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("timeCard")
public class TimeCardRest {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response get() {
        JSONArray res = new JSONArray();
        try {
            res = TimeCardDao.getInstance().getLOV();
            if (res != null)
                return Response.ok(res.toString()).build();
        } catch (IOException e) {
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        }
        return Response.ok(res.toString()).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response search(String payload) {

        Map<String, String> paramMAp =
            TimeCardDao.toMap(new JSONObject(payload));

        JSONObject res =
            BIReportModel.getInstance().runReport("Incomplete And In Error Time Cards Report",
                                                  paramMAp);

        System.out.println("DELETE CARDS SEARCH:" + paramMAp);
        if (res != null && res.has("DATA_DS") &&
            res.getJSONObject("DATA_DS").has("G_1")) {
            try {
                System.out.println("DELETE CARDS SEARCH RES:" +
                                   res.getJSONObject("DATA_DS").getJSONArray("G_1").length());
                return Response.ok(res.getJSONObject("DATA_DS").getJSONArray("G_1").toString()).build();
            } catch (Exception ignored) {
                System.out.println("DELETE CARDS SEARCH RES:" + 1);
                return Response.ok(res.getJSONObject("DATA_DS").getJSONObject("G_1").toString()).build();
            }
        } else {
            System.out.println("DELETE CARDS SEARCH RES:" + 0);
            return Response.ok(new JSONArray().toString()).build();
        }
    }

    @POST
    @Path("/del")
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response del(final String ids) {
        final JSONObject idsArray = new JSONObject(ids);
        JSONObject response = new JSONObject();
        boolean async = true;
        if (async) {
            response.put("status", "Info");
            if (TimeCardDao.IS_DELETING) {//bydefualt false
                response.put("message",
                             "{\"ar\":\"???? ?????? ??? ??? ????????, ???? ???????? ??????.\",\"en\":\"An request already processing, please try later!\"}");
                return Response.ok(response.toString()).build();
            } else {//
                new Thread(new Runnable() {
                        @Override
                        public void run() {
                            TimeCardDao.IS_DELETING = true;
                            //System.out.println("Handling request");
                            TimeCardDao.getInstance().deleteCards(idsArray);
//                            try {
//                                Thread.sleep(60000);
//                            } catch (InterruptedException e) {
//                            }
                            TimeCardDao.IS_DELETING = false;
                        }
                    }).start();
                
                
                response.put("message",
                             "{\"ar\":\"?? ????? ????? ?????, ???? ???????.\",\"en\":\"Your request has been sent successfully, rquest is being processing\"}");
                return Response.ok(response.toString()).build();
            }
        } else {
             return Response.ok(response.toString()).build();
        }
    }
}
