package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.bean.ApprovalConditionBean;
import com.appspro.fusionsshr.bean.ApprovalSetupBean;

import com.appspro.fusionsshr.bean.LocationEmpBean;

import com.appspro.fusionsshr.dao.ApprovalSetupDAO;

import com.appspro.fusionsshr.dao.LocationEmpDAO;

import com.appspro.fusionsshr.dao.OTLApplicationDAO;

import com.fasterxml.jackson.databind.ObjectMapper;

import common.restHelper.RestHelper;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;


@Path("/locationEmp")
public class LocationEmpRest extends RestHelper{
    
    
    LocationEmpDAO service = new LocationEmpDAO();
    OTLApplicationDAO otlObj=new OTLApplicationDAO();
    
    @GET
    @Path("/getAll")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getAll() {
        ArrayList<LocationEmpBean> list = new ArrayList<LocationEmpBean>();
        list = service.getData();
        return new JSONArray(list).toString();
    
    }
    
    
    @POST
    @Path("/Add/{transactionType}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertlocationEmpist(String body ,@PathParam("transactionType") String transactionType) {
    
        LocationEmpBean list = new LocationEmpBean();
        LocationEmpBean bean= new LocationEmpBean();
        try {
            if(transactionType.equals("personNumber")){
          
                JSONObject jsonObj=new JSONObject(body);
                String date=jsonObj.get("datee").toString();
                String personId=jsonObj.get("personId").toString();
                String type=jsonObj.get("type").toString();
                String personNumberArr=jsonObj.get("personNumberArr").toString();
                
                bean.setDatee(date);
                bean.setPersonId(personId);
                bean.setType(type);
                bean.setPersonNumber(personNumberArr);
                
                list = service.insertlocationEmpList(bean);
                otlObj.timeCardBypersonNumber(body);
                return new JSONObject(list).toString();
               
            }else if(transactionType.equals("location")){
                ObjectMapper mapper = new ObjectMapper();
                 bean =
                    mapper.readValue(body, LocationEmpBean.class);

                list = service.insertlocationEmpList(bean);
                otlObj.timeCardByLocation(body);
          
            }
            return new JSONObject(list).toString();
        } catch (Exception e) {
            return new JSONObject(list).toString();
        }
    }
    
    
    
    
    @POST
    @Path("/Edit")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateApprovalCondition(String body) {

        LocationEmpBean list = new LocationEmpBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            LocationEmpBean bean =
                mapper.readValue(body, LocationEmpBean.class);
            list = service.updateLocationEmp(bean);
            return new JSONObject(list).toString();
        } catch (Exception e) {
            return new JSONObject(list).toString();
        }
    }
    
    
    
    @GET
    @Path("delete/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String deleteLocationEmp(@PathParam("id")
        String body) {
        String status = "";

        status = service.deleteLocationEmp(body);
        return status;
    }
    
}
