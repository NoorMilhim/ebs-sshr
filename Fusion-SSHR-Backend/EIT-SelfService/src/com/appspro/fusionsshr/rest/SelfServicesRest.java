package com.appspro.fusionsshr.rest;
import com.appspro.fusionsshr.bean.SelfServiceBean;
import com.appspro.fusionsshr.dao.SelfServiceDAO;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/selfService")
public class SelfServicesRest {
    
    SelfServiceDAO service = new SelfServiceDAO();
    @POST
       @Path("/{transactionType}")
       @Consumes(MediaType.APPLICATION_JSON)
       @Produces(MediaType.APPLICATION_JSON)
       public String insertOrUpdateApprovalRecruitmentRequest(String body,
                                                      @PathParam("transactionType")
           String transactionType) {
           SelfServiceBean list = new SelfServiceBean();
           try {
               ObjectMapper mapper = new ObjectMapper();
               SelfServiceBean bean = mapper.readValue(body, SelfServiceBean.class);
                return service.approvalpropation(bean);
           } catch (Exception e) {
               //
           }
           return "";
       }
    
    
    
    
}
