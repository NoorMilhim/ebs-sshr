/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.bean.ApprovalSetupBean;
import com.appspro.fusionsshr.bean.AttachmentBean;
import com.appspro.fusionsshr.dao.AttachmentDAO;
import com.appspro.fusionsshr.dao.EitsDAO;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author CPBSLV
 */
@Path("/attachment")
public class Attachment {

    AttachmentDAO service = new AttachmentDAO();
    EitsDAO E_Serivce = new EitsDAO();

    @POST
    @Path("/ADD")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertAttachment(String body, @Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {
        
        AttachmentBean list = new AttachmentBean();
        try {
            list = service.InsertXXX_Attachment(body);
            return  new  JSONObject(list).toString();
        } catch (Exception e) {
            return new  JSONObject(list).toString();
        }
    }

    @POST
    @Path("/EDIT")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateAttachment(String body, @Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {

            AttachmentBean list = new AttachmentBean();
            try {
                list = service.UpdateXXX_Attachment(body);
                return  new  JSONObject(list).toString();
            } catch (Exception e) {
        return new  JSONObject(list).toString();
        }
    }
    @POST
    @Path("/draft")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateDraftAttachment(String body, @Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {

            AttachmentBean list = new AttachmentBean();
            try {
                list = service.UpdateXXX_Attachment(body);
                return  new  JSONObject(list).toString();
            } catch (Exception e) {
        return new  JSONObject(list).toString();
        }
    }

    @GET
    @Path("/{ss_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String allAttachment(@PathParam("ss_id") String ss_id, @Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {
        ArrayList<AttachmentBean> list = new ArrayList<AttachmentBean>();
        list = service.AllXXX_Attachment(ss_id);
        
        return new     JSONArray(list).toString();
    }
}
