/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.bean.JobBean;
import com.appspro.fusionsshr.bean.ListResponse;
import com.appspro.fusionsshr.bean.PaaSDefultValueBean;
import com.appspro.fusionsshr.dao.PaaSDefultValueDao;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.json.JSONArray;
import javax.ws.rs.core.Response;
/**
 *
 * @author Lenovo
 */
@Path("/paasdefultvalue")
public class PaaSDefultValueService {

    PaaSDefultValueDao service = new PaaSDefultValueDao();

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
  //  @Produces(MediaType.APPLICATION_JSON)
    public String getAllJobs(@Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {
        ArrayList<PaaSDefultValueBean> list = new ArrayList<PaaSDefultValueBean>();
    
      //  List list = new ArrayList();
        list = service.getAllPaaSDefultValue();
        //(new JSONArray(list));      
        return  new  JSONArray(list).toString();
     //  return Response.status(200).entity( new JSONArray.toString()).build();
    }

}
