/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.bean.NotificationBean;
import com.appspro.fusionsshr.bean.WorkFlowNotificationBean;
import com.appspro.fusionsshr.dao.WorkflowNotificationDAO;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONArray;
import org.json.JSONObject;

import utils.system;

/**
 *
 * @author user
 */
@Path("/workflowApproval")
public class WorkflowNotificationRest {

    @POST
    @Path("/details")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public JSONArray getEFFDetails(String bean, @Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {
        JSONObject jsonObj = new JSONObject(bean);
        ////(jsonObj.getString("position"));
        String MANAGER_ID = jsonObj.getString("MANAGER_ID");
        String managerOfManager = jsonObj.getString("managerOfManager");
        String position = jsonObj.getString("position");
        String emp = jsonObj.getString("emp");
        String self_type = jsonObj.getString("self_type");
        String request_id = jsonObj.getString("request_id");
        String type = jsonObj.getString("type");
        WorkflowNotificationDAO det = new WorkflowNotificationDAO();
        JSONArray EFFLinks = new JSONArray();

        EFFLinks = det.getWorkFlowNotification(MANAGER_ID, managerOfManager, String.valueOf(position), emp, self_type, request_id, type);
        return EFFLinks;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String worwflowApproval(String bean, @Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {
        JSONObject jsonObj = new JSONObject(bean);

        WorkflowNotificationDAO det = new WorkflowNotificationDAO();

        det.workflowAction(bean);
        return "";

    }

    @POST
    @Path("/notify/")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<WorkFlowNotificationBean> getNotifi(@FormParam("self_type") String self_type, @FormParam("request_id") String request_id, @FormParam("type") String type, @FormParam("receiver_id") String receiver_id, @Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {

        ArrayList<WorkFlowNotificationBean> list = new ArrayList<WorkFlowNotificationBean>();
        WorkflowNotificationDAO det = new WorkflowNotificationDAO();

        list = det.getWorkFlowNotificationByFilter(self_type, request_id, type, receiver_id);
        return list;
        //generalGroup
    }

    @POST
    @Path("/allNotifications")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllNotificationsDetails(String bean, @Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {
        JSONObject jsonObj = new JSONObject(bean);
        ////(jsonObj.getString("position"));
        String MANAGER_ID = jsonObj.getString("MANAGER_ID");
        String managerOfManager = jsonObj.getString("managerOfManager");
        String position = jsonObj.getString("position");
        String emp = jsonObj.getString("emp");

        
        List<String> rolesIdArr = new ArrayList<String>();
        
        for(int i=0; i<jsonObj.getJSONArray("userRoles").length(); i++){ 

            String roleId = jsonObj.getJSONArray("userRoles").getJSONObject(i).getNumber("roleId").toString();
            rolesIdArr.add(roleId);

        }
        ////("Test");
        ////(Arrays.toString(rolesIdArr.toArray()));


        WorkflowNotificationDAO det = new WorkflowNotificationDAO();
        ArrayList<NotificationBean> list = new ArrayList<NotificationBean>();

        list = det.getAllNotification(MANAGER_ID, managerOfManager, position, emp, rolesIdArr);

        return  new  JSONArray(list).toString();
        // return  Response.ok(jsonRes, MediaType.APPLICATION_JSON).build();
    }

    //*************************************************************************
    @POST
    @Path("/searchnotification")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public String getSearchNotification(@FormParam("MANAGER_ID") String manager_Id, @FormParam("managerOfManager") String managerOfManager, @FormParam("position") String position, @FormParam("emp") String emp, @FormParam("SELF_TYPE") String self_type, @FormParam("personNumber") String personNumber,@FormParam("REQUEST_ID") String request_id, @FormParam("TYPE") String type , @FormParam("nationalIdentity") String nationalIdentity,@FormParam("STATUS") String status){
        ArrayList<NotificationBean> notificationList = new ArrayList<NotificationBean>();
        WorkflowNotificationDAO det = new WorkflowNotificationDAO();

        notificationList = det.getSearchWorkFlowNotification(manager_Id, managerOfManager, position, emp, self_type, personNumber, request_id,type,nationalIdentity,status);
       // return notificationList;
        return  new  JSONArray(notificationList).toString();
        
    }
}
