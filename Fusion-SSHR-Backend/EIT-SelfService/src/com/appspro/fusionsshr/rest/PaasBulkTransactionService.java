/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;


import com.appspro.fusionsshr.bean.BulkTransactionBean;
import com.appspro.fusionsshr.bean.PeopleExtraInformationBean;
import com.appspro.fusionsshr.dao.PaasBulkTransactionDAO;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;


/**
 *
 * @author lenovo
 */
@Path("PassBulkTransaction")
public class PaasBulkTransactionService {

    PaasBulkTransactionDAO service = new PaasBulkTransactionDAO();

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response insertBulkTransaction(BulkTransactionBean BulkBean,
                                          @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {

        try {
            return Response.ok(service.addBulkTransaction(BulkBean),
                               MediaType.APPLICATION_JSON).build();
        } catch (Exception e) {
            return Response.status(500).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("/{eit_code}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getSummaryBulkTransaction(@PathParam("eit_code")
        String eitCode, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        ArrayList<PeopleExtraInformationBean> list = new ArrayList<PeopleExtraInformationBean>();
        
        list =  service.getSummaryBulkTransaction(eitCode);
        return new JSONArray(list).toString();
    }

    @GET
    @Path("getBulkTransaction/{transactionNumber}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getSummaryBulkTransactionById(@PathParam("transactionNumber")
        String transactionNumber, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        ArrayList<PeopleExtraInformationBean> list = new ArrayList<PeopleExtraInformationBean>();
        list =  service.getSummaryBulkTransactionBYTransactionNumber(transactionNumber);
        return  new JSONArray(list).toString();
    }

    @PUT
    @Path("/updateStatus")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBulkTransactionStatus(PeopleExtraInformationBean informationBean,
                                             @PathParam("transactionType")
        String transactionType, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {

        try {
            return Response.ok(service.updateStatusBulkTrasnsaction(informationBean),
                               MediaType.APPLICATION_JSON).build();
        } catch (Exception e) {
            return Response.status(500).entity(e.getMessage()).build();
        }
    }
}
