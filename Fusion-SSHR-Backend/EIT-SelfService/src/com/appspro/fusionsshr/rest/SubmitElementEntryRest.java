/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;

import com.appspro.db.AppsproConnection;
import com.appspro.db.CommonConfigReader;
import com.appspro.fusionsshr.bean.TrackServerRequestBean;
import com.appspro.fusionsshr.dao.EFFDetails;
import com.appspro.fusionsshr.dao.ElementEntryHelper;

import com.bea.core.repackaged.apache.bcel.classfile.ExceptionTable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.text.SimpleDateFormat;



import javax.servlet.http.HttpServletRequest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import javax.ws.rs.PUT;
import javax.ws.rs.core.Response;

import org.joda.time.LocalDate;
import java.util.Date;
/**
 *
 * @author Shadi Mansi-PC
 */
@Path("/upload")
public class SubmitElementEntryRest extends ElementEntryHelper {

    public SubmitElementEntryRest() {
        super();
    }

    @POST
    @Path("/elementEntry")
    @Consumes(MediaType.APPLICATION_JSON)
    public String upload(InputStream incomingData, @Context
        HttpServletRequest request) {
        StringBuilder crunchifyBuilder = new StringBuilder();
        String respone = null;
        
        JSONObject jObject;
        JSONObject jObjectRespones = null;

        try {
            BufferedReader in =
                new BufferedReader(new InputStreamReader(incomingData));
            String line = null;
            while ((line = in.readLine()) != null) {
                crunchifyBuilder.append(line);
            }
            jObject = new JSONObject(crunchifyBuilder.toString());

        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            return "error";
        }

        try {
            HttpSession session = request.getSession();
            ServletContext context = session.getServletContext();
            String path = context.getRealPath(request.getContextPath());

            if (null != path && path.contains("HHA-SSHR-BackEnd")) {
                path = path.split("HHA-SSHR-BackEnd")[0] + "/images";
            }
            if (null != path && path.contains("FFusion-SSHR-Backend-Jdev")) {
                path = path.split("Fusion-SSHR-Backend-Jdev")[0] + "/images";
            } else {
                path = System.getProperty("java.scratch.dir");
            }

            respone = createHelperBT(path, jObject, null);
        } catch (JSONException e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);

            TrackServerRequestBean bean = new TrackServerRequestBean();
            bean.setRequestId(jObject.get("trsId").toString());
            bean.setElementResponseDate(TrackServerRequestBean.getCurrentDateTime());
            bean.setElementResponseCode(500 + "");
            bean.setElementResponseDesc(e.getMessage());
            EFFDetails.getInstance().editElementResponseDetails(bean);
        }

        return respone;
    }


    @POST
    @Path("/createBankFile")
    @Consumes(MediaType.APPLICATION_JSON)
    public String uploadCreateBankFile(InputStream incomingData, @Context
        HttpServletRequest request) {
        StringBuilder crunchifyBuilder = new StringBuilder();
        String respone = null;
        try {
            BufferedReader in =
                new BufferedReader(new InputStreamReader(incomingData));
            String line = null;
            while ((line = in.readLine()) != null) {
                crunchifyBuilder.append(line);
            }
        } catch (Exception e) {
            //("Error Parsing: - ");
        }
        JSONObject jObject;
        JSONObject jObjectRespones = null;
        try {
            jObject = new JSONObject(crunchifyBuilder.toString());
            HttpSession session = request.getSession();
            ServletContext context = session.getServletContext();
            String path = context.getRealPath(request.getContextPath());

            if (null != path && path.contains("HHA-SSHR-BackEnd")) {
                path = path.split("HHA-SSHR-BackEnd")[0] + "/images";
            }
            if (null != path && path.contains("FFusion-SSHR-Backend-Jdev")) {
                path = path.split("Fusion-SSHR-Backend-Jdev")[0] + "/images";
            } else {
                path = System.getProperty("java.scratch.dir");
            }


            respone = createHelperBankFile(path, jObject);

            jObjectRespones = new JSONObject("{'status' : '" + respone + "'}");
            //            //(jObject.toString());
        } catch (JSONException e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }

        //(respone);
        return respone;
    }

    @POST
    @Path("/createPersonalPaymentMethod")
    @Consumes(MediaType.APPLICATION_JSON)
    public String uploadCreatePersonalPaymentMethod(InputStream incomingData,
                                                    @Context
        HttpServletRequest request) {
        StringBuilder crunchifyBuilder = new StringBuilder();
        String respone = null;
        try {
            BufferedReader in =
                new BufferedReader(new InputStreamReader(incomingData));
            String line = null;
            while ((line = in.readLine()) != null) {
                crunchifyBuilder.append(line);
            }
        } catch (Exception e) {
            //("Error Parsing: - ");
        }
        JSONObject jObject;
        JSONObject jObjectRespones = null;
        try {
            jObject = new JSONObject(crunchifyBuilder.toString());
            HttpSession session = request.getSession();
            ServletContext context = session.getServletContext();
            String path = context.getRealPath(request.getContextPath());

            if (null != path && path.contains("HHA-SSHR-BackEnd")) {
                path = path.split("HHA-SSHR-BackEnd")[0] + "/images";
            }
            if (null != path && path.contains("FFusion-SSHR-Backend-Jdev")) {
                path = path.split("Fusion-SSHR-Backend-Jdev")[0] + "/images";
            } else {
                path = System.getProperty("java.scratch.dir");
            }


            respone = createHelperPersonalPaymentMethod(path, jObject);

            jObjectRespones = new JSONObject("{'status' : '" + respone + "'}");
            //            //(jObject.toString());
        } catch (JSONException e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }

        //(respone);
        return respone;
    }

    @PUT
    @Path("/elementEntry")
    @Consumes(MediaType.APPLICATION_JSON)
    public String submit(InputStream incomingData, @Context
        HttpServletRequest request) {
     
        HttpSession session = request.getSession();
        ServletContext context = session.getServletContext();
        StringBuilder crunchifyBuilder = new StringBuilder();
        String path = context.getRealPath(request.getContextPath());

        if (null != path && path.contains("HHA-SSHR-BackEnd")) {
            path = path.split("HHA-SSHR-BackEnd")[0] + "/images";
        }
        if (null != path && path.contains("FFusion-SSHR-Backend-Jdev")) {
            path = path.split("Fusion-SSHR-Backend-Jdev")[0] + "/images";
        } else {
            path = System.getProperty("java.scratch.dir");
        }

        //        try {
        //            BufferedReader in =
        //                new BufferedReader(new InputStreamReader(incomingData));
        //            String line = null;
        //            while ((line = in.readLine()) != null) {
        //                crunchifyBuilder.append(line);
        //            }
        //        } catch (Exception e) {
        //            //("Error Parsing: - ");
        //        }
        JSONArray jObject;

        try {
            //            //(crunchifyBuilder.toString().replaceAll("[\\[\\]]", ""));
            jObject =
                    new JSONArray(crunchifyBuilder.toString()); //.replaceAll("[\\[\\]]", ""));
            //            //(jObject);
            for (int i = 0; i < jObject.length() - 1; i++) {
                //                //(jObject.getJSONObject(i).getString("sstype"));
                //jObject.getJSONObject(i).getString("sstype")

                callUCM(jObject.getJSONObject(i).get("id").toString(),
                        jObject.getJSONObject(jObject.length() -
                                              1).getString("startDate"), path,
                        jObject.getJSONObject(i).getString("sstype"),
                        jObject.getJSONObject(i).getString("file_bytes"),
                        jObject.getJSONObject(i).getString("file_name"));
            }

        } catch (JSONException e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }


        return Response.ok().build().toString();
    }
    
    
    
    @POST
    @Path("/elementEntryBulk")
    @Consumes(MediaType.APPLICATION_JSON)
    public String uploadElementBulk(InputStream incomingData, @Context
        HttpServletRequest request) {
        StringBuilder crunchifyBuilder = new StringBuilder();
        String respone = null;
        
        JSONObject jObject;
        JSONObject jObjectRespones = null;

        try {
            BufferedReader in =
                new BufferedReader(new InputStreamReader(incomingData));
            String line = null;
            while ((line = in.readLine()) != null) {
                crunchifyBuilder.append(line);
            }
            jObject = new JSONObject(crunchifyBuilder.toString());

        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            return "error";
        }

        try {
            HttpSession session = request.getSession();
            ServletContext context = session.getServletContext();
            String path = context.getRealPath(request.getContextPath());

            if (null != path && path.contains("HHA-SSHR-BackEnd")) {
                path = path.split("HHA-SSHR-BackEnd")[0] + "/images";
            }
            if (null != path && path.contains("FFusion-SSHR-Backend-Jdev")) {
                path = path.split("Fusion-SSHR-Backend-Jdev")[0] + "/images";
            } else {
                path = System.getProperty("java.scratch.dir");
            }

            respone = createHelperBulkElement(path, jObject, null);
        } catch (JSONException e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);

            TrackServerRequestBean bean = new TrackServerRequestBean();
            bean.setRequestId(jObject.get("trsId").toString());
            bean.setElementResponseDate(TrackServerRequestBean.getCurrentDateTime());
            bean.setElementResponseCode(500 + "");
            bean.setElementResponseDesc(e.getMessage());
            EFFDetails.getInstance().editElementResponseDetails(bean);
        }

        return respone;
    }

}
