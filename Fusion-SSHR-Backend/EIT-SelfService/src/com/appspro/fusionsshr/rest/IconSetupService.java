package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.bean.IconSetupBean;

import com.appspro.fusionsshr.dao.EitsDAO;

import com.appspro.fusionsshr.dao.IconSetupDAO;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("/iconSetup")
public class IconSetupService {
    EitsDAO E_Serivce = new EitsDAO(); 
        IconSetupDAO service = new IconSetupDAO();


    @POST
    @Path("/EDITICONS")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateAttachment(String body, @Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {

            IconSetupBean list = new IconSetupBean();
            try {
                list = service.updateIcons(body);
                return  new  JSONObject(list).toString();
            } catch (Exception e) {
        return new  JSONObject(list).toString();
        }
    }
    @POST
    @Path("/draft")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateDraftAttachment(String body, @Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {

            IconSetupBean list = new IconSetupBean();
            try {
                list = service.updateIcons(body);
                return  new  JSONObject(list).toString();
            } catch (Exception e) {
        return new  JSONObject(list).toString();
        }
    }

    @GET
    @Path("/{EITCODE}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String allAttachment(@PathParam("EITCODE") String EITCODE, @Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {
        ArrayList<IconSetupBean> list = new ArrayList<IconSetupBean>();
        list = service.getIconsTest(EITCODE);
        
        return new     JSONArray(list).toString();
    }
    
    @POST
    @Path("/DELETE")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String deleteIcons(String body, @Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {

            IconSetupBean list = new IconSetupBean();
            try {
                list = service.deleteIconsTest(body);
                return  new  JSONObject(list).toString();
            } catch (Exception e) {
        return new  JSONObject(list).toString();
        }
    }
     @POST
     @Path("/{ID}")
     @Consumes(MediaType.APPLICATION_JSON)
     @Produces(MediaType.APPLICATION_JSON)
     public String delete(@PathParam("ID") String ID, @Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {
         ArrayList<IconSetupBean> list = new ArrayList<IconSetupBean>();
         list = service.deleteIconsTests(ID);
         
         return new     JSONArray(list).toString();
     }
     
     //
     @GET
     @Consumes(MediaType.APPLICATION_JSON)
     @Produces(MediaType.APPLICATION_JSON)
     public String allIcons() {
         ArrayList<IconSetupBean> list = new ArrayList<IconSetupBean>();
         list = service.getAllIconsTest();
         
         return new     JSONArray(list).toString();
     }
    
}
