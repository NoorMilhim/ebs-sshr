package com.appspro.fusionsshr.rest;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.CommitmentAndDisciplineBean;
import com.appspro.fusionsshr.bean.PeopleExtraInformationBean;
import com.appspro.fusionsshr.bean.SummaryReportBean;

import com.appspro.fusionsshr.dao.CommitmentAndDisciplineDAO;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("/CommitmentAndDiscipline")

public class CommitmentAndDisciplineService {
    CommitmentAndDisciplineDAO service=new CommitmentAndDisciplineDAO(); 
   
    @POST
    @Path("UpdateCommitmentAndDiscipline")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String commitmentAndDisciplineAddOrUpdate(String body,@Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
       
        CommitmentAndDisciplineBean list = new CommitmentAndDisciplineBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            CommitmentAndDisciplineBean bean = mapper.readValue(body, CommitmentAndDisciplineBean.class);
            list = service.UpdateCommitmentAndDiscipliney(bean);
            return  new  JSONObject(list).toString();
        } catch (Exception e) {
            return new  JSONObject(list).toString();
        }

    }
    @POST
    @Path("/addCommitmentAndDiscipline")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getBulkTransactionByPersonsId(String body, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        CommitmentAndDisciplineBean list = new CommitmentAndDisciplineBean();
        AppsproConnection.LOG.info("test"+body);
        try {
            
            
          list=   service.executeCommitmentAndDiscipline(body);
            return  new  JSONObject(list).toString();
            
        } catch (Exception e) {
            return  new  JSONObject(list).toString();
        }
    }
    
    
    

    @GET
    @Path("/getCommitmentAndDiscipline")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllSummary() {
        List<CommitmentAndDisciplineBean> list = new ArrayList<CommitmentAndDisciplineBean>();
        list = service.getCommitmentAndDiscipline();
        return  new  JSONArray(list).toString();
    }

 
   
}
