package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.bean.EitName;
import com.appspro.fusionsshr.bean.PeopleExtraInformationBean;
import com.appspro.fusionsshr.dao.EFFDetails;

import com.appspro.fusionsshr.dao.EITNameDAO;

import com.appspro.fusionsshr.dao.IconSetupDAO;

import java.io.StringReader;

import java.util.ArrayList;
import java.util.List;

import javax.json.Json;
import javax.json.JsonObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import oracle.javatools.parser.java.v2.internal.compiler.Obj;

import org.json.JSONArray;
import org.json.JSONObject;

import utils.system;
@Path("/eitName")
public class EITNameRest {
//    EitsDAO E_Serivce = new EitsDAO(); 
   EITNameDAO service = new EITNameDAO();
    public EITNameRest() {
        super();
    }
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String allEitName() {
        ArrayList<EitName> list = new ArrayList<EitName>();
        list = service.eitName();
        
        return new     JSONArray(list).toString();
    }
}
