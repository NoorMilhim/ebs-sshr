/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.bean.ApprovalListBean;
import com.appspro.fusionsshr.bean.ApprovalSetupBean;
import com.appspro.fusionsshr.dao.ApprovalListDAO;
import common.restHelper.RestHelper;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;

/**
 *
 * @author user
 */
@Path("/approvalList")
public class ApprovalListService extends RestHelper {

    ApprovalListDAO service = new ApprovalListDAO();

    @GET
    @Path("lastStep/{transactionId}/{serviceType}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllWorkNatureAllowance(@PathParam("transactionId") String transactionId, @PathParam("serviceType") String serviceType, @Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {
        ArrayList<ApprovalListBean> list = new ArrayList<ApprovalListBean>();

        list = service.getLastStepForApproval(transactionId, serviceType);

         return  new  JSONArray(list).toString();

    }

    @GET
    @Path("/{eitcode}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllWorkNatureAllowance(@PathParam("eitcode") String eid_code, @Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {
        ArrayList<ApprovalSetupBean> list = new ArrayList<ApprovalSetupBean>();

        list = service.getApprovalByEITCode(eid_code);

         return  new  JSONArray(list).toString();
    }

    @GET
    @Path("approvaltype/{s_type}/{t_type}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getapprovaltype(@PathParam("s_type") String s_type, @PathParam("t_type") String t_type, @Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {
        ArrayList<ApprovalListBean> list = new ArrayList<ApprovalListBean>();

        list = service.getApprovalType(s_type, t_type);

        return  new  JSONArray(list).toString();
    }

}
