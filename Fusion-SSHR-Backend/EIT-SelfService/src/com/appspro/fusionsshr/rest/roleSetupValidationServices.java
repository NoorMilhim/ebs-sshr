package com.appspro.fusionsshr.rest;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.RoleSetUpBean;

import com.appspro.fusionsshr.bean.ValidationBean;
import com.appspro.fusionsshr.bean.roleSetupValidationBean;

import com.appspro.fusionsshr.dao.roleSetupValidationDAO;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;


import org.json.JSONArray;
import org.json.JSONObject;


@Path("/RoleSetupValidation")
public class roleSetupValidationServices {


    roleSetupValidationDAO serives = new roleSetupValidationDAO();


    @POST
    @Path("/{transactionType}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertOrUpdateValidation(String body,
                                           @PathParam("transactionType")
        String transactionType, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        //(body);
        roleSetupValidationBean list = new roleSetupValidationBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            roleSetupValidationBean bean =
                mapper.readValue(body, roleSetupValidationBean.class);
            //(bean);
            list =
serives.insertOrUpdateRoleSetupValidation(bean, transactionType);
            //(transactionType+bean);
            return new JSONObject(list).toString();
        } catch (Exception e) {
            return new JSONObject(list).toString();
        }
    }


    @GET
    @Path("/getRoleSetupValidation")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllRoleSetupe(@Context
        HttpServletRequest request) {
        return new JSONArray(roleSetupValidationDAO.getInstance().getRoleSetupValidation()).toString();
    }

    @GET
    @Path("/getALLRoleSetupValidation")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllRoleSetupeValidation(@Context
        HttpServletRequest request) {

        return new JSONArray(serives.getAllRoleSetupValidation()).toString();
    }

    @POST
    @Path("/getAllowEitCode")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllowRoleForEmployee(String body, @Context
        HttpServletRequest request) {
        //(body);
        JSONObject jsonObj = new JSONObject(body);
        String role_Id = jsonObj.get("roleid").toString();
        //(role_Id);


        List<RoleSetUpBean> list = new ArrayList<RoleSetUpBean>();
        list = serives.getAllowValidationForEmployee(role_Id);
        return new JSONArray(list).toString();
    }


    @POST
    @Path("/geteEitCode")
    @Produces(MediaType.APPLICATION_JSON)
    public String geteEitCode(String body, @Context
        HttpServletRequest request) {
        //(body);

        List<RoleSetUpBean> list = new ArrayList<RoleSetUpBean>();
        list = serives.getEitCode(body);
        return new JSONArray(list).toString();
    }

    @GET
    @Path("deleteRoleSetUpValidation/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String deleteRoleSetUpValidation(@PathParam("id")
        String id) {
        String status = "";

        status = serives.deleteRoleSetUpValidation(id);
        return status;
    }
    
    @POST
    @Path("/search")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String searchRoleSetup(String body) {
        ArrayList<roleSetupValidationBean> roleList =
            new ArrayList<roleSetupValidationBean>();
        try {
        ObjectMapper mapper = new ObjectMapper();
        roleSetupValidationBean bean = mapper.readValue(body, roleSetupValidationBean.class);
        roleList = serives.searchRoleSetup(bean);
        return new JSONArray(roleList).toString();
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }
        return new JSONArray(roleList).toString();
    }


}
