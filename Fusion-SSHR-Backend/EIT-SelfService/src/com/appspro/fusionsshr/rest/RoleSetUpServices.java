package com.appspro.fusionsshr.rest;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.RoleSetUpBean;
import com.appspro.fusionsshr.bean.SummaryReportBean;

import com.appspro.fusionsshr.dao.RoleSetUpDAO;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.servlet.http.HttpSession;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("/RoleSetup")
public class RoleSetUpServices {

    RoleSetUpDAO services = new RoleSetUpDAO();

    @POST
    @Path("/addRoleSetup")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String RoleSetupAdd(String body, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        //(body);
        RoleSetUpBean list = new RoleSetUpBean();
        try {

            ObjectMapper mapper = new ObjectMapper();
            RoleSetUpBean bean = mapper.readValue(body, RoleSetUpBean.class);
            list = services.addRoleSetUp(bean);
            return new JSONObject(list).toString();
        } catch (Exception e) {
            return new JSONObject(list).toString();
        }

    }


    @POST
    @Path("/updateRoleSetup")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String RoleSetupUpdate(String body, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        //(body);
        RoleSetUpBean list = new RoleSetUpBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            RoleSetUpBean bean = mapper.readValue(body, RoleSetUpBean.class);
            //(bean);
            list = services.UpdateRoleSetUp(bean);
            return new JSONObject(list).toString();
        } catch (Exception e) {
            return new JSONObject(list).toString();
        }

    }


    @GET
    @Path("/getRoleName")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllReport(@Context
        HttpServletRequest request) {
        List<RoleSetUpBean> list = new ArrayList<RoleSetUpBean>();
        list = services.getAllRoleName();
        return new JSONArray(list).toString();
    }


    @GET
    @Path("/getAllRoleSetupe")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllRoleSetupe(@Context
        HttpServletRequest request) {

        List<RoleSetUpBean> list = new ArrayList<RoleSetUpBean>();
        list = services.getRoleSetup();
        return new JSONArray(list).toString();
    }

    @GET
    @Path("deleteRoleSetUp/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String deleteRoleSetUp(@PathParam("id")
        String id) {
        String status = "";

        status = services.deleteRoleSetUp(id);
        return status;
    }
    
    @POST
    @Path("/search")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String searchRoleSetup(String body) {
        ArrayList<RoleSetUpBean> roleList =
            new ArrayList<RoleSetUpBean>();
        try {
        ObjectMapper mapper = new ObjectMapper();
        RoleSetUpBean bean = mapper.readValue(body, RoleSetUpBean.class);
        roleList = services.searchRoleSetup(bean);
        return new JSONArray(roleList).toString();
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }
        return new JSONArray(roleList).toString();
    }


}
