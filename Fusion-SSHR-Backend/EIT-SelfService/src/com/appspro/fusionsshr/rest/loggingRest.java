package com.appspro.fusionsshr.rest;


import com.appspro.fusionsshr.bean.TrackServerRequestBean;
import com.appspro.fusionsshr.dao.EFFDetails;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.json.JSONArray;

@Path("loggingRest")
public class loggingRest {

    @GET
    public Response get() {
        List<TrackServerRequestBean> res = EFFDetails.getInstance().getLog();
        return Response.ok(new JSONArray(res).toString()).build();
    }
}
