package com.appspro.fusionsshr.rest;


import com.appspro.fusionsshr.bean.MailTemplateBean;

import com.appspro.fusionsshr.dao.MailTemplateDao;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;


@Path("mailTemplate")
public class MailTemplateRest {

    //get all

    @GET
    //    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response get() {


        try {
            return Response.ok(new JSONArray(MailTemplateDao.getInstance().getAll()).toString(),
                               MediaType.APPLICATION_JSON).build();
        } catch (Exception e) {
            return Response.status(500).entity(e.getMessage()).build();
        }
    }
    //get by id

    @GET
    @Path("/{id}/{name}/{language}")
    public Response get(@PathParam("id") String id,@PathParam("name") String name,@PathParam("language") String language) {
        try {
            List<MailTemplateBean> lst = MailTemplateDao.getInstance().search(id,name,language);
            return Response.ok(new JSONArray(lst).toString(),
                               MediaType.APPLICATION_JSON).build();
        } catch (Exception e) {
            return Response.status(500).entity(e.getMessage()).build();
        }
    }
    
    @POST
    @Path("/{id}")
    public Response get(@PathParam("id") String id) {
        if(id == null || id.length() <=0)
            return Response.status(400).entity("missing params").build();
        try {
            boolean isDeleted = MailTemplateDao.getInstance().deleteById(id);
            return Response.ok(isDeleted?"Done":"Failed",MediaType.TEXT_PLAIN).build();
        } catch (Exception e) {
            return Response.status(500).entity(e.getMessage()).build();
        }
    }
}
