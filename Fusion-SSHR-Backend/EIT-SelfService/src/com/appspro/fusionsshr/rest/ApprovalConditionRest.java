package com.appspro.fusionsshr.rest;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.ApprovalConditionBean;

import com.appspro.fusionsshr.bean.roleSetupValidationBean;
import com.appspro.fusionsshr.dao.ApprovalConditionDAO;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;


import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;


import org.json.JSONArray;
import org.json.JSONObject;

@Path("/ApprovalCondition")
public class ApprovalConditionRest {
    ApprovalConditionDAO serivces = new ApprovalConditionDAO();


    @GET
    @Path("/getApprovalCondition")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllApprovalCondition(@Context
        HttpServletRequest request) {

        List<ApprovalConditionBean> list =
            new ArrayList<ApprovalConditionBean>();
        list = serivces.getAllApprovalCondition();
        return new JSONArray(list).toString();
    }

    @POST
    @Path("addApprovalCondition")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertApprovalCondition(String body) {

        ApprovalConditionBean list = new ApprovalConditionBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            ApprovalConditionBean bean =
                mapper.readValue(body, ApprovalConditionBean.class);
            list = serivces.addApprovalCondition(bean);
            return new JSONObject(list).toString();
        } catch (Exception e) {
            return new JSONObject(list).toString();
        }
    }


    @POST
    @Path("updateApprovalCondition")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateApprovalCondition(String body) {

        ApprovalConditionBean list = new ApprovalConditionBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            ApprovalConditionBean bean =
                mapper.readValue(body, ApprovalConditionBean.class);
            list = serivces.updateApprovalCondition(bean);
            return new JSONObject(list).toString();
        } catch (Exception e) {
            return new JSONObject(list).toString();
        }
    }

    @GET
    @Path("/getApprovalConditionCount/{approvalCount}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllApprovalConditionCount(@PathParam("approvalCount")
        String approvalCount, @Context
        HttpServletRequest request) {
        AppsproConnection.LOG.info(approvalCount);
        String countApprovalCondition =
            serivces.getCountOfApprovalCondition(approvalCount);
        return countApprovalCondition;
    }

    @GET
    @Path("/getAllApprovalCode/{eitcode}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllApprovalCode(@Context
        HttpServletRequest request, @PathParam("eitcode")
        String eitcode) {

        List<ApprovalConditionBean> list =
            new ArrayList<ApprovalConditionBean>();
        list = serivces.getAllApprovalCode(eitcode);
        return new JSONArray(list).toString();
    }

    @GET
    @Path("deleteApprovalCondition/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String deleteApprovalConditionById(@PathParam("id")
        String id) {
        String status = "";

        status = serivces.deleteApprovalCondition(id);
        return status;
    }
    
    @POST
    @Path("/search")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String searchApprovalConditionSetup(String body) {
        ArrayList<ApprovalConditionBean> approvalList =
            new ArrayList<ApprovalConditionBean>();
        try {
        ObjectMapper mapper = new ObjectMapper();
        ApprovalConditionBean bean = mapper.readValue(body, ApprovalConditionBean.class);
        approvalList = serivces.searchApprovalConditionSetup(bean);
        return new JSONArray(approvalList).toString();
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }
        return new JSONArray(approvalList).toString();
    }


}


