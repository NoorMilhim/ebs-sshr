/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.bean.WorkNatureAllowanceBean;
import com.appspro.fusionsshr.dao.WorkNatureAllowanceDAO;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author amro
 */
@Path("worknatureallowance")
public class WorkNatureAllowanceService {

    WorkNatureAllowanceDAO dao = new WorkNatureAllowanceDAO();

    @POST
    @Path("/{transactionType}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response insertOrUpdateWorkNatureAllowance(WorkNatureAllowanceBean bean,
                                                      @PathParam("transactionType")
        String transactionType, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        bean = dao.insertOrUpdateWorkNatureAllowance(bean, transactionType);

        try {
            bean =
dao.insertOrUpdateWorkNatureAllowance(bean, transactionType);
        } catch (Exception e) {
            return Response.status(500).entity(e.getMessage()).build();
        }
        return Response.ok(bean, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<WorkNatureAllowanceBean> getAllWorkNatureAllowance(@Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        ArrayList<WorkNatureAllowanceBean> list =
            new ArrayList<WorkNatureAllowanceBean>();
        list = dao.getAllWorkNatureAllowance();
        return list;
    }

    @GET
    @Path("/{positionCode}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<WorkNatureAllowanceBean> getAllWorkNatureAllowanceByPositionCode(@PathParam("positionCode")
        String positionCode, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        ArrayList<WorkNatureAllowanceBean> list =
            new ArrayList<WorkNatureAllowanceBean>();
        list = dao.getAllWorkNatureAllowanceByPositionCode(positionCode);
        return list;
    }

    @GET
    @Path("/search/{code}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<WorkNatureAllowanceBean> getAllWorkNatureAllowanceByCode(@PathParam("code")
        String code, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        ArrayList<WorkNatureAllowanceBean> list =
            new ArrayList<WorkNatureAllowanceBean>();
        list = dao.getAllWorkNatureAllowanceByCode(code);
        return list;
    }

}
