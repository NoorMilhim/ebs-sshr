/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


/**
 *
 * @author lenovo
 */
@Path("/date")
public class HijrahDateService {


    @GET
    @Path("/HijrahDate")
    public Response getdate(@Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        try {
            //            java.time.chrono.HijrahDate dat =
            //                java.time.chrono.HijrahDate.now();
            //            String Str = new String(dat.toString());
            //            String date = Str.substring(18);

            return Response.ok(null, MediaType.TEXT_PLAIN).build();

        } catch (Exception e) {
            return Response.status(500).entity(e.getMessage()).build();
        }

    }

}
