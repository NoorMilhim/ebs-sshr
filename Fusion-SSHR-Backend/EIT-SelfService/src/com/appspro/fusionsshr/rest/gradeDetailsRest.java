/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.bean.OrgnizationBean;
import com.appspro.fusionsshr.bean.gradeBean;
import com.appspro.fusionsshr.dao.OrganizationDetails;
import com.appspro.fusionsshr.dao.gradeDetailsDAO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author lenovo
 */
@Path("/orgGrade")
public class gradeDetailsRest {

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getGradeDetails(@Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) throws JsonProcessingException, UnsupportedEncodingException {

        ObjectMapper mapper = new ObjectMapper();
        gradeBean obj = new gradeBean();
        gradeDetailsDAO det = new gradeDetailsDAO();

        ArrayList<gradeBean> gradeList = det.getOrgDetails();
        String jsonInString = null;
        jsonInString = mapper.writeValueAsString(gradeList);
        return Response.ok(new String(jsonInString.getBytes(), "UTF-8"), MediaType.APPLICATION_JSON).build();

    }

}
