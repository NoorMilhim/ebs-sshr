package com.appspro.fusionsshr.rest;
import com.appspro.fusionsshr.bean.ApprovalSetupBean;
import com.appspro.fusionsshr.bean.ProbationaryPeriodEvaluationBean;
import com.appspro.fusionsshr.bean.SummaryReportBean;
import com.appspro.fusionsshr.dao.ProbationaryPeriodEvaluationDAO;
import com.appspro.fusionsshr.dao.SummaryReportDAO;
import javax.servlet.http.HttpSession;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("/ProbationPerformanceEvaluation")
public class ProbationaryPeriodEvaluationServices {
    ProbationaryPeriodEvaluationDAO services=new ProbationaryPeriodEvaluationDAO();
    @GET
    @Path("/getProbation/{perosnNumber}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String getReportName( @PathParam("perosnNumber")
        String perosnNumber, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        List<ProbationaryPeriodEvaluationBean> list = new ArrayList<ProbationaryPeriodEvaluationBean>();
        list = services.getAllSumary(perosnNumber);
        return  new  JSONArray(list).toString();

    }
    
    
    @GET
    @Path("/getProbationPerformanceEvaluation/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String getReportNameById( @PathParam("id")
        String id, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        List<ProbationaryPeriodEvaluationBean> list = new ArrayList<ProbationaryPeriodEvaluationBean>();
        list = services.getAllSumaryById(id);
        return  new  JSONArray(list).toString();

    }
    
   
   
    @POST
    @Path("addProbationPerformanceEvaluation")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String AddProbationAndEvaluation(String body, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        ProbationaryPeriodEvaluationBean list = new  ProbationaryPeriodEvaluationBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            ProbationaryPeriodEvaluationBean bean = mapper.readValue(body, ProbationaryPeriodEvaluationBean.class);
            list = services.addProbationPerformanceEvaluation(bean);
            return  new  JSONObject(list).toString();
        } catch (Exception e) {
            return new  JSONObject(list).toString();
        }

    }
    
    
    @POST
    @Path("UpdateProbationPerformanceEvaluation")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String UpdateProbationAndEvaluation(String body, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        ProbationaryPeriodEvaluationBean list = new  ProbationaryPeriodEvaluationBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            ProbationaryPeriodEvaluationBean bean = mapper.readValue(body, ProbationaryPeriodEvaluationBean.class);
            list = services.UpdateProbationPerformanceEvaluation(bean);
            return  new  JSONObject(list).toString();
        } catch (Exception e) {
            return new  JSONObject(list).toString();
        }

    }
    
    @POST
    @Path("UpdateStatus")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String UpdateStatus(String body, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        ProbationaryPeriodEvaluationBean list = new  ProbationaryPeriodEvaluationBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            ProbationaryPeriodEvaluationBean bean = mapper.readValue(body, ProbationaryPeriodEvaluationBean.class);
            list = services.UpdateStatus(bean);
            return  new  JSONObject(list).toString();
        } catch (Exception e) {
            return new  JSONObject(list).toString();
        }

    }
    
   
   
}
