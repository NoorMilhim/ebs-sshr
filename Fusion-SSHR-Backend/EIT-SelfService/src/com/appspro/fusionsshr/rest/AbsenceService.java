/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.bean.AbsenceBean;
import com.appspro.fusionsshr.dao.AbsenceDAO;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

/**
 *
 * @author user
 */
@Path("/absence")
public class AbsenceService {

    AbsenceDAO service = new AbsenceDAO();

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void insertAbsence(String bean) {

        try {
            JSONObject jsonObj = new JSONObject(bean);
            service.postAbsence(jsonObj.toString());
        } catch (Exception e) {
            //(e);
//return Response.status(500).entity(e.getMessage()).build();
        }

//return Response.ok(bean, MediaType.APPLICATION_JSON).build();
    }
}
