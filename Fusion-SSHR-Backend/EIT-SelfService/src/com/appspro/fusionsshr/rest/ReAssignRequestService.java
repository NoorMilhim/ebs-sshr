package com.appspro.fusionsshr.rest;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.ApprovalListBean;
import com.appspro.fusionsshr.bean.PeopleExtraInformationBean;

import com.appspro.fusionsshr.dao.ReAssignRequestDAO;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Shadi Mansi-PC
 */
@Path("reassignRequest")
public class ReAssignRequestService {
    
    ReAssignRequestDAO service = new ReAssignRequestDAO();
    
    @GET
    @Path("trackRequest/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String searchPositionsByPositionName(@Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        
        ArrayList<PeopleExtraInformationBean> list = new ArrayList<PeopleExtraInformationBean>();

        try {
            list = service.getPendingSelfServices();
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }
        return new JSONArray(list).toString();
    }
    
    @GET
    @Path("/delete/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String deleteSelfServiceByID(@PathParam("id")
        String id, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {

        String status = "";
        try {
            status = service.deleteSelfServiceRow(id);
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }
        return status;

    }
    
    //******************Start et EIT From People Extra By Person Number**************
    @POST
    @Path("/getEit")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertXXX_EIT(String body, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        
        ArrayList<PeopleExtraInformationBean> list = new ArrayList<PeopleExtraInformationBean>();
        try {
            ObjectMapper mapper = new ObjectMapper();
            PeopleExtraInformationBean bean = mapper.readValue(body, PeopleExtraInformationBean.class);
            list = service.getPendingSelfServicesByPersonNumber(bean);
            return new JSONArray(list).toString();
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }
        return new JSONArray(list).toString();
    }
    //********************End Get EIT From People Extra By Person Number**************
    @POST
    @Path("/getPendingAndApprovedByPersonNumber")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getPendingAndApprovedByPersonNumber(String body, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        
        ArrayList<PeopleExtraInformationBean> list = new ArrayList<PeopleExtraInformationBean>();
        try {
            ObjectMapper mapper = new ObjectMapper();
            PeopleExtraInformationBean bean = mapper.readValue(body, PeopleExtraInformationBean.class);
            list = service.getPendingAndApprovedSelfServicesByPersonNumber(bean);
            return new JSONArray(list).toString();
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }
        return new JSONArray(list).toString();
    }
    
    
    @GET
    @Path("approvaltype/{s_type}/{t_type}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getapprovaltype(@PathParam("s_type") String s_type, @PathParam("t_type") String t_type, @Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {
        ArrayList<ApprovalListBean> list = new ArrayList<ApprovalListBean>();

        list = service.getApprovalType(s_type, t_type);

        return  new  JSONArray(list).toString();
    }
    
    @POST
    @Path("/updateApprovalList")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateApprovalList(String body, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        
        String list = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            ApprovalListBean bean = mapper.readValue(body, ApprovalListBean.class);
            list = service.updateApprovalList(bean);
            return list;
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }
        return list;
    }
    
}
