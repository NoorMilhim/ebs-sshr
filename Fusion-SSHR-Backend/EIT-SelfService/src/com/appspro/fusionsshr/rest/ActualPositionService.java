/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;


import com.appspro.fusionsshr.bean.ActualPosition;
import com.appspro.fusionsshr.dao.ActualPositionDAO;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


/**
 *
 * @author CPBSLV
 */
@Path("/actualposition")
public class ActualPositionService {

    ActualPositionDAO service = new ActualPositionDAO();

    @POST
    @Path("/{transactionType}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response insertOrUpdatePosition(ActualPosition bean,
                                           @PathParam("transactionType")
        String transactionType, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        //(bean.getStatus() + "  " + bean.getId() + "  " +
        bean.getFamilyJob();
        ;
        try {
            service.insertOrUpdatePosition(bean, transactionType);
        } catch (Exception e) {
            //(e);
            return Response.status(500).entity(e.getMessage()).build();
        }

        return Response.ok(bean, MediaType.APPLICATION_JSON).build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updatePosition(ActualPosition bean, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        //       service.updateJob(bean);
        return Response.ok(bean, MediaType.APPLICATION_JSON).build();

    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deletePosition(ActualPosition bean, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        return Response.ok(null, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<ActualPosition> getAllPositions(@Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        ArrayList<ActualPosition> list = new ArrayList<ActualPosition>();

        list = service.getAllPositions();

        return list;
    }

    @POST
    @Path("/search")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<ActualPosition> searchPositions(@FormParam("familyJob")
        String familyJob, @FormParam("postionNumber")
        String postionNumber, @FormParam("managing")
        String managing, @FormParam("positionName")
        String positionName, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        ArrayList<ActualPosition> list = new ArrayList<ActualPosition>();

        list =
service.searchPositions(familyJob, postionNumber, managing, positionName);

        return list;
    }

    @POST
    @Path("/searchbycode")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<ActualPosition> searchPositions(@FormParam("code")
        String code, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        ArrayList<ActualPosition> list = new ArrayList<ActualPosition>();

        list = service.searchPositionsByCode(code);

        return list;
    }

    //ws to get position by position name

    @GET
    @Path("/search/{positionName}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<ActualPosition> searchPositionsByPositionName(@PathParam("positionName")
        String positionName, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        //("positionName" + positionName);
        ArrayList<ActualPosition> positionsList =
            new ArrayList<ActualPosition>();

        positionsList = service.searchPositionsByName(positionName);

        return positionsList;

    } //addedmoh
}
