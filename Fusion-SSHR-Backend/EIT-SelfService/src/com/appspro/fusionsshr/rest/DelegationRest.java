package com.appspro.fusionsshr.rest;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.DelegationBean;
import com.appspro.fusionsshr.bean.SummaryReportBean;

import com.appspro.fusionsshr.dao.AbsenceDAO;
import com.appspro.fusionsshr.dao.DelegationDAO;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;



@Path("/Delegation")
public class DelegationRest {
    
    DelegationDAO  service= new DelegationDAO();
    
    @GET
    @Path("/getDelegation")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllSummaryReport(  @QueryParam("personNumber") String personNumber) {
        List<DelegationBean> list = new ArrayList<DelegationBean>();
        list = service.getAllBypersonNumber(personNumber);
        return  new  JSONArray(list).toString();
    }
    
    @GET
    @Path("/validationDelegationPeriod")
    @Produces(MediaType.APPLICATION_JSON)
    public String DelegationCount(@QueryParam("personNumber") String personNumber) {
       
        List<DelegationBean> list = new ArrayList<DelegationBean>();
        try {
//        ObjectMapper mapper = new ObjectMapper();
//        DelegationBean delegationBean = mapper.readValue(body, DelegationBean.class);
        list= service.DelegationCountPeriod(personNumber);
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }
        return  new  JSONArray(list).toString();
    }
    
    @POST
    @Path("/UpdatevalidationDelegationPeriod")
    @Produces(MediaType.APPLICATION_JSON)
    public String UpdatevalidationDelegationPeriod(String body) {
       
        List<DelegationBean> list = new ArrayList<DelegationBean>();
        try {
        ObjectMapper mapper = new ObjectMapper();
        DelegationBean delegationBean = mapper.readValue(body, DelegationBean.class);
        list= service.validationPeriod(delegationBean);
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }
        return  new  JSONArray(list).toString();
    }
    
    @POST
    @Path("addDelegation/{transactionType}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String  AddOrUpdateDelegation(String body,
                                       @PathParam("transactionType")
        String transactionType, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        DelegationBean list = new DelegationBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            DelegationBean bean = mapper.readValue(body, DelegationBean.class);
            list = service.insertOrUpdatedelegation(bean, transactionType);
            return  new  JSONObject(list).toString();
        } catch (Exception e) {
            return new  JSONObject(list).toString();
        }

    }
    
    
    @GET
    @Path("deleteDelegation/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String deleteDelegation(@PathParam("id")
            String id) {
            String status = "";
             
       status=  service.deleteDelegationById(id);
       return status;
    }
   
   
}
