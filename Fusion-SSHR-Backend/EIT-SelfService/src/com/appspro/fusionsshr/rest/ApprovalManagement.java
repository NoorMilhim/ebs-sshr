/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.ApprovalManagementBean;
import com.appspro.fusionsshr.dao.ApprovalManagementDAO;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import java.io.IOException;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Shadi Mansi-PC
 */
@Path("/approvalManagement")
public class ApprovalManagement {

    ApprovalManagementDAO service = new ApprovalManagementDAO();

    @POST
    @Path("/{transactionType}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertOrUpdateApprovalManagement(String body, @PathParam("transactionType") String transactionType) {
        ApprovalManagementBean bean = new ApprovalManagementBean();
        try {
        bean= service.insertOrUpdateApprovalManagement(body, transactionType);
        return  new  JSONObject(bean).toString();
            
        } catch (JsonParseException e) {
        } catch (JsonMappingException e) {
        } catch (IOException e) {
        }
        return new JSONObject(bean).toString();
    }

    @GET
    @Path("search/{eit_code}/{type}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getApprovalManagementByEitCode(@PathParam("eit_code") String eitCode, @PathParam("type") String approvalType, @Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {

        ArrayList<ApprovalManagementBean> list = new ArrayList<ApprovalManagementBean>();
        try {
            list = service.getApprovalManagementByEITCode(eitCode, approvalType);
            return  new  JSONArray(list).toString();
        } catch (Exception e) {
            return new  JSONArray(list).toString();
        }

    }

    @GET
    @Path("validation/{eit_code}/{type}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getValidationApprovalManagementByEitCode(@PathParam("eit_code") String eitCode, @PathParam("type") String approvalType, @Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {

        return service.getValidationApprovalManagementByEITCode(eitCode, approvalType);

    }

    @GET
    @Path("search/{eit_code}/{type}/{positionId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getApprovalManagementByPositionId(@PathParam("eit_code") String eitCode, @PathParam("type") String approvalType, @PathParam("positionId") String positionId, @Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {

        ArrayList<ApprovalManagementBean> list = new ArrayList<ApprovalManagementBean>();
        try {
            list = service.getApprovalManagementByPositionID(eitCode, approvalType, positionId);
            return  new  JSONArray(list).toString();
        } catch (Exception e) {
            return new  JSONArray(list).toString();
        }
        
    }

    @GET
     @Path("delapproval/{eit_code}/{type}")
     @Produces(MediaType.APPLICATION_JSON)
     public String getDeleteApproval(@PathParam("eit_code")
         String eitCode,@PathParam("type")
         String type) {
         String list = null;
         try {
             list = service.deleteApprovalManagement(eitCode,type);
             return list;
         } catch (Exception e) {
            e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);

             return list;

         }
     }
    }

