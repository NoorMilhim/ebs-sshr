package com.appspro.mail;


import com.appspro.db.AppsproConnection;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.client.urlconnection.HTTPSProperties;

import java.io.OutputStream;

import java.net.URL;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import javax.xml.bind.DatatypeConverter;


public class CredentialStoreClassMail {
    
//    public static void  main(String ...args){
//        SEND_MAIL("ahmed.hussein@appspro-me.com","sub","body");
//    }
    //    public void CredentialStoreTest() {
    //        try {
    //
    //            CredentialStore credentialStore =
    //                oracle.security.jps.service.JpsServiceLocator.getServiceLocator().lookup(CredentialStore.class);
    //
    //            String map = "user.custom.map";
    //            String mykey = "AppsProMail";
    //            try {
    //                credentialStore.setCredential(map, mykey,
    //                                              CredentialFactory.newPasswordCredential("paas_autoGeneratemail@HHA.com",
    //                                                                                      "Appspro#321".toCharArray()));
    //            } catch (Exception e) {
    //               e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
    //
    //            }
    //            try {
    //                Credential cred = credentialStore.getCredential(map, mykey);
    //                if (cred != null) {
    //                
    //            } catch (Exception e) {
    //               e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
    //            }
    //        } catch (Exception eg) {
    //            eg.printStackTrace();
    //        }
    //    }

//
//    public static String SEND_MAIL(String emailTo, String subject,
//                                   String body) {
//        SSLContext ctx = null;
//        System.setProperty("DUseSunHttpHandler", "true");
//        TrustManager[] trustAllCerts =
//            new X509TrustManager[] { new X509TrustManager() {
//                public X509Certificate[] getAcceptedIssuers() {
//                    return null;
//                }
//
//                public void checkClientTrusted(X509Certificate[] certs,
//                                               String authType) {
//                }
//
//                public void checkServerTrusted(X509Certificate[] certs,
//                                               String authType) {
//                }
//            } };
//        try {
//            ctx = SSLContext.getInstance("TLSv1.2");
//            ctx.init(null, trustAllCerts, null);
//        } catch (NoSuchAlgorithmException e) {
//            
//        } catch (KeyManagementException e) {
//           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
//        }
//
//        SSLContext.setDefault(ctx);
//
//        ClientConfig def = new DefaultClientConfig();
//
//        def.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES,
//                                new HTTPSProperties(null, ctx));
//        Client client = Client.create(def);
//     
//        String input = "{  \n" +
//            "   \"Messages\":[  \n" +
//            "      {  \n" +
//            "         \"From\":{  \n" +
//            "            \"Email\":\"HR@hha.com.sa\",\n" +
//            "            \"Name\":\"PaaS Self Service Admin\"\n" +
//            "         },\n" +
//            "         \"To\":[  \n" +
//            "            {  \n" +
//            "               \"Email\":\"" + emailTo + "\",\n" +
//            "               \"Name\":\"Employee\"\n" +
//            "            }\n" +
//            "         ],\n" +
//            "         \"Subject\":\"" + subject + "\",\n" +
//            "         \"TextPart\":\"" + body + "\",\n" +
//            "         \"HTMLPart\":\"\"\n" +
//            "      }\n" +
//            "   ]\n" +
//            "}";
//
//        client.addFilter(new HTTPBasicAuthFilter("0c466d45ad6dfa75b0d02b2c0fa1c069",
//                                                 "75f2279fdd22bc3a5ac563d2da7a5dd6"));
//
//        //        serverURL = serverURL.replaceAll("\\s", "%20");
//        WebResource resource =
//            client.resource("https://api.mailjet.com/v3.1/send");
//
//        return resource.type("application/json").post(String.class, input);
//    }
    
    //    test 
    
    public static void SEND_MAIL(String emailTo, String subject,
                                   String body) throws Exception {
//        try {
//            byte[] message = ("0c466d45ad6dfa75b0d02b2c0fa1c069" + ":" + "75f2279fdd22bc3a5ac563d2da7a5dd6").getBytes();
//            String encoded = DatatypeConverter.printBase64Binary(message);
//            String model = "{  \n" +
//                "   \"Messages\":[  \n" +
//                "      {  \n" +
//                "         \"From\":{  \n" +
//                "            \"Email\":\"HR@hha.com.sa\",\n" +
//                "            \"Name\":\"PaaS Self Service Admin\"\n" +
//                "         },\n" +
//                "         \"To\":[  \n" +
//                "            {  \n" +
//                "               \"Email\":\"" + emailTo + "\",\n" +
//                "               \"Name\":\"Employee\"\n" +
//                "            }\n" +
//                "         ],\n" +
//                "         \"Subject\":\"" + subject + "\",\n" +
//                "         \"HTMLPart\":\"" + body + "\",\n" +
//                "         \"TextPart\":\"\"\n" +
//                "      }\n" +
//                "   ]\n" +
//                "}";
//               
//
//            String url = "https://api.mailjet.com/v3.1/send";
//
//            URL obj =
//                new URL(null, url, new sun.net.www.protocol.https.Handler());
//            HttpsURLConnection con = (HttpsURLConnection)obj.openConnection();
//            con.setDoInput(true);
//            con.setDoOutput(true);
//            con.setRequestMethod("POST");
//            con.setRequestProperty("Content-Type",
//                                   "application/json; charset=UTF-8");
//            con.setRequestProperty("Accept", "application/json");
//            con.setConnectTimeout(6000000);
//            con.setRequestProperty("Authorization", "Basic " + encoded);
//
//            String urlParameters = new String(model.getBytes());
//
//            OutputStream wr = con.getOutputStream();
//            wr.write(model.getBytes("UTF-8"));
//            //wr.flush();
//            wr.close();
//            int responseCode = con.getResponseCode();
//        
//            String responseDescription = con.getResponseMessage();
//   
//
//        } catch (Exception e) {
//            
//           e.printStackTrace();
//           AppsproConnection.LOG.error("ERROR", e);
        }

//    }
    

}