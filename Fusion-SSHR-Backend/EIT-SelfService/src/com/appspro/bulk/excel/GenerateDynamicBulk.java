package com.appspro.bulk.excel;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.ElementEntrySetupBean;
import com.appspro.fusionsshr.bean.EmployeeBean;
import com.appspro.fusionsshr.bean.LineSourceElementEntryBean;
import com.appspro.fusionsshr.bean.PeopleExtraInformationBean;
import com.appspro.fusionsshr.bean.TrackServerRequestBean;
import com.appspro.fusionsshr.bean.trackRequestBean;
import com.appspro.fusionsshr.dao.EFFDetails;
import com.appspro.fusionsshr.dao.ElementEntrySetupDAO;
import com.appspro.fusionsshr.dao.EmployeeDetails;
import com.appspro.fusionsshr.dao.HCMElementEntryHelper;

import com.appspro.fusionsshr.dao.PeopleExtraInformationDAO;

import common.biPReports.BIReportModel;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.lang.reflect.Method;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.poi.hssf.usermodel.DVConstraint;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDataValidation;
import org.apache.poi.hssf.usermodel.HSSFName;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.usermodel.DataValidationHelper;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddressList;

import org.json.JSONArray;
import org.json.JSONObject;

public class GenerateDynamicBulk {
    private static GenerateDynamicBulk instance = null;

    public static GenerateDynamicBulk getInstance() {
        if (instance == null) {
            instance = new GenerateDynamicBulk();
        }
        return instance;
    }


    public static String dynamicGenerate(String eitCode) throws FileNotFoundException,
                                                                IOException {

        //1-get eit segment
        BIReportModel report = BIReportModel.getInstance();

        Map<String, String> reportMaping = new HashMap<String, String>();
        reportMaping.put("LANGX", "US");
        reportMaping.put("EIT_CODE", eitCode);
        JSONObject reportResult =
            report.runReport("getSegmentsByEitCodeReport", reportMaping);
        JSONArray segmentsArray =
            reportResult.getJSONObject("DATA_DS").getJSONArray("G_1");
        //create main sheet
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet realSheet = workbook.createSheet("Sheet xls");


        String element_name = "";
        ArrayList<ElementEntrySetupBean> elements =
            ElementEntrySetupDAO.getInstance().getElementEntryByEIT(eitCode);
        if (elements.size() > 0)
            element_name = elements.get(0).getElementName();
        else {
            Map<String, String> reportMaping1 = new HashMap<String, String>();
            reportMaping1.put("EIT_CODE", eitCode);
            reportMaping1.put("LANG", "US");
            JSONObject reportResult1 =
                report.runReport("getEitNameByCodeReport", reportMaping1);

            element_name =
                    reportResult1.getJSONObject("DATA_DS").getJSONObject("G_1").get("DESCRIPTIVE_FLEX_CONTEXT_NAME").toString();
        }
        HSSFSheet metaDataSheet = workbook.createSheet("meta_sheet");

        //eit emta
        Row metaDataRow = metaDataSheet.createRow(0);
        metaDataRow.createCell(0).setCellValue(eitCode);
        metaDataRow.createCell(1).setCellValue(element_name);

        //end of met data sheet creation


        CellStyle style = workbook.createCellStyle();
        style.setFillBackgroundColor(IndexedColors.RED.getIndex());


        Row header = realSheet.createRow(0);
        //

        JSONArray seggmentToDisplayArray = new JSONArray();

        for (int x = 0; x < segmentsArray.length(); x++) { //loop over cols
            JSONObject cellJsonObject = segmentsArray.getJSONObject(x);
            //LOV HIDDEN TEXT_BOX DATE_TIME TEXT_AREA
            if (!cellJsonObject.getString("DISPLAY_TYPE").equalsIgnoreCase("HIDDEN") &&
                !cellJsonObject.getString("END_USER_COLUMN_NAME").equalsIgnoreCase("DUMMY") &&
                !cellJsonObject.getString("END_USER_COLUMN_NAME").equalsIgnoreCase("ATTACHMENT")) {
                seggmentToDisplayArray.put(cellJsonObject);
            }
        }


        //eit keys
        Row metaKeysRow = metaDataSheet.createRow(1);
        Row APPLICATION_COLUMN_NAME_META_ROW = metaDataSheet.createRow(2);
        Row type_meta = metaDataSheet.createRow(3);

        header.createCell(0).setCellValue("Person Number");
        for (int x = 0; x < seggmentToDisplayArray.length();
             x++) { //loop over cols
            int dx = x + 1;
            JSONObject cellJsonObject =
                seggmentToDisplayArray.getJSONObject(x);
            metaKeysRow.createCell(x).setCellValue(cellJsonObject.has("DESCRIPTION") ?
                                                   cellJsonObject.getString("DESCRIPTION") :
                                                   ""); //will be empty or not found in case if has no element entery
            APPLICATION_COLUMN_NAME_META_ROW.createCell(x).setCellValue(cellJsonObject.getString("APPLICATION_COLUMN_NAME"));
            type_meta.createCell(x).setCellValue(cellJsonObject.getString("DISPLAY_TYPE"));

            String type =
                cellJsonObject.getString("DISPLAY_TYPE"); //LOV HIDDEN TEXT_BOX DATE_TIME TEXT_AREA

            String lbl = cellJsonObject.getString("FORM_ABOVE_PROMPT");

            Cell cell = header.createCell(dx);

            cell.setCellValue(lbl);

            //for any constrain make it cover first 50 col
            CellRangeAddressList addressList =
                new CellRangeAddressList(1, 50, dx,
                                         dx); //fitst row,last row,first col,last col

            DataValidationHelper dvHelper =
                realSheet.getDataValidationHelper();

            //LOV
            if (type.equalsIgnoreCase("LOV") ||
                (cellJsonObject.has("DEFAULT_VALUE") &&
                 cellJsonObject.has("READ_ONLY_FLAG") &&
                 cellJsonObject.get("READ_ONLY_FLAG").toString().equalsIgnoreCase("y"))) {

                JSONArray list_labels = new JSONArray();

                //if list of values
                if (type.equalsIgnoreCase("LOV")) {
                    Map<String, String> map = new HashMap<String, String>();
                    map.put("valueSet",
                            cellJsonObject.get("FLEX_VALUE_SET_NAME").toString());
                    map.put("langCode", "US");
                    JSONObject arr =
                        report.runReport("inDependentReport", map);
                    if (arr.has("DATA_DS") &&
                        arr.getJSONObject("DATA_DS").has("G_1")) {
                        list_labels =
                                getValueListArray(arr.getJSONObject("DATA_DS").getJSONArray("G_1"));
                    }
                }

                //in case of input has defalt value and is disable, so we will display it as list of one value
                else {
                    //check if constant
                    if (cellJsonObject.has("DEFAULT_TYPE") &&
                        cellJsonObject.get("DEFAULT_TYPE").toString().equalsIgnoreCase("c")) {
                        list_labels.put(cellJsonObject.get("DEFAULT_VALUE").toString());
                    } 
                    // here we will handle sql and other cases (not needed for now)
                }
                
                if (list_labels.length() > 0) {
                    //get list labels
                    String hidden_sheet_name =
                        "hidden_" + lbl.replaceAll(" ", "");
                    HSSFSheet hidden = workbook.createSheet(hidden_sheet_name);

                    for (int i = 0; i < list_labels.length(); i++) {
                        Object name = list_labels.get(i);
                        HSSFRow row = hidden.createRow(i);
                        HSSFCell _cell = row.createCell(0);
                        _cell.setCellValue(name.toString());
                    }


                    HSSFName cellname = workbook.createName();
                    cellname.setNameName(hidden_sheet_name);
                    cellname.setRefersToFormula(hidden_sheet_name +
                                                "!$A$1:$A$" +
                                                list_labels.length());

                    DVConstraint constraint =
                        DVConstraint.createFormulaListConstraint(hidden_sheet_name);

                    HSSFDataValidation validation =
                        new HSSFDataValidation(addressList, constraint);

                    realSheet.addValidationData(validation);

                    workbook.setSheetHidden(workbook.getSheetIndex(hidden_sheet_name),
                                            true);
                }

                //DATETIME (DATE)
            } else if (type.equalsIgnoreCase("DATE_TIME")) {

                DataValidationConstraint dvConstraint =
                    dvHelper.createDateConstraint(DVConstraint.OperatorType.BETWEEN,
                                                  "01/01/1900", "31/12/9999",
                                                  "dd/MM/yyyy");

                HSSFDataValidation validation =
                    new HSSFDataValidation(addressList, dvConstraint);
                realSheet.addValidationData(validation);

            }

            //time
            else if (cellJsonObject.has("FLEX_VALUE_SET_NAME") &&
                     cellJsonObject.get("FLEX_VALUE_SET_NAME").toString().equalsIgnoreCase("XXX_HR_PAAS_TIME")) {

                DataValidationConstraint dvConstraint =
                    dvHelper.createTimeConstraint(DVConstraint.OperatorType.BETWEEN,
                                                  "=TIME(0,0,0)",
                                                  "=TIME(23,59,59)");
                HSSFDataValidation validation =
                    new HSSFDataValidation(addressList, dvConstraint);
                realSheet.addValidationData(validation);

            } else {
                cell.setCellStyle(style);
            }
        }


        workbook.setSheetHidden(workbook.getSheetIndex("meta_sheet"), true);


        CellStyle headerBg = workbook.createCellStyle();
        headerBg.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        headerBg.setFillPattern(CellStyle.SOLID_FOREGROUND);
        headerBg.setLocked(false);

        for (int x = 0; x <= seggmentToDisplayArray.length(); x++) {
            realSheet.autoSizeColumn(x);
            //            header.getCell(x).getCellStyle().setLocked(true);

        }
        return workBookToString(workbook);
    }

    public static JSONObject dynamicanalysisExcelBulk(File inp) {
        JSONObject response=new JSONObject();
        response.put("status", "Done");

        PeopleExtraInformationBean bean = new PeopleExtraInformationBean();
        Map<String, EmployeeBean> employeeData =
            new HashMap<String, EmployeeBean>();

        try {
            Workbook wb = WorkbookFactory.create(inp);
            Sheet sheet = wb.getSheetAt(0);
            Row headerCols=sheet.getRow(0);
            CellStyle redBackgroundStyle = wb.createCellStyle();
            redBackgroundStyle.setFillForegroundColor(IndexedColors.RED.getIndex());

            Sheet metaSheet = wb.getSheetAt(wb.getSheetIndex("meta_sheet"));
            Row meta_data_row = metaSheet.getRow(0);
            Row meta_keys_row = metaSheet.getRow(1);
            Row meta_app_cols_row = metaSheet.getRow(2);
            String code = meta_data_row.getCell(0).toString();
            String element_name = meta_data_row.getCell(1).toString();


            int rowsCount = sheet.getLastRowNum();
            //
            EmployeeDetails det = new EmployeeDetails();
            boolean allRowsIsValid = true;

            //array of all element enteries
            JSONArray elementsJsonArray = new JSONArray();

            for (int i = 1; i <= rowsCount; i++) {
                Row row = sheet.getRow(i);
                JSONObject eitObj = new JSONObject();
                eitObj.put("dummy", System.currentTimeMillis());
                String person_number;
                EmployeeBean emp = null;
                for (int cellIterator = 0; cellIterator < row.getLastCellNum();
                     cellIterator++) {
                    Cell poiCell = sheet.getRow(i).getCell(cellIterator);
                    String val = getCellValue(poiCell);
                    //                    String val = poiCell.toString();
                    //                    val = getDataInValidFormat(val);

                    if (cellIterator == 0) { //person number
                        person_number = val;
                        if (employeeData.get(person_number) != null) {
                            emp = employeeData.get(person_number);
                        } else {
                            //search in db
                            String id =
                                (int)Double.parseDouble(person_number) + "";
                            emp =
det.searchPersonDetails(null, null, id, null, null, null, null);
                            employeeData.put(person_number, emp);
                        }

                        if (emp != null) {
                            bean.setCode(code);
                            bean.setCreated_by("Bulk");

                            bean.setEit_name(element_name);
                            bean.setLine_manager(emp.getManagerOfMnagerName());
                            bean.setManage_of_manager(emp.getManagerOfMnagerName());
                            bean.setNationalIdentity(emp.getNationalId());
                            bean.setPersonName(emp.getDisplayName());
                            bean.setPerson_id(emp.getPersonId());
                            bean.setPerson_number(emp.getPersonNumber());
                            bean.setStatus("APPROVED");

                            bean.setCreation_date(convertDate(new Date()));
                            bean.setUrl(emp.getEmployeeURL().get(code) + "");
                        }
                    } else { //other cols
                        //create eit object
                        String key=meta_keys_row.getCell(cellIterator -1).toString();//key-must-be-in-elemententry-setup-context
                        
                        eitObj.put(key,
                                   val); //get eit key
                        String fieldName =
                            meta_app_cols_row.getCell(cellIterator -
                                                      1).toString();
                        if(key==null||key.length()==0){
                            System.out.println("the-col-"+headerCols.getCell(cellIterator).getStringCellValue()+"-has-no-key");
                                response.put("status", "Error");
                                response.put("message", "The Coloum \""+headerCols.getCell(cellIterator).getStringCellValue()+"\" Has No Value In Description Field, Please Navigate To (Manage Person Extensible Flexfield) And Set Description as API Name Value");
                            return response;
                            //this key has no-value-in-context-seup
                            //to-add-this-key-go-to-saas->home->maintainance->search->manage%per%ext->Manage Person Extensible Flexfield->edit->manage-context->search-with-code-xx->select-self-service->edit->select-col->edit->(Description-value- must-be-as-API Name)
                            }

                        //                        invoke setter by field name
                        try {
                            Method method =
                                bean.getClass().getMethod("set" + fieldName,
                                                          String.class);

                            method.invoke(bean, String.valueOf(val));
                        } catch (Exception ex) {
                           ex.printStackTrace(); AppsproConnection.LOG.error("ERROR", ex);
                        }
                    }
                }

                if (emp != null) {
                    bean.setEit(eitObj.toString());
                    eitObj.put("url", bean.getUrl());
                    //                eitObj.put("transactionTypeLbl", transactionType);
                    //                eitObj.put("governProcessingMonthLbl", governProcessingMonth);
                    bean.setEitLbl(eitObj.toString());

                    //send eit to paas and saas (postEEF)
                    PeopleExtraInformationBean peiBean =
                        PeopleExtraInformationDAO.getInstance().insertOrUpdateXXX_EIT(bean,
                                                                                      "BULK",
                                                                                      "Default");


                    //---------Build Element Entery ------------------
                    //2-get input values
                    ArrayList<ElementEntrySetupBean> elements =
                        ElementEntrySetupDAO.getInstance().getElementEntryByEIT(code);
                    if (elements.size() > 0) {
                        ElementEntrySetupBean element = elements.get(0);
                        JSONArray inputValues =
                            new JSONArray(element.getInputValue());

                        JSONObject elementJson = new JSONObject();
                        JSONArray newInputValues = new JSONArray();

                        for (int ivCounter = 0;
                             ivCounter < inputValues.length(); ivCounter++) {
                            JSONObject ivObject =
                                inputValues.getJSONObject(ivCounter);
                            String key =
                                ivObject.get("inputEitValue").toString();

                            JSONObject inputValuesLine = new JSONObject();
                            inputValuesLine.put("inputValueLbl",
                                                ivObject.get("inputValueLbl"));

                            if (ivObject.get("inputEitValue").equals("Transaction_ID")) {
                                inputValuesLine.put("inputEitValue",
                                                    "P" + peiBean.getId());
                            } else {
                                inputValuesLine.put("inputEitValue",
                                                    eitObj.has(key) ?
                                                    eitObj.get(key).toString() :
                                                    "");
                            }

                            newInputValues.put(inputValuesLine);
                        }
                        elementJson.put("inputValues", newInputValues);
                        elementJson.put("personNumber", emp.getPersonNumber());
                        elementJson.put("assignmentNumber",
                                        "E" + emp.getPersonNumber());
                        elementJson.put("elementName", element_name);
                        elementJson.put("effivtiveStartDate",
                                        element.getEffivtiveStartDate() !=
                                        null &&
                                        eitObj.get(element.getEffivtiveStartDate()) !=
                                        null ?
                                        eitObj.get(element.getEffivtiveStartDate()) :
                                        "");
                        elementJson.put("FULL_NAME", emp.getDisplayName());
                        elementJson.put("legislativeDataGroupName",
                                        element.getLegislativeDatagroupName());
                        elementJson.put("entryType", "E");
                        elementJson.put("creatorType",
                                        element.getCreatorType());
                        elementJson.put("sourceSystemId",
                                        "PaaS_" + peiBean.getId());
                        elementJson.put("trsId", peiBean.getId() + "");
                        elementJson.put("SSType", element.getRecurringEntry());
                        elementJson.put("eitCode", code);

                        //add to elementEnteryList
                        elementsJsonArray.put(elementJson);
                    }
                } else {
                    sheet =
                            markRowAsInvalid(wb, sheet, i, "Invalid Person Number");
                    allRowsIsValid = false;
                }
            }


            if (elementsJsonArray.length() > 0) {
                //handle element entery array
                String dat = createDatFileString(elementsJsonArray);

                boolean resp =
                    HCMElementEntryHelper.createAndSendZipFile("ElementEntry",
                                                               dat, null);

                //update tracking table
                for (int xx = 0; xx < elementsJsonArray.length(); xx++) {
                    String trs =
                        elementsJsonArray.getJSONObject(xx).get("trsId").toString();

                    TrackServerRequestBean b = new TrackServerRequestBean();
                    b.setRequestId(trs);
                    b.setElementResponseCode((resp ? 200 : 500) + "");
                    b.setElementResponseDesc(resp ? "Created" :
                                             "Internal Server Error");
                    b.setElementResponseDate(TrackServerRequestBean.getCurrentDateTime());
                    EFFDetails.getInstance().editElementResponseDetails(b);
                }
                //--------------------
                //2-save on DB
                //            return HDLHelper.insertHDLFile(null, trsId, ssType,
                //                                           "Upload Element File", null,
                //                                           fileContent, personNumber, toBytes,
                //                                           path,
                //                                           emp == null ? getPersonDetails(personNumber):emp,
                //                                           annualLeave,//
                //                                           filename, eitCode);

            }
            response.put("message", !allRowsIsValid ? workBookToString(wb) : "0");

        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            response.put("status", "Error");
            response.put("message", e.getMessage());
        }

        return response; //error while try later
    }

    private static String getCellValue(Cell poiCell) {
        if (poiCell.getCellType() == Cell.CELL_TYPE_NUMERIC &&
            DateUtil.isCellDateFormatted(poiCell)) {
            //get date
            Date date = poiCell.getDateCellValue();

            //set up formatters that will be used below
            SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm:ss");
            SimpleDateFormat formatYearOnly = new SimpleDateFormat("yyyy");

            /*get date year.
              *"Time-only" values have date set to 31-Dec-1899 so if year is "1899"
              * you can assume it is a "time-only" value
              */
            String dateStamp = formatYearOnly.format(date);

            if (dateStamp.equals("1899")) {
                //Return "Time-only" value as String HH:mm:ss
                return formatTime.format(date);
            } else {
                //here you may have a date-only or date-time value

                //get time as String HH:mm:ss
                String timeStamp = formatTime.format(date);

                if (timeStamp.equals("00:00:00")) {
                    //if time is 00:00:00 you can assume it is a date only value (but it could be midnight)
                    //In this case I'm fine with the default Cell.toString method (returning dd-MMM-yyyy in case of a date value)
                    return convertDate(poiCell.toString());
                } else {
                    //return date-time value as "dd-MMM-yyyy HH:mm:ss"
                    return poiCell.toString() + " " + timeStamp;
                }
            }
        }

        //use the default Cell.toString method (returning "dd-MMM-yyyy" in case of a date value)
        String val = poiCell.toString();
        try { //remove float point from number
            int newVal = (int)Double.parseDouble(val);
            return newVal + "";
        } catch (NumberFormatException e) {
        }

        return val;
    }


    private static JSONArray getValueListArray(JSONArray jsonArray) {
        JSONArray arr = new JSONArray();

        for (int x = 0; x < jsonArray.length(); x++)
            arr.put(jsonArray.getJSONObject(x).get("FLEX_VALUE"));
        return arr;
    }


    private static String convertDate(String val) {
        SimpleDateFormat source =
            new SimpleDateFormat("dd-MMM-yyyy"); // British format
        SimpleDateFormat target =
            new SimpleDateFormat("yyyy-MM-dd"); //element entery format

        try {
            return target.format(source.parse(val));
        } catch (ParseException e) {
            return val;
        }
    }

    private static String convertDate(Date date) {
        SimpleDateFormat target =
            new SimpleDateFormat("yyyy-MM-dd"); //element entery format
        return target.format(date);
    }

    private static String workBookToString(Workbook workbook) {
        try {
            ByteArrayOutputStream byteArrayOutputStream =
                new ByteArrayOutputStream();

            workbook.write(byteArrayOutputStream);
            String b64 =
                Base64.encodeBase64String(byteArrayOutputStream.toByteArray());
            return b64;
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }
        return null;
    }

    private static Sheet markRowAsInvalid(Workbook workbook, Sheet sheet,
                                          int i, String errorMsg) {

        CellStyle redBackgroundStyle = workbook.createCellStyle();
        redBackgroundStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
        redBackgroundStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
        redBackgroundStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
        //        Font font = workbook.createFont();
        //        font.setColor(IndexedColors.RED.getIndex());
        //        redBackgroundStyle.setFont(font);
        for (int x = 0; x < sheet.getRow(i).getLastCellNum(); x++)
            sheet.getRow(i).getCell(x).setCellStyle(redBackgroundStyle);

        sheet.getRow(i).createCell(sheet.getRow(i).getLastCellNum()).setCellValue(errorMsg);

        return sheet;
    }

    private static String createDatFileString(JSONArray arr) {
        StringBuilder sb = new StringBuilder();
        StringBuilder headers = new StringBuilder();
        StringBuilder bodies = new StringBuilder();
        for (int ii = 0; ii < arr.length(); ii++) {
            JSONObject jsonObj = arr.getJSONObject(ii);
            //------
            List<LineSourceElementEntryBean> declareVariableList =
                new ArrayList<LineSourceElementEntryBean>();
            List<LineSourceElementEntryBean> declareLineDataList =
                new ArrayList<LineSourceElementEntryBean>();
            LineSourceElementEntryBean declareVariable = null;
            try {
                //                EmployeeBean emp = null;
                String ssType = jsonObj.getString("SSType");
                String sourceSystemOwner = "HHA";
                String sourceSystemId = jsonObj.getString("sourceSystemId");
                String StartDate =
                    jsonObj.get("effivtiveStartDate").toString(); //key in json
                String EffectiveStartDate =
                    getEffictiveStartDate(StartDate); //"%EffectiveStartDate%";
                String EffectiveEndDate =
                    ssType.equals("R") ? "" : ssType.equals("N") ?
                                              HCMElementEntryHelper.getlastDayofCuurentMonth() :
                                              "4712/12/31";
                String ElementName = jsonObj.getString("elementName");
                String LegislativeDataGroupName =
                    jsonObj.getString("legislativeDataGroupName");
                String AssignmentNumber =
                    jsonObj.getString("assignmentNumber");
                String EntryType = jsonObj.getString("entryType");
                String CreatorType = jsonObj.getString("creatorType");
                String personNum = jsonObj.getString("personNumber");
                String eitCode = jsonObj.getString("eitCode");
                //
                String lineSourceSystemOwner = sourceSystemOwner;
                String lineElementEntryId = sourceSystemId;
                String lineEffectiveStartDate = EffectiveStartDate;
                String lineEffectiveEndDate = EffectiveEndDate;
                String lineElementName = ElementName;
                String lineLegislativeDataGroupName = LegislativeDataGroupName;
                String lineAssignmentNumber = AssignmentNumber;
                String trsId = jsonObj.get("trsId").toString();
                String inputValues = jsonObj.get("inputValues").toString();

                JSONArray jsonArr = new JSONArray(inputValues);

                JSONObject jsonObjInput;
                for (int i = 0; i < jsonArr.length(); i++) {
                    declareVariable = new LineSourceElementEntryBean();
                    jsonObjInput = jsonArr.getJSONObject(i);
                    declareVariable.setLineSourceSystemId(personNum + "_" +
                                                          eitCode + "_" +
                                                          trsId + "_" +
                                                          jsonObjInput.get("inputValueLbl").toString().replaceAll(" ",
                                                                                                                  "_"));
                    declareVariable.setLineInputValueName(jsonObjInput.get("inputValueLbl").toString());
                    declareVariable.setLineScreenEntryValue((jsonObjInput.isNull("inputEitValue") ?
                                                             "" :
                                                             jsonObjInput.get("inputEitValue").toString()));
                    declareVariableList.add(declareVariable);
                }
                for (LineSourceElementEntryBean lineSourceElementEntryBean :
                     declareVariableList) {
                    declareVariable = new LineSourceElementEntryBean();
                    declareVariable.setLineData("MERGE|ElementEntryValue|" +
                                                lineSourceSystemOwner + "|" +
                                                lineSourceElementEntryBean.getLineSourceSystemId() +
                                                "|" + lineElementEntryId +
                                                "|" + lineEffectiveStartDate +
                                                "|" + lineEffectiveEndDate +
                                                "|" + lineElementName + "|" +
                                                lineLegislativeDataGroupName +
                                                "|" + lineAssignmentNumber +
                                                "|" +
                                                lineSourceElementEntryBean.getLineInputValueName() +
                                                "|" +
                                                lineSourceElementEntryBean.getLineScreenEntryValue());
                    if (!lineSourceElementEntryBean.getLineScreenEntryValue().isEmpty()) {
                        declareLineDataList.add(declareVariable);
                    }
                }
                String header =
                    "MERGE|ElementEntry|" + sourceSystemOwner + "|" +
                    sourceSystemId + "|" + EffectiveStartDate + "|" +
                    EffectiveEndDate + "|" + ElementName + "|" +
                    LegislativeDataGroupName + "|" + AssignmentNumber + "|" +
                    EntryType + "|" + CreatorType + "\r\n";

                headers.append(header);

                String body = "";
                for (LineSourceElementEntryBean lineSourceElementEntryBean :
                     declareLineDataList) {
                    body += lineSourceElementEntryBean.getLineData() + "\r\n";
                }
                bodies.append(body);
                //                String subContent = HCMElementEntryHelper.ELEMENT_HEADER + "\r\n"+ header + HCMElementEntryHelper.ELEMENT_LINE+ "\r\n"+body;
                //
                //
                ////save each row as element entery in db
                //                HCMElementEntryHelper.uploadAndLoadFile(filename, subContent, subContent, trsId,
                //                                        ssType, personNum, filename, eitCode,
                //                                        emp);

            } catch (Exception e) {
               e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            }
            //------
        }

        //header metadata
        sb.append(HCMElementEntryHelper.ELEMENT_HEADER);
        sb.append("\n");
        //add headers
        sb.append(headers);

        //body metadata
        sb.append(HCMElementEntryHelper.ELEMENT_LINE);
        //add bodies
        sb.append("\n");
        sb.append(bodies);
        return sb.toString();
    }

    private static String getEffictiveStartDate(String startDate) {
        if (startDate.equals("%EffectiveStartDate%")) {
            return HCMElementEntryHelper.getFirstDayofCuurentMonth();
        } else {
            if (startDate.contains("/") || startDate.contains("-")) {
                return HCMElementEntryHelper.GET_FIRST_DATE_OF_SEPECIFIC_MONTH(startDate).replaceAll("-",
                                                                                                     "/");
            } else {
                BIReportModel biModel = new BIReportModel();
                Map<String, String> reportMaping =
                    new HashMap<String, String>();
                reportMaping.put("TIME_PERIOD_ID", startDate);

                JSONObject RES =
                    biModel.runReport("GET_TIME_PERIOD_REPORT", reportMaping);

                if (RES != null && RES.has("DATA_DS") &&
                    RES.getJSONObject("DATA_DS").has("G_1")) {
                    JSONObject obj =
                        RES.getJSONObject("DATA_DS").getJSONObject("G_1");
                    String PERIOD_NAME = obj.get("PERIOD_NAME").toString();
                    return HCMElementEntryHelper.PERIOD_NAME_Date_Format(PERIOD_NAME);
                }
                return HCMElementEntryHelper.getFirstDayofCuurentMonth();
            }
        }
    }

    public static void main(String[] args) {
        //        dynamicanalysisExcelBulk(new File("D:\\hussein\\ahmed\\Downloads\\Government_Relationships.xls"));
        try {
            //XXX_HR_EXEMPTION_REQ_FOR_EMP
            //XXX_HR_MEDICAL_INSUR_FOR_EMP
            //XXX_HR_REJOIN_LEAVE_REQUEST
            getInstance().dynamicGenerate("XXX_HR_REJOIN_LEAVE_REQUEST");
        } catch (FileNotFoundException e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } catch (IOException e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }
    }
}
