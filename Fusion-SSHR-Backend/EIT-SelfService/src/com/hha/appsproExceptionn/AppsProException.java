package com.hha.appsproExceptionn;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.LogsBean;

import javax.servlet.GenericServlet;

import com.appspro.fusionsshr.dao.LogsDAO;


public class AppsProException extends Exception {


    public AppsProException(Throwable throwable) {
        super(throwable);
        //Insert database log
        AppsproConnection.LOG.error("ERROR", throwable);
        throwable.printStackTrace();
    }

    public AppsProException(String message, Throwable throwable) {
        super(throwable);
        AppsproConnection.LOG.error(message, throwable);
        throwable.printStackTrace();
    }

    public AppsProException(String message) {
        super(message);
    }

    public AppsProException() {
        super();
    }
    

}
