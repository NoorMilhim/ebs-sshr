package common.login;

import java.io.IOException;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.ServletException;

public class logOut extends HttpServlet {

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getSession().invalidate();
        //  request.getSession().removeAttribute("EmpDetails");
        String serverName = request.getScheme() + "://" + request.getServerName();

        if (!serverName.contains(".com")) {
            serverName += ":" + request.getServerPort();
        }
        // Welcome@123
        serverName += request.getContextPath() + "/index.html";
        response.sendRedirect("https://hhajcs-a562419.java.em2.oraclecloudapps.com/Fusion-SSHR/");
    }
}
