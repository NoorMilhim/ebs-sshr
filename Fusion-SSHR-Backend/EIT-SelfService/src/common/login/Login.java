/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common.login;

import com.appspro.db.AppsproConnection;

import common.restHelper.RestHelper;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
//import org.xml.sax.InputSource;

public class Login extends RestHelper {

    public Login() {
        super();
    }

    public boolean validateLogin(String userName, String password) {
        String output = "";
        try {

//            String soapRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:pub=\"http://xmlns.oracle.com/oxp/service/PublicReportService\">\n"
//                    + "   <soapenv:Header/>\n"
//                    + "   <soapenv:Body>\n"
//                    + "      <pub:validateLogin>\n"
//                    + "         <pub:userID>" + userName + "</pub:userID>\n"
//                    + "         <pub:password>" + password + "</pub:password>\n"
//                    + "      </pub:validateLogin>\n"
//                    + "   </soapenv:Body>\n"
//                    + "</soapenv:Envelope>";
            
           String soapRequest =  "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:val=\"http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxrasf_intg_pkg/validate_login/\" xmlns:xxr=\"http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxrasf_intg_pkg/\">\n" + 
            "   <soapenv:Header>\n" + 
            "      <wsse:Security soapenv:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">\n" + 
            "         <wsse:UsernameToken wsu:Id=\"UsernameToken-8EA71DD8F45623F862158507343367916\">\n" + 
            "            <wsse:Username>sysadmin</wsse:Username>\n" + 
            "            <wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">sysadmin</wsse:Password>\n" + 
            "            <wsse:Nonce EncodingType=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary\">lyuam//Xpysh4Mw212bFXg==</wsse:Nonce>\n" + 
            "            <wsu:Created>2020-03-24T18:10:33.678Z</wsu:Created>\n" + 
            "         </wsse:UsernameToken>\n" + 
            "      </wsse:Security>\n" + 
            "      <xxr:SOAHeader></xxr:SOAHeader>\n" + 
            "   </soapenv:Header>\n" + 
            "   <soapenv:Body>\n" + 
            "      <val:InputParameters>\n" + 
            "         <!--Optional:-->\n" + 
            "         <val:P_USER_NAME>"+userName+"</val:P_USER_NAME>\n" + 
            "         <!--Optional:-->\n" + 
            "         <val:P_PASSWORD>" + password + "</val:P_PASSWORD>\n" + 
            "         <!--Optional:-->\n" + 
            "         <val:P_LANG>ar</val:P_LANG>\n" + 
            "      </val:InputParameters>\n" + 
            "   </soapenv:Body>\n" + 
            "</soapenv:Envelope>";
            

            System.setProperty("DUseSunHttpHandler", "true");

            byte[] buffer = new byte[soapRequest.length()];
            buffer = soapRequest.getBytes();
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            bout.write(buffer);
            byte[] b = bout.toByteArray();
            java.net.URL url = new URL(null, this.getEBSInstance(), new sun.net.www.protocol.http.Handler());
            java.net.HttpURLConnection http;
            if (url.getProtocol().toLowerCase().equals("http")) {
                trustAllHosts();
                java.net.HttpURLConnection https = (HttpURLConnection) url.openConnection();
                System.setProperty("DUseSunHttpHandler", "true");
                //https.setHostnameVerifier(DO_NOT_VERIFY);
                http = https;
            } else {
                http = (HttpURLConnection) url.openConnection();
            }
            String SOAPAction = "";
            //            http.setRequestProperty("Content-Length", String.valueOf(b.length));
            http.setRequestProperty("Content-Length",
                    String.valueOf(b.length));
            http.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
            http.setRequestProperty("SOAPAction", SOAPAction);
            http.setRequestMethod("POST");
            http.setDoOutput(true);
            http.setDoInput(true);
            OutputStream out = http.getOutputStream();
            out.write(b);
            InputStream is = http.getInputStream();
            String responseXML = getStringFromInputStream(is);

            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource src = new InputSource();
            src.setCharacterStream(new StringReader(responseXML));

            Document doc = builder.parse(src);
           // Document inp  = builder.parse (doc.getElementsByTagName("OutputParameters").item(0).getTextContent());
            System.out.println("----------"+doc.getElementsByTagName("env:Body").item(0).getTextContent());
            String data = doc.getElementsByTagName("env:Body").item(0).getTextContent();
            
          
            AppsproConnection.LOG.info(data);
            if(data.equals("S")){
                return new Boolean(true);
            }else{
                    return new Boolean(false);
                }
           

        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }
        return false;
    }
}
