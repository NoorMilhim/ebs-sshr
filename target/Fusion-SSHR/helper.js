function addFuseModelToIndexedDB(services, app, rootViewModel, commonhelper) {
    services.getFuseModelReport().then(function (data) {


        rootViewModel.globalFuseModel([]);
      //  rootViewModel.globalFuseModel(jQuery.parseJSON(data));
        rootViewModel.globalFuseModel(data);
        populateGlobalValues(rootViewModel, commonhelper);

    },
            app.failCbFn);

}
function populateGlobalValues(rootViewModel, commonhelper) {
    rootViewModel.globalHRCYesNo([]);
    rootViewModel.globalJobName([]);
    rootViewModel.globalJobCatagory([]);
    rootViewModel.globalGeneralGroup([]);
    rootViewModel.globalGroupType([]);
    rootViewModel.globalEFFUDT([]);
    rootViewModel.globalJobCodes([]);
    rootViewModel.globalHrGrades([]);
    rootViewModel.globalPositionsName([]);
    rootViewModel.globalPositionsAction([]);
    rootViewModel.globalEitNameReport([]);
    rootViewModel.globalJobsNames([]);
    rootViewModel.globalEITReportLookup([]);
    rootViewModel.globalEITDefultValueParamaterLookup([]);
    for (var i = 0; i < rootViewModel.globalFuseModel().length; i++) {
        if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.HRC_YES_NO) {
            rootViewModel.globalHRCYesNo.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR
            });
        }
        else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.EIT_Name) {
            rootViewModel.globalEitNameReport.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR
            });
        }
          else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.jobName) {
            rootViewModel.globalJobName.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR
            });
        }
        else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.XXX_HR_JOB_CATEGORY) {
            rootViewModel.globalJobCatagory.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR
            });
        } else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.HRDYNAMICCOLUMNREPORT) {
            rootViewModel.globalEITReportLookup.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR
            });
        }
       else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.XXX_HR_JOB_GENERAL_GROUP) {
            rootViewModel.globalGeneralGroup.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR
            });
        }else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.HR_DYNAMIC_COLUMN_REPORT) {
            rootViewModel.globalEITDefultValueParamaterLookup.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].DESCRIPTION, "labelAr": rootViewModel.globalFuseModel()[i].DESCRIPTION
            });
        }
        else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.JOB_TYPE_GROUP) {
            rootViewModel.globalGroupType.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR
            });
        }
        else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.HR_JOB_CODES) {
            rootViewModel.globalJobCodes.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR,"description":rootViewModel.globalFuseModel()[i].DESCRIPTION 
            });
        }        
        else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.HR_GRADE) {
            rootViewModel.globalHrGrades.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR
            });
        }
        else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.HR_POSITIONS_NAME) {
            rootViewModel.globalPositionsName.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR
            });
        }
        else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.HR_POSITION_ACTIONS) {
            rootViewModel.globalPositionsAction.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR
            });
        }        
        else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.HR_QUALIFICATIONS) {
            rootViewModel.globalQualifications.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR
            });
        }else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.UDT && rootViewModel.globalFuseModel()[i].USER_TABLE_NAME === commonhelper.HR_EFF_SEARCH) {
            rootViewModel.globalEFFUDT.push({
                "displaySequence": rootViewModel.globalFuseModel()[i].DISPLAY_SEQUENCE, "rowLowRangeOrName": rootViewModel.globalFuseModel()[i].ROW_LOW_RANGE_OR_NAME,
                "rowName": rootViewModel.globalFuseModel()[i].ROW_NAME , "userColumnName":rootViewModel.globalFuseModel()[i].USER_COLUMN_NAME,
                "userTableName":rootViewModel.globalFuseModel()[i].USER_TABLE_NAME
            });
            
         
        }
      
    }
      
}
function drawAttachment (count){
   //drawLabel(lblValue, div);
      console.log("draw1");

   var input = document.createElement("img");
    
   input.setAttribute('class','attClass'+count);
   input.setAttribute('alt',"Image preview...");
   input.setAttribute('height',"60");
   input.setAttribute('width',"60");
    var parent = document.getElementById("attachedfile"); 
  
    // return input ;
  parent.appendChild(input);
}

function drawAttachment2 (count, baseStr64){
   //drawLabel(lblValue, div);
   console.log("draw2");
   var input = document.createElement("img");
    
   input.setAttribute('class','attClass'+count);
   input.setAttribute('alt',"Image preview...");
   input.setAttribute('height',"60");
   input.setAttribute('width',"60");
   input.setAttribute('src', baseStr64);
    var parent = document.getElementById("attachedfile"); 
  
    // return input ;
  parent.appendChild(input);
}


var count = 0; 
  var checkPreviews ={};
  var preview;
function prev(files) {
    console.log(files);
  drawAttachment(count);
  
     preview = document.querySelector('.attClass'+count);
     preview1 = document.querySelector('.attClass1');
     preview2 = document.querySelector('.attClass2');
    var xx = 0;
  
    checkPreviews['checkpreview'+count]=true;
    
    var checkpreview1 = true;
    var checkpreview2 = true;
    var checkpreview3 = true;
      for (var i = 0; i < files.length; i++) {
        if( checkPreviews['checkpreview'+count]) {
              checkPreviews['checkpreview'+count] = false;
              setupReader(files[i],preview);
           
            continue;
        }
    
      
    
    }
    count++;
        

};

function setupReader(file,preview) {
    var name = file.name;
    var reader = new FileReader();  
    reader.onload = function(e) {  
        // get file content  
        var text = e.target.result; 
        preview.src = reader.result;
         
            if (file) {
                reader.readAsDataURL(file);
            }
    };
    reader.readAsText(file, "UTF-8");
}


function showEmplDetails(ko, services, app, km) {

    var rootViewModel = app;
    var url = new URL(window.location.href);
    var username;
    var hosturl;
    var jwt;
    
    if (url.searchParams.get("jwt")) {
        if (rootViewModel.hostUrl() == null || rootViewModel.hostUrl().length == 0) {
            hosturl = url.searchParams.get("host");
            rootViewModel.hostUrl(hosturl);
        }

        if (rootViewModel.jwt() == null || rootViewModel.jwt().length == 0) {
            jwt = url.searchParams.get("jwt");
            ;
            rootViewModel.jwt(jwt);
        }

if (rootViewModel.userName() == null || rootViewModel.userName().length == 0) {
            jwtJSON = jQuery.parseJSON(atob(jwt.split('.')[1]));
            username = jwtJSON.sub;
            console.log(username);
            rootViewModel.userName(username);
          //  displayname = userName.split('.')[0] + username.split('.')[1];
        }
    }

    var getEmpDetails = function (data) {
        console.log(data);
        if (!data) {
            return false;
        } else {
            rootViewModel.personDetails(data);
            if (rootViewModel.personDetails().picBase64) {
                if($(".profilepic")[0]){
                    $(".profilepic")[0].src = "data:image/png;base64," + rootViewModel.personDetails().picBase64;
                }if($(".profilepic")[1]){
                    $(".profilepic")[1].src = "data:image/png;base64," + rootViewModel.personDetails().picBase64;
                }
                
                
            }
            return true;
        }
    };
 
    services.getEmpDetails(rootViewModel.userName(), rootViewModel.jwt(), rootViewModel.hostUrl()).then(getEmpDetails, app.failCbFn);

}
function searchArray(nameKey, searchArray) {
   for (var i = 0;i < searchArray.length;i++) {
       if (searchArray[i].value === nameKey) {
           return searchArray[i].label;
       }
   }
}

function searchArrayValue(nameKey, searchArray) {
   for (var i = 0;i < searchArray.length;i++) {
       if (searchArray[i].label === nameKey) {
           return searchArray[i].value;
       }
   }
}
function searchArrayContainValue(nameKey, searchArray) {
    var arr = [];
   for (var i = 0;i < searchArray.length;i++) {
       if (searchArray[i].value === nameKey) {
           arr.push(searchArray[i].label) ;
       }
   }
   return arr; 
}
//build addational details for job screens
function buildJobNavigationPage(oj) {
    var getTranslation = oj.Translations.getTranslatedString;
    var navDataLeft = [];

    navDataLeft = [
   {
       name: getTranslation("pages.job"), id: 'searchJob',iconClass : 'oj-navigationlist-item-icon fa fa-briefcase'
   },

    {
        name : getTranslation("job.additionalDetails"), id : 'additionalDetailsSummary', iconClass : 'oj-navigationlist-item-icon fa fa-pencil-square-o'
    },
    {
        name : getTranslation("others.validGrade"), id : 'validGradeSummary', iconClass : 'oj-navigationlist-item-icon fa fa-tasks'
    }
];
    return navDataLeft;
}
//build addational details for positions screens
function buildPositionNavigationPage(oj) {
    var getTranslation = oj.Translations.getTranslatedString;
    var navDataLeft = [];

    navDataLeft = [
   {
       name: getTranslation("pages.position"), id: 'searchPosition',iconClass : 'oj-navigationlist-item-icon fa fa-address-book-o'
   },

    {
        name : getTranslation("position.positionSummaryAdditionalDetails"), id : 'positionSummaryAdditionalDetails', iconClass : 'oj-navigationlist-item-icon fa fa-pencil-square-o'
    }
];
    return navDataLeft;
}
function getPaaSLookup(services, app, rootViewModel ,commonhelper) {
    var getLookup = function (data) {
      rootViewModel.PaaSLookup([]);
        for (var i = 0;i < data.length;i++) {
            rootViewModel.PaaSLookup.push( {
                "ID" : data[i].id, "name" : data[i].name, "code" : data[i].code, "valuAr" : data[i].valueArabic, "valuEn" : data[i].valueEnglish

            });

        }
        console.log(rootViewModel.PaaSLookup());

    };
    
    
    
    services.getGeneric(commonhelper.getAllPaaSLookup).then(getLookup, app.failCbFn);
   
}


function getEITIcons(services, app, rootViewModel ,commonhelper) {
    var getIcons = function (data) {
      rootViewModel.EITIcons([]);
        for (var i = 0;i < data.length;i++) {
            rootViewModel.EITIcons.push( {
                "ID" : data[i].id, "eitCode" : data[i].eitCode, "icons" : data[i].icons

            });

        }
        console.log(rootViewModel.EITIcons());

    };
  
    services.getGeneric(commonhelper.getIcons).then(getIcons, app.failCbFn);
   
}

function getPersonNumber(services, app, rootViewModel ,commonhelper) {
    var getpersonNumber= function (data) {
      rootViewModel.PersonNumberArr([]);
        for (var i = 0;i < data.length;i++) {
            rootViewModel.PersonNumberArr.push( {
                "value" : data[i].PERSON_ID, "label" : data[i].FULL_NAME

            });

        }
        console.log(rootViewModel.PersonNumberArr());

    };
   var reportPaylod = {"reportName": "employeeNameReport"};
    services.getGenericReport(reportPaylod).then(getpersonNumber, app.failCbFn);
   
}

function getPaaSDefultValue(eitCode, segmentName, paasDefultValue) {
    var query;
    //self.passReturnArr([]);
    for (var i = 0; i < paasDefultValue.length; i++) {
        if (paasDefultValue[i].eitCode == eitCode && paasDefultValue[i].segmentName == segmentName) {

            query = paasDefultValue[i].query;

        }
    }

    return query;

}
