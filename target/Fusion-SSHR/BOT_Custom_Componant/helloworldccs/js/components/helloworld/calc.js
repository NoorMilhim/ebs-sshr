"use strict";
module.exports = {
    metadata: () => ({
            "name": "Calc",
            "properties": {
                "amountOfFunding": {"type": "int", "required": true},
                "prudect": {"type": "string", "required": true},
                "typeOfFunding": {"type": "string", "required": true},
                "monthlyIncome": {"type": "int", "required": true},
                "downpAYMENT": {"type": "int", "required": true},
                "lastInstallment": {"type": "int", "required": true},
                "duration": {"type": "int", "required": true},
                "keepTurn": {"type": "boolean", "required": false}
            },
            "supportedActions": [
            ]
        }),
    invoke: (conversation, done) => {
        var prudect = conversation.properties().prudect;
        var keepTurn = conversation.properties().keepTurn;
        var lang = conversation.variable("lang");
        var monthlyIncome = conversation.properties().monthlyIncome;
        var contractType = conversation.properties().typeOfFunding;
        var amountOfFunding = conversation.properties().amountOfFunding;
        var financingCommission ;
        var totalPremiums;
        var monthlyInstallment;
        var carPrice;
        var firstInstallment = conversation.properties().downpAYMENT;   
        var FinancePer = 0.10;
        var MurabahaPer = 0.09;
        var InsurancePer = 0.073;
        var insuranceCommission;
        var totalRents;
        var valueOfOwnership;
        var percentageOfOwnership = conversation.properties().lastInstallment;
        var duration = conversation.properties().duration;

        if (contractType == "Murabaha") {
            
            financingCommission = amountOfFunding * MurabahaPer * (duration / 12);
            totalPremiums = amountOfFunding + financingCommission;
            monthlyInstallment = totalPremiums / duration;
        } else if (contractType == "Financial Leasing") {
            carPrice = amountOfFunding + firstInstallment;
            financingCommission = amountOfFunding * FinancePer * (duration / 12);
            insuranceCommission = carPrice * InsurancePer * (duration / 12);
            totalRents = amountOfFunding + financingCommission + insuranceCommission;
            valueOfOwnership = carPrice * percentageOfOwnership;
            monthlyInstallment = (totalRents - valueOfOwnership) / duration;
        }
		var valid = true;
        var errorMessage = "xxx";

		
        if (monthlyInstallment > (monthlyIncome / 3)) {
		valid = false;
                if (lang === 'en') {
                    errorMessage = "Monthly Payment should not exceed 33% of your monthly Income!";
                } else {
                    errorMessage = "الدفعة الشهرية يجب ألا تتعدى 33% من الدخل الشهري!";
                }
            } else if (monthlyInstallment <= 0) {
		valid = false;
                if (lang === 'en') {
                    errorMessage = "Monthly Payment should not be less than minimum monthly payment!";
                } else {
                    errorMessage = "يجب أن لا يتعدى الدفعة الشهري الحد الادنى";
                }
            } else if (monthlyIncome < 2500) {
		valid = false;
                if (lang === 'en') {
                    errorMessage = "Monthly income must be at least 2500!";
                } else {
                    errorMessage = "يجب أن لا يقل الدفعة الشهري عن 2500";
                }
            } else if (contractType === "Financial Leasing") {
                if (valueOfOwnership > totalPremiums) {
		    valid = false;
                    if (lang === 'en') {
                        errorMessage = "Residual Value should not exceed the total cost!";
                    } else {
                        errorMessage = "دفعة التملك يجب أن لا تتجاوز مبلغ التمويل!";
                    }
                }
            } else if (contractType === "Murabaha") {
                if (valueOfOwnership > totalRents) {
		    valid = false;
                    if (lang === 'en') {
                        errorMessage = "Residual Value should not exceed the total cost!";
                    } else {
                        errorMessage = "دفعة التملك يجب أن لا تتجاوز مبلغ التمويل!";
                    }
                }
            } else if (valueOfOwnership > 25) {
		valid = false;
                if (lang === 'en') {
                    errorMessage = "Residual Value should not exceed 25% of the total cost!";
                } else {
                    errorMessage = "يجب أن لا تتجاوز دفعة التملك 25? من مبلغ التمويل!";
                }
            } else {
                valid = true;
            }

	
        if (valid) {
        if (lang === 'en') {
            conversation.reply(
                    {text: 'Your amount of funding: ' + amountOfFunding +
                                '\n Duration: ' + duration+' months' + '\n Payments monthly : ' +
                                Math.ceil(monthlyInstallment) + '.'});
        } else {
            conversation.reply({text: 'مبلغ التمويل:' +
                        amountOfFunding + ' \nمدة التمويل:' + duration +'  شهر  ' + '\nالدفعةالشهرية:' +
                        Math.ceil(monthlyInstallment) + ' . '});
        }

        } else {
            conversation.reply(
                    {text: errorMessage + '.'});
        }
        
        
        conversation.keepTurn(keepTurn);
        conversation.transition();
        done();

var validation = function () {
if (monthlyInstallment > (monthlyIncome / 3)) {
                if (lang === 'en') {
                    errorMessage = "Monthly Payment should not exceed 33% of your monthly Income!";
                } else {
                    errorMessage = "الدفعة الشهرية يجب ألا تتعدى 33% من الدخل الشهري!";
                }
            } else if (monthlyInstallment <= 0) {
                if (lang === 'en') {
                    errorMessage = "Monthly Payment should not be less than minimum monthly payment!";
                } else {
                    errorMessage = "يجب أن لا يتعدى الدفعة الشهري الحد الادنى";
                }
            } else if (monthlyIncome < 2500) {
                if (lang === 'en') {
                    errorMessage = "Monthly income must be at least 2500!";
                } else {
                    errorMessage = "يجب أن لا يقل الدفعة الشهري عن 2500";
                }
            } else if (contractType === "Financial Leasing") {
                if (valueOfOwnership > totalPremiums) {
                    if (lang === 'en') {
                        errorMessage = "Residual Value should not exceed the total cost!";
                    } else {
                        errorMessage = "دفعة التملك يجب أن لا تتجاوز مبلغ التمويل!";
                    }
                }
            } else if (contractType === "Murabaha") {
                if (valueOfOwnership > totalRents) {
                    if (lang === 'en') {
                        errorMessage = "Residual Value should not exceed the total cost!";
                    } else {
                        errorMessage = "دفعة التملك يجب أن لا تتجاوز مبلغ التمويل!";
                    }
                }
            } else if (percentageOfOwnership > 25) {
                if (lang === 'en') {
                    errorMessage = "Residual Value should not exceed 25% of the total cost!";
                } else {
                    errorMessage = "يجب أن لا تتجاوز دفعة التملك 25? من مبلغ التمويل!";
                }
            } else {
                valid = true;
            }
	}
    }
};
