define([], function () {

    function commonHelper() {
        var self = this;

        self.getSaaSHost = function () {
            var host = "http://192.168.1.118:7101/EBSMobile/rest";
            return host;
        };

        self.getPaaSHost = function () {
            var host = "https://apex-hcuk.db.em2.oraclecloudapps.com/apex/xx_selfService/";
            return host;
        };

        self.getAppHost = function () {
            var host = "/Fusion-SSHR/";
            return host;
        };
        self.HRC_YES_NO = 'HRC_YES_NO';
        self.jobHisoryUrl="jobs/search/";
        self.validGradeUrl="validGrade/";
        self.supervisorallowanceUrl="supervisorallowance/";
        self.requirededuicationlevelUrl="requirededuicationlevel/";
        self.securityallowanceUrl="securityallowance/";
        self.clothesallowanceUrl="clothesallowance/";
        self.requiredqualificationUrl="requiredqualification/";
        self.jobContUrl="jobs/jobobCount";
        self.positionSearchUrl ="position/search/";
        self.positionUrl="position/";
        self.worknatureallowanceUrl="worknatureallowance/";
        self.positionSearchCodeUrl ="position/searchbycode/";
        self.workNatureAllowanceUrl ="worknatureallowance/";
        self.orgnizationUrl = "org/";
        self.jobName='XXX_HR_JOB_NAME';
        self.JOB_TYPE_GROUP='XXX_HR_JOB_TYPE_GROUP';
        self.XXX_HR_JOB_GENERAL_GROUP='XXX_HR_JOB_GENERAL_GROUP';
        self.XXX_HR_JOB_CATEGORY='XXX_HR_JOB_CATEGORY';
        self.HR_JOB_CODES ='XXX_HR_JOB_CODES';
        self.HR_GRADE = 'XXX_HR_GRADE';
        self.HR_POSITIONS_NAME= 'XXX_HR_POSITIONS_NAME';
        self.HR_POSITION_ACTIONS ='XXX_HR_POSITION_ACTIONS';
        self.HR_QUALIFICATIONS = 'XXX_HR_QUALIFICATIONS';
        self.getEffData = "eff/details/";
        self.jobFutureUrl= "jobs/jobFuture";
        
       
        
        self.REPORT_DATA_TYPE = {
            UDT : 'UDT', LOOKUP : 'LOOKUP', TERRITORIES : 'TERRITORIES'
        };

        self.FUSE_SERVICE_PARAM = {
            BINDLOOKUPTYPE : self.BTRIP_TYPE + ',' + self.YES_NO + ',' + self.ADV_MONTHS + ',' + self.SA_BANKS + ',' + self.BTRIPDRIVER_TYPE + ',' + self.BTRIPDRIVER_AREA + ',' + self.HRC_YES_NO + ',' + self.NADEC_HR_ID_MAIL_TYPE + ',' + self.NADEC_HR_IDENTIFICATION_LANG + ',' + self.HR_TICKETS_ROUTES + ',' + self.HR_TICKETS_REASONS + ',' + self.HR_TICKETS_CLASS + ',' + self.HR_GRIEVANCE_TYPE + ',' + self.HR_GRIEVANCE_STATUS + ',' + self.HR_MGR_GRIEVANCE_STATUS + ',' + self.HR_HEAD_GRIEVANCE_STATUS + ',' + self.EDU_YEARS + ',' + self.EDU_SEMESTER + ',' + self.HR_ALLOWANCES + ',' + self.NADEC_BTR_ROUTE_TYPE + ',' + self.HR_HEAD_HR_GRIEVANCE_STATUS + ',' + self.NADEC_CAR_INSIDE_LOV+ ',' + self.HR_CAR_LOCATION+','+ self.NADEC_TICKET_RAUTES_NEW_US+','+self.HR_NADEC_HOUSING_PERIOD+','+ self.NADEC_TICKET_REFUND+','+ self.NADEC_TRIP_DIRECTION_1+','+self.NADEC_TICKET_RAUTES_NEW_US,             
            P_TABLE_NAME : 'XXX_HR_REG_BTRIP_DAYS_B,XXX_HR_TRAIN_BTRIP_DAYS_B,XXX_HR_REG_BTRIP_DAYS_A,XXX_HR_TRAIN_BTRIP_DAYS_A,XXX_HR_REG_BTRIP_PERDIEM,XXX_HR_TRAIN_BTRIP_PERDIEM,XXX_HR_REG_BTRIP_TICKET,XXX_HR_TRAIN_BTRIP_TICKET,XXX_HR_PART_OF_EOS_AMT,XXX_HR_GLOBAL_VALUES,XXX_HR_ALLOWANCES_DETAILS,' + self.NADEC_BTR_ROUTE_TYPE, BINDLANGUAGE : 'US'
        };

        self.getBiReportServletPath = function () {
            var host = "report/commonbireport";
            return host;
        };

        self.getInternalRest = function () {
            var host = "webapi/";
            return host;
        };

    }

    return new commonHelper();
});