define(['ojs/ojcore', 'knockout', 'util/commonhelper', 'config/services', 'knockout-mapping', 'notify', 'ojs/ojmodule-element-utils', 'ojs/ojarraydataprovider', 'ojs/ojmodule-element', 'ojs/ojrouter',
    'ojs/ojknockout', 'ojs/ojarraytabledatasource', 'ojs/ojknockout', 'ojs/ojavatar', 'ojs/ojoffcanvas', 'ojs/ojknockout-validation', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojmenu', 'ojs/ojoption', 'ojs/ojdialog', 'ojs/ojlabel', , 'ojs/ojselectcombobox', 'ojs/ojpopup', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojmessage'],
    function (oj, ko, commonhelper, services, km, n, moduleUtils, ArrayDataProvider) {
        function ControllerViewModel() {
            const self = this;
            const getTranslation = oj.Translations.getTranslatedString;
            self.isGettingMaster = ko.observable(false);
            self.rootViewModel = ko.dataFor(document.getElementById('globalBody'));
            var smQuery = oj.ResponsiveUtils.getFrameworkQuery(oj.ResponsiveUtils.FRAMEWORK_QUERY_KEY.SM_ONLY);
            var mdQuery = oj.ResponsiveUtils.getFrameworkQuery(oj.ResponsiveUtils.FRAMEWORK_QUERY_KEY.MD_UP);
            var pageBody = $('*');
            self.iconsArr = ko.observableArray([]);
            self.actionStatus = ko.observable("0");
            self.tempIcons = ko.observableArray([]);
            self.tempIcons().length = 13;
            self.valueofsharethouts = ko.observable();
            self.countPending = ko.observable(0);
            self.countApproved = ko.observable(0);
            self.countRejected = ko.observable(0);
            self.jobLbl = ko.observable();
            self.employeeNewsLbl = ko.observable();
            self.myFlagsLbl = ko.observable();
            self.initials = ko.observable();
            self.image = ko.observable();

            self.position =
                {
                    "my": { "vertical": "top", "horizontal": "end" },
                    "at": { "vertical": "top", "horizontal": "end" },
                    "of": "window"
                };
            self.messages = ko.observableArray([]);
            self.messagesDataProvider = new oj.ArrayDataProvider(self.messages);
            self.closeMessageHandler = function (event) {
                self.messages.remove(event.detail.message);
            };
            self.tracker = ko.observable();
            self.viewProfileLbl = ko.observable();
            self.disOnLogin = ko.observable(false);
            self.image = ko.observable('');
            self.hasErrorOccured = ko.observable(false);
            self.isNotificationCount = ko.observable(false);
            self.isUserLoggedIn = ko.observable();
            self.confirmMessage = ko.observable();
            self.oprationMessage = ko.observable();
            self.yes = ko.observable();
            self.no = ko.observable();
            self.showDetails = ko.observable();
            self.signoLbl = ko.observable();
            self.changeLbl = ko.observable();
            self.passwordLabel = ko.observable();
            self.userName = ko.observable('');
            self.password = ko.observable('');
            //                self.HideLogin = ko.observable(true);
            self.loading = ko.observable(false);
            self.loginLabel = ko.observable();
            self.loginFailureText = ko.observable();
            self.navDataSourceLeft = ko.observable();
            self.navDataSource = ko.observable();
            self.N_navDataSource = ko.observable();
            self.navLeftArray = ko.observableArray();
            self.navTopArray = ko.observableArray();
            self.notificationsCount = ko.observable();
            self.lblSignOut = ko.observableArray();
            self.Usernametext = ko.observableArray();
            self.SignIn = ko.observable();
            self.forgetPassword = ko.observable();
            self.Searchlbl = ko.observable();
            self.userKeywordLable = ko.observable();
            self.recieveType = ko.observable();
            self.avatarSize = ko.observable("xxs");
            self.firstName = ko.observable();
            self.lastName = ko.observable();
            self.initials = ko.observable();
            var timer;
            self.selectedTableRowKeyNotifiation = ko.observable();
            self.reviewNotiType = ko.observable();
            self.ok = ko.observable();
            self.managerType = ko.observable();
            self.projectedStartDate = ko.observable();
            self.assignmentProjectedEndDate = ko.observable();
            self.peopleGroup = ko.observable();
            self.assignmentStatusTypeId = ko.observable();
            self.fax = ko.observable();
            self.faxExt = ko.observable();
            self.allRoles = ko.observableArray();
            self.organizationName = ko.observable();
            self.departmentNamenavDataSource = ko.observable();
            self.PaaSLookup = ko.observableArray([]);
            self.ApprovalCondition = ko.observableArray([]);
            self.EITIcons = ko.observableArray([]);
            self.PersonNumberArr = ko.observableArray([]);
            self.roleOptionType = ko.observableArray([]);
            self.rolesOption = ko.observableArray([]);
            self.validation = {};

            self.xx = ko.observable("N");
            self.allPaaSDefultValuearry = ko.observableArray([]);
            self.allEITArray = ko.observableArray([]);
            self.refreshViewForLanguage = ko.observable(false);
            self.personDetails = ko.observable('');
            self.empName = ko.observable();
            self.globalEitNameReportLength = ko.observable();
            self.jwt = ko.observable('');
            self.hostUrl = ko.observable('');
            self.backBtnVis = ko.observable(false);
            self.isScreenSMorMD = oj.ResponsiveKnockoutUtils.createMediaQueryObservable(oj.ResponsiveUtils.getFrameworkQuery(oj.ResponsiveUtils.FRAMEWORK_QUERY_KEY.MD_DOWN));
            // Media queries for repsonsive layouts
            self.smScreen = oj.ResponsiveKnockoutUtils.createMediaQueryObservable(smQuery);
            self.mdScreen = oj.ResponsiveKnockoutUtils.createMediaQueryObservable(mdQuery);
            self.screenRange = oj.ResponsiveKnockoutUtils.createScreenRangeObservable();
            self.windowSize = ko.observable();
            self.BusinessMissionRequestLbl = ko.observable();
            self.overtimeRequestLbl = ko.observable();
            self.languageSwitch_lng = ko.observable();
            self.globalEffictiveDate = ko.observable();
            self.globalspecialistEMP = ko.observable('');
            self.viewBulkLbl = ko.observable();//hussein
            self.NavIConsTopArr = ko.observableArray();
            self.specialist = ko.observableArray();
            self.SpecialistLbl = ko.observableArray();
            self.employeeTimeAttandancelbl = ko.observableArray();
            self.formVisible = ko.observable(true);
            self.reterieveIconData = ko.observableArray([]);
            self.loginDateFromSass = ko.observable();
            self.hideLeftMenu = ko.observable();
            self.showDashboard = ko.observable();
            self.JWTExpired = ko.observable();

            // Start project name
            self.hideLeftMenu = function () {
                return commonhelper.hideLeftMenu;

            }
            self.showDashboard = function () {
                return commonhelper.showDashboard;

            }

            //End projrct name

            var deviceIp;
            var today = new Date();
            var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
            var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
            var deviceAndBrowserDetails = navigator.userAgent;
            var LogindateTime;
            self.globalFuseModel = ko.observableArray([{
                "dataType": '',
                "tableName": '',
                "colName": '',
                "rowName": '',
                "udtValue": '',
                "lookupCode": '',
                "lookupValue": '',
                "lookupType": ''

            }]);
            self.globalHRCYesNo = ko.observableArray([{
                "value": '',
                "label": '',
                "labelAr": ''
            }]);
            self.globalJobCatagory = ko.observableArray([{
                "value": '',
                "label": '',
                "labelAr": ''
            }]);
            self.globalJobName = ko.observableArray([{
                "value": '',
                "label": '',
                "labelAr": ''
            }]);
            self.globalGeneralGroup = ko.observableArray([{
                "value": '',
                "label": '',
                "labelAr": ''
            }]);
            self.globalGroupType = ko.observableArray([{
                "value": '',
                "label": '',
                "labelAr": ''
            }]);
            self.globalJobCodes = ko.observableArray([{
                "value": '',
                "label": '',
                "labelAr": '',
                "description": ""
            }]);
            self.globalHrGrades = ko.observableArray([{
                "value": '',
                "label": '',
                "labelAr": ''
            }]);
            self.globalPositionsName = ko.observableArray([{
                "value": '',
                "label": '',
                "labelAr": ''
            }]);
            self.globalEmployeeEITLookup = ko.observableArray([]);
            self.globalPositionsAction = ko.observableArray([{
                "value": '',
                "label": '',
                "labelAr": ''
            }]);
            self.globalQualifications = ko.observableArray([{
                "value": '',
                "label": '',
                "labelAr": ''
            }]);
            self.globalPDFReport = ko.observableArray([{
                "value": '',
                "label": '',
                "labelAr": ''
            }]);
            self.globalEFFUDT = ko.observableArray([{
                "displaySequence": '',
                "rowLowRangeOrName": '',
                "rowName": '',
                "userColumnName": "",
                "userTableName": ""
            }]);

            self.globalJobsNames = ko.observableArray([{
                "value": '',
                "label": '',
                "labelAr": ''
            }]);
            self.globalEitNameReport = ko.observableArray([{
                "value": '',
                "label": '',
                "labelAr": ''
            }]);
            self.globalEITReportLookup = ko.observableArray([{
                "value": '',
                "label": '',
                "labelAr": '',
                "description": ''
            }]);
            self.globalSpecialistLookup = ko.observableArray([{
                "value": '',
                "label": '',
                "labelAr": '',
                "description": ''
            }]);
            self.globalManagerLookup = ko.observableArray([{
                "value": '',
                "label": '',
                "labelAr": '',
                "description": ''
            }]);
            self.globalEITDefultValueParamaterLookup = ko.observableArray([{
                "value": '',
                "label": '',
                "labelAr": ''
            }]);
            self.globalAbsenceType = ko.observableArray([{
                "value": '',
                "label": '',
                "labelAr": ''
            }]);


            self.tempAuthkey = ko.observable('Basic QXNocmFmLmFidS1zYWFkOldlbGNvbWVAMTIz');

            $('.body-page, #drawerToggleButtonor, #backButton, .header_logo_min').on('click', function () {
                $('.MYdropdown_menu').hide();
            });

            self._showComponentValidationErrors = function (trackerObj) {
                if (trackerObj !== undefined) {
                    trackerObj.showMessages();
                    if (trackerObj.focusOnFirstInvalid())
                        return false;
                }
                return true;
            };

            self.displaySreenRange = ko.computed(function () {
                var range = self.screenRange();
                if (range === oj.ResponsiveUtils.SCREEN_RANGE.SM) {
                    self.windowSize('SM');
                } else if (range === oj.ResponsiveUtils.SCREEN_RANGE.MD) {
                    self.windowSize('MD');
                } else if (range === oj.ResponsiveUtils.SCREEN_RANGE.LG) {
                    self.windowSize('LG');
                } else if (range === oj.ResponsiveUtils.SCREEN_RANGE.XL) {
                    self.windowSize('XL');
                }
            });

            self.getAllAbsenceTypes = function () {
                var getReportCBCF = function (data) {
                    self.globalAbsenceType([]);
                    for (var index = 0; index < data.length; index++) {
                        self.globalAbsenceType.push({
                            "value": data[index].value, "label": data[index].label, "labelAr": data[index].label
                        });
                    }

                };
                var reportPaylod = { "reportName": "AbsenceTypeReport" };
                services.getGenericReport(reportPaylod).then(getReportCBCF, self.failCbFn);
            };

            self.viewProfile = function () {
                self.router.go('profile');
            };
            self.viewHome = function () {
                self.router.go('home');
            };

            self.getBusinessUnits = function () {

                var searhCbFn = function (data) {
                    self.arr = ko.observableArray([]);
                    self.arr(data);
                    self.globalEitNameReport([]);
                    for (var i = 0; i < self.arr().length; i++) {
                        self.globalEitNameReport.push({ "value": self.arr()[i].DESCRIPTIVE_FLEX_CONTEXT_CODE, "label": self.arr()[i].DESCRIPTIVE_FLEX_CONTEXT_NAME });
                    }

                };
                var searhfailCbFn = function () { };
                if (self.getLocale() == 'ar') {
                    self.lang = "AR";
                } else {
                    self.lang = "US";
                }
                // services.getEitNameReportReport(self.lang).then(searhCbFn, searhfailCbFn);
            };


            self.EmployeeAuthentication = ko.observable("");
            self.adminAuthentication = ko.observable("");
            self.lineManager = ko.observable("");
            self.checkUserLoginSpecialist = ko.observable("");
            self.checkUserLoginEmployee = ko.observable("");

            self.getLocale = function () {
                return oj.Config.getLocale();

            };
            self.userAuthentication = function () {
                var searhCbFn = function (data) {
                    if (data.length > 1) {

                        for (var i = 0; i < data.length; i++) {

                            if (data[i].ROLE_NAME === "HHA PAAS Employee Custom") {

                                self.EmployeeAuthentication(data[i].ROLE_NAME);


                            } else if (data[i].ROLE_NAME === "Application Implementation Administrator") {

                                self.adminAuthentication(data[i].ROLE_NAME);

                            } else if (data[i].ROLE_NAME === "HHA-Line Manager Custom V3") {

                                self.lineManager(data[i].ROLE_NAME);
                            }
                        }
                        ;
                    } else {
                        var roleName = data.ROLE_NAME;
                        if (roleName === "HHA PAAS Employee Custom") {
                            self.EmployeeAuthentication(roleName);

                        } else if (roleName === "Application Implementation Administrator") {
                            self.adminAuthentication(roleName);

                        } else if (roleName === "HHA-Line Manager Custom V3") {
                            self.lineManager(roleName);
                        }


                    }
                };

                var searhfailCbFn = function () {


                };

                // services.getGenericAuthentication(self.userName()).then(searhCbFn, searhfailCbFn); 
            };


            self.beforeSelectItem = function (event, ui) {
                self.closeHeaderNav();
                self.loading(true);

            };

            // Router setup
            self.router = oj.Router.rootInstance;
            self.router.dispose();
            self.arr2 = ko.observableArray();


            self.router.configure({
                'home': { label: 'HHA-SSHR', value: "home/home", title: "Home", isDefault: !self.showDashboard() },
                'setup': { label: 'Setup', value: "setup/home/home", title: "Setup" },
                'profile': { label: 'Profile', title: "Profile" },
                'dashboard': { label: 'dashboard', value: "dashboard", title: "Dashboard", isDefault: self.showDashboard() },
                'notificationScreen': { label: 'Notification', value: "notification/notificationScreen", title: "Notification" },
                'searchJob': { label: 'Job', value: "job/searchJob", title: "Job" },
                'job': { label: 'Job', value: "job/job", title: "Job" },
                'detailsJob': { label: 'Job', value: "job/detailsJob", title: "Job Details" },
                'validGradeSummary': { label: 'Valid Grade ', value: "job/validGrade/summaryValidGrade", title: "Valid Grade" },
                'validGradeOprationScreen': { label: 'Valid Grade ', value: "job/validGrade/validGradeOprationScreen", title: "Valid Grade Opration" },
                'additionalDetailsSummary': { label: 'Additional Details ', value: "job/additionalDetails/summaryAdditionalDetails", title: "Additional Details" },
                'clothesAllowanceOprationScreen': { label: 'Additional Details ', value: "job/additionalDetails/clothesAllowance/clothesAllowanceOprationScreen", title: "Clothes Allowance Opration" },
                'securityAllowanceOprationScreen': { label: 'Additional Details ', value: "job/additionalDetails/securityAllowance/securityAllowanceOprationScreen", title: "Security Allowance Opration" },
                'supervisorAllowanceOprationScreen': { label: 'Additional Details ', value: "job/additionalDetails/supervisorAllowance/supervisorAllowanceOprationScreen", title: "Supervisor Allowance Opration" },
                'requiredQualificationsOprationScreen': { label: 'Additional Details ', value: "job/additionalDetails/requiredQualifications/requiredQualificationsOprationScreen", title: "Required Qualifications Opration" },
                'minimumRequiredEducationLeveOprationScreen': { label: 'Additional Details ', value: "job/additionalDetails/minimumRequiredEducationLeve/minimumRequiredEducationLeveOprationScreen", title: "Minimum Required Education Opration" },
                'about': { label: 'About', title: "About" },
                'xx': { label: 'xx Details ', value: "actualPosition/actualPositionOperationScreen", title: "Details" },
                'actualposition': { label: 'Actual Postion', value: "actualPosition/actualposition", title: "Actual Postion" },
                'position': { label: 'Position', value: "position/position", title: "Position" },
                'positionSummaryAdditionalDetails': { label: 'Position Additional Details ', value: "position/additionalDetails/positionSummaryAdditionalDetails", title: "Position Summary Additional Details" },
                'searchPosition': { label: 'Position', value: "position/searchPosition", title: "Search position" },
                'workNatureAllowanceOprationScreen': { label: 'Work Nature Allowance', value: "position/additionalDetails/workNatureAllowance/workNatureAllowanceOprationScreen", title: "Work Nature Allowance" },
                'dynamic': { label: ' Work Nature Allowance  ', value: "dynamic/dynamicOperationScreen", title: "Operation" },
                'dynamicSummary': { label: ' Work Nature Allowance  ', value: "dynamic/dynamicSummary", title: "Summary" },
                'dynamicReview': { label: ' Work Nature Allowance  ', value: "dynamic/dynamicReview", title: "Review" },
                'reportSummary': { label: 'summary', value: "report/summary", title: "Report Summary" },
                'validationScreen': { label: ' Validation Screen ', value: "validation/validationScreen", title: "Validation Screen" },
                'validation': { label: ' validation ', value: "validation/validation", title: "Validation" },
                'manageOnlineHelp': { label: ' Manage Online Help ', value: "manageOnlineHelp/manageOnlineHelp", title: "ManageOnlineHelp" },
                'approvalSummary': { label: ' Approval Summary   ', value: "approval/approvalSummary", title: "Approval Summary" },
                'approvalOperationScreen': { label: ' Approval Operation Screen    ', value: "approval/approvalOperationScreen", title: "Approval Operation" },
                'getSummary': { label: 'Get Summary ', value: "report/getSummary", title: "Get Summary" },
                'form': { label: 'form', value: "form/form", title: "Form" },
                'getPassLookup': { label: 'Get Lookup', value: "passLookup/getPassLookup", title: "Get PaaS Lookup" },
                'lookup': { label: 'lookup', value: "passLookup/lookup", title: "Lookup" },
                'viewAbsence': { label: 'View Absences', value: "absence/viewAbsence", title: "View Absences" },
                'timeCard': { label: 'Time Details', value: "timeCard/timeEventDetails", title: "Time Details" },
                'elementEntry': { label: 'Element Entry', value: "elementEntry/elementEntrySummary", title: "Element Entry" },
                'elementEntryAdd': { label: 'Element Entry', value: "elementEntry/elementEntryAdd", title: "Add Element Entry" },
                'bulkTransactionHome': { label: 'bulkTransactionHome', value: "bulkTransaction/bulkTransactionHome", title: "Bulk Transaction" },
                'dynamicSummaryBulkTransaction': { label: 'bulkTransactionHome', value: "bulkTransaction/dynamicSummaryBulkTransaction", title: "Bulk Transaction Summary" },
                'dynamicOperationScreenBulkTransaction': { label: 'bulkTransactionHome', value: "bulkTransaction/dynamicOperationScreenBulkTransaction", title: "Bulk Transaction Operation" },
                'approvalManagmentSummary': { label: 'approvalManagmentHome', value: "approvalManagment/approvalManagementSummary", title: "Approval Managment Summary" },
                'approvalManagmentScreen': { label: 'approvalManagmentHome', value: "approvalManagment/approvalManagmentScreen", title: "Approval Managment Operation" },
                'approvalManagmentUpdateScreen': { label: 'approvalManagmentEdit', value: "approvalManagment/approvalManagementEditScreen", title: "Approval Managment Operation" },
                'personSearch': { label: 'person Search', value: "specialistServices/personSearch", title: "Person Search" },
                'searchPayroll': { label: 'Payroll', value: "payrollValidator/searchPayroll", title: "Payroll" },
                'roleSetupSummary': { label: 'roleSetupSummary', value: "roleSetUp/roleSetupSummary", title: "role Setup Summary" },
                'roleSetupOperation': { label: 'roleSetupOperation', value: "roleSetUp/roleSetupOperation", title: "role Setup Operation" },
                'roleSetupValidationSummary': { label: 'roleSetupValidationSummary', value: "roleSetupValidation/roleSetupValidationSummary", title: "role Setup Validation Summary" },
                'roleSetupValidationOperation': { label: 'roleSetupValidationOperation', value: "roleSetupValidation/roleSetupValidationOperation", title: "role Setup Validation Operation" },
                'ProbationPerformanceEvaluationSumary': { label: 'Probation Performance Evaluation Sumary', value: "ProbationPerformanceEvaluation/ProbationPerformanceEvaluationSumary", title: "Probation Performance Evaluation Sumary" },
                'ProbationPerformanceEvaluationOperation': { label: 'ProbationPer formance Evaluation Operation', value: "ProbationPerformanceEvaluation/ProbationPerformanceEvaluationOperation", title: "Probation Performance Evaluation Operation" },
                'ProbationPerformanceEvaluationReview': { label: 'Probation Performance Evaluation Review', value: "ProbationPerformanceEvaluation/ProbationPerformanceEvaluationReview", title: "Probation Performance Evaluation Operation" },
                'employeeTimeAttandanceSummary': { label: 'Employee Time Attandance Summary', value: "employeeTimeAttandance/employeeTimeAttandanceSummary", title: "Employee Time Attandance Summary" },
                'ApprovalConditionSummary': { label: 'Approval Condition Summary', value: "ApprovalCondition/ApprovalConditionSummary", title: "Approval Condition Summary" },
                'approvalConditionOperation': { label: 'Approval Condition Operation', value: "ApprovalCondition/approvalConditionOperation", title: "Approval Condition Operation" },
                'menu': { label: 'Bulk Menu', value: "bulkMenu/menu", title: "Bulk Menu" },
                'bulkOperations': { label: 'Bulk Operations', value: "bulkMenu/bulkOperations", title: "Bulk Operations download/upload" },
                'bulkSetup': { label: 'Bulk Setup', value: "bulkMenu/setup", title: "Bulk Configration Setup" },
                'mailSummary': { label: 'Email', value: "MailTemplate/mailSummary", title: "Email Summary" },
                'mailOperations': { label: 'Email Operations', value: "MailTemplate/mailOperations", title: "Email Operations" },
                'baseScreen': { label: 'Re-Assign', value: "reAssignSelfService/baseScreen", title: "Re-Assign" },
                'trackRequestScreen': { label: 'Re-Assign', value: "reAssignSelfService/trackRequestScreen", title: "Re-Assign" },
                'reassignRequestScreen': { label: 'Re-Assign', value: "reAssignSelfService/reassignRequestScreen", title: "Re-Assign" },
                'reassignRequestOperation': { label: 'Re-Assign', value: "reAssignSelfService/reassignRequestOperation", title: "Re-Assign" },
                'manageOnlineHelpSummary': { label: 'manageOnlineHelp', value: "manageOnlineHelp/manageOnlineHelpSummary", title: "Manage Online Help" },
                'delegationOperation': { label: 'delegation', value: "reAssignSelfService/DelegationOperation", title: "Delegation Operation" },
                'approvePage': { label: 'approvePage', value: "ApprovePage", title: "Approve Page" },
                'delegationSummary': { label: 'Delegation Employee', value: "reAssignSelfService/DelegationSummary", title: "Delegation Summary" },
                'iconSetup': { label: 'Icons Setup', value: "IconSetup/iconSetup", title: "IconSetup" },
                'iconSetupSummary': { label: 'Icons Setup', value: "IconSetup/iconSetupSummary", title: "IconSetup" },
                'locationEmployee': { label: 'location Employee', value: "LocationEmployee/locationEmployee", title: "LocationEmployee" },
                'employeeTimeAttandanceSearch': { label: 'Employee Time Attandance', value: "employeeTimeAttandance/employeeTimeAttandanceSearch", title: "Employee Time Attandance" },
                'locationSummary': { label: 'location Summary', value: "LocationEmployee/locationSummary", title: "location Summary" },
                'logScreen': { label: 'log Screen Summary', value: "logScreen/logScreenSummary", title: "log Screen Summary" },
                'timeCard': { label: 'Time Card', value: "timeCard/timeCardSummary", title: "Time Card Summary" },
                'attachElement': { label: 'Time Card', value: "AttachElement/attachElement", title: "Attach Element" },
                //   'locationSummary': {label: 'location Summary', value: "LocationEmployee/locationSummary", title: "location Summary"},
                'approvalManagementOperation': { label: 'approvalManagmentHome', value: "approvalManagment/approvalManagementOperation", title: "Approval Managment Operation" }
            });
            oj.Router.defaults['urlAdapter'] = new oj.Router.urlParamAdapter();

            self.moduleConfig = ko.observable({ 'view': [], 'viewModel': null });
            ko.computed(function () {
                self.loading() ? pageBody.css('cursor', 'wait') : pageBody.css('cursor', '');
                if (self.router._stateId() == 'home')
                    self.loading(false);
                if (self.router._stateId() != 'home'
                    && self.router._stateId() != 'setup'
                    && self.router._stateId() != 'bulkTransactionHome'
                    && self.router._stateId() != 'notificationScreen') {
                    $('#drawerToggleButtonor').hide();
                    $('#backButton').show();
                } else {
                    $('#backButton').hide();
                    $('#drawerToggleButtonor').show();
                }
                var name = self.router.moduleConfig.name();
                var viewPath = 'views/' + name + '.html';
                var modelPath = 'viewModels/' + name;
                var masterPromise = Promise.all([
                    moduleUtils.createView({ 'viewPath': viewPath }),
                    moduleUtils.createViewModel({ 'viewModelPath': modelPath })
                ]);
                masterPromise.then(
                    function (values) {
                        self.moduleConfig({ 'view': values[0], 'viewModel': values[1] });
                    },
                    function (reason) { }
                );

                document.addEventListener("deviceready", onDeviceReady, false);
                function onDeviceReady() {
                    document.addEventListener("backbutton", function (e) {
                        e.preventDefault();
                        if (self.router._stateId() == 'home') {
                            navigator.app.exitApp();
                        } else if (self.router._stateId() == 'setup'
                            && self.router._stateId() == 'notificationScreen'
                            || typeof self.router._stateId() == 'undefined') {
                            self.router.go(self.showDashboard() ? 'dashboard' : 'home');
                        } else {
                            history.go(-1);
                            return false;
                        }
                    }, false);
                }
            });

            self.subModule = ko.computed(function () {
                if (self.router.currentState()) {
                    //constructNavLeftData();
                    self.constructNavTopData();
                    self.navDataSource(new oj.ArrayTableDataSource(self.NavIConsTopArr(), { idAttribute: 'id' }));
                    self.N_navDataSource(new ArrayDataProvider(self.NavIConsTopArr(), { keyAttributes: 'id' }));


                }
            });
            var navDataLeft = [];

            navDataLeft.push({
                name: getTranslation("pages.home"), id: 'home',
                iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-home-icon-24 icon-size'
            },
                {
                    name: getTranslation("pages.job"), id: 'searchJob',
                    iconClass: 'oj-navigationlist-item-icon fa fa-briefcase icon-size'
                },
                {
                    name: getTranslation("pages.actualPosition"), id: 'xx',
                    iconClass: 'oj-navigationlist-item-icon fa fa-user icon-size'
                },
                {
                    name: getTranslation("pages.position"), id: 'searchPosition',
                    iconClass: 'oj-navigationlist-item-icon fa fa-user icon-size'
                });


            self.navLeftArray(navDataLeft);
            self.navDataSourceLeft(new oj.ArrayTableDataSource(self.navLeftArray(), { idAttribute: 'id' }));


            self.selectedMenuItem = ko.observable("");

            self.menuItemAction = function (event) {
                self.selectedMenuItem(event.target.value);
            };

            function constructNavLeftData() {
                //                 
                if (self.EmployeeAuthentication() === "HHA PAAS Employee Custom" && self.lineManager() === "HHA-Line Manager Custom V3" && self.adminAuthentication() === "Application Implementation Administrator") {

                    var navDataLeft = [
                        {
                            name: getTranslation("pages.Employee"), id: 'home',
                            iconClass: 'oj-navigationlist-item-icon fa fa-male'
                        },
                        {
                            name: getTranslation("pages.LineManager"), id: 'personSearch',
                            iconClass: 'oj-navigationlist-item-icon fa fa-users'
                        },
                        {
                            name: getTranslation("pages.Administrator"), id: 'personSearch',
                            iconClass: 'oj-navigationlist-item-icon fa fa-users'
                        }
                    ];
                    self.navLeftArray(navDataLeft);
                    self.navDataSourceLeft(new oj.ArrayTableDataSource(self.navLeftArray(), { idAttribute: 'id' }));

                    self.checkUserLoginSpecialist("Specialist");
                }
                if (self.EmployeeAuthentication() === "de" && self.lineManager() === "HHA-Line Manager Custom V3" && self.adminAuthentication() === "") {

                    var navDataLeft = [
                        {
                            name: getTranslation("pages.Employee"), id: 'home',
                            iconClass: 'oj-navigationlist-item-icon fa fa-male'
                        },
                        {
                            name: getTranslation("pages.LineManager"), id: 'personSearch',
                            iconClass: 'oj-navigationlist-item-icon fa fa-users'
                        }
                    ];
                    self.navLeftArray(navDataLeft);
                    self.navDataSourceLeft(new oj.ArrayTableDataSource(self.navLeftArray(), { idAttribute: 'id' }));
                    self.checkUserLoginSpecialist("Specialist");
                }
                if (self.EmployeeAuthentication() === "HHA PAAS Employee Custom" && self.lineManager() === "" && self.adminAuthentication() === "") {

                    var navDataLeft = [
                        {
                            name: getTranslation("pages.Employee"), id: 'home',
                            iconClass: 'oj-navigationlist-item-icon fa fa-male'
                        }
                    ];
                    self.navLeftArray(navDataLeft);
                    self.navDataSourceLeft(new oj.ArrayTableDataSource(self.navLeftArray(), { idAttribute: 'id' }));
                    self.checkUserLoginEmployee("HHA PAAS Employee Custom");
                }

            }

            function navTopFillData() {
                var navData = ko.observableArray([]);
                //---------
                //hussein

                //---------

                var userRoles;
                if (JSON.parse(sessionStorage.getItem("userRole"))) {
                    userRoles = JSON.parse(sessionStorage.getItem("userRole"));

                }
                if (!userRoles) {
                    userRoles = '';
                }
                if (!userRoles.length) {
                    userRoles = [userRoles];
                }
                var indexOfRole;
                navData.splice(0, 0, {
                    name: getTranslation("pages.home"), id: 'home',
                    iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-home-icon-24'
                });

                for (var i = 0; i < userRoles.length; i++) {

                    if (userRoles[i].ROLE_NAME == "HHA PAAS Employee Custom") {
                        //
                        navData.splice(1, 0, {
                            name: getTranslation("pages.Notification"), id: 'notificationScreen',
                            iconClass: 'oj-navigationlist-item-icon fa fa-bell'
                        });
                        self.isNotificationCount(true);
                    } else if (userRoles[i].ROLE_NAME == "Application Implementation Administrator") {
                        if (!detectmob()) {
                            navData.push({
                                name: getTranslation("pages.setup"), id: 'setup', style: 'font-size: -webkit-xxx-large',
                                iconClass: 'oj-navigationlist-item-icon fa fa-gear'
                            });
                            navData.push({
                                name: getTranslation("pages.reassignSetup"), id: 'baseScreen', style: 'font-size: -webkit-xxx-large',
                                iconClass: 'oj-navigationlist-item-icon fa fa-certificate'
                            });

                            navData.push({
                                name: getTranslation("pages.bulkmenu"), id: 'menu', style: 'font-size: -webkit-xxx-large',
                                iconClass: 'oj-navigationlist-item-icon fa fa-file'
                            });
                        }
                    } else if (userRoles[i].ROLE_NAME == "HHA-Line Manager Custom V3") {
                        if (navData().find(e => e.id == 'personSearch')) {
                            //                              console.log("exist");
                        } else {
                            navData.push({
                                name: getTranslation("pages.Specialist"), id: 'personSearch', iconClass: 'oj-navigationlist-item-icon fa fa-male'
                            });
                        }

                        self.checkUserLoginSpecialist("Specialist");
                    } else if (userRoles[i].ROLE_NAME == "HHA Manager Custom") {

                        self.checkUserLoginSpecialist("Specialist");
                        //                                navData.push({name: getTranslation("pages.reassignSetup"), id: 'baseScreen', style: 'font-size: -webkit-xxx-large',
                        //                                    iconClass: 'oj-navigationlist-item-icon fa fa-certificate'});
                    } else if (userRoles[i].ROLE_NAME == "HHA-Line Manager Custom V3") {


                    } else if (userRoles[i].ROLE_NAME == "HHA PAAS Specialist Custom") {
                        if (navData().find(e => e.id == 'personSearch')) {
                            //                              console.log("exist");
                        } else {
                            navData.push({
                                name: getTranslation("pages.Specialist"), id: 'personSearch', iconClass: 'oj-navigationlist-item-icon fa fa-male'
                            });
                        }
                        self.checkUserLoginSpecialist("Specialist");
                    } else if (userRoles[i].ROLE_NAME == 'HHA PAAS Government Relations Specialist Custom') {
                        //                           navData.push({name: getTranslation("pages.bulk"), id: 'bulk', style: 'font-size: -webkit-xxx-large',
                        //                                    iconClass: 'oj-navigationlist-item-icon fa fa-file'});
                    }
                }

                //}

                //                    switch (true) {
                ////                        case (self.EmployeeAuthentication() === "Employee" && self.adminAuthentication() === "Application Implementation Administrator"):
                ////                            navData = [
                ////                                {name: getTranslation("pages.home"), id: 'dashboard',
                ////                                    iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-home-icon-24'},
                ////                                {name: getTranslation("pages.Notification"), id: 'notificationScreen',
                ////                                    iconClass: 'oj-navigationlist-item-icon fa fa-bell'},
                ////                                {name: getTranslation("pages.setup"), id: 'setup', style: 'font-size: -webkit-xxx-large',
                ////                                    iconClass: 'oj-navigationlist-item-icon fa fa-gear'},
                ////                                {name: getTranslation("pages.Notification"), id: 'searchPayroll',
                ////                                    iconClass: 'oj-navigationlist-item-icon fa fa-bell'},
                ////                                {name: getTranslation("pages.overtimeRequest"), id: 'bulkTransactionHome',
                ////                                    iconClass: 'oj-navigationlist-item-icon fa fa-certificate'},
                ////                                {name: getTranslation("pages.Specialist"), id: 'personSearch', iconClass: 'oj-navigationlist-item-icon fa fa-gear'
                ////                                }
                ////                            ];
                ////                            self.isNotificationCount(true);
                ////                            self.checkUserLoginSpecialist("Specialist");
                ////                            break;
                //                        case (self.EmployeeAuthentication() === "Employee" && self.lineManager() === "Line Manager" && self.adminAuthentication() === ""):
                //                            navData = [
                //                                {name: getTranslation("pages.home"), id: 'dashboard',
                //                                    iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-home-icon-24'},
                //
                //                                {name: getTranslation("pages.Notification"), id: 'notificationScreen',
                //                                    iconClass: 'oj-navigationlist-item-icon fa fa-bell'},
                //                                {name: getTranslation("pages.overtimeRequest"), id: 'bulkTransactionHome',
                //                                    iconClass: 'oj-navigationlist-item-icon fa fa-certificate'},
                //                                {name: getTranslation("pages.Notification"), id: 'searchPayroll',
                //                                    iconClass: 'oj-navigationlist-item-icon fa fa-money'},
                //                                {name: getTranslation("pages.setup"), id: 'setup', style: 'font-size: -webkit-xxx-large',
                //                                    iconClass: 'oj-navigationlist-item-icon fa fa-gear'},
                //                                {name: getTranslation("pages.Specialist"), id: 'personSearch', iconClass: 'oj-navigationlist-item-icon fa fa-male'
                //                                }
                //                            ];
                //                            self.isNotificationCount(true);
                //                            self.checkUserLoginSpecialist("Specialist");
                //                            break;
                //                        case (self.EmployeeAuthentication() === "Employee" && self.lineManager() === "" && self.adminAuthentication() === ""):
                //                            navData = [
                //                                {name: getTranslation("pages.home"), id: 'dashboard',
                //                                    iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-home-icon-24'},
                //                                {name: getTranslation("pages.Notification"), id: 'notificationScreen',
                //                                    iconClass: 'oj-navigationlist-item-icon fa fa-bell'},
                //                                {name: getTranslation("pages.Notification"), id: 'searchPayroll',
                //                                    iconClass: 'oj-navigationlist-item-icon fa fa-money'}
                //                            ];
                //                            self.isNotificationCount(true);
                //                            self.checkUserLoginEmployee("Employee");
                //                            break;
                //                        case (self.EmployeeAuthentication() === "" && self.adminAuthentication() === "Application Implementation Administrator"):
                //                            navData = [
                //                                {name: getTranslation("pages.home"), id: 'dashboard',
                //                                    iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-home-icon-24'},
                //                                {name: getTranslation("pages.setup"), id: 'setup', style: 'font-size: -webkit-xxx-large',
                //                                    iconClass: 'oj-navigationlist-item-icon fa fa-gear'},
                //                                {name: getTranslation("pages.Notification"), id: 'searchPayroll',
                //                                    iconClass: 'oj-navigationlist-item-icon fa fa-money'}
                //                            ];
                //                            self.isNotificationCount(false);
                //                            self.checkUserLoginEmployee("Employee");
                //                            break;
                //                        default:
                //                            navData = [
                //                                {name: getTranslation("pages.home"), id: 'dashboard',
                //                                    iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-home-icon-24'}
                //                            ];
                //                            self.checkUserLoginEmployee("Employee");
                //                            self.isNotificationCount(false);
                //                    }

                return navData();
            }
            ;

            // Navigation setup
            var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
            self.constructNavTopData = function () {
                //                    var navData = ko.observableArray( navTopFillData());
                var navData = navTopFillData();
                self.NavIConsTopArr(navTopFillData());
                self.navTopArray(navData);
                self.navDataSource(new oj.ArrayTableDataSource(self.navTopArray(), { idAttribute: 'id' }));
                // /*new navigation list*/ self.NavIConsTopArr();                                
                self.N_navDataSource(new ArrayDataProvider(self.NavIConsTopArr(), { keyAttributes: 'id' }));

                /*new navigation list*/

            }

            // Drawer
            // Close offcanvas on medium and larger screens
            self.drawerParams = {
                displayMode: 'push',
                selector: '#navDrawer',
                content: '#pageContent'
            };
            self.mdScreen.subscribe(function () {
                oj.OffcanvasUtils.close(self.drawerParams);
            });

            // Called by navigation drawer toggle button and after selection of nav drawer item
            self.toggleDrawer = function () {
                return oj.OffcanvasUtils.toggle(self.drawerParams);
            };
            // Add a close listener so we can move focus back to the toggle button when the drawer closes
            $("#navDrawer").on("ojclose", function () {
                $('#drawerToggleButton').focus();
            });
            // Header
            // Application Name used in Branding Area
            self.appName = ko.observable("SSHR");
            // User Info used in Global Navigation area
            self.userLogin = ko.observable("");
            // Footer
            function footerLink(name, id, linkTarget) {
                this.name = name;
                this.linkId = id;
                this.linkTarget = linkTarget;
            }
            self.footerLinks = ko.observableArray([
                new footerLink('About Appspro', 'about Appspro', 'http://www.appspro-me.com/about-us/'),
                new footerLink('Contact Us', 'contactUs', 'http://www.appspro-me.com/contact/')

            ]);
            self.failCbFn = function (xhr, thrownError) {
                //                   self.hidePreloader();
                self.loading(false);
                self.isGettingMaster(false);
            };

            //Enter key will trigger login button
            //BUGGY CODE
            //                var inputPassword = document.getElementById("password");
            //                inputPassword.addEventListener("keyup", function (e) {
            //                    e.preventDefault();
            //                    if (event.keyCode === 13) {
            //                        self.onLogin();
            //                    }
            //                });
            //                var inputUserName = document.getElementById("userName");
            //                inputUserName.addEventListener("keyup", function (e) {
            //                    e.preventDefault();
            //                    if (event.keyCode === 13) {
            //                        self.onLogin();
            //                    }
            //                });


            self.computeIconsArray = ko.computed(function () {
                if (self.personDetails().picBase64 === undefined || self.personDetails().picBase64 === null || self.personDetails().picBase64 === "") {
                    self.image('');
                } else {
                    self.image("data:image/png;base64," + self.personDetails().picBase64);

                }
                if (self.personDetails().picBase64) {
                    if ($(".profilepic")[0]) {
                        $(".profilepic")[0].src = "data:image/png;base64," + self.personDetails().picBase64;
                    }
                    if ($(".profilepic")[1]) {
                        $(".profilepic")[1].src = "data:image/png;base64," + self.personDetails().picBase64;
                    }
                } else {
                    if ($(".profilepic")[0]) {
                        $(".profilepic")[0].src = "css/images/avatar_24px.png";
                    }
                    if ($(".profilepic")[1]) {
                        $(".profilepic")[1].src = "css/images/avatar_24px.png";
                    }
                }
            });


            self.onLogin = function () {
                if (!self.userName() || !self.password())
                    return;
                //self.loading(true);
                self.disOnLogin(true);
                $(".apLoginBtn").addClass("loading");
                pageBody.css('cursor', 'wait');
                // Validating the components
                self.loginFailureText("");
                var trackerObj = ko.utils.unwrapObservable(self.tracker);
                //                    
                if (!this._showComponentValidationErrors(trackerObj)) {
                    return;
                }
                var loginSuccessCbFn = function () {
                    LogindateTime = date + ' ' + time;
                    self.loginDateFromSass(LogindateTime);
                    var payloadLoginHistory = {};
                    $.getJSON("https://api.ipify.org/?format=json", function (e) {
                        deviceIp = e.ip;
                        payloadLoginHistory = {
                            "personNumber": self.userName(),
                            "loginDate": LogindateTime,
                            "browserAndDeviceDetails": deviceAndBrowserDetails,
                            "deviceIp": deviceIp
                        };

                        sessionStorage.setItem("SShrLOginHistroy", JSON.stringify(payloadLoginHistory));


                        var SShrLoginHistory = function (data) {

                            // sessionStorage.setItem("SShrLOginHistroy","");
                        };


                        services.addGeneric("SShrLoginHistory/addSShrLoginHistory", sessionStorage.getItem("SShrLOginHistroy")).then(SShrLoginHistory, self.failCbFn());
                    });



                    //end user login histiry
                    $(".apLoginBtn").removeClass("loading");
                    pageBody.css('cursor', '');
                    //self.loading(false);

                    self.disOnLogin(false);

                    //self.hidePreloader();

                    self.router.go(self.showDashboard() ? 'dashboard' : 'home');
                    self.refresh();
                    self.isUserLoggedIn(true);
                    self.constructNavTopData();
                    //addFuseModelToIndexedDB(services, self, self, commonhelper);
                    //getPaaSLookup(services, self, self, commonhelper);
                    //getEITIcons(services, self, self, commonhelper);
                    //getPersonNumber(services, self, self, commonhelper);
                    //                        if (self.personDetails().picBase64) {
                    //                            if ($(".profilepic")[0]) {
                    //                                $(".profilepic")[0].src = "data:image/png;base64," + self.personDetails().picBase64;
                    //                            }
                    //                            if ($(".profilepic")[1]) {
                    //                                $(".profilepic")[1].src = "data:image/png;base64," + self.personDetails().picBase64;
                    //                            }
                    //                        } else {
                    //                            if ($(".profilepic")[0]) {
                    //                                $(".profilepic")[0].src = "css/images/avatar_24px.png";
                    //                            }
                    //                            if ($(".profilepic")[1]) {
                    //                                $(".profilepic")[1].src = "css/images/avatar_24px.png";
                    //                            }
                    //                        }

                    self.loginFailureText("");
                    if (self.getLocale() === "ar") {
                        $('.panelstyle').addClass('arabicpanel');
                    } else {
                        $('.panelstyle').removeClass('arabicpanel');
                        $('.noti').removeClass('noti-count-ar');
                        $('.noti').addClass('noti-count');
                    }
                    //self.getBusinessUnits();
                    //self.getAllAbsenceTypes();
                    //self.userAuthentication();

                    //pushNav();
                    //self.router.store(self.globalEitNameReport());
                    //-----------------time Out-------------------//  

                    if (self.personDetails().picBase64 === undefined || self.personDetails().picBase64 === null || self.personDetails().picBase64 === "") {
                        self.image('');
                    } else {
                        self.image("data:image/png;base64," + self.personDetails().picBase64);
                    }
                    //-----------------time Out-------------------//
                };



                var loginFailCbFn = function () {
                    //self.loading(false);
                    $(".apLoginBtn").removeClass("loading");
                    self.disOnLogin(false);
                    //self.hidePreloader();
                    self.isUserLoggedIn(false);
                    self.loginFailureText("error");
                    self.loginFailureText(getTranslation("login.loginFailureText"));
                };
                //                    self.showPreloader();
                function authinticate(data) {
                    var result = data;
                    var check = result.result;
                    if (check) {
                        loginSuccessCbFn();
                    } else {
                        loginFailCbFn();
                    }
                    $(".apLoginBtn").removeClass("loading");
                    self.disOnLogin(false);

                }
                services.authenticate(self.userName(), self.password()).then(authinticate, self.failCbFn);
                //$(".apLoginBtn").removeClass("loading");
            };
            //-----------------time Out-------------------//  
            self.resetTimer = function () {
                var isLocal = window.location.hostname == 'localhost' || window.location.hostname == '127.0.0.1';
                if (!detectmob() && !isLocal) {
                    clearTimeout(timer);
                    timer = setTimeout(_ => {
                        self.signOut();
                    }, 10 * 60 * 1000);
                }
            };
            self.refreshData = ko.computed(function () {
                if (self.refreshViewForLanguage()) {
                    //    self.constructNavTopData();
                    self.navDataSource(new oj.ArrayTableDataSource(self.NavIConsTopArr(), { idAttribute: 'id' }));
                    self.N_navDataSource(new ArrayDataProvider(self.NavIConsTopArr(), { keyAttributes: 'id' }));
                    //constructNavLeftData();
                    // initFooterLinks();
                }
            });
            //------------------------------draw self services----------------------------------//
            self.drawIconsInMenu = function () {
                var eitNames = self.globalEitNameReport();


              var arr =[];
                for(var i =0 ; i<self.globalEitNameReport().length;i++){
                    console.log(self.globalEitNameReport()[i]);
                   arr.push({
                        label: 'c',
                        value: 'demo-icon-circle-a',
                        router: 'dynamicSummary',
                        iconType: "",
                        text: self.globalEitNameReport()[i].value,
                        visible: true,
                        IconVarBG: '_RBGCG_0',
                        code: self.globalEitNameReport()[i].value
                    });
               }
               console.log(arr);
               self.iconsArr(arr);
               
            };
            //------------------------------End draw self services-------------------------------------//  
            ko.computed(() => {
                if (1==1) {
                    self.drawIconsInMenu();
                }
                
                // self.drawIconsInMenu();
            });

            self.onReset = function () {
                self.isUserLoggedIn(false);
                self.userName("");
                self.password("");
                $("#userName").ojInputText("reset");
                $("#password").ojInputPassword("reset");
            };
            self.rightDrawerParams = {
                displayMode: 'push',
                selector: '#navRightDrawer',
                content: '#pageContent'
            };
            self.toggleRightDrawer = function () {
                return oj.OffcanvasUtils.toggle(self.rightDrawerParams);
            };
            self.buttonClick = function () {
                if (typeof self.showingFront == 'undefined') {
                    self.showingFront = true;
                }
                var elem = document.getElementById('animatableCard');
                // Determine startAngle and endAngle
                var startAngle = self.showingFront ? '0deg' : '180deg';
                var endAngle = self.showingFront ? '180deg' : '0deg';
                // Animate the element
                oj.AnimationUtils['flipOut'](elem, {
                    'flipTarget': 'children',
                    'persist': 'all',
                    'startAngle': startAngle,
                    'endAngle': endAngle
                });
                self.showingFront = !self.showingFront;
            };


            self.isHideLeftMenue = ko.observable(true);
            self.navCollMini = function () {
                $('.DDMenuBody').attr({ 'style': 'display:block !important;' });
                self.drawIconsInMenu();
                self.drawIconsSpecialist();
            };

            self.mobNavDis = function () {
                $('#mobileNavBar').addClass('app_cont_fst_sec ');
                $('#mobileNavBar').removeClass('app_cont_fst_sec_col');
            };
            self.closeHeaderNav = function () {
                $('#mobileNavBar').addClass('app_cont_fst_sec_col');
                $('#mobileNavBar').removeClass('app_cont_fst_sec');
            };


            self.navCollMax = function () {

                $('.DDMenuBody').attr({ 'style': 'display:none !important;' });


            };


            self.setLocale = function (lang) {

                oj.Config.setLocale(lang,
                    function () {
                        self.refreshViewForLanguage(false);
                        if (lang === 'ar') {
                            $("html").attr("lang", lang);
                            $("html").attr("dir", "rtl");
                            $('.panelstyle').addClass('arabicpanel');
                            $('.textResponsiveDetails').addClass('textResponsiveDetailsar');
                            $('#prefxsignoutor').addClass('prefxsignout_2');
                            $('#prefxsignoutor').removeClass('prefxsignout');
                            $('#drawerToggleButtonor').addClass('drawerToggleButton_2');
                            $('#drawerToggleButtonor').removeClass('drawerToggleButton');
                            $('.approval-card-body').removeClass('app-crd-bdy-border-en');
                            $('.approval-card-body').addClass('app-crd-bdy-border-ar');
                            $('.app-crd-type').removeClass('app-card-for-en');
                            $('.app-crd-type').addClass('app-card-for-ar');
                            $('.blockquote-footer').removeClass('app-card-for-en');
                            $('.blockquote-footer').addClass('app-card-for-ar');


                        } else {
                            $("html").attr("lang", lang);
                            $("html").attr("dir", "ltr");

                            $('.panelstyle').removeClass('arabicpanel');
                            $('.textResponsiveDetails').addClass('textResponsiveDetailseng');
                            $('#prefxsignoutor').addClass('prefxsignout');
                            $('#prefxsignoutor').removeClass('prefxsignout_2');
                            $('#drawerToggleButtonor').addClass('drawerToggleButton');
                            $('#drawerToggleButtonor').removeClass('drawerToggleButton_2');
                            $('.approval-card-body').removeClass('app-crd-bdy-border-ar');
                            $('.approval-card-body').addClass('app-crd-bdy-border-en');
                            $('.app-crd-type').removeClass('app-card-for-ar');
                            $('.app-crd-type').addClass('app-card-for-en');
                            $('.blockquote-footer').removeClass('app-card-for-ar');
                            $('.blockquote-footer').addClass('app-card-for-en');


                        }
                        initTranslations();
                        //self.getBusinessUnits();

                        self.refreshViewForLanguage(true);
                    }
                );
            };

            self.openDialog = function () {

                document.querySelector("#detailsImage").open();
            };
            self.cancelButtonOk = function () {
                document.querySelector("#detailsImage").close();
            };

            // getAllNotificationsDetails
            //                self.notificationsCount = ko.observable();
            function getAllNotificationsDetails() {
                var jsonData = ko.toJSON(self.noticationModel);
                var getNotificationCBF = function (data) {
                    self.notificationsCount(data.length);
                };
                services.addGeneric(commonhelper.allNotifications, jsonData).then(getNotificationCBF, self.failCbFn);
            }

            self.userRoles = ko.observableArray([]);

            // self.getUserRoles = function () {
            //     self.getCountEITRequest();
            //     var reportPaylod = {"reportName": "USER_ROLES", "personId": self.personDetails().personId};
            //     var getRolesCBCF = function (data) {
            //         console.log("getting")
            //         for (i = 0; i < data.length; i++) {
            //             self.userRoles.push({
            //                 roleId: data[i].ROLE_ID
            //             });
            //         }
            //         getAllNotificationsDetails();
            //     };
            //     services.getGenericReportAsync(reportPaylod).then(getRolesCBCF, self.failCbFn);

            //     self.noticationModel = {
            //         "MANAGER_ID": self.personDetails().personId,
            //         "managerOfManager": "x",
            //         "position": self.personDetails().positionId,
            //         "emp": self.personDetails().personId,
            //         "userRoles": self.userRoles()
            //     };

            // };
            self.getUserRoles = function () {
//                self.getCountEITRequest();
                var reportPaylod = { "reportName": "USER_ROLES", "personId": self.personDetails().personId };
                var getRolesCBCF = function (data) {
                    for (i = 0; i < data.length; i++) {
                        self.userRoles.push({
                            roleId: data[i].ROLE_ID
                        });
                    }
                    getAllNotificationsDetails();
                };
                services.getGenericReportAsync(reportPaylod).then(getRolesCBCF, self.failCbFn);
                self.noticationModel = {
                    "MANAGER_ID": self.personDetails().personId,
                    "managerOfManager": "x",
                    "position": self.personDetails().positionId,
                    "emp": self.personDetails().personId,
                    "userRoles": self.userRoles()
                };
            };
            //compare function 
        self.compare = function (post, operator, value) {
                var postType = typeof post;
                var valueType = typeof value;
                console.log(post +"Type: "+postType);
                console.log(value +"Type : "+valueType);
                console.log(('8'>'55'));
                console.log(('8'<'55'));
                if (postType == 'boolean' && valueType == 'boolean') {
                    switch (post & value) {
                        case 0:
                            return false;
                            break;
                        case 1:
                            return true;
                            break;
                    }
                }
                switch (operator) {
                    case '>':
                        return post > value;
                        break;
                    case '<':
                        return post < value;
                         break;
                    case '>=':
                        return post >= value;
                         break;
                    case '<=':
                        return post <= value;
                         break;
                    case '==':
                        return post == value;
                         break;
                    case '!=':
                        return post != value;
                         break;
                    case '===':
                        return post === value;
                         break;
                    case '!==':
                        return post !== value;
                         break;
                }
            };
            
            
            //draw menu with eit 
            // self.EitName = ko.observableArray();
    
            self.icons2 = ko.observableArray();

            self.iconNavigation = function (router, code) {
                self.navCollMax();
                oj.Router.rootInstance.store(code);
                oj.Router.rootInstance.go(router);
                return true;

            };
            self.drawIconsSpecialist = function () {

                if (self.personDetails()) {

                    self.icons2([
                        {
                            label: 'b', value: 'demo-icon-circle-b', router: 'personSearch', iconType: 'fa fa-users', text: self.specialist(), visible: true, code: "Job"
                        }


                    ]);


                }

            };


            // get All PaaS Defult Value
            self.getAllPaaSDefultValue = function () {

                var getPaasSDefultValueCBF = function (data) {

                    Object.assign(self.allPaaSDefultValuearry(), data);

                };
                services.getGeneric(commonhelper.allPaaSDefultValue).then(getPaasSDefultValueCBF, self.failCbFn);
            };

            self.getEITSegments = function (eitCode) {
                var tempArray = [];
                for (var i = 0; i < self.allEITArray().length; i++) {
                    if (self.allEITArray()[i].DESCRIPTIVE_FLEX_CONTEXT_CODE == eitCode) {

                        tempArray.push(self.allEITArray()[i]);
                    }
                }
                return tempArray;
            };
            if (self.userName() || self.password())
                self.getAllPaaSDefultValue();
            self.getAllEIT = function () {
                var getAllEitCBFN = function (data) {
                    self.allEITArray(data);

                };
                var lang = '';
                if (self.getLocale() === "ar") {
                    lang = 'AR';
                } else {
                    lang = 'US';
                }
                //  services.getAllEIT(self.EitCode, lang).then(getAllEitCBFN, self.failCbFn);
            };
            self.signOut = function () {

                self.loading(true);
                self.navTopArray([]);

                var today = new Date();
                var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
                var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
                var signOutdateTime = date + ' ' + time;
                var payloadsignOutHistory = {
                    "personNumber": self.userName(),
                    "browserAndDeviceDetails": deviceAndBrowserDetails,
                    "deviceIp": deviceIp,
                    "loginDate": self.loginDateFromSass(),
                    "signOut": signOutdateTime

                };
                var SShrsignOutHistory = function (data) {
                    setTimeout(function () {
                        var getEmpDetails = function () { };
                        services.logOutUser(self.userName(), self.jwt(), self.hostUrl()).then(getEmpDetails, getEmpDetails);
                    }, 1);

                    /* Replace the Fusion-SSHR/ with the project name
                     *  Works only on deployed if the link is (eg. https://example.com/Fusion-SSHR/ )
                     *  This method clears the URL with the project name to avoid any uncaught errors
                     */
                    window.history.pushState({}, document.title, "/" + "Fusion-SSHR-PROD/");
                    location.reload();
                    $('.MYdropdown_menu').hide();
                    self.isUserLoggedIn(false);
                    sessionStorage.setItem('username', '');
                    localStorage.setItem('selectedLanguage', '');
                    sessionStorage.clear();

                    if (self.getLocale() === "ar") {
                        $('html').attr({ 'dir': 'rtl' });
                    } else {
                        $('html').attr({ 'dir': 'ltr' });
                        $('.noti').removeClass('noti-count-ar');
                        $('.noti').addClass('noti-count');
                    }

                    if (!detectmob()) {
                        window.history.pushState({}, document.title, "/" + "Fusion-SSHR-PROD/");
                        location.reload();
                    } else {
                        oj.Router.rootInstance.go(self.showDashboard() ? 'dashboard' : 'home');
                    }
                    self.navTopArray([]);
                    self.navDataSource(new oj.ArrayTableDataSource(self.navTopArray(), { idAttribute: 'id' }));

                    self.navTopArray([]);
                    self.navDataSource(new oj.ArrayTableDataSource(self.navTopArray(), { idAttribute: 'id' }));
                    self.N_navDataSource(new ArrayDataProvider(self.NavIConsTopArr(), { keyAttributes: 'id' }));
                };
                services.addGeneric("SShrLoginHistory/UpdateSShrLoginHistory", JSON.stringify(payloadsignOutHistory)).then(SShrsignOutHistory, self.failCbFn());

            };
 self.BrowserDetect = {
                init: function () {
                    this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
                    this.version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion) || "an unknown version";
                    this.OS = this.searchString(this.dataOS) || "an unknown OS";
                },
                searchString: function (data) {
                    // console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        var dataString = data[i].string;
                        var dataProp = data[i].prop;
                        this.versionSearchString = data[i].versionSearch || data[i].identity;
                        if (dataString) {
                            if (dataString.indexOf(data[i].subString) != -1)
                                return data[i].identity;
                        } else if (dataProp)
                            return data[i].identity;
                    }
                },
                searchVersion: function (dataString) {
                    var index = dataString.indexOf(this.versionSearchString);
                    if (index == -1)
                        return;
                    return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
                },
                dataBrowser: [{
                    string: navigator.userAgent,
                    subString: "Chrome",
                    identity: "Chrome"
                }, {
                    string: navigator.userAgent,
                    subString: "OmniWeb",
                    versionSearch: "OmniWeb/",
                    identity: "OmniWeb"
                }, {
                    string: navigator.vendor,
                    subString: "Apple",
                    identity: "Safari",
                    versionSearch: "Version"
                }, {
                    prop: window.opera,
                    identity: "Opera",
                    versionSearch: "Version"
                }, {
                    string: navigator.vendor,
                    subString: "iCab",
                    identity: "iCab"
                }, {
                    string: navigator.vendor,
                    subString: "KDE",
                    identity: "Konqueror"
                }, {
                    string: navigator.userAgent,
                    subString: "Firefox",
                    identity: "Firefox"
                }, {
                    string: navigator.vendor,
                    subString: "Camino",
                    identity: "Camino"
                }, {// for newer Netscapes (6+)
                    string: navigator.userAgent,
                    subString: "Netscape",
                    identity: "Netscape"
                }, {
                    string: navigator.userAgent,
                    subString: "MSIE",
                    identity: "Explorer",
                    versionSearch: "MSIE"
                }, {
                    string: navigator.userAgent,
                    subString: "Gecko",
                    identity: "Mozilla",
                    versionSearch: "rv"
                }, {// for older Netscapes (4-)
                    string: navigator.userAgent,
                    subString: "Mozilla",
                    identity: "Netscape",
                    versionSearch: "Mozilla"
                }],
                dataOS: [{
                    string: navigator.platform,
                    subString: "Win",
                    identity: "Windows"
                }, {
                    string: navigator.platform,
                    subString: "Mac",
                    identity: "Mac"
                }, {
                    string: navigator.userAgent,
                    subString: "iPhone",
                    identity: "iPhone/iPod"
                }, {
                    string: navigator.platform,
                    subString: "Linux",
                    identity: "Linux"
                }]

            }
        self.BrowserDetect.init();
            self.refresh = function () {
                initTranslations();
                if (!self.personDetails()) {
                    console.log(self.BrowserDetect);
//                      
                       
                 if(navigator.userAgent.indexOf('Edge') <= 0){
                    $.when(showEmplDetails(ko, services, self, km, false, commonhelper)).done(function () {
                        if (self.personDetails()) {
                        } else {

                            self.isUserLoggedIn(false);
                        }
                    });
                }else{
                    $.notify("Please Use Google chrome Or Firefox", "error");
                    self.disOnLogin(true);
                }


                } else {
                    self.isUserLoggedIn(true);
                    self.globalFuseModel(JSON.parse(sessionStorage.getItem("fuseData")));
                    self.allEITArray(JSON.parse(sessionStorage.getItem("allEitSegment")));
                    populateEitName(this, commonhelper, JSON.parse(sessionStorage.getItem("eitName")));
                    papulateUserRole(this, commonhelper, JSON.parse(sessionStorage.getItem("userRole")));
                }
                //self.getAllEIT();
                //pushNav();
                //self.getBusinessUnits();
                //self.getAllAbsenceTypes();
            };

            self.refresh();

            self.switchl = function () {
                document.querySelector("#switch").open();
            };
            self.switchclose = function () {
                $('.MYdropdown_menu').hide();
                document.querySelector("#switch").close();
            };
            self.switchLanguage = function () {

                $('.MYdropdown_menu').hide();
                document.querySelector("#switch").close();
                if (self.getLocale() === "ar") {
                    self.loading(true);
                    self.setLocale("en-US");
                    localStorage.setItem("selectedLanguage", "en-US");
                    addFuseModelToIndexedDB(services, self, self, commonhelper);
                } else if (self.getLocale() === "en-US") {
                    self.loading(true);
                    self.setLocale("ar");
                    localStorage.setItem("selectedLanguage", "ar");
                    addFuseModelToIndexedDB(services, self, self, commonhelper);
                }

            };
            self.confirm = function () {
                document.querySelector("#signoutDialog").open();
            };

            self.cancelButton = function () {
                $('.MYdropdown_menu').hide();
                document.querySelector("#signoutDialog").close();
            };

            //        if (typeof (Storage) !== "undefined") {
            var selectedLanguage = localStorage.getItem("selectedLanguage");

            if (selectedLanguage !== null && (selectedLanguage === "en-US" || selectedLanguage === "ar")) {
                self.setLocale(selectedLanguage);
            }
            //        }

            //for backbtn
            self.BackToHome = function () {
                oj.Router.rootInstance.go(self.showDashboard() ? 'dashboard' : 'home');
                console.log("back to home");
            };


            //End
            self.getPaaSLookup = function (lookupName) {
                var temparry = [];
                //self.passReturnArr([]);
                for (var i = 0; i < self.PaaSLookup().length; i++) {
                    if (self.PaaSLookup()[i].name == lookupName) {
                        if (localStorage.getItem("selectedLanguage") === "ar") {

                            temparry.push({
                                value: self.PaaSLookup()[i].code, label: self.PaaSLookup()[i].valuAr
                            });
                        } else {
                            temparry.push({
                                value: self.PaaSLookup()[i].code, label: self.PaaSLookup()[i].valuEn
                            });
                        }
                    }
                }

                return temparry;
            };

            self.getEITIcons = function (eitcode, icontype) {

                for (var i = 0; i < self.EITIcons().length; i++) {
                    if (self.EITIcons()[i].eitCode === eitcode && icontype == "ImageIcone") {

                        return self.EITIcons()[i].icons;
                    } else if (self.EITIcons()[i].eitCode === eitcode && icontype == "FontAwesome") {

                        return self.EITIcons()[i].leftIcons;

                    }
                }


            };
            //icons setup

            self.getEITIconsSetup = function (eitcode, icontype) {
                for (var i = 0; i < self.EITIcons().length; i++) {

                    if (self.EITIcons()[i].eitCode === eitcode && icontype == "ImageIcone") {
                        return self.EITIcons()[i].icons;
                    } else if (self.EITIcons()[i].eitCode === eitcode && icontype == "FontAwesome") {

                        return self.EITIcons()[i].NAME;

                    }
                }


            };
            //end icons setup




            self.getPersonNumber = function (personNumber) {

                for (var i = 0; i < self.PersonNumberArr().length; i++)
                    if (self.PersonNumberArr()[i].value == personNumber) {
                        return self.PersonNumberArr()[i].label;
                    }

            };
            self.conutEitRequest = ko.observableArray([]);
            self.getCountEITRequest = function () {
                // self.firstName(self.personDetails().firstName);
                // self.lastName(self.personDetails().lastName);
                // self.initials(oj.IntlConverterUtils.getInitials(self.firstName(),self.lastName()));
                // var getCountEITRequestCBF = function (data) {
                //     self.conutEitRequest(data);

                // };
                // await services.getGenericAsync("PeopleExtra/countRequest/" + self.personDetails().personId).then(getCountEITRequestCBF, self.failCbFn);
            };

            function initTranslations() {
                self.Usernametext(getTranslation("login.userName"));
                self.passwordLabel(getTranslation("login.Password"));
                self.loginLabel(getTranslation("login.loginLabel"));
                self.lblSignOut(getTranslation("login.SignOut"));
                self.SignIn(getTranslation("login.SignIn"));
                self.forgetPassword(getTranslation("login.forgetPassword"));
                self.Searchlbl(getTranslation("login.Search"));
                self.ok(getTranslation("login.ok"));
                self.managerType(getTranslation("login.managerType"));
                self.yes(getTranslation("others.yes"));
                self.no(getTranslation("others.no"));
                self.showDetails(getTranslation("login.showDetails"));
                self.confirmMessage(getTranslation("labels.confirmMessage"));
                self.oprationMessage(getTranslation("labels.message"));
                self.projectedStartDate(getTranslation("login.projectedStartDate"));
                self.assignmentProjectedEndDate(getTranslation("login.assignmentProjectedEndDate"));
                self.changeLbl(getTranslation("labels.changeLang"));
                self.signoLbl(getTranslation("labels.signOut"));
                self.peopleGroup(getTranslation("login.peopleGroup"));
                self.assignmentStatusTypeId(getTranslation("login.assignmentStatusTypeId"));
                self.fax(getTranslation("login.workPhoneExt"));
                self.faxExt(getTranslation("login.faxExt"));
                self.organizationName(getTranslation("login.organizationName"));
                self.BusinessMissionRequestLbl(getTranslation("pages.BusinessMissionRequest"));
                self.overtimeRequestLbl(getTranslation("pages.overtimeRequest"));
                self.userKeywordLable(getTranslation("login.KeywordLable"));
                self.viewProfileLbl(getTranslation("common.viewProfile"));
                self.viewBulkLbl(getTranslation("common.bulk"));
                self.specialist(getTranslation("common.specialist"));
                self.SpecialistLbl(getTranslation("common.specialist"));
                self.employeeTimeAttandancelbl(getTranslation("pages.employeeTimeAttandance"));
                if (self.loginFailureText() !== undefined && self.loginFailureText() !== "") {
                    self.loginFailureText(getTranslation("login.loginFailureText"));
                }
                //constructNavLeftData();
                //                  self.constructNavTopData();
                self.navDataSource(new oj.ArrayTableDataSource(self.NavIConsTopArr(), { idAttribute: 'id' }));
                self.N_navDataSource(new ArrayDataProvider(self.NavIConsTopArr(), { keyAttributes: 'id' }));

                self.languageSwitch_lng(getTranslation("common.switchLang"));

                self.valueofsharethouts('Share thoughts with colleagues..')
                self.jobLbl(getTranslation("pages.job"));
                self.employeeNewsLbl(getTranslation('others.employeeNews'));
                self.myFlagsLbl(getTranslation("others.myFlags"));
            }

            self.isLastApprover = function (transationId, ss_type, isOperationScreen) {
                //isOperationScreen : When isOperationScreen :  
                var roleType, roleId;
                var isLast;
                var responseCode;
                var getLastApprove = function (data) {
                    if (data.length) {
                        roleType = data[0].rolrType;
                        roleId = data[0].roleId;
                        responseCode = data[0].responseCode;
                        if (roleType == "POSITION" && roleId == self.personDetails().positionId && !isOperationScreen) {

                            isLast = true;

                        } else if ((roleType == "LINE_MANAGER" || roleType == "LINE_MANAGER+1") && roleId == self.personDetails().personId) {

                            isLast = true;

                        } else if (roleType == "ROLES" && !isOperationScreen) {
                            for (var i = 0; i < self.userRoles().length; i++) {
                                if (self.userRoles()[i].roleId == roleId) {
                                    isLast = true;

                                    break;
                                }
                            }

                        } else if (roleType == "JOB_LEVEL" && roleId == self.personDetails().personId) {
                            isLast = true;
                        } else if (roleType == "AOR" && roleId == self.personDetails().personId) {
                            isLast = true;
                        } else if (isOperationScreen) {
                            isLast = false;
                        }
                    } else {
                        isLast = true;

                    }

                };


                services.getGeneric(commonhelper.isLastStepApproval + transationId + "/" + ss_type).then(getLastApprove, self.failCbFn);
                return isLast;
            };


            self.OpenReportSeesion = async function () {
                var roleType, roleId;
                var isLast;
                var geOpenReportSeesionCBFN = function (data) {
                };
                await services.getGenericAsync(commonhelper.reportSession).then(geOpenReportSeesionCBFN, self.failCbFn);
                return isLast;
            };

            //                self.OpenReportSeesion();
            self.getApprovalCondetion = function (eitCode, model) {
                var approvalCode = 'Default';
                var firstKey;
                var operation;
                var secondKey;
                for (var i = 0; i < self.ApprovalCondition().length; i++) {
                    if (self.ApprovalCondition()[i].eitCode == eitCode) {
                        if (self.ApprovalCondition()[i].firstKeyType == "static") {
                            firstKey = self.ApprovalCondition()[i].firstKeyType;
                        } else if (self.ApprovalCondition()[i].firstKeyType == "eitSegment") {
                            firstKey = model[self.ApprovalCondition()[i].firstEitSegment]()
                        }
                        operation = self.ApprovalCondition()[i].operation;
                        if (self.ApprovalCondition()[i].secondKeyType == "static") {
                            secondKey = self.ApprovalCondition()[i].secondKeyValue
                        } else if (self.ApprovalCondition()[i].secondKeyType == 'eitSegment') {
                            secondKey = model[self.ApprovalCondition()[i].secondKeyType]
                        }
                        if (self.compare(firstKey, operation, secondKey)) {
                            approvalCode = self.ApprovalCondition()[i].approvalCode;
                            return approvalCode;
                        }
                    }
                }
                return approvalCode;

            };
         

            self.dialogYes = ko.observable();
            self.dialogNo = ko.observable();
            self.msg = ko.observable('')
            self.confirmMsg = function (msg, cb) {
                self.msg(msg);
                self.dialogYes(function () {
                    document.querySelector("#GenericYesNoDialog").close();
                    cb(true)
                })
                self.dialogNo(function () {
                    document.querySelector("#GenericYesNoDialog").close();
                    cb(false)
                })
                document.querySelector("#GenericYesNoDialog").open();
            };
            self.infMsg = ko.observable();
            self.infoMsg = function (msg) {
                self.infMsg(msg)
                document.querySelector("#GenericInfoDialog").open();
            }
            self.BrowserDetect.init();
            function detectmob() {
                if (navigator.userAgent.match(/Android/i)
                    || navigator.userAgent.match(/webOS/i)
                    || navigator.userAgent.match(/iPhone/i)
                    || navigator.userAgent.match(/iPad/i)
                    || navigator.userAgent.match(/iPod/i)
                    || navigator.userAgent.match(/BlackBerry/i)
                    || navigator.userAgent.match(/Windows Phone/i)
                ) {
                    return true;
                }
                else {
                    return false;
                }


                ///// mobile
                var isMobile = {
                    Android: function () {
                        return navigator.userAgent.match(/Android/i);
                    },
                    BlackBerry: function () {
                        return navigator.userAgent.match(/BlackBerry/i);
                    },
                    iOS: function () {
                        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
                    },
                    Opera: function () {
                        return navigator.userAgent.match(/Opera Mini/i);
                    },
                    Windows: function () {
                        return navigator.userAgent.match(/IEMobile/i);
                    },
                    any: function () {
                        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
                    }
                };

            }

            // start generic file viewer
            self.fileType = ko.observable();
            self.pdfViewer = function (pathOrBase64, type) {
                var element;
                if (document.querySelector("#fileViewer") && pathOrBase64) {
                    element = document.getElementById("fileViewerTemplate");
                    if (pathOrBase64 && element) {
                        element.src = pathOrBase64;
                        document.querySelector("#fileViewer").open();
                    }
                }
            }

            self.closeAttachmentViewer = function (event) {
                if (document.getElementById("fileViewer"))
                    document.getElementById('fileViewer').close();
            };
            // end generic file viewer
            self.translate = function (key) {
                return getTranslation(key);
            }


            //-------left menu data--------
            //note:this data/info was dublicated in home.js and bulkmenu 
            //and will be copied for any page need to implement left side menu(employee details)
            //so we put it here to be generic and used in html by using $root before name of observable
            self.bindLeftMenuData = function(){
                if (self.conutEitRequest()[0]) {
                    self.countPending(self.conutEitRequest()[0].countPendingApproved);
                    self.countApproved(self.conutEitRequest()[0].countApproved);
                    self.countRejected(self.conutEitRequest()[0].countRejected);
                }
                self.initials = oj.IntlConverterUtils.getInitials(self.personDetails().firstName, self.personDetails().lastName);
            }
            //-----------------------------
    
            
    
  





            
            
            
        
        }
        return new ControllerViewModel();
    }

);
