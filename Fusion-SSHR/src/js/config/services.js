/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define(['config/serviceconfig', 'util/commonhelper'],
        function (serviceConfig, commonHelper) {

            /**
             * 
             * 
             * The view model for managing all services
             */
            function services() {

                var self = this;

                var servicesHost = commonHelper.getSaaSHost();
                var paasServiceHost = commonHelper.getPaaSHost();
                //var appServiceHost = commonHelper.getAppHost();
                var appServiceHost = "";
                var biReportServletPath = commonHelper.getBiReportServletPath();
                var restPath = commonHelper.getInternalRest();
                // POST example
                self.authenticate = function (userName, password) {
                    var serviceURL = restPath + "login/login2";
                    var payload = {
                        "userName": userName, "password": password, "mainData": 'true'
                    };

                    var headers = {
                        // "Authorization":"Basic amphZG9vbkBoaGEuY29tLnNhOmhoYUAxMjMkTUlT"
                    };
                    return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
                };



                self.getNotification = function (headers) {
                    var serviceURL = paasServiceHost + 'workflowApproval/';
                    return serviceConfig.callGetApexService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, true);
                };
                self.workflowAction = function (payload) {
                    var serviceURL = paasServiceHost + 'workflowApproval/';
                    var headers = {
                    };

                    return serviceConfig.callPostApexService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, headers, true);
                };
                self.getFuseModelReport = function () {
                    var serviceURL = appServiceHost + restPath + biReportServletPath;

                    var payload = {
                        "reportName": "FuseReport2"
                    };
                    var headers = {

                    };
                    return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, false, headers);
                };
                self.getMasterDataReport = function (userName, lang) {
                    var serviceURL = appServiceHost + restPath + biReportServletPath;

                    var payload = {
                        "reportName": "MasterDataReport",
                        "LANG": lang,
                        "LANGX": lang,
                        "USERNAME": userName
                    };
                    var headers = {

                    };
                    return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
                };
                self.getEitNameReportReport = function (Lang) {
                    var serviceURL = appServiceHost + restPath + biReportServletPath;

                    var payload = {
                        "reportName": "EitNameReport",
                        "LANG": Lang
                    };
                    var headers = {

                    };
                    return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, false, headers);

                };
                self.getEIT = function (Structure_Code, LANG) {
                    var serviceURL = appServiceHost + restPath + biReportServletPath;
                    var payload = {
                        "Structure_Code": Structure_Code, "reportName": "EIT Report", "LANG": LANG
                    };
//console.log(payload)
                    var headers = {

                    };
                    return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, false, headers);
                };
                self.getEITasync = function (Structure_Code, LANG) {
                    var serviceURL = appServiceHost + restPath + biReportServletPath;
                    var payload = {
                        "Structure_Code": Structure_Code, "reportName": "EIT Report", "LANG": LANG
                    };

                    var headers = {

                    };
                    return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
                };
                self.getAllEIT = function (Structure_Code, LANG) {
                    var serviceURL = appServiceHost + restPath + biReportServletPath;
                    var payload = {
                        "reportName": "getAllEITReport", "LANG": LANG
                    };

                    var headers = {

                    };
                    return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, false, headers);
                };
                self.getAbsenceReport = function () {

                    var serviceURL = appServiceHost + restPath + biReportServletPath;
                    var payload = {
                        "reportName": "AbsenceReport", "employeeNumber": "204"
                    };
                    var headers = {

                    };
                    return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
                };
                self.getQueryForListReport = function (Structure_Code) {
                    var serviceURL = appServiceHost + restPath + biReportServletPath;
                    var payload = {
                        "valueSet": Structure_Code, "reportName": "getQueryForListReport"
                    };

                    var headers = {

                    };
                    return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, false, headers);
                };
                self.getDynamicReport = function (str) {
                    var serviceURL = appServiceHost + restPath + biReportServletPath;
                    var payload = {
                        "str": str, "reportName": "DynamicReport2"
                    };

                    var headers = {

                    };
                    return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, false, headers);
                };
                self.getElementEntry = function () {
                    var serviceURL = paasServiceHost + "elementEntry";
                    var headers = {
                    };

                    return serviceConfig.callGetApexService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, true);

                };
                self.addElementEntry = function (payload) {
                    var serviceURL = paasServiceHost + "elementEntry";
                    var headers = {
                    };

                    return serviceConfig.callPostApexService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
                };
                self.editElementEntry = function (payload) {
                    var serviceURL = paasServiceHost + "elementEntry";
                    var headers = {
                    };

                    return serviceConfig.callPutApexService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
                };
                self.getElementEnteryById = function (id) {
                    var serviceURL = paasServiceHost + "elementEntery/" + id;
                    var headers = {
                    };

                    return serviceConfig.callGetApexService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, true);
                };

                self.getElementEntryValue = function () {
                    var serviceURL = paasServiceHost + "elementEnteryValue";
                    var headers = {
                    };

                    return serviceConfig.callGetApexService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, true);

                };
                self.addElementEntryValue = function (payload) {
                    var serviceURL = paasServiceHost + "elementEnteryValue";
                    var headers = {
                    };

                    return serviceConfig.callPostApexService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
                };
                self.editElementEntryValue = function (payload) {
                    var serviceURL = paasServiceHost + "elementEnteryValue";
                    var headers = {
                    };

                    return serviceConfig.callPutApexService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
                };

                self.logOutUser = function (username2, token, hostURL) {

                    var serviceURL = restPath + "emp/logout?username2=" + username2 + "&token=" + token + "&hostURL=" + hostURL;
                    var headers = {
                    };
                    return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, false);
                };

                self.getEmpDetails = function (username2, token, hostURL) {

                    var serviceURL = restPath + "emp/info?username2=" + username2 + "&token=" + token + "&hostURL=" + hostURL + "&mainData=false";
                    var headers = {
                    };
                    return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, true);
                };

                self.getEmpDetailsAsync = function (username2, token, hostURL) {

                    var serviceURL = restPath + "emp/info?username2=" + username2 + "&token=" + token + "&hostURL=" + hostURL + "&mainData=false";
                    var headers = {
                    };
                    return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, false);
                };

                self.getAllPositions = function () {
                    var serviceURL = appServiceHost + restPath + biReportServletPath;
                    var payload = {
                        "reportName": "GetAllPositions"

                    };
                    var headers = {

                    };
                    return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, false, headers);
                };
                self.getEmpDetailsbyPersonId = function (personId, token, hostURL) {

                    var serviceURL = restPath + "emp/infoById?personId=" + personId + "&token=" + token + "&hostURL=" + hostURL;
                    var headers = {
                    };
                    return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, false);
                };

                self.searchEmployees = function (name, nationalId, personNumber, managerId, effectiveAsof, token, hostURL) {

                    var serviceURL = restPath + "emp/searchEmployees?name=" + name + "&nationalId=" + nationalId + "&personNumber=" + personNumber + "&managerId=" + managerId + "&effectiveAsof=" + effectiveAsof;

                    var headers = {
                    };
                    return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, false);
                };


                self.submitElementEntry = function (payload) {
                    var serviceURL = appServiceHost + restPath + "upload/elementEntry";
                    var headers = {

                    };
                    return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
                };
               self.submitElementEntryBulk = function (payload) {
                    var serviceURL = appServiceHost + restPath + "upload/elementEntryBulk";
                    var headers = {

                    };
                    return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
                };



                self.submitHDLFile = function (url, payload) {
                    var serviceURL = appServiceHost + restPath + "upload/" + url;
                    var headers = {

                    };
                    return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
                };
                self.getPersonElementEntry = function (personId, startDate) {
                    var serviceURL = appServiceHost + restPath + biReportServletPath;
                    var payload = {
                        "P_PERSON_ID": personId, "P_START_DATE": startDate, "reportName": "PersonElementEntryReport"
                    };
                    var headers = {

                    };
                    return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, false, headers);
                };
                self.getEmployees = function (userName, token, hostURL) {
                    var serviceURL = restPath + "emp/getEmployees?username=" + name + "&token=" + token + "&hostURL=" + hostURL;
                    var headers = {
                    };
                    return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, false);
                };
                self.getHDLFiles = function (id, ssType, personNumber, effectiveDate, processedInPayroll, assigment) {
                    var serviceURL = paasServiceHost + "HDL";
                    var headers = {

                    };

                    if (id) {
                        headers.id = id;
                    }

                    if (ssType) {
                        headers.SS_TYPE = ssType;
                    }

                    if (effectiveDate) {
                        headers.CREATION_DATE = effectiveDate;
                    }

                    if (personNumber) {
                        headers.PERSON_NUMBER = personNumber;
                    }

                    if (processedInPayroll) {
                        headers.STATUS = processedInPayroll;
                    }

                    if (assigment) {
                        headers.ASSIGNMENT_STATUS_TYPE = assigment;
                    }

                    return serviceConfig.callGetApexService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, true);
                };

                self.processElementEntry = function (payload) {
                    var serviceURL = appServiceHost + restPath + "upload/elementEntry";
                    var headers = {

                    };
                    return serviceConfig.callPutService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
                };
                self.getPayrollReport = function (personId) {
                    var serviceURL = appServiceHost + restPath + biReportServletPath;
                    var payload = {
                        "reportName": "EmployeePrepaymentInPeriodReport"
                    };

                    var headers = {

                    };
                    return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
                };

                self.getGeneric = function (serviceName, headers) {
                    var serviceURL = restPath + serviceName;

//          headers.Authorization = "Basic YW1yby5hbGZhcmVzQGFwcHNwcm8tbWUuY29tOkFwcHNwcm9AMTIzNA=="
                    return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, false);
                };

                self.getGenericAsync = function (serviceName, headers) {
                    var serviceURL = restPath + serviceName;

//          headers.Authorization = "Basic YW1yby5hbGZhcmVzQGFwcHNwcm8tbWUuY29tOkFwcHNwcm9AMTIzNA=="
                    return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, true);
                };
                self.editGeneric = function (serviceName, payload) {
                    var serviceURL = restPath + serviceName;
                    var headers = {
                    };
                    return serviceConfig.callPutApexService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
                };
                
                self.addGeneric = function (serviceName, payload) {
                    var serviceURL = restPath + serviceName;

                    var headers = {
                    };
                    return serviceConfig.callPostApexService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, false, headers);
                };
                self.addFile = function (serviceName, payload) {
                    var headers = {
                    };
                    var serviceURL = restPath + serviceName;
                    return serviceConfig.callAddFileService(serviceURL, payload, headers);
                };
                self.getGeneric3 = function (serviceName) {
                    var serviceURL = restPath + serviceName;
                    var headers = {
                    };

                    return serviceConfig.callGetApexService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, true);
                };
                self.jobs = function (payload, type) {

                    var serviceURL = restPath + "jobs/" + type;
                    var headers = {
                    };

                    return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
                };
                self.searcheGeneric = function (serviceName, payload) {
                    var serviceURL = restPath + serviceName;
                    var headers = {
                        "content-type": "application/x-www-form-urlencoded"
                    };

                    return serviceConfig.callPostServiceUncodeed(serviceURL, payload, serviceConfig.contentTypeApplicationXWWw, true, headers);
                };
                self.searcheGeneric2 = function (serviceName, payload) {
                    var serviceURL = restPath + serviceName;
                    var headers = {
                        "content-type": "application/x-www-form-urlencoded"
                    };

                    return serviceConfig.callPostServiceUncodeed2(serviceURL, payload, serviceConfig.contentTypeApplicationXWWw, true, headers);
                };
                //  add delete Generic function
                self.deleteGeneric = function (serviceName, payload) {
                    var serviceURL = restPath + serviceName;
                    var headers = {
                    };

                    return serviceConfig.callDeleteService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
                };

                self.searchGenericUncodeedAsync = function (serviceName, payload) {
                    var serviceURL = restPath + serviceName;
                    var headers = {
                        "content-type": "application/x-www-form-urlencoded"
                    };

                    return serviceConfig.callPostServiceUncodeedAsync(serviceURL, payload, serviceConfig.contentTypeApplicationXWWw, true, headers);
                };

                self.searcheGenericAsync = function (serviceName, payload) {
                    var serviceURL = restPath + serviceName;
                    var headers = {
                        "content-type": "application/x-www-form-urlencoded"
                    };

                    return serviceConfig.callPostServiceUncodeedAsync(serviceURL, payload, serviceConfig.contentTypeApplicationXWWw, false, headers);
                };
                self.getJobByJobName = function () {
                    var serviceURL = restPath + "jobs/search/66";



                    var headers = {

                    };
                    return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, false);
                };//PATCH
                self.genericPatch = function (serviceName, payload) {
                    var serviceURL = restPath + serviceName;
                    var headers = {
                    };
                    return serviceConfig.callPostApexService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
                };

                self.addReport = function (serviceName, payload) {
                    var serviceURL = restPath + serviceName;

                    var headers = {
                    };
                    return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
                };

                self.genericAddOrEdit = function (serviceName, payload, type) {

                    var serviceURL = restPath + serviceName + type;

                    var headers = {
                    };

                    return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
                };

                self.getReportName = function (serviceName) {
                    var serviceURL = restPath + serviceName;
                    var headers = {
                    };

                    return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, false);
                };
                self.getAllSummary = function (serviceName) {
                    var serviceURL = restPath + serviceName;
                    var headers = {
                    };

                    return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, false);
                };
                self.getAllRoleValidation = function (serviceName) {
                    var serviceURL = restPath + serviceName;

                    var headers = {
                    };

                    return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, false);
                };
                self.workflowAction = function (serviceName, payload, type) {

                    var serviceURL = restPath + serviceName;
                    var headers = {
                    };

                    return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
                };
                self.getLookUpCode = function (serviceName) {
                    var serviceURL = restPath + serviceName;

                    var headers = {
                    };

                    return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, false);
                };

                self.getBulkReports = function (payload) {
                    var serviceURL = appServiceHost + restPath + "bulk/get";

                    //            var payload = {
                    //                "reportName": "EitNameReport"
                    //            };
                    var headers = {

                    };
                    return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, false, headers);
                };
                self.getLookUpName = function (serviceName) {
                    var serviceURL = restPath + serviceName;

                    var headers = {
                    };

                    return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, false);
                };
//                self.getGenericReport = function (payload) {
//                    var serviceURL = appServiceHost + restPath + biReportServletPath;
//
//                    //            var payload = {
//                    //                "reportName": "EitNameReport"
//                    //            };
//                    var headers = {
//
//                    };
//                    return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, false, headers);
//                };
//                
//                self.getBulkReports = function (payload) {
//                    var serviceURL = appServiceHost + restPath + "bulk/get";

                self.getGenericReportAsyn2 = function () {
                    var serviceURL = appServiceHost + restPath + biReportServletPath;

                        var payload = {
                                   "reportName": "All_Employees_ in_specific_location",
                                   "LocationName": "HHA Head Office"
                               };
                    var headers = {

                    };
                    return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
                };
                self.getGenericReportAsync = function (payload) {
                    var serviceURL = appServiceHost + restPath + biReportServletPath;
                    //            var payload = {
                    //                "reportName": "EitNameReport"
                    //            };
                    var headers = {
                    };
                    return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
                };

                self.getcountToValidation = function (serviceName, payload) {
                    var serviceURL = restPath + serviceName;

                    var headers = {

                    };
                    return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, false, headers);
                };
                self.getGrade = function (serviceName) {
                    var serviceURL = restPath + serviceName;

                    var headers = {

                    };
                    return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, false);
                };

                self.getAbsences = function (serviceName, headers) {
                    //var serviceURL = restPath + serviceName;
                    var serviceURL = serviceName;

                    return serviceConfig.callGetApexService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers);
                };

                self.getGenericPdfReport = function (payload) {

                    var serviceURL = appServiceHost + restPath + biReportServletPath;


                    var headers = {

                    };
                    return serviceConfig.callPostService(serviceURL, JSON.stringify(payload), serviceConfig.contentTypeApplicationJSON, false, headers);
                };


                self.getGenericAuthentication = function (userName) {
                    var payload = {"reportName": "UserAuthentication", "USERNAME": userName};
                    var serviceURL = appServiceHost + restPath + biReportServletPath;
                    var headers = {

                    };
                    return serviceConfig.callPostService(serviceURL, JSON.stringify(payload), serviceConfig.contentTypeApplicationJSON, false, headers);
                };
                self.getGenericDepartment = function () {
                    var payload = {"reportName": "EPPE_Dep_VS_R"};
                    var serviceURL = appServiceHost + restPath + biReportServletPath;
                    var headers = {

                    };
                    return serviceConfig.callPostService(serviceURL, JSON.stringify(payload), serviceConfig.contentTypeApplicationJSON, false, headers);
                };
                self.getLocation = function () {
                    var payload = {"reportName": "GET_LOCATION"};
                    var serviceURL = appServiceHost + restPath + biReportServletPath;
                    var headers = {

                    };
                    return serviceConfig.callPostService(serviceURL, JSON.stringify(payload), serviceConfig.contentTypeApplicationJSON, false, headers);
                };

                self.getAllEmp = function () {
                    var payload = {"reportName": "All_Employees_ in_specific_location", "LocationName": "HHA Head Office"};
                    var serviceURL = appServiceHost + restPath + biReportServletPath;
                    //console.log(serviceURL+JSON.stringify(payload));
                    var headers = {

                    };
                    return serviceConfig.callPostService(serviceURL, JSON.stringify(payload), serviceConfig.contentTypeApplicationJSON, false, headers);
                };

                self.getGenericReport = function (payload) {
                    var serviceURL =appServiceHost+ restPath + biReportServletPath;
                   var headers = {};
                    return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true,headers);
                };
                self.getGenericReportNotAsync = function (payload) {
                    var serviceURL =appServiceHost+ restPath + biReportServletPath;
                   var headers = {};
                    return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, false,headers);
                };

//                 self.getAllEmp = function () {
//                    var payload = {"reportName": "All_Employees_ in_specific_location"};
//                    var serviceURL = appServiceHost + restPath + biReportServletPath;
//                    var headers = {
//
//                    };
//                    return serviceConfig.callPostService(serviceURL, JSON.stringify(payload), serviceConfig.contentTypeApplicationJSON, false, headers);
//                };

            }
            ;

            return new services();
        });