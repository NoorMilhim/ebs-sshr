define([], function () {

    function commonHelper() {
        var self = this;

        self.getSaaSHost = function () {
            var host = "http://192.168.1.118:7101/EBSMobile/rest";
            return host;
        };

        self.getPaaSHost = function () {
            var host = "https://apex-hcuk.db.em2.oraclecloudapps.com/apex/xx_selfService/";
            return host;
        };

        self.getAppHost = function () {
            var host = "/HHA-SSHR-BackEnd/";
            return host;
        };
        self.HRC_YES_NO = 'HRC_YES_NO';
        self.jobHisoryUrl = "jobs/search/";
        self.validGradeUrl = "validGrade/";
        self.supervisorallowanceUrl = "supervisorallowance/";
        self.requirededuicationlevelUrl = "requirededuicationlevel/";
        self.securityallowanceUrl = "securityallowance/";
        self.clothesallowanceUrl = "clothesallowance/";
        self.requiredqualificationUrl = "requiredqualification/";
        self.jobContUrl = "jobs/jobobCount";
        self.positionSearchUrl = "position/search/";
        self.positionUrl = "position/";
        self.actualpositionSearchUrl = "actualposition/search/";
        self.actualpositionUrl = "actualposition/";
        self.actualpositionSearchCodeUrl = "actualposition/searchbycode/";
        self.worknatureallowanceUrl = "worknatureallowance/";
        self.positionSearchCodeUrl = "position/searchbycode/";
        self.workNatureAllowanceUrl = "worknatureallowance/";
        self.attachment = "attachment/add";
        self.orgnizationUrl = "org/";
        self.jobName = 'XXX_HR_JOB_NAME';
        self.JOB_TYPE_GROUP = 'XXX_HR_JOB_TYPE_GROUP';
        self.XXX_HR_JOB_GENERAL_GROUP = 'XXX_HR_JOB_GENERAL_GROUP';
        self.XXX_HR_JOB_CATEGORY = 'XXX_HR_JOB_CATEGORY';
        self.pdfReportLookup = 'XXX_HR_PAAS_REPORTS';
        self.HR_JOB_CODES = 'XXX_HR_JOB_CODES';
        self.HR_GRADE = 'XXX_HR_GRADE';
        self.HR_POSITIONS_NAME = 'XXX_HR_POSITIONS_NAME';
        self.HR_POSITION_ACTIONS = 'XXX_HR_POSITION_ACTIONS';
        self.EIT_Name = 'EitNameReport';
        self.HR_QUALIFICATIONS = 'XXX_HR_QUALIFICATIONS';
        self.HR_EFF_SEARCH = 'XXX_HR_STRUCTURE_SEARCH_CRITERIA';
        self.getEffData = "eff/details/";
        self.getEitsData = "EITSS/details/";
        self.jobFutureUrl = "jobs/jobFuture";
        self.getAllJobs = "jobs/jobsNames/";
        self.approvalSetupUrl = "approvalSetup/";
        self.elementEntry = "elementEntrySetup/";
        self.allElementEntries = "elementEntrySetup/getAllElements";
        self.validationSetupUrl = "vaildation/";
        self.addReportUrl = "summary/addSummary/";
        self.getReportName = "summary/reportName/";
        self.getAllSummary = "summary/Report/";
        self.getNotification = "workflowApproval/details";
        self.getNotify = "workflowApproval/notify/";
        self.getLookup = "xxx_paas_lookup/getLookup";
        self.addOrUpdateLookup = "xxx_paas_lookup/addOrEditPassLookup/";
        self.lookupCode = "xxx_paas_lookup/codeLookup/";
        self.lookupNmae = "xxx_paas_lookup/nameLookup/";
        self.getAllPaaSLookup = "xxx_paas_lookup/getPassLookup/";
        self.getDate = "date/HijrahDate";
        self.isLastStepApproval = "approvalList/lastStep/";
        self.reportPram = "summary/reportNamePram/";
        self.validationValuesName = "validation/validationValue/";
        self.allNotifications = "workflowApproval/allNotifications";
        self.searchNotifications = "workflowApproval/searchnotification";
        self.HRDYNAMICCOLUMNREPORT = "XXX_HR_DYNAMIC_COLUMN_REPORT";
        self.HR_DYNAMIC_COLUMN_REPORT = "XXX_HR_PAAS_DEFAULT_VALUES";
        self.getCountValidation = "validation/validationCount";
        self.gradeUrl = "orgGrade/";
        self.getIcons = "Icons";
        self.allPaaSDefultValue = "paasdefultvalue";
        self.getPeopleExtra = "PeopleExtra/details/";
        self.getBulkTransaction = "PassBulkTransaction/";
        self.getAllSummaryBulk = "PassBulkTransaction";
        self.getAllSummaryByBulkId = "PassBulkTransaction/getBulkTransaction";
        self.reportSession = 'summary/ReportCashing/';
        self.roleSetup = 'RoleSetup/addRoleSetup';
        self.getRoleName = 'RoleSetup/getRoleName';
        self.getAllRoleSetupValidation = 'RoleSetupValidation/getRoleSetupValidation';
        self.addOrUpdateRoleValidation = 'RoleSetupValidation/';
        self.getAllRoleSetup = 'RoleSetup/getAllRoleSetupe';
        self.roleSetupUpdate = 'RoleSetup/updateRoleSetup';
        self.employeeReqeustLookup = 'XXX_HR_EMPLOYEE_REQUESTS';
        self.getALLALLowanceRoleEmployee = 'RoleSetupValidation/getAllowEitCode';
        self.getEitCode = 'RoleSetupValidation/geteEitCode';
        self.XXX_HR_SPECIALIST_REQUESTS = 'XXX_HR_SPECIALIST_REQUESTS';
        self.XXX_HR_MANAGER_REQUESTS = 'XXX_HR_MANAGER_REQUESTS';
        self.getprobationAndEvaluation = 'ProbationPerformanceEvaluation/getProbationPerformanceEvaluation';
        self.addprobationAndEvaluation = 'ProbationPerformanceEvaluation/addProbationPerformanceEvaluation';
        self.UpdateprobationAndEvaluation = 'ProbationPerformanceEvaluation/UpdateProbationPerformanceEvaluation';
        self.getprobationAndEvaluationbypersonNumber = 'ProbationPerformanceEvaluation/getProbation';
        self.getAllApprovalCondition = 'ApprovalCondition/getApprovalCondition';
        self.addApprovalCondition = 'ApprovalCondition/addApprovalCondition';
        self.updateApprovalCondition = 'ApprovalCondition/updateApprovalCondition';
        self.getCountApprovalCondition = 'ApprovalCondition/getApprovalConditionCount/';
        self.getAllApprovalCode = 'ApprovalCondition/getAllApprovalCode/';
        self.getRoleSetupValidation = 'RoleSetupValidation/getALLRoleSetupValidation/';
        self.getSelfService = 'ManageSelfSerivce/';
        self.addSelfService = 'ManageSelfSerivce/';
        self.mailNotificationSearch='mailApproval/searchnotification';
        self.iconSetupUrl='iconSetup/'; 
        self.getIconSetup='iconSetup';

        // if you need to show the left menu you can change the self.hideLeftMenu = false
        self.hideLeftMenu = true;

        // if you need to show the dashboard you can change the self.showDashboard = true 
        self.showDashboard = false;
  




        self.REPORT_DATA_TYPE = {
            UDT: 'UDT', LOOKUP: 'LOOKUP', TERRITORIES: 'TERRITORIES'
        };

        self.FUSE_SERVICE_PARAM = {
            BINDLOOKUPTYPE: self.BTRIP_TYPE + ',' + self.YES_NO + ',' + self.ADV_MONTHS + ',' + self.SA_BANKS + ',' + self.BTRIPDRIVER_TYPE + ',' + self.BTRIPDRIVER_AREA + ',' + self.HRC_YES_NO + ',' + self.NADEC_HR_ID_MAIL_TYPE + ',' + self.NADEC_HR_IDENTIFICATION_LANG + ',' + self.HR_TICKETS_ROUTES + ',' + self.HR_TICKETS_REASONS + ',' + self.HR_TICKETS_CLASS + ',' + self.HR_GRIEVANCE_TYPE + ',' + self.HR_GRIEVANCE_STATUS + ',' + self.HR_MGR_GRIEVANCE_STATUS + ',' + self.HR_HEAD_GRIEVANCE_STATUS + ',' + self.EDU_YEARS + ',' + self.EDU_SEMESTER + ',' + self.HR_ALLOWANCES + ',' + self.NADEC_BTR_ROUTE_TYPE + ',' + self.HR_HEAD_HR_GRIEVANCE_STATUS + ',' + self.NADEC_CAR_INSIDE_LOV + ',' + self.HR_CAR_LOCATION + ',' + self.NADEC_TICKET_RAUTES_NEW_US + ',' + self.HR_NADEC_HOUSING_PERIOD + ',' + self.NADEC_TICKET_REFUND + ',' + self.NADEC_TRIP_DIRECTION_1 + ',' + self.NADEC_TICKET_RAUTES_NEW_US,
            P_TABLE_NAME: 'XXX_HR_REG_BTRIP_DAYS_B,XXX_HR_TRAIN_BTRIP_DAYS_B,XXX_HR_REG_BTRIP_DAYS_A,XXX_HR_TRAIN_BTRIP_DAYS_A,XXX_HR_REG_BTRIP_PERDIEM,XXX_HR_TRAIN_BTRIP_PERDIEM,XXX_HR_REG_BTRIP_TICKET,XXX_HR_TRAIN_BTRIP_TICKET,XXX_HR_PART_OF_EOS_AMT,XXX_HR_GLOBAL_VALUES,XXX_HR_ALLOWANCES_DETAILS,' + self.NADEC_BTR_ROUTE_TYPE, BINDLANGUAGE: 'US'
        };

        self.getBiReportServletPath = function () {
            var host = "report/commonbireport";
            return host;
        };

        self.getInternalRest = function () {
            //MOE dEPLOYED Test URL 
         //  var host = "https://144.21.78.176:7002/Fusion-SSHR-Backend-Jdev-prod/resources/";
         var host = "http://127.0.0.1:7101/Fusion-SSHR-Backend-Jdev-prod/rest/";
//         var host="http://localhost:7101/Fusion-SSHR-Backend-TEST/rest/"
           // var host = "https://144.21.83.54:7002/HHA-SSHR/webapi/";
        // var host = "https://144.21.78.176:7002/HHA-SSSHR-BackEnd/webapi/";
       //  var host ="https://hhajcs-a562419.java.em2.oraclecloudapps.com/Fusion-SSHR-Backend-Jdev/rest/";
//         var host = "https://hhajcs-a562419.java.em2.oraclecloudapps.com/Fusion-SSHR-Backend-Jdev-prod/rest/";
         
         
         //-------HHA LOCAL 
//        var host =  "https://hop1.hha.com.sa/Fusion-SSHR-Backend-Jdev-prod/resources/";
         
        
            return host;
        };

        self.isConnected = function () {
            //show toast
            if (navigator.connection && navigator.connection.type) {
                if (navigator.connection.type == 'none') {
                    if (window.plugins && window.plugins.toast)
                        window.plugins.toast.showShortCenter('No Internet Connection!', () => {
                        }, () => {
                        });
                    return false;
                }
            }
            return true;
        };
    }

    return new commonHelper();
});