/**
 * 
 * @param {type} oj
 * @param {type} ko
 * @param {type} $
 * @param {type} app
 * @param {type} commonUtil
 * @param {type} services
 * @param {type} translatehelper
 * @returns {JobL#13.JobContentViewModel}
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojknockout', 'ojs/ojtrain', 'ojs/ojbutton', 'ojs/ojarraydataprovider', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker',
    'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup', 'ojs/ojcheckboxset', 'ojs/ojradioset',
    'ojs/ojlabel', 'ojs/ojgauge', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojmessage', 'jquery-calendar',
    'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin','ojs/ojtable',
    'jquery-calendar-picker', 'ojs/ojvalidationgroup', 'ojs/ojarraydataprovider'],
        function (oj, ko, $, app, commonUtil, services) {
            /**
             * The view model for the main content view template
             */
            function JobContentViewModel() {
                var self = this;
                self.columnArray = ko.observableArray();
                self.addLbl = ko.observable();
                self.editLbl = ko.observable();
                self.viewLbl = ko.observable();
                self.lookupNameLbl = ko.observable();
                self.lookupcode1Lbl = ko.observable();
                self.lookupArabicVallLbl = ko.observable();
                self.lookupEnglishValLbl = ko.observable();
                self.keyWordSearch = ko.observable();
                self.searchval = ko.observable();
                self.Search = ko.observable();
                self.deleteLbl = ko.observable();
                self.RestLbl = ko.observable();
                self.searchLable = ko.observable();
                self.checkResultSearch = ko.observable();
                self.backActionLbl = ko.observable();   


                var deptArray = [];
                self.summaryObservableArray = ko.observableArray(deptArray);
                self.dataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.summaryObservableArray, {idAttribute: 'id'}));
                self.isDisable = ko.observable(true);
//                self.dataSource=ko.observableArray([]);

                self.btnAdd = function () {
                    app.loading(true);
                    oj.Router.rootInstance.store('ADD');
                    oj.Router.rootInstance.go('lookup');
                };
                self.btnEdit = function () {
                    app.loading(true);
                    var element = document.getElementById('table');
                    var currentRow = element.currentRow;
                    var payload = {
                        clickType: 'Edit',
                        data: self.summaryObservableArray()[currentRow['rowIndex']]
                    };

                    oj.Router.rootInstance.store(payload);
                    oj.Router.rootInstance.go('lookup');

                };
                self.btnView = function () {
                    app.loading(true);
                    var element = document.getElementById('table');
                    var currentRow = element.currentRow;

                    var payload = {
                        clickType: 'view',
                        data: self.summaryObservableArray()[currentRow['rowIndex']]
                    };

                    oj.Router.rootInstance.store(payload);
                    oj.Router.rootInstance.go('lookup');

                };

                self.tableSelectionListener = function () {
                    self.isDisable(false);

                };

                self.getSummary = function () {

                    var getReportCbFn = function (data) {
                        if (data.length !== 0) {
                            $.each(data, function (index) {

                                self.summaryObservableArray.push({
                                    id: [data[index].id],
                                    name: [data[index].name],
                                    code: [data[index].code],
                                    arabicVal: [data[index].valueArabic],
                                    englishVal: [data[index].valueEnglish]

                                });
                            });
                        } else {
                            $.notify(self.checkResultSearch(), "error");
                        }
                    };
                    var failCbFn = function () {
                        $.notify(self.checkResultSearch(), "error");
                    };
                    var serviceName = commonUtil.getLookup + "/" + self.searchval();
                    services.getAllSummary(serviceName).then(getReportCbFn, failCbFn);
                };


                self.search = function () {
                    self.summaryObservableArray([]);
                    self.getSummary();
                };
                self.btndelete = function () {
                };
                self.btnRest = function () {
                    self.searchval("");
                };
                self.clear = function () {
                    self.searchval("");
                };
                self.backAction = function () {
                    oj.Router.rootInstance.go('setup');
                };


                self.handleAttached = function (info) {
                    app.loading(false);
                    //self.getSummary();
                };




                var getTranslation = oj.Translations.getTranslatedString;

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();

                    }

                });
                function initTranslations() {
                    self.addLbl(getTranslation("lookUp.add"));
                    self.editLbl(getTranslation("lookUp.edit"));
                    self.viewLbl(getTranslation("lookUp.view"));
                    self.lookupNameLbl(getTranslation("lookUp.name"));
                    self.lookupcode1Lbl(getTranslation("lookUp.code"));
                    self.lookupArabicVallLbl(getTranslation("lookUp.arabicVal"));
                    self.lookupEnglishValLbl(getTranslation("lookUp.englishVal"));
                    self.keyWordSearch(getTranslation("lookUp.keyWordSearch"));
                    self.Search(getTranslation("lookUp.Search"));
                    self.deleteLbl(getTranslation("lookUp.deleteLbl"));
                    self.RestLbl(getTranslation("lookUp.RestLbl"));
                    self.searchLable(getTranslation("lookUp.searchLable"));
                    self.checkResultSearch(getTranslation("common.checkResultSearch"));
                    self.backActionLbl(getTranslation("approvalScreen.backAction"));

                    self.columnArray([
                        {
                            "headerText": self.lookupNameLbl(), "field": "name"
                        },
                        {
                            "headerText": self.lookupcode1Lbl(), "field": "code"
                        },
                        {
                            "headerText": self.lookupArabicVallLbl(), "field": "arabicVal"
                        },
                        {
                            "headerText": self.lookupEnglishValLbl(), "field": "englishVal"
                        }
                    ]);
                }
                initTranslations();
            }
            return JobContentViewModel;
        });
