/**
 * 
 * @param {type} oj
 * @param {type} ko
 * @param {type} $
 * @param {type} app
 * @param {type} commonUtil
 * @param {type} services
 * @param {type} translatehelper
 * @returns {JobL#13.JobContentViewModel}
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojknockout', 'ojs/ojtrain', 'ojs/ojbutton',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker',
    'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup', 'ojs/ojcheckboxset', 'ojs/ojradioset',
    'ojs/ojlabel', 'ojs/ojgauge', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojmessage', 'jquery-calendar',
    'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin',
    'jquery-calendar-picker', 'ojs/ojvalidationgroup', 'ojs/ojmessages', 'ojs/ojmessage', 'ojs/ojarraydataprovider'],
        function (oj, ko, $, app, commonUtil, services) {
            /**
             * The view model for the main content view template
             */
            function JobContentViewModel() {
                var self = this;
                var operationType;
                self.disableSubmit = ko.observable(false);
                self.disableSubmitEdit = ko.observable(false);
                self.endDate = ko.observable();
                self.reportName = ko.observable();
                self.Parameter1 = ko.observable();
                self.Parameter2 = ko.observable();
                self.Parameter3 = ko.observable();
                self.Parameter4 = ko.observable();
                self.Parameter5 = ko.observable();
                self.createReport = ko.observable();
                self.reportNameVal = ko.observable("");
                self.Parameter1Val = ko.observable("");
                self.Parameter2Val = ko.observable("");
                self.Parameter3Val = ko.observable("");
                self.Parameter4Val = ko.observable("");
                self.Parameter5Val = ko.observable("");
                self.reportTypeVal = ko.observable("");
                self.reportTitleLabl = ko.observable();
                self.reportTypelbl = ko.observable();
                 self.reportTypeArr=ko.observableArray();
                 self.DepartmentArr=ko.observableArray();

                self.addJob = ko.observable();
                self.updateJob = ko.observable();
                self.viewJob = ko.observable();
                self.jobTitel = ko.observable();
                self.trainView = ko.observable();
                self.confirmMessage = ko.observable();
                self.addMessage = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.pervious = ko.observable();
                self.next = ko.observable();
                self.cancel = ko.observable();
                self.submit = ko.observable();
                self.create = ko.observable();
                self.review = ko.observable();
                self.ok = ko.observable();
                self.oprationMessage = ko.observable();
                self.addMassageLbl = ko.observable();
                self.editMassageLbl = ko.observable();
                self.placeholder = ko.observable();
                self.date1 = ko.observable(oj.IntlConverterUtils.dateToLocalIso(new Date()));

                self.serviceType = ko.observable();
                self.ministryJobModle = ko.observable();
                self.groupValid = ko.observable();
                self.jnVisibility = ko.observable();
                self.jnVisibility = ko.observable();
                self.trainVisibility = ko.observable();
                self.tracker = ko.observable();
                self.isDisabled = ko.observable(false);
                self.nextBtnVisible = ko.observable(true);
                self.addBtnVisible = ko.observable(false);
                self.columnArray = ko.observableArray();
                self.isRequired = ko.observable(true);
                self.stepArray = ko.observableArray([]);
                self.messages = ko.observable();
                self.errormessage = ko.observable();
                self.errormessagecheckInDatabase = ko.observable();
                self.validationMessage = ko.observable();
                self.oprationMessageUpdate = ko.observable();
                self.reportNamelbl = ko.observable();
                self.Parameter1lbl = ko.observable();
                self.Parameter2lbl = ko.observable();
                self.Parameter3lbl = ko.observable();
                self.Parameter4lbl = ko.observable();
                self.Parameter5lbl = ko.observable();
                self.departmentLbl = ko.observable();
              
                

                self.currentStepValue = ko.observable('stp1');
                self.hidecancel = ko.observable(true);
                self.hidepervious = ko.observable(false);
                self.hidenext = ko.observable(true);
                self.hidesubmit = ko.observable(false);
                self.hideUpdatesubmit = ko.observable(false);
                self.messages = ko.observable();
               




               //define 
             
                self.personDetails = ko.observable(app.globalspecialistEMP());
               self.dateVal=ko.observable();
               self.empployeeNameVal=ko.observable();
               self.employeeNumberVal=ko.observable();
               self.GradeCandidateVal=ko.observable();
               self.CandidateDateVal=ko.observable();
               self.workSiteVal=ko.observable();
               self.radiotest1=ko.observable();
               self.totalCommitmentAnddiscipline=ko.observable();
               self.appearanceAndbehavior1=ko.observable();
               self.appearanceAndbehavior2=ko.observable();
               self.appearanceAndbehavior3=ko.observable();
               self.appearanceAndbehavior4=ko.observable();
               self.appearanceAndbehavior5=ko.observable();
               self.InteractingWithothers1=ko.observable();
               self.InteractingWithothers2=ko.observable();
               self.InteractingWithothers3=ko.observable();
               self.InteractingWithothers4=ko.observable();
               self.InteractingWithothers5=ko.observable();
               self.qualityOfcommunication1=ko.observable();
               self.qualityOfcommunication2=ko.observable();
               self.qualityOfcommunication3=ko.observable();
               self.qualityOfcommunication4=ko.observable();
               self.qualityOfcommunication5=ko.observable();
               self.DirectManagerVal=ko.observable();
               self.firstKeyType=ko.observable();
               self.finalDecisionVal=ko.observable();
               self.elementLbl=ko.observable();
               self.LetterArr=ko.observable();
               self.letterNameVal=ko.observable();
               self.pageMode=ko.observable();
               self.pageMode('ADD'); 
               self.validationModle = {
                    radioCommitmentAnddisciplineq1: ko.observable(),
                    radioCommitmentAnddisciplineq2: ko.observable(),
                    radioCommitmentAnddisciplineq3: ko.observable(),
                    radioCommitmentAnddisciplineq4: ko.observable(),
                    radioCommitmentAnddisciplineq5: ko.observable(),
                    appearanceAndbehavior1: ko.observable(),
                    appearanceAndbehavior2: ko.observable(),
                    appearanceAndbehavior3: ko.observable(),
                    appearanceAndbehavior4: ko.observable(),
                    appearanceAndbehavior5: ko.observable(),
                    InteractingWithothers1: ko.observable(),
                    InteractingWithothers2: ko.observable(),
                    InteractingWithothers3: ko.observable(),
                    InteractingWithothers4: ko.observable(),
                    InteractingWithothers5: ko.observable(),
                    qualityOfcommunication1: ko.observable(),
                    qualityOfcommunication2: ko.observable(),
                    qualityOfcommunication3: ko.observable(),
                    qualityOfcommunication4: ko.observable(),
                    qualityOfcommunication5: ko.observable(),
                    DirectManagerVal: ko.observable(),
                    finalDecisionVal: ko.observable(),
                    departmentVal: ko.observable(),
                    url:ko.observable()
                   
                };
               self.RadioArr1 = ko.observableArray([
                    {"value": '1'},
                    {"value": '2'},
                    {"value": '3'},
                    {"value": '4'},
                    {"value": '5'}
                ]);
                self.RadioArr2 = ko.observableArray([
                    {"value": '6'},
                    {"value": '7'},
                    {"value": '8'},
                    {"value": '9'},
                    {"value": '10'}
                ]);
                
               var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1; //January is 0!
                var yyyy = today.getFullYear();

                if (dd < 10) {
                    dd = '0' + dd;
                }

                if (mm < 10) {
                    mm = '0' + mm;
                }

                today = dd + '-' + mm + '-' + yyyy;
                
               self.currentDate=ko.observable();
               self.currentDate(today); 



                self.currentStepValueText = function () {


                };
                self.previousStep = function () {
                  self.isDisabled(false);
                  self.hidenext(true);
                  self.hidesubmit(false);
                  self.hidepervious(false);
                  self.hideUpdatesubmit(false);
                  self.currentStepValue('stp1');

                };

                self.nextStep = function () {
                    var tracker = document.getElementById("tracker");
                                if (tracker.valid === "valid") {
                                self.hidecancel(true);
                                self.hidepervious(true);
                                self.hidenext(false);
                                self.isDisabled(true);
                                $(".disable-element").prop('disabled', false);
                                 self.currentStepValue('stp2');
                            if(self.serviceType()==='ADD'){
                                self.hidesubmit(true);
                                }else if(self.serviceType()==='Update'){
                                
                                self.hideUpdatesubmit(true);
                            }
                        
                        
                        
                        
                                }else {
                        tracker.showMessages();

                    }
                };


                self.disableInput = function () {
                    self.hidecancel(false);
                    self.hidepervious(true);
                    self.hidenext(false);

                    $(".disable-element").prop('disabled', true);
                    self.currentStepValue('stp2');
                };

                self.cancelAction = function () {

                    oj.Router.rootInstance.go('ProbationPerformanceEvaluationSumary');

                };

            
                self.submitButton = function () {
                    document.querySelector("#yesNoDialog").open();

                };
                self.commitRecordupdate = function () {
                  
                      var payload={                        
                        
                        "commitmentanddisciplineq1": self.validationModle.radioCommitmentAnddisciplineq1(),
                        "commitmentanddisciplineq2": self.validationModle.radioCommitmentAnddisciplineq2(),
                        "commitmentanddisciplineq3": self.validationModle.radioCommitmentAnddisciplineq3(),
                        "commitmentanddisciplineq4": self.validationModle.radioCommitmentAnddisciplineq4(),
                        "commitmentanddisciplineq5": self.validationModle.radioCommitmentAnddisciplineq5(),
                        "totalcommitmentanddisciplineq":parseInt(self.validationModle.radioCommitmentAnddisciplineq1())+parseInt(self.validationModle.radioCommitmentAnddisciplineq2())+parseInt(self.validationModle.radioCommitmentAnddisciplineq3())+parseInt(self.validationModle.radioCommitmentAnddisciplineq4())+parseInt(self.validationModle.radioCommitmentAnddisciplineq5()),
                        "appearanceandbehavior1":self.validationModle.appearanceAndbehavior1(),
                        "appearanceandbehavior2":self.validationModle.appearanceAndbehavior2(),
                        "appearanceandbehavior3":self.validationModle.appearanceAndbehavior3(),
                        "appearanceandbehavior4":self.validationModle.appearanceAndbehavior4(),
                        "appearanceandbehavior5":self.validationModle.appearanceAndbehavior5(),
                        "totalappearanceandbehavior":parseInt(self.validationModle.appearanceAndbehavior1())+parseInt(self.validationModle.appearanceAndbehavior2())+parseInt(self.validationModle.appearanceAndbehavior3())+parseInt(self.validationModle.appearanceAndbehavior4())+parseInt(self.validationModle.appearanceAndbehavior5()),
                        "interactionwithothers1": self.validationModle.InteractingWithothers1(),
                        "interactionwithothers2":self.validationModle.InteractingWithothers2() ,
                        "interactionwithothers3":self.validationModle.InteractingWithothers3() ,
                        "interactionwithothers4":self.validationModle.InteractingWithothers4() ,
                        "interactionwithothers5":self.validationModle.InteractingWithothers5() ,
                        "totalinteractionwithothers":parseInt(self.validationModle.InteractingWithothers1())+parseInt(self.validationModle.InteractingWithothers2())+parseInt(self.validationModle.InteractingWithothers3())+parseInt(self.validationModle.InteractingWithothers4())+parseInt(self.validationModle.InteractingWithothers5()),
                         "qualityofcommunication1":self.validationModle.qualityOfcommunication1(),
                         "qualityofcommunication2":self.validationModle.qualityOfcommunication2(),
                         "qualityofcommunication3":self.validationModle.qualityOfcommunication3(),
                         "qualityofcommunication4":self.validationModle.qualityOfcommunication4(),
                         "qualityofcommunication5":self.validationModle.qualityOfcommunication5(),
                         "finalEvaluationSummary":self.validationModle.DirectManagerVal(),
                         "finaldecision":self.validationModle.finalDecisionVal(),   
                         "department":self.validationModle.departmentVal(),
                         "totalqualityofcommunication":parseInt(self.validationModle.qualityOfcommunication1())+parseInt(self.validationModle.qualityOfcommunication2())+parseInt(self.validationModle.qualityOfcommunication3())+parseInt(self.validationModle.qualityOfcommunication4())+parseInt(self.validationModle.qualityOfcommunication5()),
                         "employeename":app.globalspecialistEMP().displayName,
                         "jobnumber":app.globalspecialistEMP().personNumber,
                         "location":app.globalspecialistEMP().employeeLocation,
                         "hiredate":app.globalspecialistEMP().hireDate,
                         "jobname":app.globalspecialistEMP().jobName,
                         "gradecandidate":app.globalspecialistEMP().grade,
                         "id":self.retrieve.id
                      
                    };
                   
                    
                    
                    var createpropationAndEvaluationCbFn=function(data){
                        
                        document.querySelector("#yesNoDialogUpdate").close();
                        oj.Router.rootInstance.go('ProbationPerformanceEvaluationSumary');
                        
                        
                    };
                   
                      services.addGeneric(commonUtil.UpdateprobationAndEvaluation , JSON.stringify(payload)).then(createpropationAndEvaluationCbFn, app.failCbFn);
                   
                };
                self.cancelButtonupdate = function () {
                    document.querySelector("#yesNoDialogUpdate").close();

                };
                self.submitUpdateButton = function () {
                    document.querySelector("#yesNoDialogUpdate").open();

                };


                self.restInput = function () {
                    self.reportNameVal("");
                    self.Parameter1Val("");
                    self.Parameter2Val("");
                    self.Parameter3Val("");
                    self.Parameter4Val("");
                    self.Parameter5Val("");
                };

                self.commitRecord = function () {
                  
                    var payload={                        
                        
                        "commitmentanddisciplineq1": self.validationModle.radioCommitmentAnddisciplineq1(),
                        "commitmentanddisciplineq2": self.validationModle.radioCommitmentAnddisciplineq2(),
                        "commitmentanddisciplineq3": self.validationModle.radioCommitmentAnddisciplineq3(),
                        "commitmentanddisciplineq4": self.validationModle.radioCommitmentAnddisciplineq4(),
                        "commitmentanddisciplineq5": self.validationModle.radioCommitmentAnddisciplineq5(),
                        "totalcommitmentanddisciplineq":parseInt(self.validationModle.radioCommitmentAnddisciplineq1())+parseInt(self.validationModle.radioCommitmentAnddisciplineq2())+parseInt(self.validationModle.radioCommitmentAnddisciplineq3())+parseInt(self.validationModle.radioCommitmentAnddisciplineq4())+parseInt(self.validationModle.radioCommitmentAnddisciplineq5()),
                        "appearanceandbehavior1":self.validationModle.appearanceAndbehavior1(),
                        "appearanceandbehavior2":self.validationModle.appearanceAndbehavior2(),
                        "appearanceandbehavior3":self.validationModle.appearanceAndbehavior3(),
                        "appearanceandbehavior4":self.validationModle.appearanceAndbehavior4(),
                        "appearanceandbehavior5":self.validationModle.appearanceAndbehavior5(),
                        "totalappearanceandbehavior":parseInt(self.validationModle.appearanceAndbehavior1())+parseInt(self.validationModle.appearanceAndbehavior2())+parseInt(self.validationModle.appearanceAndbehavior3())+parseInt(self.validationModle.appearanceAndbehavior4())+parseInt(self.validationModle.appearanceAndbehavior5()),
                        "interactionwithothers1": self.validationModle.InteractingWithothers1(),
                        "interactionwithothers2":self.validationModle.InteractingWithothers2() ,
                        "interactionwithothers3":self.validationModle.InteractingWithothers3() ,
                        "interactionwithothers4":self.validationModle.InteractingWithothers4() ,
                        "interactionwithothers5":self.validationModle.InteractingWithothers5() ,
                        "totalinteractionwithothers":parseInt(self.validationModle.InteractingWithothers1())+parseInt(self.validationModle.InteractingWithothers2())+parseInt(self.validationModle.InteractingWithothers3())+parseInt(self.validationModle.InteractingWithothers4())+parseInt(self.validationModle.InteractingWithothers5()),
                         "qualityofcommunication1":self.validationModle.qualityOfcommunication1(),
                         "qualityofcommunication2":self.validationModle.qualityOfcommunication2(),
                         "qualityofcommunication3":self.validationModle.qualityOfcommunication3(),
                         "qualityofcommunication4":self.validationModle.qualityOfcommunication4(),
                         "qualityofcommunication5":self.validationModle.qualityOfcommunication5(),
                         "finaldecision":self.validationModle.finalDecisionVal(),
                         "department":self.validationModle.departmentVal(),
                         "totalqualityofcommunication":parseInt(self.validationModle.qualityOfcommunication1())+parseInt(self.validationModle.qualityOfcommunication2())+parseInt(self.validationModle.qualityOfcommunication3())+parseInt(self.validationModle.qualityOfcommunication4())+parseInt(self.validationModle.qualityOfcommunication5()),
                         "finalEvaluationSummary":self.validationModle.DirectManagerVal(),
                         "status":"Pending Approval",
                         "employeename":app.globalspecialistEMP().displayName,
                         "jobnumber":app.globalspecialistEMP().personNumber,
                         "location":app.globalspecialistEMP().employeeLocation,
                         "hiredate":app.globalspecialistEMP().hireDate,
                         "jobname":app.globalspecialistEMP().jobName,
                         "gradecandidate":app.globalspecialistEMP().grade,                        
                         "url":self.personDetails().employeeURL["XXX_HR_PROBATION_PERFORMANCE"]
                         
                      
                    };
                   
                    
                    
                    var createpropationAndEvaluationCbFn=function(data){
                       
                        ApprovalOprationServiceRecord();
                        document.querySelector("#yesNoDialog").close();
                        oj.Router.rootInstance.go('ProbationPerformanceEvaluationSumary');
                        
                        
                    };
                   
                      services.addGeneric(commonUtil.addprobationAndEvaluation , JSON.stringify(payload)).then(createpropationAndEvaluationCbFn, app.failCbFn);
                };



          self.approvlModel = {};
          
       function ApprovalOprationServiceRecord() {
           //self.approvalSetupModel.inputValue(JSON.stringify(self.inputValues()));
           console.log("Noor ");
           self.approvlModel.type = 'XXX_HR_PROBATION_PERFORMANCE';
           self.approvlModel.personNumber = app.globalspecialistEMP().personNumber;
           self.approvlModel.personName = app.globalspecialistEMP().displayName;          
           self.approvlModel.presonId = app.globalspecialistEMP().personId;
           self.approvlModel.managerId = app.globalspecialistEMP().managerId;
           self.approvlModel.managerName = app.globalspecialistEMP().managerName;//
           self.approvlModel.managerOfManager = app.globalspecialistEMP().managerOfManager;
           self.approvlModel.managerOfMnagerName = app.globalspecialistEMP().managerOfMnagerName;//
           self.approvlModel.created_by = app.globalspecialistEMP().personId;
           self.approvlModel.creation_date =app.globalspecialistEMP().personId;
           var jsonData = ko.toJSON(self.approvlModel);
               var getValidGradeCBF = function (data) {
                        
               };

               services.addGeneric("selfService/"+self.pageMode(), jsonData).then(getValidGradeCBF, self.failCbFn);
           }

                
                
                  self.getSummary = function () {

                    var getReportCbFn = function (data) {
                        if (data.length !== 0)
                        {

                            $.each(data, function (index) {

                                self.summaryObservableArray.push({
                                    id: [data[index].id],
                                    report_Name: [data[index].report_Name],
                                    parameter1: [data[index].parameter1],
                                    parameter2: [data[index].parameter2],
                                    parameter3: [data[index].parameter3],
                                    parameter4: [data[index].parameter4],
                                    parameter5: [data[index].parameter5],
                                    reportTypeVal: [data[index].reportTypeVal]

                                });
                            });
                        } else {
                            $.notify(self.checkResultSearch(), "error");
                        }



                    };
                    var failCbFn = function () {
                        $.notify(self.checkResultSearch(), "error");
                    };
                    var serviceName = commonUtil.getAllSummary;
                    services.getAllSummary(serviceName).then(getReportCbFn, failCbFn);
                };



//////////////////////////////


                self.reportName = ko.observable();
                self.search = ko.observable();
                self.clear = ko.observable();


                self.reportSearch = {
                    reportname: ko.observable()

                };
                var el = document.getElementById("searchName");


                self.searchPerson = function () {

                    if (!self.reportSearch.reportname()) {

                        self.getSummary();
                    } else {
                        self.summaryObservableArray([]);
                        self.searchReportName();

                    }
                };

                self.reset = function () {
                    clearContent();
                };
                function clearContent() {
                    self.reportSearch.reportname("");
                    self.summaryObservableArray([]);
                }

                self.checkResultSearch = ko.observable();
                self.getAllDepartment = function () {
                    var departmentCbFn = function (data) {
                  
                        $.each(data, function (index) {

                                self.DepartmentArr.push({
                                    
                                    value: data[index].DEPARTMENT_NAME,
                                    label: data[index].DEPARTMENT_NAME
                                  

                                });
                            });
                    };
                   
                   
                    
                    services.getGenericDepartment().then(departmentCbFn, app.failCbFn);
                };

                self.cancelButton = function () {

                    document.querySelector("#yesNoDialog").close();
                };

                self.commitDraft = function (data, event) {
                    return true;
                };
                self.stopSelectListener = function () {
                    var tracker = document.getElementById("tracker");

                    if (tracker.valid !== "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        event.preventDefault();
                        return;
                    }

                };

                self.handleAttached = function (info) {
                    app.loading(false);
                    
                   //console.log(app.globalspecialistEMP());
                       self.getAllDepartment();
                    
                     self.retrieve = JSON.parse(sessionStorage.getItem("StoreObject"));
                      sessionStorage.removeItem("StoreObject");
                    self.personDetails = ko.observable(app.globalspecialistEMP());
                    console.log(self.personDetails());
                    if (self.retrieve.type === 'ADD') {

                        self.serviceType('ADD');
                        self.hidecancel(true);
                        self.hidepervious(false);
                        self.hidenext(true);
                        self.hidesubmit(false);
                        $(".disable-element").prop('disabled', false);
                        self.currentStepValue('stp1');
                        self.currentStepValue('stp1');


                    } else if (self.retrieve.type === 'View')
                    {
                    
                        var department=self.retrieve.department;
                         self.validationModle = {
                    radioCommitmentAnddisciplineq1: ko.observable(self.retrieve.commitmentanddisciplineq1),
                    
                    radioCommitmentAnddisciplineq2: ko.observable(self.retrieve.commitmentanddisciplineq2),
                    radioCommitmentAnddisciplineq3: ko.observable(self.retrieve.commitmentanddisciplineq3),
                    radioCommitmentAnddisciplineq4: ko.observable(self.retrieve.commitmentanddisciplineq4),
                    radioCommitmentAnddisciplineq5: ko.observable(self.retrieve.commitmentanddisciplineq5),
                    appearanceAndbehavior1: ko.observable(self.retrieve.appearanceandbehavior1),
                    appearanceAndbehavior2: ko.observable(self.retrieve.appearanceandbehavior2),
                    appearanceAndbehavior3: ko.observable(self.retrieve.appearanceandbehavior3),
                    appearanceAndbehavior4: ko.observable(self.retrieve.appearanceandbehavior4),
                    appearanceAndbehavior5: ko.observable(self.retrieve.appearanceandbehavior5),
                    InteractingWithothers1: ko.observable(self.retrieve.interactionwithothers1),
                    InteractingWithothers2: ko.observable(self.retrieve.interactionwithothers2),
                    InteractingWithothers3: ko.observable(self.retrieve.interactionwithothers3),
                    InteractingWithothers4: ko.observable(self.retrieve.interactionwithothers4),
                    InteractingWithothers5: ko.observable(self.retrieve.interactionwithothers5),
                    qualityOfcommunication1: ko.observable(self.retrieve.qualityofcommunication1),
                    qualityOfcommunication2: ko.observable(self.retrieve.qualityofcommunication2),
                    qualityOfcommunication3: ko.observable(self.retrieve.qualityofcommunication3),
                    qualityOfcommunication4: ko.observable(self.retrieve.qualityofcommunication4),
                    qualityOfcommunication5: ko.observable(self.retrieve.qualityofcommunication5),
                    DirectManagerVal: ko.observable(self.retrieve.finalEvaluationSummary),
                    finalDecisionVal: ko.observable(self.retrieve.finaldecision),
                    departmentVal: ko.observable(department)
                   
                     };
                       
                        self.hidecancel(true);
                        self.hidepervious(false);
                        self.hidenext(false);
                        self.hidesubmit(false);
                        self.isDisabled(true);
                        $(".disable-element").prop('disabled', true);
                        self.currentStepValue('stp2');

                    } else if ( self.retrieve.type === 'Update') {
                        self.serviceType('Update');
                        var department=self.retrieve.department;                        
                        
                        self.validationModle = {
                    radioCommitmentAnddisciplineq1: ko.observable(self.retrieve.commitmentanddisciplineq1),
                    radioCommitmentAnddisciplineq2: ko.observable(self.retrieve.commitmentanddisciplineq2),
                    radioCommitmentAnddisciplineq3: ko.observable(self.retrieve.commitmentanddisciplineq3),
                    radioCommitmentAnddisciplineq4: ko.observable(self.retrieve.commitmentanddisciplineq4),
                    radioCommitmentAnddisciplineq5: ko.observable(self.retrieve.commitmentanddisciplineq5),
                    appearanceAndbehavior1: ko.observable(self.retrieve.appearanceandbehavior1),
                    appearanceAndbehavior2: ko.observable(self.retrieve.appearanceandbehavior2),
                    appearanceAndbehavior3: ko.observable(self.retrieve.appearanceandbehavior3),
                    appearanceAndbehavior4: ko.observable(self.retrieve.appearanceandbehavior4),
                    appearanceAndbehavior5: ko.observable(self.retrieve.appearanceandbehavior5),
                    InteractingWithothers1: ko.observable(self.retrieve.interactionwithothers1),
                    InteractingWithothers2: ko.observable(self.retrieve.interactionwithothers2),
                    InteractingWithothers3: ko.observable(self.retrieve.interactionwithothers3),
                    InteractingWithothers4: ko.observable(self.retrieve.interactionwithothers4),
                    InteractingWithothers5: ko.observable(self.retrieve.interactionwithothers5),
                    qualityOfcommunication1: ko.observable(self.retrieve.qualityofcommunication1),
                    qualityOfcommunication2: ko.observable(self.retrieve.qualityofcommunication2),
                    qualityOfcommunication3: ko.observable(self.retrieve.qualityofcommunication3),
                    qualityOfcommunication4: ko.observable(self.retrieve.qualityofcommunication4),
                    qualityOfcommunication5: ko.observable(self.retrieve.qualityofcommunication5),
                    DirectManagerVal: ko.observable(self.retrieve.finalEvaluationSummary),
                    finalDecisionVal: ko.observable(self.retrieve.finaldecision),
                    departmentVal: ko.observable(department)
                   
                     };                    
                    }
                };

                   // translation 
                 self.Titlelbl=ko.observable();
                 self.dateLbl=ko.observable();
                 self.newEmployeeNameLbl=ko.observable();
                 self.EmployeeNumberLbl=ko.observable();
                 self.GradeCandidateLbl=ko.observable();
                 self.CandidateDateLbl=ko.observable();
                 self.workSiteLbl=ko.observable();
                 self.jobNameLbl=ko.observable();
                 self.noteLbl=ko.observable();
                 self.noteLblNumber1=ko.observable();
                 self.noteLblNumber2=ko.observable();
                 self.CommitmentAnddiscipline=ko.observable();
                 self.CommitmentAnddiscipline1=ko.observable();
                 self.CommitmentAnddiscipline2=ko.observable();
                 self.CommitmentAnddiscipline3=ko.observable();
                 self.CommitmentAnddiscipline4=ko.observable();
                 self.CommitmentAnddiscipline5=ko.observable();
                 self.radioCommitmentAnddisciplineq1=ko.observable();
                 self.radioCommitmentAnddisciplineq2=ko.observable();
                 self.radioCommitmentAnddisciplineq3=ko.observable();
                 self.radioCommitmentAnddisciplineq4=ko.observable();
                 self.radioCommitmentAnddisciplineq5=ko.observable();
                 self.appearanceAndbehaviorLbl=ko.observable();
                 self.appearanceAndbehaviorLbl1=ko.observable();
                 self.appearanceAndbehaviorLbl2=ko.observable();
                 self.appearanceAndbehaviorLbl3=ko.observable();
                 self.appearanceAndbehaviorLbl4=ko.observable();
                 self.appearanceAndbehaviorLbl5=ko.observable();
                 self.InteractingWithothersLbl=ko.observable();
                 self.InteractingWithothersLbl1=ko.observable();
                 self.InteractingWithothersLbl2=ko.observable();
                 self.InteractingWithothersLbl3=ko.observable();
                 self.InteractingWithothersLbl4=ko.observable();
                 self.InteractingWithothersLbl5=ko.observable();
                 self.qualityOfcommunicationLbl=ko.observable();
                 self.qualityOfcommunicationLbl1=ko.observable();
                 self.qualityOfcommunicationLbl2=ko.observable();
                 self.qualityOfcommunicationLbl3=ko.observable();
                 self.qualityOfcommunicationLbl4=ko.observable();
                 self.qualityOfcommunicationLbl5=ko.observable();
                 self.LetterNameLbl=ko.observable();
                 self.DirectManagerLbl=ko.observable();
                 self.FinalDecisionLbl=ko.observable();
                 self.confirmEmploymentLbl=ko.observable();
                 self.extendProbationPeriodLbl=ko.observable();
                 self.endEmploymentLbl=ko.observable();
                 self.FinalDecisionArr=ko.observableArray();
                var getTranslation = oj.Translations.getTranslatedString;
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });
                function initTranslations() {
                    
                    self.LetterArr(app.getPaaSLookup('LETTERNAME'));
                    self.FinalDecisionArr(app.getPaaSLookup('probationAndEvaluation'));
                    self.errormessagecheckInDatabase(getTranslation("report.errormessagecheckInDatabase"));
                    self.oprationMessage(getTranslation("report.oprationMessage"));
                    self.confirmMessage(getTranslation("report.confirmMessage"));
                    self.oprationMessageUpdate(getTranslation("report.oprationMessageUpdate"));
                    self.yes(getTranslation("report.yes"));
                    self.no(getTranslation("report.no"));
                    self.addJob(getTranslation("job.addJob"));
                    self.updateJob(getTranslation("job.updateJob"));
                    self.viewJob(getTranslation("job.viewJob"));
                    self.addMassageLbl(getTranslation("job.addMessage"));
                    self.editMassageLbl(getTranslation("job.editMessage"));
                    self.createReport(getTranslation("report.createReport"));
                    self.createReport(getTranslation("report.create"));
                    self.trainView(getTranslation("others.review"));

                    self.placeholder(getTranslation("labels.placeHolder"));
                    self.pervious(getTranslation("others.pervious"));
                    self.next(getTranslation("others.next"));
                    self.cancel(getTranslation("others.cancel"));

                    self.submit(getTranslation("others.submit"));

                    self.create(getTranslation("labels.create"));
                    self.review(getTranslation("others.review"));
                    self.ok(getTranslation("others.ok"));
                    
                    self.Titlelbl(getTranslation("probation.Title"));
                    self.dateLbl(getTranslation("probation.date"));
                    self.newEmployeeNameLbl(getTranslation("probation.newEmployeeName"));
                    self.EmployeeNumberLbl(getTranslation("probation.EmployeeNumber"));
                    self.GradeCandidateLbl(getTranslation("probation.GradeCandidate"));
                    self.CandidateDateLbl(getTranslation("probation.CandidateDate"));
                    self.workSiteLbl(getTranslation("probation.workSite"));
                    self.jobNameLbl(getTranslation("probation.jobName"));
                    self.noteLbl(getTranslation("probation.note"));
                    self.noteLblNumber1(getTranslation("probation.noteLblNumber1"));
                    self.noteLblNumber2(getTranslation("probation.noteLblNumber2"));
                    self.CommitmentAnddiscipline(getTranslation("probation.CommitmentAnddiscipline"));
                    self.CommitmentAnddiscipline1(getTranslation("probation.CommitmentAnddiscipline1"));
                    self.CommitmentAnddiscipline2(getTranslation("probation.CommitmentAnddiscipline2"));
                    self.CommitmentAnddiscipline3(getTranslation("probation.CommitmentAnddiscipline3"));
                    self.CommitmentAnddiscipline4(getTranslation("probation.CommitmentAnddiscipline4"));
                    self.CommitmentAnddiscipline5(getTranslation("probation.CommitmentAnddiscipline5"));
                    self.appearanceAndbehaviorLbl(getTranslation("probation.appearanceAndbehavior"));
                    self.appearanceAndbehaviorLbl1(getTranslation("probation.appearanceAndbehaviorLbl1"));
                    self.appearanceAndbehaviorLbl2(getTranslation("probation.appearanceAndbehaviorLbl2"));
                    self.appearanceAndbehaviorLbl3(getTranslation("probation.appearanceAndbehaviorLbl3"));
                    self.appearanceAndbehaviorLbl4(getTranslation("probation.appearanceAndbehaviorLbl4"));
                    self.appearanceAndbehaviorLbl5(getTranslation("probation.appearanceAndbehaviorLbl5"));
                    self.InteractingWithothersLbl(getTranslation("probation.InteractingWithothersLbl"));
                    self.InteractingWithothersLbl1(getTranslation("probation.InteractingWithothersLbl1"));
                    self.InteractingWithothersLbl2(getTranslation("probation.InteractingWithothersLbl2"));
                    self.InteractingWithothersLbl3(getTranslation("probation.InteractingWithothersLbl3"));
                    self.InteractingWithothersLbl4(getTranslation("probation.InteractingWithothersLbl4"));
                    self.InteractingWithothersLbl5(getTranslation("probation.InteractingWithothersLbl5"));
                    self.qualityOfcommunicationLbl(getTranslation("probation.qualityOfcommunicationLbl"));
                    self.qualityOfcommunicationLbl1(getTranslation("probation.qualityOfcommunicationLbl1"));
                    self.qualityOfcommunicationLbl2(getTranslation("probation.qualityOfcommunicationLbl2"));
                    self.qualityOfcommunicationLbl3(getTranslation("probation.qualityOfcommunicationLbl3"));
                    self.qualityOfcommunicationLbl4(getTranslation("probation.qualityOfcommunicationLbl4"));
                    self.qualityOfcommunicationLbl5(getTranslation("probation.qualityOfcommunicationLbl5"));
                    self.DirectManagerLbl(getTranslation("probation.DirectManagerLbl"));
                    self.LetterNameLbl(getTranslation("probation.LetterNameLbl"));
                    self.FinalDecisionLbl(getTranslation("probation.FinalDecision"));
                    self.confirmEmploymentLbl(getTranslation("probation.confirmEmployment"));
                    self.extendProbationPeriodLbl(getTranslation("probation.extendProbationPeriod"));
                    self.endEmploymentLbl(getTranslation("probation.endEmployment"));
                    self.elementLbl(getTranslation("probation.elementLbl"));
                    self.departmentLbl(getTranslation("common.department"));
                    
                    
                    
                    
                    
                    
                    


                    self.stepArray([{label: self.createReport(), id: 'stp1'},
                        {label: self.trainView(), id: 'stp2'}
                    ]);
                }
                initTranslations();
            }
            return JobContentViewModel;
        });
