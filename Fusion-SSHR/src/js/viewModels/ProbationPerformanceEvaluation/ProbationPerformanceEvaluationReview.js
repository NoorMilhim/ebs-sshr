/**
 * 
 * @param {type} oj
 * @param {type} ko
 * @param {type} $
 * @param {type} app
 * @param {type} commonUtil
 * @param {type} services
 * @param {type} translatehelper
 * @returns {JobL#13.JobContentViewModel}
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojknockout', 'ojs/ojtrain', 'ojs/ojbutton',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker',
    'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup', 'ojs/ojcheckboxset', 'ojs/ojradioset',
    'ojs/ojlabel', 'ojs/ojgauge', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojmessage', 'jquery-calendar',
    'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin',
    'jquery-calendar-picker', 'ojs/ojvalidationgroup', 'ojs/ojmessages', 'ojs/ojmessage', 'ojs/ojarraydataprovider'],
        function (oj, ko, $, app, commonUtil, services) {
            /**
             * The view model for the main content view template
             */
            function JobContentViewModel() {
                var self = this;
                var operationType;
                self.disableSubmit = ko.observable(false);
                self.disableSubmitEdit = ko.observable(false);
                self.endDate = ko.observable();
                self.reportName = ko.observable();
                self.Parameter1 = ko.observable();
                self.Parameter2 = ko.observable();
                self.Parameter3 = ko.observable();
                self.Parameter4 = ko.observable();
                self.Parameter5 = ko.observable();
                self.createReport = ko.observable();
                self.reportNameVal = ko.observable("");
                self.Parameter1Val = ko.observable("");
                self.Parameter2Val = ko.observable("");
                self.Parameter3Val = ko.observable("");
                self.Parameter4Val = ko.observable("");
                self.Parameter5Val = ko.observable("");
                self.reportTypeVal = ko.observable("");
                self.pageReviewTitle = ko.observable();
                self.reportTypelbl = ko.observable();
                 self.reportTypeArr=ko.observableArray();

                self.addJob = ko.observable();
                self.updateJob = ko.observable();
                self.viewJob = ko.observable();
                self.jobTitel = ko.observable();
                self.trainView = ko.observable();
                self.confirmMessage = ko.observable();
                self.addMessage = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.pervious = ko.observable();
                self.next = ko.observable();
                self.cancel = ko.observable();
                self.submit = ko.observable();
                self.create = ko.observable();
                self.review = ko.observable();
                self.ok = ko.observable();
                self.oprationMessage = ko.observable();
                self.addMassageLbl = ko.observable();
                self.editMassageLbl = ko.observable();
                self.placeholder = ko.observable();
                self.date1 = ko.observable(oj.IntlConverterUtils.dateToLocalIso(new Date()));

                self.serviceType = ko.observable();
                self.ministryJobModle = ko.observable();
                self.groupValid = ko.observable();
                self.jnVisibility = ko.observable();
                self.jnVisibility = ko.observable();
                self.trainVisibility = ko.observable();
                self.tracker = ko.observable();
                self.isDisabled = ko.observable(false);
                self.nextBtnVisible = ko.observable(true);
                self.addBtnVisible = ko.observable(false);
                self.columnArray = ko.observableArray();
                self.isRequired = ko.observable(true);
                self.stepArray = ko.observableArray([]);
                self.messages = ko.observable();
                self.errormessage = ko.observable();
                self.errormessagecheckInDatabase = ko.observable();
                self.validationMessage = ko.observable();
                self.oprationMessageUpdate = ko.observable();
                self.reportNamelbl = ko.observable();
                self.Parameter1lbl = ko.observable();
                self.Parameter2lbl = ko.observable();
                self.Parameter3lbl = ko.observable();
                self.Parameter4lbl = ko.observable();
                self.Parameter5lbl = ko.observable();
              self.workFlowApprovalId=ko.observable();
                

                self.currentStepValue = ko.observable('stp1');
                self.hidecancel = ko.observable(true);
                self.hidepervious = ko.observable(false);
                self.hidenext = ko.observable(true);
                self.hidesubmit = ko.observable(false);
                self.hideUpdatesubmit = ko.observable(false);
                self.messages = ko.observable();
              

               //define 
             
                self.personDetails = ko.observable(app.personDetails());
               self.dateVal=ko.observable();
               self.empployeeNameVal=ko.observable();
               self.employeeNumberVal=ko.observable();
               self.GradeCandidateVal=ko.observable();
               self.CandidateDateVal=ko.observable();
               self.workSiteVal=ko.observable();
               self.radiotest1=ko.observable();
               self.totalCommitmentAnddiscipline=ko.observable();
               self.appearanceAndbehavior1=ko.observable();
               self.appearanceAndbehavior2=ko.observable();
               self.appearanceAndbehavior3=ko.observable();
               self.appearanceAndbehavior4=ko.observable();
               self.appearanceAndbehavior5=ko.observable();
               self.InteractingWithothers1=ko.observable();
               self.InteractingWithothers2=ko.observable();
               self.InteractingWithothers3=ko.observable();
               self.InteractingWithothers4=ko.observable();
               self.InteractingWithothers5=ko.observable();
               self.qualityOfcommunication1=ko.observable();
               self.qualityOfcommunication2=ko.observable();
               self.qualityOfcommunication3=ko.observable();
               self.qualityOfcommunication4=ko.observable();
               self.qualityOfcommunication5=ko.observable();
               self.DirectManagerVal=ko.observable();
               self.firstKeyType=ko.observable();
               self.LetterArr=ko.observable();
               self.letterNameVal=ko.observable();
               self.pageMode=ko.observable();
               self.personNumber=ko.observable();
               self.pageMode('ADD'); 
                self.isLastApprover = ko.observable(false);
               self.validationModle = {
                    radioCommitmentAnddisciplineq1: ko.observable(),
                    radioCommitmentAnddisciplineq2: ko.observable(),
                    radioCommitmentAnddisciplineq3: ko.observable(),
                    radioCommitmentAnddisciplineq4: ko.observable(),
                    radioCommitmentAnddisciplineq5: ko.observable(),
                    appearanceAndbehavior1: ko.observable(),
                    appearanceAndbehavior2: ko.observable(),
                    appearanceAndbehavior3: ko.observable(),
                    appearanceAndbehavior4: ko.observable(),
                    appearanceAndbehavior5: ko.observable(),
                    InteractingWithothers1: ko.observable(),
                    InteractingWithothers2: ko.observable(),
                    InteractingWithothers3: ko.observable(),
                    InteractingWithothers4: ko.observable(),
                    InteractingWithothers5: ko.observable(),
                    qualityOfcommunication1: ko.observable(),
                    qualityOfcommunication2: ko.observable(),
                    qualityOfcommunication3: ko.observable(),
                    qualityOfcommunication4: ko.observable(),
                    qualityOfcommunication5: ko.observable(),
                    letterNameVal: ko.observable(),
                    DirectManagerVal: ko.observable(),
                    finalDecisionVal: ko.observable(),
                    url:ko.observable()
                    
                   
                };
                self.FinalDecisionArr=ko.observableArray();
               self.RadioArr1 = ko.observableArray([
                    {"value": '1'},
                    {"value": '2'},
                    {"value": '3'},
                    {"value": '4'},
                    {"value": '5'}
                ]);
                self.RadioArr2 = ko.observableArray([
                    {"value": '6'},
                    {"value": '7'},
                    {"value": '8'},
                    {"value": '9'},
                    {"value": '10'}
                ]);
               var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1; //January is 0!
                var yyyy = today.getFullYear();

                if (dd < 10) {
                    dd = '0' + dd;
                }

                if (mm < 10) {
                    mm = '0' + mm;
                }

                today = dd + '-' + mm + '-' + yyyy;
              var   todayreverse= yyyy+'-'+mm+'-'+dd;
               self.currentDate=ko.observable();
               self.currentDate(today); 

         
         self.cancelRewaardRequst=function(){
              oj.Router.rootInstance.go('notificationScreen');
         };
    

               self.submitButton = function () {
                    document.querySelector("#yesNoDialog").open();
                };
                 self.cancelButton = function () {
                    document.querySelector("#yesNoDialog").close();
                };
                
                self.updateStatusAfterAppoval = function () {
                    var payload = {
                        "status": "Approval",
                        "id": self.retrieve.TRS_ID
                    };
                    var rejectedActionCBFN = function (data) {

                    };


                    services.addGeneric("ProbationPerformanceEvaluation/UpdateStatus/", payload).then(rejectedActionCBFN, app.failCbFn)
                }; 
                self.workFlowApproval=function(){
                    var headers = {
                        "MSG_TITLE": self.retrieve.MSG_TITLE,
                        "MSG_BODY": self.retrieve.MSG_BODY,
                        "TRS_ID": self.retrieve.TRS_ID,
                        "PERSON_ID": app.personDetails().personId,
                        "RESPONSE_CODE": "Approval",
                        "ssType": self.retrieve.SELF_TYPE,
                        "PERSON_NAME":self.retrieve.personName,
                        "rejectReason":" ",
                         "workflowId":self.workFlowApprovalId()
                    };

                    var approvalActionCBFN = function (data) {
                       
                    };
                 
                    services.workflowAction("workflowApproval/", headers).then(approvalActionCBFN, app.failCbFn)
                };
              self.commitRecord = function () {
                   self.approveRequst();
//                  var submitEFFCBFN=function (data){
////                     self.workFlowApproval();
//                     self.updateStatusAfterAppoval();
//                     document.querySelector("#yesNoDialog").close();
//                     oj.Router.rootInstance.go('notificationScreen');
//                    };
//                  
//                     var payload={
//                      url:self.personDetails().employeeURL["XXX_HR_PROBATION_PERFORMANCE"] ,
//                      effectiveDate: todayreverse,
//                      employeeNumber:app.personDetails().personNumber,
//                      finalDecision:self.validationModle.finalDecisionVal()
//                      
//                     };
//                     console.log(payload);
//                services.addGeneric("eff/" + "ADD",  JSON.stringify(payload)).then(submitEFFCBFN, app.failCbFn); 
                };


    //approval 
              self.approveRequst = function () {
                 
                    var headers = {
                        "MSG_TITLE": self.retrieve.MSG_TITLE,
                        "MSG_BODY": self.retrieve.MSG_BODY,
                        "TRS_ID": self.retrieve.TRS_ID,
                        "PERSON_ID": self.personDetails().personId,
                        "RESPONSE_CODE": "APPROVED",
                        "ssType": self.retrieve.SELF_TYPE,
                        "PERSON_NAME":self.retrieve.personName,
                        "rejectReason":" ",
                        "workflowId":self.workFlowApprovalId()
                       
                    };
                    var approvalActionCBFN = function (data) {
                        //                     
                        if (self.isLastApprover()) {
                           
                            var payload = {
                                "status": "Approval",
                                "id": self.retrieve.TRS_ID
                            };
                            
                            var updateStatusCBFN =function (data){
                             
                            };
                            
                             services.addGeneric("ProbationPerformanceEvaluation/UpdateStatus/", payload).then(updateStatusCBFN, app.failCbFn);
                           
                           console.log(self.validationModle)
                            var modelXX = {
                                url: self.validationModle.url(),
                                effectiveDate: todayreverse,
                                employeeNumber: self.personNumber(),
                                finalDecision: self.validationModle.finalDecisionVal()

                            };
                            
                             var jsonData = {model:modelXX,TRS_ID:self.retrieve.TRS_ID,eitCode:'XXX_HR_PROBATION_PERFORMANCE'}
                            var supmitEFFCBFN = function (data) {
                                var Data = data;
                                // self.disableSubmit(false);
                                if (oj.Router.rootInstance._navHistory.length > 1) {
                                    oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                                } else {
                                    oj.Router.rootInstance.go('home');
                                }

                            };

                            services.addGeneric("eff/" + "ADD"+"/"+self.retrieve.person_number,JSON.stringify(jsonData)).then(supmitEFFCBFN, app.failCbFn);
                            
                       
                        } else {
                            if (oj.Router.rootInstance._navHistory.length > 1) {
                                oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                            } else {
                                oj.Router.rootInstance.go('home');
                            }
                        }
                        document.querySelector("#yesNoDialog").close();
                        // oj.Router.rootInstance.go('notificationScreen');
                    };
                    //services.workflowAction("workflowApproval/", headers).then(approvalActionCBFN, app.failCbFn)
                    services.workflowAction("workflowApproval/", headers).then( app.failCbFn,approvalActionCBFN)
                };




















            self.summaryObservableArray=ko.observableArray();       
                 self.getAllPropationAndEvaluation=function(id){
         
                    self.summaryObservableArray([]);

                    var getReportCbFn = function (data) {
                        
                        if (data.length !== 0)
                        {
                          
                      
                  self.validationModle = {
                    radioCommitmentAnddisciplineq1: ko.observable(data[0].commitmentanddisciplineq1),
                    letterNameVal: ko.observable(data[0].letterName),
                    radioCommitmentAnddisciplineq2: ko.observable(data[0].commitmentanddisciplineq2),
                    radioCommitmentAnddisciplineq3: ko.observable(data[0].commitmentanddisciplineq3),
                    radioCommitmentAnddisciplineq4: ko.observable(data[0].commitmentanddisciplineq4),
                    radioCommitmentAnddisciplineq5: ko.observable(data[0].commitmentanddisciplineq5),
                    appearanceAndbehavior1: ko.observable(data[0].appearanceandbehavior1),
                    appearanceAndbehavior2: ko.observable(data[0].appearanceandbehavior2),
                    appearanceAndbehavior3: ko.observable(data[0].appearanceandbehavior3),
                    appearanceAndbehavior4: ko.observable(data[0].appearanceandbehavior4),
                    appearanceAndbehavior5: ko.observable(data[0].appearanceandbehavior5),
                    InteractingWithothers1: ko.observable(data[0].interactionwithothers1),
                    InteractingWithothers2: ko.observable(data[0].interactionwithothers2),
                    InteractingWithothers3: ko.observable(data[0].interactionwithothers3),
                    InteractingWithothers4: ko.observable(data[0].interactionwithothers4),
                    InteractingWithothers5: ko.observable(data[0].interactionwithothers5),
                    qualityOfcommunication1: ko.observable(data[0].qualityofcommunication1),
                    qualityOfcommunication2: ko.observable(data[0].qualityofcommunication2),
                    qualityOfcommunication3: ko.observable(data[0].qualityofcommunication3),
                    qualityOfcommunication4: ko.observable(data[0].qualityofcommunication4),
                    qualityOfcommunication5: ko.observable(data[0].qualityofcommunication5),
                    finalDecisionVal: ko.observable(data[0].finaldecision),
                    DirectManagerVal: ko.observable(data[0].finalEvaluationSummary),
                    url: ko.observable(data[0].url)
                   
                     };
                            
                             
                           
                        } else {
//                            $.notify(self.checkResultSearch(), "error");
                        }



                    };
                    var failCbFn = function () {
//                        $.notify(self.checkResultSearch(), "error");
                    };
                    var serviceName = commonUtil.getprobationAndEvaluation+"/"+id;
                    services.getAllSummary(serviceName).then(getReportCbFn, failCbFn);
        
                };
            
            
            
             self.rejectButton = function () {
                    document.querySelector("#rejectDialog").open();
                };
                  self.rejectCancelButton = function () {
                    document.querySelector("#rejectDialog").close();
                };
            
             self.rejectRewaardRequst = function (data, event) {
                    var headers = {
                        "MSG_TITLE": self.retrieve.MSG_TITLE,
                        "MSG_BODY": self.retrieve.MSG_BODY,
                        "TRS_ID": self.retrieve.TRS_ID,
                        "PERSON_ID": app.personDetails().personId,
                        "RESPONSE_CODE": "REJECTED",
                        "ssType": self.retrieve.SELF_TYPE,
                        "PERSON_NAME":self.retrieve.personName,
                         "rejectReason":" ",
                         "workflowId":self.workFlowApprovalId()
                    };

                    var approvalActionCBFN = function (data) {
                        self.updateStatus();
                        document.querySelector("#rejectDialog").close();
                        oj.Router.rootInstance.go('notificationScreen');
                    };
                    var failCbFn=function(){
                        self.updateStatus();
                        document.querySelector("#rejectDialog").close();
                        oj.Router.rootInstance.go('notificationScreen');
                        
                    };
                    services.workflowAction("workflowApproval/", headers).then(approvalActionCBFN, failCbFn)

                };
            
         self.updateStatus=function(){
             var payload={
                  "status":"REJECTED",
                   "id":self.retrieve.TRS_ID
              };
             var rejectedActionCBFN=function(data){
                 
             };
             
             
             services.addGeneric("ProbationPerformanceEvaluation/UpdateStatus/", payload).then(rejectedActionCBFN, app.failCbFn)
         };      
         

          self.approvlModel = {};
          
                     var getPersonDetailsCbFn = function (data) {
                    
                      
                      
                     self.personDetails(data)
               
//                    self.personDetails.push({
//                                    assignmentName: data.assignmentName, assignmentProjectedEndDate: data.assignmentProjectedEndDate, assignmentStatus: data.assignmentStatus,
//                                    assignmentStatusTypeId: data.assignmentStatusTypeId, carAllowance: data.carAllowance, citizenshipLegislationCode: data.citizenshipLegislationCode, nationalId: (data.nationalId === 'null' || data.nationalId === null) ? '' : data.nationalId,
//                                    city: data.city, country: data.country, dateOfBirth: data.dateOfBirth, department: data.department, displayName: data.displayName, personNumber: data.personNumber,
//                                    email: data.email, employeeCertificate: data.employeeCertificate, employeeLocation: data.employeeLocation, fax: data.fax, faxExt: data.faxExt,
//                                    grade: data.grade, gradeId: data.gradeId, hireDate: data.hireDate, housingType: data.housingType,
//                                    jobId: data.jobId, jobName: data.jobName, legalEntityId: data.legalEntityId, managerId: data.managerId, managerName: data.managerName, managerOfManager: data.managerOfManager,
//                                    managerOfMnagerName: data.managerOfMnagerName, managerType: data.managerType, maritalStatus: data.maritalStatus, ministryJobForEmployee: data.ministryJobForEmployee, mobileAllowance: data.mobileAllowance, mobileNumber: data.mobileNumber,
//                                    organizationName: data.organizationName, overTimePaymentUrl: data.overTimePaymentUrl, overTimeURL: data.overTimeURL, peopleGroup: data.peopleGroup, personId: data.personId, picBase64: data.picBase64,
//                                    positionName: data.positionName, probationPeriodEndDate: data.probationPeriodEndDate, projectedStartDate: data.projectedStartDate, region: data.region, salaryAmount: data.salaryAmount, transportationAlowance: data.transportationAlowance,
//                                    workPhone: data.workPhone, workPhoneExt: data.workPhoneExt, employeeURL: data.employeeURL
//                     });
              
                };

                self.handleAttached = function (info) {
                    app.loading(false);
                    self.personDetails= ko.observable();
                    
                   
                    
                        
                    
                     self.retrieve = oj.Router.rootInstance.retrieve();
                     self.workFlowApprovalId(self.retrieve.id);
                      self.pageReviewTitle(" Employee name :" + self.retrieve.personName + " Employee Number: " + self.retrieve.person_number);
                      self.isLastApprover(app.isLastApprover(self.retrieve.TRS_ID,'XXX_HR_PROBATION_PERFORMANCE',false));
                      console.log(app.isLastApprover(self.retrieve.TRS_ID,'XXX_HR_PROBATION_PERFORMANCE',false));
                     self.getAllPropationAndEvaluation(self.retrieve.TRS_ID);
                     self.personNumber(self.retrieve.person_number);
                     services.searchEmployees('', '', self.personNumber(), '', '').then(getPersonDetailsCbFn, app.failCbFn);
                    
                       
                        self.hidecancel(true);
                        self.hidepervious(false);
                        self.hidenext(false);
                        self.hidesubmit(false);
                        self.isDisabled(true);
                        $(".disable-element").prop('disabled', true);
                        self.currentStepValue('stp2');


                };

                   // translation 
                 self.Titlelbl=ko.observable();
                 self.dateLbl=ko.observable();
                 self.newEmployeeNameLbl=ko.observable();
                 self.EmployeeNumberLbl=ko.observable();
                 self.GradeCandidateLbl=ko.observable();
                 self.CandidateDateLbl=ko.observable();
                 self.workSiteLbl=ko.observable();
                 self.jobNameLbl=ko.observable();
                 self.noteLbl=ko.observable();
                 self.noteLblNumber1=ko.observable();
                 self.noteLblNumber2=ko.observable();
                 self.CommitmentAnddiscipline=ko.observable();
                 self.CommitmentAnddiscipline1=ko.observable();
                 self.CommitmentAnddiscipline2=ko.observable();
                 self.CommitmentAnddiscipline3=ko.observable();
                 self.CommitmentAnddiscipline4=ko.observable();
                 self.CommitmentAnddiscipline5=ko.observable();
                 self.radioCommitmentAnddisciplineq1=ko.observable();
                 self.radioCommitmentAnddisciplineq2=ko.observable();
                 self.radioCommitmentAnddisciplineq3=ko.observable();
                 self.radioCommitmentAnddisciplineq4=ko.observable();
                 self.radioCommitmentAnddisciplineq5=ko.observable();
                 self.appearanceAndbehaviorLbl=ko.observable();
                 self.appearanceAndbehaviorLbl1=ko.observable();
                 self.appearanceAndbehaviorLbl2=ko.observable();
                 self.appearanceAndbehaviorLbl3=ko.observable();
                 self.appearanceAndbehaviorLbl4=ko.observable();
                 self.appearanceAndbehaviorLbl5=ko.observable();
                 self.InteractingWithothersLbl=ko.observable();
                 self.InteractingWithothersLbl1=ko.observable();
                 self.InteractingWithothersLbl2=ko.observable();
                 self.InteractingWithothersLbl3=ko.observable();
                 self.InteractingWithothersLbl4=ko.observable();
                 self.InteractingWithothersLbl5=ko.observable();
                 self.qualityOfcommunicationLbl=ko.observable();
                 self.qualityOfcommunicationLbl1=ko.observable();
                 self.qualityOfcommunicationLbl2=ko.observable();
                 self.qualityOfcommunicationLbl3=ko.observable();
                 self.qualityOfcommunicationLbl4=ko.observable();
                 self.qualityOfcommunicationLbl5=ko.observable();
                 self.LetterNameLbl=ko.observable();
                 self.DirectManagerLbl=ko.observable();
                 self.approve=ko.observable();
                 self.reject=ko.observable();
                 self.back=ko.observable();
                 self.rejectMessage = ko.observable();
                 self.confirmMessage = ko.observable();
                 self.FinalDecisionLbl = ko.observable();
                 self.elementLbl = ko.observable();

                var getTranslation = oj.Translations.getTranslatedString;
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });
                function initTranslations() {
                    self.FinalDecisionArr(app.getPaaSLookup('probationAndEvaluation'));
                    self.LetterArr(app.getPaaSLookup('LETTERNAME'));
                    self.errormessagecheckInDatabase(getTranslation("report.errormessagecheckInDatabase"));
                    self.oprationMessage(getTranslation("others.oprationMessage"));
                    self.confirmMessage(getTranslation("report.confirmMessage"));
                    self.oprationMessageUpdate(getTranslation("report.oprationMessageUpdate"));
                    self.yes(getTranslation("report.yes"));
                    self.no(getTranslation("report.no"));
                    self.addJob(getTranslation("job.addJob"));
                    self.updateJob(getTranslation("job.updateJob"));
                    self.viewJob(getTranslation("job.viewJob"));
                    self.addMassageLbl(getTranslation("job.addMessage"));
                    self.editMassageLbl(getTranslation("job.editMessage"));
                    self.createReport(getTranslation("report.createReport"));
                    self.createReport(getTranslation("report.create"));
                    self.trainView(getTranslation("others.review"));

                    self.placeholder(getTranslation("labels.placeHolder"));
                    self.pervious(getTranslation("others.pervious"));
                    self.next(getTranslation("others.next"));
                    self.cancel(getTranslation("others.cancel"));

                    self.submit(getTranslation("others.submit"));

                    self.create(getTranslation("labels.create"));
                    self.review(getTranslation("others.review"));
                    self.ok(getTranslation("others.ok"));
                    
                    self.Titlelbl(getTranslation("probation.Title"));
                    self.dateLbl(getTranslation("probation.date"));
                    self.newEmployeeNameLbl(getTranslation("probation.newEmployeeName"));
                    self.EmployeeNumberLbl(getTranslation("probation.EmployeeNumber"));
                    self.GradeCandidateLbl(getTranslation("probation.GradeCandidate"));
                    self.CandidateDateLbl(getTranslation("probation.CandidateDate"));
                    self.workSiteLbl(getTranslation("probation.workSite"));
                    self.jobNameLbl(getTranslation("probation.jobName"));
                    self.noteLbl(getTranslation("probation.note"));
                    self.noteLblNumber1(getTranslation("probation.noteLblNumber1"));
                    self.noteLblNumber2(getTranslation("probation.noteLblNumber2"));
                    self.CommitmentAnddiscipline(getTranslation("probation.CommitmentAnddiscipline"));
                    self.CommitmentAnddiscipline1(getTranslation("probation.CommitmentAnddiscipline1"));
                    self.CommitmentAnddiscipline2(getTranslation("probation.CommitmentAnddiscipline2"));
                    self.CommitmentAnddiscipline3(getTranslation("probation.CommitmentAnddiscipline3"));
                    self.CommitmentAnddiscipline4(getTranslation("probation.CommitmentAnddiscipline4"));
                    self.CommitmentAnddiscipline5(getTranslation("probation.CommitmentAnddiscipline5"));
                    self.appearanceAndbehaviorLbl(getTranslation("probation.appearanceAndbehavior"));
                    self.appearanceAndbehaviorLbl1(getTranslation("probation.appearanceAndbehaviorLbl1"));
                    self.appearanceAndbehaviorLbl2(getTranslation("probation.appearanceAndbehaviorLbl2"));
                    self.appearanceAndbehaviorLbl3(getTranslation("probation.appearanceAndbehaviorLbl3"));
                    self.appearanceAndbehaviorLbl4(getTranslation("probation.appearanceAndbehaviorLbl4"));
                    self.appearanceAndbehaviorLbl5(getTranslation("probation.appearanceAndbehaviorLbl5"));
                    self.InteractingWithothersLbl(getTranslation("probation.InteractingWithothersLbl"));
                    self.InteractingWithothersLbl1(getTranslation("probation.InteractingWithothersLbl1"));
                    self.InteractingWithothersLbl2(getTranslation("probation.InteractingWithothersLbl2"));
                    self.InteractingWithothersLbl3(getTranslation("probation.InteractingWithothersLbl3"));
                    self.InteractingWithothersLbl4(getTranslation("probation.InteractingWithothersLbl4"));
                    self.InteractingWithothersLbl5(getTranslation("probation.InteractingWithothersLbl5"));
                    self.qualityOfcommunicationLbl(getTranslation("probation.qualityOfcommunicationLbl"));
                    self.qualityOfcommunicationLbl1(getTranslation("probation.qualityOfcommunicationLbl1"));
                    self.qualityOfcommunicationLbl2(getTranslation("probation.qualityOfcommunicationLbl2"));
                    self.qualityOfcommunicationLbl3(getTranslation("probation.qualityOfcommunicationLbl3"));
                    self.qualityOfcommunicationLbl4(getTranslation("probation.qualityOfcommunicationLbl4"));
                    self.qualityOfcommunicationLbl5(getTranslation("probation.qualityOfcommunicationLbl5"));
                    self.DirectManagerLbl(getTranslation("probation.DirectManagerLbl"));
                    self.LetterNameLbl(getTranslation("probation.LetterNameLbl"));
                    self.FinalDecisionLbl(getTranslation("probation.FinalDecision"));
                    self.approve(getTranslation("others.approve"));
                    self.reject(getTranslation("others.reject"));
                    self.back(getTranslation("others.back"));
                    self.rejectMessage(getTranslation("others.rejectMSG"));
                    self.elementLbl(getTranslation("probation.elementLbl"));
                    
                    
                    
                    
                    
                    


                    self.stepArray([{label: self.createReport(), id: 'stp1'},
                        {label: self.trainView(), id: 'stp2'}
                    ]);
                }
                initTranslations();
            }
            return JobContentViewModel;
        });
