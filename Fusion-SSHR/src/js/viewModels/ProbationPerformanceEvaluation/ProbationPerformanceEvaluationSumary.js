/**
 * 
 * @param {type} oj
 * @param {type} ko
 * @param {type} $
 * @param {type} app
 * @param {type} commonUtil
 * @param {type} services
 * @param {type} translatehelper
 * @returns {JobL#13.JobContentViewModel}
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojknockout', 'ojs/ojtrain', 'ojs/ojbutton', 'ojs/ojarraydataprovider', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker', 'ojs/ojtable',
    'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup', 'ojs/ojcheckboxset', 'ojs/ojradioset',
    'ojs/ojlabel', 'ojs/ojgauge', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojmessage', 'jquery-calendar',
    'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin',
    'jquery-calendar-picker', 'ojs/ojvalidationgroup', 'ojs/ojarraydataprovider'],
        function (oj, ko, $, app, commonUtil, services) {
            /**
             * The view model for the main content view template
             */
            function JobContentViewModel() {
                var self = this;
                self.columnArray = ko.observableArray();
                self.addSummaryLbl = ko.observable();
                self.editSummaryLbl = ko.observable();
                self.viewSummaryLbl = ko.observable();
                self.reportNameLbl = ko.observable();
                self.parameter1Lbl = ko.observable();
                self.parameter2Lbl = ko.observable();
                self.parameter3Lbl = ko.observable();
                self.parameter4Lbl = ko.observable();
                self.parameter5Lbl = ko.observable();
                self.reportTypelbl = ko.observable();
                self.roleOptionType = ko.observableArray([]);
                self.positionName = ko.observable();
                var deptArray = [];
                var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
                self.summaryObservableArray = ko.observableArray(deptArray);
                self.dataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.summaryObservableArray, {idAttribute: 'id'}));
                self.isDisable = ko.observable(true);
                self.dataSource;
                self.checkResultSearch = ko.observable();
                self.positionName = ko.observable();
                // define 
                self.newEmployeeNameLbl = ko.observable();
                self.LetterNameLbl = ko.observable();
                self.jobNameLbl = ko.observable();
                self.totalcommitmentanddisciplineqbl = ko.observable();
                self.totalinteractionwithothersLbl = ko.observable();
                self.totalqualityofcommunicationLbl = ko.observable();
                self.totalappearanceandbehaviorlbl = ko.observable();
                self.statusLbl = ko.observable();
                self.AprovalListLbl = ko.observable();
                self.Approval_list = ko.observable();
                self.Approval_name = ko.observable();
                self.Approval_noti = ko.observable();
                self.Approval_status = ko.observable();
                self.Approval_date = ko.observable();
                self.ok = ko.observable();
                self.departmentLbl = ko.observable();
                self.totalfinaldecisionlbl = ko.observable();
                self.datacrad = ko.observableArray([]);

                self.btnAddSummary = function () {
                    app.loading(true);
                    var so={"type":'ADD'};
                     sessionStorage.setItem("StoreObject",JSON.stringify(so));
//                    oj.Router.rootInstance.store('ADD');
                    oj.Router.rootInstance.go('ProbationPerformanceEvaluationOperation');
                    self.closeDialog();
                };
                self.btnEditSummary = function () {
                    app.loading(true);
                    var element = document.getElementById('table');
                    var currentRow = element.currentRow;
                    self.summaryObservableArray()[currentRow['rowIndex']].type = "Update";
                      sessionStorage.setItem("StoreObject", JSON.stringify(self.summaryObservableArray()[currentRow['rowIndex']]));
//                    oj.Router.rootInstance.store();
                    oj.Router.rootInstance.go('ProbationPerformanceEvaluationOperation');
                };
                self.btnViewSummary = function () {
                    app.loading(true);
                    var element = document.getElementById('table');
                    var currentRow = element.currentRow;
                    self.summaryObservableArray()[currentRow['rowIndex']].type = "View";
                     sessionStorage.setItem("StoreObject", JSON.stringify(self.summaryObservableArray()[currentRow['rowIndex']]));
//                    oj.Router.rootInstance.store(self.summaryObservableArray()[currentRow['rowIndex']]);
                    oj.Router.rootInstance.go('ProbationPerformanceEvaluationOperation');
                };
                self.tableSelectionListener = function () {
                    self.isDisable(false);
                };
                self.getSummary = function () {
                    self.summaryObservableArray([]);
                    var getReportCbFn = function (data) {
                        if (data.length !== 0)
                        {
                            self.summaryObservableArray(data);
                        } else {
                            $.notify(self.checkResultSearch(), "error");
                        }
                    };
                    var failCbFn = function () {
                        $.notify(self.checkResultSearch(), "error");
                    };
                    var serviceName = commonUtil.getprobationAndEvaluationbypersonNumber + "/" + app.globalspecialistEMP().personNumber;
                    services.getAllSummary(serviceName).then(getReportCbFn, failCbFn);
                }

                //------------start work flow approval-------------------//
                self.btnApprovalList = function () {
                    self.rolrType = ko.observableArray([]);
                    self.notificationType = ko.observableArray([]);
                    self.responseCode = ko.observableArray([]);
                    self.responseDate = ko.observableArray([]);
                    self.approvalResponseArr = ko.observableArray([]);
//      
                    self.datacrad([]);
                    var getValidGradeCBF = function (data) {
                       // console.log(data);
                        if (data.length != 0) {
                            var aprrovalStatus = "PENDING";
                            for (var i = 0; i < data.length; i++) {
                                var notificationStatus, bodyCardStatus, cardStatus;

                                if (data[i].notificationType === "FYA") {
                                    notificationStatus = 'app-type-a';
                                } else {
                                    cardStatus = 'badge-secondary';
                                    notificationStatus = 'app-type-i';
                                    bodyCardStatus = 'app-crd-bdy-border-ntre';
                                }

                                if (!data[i].responseCode) {
                                    cardStatus = 'badge-warning';
                                    bodyCardStatus = 'app-crd-bdy-border-pen';
                                    data[i].responseDate = '';
                                } else if (data[i].responseCode === 'APPROVED') {
                                    cardStatus = 'badge-success';
                                    bodyCardStatus = 'app-crd-bdy-border-suc';
                                } else {

                                }


                                if (data[i].rolrType === "EMP") {
//                            data[i].rolrType= app.personDetails().displayName
                                    if (!data[i].responseCode) {
                                        self.datacrad.push(
                                                {personName: app.personDetails().displayName,
                                                    FYA: data[i].notificationType,
                                                    Approved: aprrovalStatus,
                                                    date: data[i].responseDate,
                                                    status: notificationStatus,
                                                    bodyStatus: bodyCardStatus,
                                                    appCardStatus: cardStatus
                                                },
                                                );
                                    } else {
                                        self.datacrad.push(
                                                {personName: data[i].personName,
                                                    FYA: data[i].notificationType,
                                                    Approved: data[i].responseCode,
                                                    date: data[i].responseDate,
                                                    status: notificationStatus,
                                                    bodyStatus: bodyCardStatus,
                                                    appCardStatus: cardStatus
                                                },
                                                );
                                    }

                                } else if (data[i].rolrType == "LINE_MANAGER") {
//                            data[i].rolrType= app.personDetails().managerName;
                                    if (!data[i].responseCode) {
                                        self.datacrad.push(
                                                {personName: data[i].lineManagerName,
                                                    FYA: data[i].notificationType,
                                                    Approved: aprrovalStatus,
                                                    date: data[i].responseDate,
                                                    status: notificationStatus,
                                                    bodyStatus: bodyCardStatus,
                                                    appCardStatus: cardStatus
                                                }, );
                                    } else {
                                        self.datacrad.push(
                                                {personName: data[i].lineManagerName,
                                                    FYA: data[i].notificationType,
                                                    Approved: data[i].responseCode,
                                                    date: data[i].responseDate,
                                                    status: notificationStatus,
                                                    bodyStatus: bodyCardStatus,
                                                    appCardStatus: cardStatus
                                                }, );
                                    }

                                } else if (data[i].rolrType == "LINE_MANAGER+1") {
//                               data[i].rolrType= app.personDetails().managerOfMnagerName;
                                    if (!data[i].responseCode) {
                                        self.datacrad.push(
                                                {personName: data[i].lineManagerName,
                                                    FYA: data[i].notificationType,
                                                    Approved: aprrovalStatus,
                                                    date: data[i].responseDate,
                                                    status: notificationStatus,
                                                    bodyStatus: bodyCardStatus,
                                                    appCardStatus: cardStatus
                                                }, );
                                    } else {
                                        self.datacrad.push(
                                                {personName: data[i].lineManagerName,
                                                    FYA: data[i].notificationType,
                                                    Approved: data[i].responseCode,
                                                    date: data[i].responseDate,
                                                    status: notificationStatus,
                                                    bodyStatus: bodyCardStatus,
                                                    appCardStatus: cardStatus
                                                }, );
                                    }

                                } else if (data[i].rolrType == "POSITION") {
//                               data[i].rolrType= app.personDetails().managerOfMnagerName;
                                    for (var j = 0; j < self.roleOptionType().length; j++) {
                                        if (self.roleOptionType()[j].value == data[i].roleId)
                                        {
                                            self.positionName(self.roleOptionType()[j].label);
                                        }
                                    }
                                    if (!data[i].responseCode) {
                                        self.datacrad.push(
                                                {personName: self.positionName(),
                                                    FYA: data[i].notificationType,
                                                    Approved: aprrovalStatus,
                                                    date: data[i].responseDate,
                                                    status: notificationStatus,
                                                    bodyStatus: bodyCardStatus,
                                                    appCardStatus: cardStatus
                                                }, );
                                    } else {
                                        self.datacrad.push(
                                                {personName: self.positionName(),
                                                    FYA: data[i].notificationType,
                                                    Approved: data[i].responseCode,
                                                    date: data[i].responseDate,
                                                    status: notificationStatus,
                                                    bodyStatus: bodyCardStatus,
                                                    appCardStatus: cardStatus
                                                }, );
                                    }

                                } else if (data[i].rolrType == "ROLES") {
//                               data[i].rolrType= app.personDetails().managerOfMnagerName;

                                    for (var j = 0; j < rootViewModel.rolesOption().length; j++) {

                                        if (rootViewModel.rolesOption()[j].value == data[i].roleId)
                                        {
                                            self.positionName(rootViewModel.rolesOption()[j].label);
                                        }
                                    }
                                    if (!data[i].responseCode) {
                                        self.datacrad.push(
                                                {personName: self.positionName(),
                                                    FYA: data[i].notificationType,
                                                    Approved: aprrovalStatus,
                                                    date: data[i].responseDate,
                                                    status: notificationStatus,
                                                    bodyStatus: bodyCardStatus,
                                                    appCardStatus: cardStatus
                                                }, );
                                    } else {
                                        self.datacrad.push(
                                                {personName: self.positionName(),
                                                    FYA: data[i].notificationType,
                                                    Approved: data[i].responseCode,
                                                    date: data[i].responseDate,
                                                    status: notificationStatus,
                                                    bodyStatus: bodyCardStatus,
                                                    appCardStatus: cardStatus
                                                }, );
                                    }

                                }


//                       self.notificationType.push(data[i].notificationType);
//                       self.responseCode.push(data[i].responseCode);
//                       self.responseDate.push(data[i].responseDate);  

                                //  data[i].approvalType = searchArray(data[i].approvalType, self.approvalType());
                                //  data[i].roleName = searchArray(data[i].roleName, self.roleName());

                            }
                        } else
                        {
                            self.datacrad.push({
                                personName: 'There is no approval list to this request',
                                FYA: '',
                                Approved: '',
                                date: '',
                                status: '',
                                bodyStatus: '',
                                appCardStatus: ''
                            });
                        }
//                    var Data = data;
//          
//                self.dataSourceTB2(new oj.ArrayTableDataSource(Data));

                    };
                    // ... leave firstName and lastName unchanged ...

                    //self.lbl = ko.observable(self.approvaltype());


                    var element = document.getElementById('table');
                    var currentRow = element.currentRow;
//                    console.log(self.summaryObservableArray()[currentRow['rowIndex']]);
                    var transactionId = self.summaryObservableArray()[currentRow['rowIndex']].id;
                    var eitcode = "XXX_HR_PROBATION_PERFORMANCE";
                    services.getGeneric("approvalList/approvaltype/" + eitcode + "/" + transactionId).then(getValidGradeCBF, app.failCbFn);
                    document.querySelector("#modalDialog1").open();
                };
                self.closeDialog = function () {
                    $("#modalDialog1").ojDialog("close");
                };
                //------------End work flow approval-------------------//
//                self.DepartmentArr=ko.observableArray();
//                self.getAllDepartment = function () {
//                    var departmentCbFn = function (data) {
//                      
//                        $.each(data, function (index) {
//
//                                self.DepartmentArr.push({
//                                    
//                                    value: data[index].organizationId,
//                                    label: data[index].organizationName
//                                  
//
//                                });
//                            });
//                            self.getSummary();
//                    };
//                   
//                   
//                    var service = "department/";
//                    services.getGeneric(service, {}).then(departmentCbFn, app.failCbFn);
//                };


                self.handleAttached = function (info) {



                    app.loading(false);
                    self.getSummary();
                    //self.searchReportName();
                    //self.reportSearch.reportname("");
                    self.globalLookupReport = ko.observableArray();
                    self.globalLookupReport(app.globalFuseModel());
                    for (var counter = 0; counter < self.globalLookupReport().length; counter++) {
                        if (self.globalLookupReport()[counter].LOOKUP_TYPE === "XXX_HR_PAAS_REPORTS") {
                        }
                    }
                };


                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });
                function initTranslations() {
                    self.addSummaryLbl(app.translate("report.addSummary"));
                    self.editSummaryLbl(app.translate("report.editSummary"));
                    self.viewSummaryLbl(app.translate("report.viewSummary"));
                    self.reportNameLbl(app.translate("report.reportName"));
                    self.parameter1Lbl(app.translate("report.Parameter1"));
                    self.parameter2Lbl(app.translate("report.Parameter2"));
                    self.parameter3Lbl(app.translate("report.Parameter3"));
                    self.parameter4Lbl(app.translate("report.Parameter4"));
                    self.parameter5Lbl(app.translate("report.Parameter5"));
                    self.reportTypelbl(app.translate("report.reportType"));
                    self.checkResultSearch(app.translate("common.checkResultSearch"));
                    //new 
                    self.newEmployeeNameLbl(app.translate("probation.newEmployeeName"));
                    self.LetterNameLbl(app.translate("probation.LetterNameLbl"));
                    self.jobNameLbl(app.translate("probation.jobName"));
                    self.totalcommitmentanddisciplineqbl(app.translate("probation.totalcommitmentanddiscipline"));
                    self.totalinteractionwithothersLbl(app.translate("probation.totalinteractionwithothers"));
                    self.totalqualityofcommunicationLbl(app.translate("probation.totalqualityofcommunication"));
                    self.totalappearanceandbehaviorlbl(app.translate("probation.totalappearanceandbehavior"));
                    self.statusLbl(app.translate("probation.Status"));
                    self.AprovalListLbl(app.translate("probation.AprovalListLbl"));
                    self.Approval_list(app.translate("labels.Approval_list"));
                    self.Approval_name(app.translate("labels.Approval_name"));
                    self.Approval_noti(app.translate("labels.Approval_noti"));
                    self.Approval_status(app.translate("labels.Approval_status"));
                    self.Approval_date(app.translate("labels.Approval_date"));
                    self.totalfinaldecisionlbl(app.translate("labels.totalfinaldecisionlbl"));
                    self.ok(app.translate("others.ok"));
                    self.departmentLbl(app.translate("common.department"));
                    self.columnArray([
                        {
                            "headerText": self.statusLbl(), "field": "status"
                        },
                        {
                            "headerText": self.newEmployeeNameLbl(), "field": "employeename"
                        },
                        {
                            "headerText": self.jobNameLbl(), "field": "jobname"
                        },
                        {
                            "headerText": self.departmentLbl(), "field": "department"
                        },
                        {
                            "headerText": self.totalcommitmentanddisciplineqbl(), "field": "totalcommitmentanddisciplineq"
                        },
                        {
                            "headerText": self.totalinteractionwithothersLbl(), "field": "totalinteractionwithothers"
                        },
                        {
                            "headerText": self.totalqualityofcommunicationLbl(), "field": "totalqualityofcommunication"
                        },
                        {
                            "headerText": self.totalappearanceandbehaviorlbl(), "field": "totalappearanceandbehavior"
                        },
                        {
                            "headerText": self.totalfinaldecisionlbl(), "field": "finaldecision"
                        }
                    ]);
                }
                initTranslations();
            }
            return JobContentViewModel;
        });
