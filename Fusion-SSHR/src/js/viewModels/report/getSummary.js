/**
 * 
 * @param {type} oj
 * @param {type} ko
 * @param {type} $
 * @param {type} app
 * @param {type} commonUtil
 * @param {type} services
 * @param {type} translatehelper
 * @returns {JobL#13.JobContentViewModel}
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojknockout', 'ojs/ojtrain', 'ojs/ojbutton', 'ojs/ojarraydataprovider', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker',
    'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup', 'ojs/ojcheckboxset', 'ojs/ojradioset',
    'ojs/ojlabel', 'ojs/ojgauge', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojmessage', 'jquery-calendar',
    'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin','ojs/ojtable',
    'jquery-calendar-picker', 'ojs/ojvalidationgroup', 'ojs/ojarraydataprovider'],
        function (oj, ko, $, app, commonUtil, services) {
            /**
             * The view model for the main content view template
             */
            function JobContentViewModel() {
                var self = this;
                self.columnArray = ko.observableArray();
                self.addSummaryLbl = ko.observable();
                self.editSummaryLbl = ko.observable();
                self.viewSummaryLbl = ko.observable();
                self.reportNameLbl = ko.observable();
                self.parameter1Lbl = ko.observable();
                self.parameter2Lbl = ko.observable();
                self.parameter3Lbl = ko.observable();
                self.parameter4Lbl = ko.observable();
                self.parameter5Lbl = ko.observable();
                self.reportTypelbl = ko.observable();
                self.backActionLbl = ko.observable();
                self.DeleteSummaryLbl = ko.observable();
                self.confirmMessage = ko.observable();
                self.oprationMessage = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                var deptArray = [];
                self.summaryObservableArray = ko.observableArray(deptArray);
                self.dataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.summaryObservableArray, {idAttribute: 'id'}));
                self.isDisable = ko.observable(true);
                self.dataSource;
                self.checkResultSearch = ko.observable();

                self.btnAddSummary = function () {
                    app.loading(true);
                    oj.Router.rootInstance.store('ADD');
                    oj.Router.rootInstance.go('reportSummary');
                };
                self.btnEditSummary = function () {
                    app.loading(true);
                    var element = document.getElementById('table');
                    var currentRow = element.currentRow;
                    var payload = {
                        clickType: 'Edit',
                        data: self.summaryObservableArray()[currentRow['rowIndex']]
                    };

                    oj.Router.rootInstance.store(payload);
                    oj.Router.rootInstance.go('reportSummary');

                };
                self.btnViewSummary = function () {
                    app.loading(true);
                    var element = document.getElementById('table');
                    var currentRow = element.currentRow;
                    var payload = {
                        clickType: 'view',
                        data: self.summaryObservableArray()[currentRow['rowIndex']]
                    };

                    oj.Router.rootInstance.store(payload);
                    oj.Router.rootInstance.go('reportSummary');

                };

                self.tableSelectionListener = function () {
                    self.isDisable(false);

                };

                self.getSummary = function () {

                    var getReportCbFn = function (data) {
                        if (data.length !== 0)
                        {

                            $.each(data, function (index) {

                                self.summaryObservableArray.push({
                                    id: [data[index].id],
                                    report_Name: [data[index].report_Name],
                                    parameter1: [data[index].parameter1],
                                    parameter2: [data[index].parameter2],
                                    parameter3: [data[index].parameter3],
                                    parameter4: [data[index].parameter4],
                                    parameter5: [data[index].parameter5],
                                    reportTypeVal: [data[index].reportTypeVal]

                                });
                            });
                        } else {
                            $.notify(self.checkResultSearch(), "error");
                        }



                    };
                    var failCbFn = function () {
                        $.notify(self.checkResultSearch(), "error");
                    };
                    var serviceName = commonUtil.getAllSummary;
                    services.getAllSummary(serviceName).then(getReportCbFn, failCbFn);
                };



//////////////////////////////


                self.reportName = ko.observable();
                self.search = ko.observable();
                self.clear = ko.observable();


                self.reportSearch = {
                    reportname: ko.observable()

                };
                var el = document.getElementById("searchName");


                self.searchPerson = function () {

                    if (!self.reportSearch.reportname()) {

                        self.getSummary();
                    } else {
                        self.summaryObservableArray([]);
                        self.searchReportName();

                    }
                };

                self.reset = function () {
                    clearContent();
                };
                function clearContent() {
                    self.reportSearch.reportname("");
                    self.summaryObservableArray([]);
                }

                self.checkResultSearch = ko.observable();
                self.searchReportName = function () {

                    var reportNameCodeCbFn = function (data) {



//                        if (data.length !== 0) {
//                            self.summaryObservableArray(data);
//
//                            //self.dataprovider = new oj.ArrayDataProvider(self.deptObservableArray, {idAttribute: 'jobId'});  
//                        } else {
////                            $.notify(self.checkResultSearch(), "error");
//                            self.summaryObservableArray([]);
//                        }

                        if (data.length !== 0)
                        {

                            $.each(data, function (index) {

                                self.summaryObservableArray.push({
                                    id: [data[index].id],
                                    report_Name: [data[index].report_Name],
                                    parameter1: [data[index].parameter1],
                                    parameter2: [data[index].parameter2],
                                    parameter3: [data[index].parameter3],
                                    parameter4: [data[index].parameter4],
                                    parameter5: [data[index].parameter5],
                                    reportTypeVal: [data[index].reportTypeVal]

                                });
                            });
                        } else {
                            $.notify(self.checkResultSearch(), "error");
                        }
                    };
                    //self.dataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.summaryObservableArray,{idAttribute : 'id'}));
                    var failCbFn = function () {
                        //$.notify(self.checkResultSearch(), "error");
                    };
                    var service = "summary/search";
                    services.searcheGeneric(service, self.reportSearch).then(reportNameCodeCbFn, failCbFn);
                };

                ////////////////////


                self.handleAttached = function (info) {
                    app.loading(false);
                    self.getSummary();
                    //console.log(app.personDetails());
                    
                };
                 self.backAction=function(){
                   oj.Router.rootInstance.go('setup');  
                };
                 self.btnDeleteSummary=function(){
                  document.querySelector("#yesNoDialog").open();
                };
                 self.commitRecordDelete=function(){
                     var element = document.getElementById('table');
                    var currentRow = element.currentRow;
                   var id = self.summaryObservableArray()[currentRow['rowIndex']].id;
                   var deleteReportCBF=function(data){
                       self.getSummary();
                       document.querySelector("#yesNoDialog").close();
                   };
                   
                  services.getGeneric("summary/deleteReport/" + id, {}).then(deleteReportCBF, app.failCbFn);
                };
                 self.cancelButtonDelete=function(){
                  document.querySelector("#yesNoDialog").close();
                };



                var getTranslation = oj.Translations.getTranslatedString;

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();

                    }

                });
                function initTranslations() {
                    self.search(getTranslation("others.search"));
                    self.clear(getTranslation("others.clear"));
                    self.addSummaryLbl(getTranslation("report.addSummary"));
                    self.editSummaryLbl(getTranslation("report.editSummary"));
                    self.viewSummaryLbl(getTranslation("report.viewSummary"));
                    self.reportNameLbl(getTranslation("report.reportName"));
                    self.parameter1Lbl(getTranslation("report.Parameter1"));
                    self.parameter2Lbl(getTranslation("report.Parameter2"));
                    self.parameter3Lbl(getTranslation("report.Parameter3"));
                    self.parameter4Lbl(getTranslation("report.Parameter4"));
                    self.parameter5Lbl(getTranslation("report.Parameter5"));
                    self.reportTypelbl(getTranslation("report.reportType"));
                    self.checkResultSearch(getTranslation("common.checkResultSearch"));
                    self.backActionLbl(getTranslation("approvalScreen.backAction"));
                    self.DeleteSummaryLbl(getTranslation("others.delete"));
                    self.confirmMessage(getTranslation("report.confirmMessage"));
                    self.oprationMessage(getTranslation("labels.message"));
                    self.no(getTranslation("others.no"));
                    self.yes(getTranslation("others.yes"));
                    self.columnArray([
                        {
                            "headerText": self.reportNameLbl(), "field": "report_Name"
                        },
                        {
                            "headerText": self.parameter1Lbl(), "field": "parameter1"
                        },
                        {
                            "headerText": self.parameter2Lbl(), "field": "parameter2"
                        },
                        {
                            "headerText": self.parameter3Lbl(), "field": "parameter3"
                        },
                        {
                            "headerText": self.parameter4Lbl(), "field": "parameter4"
                        },
                        {
                            "headerText": self.parameter5Lbl(), "field": "parameter5"
                        },
                        {
                            "headerText":self.reportTypelbl(), "field": "reportTypeVal"
                        }
                    ]);
                }
                initTranslations();
            }
            return JobContentViewModel;
        });
