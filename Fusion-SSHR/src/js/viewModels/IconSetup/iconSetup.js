/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * homeministryJob module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper',
    'ojs/ojknockout', 'ojs/ojarraydataprovider', 'ojs/ojdatetimepicker', 'ojs/ojtimezonedata',
    'ojs/ojselectcombobox', 'ojs/ojdialog', 'ojs/ojtrain', 'ojs/ojvalidationgroup','ojs/ojmessages', 'ojs/ojmessage','ojs/ojarraydataprovider', 'ojs/ojinputtext', 'ojs/ojlabel', 'ojs/ojfilepicker'],
        function (oj, ko, $, app, services, commonhelper) {

            function iconStupScreenModel() {
                var self = this;
                var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
                self.columnArray = ko.observableArray();
                self.tracker = ko.observable();
                self.groupValid = ko.observable();
                self.placeholder = ko.observable("Please Select Value");
                self.dataSourceTB2 = ko.observable();
                self.isVisibleRoleName = ko.observable(false);
                self.isVisibleSpecialCase = ko.observable(false);
                var deptArray = [];
                self.isDisabled = ko.observable(false);
                self.selctedStruct = ko.observable();
                self.isRequired = ko.observable(true);
                self.currentStepValue = ko.observable('stp1');
                self.addBtnVisible = ko.observable(false);
                self.nextBtnVisible = ko.observable(true);
                self.pageMode = ko.observable();
                self.supervisorHRCYesNoList = rootViewModel.globalHRCYesNo();
                self.oprationMessage = ko.observable();
                self.oprationEditMessage = ko.observable();
                self.approval = ko.observable();
                self.disableSubmit = ko.observable(false);
                self.xx = ko.observable();
                self.isRequiredRoleName = ko.observable(false);
                self.isRequiredSpecialCase = ko.observable(false);
                self.roleOptionType = ko.observableArray([]);
                self.rolesOption = ko.observableArray([]);
                self.approvalCodeArr = ko.observableArray([]);
                self.historyDetails = ko.observable();
                var deptArray = [];
                self.disableBtnSubmit = ko.observable(false);
                self.isExist = ko.observable();
                self.deptObservableArray = ko.observableArray(deptArray);
                self.isVisibleRoles = ko.observable(false);
                self.isRequiredRoles = ko.observable(false);
                self.isExist = ko.observable();
                var countValid = "";
                self.selectedItemsAttachment = ko.observableArray([]);
                self.attachmentViewerData = ko.observable();

                // Call self Service
                self.EitNameArr = ko.observableArray([]);
                self.staticRequestArr = ko.observableArray();
                
                self.messages = ko.observableArray();
                self.messagesDataProvider = new oj.ArrayDataProvider(self.messages);
                self.position =
                        {
                            "my": {"vertical": "top", "horizontal": "end"},
                            "at": {"vertical": "top", "horizontal": "end"},
                            "of": "window"
                        };
                self.closeMessageHandler = function (event) {
                    self.messages.remove(event.detail.message);
                };
                self.callEITCode = function () {

                    var EITCodeCbFn = function (data) {

                        var tempObject = data;
                        for (var i = 0; i < tempObject.length; i++) {

                            // push in array
                            self.EitNameArr.push({
                                value: tempObject[i].DESCRIPTIVE_FLEX_CONTEXT_CODE,
                                label: tempObject[i].DESCRIPTIVE_FLEX_CONTEXT_NAME
                            });

                        }
//                         self.EitNameArr.push({
//                                value: "XXX_RECRUITMENT_REQUEST",
//                                label: "Employee Probationary Period Evaluation"
//                            });

                    };
                    var failCbFn = function () {
                    };

                    services.getEitNameReportReport(app.lang).then(EITCodeCbFn, failCbFn);
                };

                var positionObj;
                self.stepArray = ko.observableArray([{label: "XXXX", id: 'stp1'},
                    {label: "YYYY", id: 'stp2'}]);//workNatureAllowance

                self.iconsSetupModel = {
                    ID: ko.observable(),
                    EITCODE: ko.observable(),
                    ICONS: ko.observable(),
                    NAME: ko.observable()
                };

                self.backAction = function () {
                    if (oj.Router.rootInstance._navHistory.length > 1) {
                        app.loading(true);
                        oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                    }
                };
                function getEITIconsSetup() {
                    var getIcons = function (data) {
                        app.EITIcons([]);
                        for (var i = 0; i < data.length; i++) {
                            app.EITIcons.push({
                                "ID": data[i].id, "eitCode": data[i].EITCODE, "icons": data[i].ICONS, "name": data[i].NAME
                            });
                        }
                    };
                    services.getGenericAsync(commonhelper.getIconSetup).then(getIcons, app.failCbFn);
                }

                self.handleAttached = function () {
                    app.loading(false);
                    initTranslations();

                    var arr = [{value: "XXX_RECRUITMENT_REQUEST",
                            label: self.EmployeeProbationaryPeriodEvaluationLBL()}];
                    var totalArr = [];
                    totalArr = arr.concat(app.globalEitNameReport());


                    self.EitNameArr(totalArr);
                    positionObj = JSON.parse(localStorage.getItem('reterieveIconData'));



                    if (positionObj.type == 'ADD') {

                        self.pageMode('ADD');
                        self.clothesAllowance(self.addManageLbl());
                        self.validationMessage(self.manageAdd());

                    } else if (positionObj.type == 'EDIT') {
                        self.pageMode('EDIT');
                            
                        self.connected();
                        self.clothesAllowance(self.editManageLbl());
                        self.validationMessage(self.editMassageLbl());
                        self.iconsSetupModel.ID(positionObj.ID);
                        console.log("disable");
                        self.isDisabled(true);

                    }

                };

                //-------------This Function For Update Train Step To previousStep------------------
                self.previousStep = function () {
                    var prev = document.getElementById("train").getPreviousSelectableStep();
                    if (prev != null)
                        self.currentStepValue(prev);
                };
                self.nextStep = function ()
                {
                    var getIcons = function (data) {
                         countValid = data.length;
                    };
                    services.getGeneric(commonhelper.iconSetupUrl + self.iconsSetupModel.EITCODE(), {}).then(getIcons, app.failCbFn);

                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        return;
                    }
                    var next = document.getElementById("train").getNextSelectableStep();
                    if (next != null)
                    {
                        var reterieveIconData = JSON.parse(localStorage.getItem('reterieveIconData'));
                        if (parseInt(countValid) > 0 && reterieveIconData.type == 'ADD')
                        {
                            self.messages.push({
                                severity: 'error',
                                summary: self.validMessageLbl(),
                                autoTimeout: 0
                            });
                        }else if (self.koArray()==''){
                            self.messages.push({
                                severity: 'error',
                                summary: self.validateAttachment(),
                                autoTimeout: 0
                            });
                        } 
                        else
                        {
                            var flag = true;

                            if (flag)
                            {

                                self.currentStepValue(next);

                                self.currentStepValueText();
                            }
                        }
                    }

                };
                self.currentStepValueText = function () {
                    if (self.currentStepValue() == 'stp2') {
                        self.isDisabled(true);
                        self.addBtnVisible(true);
                        self.nextBtnVisible(false);
                    }else if(self.pageMode()=='EDIT'){                        
                        self.isDisabled(true);                        
                        self.addBtnVisible(false);
                        self.nextBtnVisible(true);
                    } 
                    else {
                        self.isDisabled(false);
                        self.addBtnVisible(false);
                        self.nextBtnVisible(true);
                    }

                    return self.clothesAllowance();
                };
                //------------------End Of Section -------------------------------------------------

                //------------------This Section For Dialog Action (Open ,Close)
                self.submitButton = function () {
                    document.querySelector("#yesNoDialog").open();
                };
                self.cancelButton = function () {
                    document.querySelector("#yesNoDialog").close();
                };
                self.cancelButtonDialog = function () {
                    document.querySelector("#NoDialog").close();
                    document.querySelector("#yesNoDialog").close();
                };
                //------------------End Of Section ----------------------------------------------

                //-------------This Function To Stop Train Action If Action Are Not Valid -------
                self.stopSelectListener = function (event, ui) {
                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        event.preventDefault();
                        return;
                    }
                };
                //-------------------End Of Function ---------------------------------------------
                //------------------ This Section For Call Web Service For Insert Or Update ---------
                function oprationServiceRecord() {
                    self.iconsSetupModel.ICONS(self.koArray()[0].data);
                    self.iconsSetupModel.NAME(self.koArray()[0].name);
                    self.jsonArr = ko.observableArray([self.iconsSetupModel]);
                    
                    var jsonData = ko.toJSON(self.jsonArr);                    
                        var getIconCBFN2 = function (data) {
                               
                       self.messages.push({
                            severity: 'confirmation',
                            summary: self.editMassage(),
                            autoTimeout: 0
                        });

                            var Data = data;
                            self.disableSubmit(false);
                            if (oj.Router.rootInstance._navHistory.length > 1) {
                                oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                            } else {
                                oj.Router.rootInstance.go('iconSetupSummary');
                            }
                            getEITIconsSetup();
                        };

                        if (positionObj.type === 'ADD')
                        {

                            var getIconCBFN = function (data) {

                                if (data === '0') {

                                    return;
                                }

                                self.deptObservableArray(data);
                                self.messages.push({
                                severity: 'confirmation',
                                summary: self.addMassageLbl(),
                                autoTimeout: 0
                                });
                                if (oj.Router.rootInstance._navHistory.length > 1) {
                                    oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                                } else {
                                    oj.Router.rootInstance.go('iconSetupSummary');
                                }
                                getEITIconsSetup();

                            };
                            var failCbFn = function () {
                                $.notify(self.isExist(), "error");
                                document.querySelector("#yesNoDialog").close();
                            };

                            services.addGeneric(commonhelper.iconSetupUrl + self.pageMode(), jsonData).then(getIconCBFN, failCbFn);

                        } else {
                            services.addGeneric(commonhelper.iconSetupUrl + self.pageMode(), jsonData).then(getIconCBFN2, failCbFn);
                        }
                }
                ;
                self.commitRecord = function (data, event) {
                    if (!self.disableSubmit()) {
                        self.disableSubmit(true);
                        oprationServiceRecord();
                        return true;
                    }
                };

                //-------------------------Start Load Funcation------------------------------

                self.connected = function () {
                    var reterieveIconData = JSON.parse(localStorage.getItem('reterieveIconData'));
                    self.iconsSetupModel.ID(reterieveIconData.ID);
                    self.iconsSetupModel.EITCODE(reterieveIconData.EITCODE);
                    self.koArray.push({
                                    "name": reterieveIconData.NAME,
                                    "data":reterieveIconData.ICONS
                                });
//                    self.iconsSetupModel.ICONS(oj.Router.rootInstance.retrieve().descriptionEn);
//                    self.iconsSetupModel.NAME(oj.Router.rootInstance.retrieve().selfServConfMessageAr);
                };
                //-------------------------End Load Function---------------------------------
                //-------------------------------Icons ----------------//
                var dataFiles = {};
                self.koArray = ko.observableArray([]);
                self.dataProviderAttachment = new oj.ArrayDataProvider(self.koArray, {keyAttributes: 'id'});
                self.attachmentId = 0;
                self.selectedItemsAttachment = ko.observableArray([]);
                self.attachmentViewerData = ko.observable();
                self.attachmentSelectListener = function (event) {
                    var files = event.detail.files;
                    if (files.length > 0) {

                        //add the new files at the beginning of the list

                        for (var i = 0; i < files.length; i++) {
                            function getBase64(file) {
                                return new Promise((resolve, reject) => {
                                    const reader = new FileReader();
                                    reader.readAsDataURL(file);
                                    reader.onload = () => resolve(reader.result);
                                    reader.onerror = error => reject(error);
                                });
                            }
                            dataFiles.name = files[i].name;
                            dataFiles.id = i + 1;
                            getBase64(files[i]).then(function (data) {
                                dataFiles.data = (data);
                                self.attachmentId = self.attachmentId + 1;
                                dataFiles.id = (self.attachmentId);
//                        self.koArray.push(dataFiles);
                                self.koArray([]);
                                self.koArray.push({
                                    "name": dataFiles.name,
                                    "data": dataFiles.data,
                                    "id": dataFiles.id
                                });



//                        self.approvalSetupModel.resumeAttachment(dataFiles.data);
                            }
                            );
                        }
                    }

                };
                self.removeSelectedAttachment = function (event) {
                    $.each(self.selectedItemsAttachment(), function (index, value)
                    {
                        self.koArray.remove(function (item)
                        {
                            return (item.id == value);
                        });
                    });
                };

                self.openAttachmentViewer = function ()
                {
                    $.each(self.selectedItemsAttachment(), function (index, value)
                    {
                        var data = self.koArray().find(e=>e.id == value);
                        if(data && data.name){
                            var ext = data.name.substring(data.name.lastIndexOf('.')+1, data.name.length) || data.name;
                            if( ext == 'pdf'){
                                app.pdfViewer(data.data,"pdf");
                            }else{
                                app.pdfViewer(data.data,"img");
                            }
                        }else{
                            var reterieveIconData = JSON.parse(localStorage.getItem('reterieveIconData'));
                            app.pdfViewer(reterieveIconData.ICONS,"img");
                        }
                    });
                };

                //-----------------end icons----------------------------//

                //---------------------Translate Section ----------------------------------------
                self.ok = ko.observable();
                self.startDateLbl = ko.observable();
                self.endDateLbl = ko.observable();
                self.addLbl = ko.observable();
                self.editLbl = ko.observable();
                self.gradeLbl = ko.observable();
                self.validGradeLbl = ko.observable();
                self.structureNameLbl = ko.observable();
                self.backLbl = ko.observable();
                self.nextLbl = ko.observable();
                self.submitLbl = ko.observable();
                self.placeholder = ko.observable();
                self.eitCodeLbl = ko.observable();
                self.selfService = ko.observable();
                self.descriptionArLbl = ko.observable();
                self.descriptionEnLbl = ko.observable();
                self.selfServiceConfMessageArLbl = ko.observable();
                self.selfServiceConfMessageEnLbl = ko.observable();

                self.EmployeeProbationaryPeriodEvaluationLBL = ko.observable();

                self.cancel = ko.observable();
                self.create = ko.observable();
                self.review = ko.observable();
                self.workNatureAllowanceScreenAddLbl = ko.observable();
                self.workNatureAllowanceScreenEditLbl = ko.observable();
                self.manageAdd = ko.observable();
                self.addManageLbl = ko.observable();
                self.editManageLbl = ko.observable();
                self.addMassageLbl = ko.observable();
                self.validMessageLbl = ko.observable();
                self.validateAttachment = ko.observable();
                self.editMassageLbl = ko.observable();                
                self.editMassage = ko.observable();
                self.validationMessage = ko.observable();
                self.confirmMessage = ko.observable();
                self.clothesAllowance = ko.observable();
                self.roleNameLbl = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.validNotificationTypeLbl = ko.observable();
                self.rolesLbl = ko.observable();
                self.attachmentLbl = ko.observable();
                self.UploadFileLbl = ko.observable();
                self.removeBtnLbl = ko.observable();
                self.viewLbl = ko.observable();
                var getTranslation = oj.Translations.getTranslatedString;
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        self.stepArray([{label: self.create(), id: 'stp1'},
                            {label: self.review(), id: 'stp2'}]);
                        initTranslations();
                    }
                });
                function initTranslations() {
                    self.create(getTranslation("labels.create"));
                    self.review(getTranslation("others.review"));
                    self.structureNameLbl(getTranslation("additionalDetails.structureName"));
                    self.backLbl(getTranslation("others.back"));
                    self.nextLbl(getTranslation("others.next"));
                    self.submitLbl(getTranslation("others.submit"));
                    self.ok(getTranslation("others.ok"));
                    self.addLbl(getTranslation("others.add"));
                    self.editLbl(getTranslation("others.edit"));
                    self.validGradeLbl(getTranslation("others.validGrade"));
                    self.eitCodeLbl(getTranslation("approvalScreen.eitCode"));
                    self.selfService(getTranslation("manageOnlineHelp.selfSerivce"));
                    self.cancel(getTranslation("others.cancel"));
                    self.workNatureAllowanceScreenAddLbl(getTranslation("manageOnlineHelp.manageAdd"));
                    self.workNatureAllowanceScreenEditLbl(getTranslation("approvalScreen.ApprovalEdit"));
                    self.manageAdd(getTranslation("iconSetup.operationMessage"));
                    self.addManageLbl(getTranslation("iconSetup.addManageLbl"));
                    self.addManageLbl(getTranslation("iconSetup.addManageLbl"));
                    self.editManageLbl(getTranslation("iconSetup.editManageLbl"));
                    self.editMassage(getTranslation("iconSetup.editMassage"));                    
                    self.addMassageLbl(getTranslation("iconSetup.addMessage"));
                    self.confirmMessage(getTranslation("labels.confirmMessage"));
                    self.oprationEditMessage(getTranslation("iconSetup.oprationEditMessage"));
                    self.yes(getTranslation("others.yes"));
                    self.no(getTranslation("others.no"));
                    self.roleNameLbl(getTranslation("approvalScreen.roleName"));
                    self.placeholder(getTranslation("labels.placeHolder"));
                    self.validNotificationTypeLbl(getTranslation("workNatureAllowance.notificationMessage"));
                    self.rolesLbl(getTranslation("approvalScreen.roles"));
                    self.EmployeeProbationaryPeriodEvaluationLBL(getTranslation("approvalScreen.EmployeeProbationaryPeriodEvaluation"));
                    self.attachmentLbl(getTranslation("iconSetup.attachmentLbl"));
                    self.UploadFileLbl(getTranslation("common.UploadFileLbl"));
                    self.removeBtnLbl(getTranslation("common.removeBtnLbl"));
                    self.viewLbl(getTranslation("common.viewLbl"));                    
                    self.validMessageLbl(getTranslation("iconSetup.validMessageLbl"));                    
                    self.validateAttachment(getTranslation("iconSetup.validateAttachment"));
                    self.editMassageLbl(getTranslation("iconSetup.oprationEditMessage"));
                    self.stepArray([{label: self.create(), id: 'stp1'},
                        {label: self.review(), id: 'stp2'}]);
                }
                initTranslations();

            }

            return iconStupScreenModel;
        });
