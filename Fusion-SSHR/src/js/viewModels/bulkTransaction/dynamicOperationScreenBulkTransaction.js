/**
 * @license
 * Copyright (c) 2014, 2018, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your about ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper',
    'config/buildScreen', 'ojs/ojknockout', 'ojs/ojarraydataprovider', 'ojs/ojdatetimepicker', 'ojs/ojtimezonedata', 'ojs/ojinputnumber',
    'ojs/ojselectcombobox', 'ojs/ojdialog', 'ojs/ojtrain', 'ojs/ojvalidationgroup', 'ojs/ojmessages', 'ojs/ojformlayout', , 'ojs/ojselectcombobox', 'ojs/ojtimezonedata', 'ojs/ojdatetimepicker', 'ojs/ojselectcombobox', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource'],
        function (oj, ko, $, app, services, commonhelper, buildScreen) {

            function dynamicOperationScreenViewModel() {
                var self = this;
                var Typs = ['date', 'date', 'number'];
                var disabeld = [];
                var count = 0;
                var isErrMsg;
                var globalCheckDepartmentOrPerson;
                var Keys = [];
                var PaaSKeys = [];
                var ArrKeys = [];
                var tempObject;
                var dummyCount = 0;
                self.firstOpen = ko.observable(true);
                self.dynamicColumnIterater = ko.observable();
                self.pageMode = ko.observable();
                self.eit_id = ko.observable();
                self.EitName = ko.observableArray();
                self.eit_n = ko.observable();
                self.submitdraft = ko.observable(false);
                self.getStatus = ko.observable();
                self.status = ko.observable();
                self.columnArray1 = ko.observableArray();
                self.lang = "";
                self.ishide = ko.observable(false);
                self.actionModelsArr = [];
                self.departmentorPersonNumber = ko.observable();
                self.selectedValue = ko.observable();
                self.department = ko.observable();
                self.refNumber = ko.observable();
                self.validationMessagesArr = ko.observableArray();
                self.validationMessagesProvider = new oj.ArrayDataProvider(self.validationMessagesArr);
                self.DepartmentOrPersonNumberArr = ko.observableArray([]);
                self.DepartmentArr = ko.observableArray([]);
                self.departmentValue = ko.observable();
                self.personNumber = ko.observable();
                self.personnumberValue = ko.observable();
                self.dataSource;
                self.personNumberArr = ko.observableArray([]);
                self.numberOfCount = ko.observable();
                self.btnAddLbl = ko.observable();
                self.btnRestLbl = ko.observable();
                self.btnDeleteLbl = ko.observable();
                self.columnArray = ko.observableArray();
                self.blookAction = false;
                self.dataArray = [{value: 'zz', label: 'zz'}];
                self.validationCheck = ko.observable(false);
                self.val = ko.observable("CH");
                self.attch = ko.observable();
                self.isDisabledx = {};
                self.Arrs = {};
                self.model = {};
                self.PaaSmodel = {};
                self.approvlModel = {};
                self.actionAvilable = ko.observable(true);
                self.ok = ko.observable();
                self.startDateLbl = ko.observable();
                self.endDateLbl = ko.observable();
                self.approvaltype = ko.observable();
                self.approvalrole = ko.observable();
                self.addLbl = ko.observable();
                self.editLbl = ko.observable();
                self.submitdraftLbl = ko.observable();
                self.gradeLbl = ko.observable();
                self.validGradeLbl = ko.observable();
                self.aproval = ko.observable();
                self.savedraft = ko.observable();
                self.attachment = ko.observable();
                self.structureNameLbl = ko.observable();
                self.backLbl = ko.observable();
                self.nextLbl = ko.observable();
                self.submitLbl = ko.observable();
                self.placeholder = ko.observable("Please Select Value");
                self.percentageLbl = ko.observable();
                self.qualificationNameLbl = ko.observable();
                self.experienceYearsLbl = ko.observable();
                self.cancel = ko.observable();
                self.create = ko.observable();
                self.review = ko.observable();
                self.workNatureAllowanceScreenAddLbl = ko.observable();
                self.workNatureAllowanceScreenEditLbl = ko.observable();
                self.addMassageLbl = ko.observable();
                self.editMassageLbl = ko.observable();
                self.confirmMessage = ko.observable();
                self.selectedPage = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.sequencetable = ko.observable();
                self.personName = ko.observable();
                self.bulkTableEmpty = ko.observable();
                self.refNumberValue = ko.observable();
                self.personNumberErr = ko.observable();
                self.employeesArrayFullDetails = ko.observableArray([]);
                self.employeeNameArr = ko.observableArray([]);
                self.employeeOptionsProvider = new oj.ArrayDataProvider(self.employeeNameArr, {keyAttributes: 'value'});
                self.personNumberOfTable = ko.observable();
                self.dateConverter = ko.observable(oj.Validation.converterFactory(oj.ConverterFactory.CONVERTER_TYPE_DATETIME).createConverter({
                    pattern: 'dd/MM/yyyy'
                }));
                //When Change Any Feield Must go to for chech if this value Are avilable in action Model 
                //action Model Must Contains array of object evrey object contain 
                //Segment Name set value into (value Name )
                //Array of flex observable that include in this Action (on change must update values )
                //and str qeurry 
                //
                //

                //start  code bulk  for bulk transaction
                self.departmentOrPersonNumber = {
                    selectedValue: ko.observable(),
                    personnumberValue: ko.observable(),
                    departmentValue: ko.observable()
                };
                self.departmentData = function () {
                    var getDepartmentCbfn = function (data) {
                        $.each(data, function (index) {
                            self.DepartmentArr.push({
                                value: data[index].locationId,
                                label: data[index].name

                            });
                        });

                    };
                    var defultUrl = commonhelper.orgnizationUrl;
                    services.getGrade(defultUrl).then(getDepartmentCbfn, app.failCbFn);
                };
                var globalPersonNumber;
                var dynamicArr;
                self.PersonChangeHandler = function (event) {
                    var valueObj = buildValueChange(event['detail']);
                    globalPersonNumber = valueObj.value;
                    if (typeof tempObject !== 'undefined') {
                        for (var i = 0; i < tempObject.length; i++) {
                            if (tempObject[i].FLEX_value_SET_NAME == "XXX_HR_DYNAMIC_COLUMN") {

                                var reportName = searchArray(tempObject[i].DESCRIPTION, app.globalEITReportLookup());
                                var getReportCBCF = function (data) {
                                    if (data.length) {
                                        self.Arrs[[dynamicArr][self.dynamicColumnIterater()]](data);
                                    } else {
                                        self.Arrs[[dynamicArr][self.dynamicColumnIterater()]]([data]);
                                    }
                                    self.firstOpen(false);
                                    self.validationMessagesArr([]);
                                };
                                var getReportCBCFailed = function (err) {
                                    if (!self.firstOpen())
                                        self.validationMessagesArr.push(self.createMessage(err.responseText));
                                    self.firstOpen(false);
                                };

                                var reportPaylad = {"reportName": reportName, "personId": globalPersonNumber + ""};
                                 services.getGenericReport(reportPaylad).then(getReportCBCF, getReportCBCFailed);
                            }
                        }
                    }

                };

                var tempArr = {
                    seq: count,
                    personNumber: self.personnumberValue(),
                    personName: app.getPersonNumber(self.personnumberValue())
                };

                self.fullTableWithData = function (data) {
                    self.personNumberArr.push(tempArr);
                    self.dataSourceTB2 = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.personNumberArr, {idAttribute: 'seq'}));
                };

                self.buttonAdd = function () {
                    // validation 
                    for (var i = 0; i < self.personNumberArr().length; i++) {
                        if (typeof self.personNumberArr()[i] !== 'undefined') {
                            if (self.personNumberArr()[i].personNumber === self.personnumberValue()
                                    || self.personNumberArr()[i].personId === self.personnumberValue()) {
                                self.validationMessagesArr.push(self.createMessage(self.personNumberErr()));
                                return;
                            } else {
                                self.validationMessagesArr([]);
                            }
                        }
                    }
                    count++;
                    dummyCount++;
                    tempArr = {};
                    for (var j = 0; j < self.employeesArrayFullDetails().length; j++) {
                        if (self.employeesArrayFullDetails()[j].PERSON_ID == self.personnumberValue())
                        {
                            self.personNumberOfTable(self.employeesArrayFullDetails()[j].PERSON_NUMBER);
                        }
                    }
                    tempArr = {
                        seq: count,
                        personId:globalPersonNumber,
                        personName:app.getPersonNumber(globalPersonNumber) 
                        
                    };
                    for (var i = 0; i < tempObject.length; i++) {
                        var tempObjectDecs = tempObject[i].DESCRIPTION;
                        var selfModel = self.model[tempObjectDecs]();
                        
                        if (typeof self.Arrs[tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr"] !== 'undefined') {
                            var tempArrs = self.Arrs[tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr"]._latestValue;
                            for (var j = 0; j < tempArrs.length; j++) {
                                if (typeof tempArrs[j] !== 'undefined' && selfModel === tempArrs[j].value) {
                                    selfModel = tempArrs[j].label;
                                }
                            }
                        }

                        if (tempObjectDecs === 'dummy') {
                            tempArr[tempObjectDecs] = selfModel + dummyCount;
                        } else {
                            tempArr[tempObjectDecs] = selfModel;
                        }
                    }
                    self.fullTableWithData();
                };

                self.buttonRest = function () {
                    self.personNumberArr([]);
//                    self.personnumberValue("");
//                    self.departmentValue("");
                    count = 0;
                };

                self.departmentOrPersonChangeHandler = function (event) {

                    var valueObj = buildValueChange(event['detail']);
                    globalCheckDepartmentOrPerson = valueObj;
                    if (valueObj.value === "Department") {
                        self.buttonRest();
                    } else {
                        self.DepartmentArr([]);
                        if (count === 0) {
                            self.buttonRest();

                        }

                    }

                };
                
                function buildValueChange(valueParam) {
                    var valueObj = {};

                    valueObj.previousValue = valueParam.previousValue;
                    valueObj.value = valueParam.value;
                    valueObj.label = valueParam.label;

                    return valueObj;
                }

                self.dataSourceTB2 = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.personNumberArr, {idAttribute: 'seq'}));

                //start get data in combox
                self.getpersonname =  function () {
                    var getReportValidationCBCF = function (data) {
                        self.employeeNameArr([]);
                        self.employeesArrayFullDetails(data);
                        
                        for (var i = 0; i < data.length; i++) {
                            self.employeeNameArr.push({
                                value: data[i].PERSON_ID,
                                label: data[i].FULL_NAME
                            });
                        }
                        self.employeeOptionsProvider = new oj.ArrayDataProvider(self.employeeNameArr, {keyAttributes: 'value'});

                    };
                    var reportPaylod = {"reportName": "employeeNameReport"};
                    services.getGenericReport(reportPaylod).then(getReportValidationCBCF, app.failCbFn);
                };

                //End get data in combox 

                //End code bulk  for bulk transaction

                self.handleDetached = function () {
                    self.DepartmentArr([]);
                    self.personNumberArr([]);
                };

                self.handleAttached = function (info) {
                    app.loading(false);
					self.firstOpen(true);
                    self.departmentData();
                    self.getpersonname();
                    self.disableSubmit2(false);
                    self.validationMessagesArr([]);
                    var Eit_n = searchArray(self.EitCode, self.EitName());
                    ArrKeys = [];
                    self.Arrs = {};
                    self.fullTableWithData();
                    self.personNumberArr([]);
                    self.EitName(app.globalEitNameReport());
                    self.currentStepValue('stp1');
                    self.prevPram = oj.Router.rootInstance.retrieve();
                    self.EitCode = self.prevPram.eitCoed;
                    self.status(self.prevPram.status);
                    self.pageMode(self.prevPram.pageMode);
                    self.eit_id(self.prevPram.id);
                    self.actionModelsArr = [];
                    self.eit_n(Eit_n);
                    disabeld = [];
                    self.isDisabledx = {};
                    self.selectedPage(self.pageMode());
                    self.createMessage = function (summary) {
                        return {
                            severity: 'error',
                            summary: summary,
                            autoTimeout: 5000
                        };
                    };
                    var getEit = function (data) {
                    tempObject = data;
                    if (tempObject.length) {

                    } else {
                        tempObject = [tempObject];
                    }
                    Keys = [];
                    var Iterater = 0;
                    for (var i = 0; i < tempObject.length; i++) {
                        //Build Model                 
                        Keys.push(tempObject[i].DESCRIPTION);
                        if (Keys[i] == "dummy") {
                            self.model[Keys[i]] = ko.observable(new Date().getTime());
                        } else {
                            self.model[Keys[i]] = ko.observable();
                            if (tempObject[i].IN_FIELD_HELP_TEXT && tempObject[i].IN_FIELD_HELP_TEXT != "PAAS") {
                                var actionModel = {
                                    reportName: tempObject[i].IN_FIELD_HELP_TEXT,
                                    whenChangeValue: [],
                                    segmentName: Keys[i]
                                };


                                actionModel.whenChangeValue = searchArrayContainValue(tempObject[i].IN_FIELD_HELP_TEXT, app.globalEITDefultValueParamaterLookup());
                                if (actionModel.whenChangeValue[0]) {
                                    actionModel.whenChangeValue = actionModel.whenChangeValue[0].split(',');
                                }


                                self.actionModelsArr.push(actionModel);
                                for (var index = 0; index < actionModel.whenChangeValue.length; index++) {


                                    var reportPaylod = {"reportName": tempObject[i].IN_FIELD_HELP_TEXT};
                                    for (var index = 0; index < actionModel.whenChangeValue.length; index++) {
                                        if (Object.keys(self.model).indexOf(actionModel.whenChangeValue[index]) != -1) {
                                            reportPaylod[actionModel.whenChangeValue[index]] = self.model[actionModel.whenChangeValue[index]]();
                                        } else {
                                            reportPaylod[actionModel.whenChangeValue[index]] = app.personDetails()[actionModel.whenChangeValue[index]];
                                        }
                                    }
                                    var getReportValidationCBCF = function (dataSaaSQuery) {
                                        var resultOfQueryObj = dataSaaSQuery;

                                        self.model[Keys[i]](resultOfQueryObj[ Object.keys(dataSaaSQuery)[0]]);

                                    };
                                    services.getGenericReport(reportPaylod).then(getReportValidationCBCF, app.failCbFn);

                                }

                            } else if (tempObject[i].DEFAULT_value && tempObject[i].DEFAULT_value != "PAAS" &&tempObject[i].DEFAULT_TYPE!='C') {
                                var actionModel = {
                                    str: tempObject[i].DEFAULT_value,
                                    whenChangeValue: [],
                                    segmentName: Keys[i]
                                };
                                var str = tempObject[i].DEFAULT_value;
                                var flex = ":";
                                var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));
                                var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf('#'));
                                var is_HaveFlex = true;
                                if (str.indexOf(':') == -1) {
                                    is_HaveFlex = false;
                                }
                                while (is_HaveFlex) {
                                    var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));
                                    var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf('#'));
                                    actionModel.whenChangeValue.push(ModelName);
                                    strWitoutFlex.substring(1, strWitoutFlex.length);

                                    if (Object.keys(app.personDetails()).indexOf(strWitoutFlex.substring(1, strWitoutFlex.length)) != -1) {
                                        str = str.replace(strWitoutFlex, app.personDetails().personId);
                                    } else if (Object.keys(self.model).indexOf(strWitoutFlex.substring(1, strWitoutFlex.length - 1)) != -1) {
                                        str = str.replace(strWitoutFlex, self.model[strWitoutFlex.substring(1, strWitoutFlex.length - 1)]());
                                    }

                                    is_HaveFlex = false;
                                    if (str.indexOf(":") == -1) {
                                        is_HaveFlex = false;
                                    }
                                }


                                if (str.includes('undefined')) {
                                } else {
                                    //Here Must Call Report Fpr This Perpus 
                                    var getReportValidationCBCF = function (dataSaaSQuery) {

                                        var resultOfQueryObj = dataSaaSQuery;

                                        self.model[Keys[i]](resultOfQueryObj[ Object.keys(dataSaaSQuery)[0]]);

                                    };
                                    var validationQueryReportModel = {"reportName": 'DynamicValidationReport', "str": str};
                                    services.getGenericReport(validationQueryReportModel).then(getReportValidationCBCF, app.failCbFn);

                                }
                            }else if(tempObject[i].DEFAULT_TYPE=='C'){
                                 self.model[Keys[i]](tempObject[i].DEFAULT_value);
                            }else if (tempObject[i].IN_FIELD_HELP_TEXT == "PAAS") {
                                var defultUrl = getPaaSDefultValue(tempObject[i].DESCRIPTIVE_FLEX_CONTEXT_CODE, tempObject[i].DESCRIPTION, app.allPaaSDefultValuearry());
                                var getDefultPaaSCbfn = function (data) {
                                    var jsonData = JSON.parse(data);
                                    self.model[Keys[i]](jsonData[Object.keys(jsonData)[0]]);
                                };

                                services.getGeneric(defultUrl).then(getDefultPaaSCbfn, app.failCbFn);

                            }

                        }
                        
                        PaaSKeys.push(tempObject[i].APPLICATION_COLUMN_NAME);
                        self.PaaSmodel[PaaSKeys[i]] = ko.observable(self.model[Keys[i]]());
                        //end OF Build Model 
                        if ((tempObject[i].FLEX_value_SET_NAME != "100 Character No Validation") &&
                                (tempObject[i].FLEX_value_SET_NAME != "10 Number") &&
                                (tempObject[i].FLEX_value_SET_NAME != "HRC_NUMBER_10") &&
                                (tempObject[i].FLEX_value_SET_NAME != "HRC_STANDARD_DATE") &&
                                (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_EIT_ATTACHMENTS") &&
                                (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_PAAS_NUMBER") &&
                                (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_PAAS_DATE") &&
                                (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_PAAS_VIEW_REPORT")
                                ) {
                            if(tempObject[i].value_SET_TYPE=="X"||tempObject[i].value_SET_TYPE=="I"){
                                self.ishide(false);
                                ArrKeys.push(tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr");
                                self.Arrs[ArrKeys[Iterater]] = ko.observableArray();
                                var getReportCBCF = function (data) {
                                    var tempObject2 = data;
                                    var arr = tempObject2;
                                    var arrays = [];
                                    for (var x = 0; x < arr.length; x++) {
                                        arrays.push({"value": arr[x].FLEX_value, "label": arr[x].value_DESCRIPTION})
                                    }

                                    self.Arrs[ArrKeys[Iterater]](arrays);

                                };
                                var xx = {"reportName": "inDependentReport", "valueSet": tempObject[i].FLEX_value_SET_NAME};
                                services.getGenericReport(xx).then(getReportCBCF, app.failCbFn);
                                Iterater = Iterater + 1;
                            } else if (tempObject[i].value_SET_TYPE == "Y") {
                                self.ishide(false);
                                ArrKeys.push(tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr");
                                self.Arrs[ArrKeys[Iterater]] = ko.observableArray();

                                Iterater = Iterater + 1;

                            } else if (tempObject[i].value_SET_TYPE == "F") {
                                self.ishide(false);
                                ArrKeys.push(tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr");
                                self.Arrs[ArrKeys[Iterater]] = ko.observableArray();
                               var val = "value";
                                    var lbl = "label";
                                    var q;
                                   // var tempObject = data;
                                    q = "select " + tempObject[i].ID_COL + " " + val + " ,  " + tempObject[i].value_COL + "  " + lbl
                                            + " From " + tempObject[i].APPLICATION_TABLE_NAME + "  " ;
                                    if( tempObject[i].ADDITIONAL_WHERE_CLAUSE){
                                        q=q+ tempObject[i].ADDITIONAL_WHERE_CLAUSE;
                                    }

                                    var str = q;

                                    var flex = ":";
                                    var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));

                                    var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf('#'));
                                    var is_HaveFlex = true;
                                    if (str.indexOf(':') == -1) {
                                        is_HaveFlex = false;
                                    }
                                    while (is_HaveFlex) {
                                        var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));
                                        var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf('#'));
                                        actionModel.whenChangeValue.push(ModelName);
                                        str = str.replace(strWitoutFlex, app.personDetails().personId);
                                        if (str.indexOf(":") == -1) {
                                            is_HaveFlex = false;
                                        }
                                    }




                                    if (str.includes('undefined')) {
                                    } else {
                                        //Here Must Call Report Fpr This Perpus 
                                        var getArrCBCF = function (data) {
                                            var tempObject2 = data;
                                            var arr = tempObject2;
                                            if (tempObject2.length) {
                                                self.Arrs[ArrKeys[Iterater]](arr);
                                            } else {
                                                self.Arrs[ArrKeys[Iterater]]([arr]);
                                            }
                                        };
                                        services.getDynamicReport(str).then(getArrCBCF, app.failCbFn);

                                    }
                                Iterater = Iterater + 1;
                            } else if (tempObject[i].FLEX_value_SET_NAME == "XXX_HR_DYNAMIC_COLUMN") {
                                self.dynamicColumnIterater(Iterater);
                                dynamicArr = tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr";
                                ArrKeys.push(tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr");
                                self.Arrs[ArrKeys[Iterater]] = ko.observableArray();
                                Iterater = Iterater + 1;

                            }



                        } else if (tempObject[i].FLEX_value_SET_NAME==="XXX_HR_EIT_ATTACHMENTS") {
                            self.ishide(true);
                            ArrKeys.push(tempObject[i].FLEX_value_SET_NAME + "Arr");
                            self.Arrs[ArrKeys[Iterater]] = ko.observableArray();
                            Iterater = Iterater + 1;
                        }

                        //Build Disabeld Model 
                        disabeld.push("isDisabled" + tempObject[i].DESCRIPTION);
                        if (tempObject[i].READ_ONLY_FLAG == "Y") {
                            self.isDisabledx[disabeld[i]] = ko.observable(true);
                        } else if (tempObject[i].READ_ONLY_FLAG == "N") {
                            self.isDisabledx[disabeld[i]] = ko.observable(false);
                        }
                        //End Of Disabeld Model 

                        Typs.push(tempObject[i].FLEX_value_SET_NAME);
                    }
                    buildScreen.buildScreen(tempObject, "xx", self.model, self.isDisabledx, self.dataArray);
                    if (typeof tempObject !== 'undefined') {
                        self.columnArray([
                            {
                                "headerText": self.sequencetable(), "field": "seq"
                            },
                            {
                                "headerText": self.personNumber(), "field": "personId"
                            },
                            {
                                "headerText": self.personName(), "field": "personName"
                            }
                        ]);
                        for (var i = 0; i < tempObject.length; i++) {
                            if (tempObject[i].DESCRIPTION !== 'dummy')
                                self.columnArray.push({
                                    "headerText": tempObject[i].FORM_ABOVE_PROMPT, "field": tempObject[i].DESCRIPTION
                                });
                        }
                    }
                    self.actionAvilable(true);
                    if (self.pageMode() == "ADD") {
                        self.submitdraft(false);
                    }
                    if (self.pageMode() == "EDIT" || self.pageMode() == "VIEW") {
                        self.submitdraft(false);
                        if (self.pageMode() == "EDIT" && self.status() == "DRAFT") {
                            self.submitdraft(true);
                        }

                        self.status = ko.observable();
                        self.setModelValue();
                        if (self.pageMode() == "VIEW") {
                            self.submitdraft(false);
                            self.ishide(false);
                            self.setToDisabeld();
                            var attachmentView = function (data) {

                                for (var i = 0; i < data.length; i++) {

                                    var baseStr64 = data[i].attachment;
                                    drawAttachment2(i, baseStr64);
                                }

                                // imgElem.setAttribute('src', baseStr64);
                            };
                            services.getGeneric("attachment/" + self.eit_id().toString()).then(attachmentView, app.failCbFn);
                            self.actionAvilable(false);

                        }
                    }
                    //Here Hide 

                };
                
                    if (app.getLocale() == 'ar') {
                        self.lang = "AR";
                    } else {
                        self.lang = "US";
                    }
                    services.getEIT(self.EitCode, self.lang).then(getEit, app.failCbFn);
                    self.getLocale = function () {
                        return oj.Config.getLocale();

                    };
                    if (self.getLocale() === "ar") {
                        $('.approval-card-body').removeClass('app-crd-bdy-border-en');
                        $('.approval-card-body').addClass('app-crd-bdy-border-ar');
                        $('.app-crd-type').removeClass('app-card-for-en');
                        $('.app-crd-type').addClass('app-card-for-ar');
                        $('.blockquote-footer').removeClass('app-card-for-en');
                        $('.blockquote-footer').addClass('app-card-for-ar');

                    } else {
                        $('.approval-card-body').removeClass('app-crd-bdy-border-ar');
                        $('.approval-card-body').addClass('app-crd-bdy-border-en');
                        $('.app-crd-type').removeClass('app-card-for-ar');
                        $('.app-crd-type').addClass('app-card-for-en');
                        $('.blockquote-footer').removeClass('app-card-for-ar');
                        $('.blockquote-footer').addClass('app-card-for-en');
                    }

                };
                function drawAttachment2(count, baseStr64) {
                    //drawLabel(lblValue, div);
                    var input = document.createElement("img");

                    input.setAttribute('class', 'attClass' + count);
                    input.setAttribute('alt', "Image preview...");
                    input.setAttribute('height', "60");
                    input.setAttribute('width', "60");
                    input.setAttribute('src', baseStr64);
                    var parent = document.getElementById("attachedfile");

                    // return input ;
                    parent.appendChild(input);
                }
                self.setModelValue = function () {
                    for (var i = 0; i < Object.keys(self.model).length; i++) {
                        for (var j = 0; Object.keys(self.prevPram).length > j; j++) {
                            if (Object.keys(self.model)[i] == Object.keys(self.prevPram)[j]) {

                                self.model[Object.keys(self.model)[i]](self.prevPram[Object.keys(self.prevPram)[j]]);
                                break;
                            }
                        }
                    }
                    self.model.PersonExtraInfoId = self.prevPram.PersonExtraInfoId;
                    self.model.PersonId = self.prevPram.PersonId;
                };

                
                self.segmentChangedHandler = function (event, data) {
                    if (event.path[0].attributes.id) {

                        var id = event.path[0].attributes.id.nodeValue;

                        var res = id.substr(6, id.length);

                        var modelArr = Object.keys(self.model);
                        var realIndex = modelArr.indexOf(res);
                        var indexOfModel = modelArr.indexOf(res) + 1;
                        if (tempObject[indexOfModel].value_SET_TYPE == "Y") {

                            var getReportCBCF = function (data) {

                                var tempObject2 = data;
                                var arr = tempObject2;
                                var arrays = [];
                                if (arr.length) {
                                    for (var ind = 0; ind < arr.length; ind++) {
                                        arrays.push({"value": arr[ind].FLEX_value, "label": arr[ind].value_DESCRIPTION});
                                    }
                                } else {
                                    arrays.push({"value": arr.FLEX_value, "label": arr.value_DESCRIPTION});
                                }
                                self.Arrs[tempObject[indexOfModel].FLEX_value_SET_NAME + tempObject[indexOfModel].DESCRIPTION + "Arr"](arrays);
                            };
                            var xx = {"reportName": "DependentReport", "valueSet": tempObject[indexOfModel].FLEX_value_SET_NAME, "parent": self.model[modelArr[realIndex]]()};

                            services.getGenericReport(xx).then(getReportCBCF, app.failCbFn);
                        }

                    }

                    if (event.path[0].classList[0]) {

                        var curuntElement = event.path[0].classList[0];
                        for (var index = 0; index < self.actionModelsArr.length; index++) {
                            var actionObject = self.actionModelsArr[index];
                            if (self.actionModelsArr[index].whenChangeValue.indexOf(curuntElement) != -1) {
                                var haveUndifindValue = false;
                                var reportPaylod = {"reportName": actionObject.reportName};
                                for (var whenCangeIndex = 0; whenCangeIndex < actionObject.whenChangeValue.length; whenCangeIndex++)
                                {
                                    if (Object.keys(self.model).indexOf(actionObject.whenChangeValue[whenCangeIndex]) != -1) {
                                        if (self.model[actionObject.whenChangeValue[whenCangeIndex]]()) {
                                            reportPaylod[actionObject.whenChangeValue[whenCangeIndex]] = self.model[actionObject.whenChangeValue[whenCangeIndex]]().toString();

                                        }
                                    } else if (Object.keys(app.personDetails()).indexOf(actionObject.whenChangeValue[whenCangeIndex]) != -1) {
                                        if (app.personDetails()[actionObject.whenChangeValue[whenCangeIndex]]) {
                                            reportPaylod[actionObject.whenChangeValue[whenCangeIndex]] = app.personDetails()[actionObject.whenChangeValue[whenCangeIndex]];
                                        }
                                    } else {
                                        haveUndifindValue = true;
                                    }
                                }
                                if (!haveUndifindValue) {
                                    var getReportValidationCBCF = function (dataSaaSQuery) {
                                        var resultOfQueryObj = dataSaaSQuery;
                                        self.model[actionObject.segmentName](resultOfQueryObj[ Object.keys(dataSaaSQuery)[0]]);
                                    };

                                    services.getGenericReport(reportPaylod).then(getReportValidationCBCF, app.failCbFn);
                                }
                            }

                        }

                    }

                };

                //----------------------------Standerd Screen -------------------------------------------
                var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
                self.tracker = ko.observable();
                self.groupValid = ko.observable();
                self.placeholder = ko.observable("Please Select Value");
                self.dataSourceTB2 = ko.observable();
                self.isDisabled = ko.observable(false);
                self.selctedStruct = ko.observable();
                self.isRequired = ko.observable(true);
                self.currentStepValue = ko.observable('stp1');
                self.addBtnVisible = ko.observable(false);
                self.nextBtnVisible = ko.observable(true);
                self.supervisorHRCYesNoList = rootViewModel.globalHRCYesNo();
                self.oprationMessage = ko.observable();
                self.disableSubmit = ko.observable(false);
                self.disableSubmit2 = ko.observable(false);
                self.disableDeleteBtn = ko.observable(true);

                self.stepArray = ko.observableArray([{label: "XXXX", id: 'stp1'},
                    {label: "YYYY", id: 'stp2'}]);//workNatureAllowance

                self.backAction = function () {
                    if (oj.Router.rootInstance._navHistory.length > 1) {
                        app.loading(true);
                        oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                    }
                    oj.Router.rootInstance.go('dynamicSummaryBulkTransaction');
                };
                
                self.formatDate = function (date) {
                    //var date = new Date()
                    var month = '' + (date.getMonth() + 1),
                            day = '' + date.getDate(),
                            year = date.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;

                    return [year, month, day].join('-');
                };


                //-------------This Function For Update Train Step To previousStep------------------
                self.previousStep = function () {
                    var prev = document.getElementById("train").getPreviousSelectableStep();
                    if (prev != null)
                        self.currentStepValue(prev);
                    self.disableSubmit2(false);
                };
                self.nextStep = function () {
                    var jsonData = ko.toJSON(self.model);
                    var cx = JSON.stringify(jsonData);
                    isErrMsg = false;
                    cx.replace("{", "");
                    cx.replace("}", "");

                    self.validationSearch = {
                        eitCode: ko.observable(self.EitCode)

                    };
                    //----------------------------Dynamic Validation ------------------------    
                    var EITCodeCbFn = function (data) {
                        var firstValue;
                        var sucendValue;
                        var operation;
                        var reportCallModel = {};
                        if (data) {
                            for (var i = 0; i < data.length; i++) {
                                reportCallModel = {};
                                //--------------First Key Value ----------------
                                if (data[i].firstKeyType == "Application_Value") {
                                    if (data[i].applicationValueFamily == "EIT") {

                                        firstValue = self.model[data[i].applicationKey]();
                                    } else if (data[i].applicationValueFamily == "Validation_Value") {
                                        //app.validation[data[i].validationValuesName]
                                        firstValue = app.validation[data[i].applicationKey];
                                    } else if (data[i].applicationValueFamily == "Personal_Information") {
                                        firstValue = app.personDetails()[data[i].applicationKey];
                                    }
                                } else if (data[i].firstKeyType == "Static") {
                                    firstValue = data[i].firstKeyValue;

                                } else if (data[i].firstKeyType == "SaaS") {
                                    if (data[i].saaSSource == "Report") {
                                        //reportName
                                        reportCallModel.reportName = data[i].reportName;
                                        //reportCallModel.reportName ="PersonAbsenceReport";
                                        var reportPramCbFn = function (dataReport) {
                                            var getReportCBCF = function (datac) {
                                                var valueObj = datac;
                                                firstValue = valueObj.value;
                                            };
                                            // reportCallModel[dataReport[0].parameter1]=

                                            var xx = {"reportName": data[i].reportName};

                                            if (dataReport[0].parameter1) {
                                                if (Object.keys(self.model).indexOf(data[i].parameter1Name) != -1) {
                                                    xx[dataReport[0].parameter1] = self.model[data[i].parameter1Name]();
                                                } else if (data[i].parameter1Name == 'P_STRUCTURE_CODE') {
                                                    xx[dataReport[0].parameter1] = tempObject[0].DESCRIPTIVE_FLEX_CONTEXT_CODE;
                                                } else {
                                                    xx[dataReport[0].parameter1] = app.personDetails()[data[i].parameter1Name];
                                                }

                                            }
                                            if (dataReport[0].parameter2) {
                                                if (Object.keys(self.model).indexOf(data[i].parameter2Name) != -1) {
                                                    xx[dataReport[0].parameter2] = self.model[data[i].parameter2Name]();
                                                } else if (data[i].parameter2Name == 'EIT_CODE') {

                                                    xx[dataReport[0].parameter2] = tempObject[0].DESCRIPTIVE_FLEX_CONTEXT_CODE;
                                                } else {
                                                    xx[dataReport[0].parameter2] = app.personDetails()[data[i].parameter2Name];
                                                }
                                            }
                                            if (dataReport[0].parameter3) {
                                                if (Object.keys(self.model).indexOf(data[i].parameter3Name) != -1) {
                                                    xx[dataReport[0].parameter3] = self.model[data[i].parameter3Name]();
                                                } else if (data[i].parameter3Name == 'EIT_CODE') {
                                                    xx[dataReport[0].parameter3] = tempObject[0].DESCRIPTIVE_FLEX_CONTEXT_CODE;
                                                } else {
                                                    xx[dataReport[0].parameter3] = app.personDetails()[data[i].parameter3Name];
                                                }
                                            }
                                            if (dataReport[0].parameter4) {
                                                if (Object.keys(self.model).indexOf(data[i].parameter4Name) != -1) {
                                                    xx[dataReport[0].parameter4] = self.model[data[i].parameter4Name]();
                                                } else if (data[i].parameter4Name == 'EIT_CODE') {
                                                    xx[dataReport[0].parameter4] = tempObject[0].DESCRIPTIVE_FLEX_CONTEXT_CODE;
                                                } else {
                                                    xx[dataReport[0].parameter4] = app.personDetails()[data[i].parameter4Name];
                                                }

                                            }
                                            if (dataReport[0].parameter5) {
                                                if (Object.keys(self.model).indexOf(data[i].parameter5Name) != -1) {
                                                    xx[dataReport[0].parameter5] = self.model[data[i].parameter5Name]();
                                                } else if (data[i].parameter5Name == 'EIT_CODE') {
                                                    xx[dataReport[0].parameter5] = tempObject[0].DESCRIPTIVE_FLEX_CONTEXT_CODE;
                                                } else {
                                                    xx[dataReport[0].parameter5] = app.personDetails()[data[i].parameter5Name];
                                                }

                                            }
                                            services.getGenericReport(xx).then(getReportCBCF, app.failCbFn);
                                        };
                                        services.getGeneric(commonhelper.reportPram + reportCallModel.reportName).then(reportPramCbFn, failCbFn);

                                    } else if (data[i].saaSSource == "Query") {
                                        var str = data[i].saaSQuery;
                                        var flex = ":$FLEX$.";
                                        var strWitoutFlex = str.substr((str.indexOf("$FLEX$.") + flex.length - 1));
                                        var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf('#'));
                                        var is_HaveFlex = true;
                                        if (str.indexOf(':$FLEX$.') == -1) {
                                            is_HaveFlex = false;
                                        }

                                        while (is_HaveFlex) {
                                            var strWitoutFlex = str.substr((str.indexOf("$FLEX$.") + flex.length - 1));
                                            var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf('#'));
                                            str = str.replace(':$FLEX$.' + ModelName + '#', self.model[ModelName]());
                                            if (str.indexOf(":$FLEX$.") == -1) {
                                                is_HaveFlex = false;
                                            }
                                        }
                                        if (str.substr('undefined ') != -1) {
                                        }


                                        var getReportValidationCBCF = function (dataSaaSQuery) {
                                            var resultOfQueryObj = dataSaaSQuery;
                                            firstValue = resultOfQueryObj.value;
                                        };
                                        var validationQueryReportModel = {"reportName": 'DynamicValidationReport', "str": str};
                                        services.getGenericReport(validationQueryReportModel).then(getReportValidationCBCF, app.failCbFn);
                                    }
                                }
                                operation = data[i].operation;
                                //-------Secound Key Value --------------------
                                if (data[i].secondKeyType == "Application_Value") {
                                    if (data[i].secondapplicationValueFamily == "EIT") {

                                        sucendValue = self.model[data[i].secondapplicationKey]();
                                    } else if (data[i].secondapplicationValueFamily == "Validation_Value") {
                                        //app.validation[data[i].validationValuesName]
                                        sucendValue = app.validation[data[i].secondapplicationKey];

                                    } else if (data[i].secondapplicationValueFamily == "Personal_Information") {
                                        sucendValue = app.personDetails()[data[i].secondapplicationKey];
                                    }
                                } else if (data[i].secondKeyType == "Static") {
                                    sucendValue = data[i].secondKeyValue;

                                } else if (data[i].secondKeyType == "SaaS") {
                                    if (data[i].secondsaaSSource == "Report") {
                                        reportCallModel.reportName = data[i].secondreportName;
                                        var reportPramCbFn = function (dataReport) {
                                            var getReportCBCF = function (datac) {
                                                var valueObj = datac;
                                                sucendValue = valueObj.value;
                                            };
                                            // reportCallModel[dataReport[0].parameter1]=

                                            var xx = {"reportName": data[i].secondreportName};

                                            if (dataReport[0].parameter1) {
                                                if (Object.keys(self.model).indexOf(data[i].secondparameter1Name) != -1) {
                                                    xx[dataReport[0].parameter1] = self.model[data[i].secondparameter1Name]();
                                                } else {
                                                    xx[dataReport[0].parameter1] = app.personDetails()[data[i].secondparameter1Name];
                                                }

                                            }
                                            if (dataReport.parameter2) {
                                                if (Object.keys(self.model).indexOf(data[i].secondparameter2Name) != -1) {
                                                    xx[dataReport[0].parameter2] = self.model[data[i].secondparameter2Name]();
                                                } else {
                                                    xx[dataReport[0].parameter2] = app.personDetails()[data[i].secondparameter2Name];
                                                }

                                            }
                                            if (dataReport.parameter3) {
                                                if (Object.keys(self.model).indexOf(data[i].secondparameter3Name) != -1) {
                                                    xx[dataReport[0].parameter3] = self.model[data[i].secondparameter3Name]();
                                                } else {
                                                    xx[dataReport[0].parameter3] = app.personDetails()[data[i].secondparameter3Name];
                                                }

                                            }
                                            if (dataReport.parameter4) {
                                                if (Object.keys(self.model).indexOf(data[i].secondparameter4Name) != -1) {
                                                    xx[dataReport[0].parameter4] = self.model[data[i].secondparameter4Name]();
                                                } else {
                                                    xx[dataReport[0].parameter4] = app.personDetails()[data[i].secondparameter4Name];
                                                }

                                            }
                                            if (dataReport.parameter5) {
                                                if (Object.keys(self.model).indexOf(data[i].secondparameter5Name) != -1) {
                                                    xx[dataReport[0].parameter5] = self.model[data[i].secondparameter5Name]();
                                                } else {
                                                    xx[dataReport[0].parameter5] = app.personDetails()[data[i].secondparameter5Name];
                                                }

                                            }
                                            services.getGenericReport(xx).then(getReportCBCF, app.failCbFn);
                                        };
                                        services.getGeneric(commonhelper.reportPram + reportCallModel.reportName).then(reportPramCbFn, failCbFn);
                                    } else if (data[i].secondsaaSSource == "Query") {
                                        var str = data[i].secondsaaSQuery;
                                        var flex = ":$FLEX$.";
                                        var strWitoutFlex = str.substr((str.indexOf("$FLEX$.") + flex.length - 1));
                                        var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf('#'));
                                        var is_HaveFlex = true;
                                        if (str.indexOf(':$FLEX$.') == -1) {
                                            is_HaveFlex = false;
                                        }

                                        while (is_HaveFlex) {
                                            var strWitoutFlex = str.substr((str.indexOf("$FLEX$.") + flex.length - 1));
                                            var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf('#'));
                                            str = str.replace(':$FLEX$.' + ModelName + '#', self.model[ModelName]());
                                            if (str.indexOf(":$FLEX$.") == -1) {
                                                is_HaveFlex = false;
                                            }
                                        }
                                        if (str.substr('undefined ') != -1) {
                                        }

                                        var getReportValidationCBCF = function (dataSaaSQuery) {
                                            var resultOfQueryObj = dataSaaSQuery;
                                            sucendValue = resultOfQueryObj.value;
                                        };
                                        var validationQueryReportModel = {"reportName": 'DynamicValidationReport', "str": str};
                                        services.getGenericReport(validationQueryReportModel).then(getReportValidationCBCF, app.failCbFn);
                                    }
                                }
                                if (data[i].typeOFAction == "Error_Massage") {
                                    if (app.compare(firstValue, operation, sucendValue)) {
                                        self.validationCheck(true);
                                        isErrMsg = true;
                                        if (app.getLocale() == 'ar') {
                                            self.validationMessagesArr.push(self.createMessage(data[i].errorMassageAr));
                                        } else {
                                            self.validationMessagesArr.push(self.createMessage(data[i].errorMassageEn));
                                        }
                                    } else {
                                        self.validationMessagesArr([]);
                                        self.validationCheck(false);
                                    }
                                } else {
                                    isErrMsg = false;
                                }

                                if (data[i].saveResultToValidationsValues == "Yes") {
                                    app.validation[data[i].validationValuesName] = app.compare(firstValue, operation, sucendValue);
                                }


                            }
                        } else {
                            self.deptObservableArray([]);
                        }
                    };
                    var failCbFn = function () {
                    };
                    if (self.personNumberArr().length < 1) {
                        self.validationMessagesArr.push(self.createMessage(self.bulkTableEmpty()));
                        return;
                    }else{
                        self.validationMessagesArr([]);
                    }
                    var service = "validation/search";
                    services.searcheGeneric(service, self.validationSearch).then(EITCodeCbFn, failCbFn);
                    if (self.validationCheck()) {
                        return;
                    }
                    //self.model[Keys[z]]()          
                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid") {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        return;
                    }
                    if (isErrMsg) {
                        return;
                    }
                    var next = document.getElementById("train").getNextSelectableStep();
                    if (next != null)
                        self.currentStepValue(next);

                    self.disableSubmit2(true);
                    self.disableDeleteBtn(true);
                };
                self.setToDisabeld = function () {
                    for (var i = 0; i < Object.keys(self.isDisabledx).length; i++) {
                        self.isDisabledx[Object.keys(self.isDisabledx)[i]](true);
                    }
                };
                self.setToDefultDisabeld = function () {
                    if (typeof tempObject !== 'undefined') {
                        for (var i = 0; i < tempObject.length; i++) {
                            if (tempObject[i].READ_ONLY_FLAG == "Y") {
                                self.isDisabledx[Object.keys(self.isDisabledx)[i]](true);
                            } else {
                                self.isDisabledx[Object.keys(self.isDisabledx)[i]](false);
                            }
                        }
                    }
                };
                self.currentStepValueText = function () {
                    if (self.currentStepValue() == 'stp2') {
                        self.isDisabled(true);
                        self.addBtnVisible(true);
                        self.nextBtnVisible(false);
                        self.setToDisabeld();

                    } else {
                        self.isDisabled(false);
                        self.addBtnVisible(false);
                        self.nextBtnVisible(true);
                        if (self.pageMode() != "VIEW") {
                            self.setToDefultDisabeld();
                        }

                    }

                    return self.eit_n();
                };
                //------------------End Of Section -------------------------------------------------
                //------------------This Section For Dialog Action (Open ,Close)
                self.submitButton = function () {
                    document.querySelector("#yesNoDialog").open();
                };
                self.cancelButton = function () {
                    document.querySelector("#yesNoDialog").close();
                };

                self.submitButton2 = function () {
                    document.querySelector("#yesNoDialog2").open();
                };
                self.cancelButton2 = function () {
                    document.querySelector("#yesNoDialog2").close();
                };
                self.submitButton3 = function () {
                    document.querySelector("#yesNoDialog3").open();
                };
                self.cancelButton3 = function () {
                    document.querySelector("#yesNoDialog3").close();
                };
                //------------------End Of Section ----------------------------------------------
                //-------------This Function To Stop Train Action If Action Are Not Valid -------
                self.stopSelectListener = function (event, ui) {
                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        event.preventDefault();
                        return;
                    }
                };
                //-------------------End Of Function ---------------------------------------------
                //------------------ This Section For Call Web Service For Insert Or Update ---------

                self.tableSelectionListener = function (event) {
                    var data = event.detail;
                    var currentRow = data.currentRow;
                    if (currentRow !== null && currentRow['rowKey']) {
                        self.disableDeleteBtn(false);
                    } else {
                        self.disableDeleteBtn(true);
                    }
                };

                //used to remove the selected row
                self.removeRow = function () {
                    var element = document.getElementById('tbl12');
                    var currentRow = element.currentRow;

                    if (currentRow != null) {
                        self.personNumberArr.splice(currentRow['rowIndex'], 1);
                    }
                };

                //----------This Function To Set Url Call 
                self.setUrl = function (EitCode) {
                    if (EitCode == "XXX_ABS_CREATE_DECREE") {
                        //self.hidden("block");
                        return rootViewModel.personDetails().absenceRequestUrl;
                    } else if (EitCode == "XXX_ABS_UPDATE_DECREE") {
                        return rootViewModel.personDetails().absenceUpdateRequestUrl;
                    } else if (EitCode == "XXX_ABS_DELETE_DECREE") {
                        return rootViewModel.personDetails().absenceCancelRequestUrl;
                    } else if (EitCode == "XXX_EMP_MINISTRY_JOB") {
                        return rootViewModel.personDetails().ministryJobForEmployee;
                    } else if (EitCode == "XXX_HR_BUSINESS_PAYMENT") {
                        return rootViewModel.personDetails().businessMissionPaymentUrl;
                    } else if (EitCode == "XXX_HR_BUSINESS_MISSION") {
                        return rootViewModel.personDetails().businessMissionRequestUrl;
                    }
                };
                
                function oprationServiceRecord() {

                    var bulkTransactionArr = [];
                    var jsonData = ko.toJSON(self.model);

                    self.approvlModel.created_by = rootViewModel.personDetails().personId;
                    self.approvlModel.creation_date = rootViewModel.personDetails().personId;
                    self.approvlModel.line_manager = rootViewModel.personDetails().managerId;
                    self.approvlModel.manage_of_manager = rootViewModel.personDetails().managerOfManager;
                    self.approvlModel.eit_name = self.eit_n();
                    self.approvlModel.person_id = rootViewModel.personDetails().personId;

                    self.model.url = self.setUrl(self.EitCode);
                    self.approvlModel.code = self.EitCode;
                    if (self.getStatus() == "draft") {
                        self.approvlModel.status = "DRAFT";
                    } else {
                        self.approvlModel.status = "PENDING_APPROVED";
                    }


                    for (var eitModelIndex = 0; eitModelIndex < tempObject.length; eitModelIndex++) {
                        self.approvlModel[tempObject[eitModelIndex].APPLICATION_COLUMN_NAME] = self.model[tempObject[eitModelIndex].DESCRIPTION]();
                    }

                    var objARR = {};
                    for (var indexOfPersonId = 0; indexOfPersonId < self.personNumberArr().length; indexOfPersonId++) {
//                       self.approvlModel.person_id = rootViewModel.personDetails().personId;
                        var bulkOBj = {};
                        bulkOBj.code = self.EitCode;
                        self.approvlModel.person_number = self.personNumberArr()[indexOfPersonId].personNumber;
                        bulkOBj.person_number = self.personNumberArr()[indexOfPersonId].personNumber;
//                         self.approvlModel.person_id = rootViewModel.personDetails().personId;
                        bulkOBj.created_by = rootViewModel.personDetails().personId;
                        bulkOBj.creation_date = rootViewModel.personDetails().personId;
                        bulkOBj.line_manager = rootViewModel.personDetails().managerId;
                        bulkOBj.manage_of_manager = rootViewModel.personDetails().managerOfManager;
                        bulkOBj.url = self.setUrl(self.EitCode);
                        bulkOBj.reference_num = self.refNumberValue();
                        //self.personNumberArr()[indexOfPersonId][tempObject[indexOfPersonId].DESCRIPTION] = searchArrayValue(self.personNumberArr()[indexOfPersonId][tempObject[indexOfPersonId].DESCRIPTION], self.Arrs[tempObject[indexOfPersonId].FLEX_value_SET_NAME + tempObject[indexOfPersonId].DESCRIPTION + "Arr"]());
                        //self.personNumberArr()[indexOfPersonId].overtimeType = searchArrayValue(self.personNumberArr()[indexOfPersonId].overtimeType, self.Arrs['XXMOE_OVETIME_TYPEovertimeTypeArr']());
                        bulkOBj.eit = JSON.stringify(self.personNumberArr()[indexOfPersonId]);
                        bulkOBj.eit_name = self.eit_n();
                        bulkOBj.person_id = self.personNumberArr()[indexOfPersonId].personNumber;
                        self.approvlModel.eit = ko.toJSON(self.personNumberArr()[indexOfPersonId]);
                        bulkOBj = Object.assign(bulkOBj, self.approvlModel);
                        objARR["bulkOBj" + indexOfPersonId] = bulkOBj;
                        bulkTransactionArr.push(objARR["bulkOBj" + indexOfPersonId]);
                    }


                    if (self.pageMode() == "EDIT" || self.pageMode() == "draft") {
                        self.approvlModel.id = self.eit_id();
                    }
                    jsonData.id = self.eit_id();
                    var jsonData = ko.toJSON(self.bulkTransactionArr);


                    var getValidGradeCBF = function (data) {
                        var attchArr = [];
                        for (var i = 0; i < count; i++) {
                            var preview = document.querySelector('.attClass' + i);
                            if (preview !== null) {
                                self.image = ko.observable(preview.src);
                                if (self.pageMode() == "EDIT") {
                                    att = {"SS_id": self.eit_id(), "attachment": self.image()}
                                } else {
                                    att = {"SS_id": data.id, "attachment": self.image()}
                                }

                                attchArr.push(att);

                            }
                        }

                        attJson = ko.toJSON(attchArr);

                        var addAttachedCbfn = function (data2) {
                            var Data = data;

                            self.disableSubmit(false);
                            if (oj.Router.rootInstance._navHistory.length > 1) {
                                app.loading(false);
                                oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                            } else {
                                app.loading(false);
                                oj.Router.rootInstance.go('home');
                            }

                        };


                        services.addGeneric("attachment/add/", attJson).then(addAttachedCbfn, app.failCbFn);

                        count = 0;

                    };
                    services.addGeneric("bulkTransaction/createBulkTransactionPersonId/BULK", ko.toJSON(bulkTransactionArr)).then(getValidGradeCBF, app.failCbFn);
                }
                
                self.commitRecord = function (data, event) {
                    if (!self.disableSubmit()) {
                        app.loading(true);
                        self.disableSubmit(true);
                        oprationServiceRecord();
                        self.getStatus("PENDING_APPROVED");
                        return true;
                    }
                };

                self.commitRecord2 = function (data, event) {
                    if (!self.disableSubmit()) {
                        self.getStatus("draft");
                        app.loading(true);
                        self.disableSubmit(true);
                        oprationServiceRecord();

                        return true;
                    }
                };

                self.commitRecord3 = function (data, event) {
                    if (!self.disableSubmit()) {
                        self.pageMode("draft");
                        self.getStatus("PENDING_APPROVED");
                        app.loading(true);
                        self.disableSubmit(true);
                        oprationServiceRecord();

                        return true;
                    }
                };
                //-------------------End Of Section ------------------------------------------------
                //-------------------Leave Page -----------------------------
                self.handleDetached = function (info) {
                    oj.Router.rootInstance.store(self.EitCode);
                    self.isDisabledx = {};
                    self.model = {};
                };
                //-------------------- view approvals ---------------------------
                self.approvalType = ko.observableArray([]);
                self.datacrad = ko.observableArray([]);
                self.roleName = ko.observableArray([]);
                self.workglow_Approval = function () {
                    self.datacrad([]);
                    var eitcode = self.EitCode;

                    var getValidGradeCBF = function (data) {
                        if (data) {

                            for (var i = 0; i < data.length; i++) {
                                self.approvalType.push(data[i].approvalType);
                                self.roleName.push(data[i].roleName);

                                var notificationStatus, FYAValue, approvalTypeValue;
                                if (data[i].notificationType === "FYA") {
                                    notificationStatus = 'app-type-a';
                                    FYAValue = "لاجل الموافقة عليها";
                                } else {

                                    notificationStatus = 'app-type-i';
                                    FYAValue = "لاجل العلم فقط";
                                }
                                if (data[i].approvalType === "EMP") {
                                    approvalTypeValue = "موظف";
                                } else if (data[i].approvalType === "LINE_MANAGER") {
                                    approvalTypeValue = "المدير المباشر";
                                } else if (data[i].approvalType === "LINE_MANAGER+1") {
                                    approvalTypeValue = "المدير الثانى";
                                }

                                self.datacrad.push({
                                    approvalType: approvalTypeValue,
                                    roleType: data[i].roleName,
                                    FYA: FYAValue,
                                    status: notificationStatus
                                });

                            }
                        }
                    };
                    self.lbl = ko.observable(self.approvaltype());
                    services.getGeneric(commonhelper.approvalSetupUrl + eitcode).then(getValidGradeCBF, app.failCbFn);
                    document.querySelector("#modalDialog1").open();
                };

                self.closeDialog = function () {
                    $("#modalDialog1").ojDialog("close");
                };

                //---------------------Translate Section ----------------------------------------

                var getTranslation = oj.Translations.getTranslatedString;
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        self.stepArray([{label: self.create(), id: 'stp1'},
                            {label: self.review(), id: 'stp2'}]);
                        initTranslations();
                    }
                });
                function initTranslations() {
                    self.DepartmentOrPersonNumberArr(app.getPaaSLookup('Department_Or_PersonNumber'));
                    self.create(getTranslation("labels.create"));
                    self.review(getTranslation("others.review"));
                    self.structureNameLbl(getTranslation("additionalDetails.structureName"));
                    self.backLbl(getTranslation("others.back"));
                    self.nextLbl(getTranslation("others.next"));
                    self.submitLbl(getTranslation("others.submit"));
                    self.approvalrole(getTranslation("labels.approvalrole"));
                    self.approvaltype(getTranslation("labels.approvaltype"));
                    self.savedraft(getTranslation("labels.saveDraft"));
                    self.aproval(getTranslation("labels.aproval"));
                    self.attachment(getTranslation("others.attachment"));
                    self.submitdraftLbl(getTranslation("labels.submitdraft"));
                    self.ok(getTranslation("others.ok"));
                    self.addLbl(getTranslation("others.add"));
                    self.editLbl(getTranslation("others.edit"));
                    self.validGradeLbl(getTranslation("others.validGrade"));
                    self.gradeLbl(getTranslation("job.grade"));
                    self.percentageLbl(getTranslation("position.percentage"));
                    self.cancel(getTranslation("others.cancel"));
                    self.yes(getTranslation("others.yes"));
                    self.no(getTranslation("others.no"));
                    self.oprationMessage(getTranslation("others.addGe"));
                    self.departmentorPersonNumber(getTranslation("others.selctdeptOrPersonNumber"));
                    self.department(getTranslation("others.department"));
                    self.refNumber(getTranslation("others.refNumber"));
                    self.personNumber(getTranslation("others.personNumber"));
                    self.sequencetable(getTranslation("others.sequencetable"));
                    self.btnAddLbl(getTranslation("others.btnAdd"));
                    self.btnRestLbl(getTranslation("others.btnDeleteAll"));
                    self.btnDeleteLbl(getTranslation("others.delete"));
                    self.personName(getTranslation("others.personName"));
                    self.bulkTableEmpty(getTranslation("validation.bulkTableEmpty"));
                    self.personNumberErr(getTranslation("validation.personNumberErr"));

                    self.stepArray([{label: self.create(), id: 'stp1'},
                        {label: self.review(), id: 'stp2'}]);
                    self.columnArray1([

                        {
                            "headerText": self.approvaltype(), "field": "approvalType"
                        },
                        {
                            "headerText": self.approvalrole(), "field": "roleName"
                        }
                    ]);
                    self.columnArray([
                        {
                            "headerText": self.sequencetable(), "field": "seq"
                        },
                        {
                            "headerText": self.personNumber(), "field": "personId"
                        },
                        {
                            "headerText": self.personName(), "field": "personName"
                        }
                    ]);

                }
                initTranslations();

            }

            return new dynamicOperationScreenViewModel();
        }
);
