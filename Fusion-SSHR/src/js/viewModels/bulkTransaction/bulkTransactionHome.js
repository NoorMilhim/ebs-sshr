define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'ojs/ojtrain', 'config/services',
    'util/commonhelper', 'knockout-mapping', 'ojs/ojmodel', 'ojs/ojknockout',
    'ojs/ojknockout-model', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojselectcombobox',
    'ojs/ojdatetimepicker', 'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup',
    'ojs/ojknockout-validation', 'ojs/ojfilmstrip', 'ojs/ojpagingcontrol', 'ojs/ojcheckboxset',
    'ojs/ojradioset', 'ojs/ojlabel', 'jquery-calendar', 'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin', 'jquery-calendar-picker',
    'ojs/ojinputtext'],
        function (oj, ko, $, app, ojtrain, services, commonhelper, km) {

            function homeViewModel() {

                var self = this;
                self.valueofsharethouts = ko.observable("Share thoughts with colleagues..");
                self.agreement = ko.observableArray();
                self.EitName = ko.observableArray();
                self.currentColor = ko.observable("red");
                var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
                var getTranslation = oj.Translations.getTranslatedString;
                self.nam = ko.observable();
                self.jobLbl = ko.observable();
                self.positionLbl = ko.observable();
                self.notificationlbl = ko.observable();
                self.BusinessMissionRequestLbl = ko.observable();
                self.overtimeRequestLbl = ko.observable();


                self.error = function () {
                    app.hasErrorOccured(true);
                    app.router.go("error");
                };

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });
                self.icons = ko.observableArray([]);

                self.computeIconsArray = ko.computed(function () {
                    if (rootViewModel.personDetails()) {
                        self.icons([]);
//                        self.icons([
////                            {
////                                label: 'b', value: 'demo-icon-circle-b',router:'dynamicSummaryBulkTransaction', iconType:app.getEITIcon("XXX_HR_BUSINESS_MISSION")[0].icons,text:self.BusinessMissionRequestLbl(),visible:true ,code:"XXX_HR_BUSINESS_MISSION"
////                            },
////                            {
////                                label: 'c', value: 'demo-icon-circle-c', router: 'dynamicSummaryBulkTransaction', iconType: app.getEITIcons("XXX_HR_BUSINESS_MISSION"), text: self.overtimeRequestLbl(), visible: true, code: "XXX_OVERTIME_REQUEST"
////                            }
//                        ]);
                          for(var i=0;i<self.EitName().length;i++){
                                var IconBGSetG = ['_RBGCG_0','_RBGCG_1','_RBGCG_2','_RBGCG_3','_RBGCG_4','_RBGCG_5','_RBGCG_6','_RBGCG_7','_RBGCG_8','_RBGCG_9'];
                                 var choosenColor = IconBGSetG[Math.floor((Math.random() * 9) + 1)];
                              self.icons.push(
                                     {label: 'c',
                                 value: 'demo-icon-circle-c',
                                 router:'dynamicSummaryBulkTransaction',
                                 iconType: app.getEITIcons(self.EitName()[i].value),
                                 text:self.EitName()[i].label,
                                 visible:true ,
                                 IconVarBG: choosenColor,
                                 code:self.EitName()[i].value},
                             
                              );
                             
                           }

                    }
                });

                self.pagingModel = null;

                self.iconNavigation = function (router, code) {
                    app.loading(true);
                    oj.Router.rootInstance.store(code);
                    oj.Router.rootInstance.go(router);
                    return true;
                };

                getPagingModel = function () {
                    if (!self.pagingModel) {
                        var filmStrip = $("#filmStrip");
                        var pagingModel = filmStrip.ojFilmStrip("getPagingModel");
                        self.pagingModel = pagingModel;
                    }
                    return self.pagingModel;
                };

                self.handleAttached = function (info) {
                    app.loading(false);
                    self.EitName(app.globalEitNameReport());
                    initTranslations();

                };

                function initTranslations() {
                    // function to add translations for the home page module
                    self.nam(getTranslation("common.name"));
                    self.jobLbl(getTranslation("pages.job"));
                    self.positionLbl(getTranslation("pages.position"));
                    self.notificationlbl(getTranslation("notification.notificationTile"));
                    self.BusinessMissionRequestLbl(getTranslation("pages.BusinessMissionRequest"));
                    self.overtimeRequestLbl(getTranslation("pages.overtimeRequest"));
                }

                return true;
            }

            return new homeViewModel();
        });
