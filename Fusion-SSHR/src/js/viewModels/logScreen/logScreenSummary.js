define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojarraydataprovider', 'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojtable',
    'ojs/ojlabel', 'ojs/ojvalidationgroup', 'ojs/ojformlayout'],
    (oj, ko, $, app, commonUtil, services, ArrayDataProvider) => {

        function logScreenSummaryViewModel() {
            var self = this;

            self.title = ko.observable();
            self.eitNameLbl = ko.observable();
            self.searchLbl = ko.observable();
            self.backLbl = ko.observable();
            self.addLbl = ko.observable();
            self.eitNameVal = ko.observable();
            self.eitNameOpts = ko.observableArray([]);
            self.searchDisable = ko.observable(false);
            self.addDisable = ko.observable(true);
            self.statusUpdate = ko.observable();

            self.selectedIndex = ko.observable();
            self.current = ko.observable();

            self.columnArray = ko.observableArray();
            self.summaryObservableArray = ko.observableArray([]);
            self.summaryObservableArrayTemp = ko.observableArray([]);

            self.dataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.summaryObservableArray, { idAttribute: 'id' }));

            ko.computed(_ => {
                self.searchDisable(true);
                if (self.eitNameVal()) {
                    self.searchLbl(app.translate("others.search"));
                    self.searchDisable(false);
                } else {
                    if (self.summaryObservableArrayTemp().length == self.summaryObservableArray().length)
                        self.searchLbl(app.translate("others.search"));
                    else {
                        self.searchLbl(app.translate('common.showAll'));
                        self.searchDisable(false);
                    }
                }
            });



            self.resendBtnAction = function () {
                app.loading(true);
                var payload = {
                    "URL": self.summaryObservableArray()[self.selectedIndex()].url
                    ,
                    "payload": self.summaryObservableArray()[self.selectedIndex()].payload,
                    "eitCode": self.summaryObservableArray()[self.selectedIndex()].eitCode,
                    "eitName": self.summaryObservableArray()[self.selectedIndex()].eitName,
                    "requestId": self.summaryObservableArray()[self.selectedIndex()].requestId

                };
                //console.log(payload);


                var ResendReqCbFn = function (data) {
                    app.loading(false);
                    setData(data);
                    //console.log("Dooooooooooone!")
                };

                var serviceName = "eff/resend/";
                services.addGeneric(serviceName, payload).then(ResendReqCbFn, app.failCbFn);
            };


            self.searchBtnAction = function () {

                app.loading(true);
                self.summaryObservableArray([])
                setTimeout(() => {
                    self.summaryObservableArray(self.summaryObservableArrayTemp().filter(e => {
                        return self.eitNameVal() && e.eitName ? e.eitName.toLowerCase().includes(self.eitNameVal().toLowerCase()) : true
                    }));
                    app.loading(false);
                }, 100);

            };

            self.backAction = function () {
                oj.Router.rootInstance.go('setup');
            };


            self.tableSelectionListener = function (e) {
                var data = e.detail;
                var currentRow = data.currentRow;
                // console.log(currentRow);

                if (currentRow != null) {
                    self.selectedIndex(currentRow['rowIndex']);
                }


                var checkArray = self.summaryObservableArray()[self.selectedIndex()];
                if (checkArray) {
                    //                console.log(checkArray);

                    //                console.log(self.selectedIndex());


                    self.statusUpdate(checkArray.eitResponseCode);


                    if (currentRow) {
                        var rowIndex = currentRow.rowIndex;
                        var selectedRow = self.summaryObservableArray()[rowIndex];
                        // console.log(selectedRow.eitResponseCode);
                        if (selectedRow.eitResponseCode >= 400) //status Condition
                            self.addDisable(false);
                        else
                            self.addDisable(true);


                        //console.log(self.addDisable())
                    }
                }

            }


            self.getData = function () {
                self.summaryObservableArray([]);

                var success = function (data) {
                    setData(data);
                };
                var failure = function () {
                    app.loading(false);
                };
                app.loading(true);
                services.getGeneric("loggingRest").then(success, failure);
            };

            function setData(data) {
                //                  console.log(data)
                try {
                    data = JSON.parse(data)
                } catch (e) {
                }
                app.loading(false);
                if (data.length !== 0) {
                    self.summaryObservableArray(data);
                    self.summaryObservableArrayTemp(data);
                    arr = data.map(e => {
                        return { label: e.eitName, value: e.eitName }
                    });
                    var uniqueArr = [];
                    arr.forEach(element => {
                        if (!uniqueArr.find(e => e.label == element.label))
                            uniqueArr.push(element);
                    });
                    self.eitNameOpts(uniqueArr);
                }
            }
            self.handleAttached = function (info) {
                app.loading(false);
                self.getData();
            };

            function initTranslations() {
                self.title(app.translate("pages.logScreenSummary"));
                self.eitNameLbl(app.translate("logscreen.EITNAME"));
                self.searchLbl(app.translate("others.search"));
                self.backLbl(app.translate("labels.backlbl"));
                self.addLbl(app.translate("labels.addLbl"));

                self.columnArray([
                    // { "headerText": "ID", "field": "id" },
                    { "headerText": app.translate("logscreen.PROJECTNAME"), "field": "projectName" },
                    { "headerText": app.translate("logscreen.EITCODE"), "field": "eitCode" },
                    { "headerText": app.translate("logscreen.EITNAME"), "field": "eitName" },
                    { "headerText": app.translate("logscreen.EIT_RESPONSE_CODE"), "field": "eitResponseCode" },
                    { "headerText": app.translate("logscreen.EIT_RESPONSE_DESC"), "field": "eitResponseDesc" },
                    { "headerText": app.translate("logscreen.EIT_RESPONSE_DATE"), "field": "eitResponseDate" },
                    { "headerText": app.translate("logscreen.ELEMNT_RESPONSE_CODE"), "field": "elementResponseCode" },
                    { "headerText": app.translate("logscreen.ELEMNT_RESPONSE_DESC"), "field": "elementResponseDesc" },
                    { "headerText": app.translate("logscreen.ELEMNT_RESPONSE_DATE"), "field": "elementResponseDate" },
                    // { "headerText": "REQUESTID", "field": "REQUESTID" },
                ]);
            }
            initTranslations();
        }
        return new logScreenSummaryViewModel();
    });
