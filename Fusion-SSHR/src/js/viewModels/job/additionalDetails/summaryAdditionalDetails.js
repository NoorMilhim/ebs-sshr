/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * homeministryJob module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojknockout', 'ojs/ojarraydataprovider',
    'ojs/ojselectcombobox', 'ojs/ojdialog'],
        function (oj, ko, $, app, services, commonhelper) {

            function summaryAdditionalDetails() {
                var self = this;
                self.columnArray = ko.observableArray();
                self.placeholder = ko.observable("Please Select Value");
                self.dataSourceTB2 = ko.observable();
                var deptArray = [];
                self.selctedStruct = ko.observable();
                self.isRequired = ko.observable(true);
                self.oprationdisabled = ko.observable(true);
                self.jobNam = ko.observable();
                self.jName = ko.observable();
                self.selectedRowKey = ko.observable();
                self.seqaunce = ko.observable();
                self.selectedIndex = ko.observable();
                self.nextPage = ko.observable();
                self.isVisible = ko.observable(false);
                self.deptObservableArray = ko.observableArray(deptArray);
                var Url;
                var jobObj;
                self.supervisorHRCYesNoList = ko.observableArray(app.globalHRCYesNo());
                self.qualificationNameList = ko.observableArray(app.globalQualifications());
                self.gradesList = ko.observableArray(app.globalHrGrades());
                self.dataprovider = new oj.ArrayDataProvider(self.deptObservableArray, {idAttribute: 'DepartmentId'});
                function clearContent() {

                }
                self.structureName = ko.observable([
                    {value: "Supervisor_Allowance", label: "Supervisor Allowance"},
                    {value: "Minimum_Required_Education_Level", label: "Minimum Required Education Level"},
                    {value: "Security_Allowance", label: "Security Allowance"},
                    {value: "Clothes_Allowance", label: "Clothes Allowance"},
                    {value: "Rrequired_Qualifications", label: "Required Qualifications"}

                ]);
                self.tableSelectionListener = function (event) {
                    var data = event.detail;
                    var currentRow = data.currentRow;
                    self.oprationdisabled(false);
                    self.selectedRowKey(currentRow['rowKey']);
                    self.selectedIndex(currentRow['rowIndex']);

                };

                self.addAdditionalDetails = function () {
                    jobObj.type = "ADD";
                    oj.Router.rootInstance.store(jobObj);
                    oj.Router.rootInstance.go(self.nextPage());
                };
                self.updateAdditionalDetails = function () {
                    self.deptObservableArray()[self.selectedIndex()].type = "EDIT";
                    oj.Router.rootInstance.store(self.deptObservableArray()[self.selectedIndex()]);
                    oj.Router.rootInstance.go(self.nextPage());
                };
                self.backAction = function () {
                    app.loading(true);
                    oj.Router.rootInstance.go('searchJob');

                };
                self.handleAttached = function () {
                    app.loading(false);
                    jobObj = oj.Router.rootInstance.retrieve();
                    self.jName(jobObj.seriesCategoryLbl);
                };
                //------------------Option Change Section ----------------------------
                /*
                 * Tyis Function Load Date From Web Servise Dependant On Selected Structure Name 
                 * And Set The Next Page To Direct So When Crate The Your Pape Set The True Value 
                 * in Var self.nextPage 
                 * 
                 */
                self.structureChangedHandler = function (event, data) {
                    var jobName = jobObj.jobName;
                    if (self.selctedStruct() == "Supervisor_Allowance") {
                        self.isVisible(true);

                        Url = commonhelper.supervisorallowanceUrl;
                        jobName = jobObj.jobName;
                        self.nextPage('supervisorAllowanceOprationScreen');
                    } else if (self.selctedStruct() == "Minimum_Required_Education_Level") {
                        self.isVisible(true);

                        Url = commonhelper.requirededuicationlevelUrl;
                        jobName = jobObj.jobName;
                        self.nextPage('minimumRequiredEducationLeveOprationScreen');
                    } else if (self.selctedStruct() == "Security_Allowance") {
                        self.isVisible(true);

                        Url = commonhelper.securityallowanceUrl;
                        jobName = jobObj.jobName;
                        self.nextPage('securityAllowanceOprationScreen');
                    } else if (self.selctedStruct() == "Clothes_Allowance") {
                        self.isVisible(true);
                        Url = commonhelper.clothesallowanceUrl;
                        self.nextPage('clothesAllowanceOprationScreen');
                        jobName = jobObj.jobName;
                    } else if (self.selctedStruct() == "Rrequired_Qualifications") {
                        self.isVisible(true);
                        Url = commonhelper.requiredqualificationUrl;
                        jobName = jobObj.jobName;
                        self.nextPage('requiredQualificationsOprationScreen');
                    }
                    var getValidGradeCBF = function (data) {
                        var Data = data;
                        var count = 0;
                        for (var i = 0; i < data.length; i++) {
                            if (self.selctedStruct() == "Supervisor_Allowance") {
                                Data[i].supervisorLbl = searchArray(data[i].supervisor, self.supervisorHRCYesNoList());
                                data[i].seq = count += 1;
                            } else if (self.selctedStruct() == "Minimum_Required_Education_Level") {//
                                Data[i].requiredEducationLevelLbl = searchArray(data[i].requiredEducationLevel, self.qualificationNameList());
                                data[i].seq = count += 1;
                            } else if (self.selctedStruct() == "Rrequired_Qualifications") {//
                                Data[i].qualificationNameLbl = searchArray(data[i].qualificationName, self.qualificationNameList());
                                Data[i].gradeLbl = searchArray(data[i].grades, self.gradesList());
                                data[i].seq = count += 1;
                            } else if (self.selctedStruct() == "Security_Allowance") {//

                                data[i].seq = count += 1;
                            } else if (self.selctedStruct() == "Clothes_Allowance") {//

                                data[i].seq = count += 1;
                            }

                        }

                        self.deptObservableArray(Data);
                    };
                    services.getGeneric(Url + jobName, {}).then(getValidGradeCBF, self.failCbFn);
                    self.buildTable();
                };
                //------------------------End Of Section ----------------------------

                //---------------------------Function For Build Table-----------------------------
                self.buildTable = function () {
                    if (self.selctedStruct() == "Supervisor_Allowance") {
                        self.columnArray([
                            {
                                "headerText": self.seqaunce(), "field": "seq"
                            },
                            {
                                "headerText": self.startDateLbl(), "field": "startDate"
                            },
                            {
                                "headerText": self.endDateLbl(), "field": "endDate"
                            },
                            {
                                "headerText": self.supervisorLbl(), "field": "supervisorLbl"
                            }
                        ]);
                    } else if (self.selctedStruct() == "Minimum_Required_Education_Level") {
                        self.columnArray([
                            {
                                "headerText": self.seqaunce(), "field": "seq"
                            },
                            {
                                "headerText": self.startDateLbl(), "field": "startDate"
                            },
                            {
                                "headerText": self.endDateLbl(), "field": "endDate"
                            },
                            {
                                "headerText": self.qualificationNameLbl(), "field": "requiredEducationLevelLbl"
                            }
                        ]);
                    } else if (self.selctedStruct() == "Security_Allowance") {
                        self.columnArray([
                            {
                                "headerText": self.seqaunce(), "field": "seq"
                            },
                            {
                                "headerText": self.startDateLbl(), "field": "startDate"
                            },
                            {
                                "headerText": self.endDateLbl(), "field": "endDate"
                            }
                        ]);
                    } else if (self.selctedStruct() == "Clothes_Allowance") {
                        self.columnArray([
                            {
                                "headerText": self.seqaunce(), "field": "seq"
                            },
                            {
                                "headerText": self.startDateLbl(), "field": "startDate"
                            },
                            {
                                "headerText": self.endDateLbl(), "field": "endDate"
                            },
                            {
                                "headerText": self.amountLbl(), "field": "amount"
                            }
                        ]);
                    } else if (self.selctedStruct() == "Rrequired_Qualifications") {
                        self.columnArray([
                            {
                                "headerText": self.seqaunce(), "field": "seq"
                            },
                            {
                                "headerText": self.startDateLbl(), "field": "startDate"
                            },
                            {
                                "headerText": self.endDateLbl(), "field": "endDate"
                            },
                            {
                                "headerText": self.gradeLbl(), "field": "gradeLbl"
                            },
                            {
                                "headerText": self.qualificationNameLbl(), "field": "qualificationNameLbl"
                            },
                            {
                                "headerText": self.experienceYearsLbl(), "field": "experienceYears"
                            }
                        ]);
                    }

                };
                //---------------------------End Of Function ---------------------------------
                //--------------------------History Section --------------------------------- 
                self.historyDetails = ko.observable();
                self.closeDialog = function () {
                    $("#modalDialog1").ojDialog("close");
                };
                self.openDialog = function () {
                    var positionHistoryCBF = function (data) {
                        var Data = data;
                        var count = 0;
                        for (var i = 0; i < data.length; i++) {
                            if (self.selctedStruct() == "Supervisor_Allowance") {
                                Data[i].supervisorLbl = searchArray(data[i].supervisor, self.supervisorHRCYesNoList());
                                data[i].seq = count += 1;
                            } else if (self.selctedStruct() == "Minimum_Required_Education_Level") {//
                                Data[i].requiredEducationLevelLbl = searchArray(data[i].requiredEducationLevel, self.qualificationNameList());
                                data[i].seq = count += 1;
                            } else if (self.selctedStruct() == "Rrequired_Qualifications") {//
                                Data[i].qualificationNameLbl = searchArray(data[i].qualificationName, self.qualificationNameList());
                                Data[i].gradeLbl = searchArray(data[i].grades, self.gradesList());
                                data[i].seq = count += 1;
                            } else if (self.selctedStruct() == "Security_Allowance") {//

                                data[i].seq = count += 1;
                            } else if (self.selctedStruct() == "Clothes_Allowance") {//

                                data[i].seq = count += 1;
                            }
                        }
                        self.dataSourceTB2(new oj.ArrayTableDataSource(Data));
                    };
                    var code = self.deptObservableArray()[self.selectedIndex()].code;

                    services.getGeneric(Url + 'search/' + code, {}).then(positionHistoryCBF, app.failCbFn);
                    document.querySelector("#modalDialog1").open();
                };


                //--------------------------End Of Section ----------------------------------
                //---------------------Translate Section ----------------------------------------
                self.ok = ko.observable();
                self.startDateLbl = ko.observable();
                self.endDateLbl = ko.observable();
                self.addLbl = ko.observable();
                self.editLbl = ko.observable();
                self.gradeLbl = ko.observable();
                self.validGradeLbl = ko.observable();
                self.structureNameLbl = ko.observable();
                self.backLbl = ko.observable();
                self.placeholder = ko.observable("Please Select Value");
                self.supervisorLbl = ko.observable();
                self.qualificationNameLbl = ko.observable();
                self.experienceYearsLbl = ko.observable();
                self.amountLbl = ko.observable();
                self.historyLbl = ko.observable();
                self.addtionalDetails = ko.observable();
                var getTranslation = oj.Translations.getTranslatedString;
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();

                    }

                });
                function initTranslations() {
                    self.historyLbl(getTranslation("others.history"));
                    self.structureNameLbl(getTranslation("additionalDetails.structureName"));
                    self.backLbl(getTranslation("others.back"));
                    self.ok(getTranslation("others.ok"));
                    self.addLbl(getTranslation("others.add"));
                    self.jobNam(getTranslation("job.jobName"));
                    self.historyDetails(getTranslation("job.historyDetails"));
                    self.editLbl(getTranslation("others.edit"));
                    self.validGradeLbl(getTranslation("others.validGrade"));
                    self.startDateLbl(getTranslation("job.startDate"));
                    self.endDateLbl(getTranslation("job.endDate"));
                    self.seqaunce(getTranslation("labels.Sequence"));
                    self.gradeLbl(getTranslation("additionalDetails.grades"));
                    self.supervisorLbl(getTranslation("additionalDetails.Supervisor"));
                    self.qualificationNameLbl(getTranslation("additionalDetails.qualificationName"));
                    self.amountLbl(getTranslation("additionalDetails.amount"));
                    self.experienceYearsLbl(getTranslation("additionalDetails.experienceYears"));
                    self.addtionalDetails(getTranslation("job.additionalDetails"));


                    self.columnArray([
                        {
                            "headerText": self.seqaunce(), "field": "seq"
                        },
                        {
                            "headerText": self.gradeLbl(), "field": "grade"
                        },
                        {
                            "headerText": self.startDateLbl(), "field": "startDate"
                        },
                        {
                            "headerText": self.endDateLbl(), "field": "endDate"
                        }
                    ]);
                }
                initTranslations();




            }

            return summaryAdditionalDetails;
        });
