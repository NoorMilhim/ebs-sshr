/**
 * 
 * @param {type} oj
 * @param {type} ko
 * @param {type} $
 * @param {type} app
 * @param {type} commonUtil
 * @param {type} 
 * 
 * @param {type} translatehelper
 * @returns {JobL#13.JobContentViewModel}
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojknockout', 'ojs/ojtrain', 'ojs/ojbutton',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker',
    'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup', 'ojs/ojcheckboxset', 'ojs/ojradioset',
    'ojs/ojlabel', 'ojs/ojgauge', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojmessage', 'jquery-calendar',
    'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin',
    'ojs/ojtimezonedata', 'ojs/ojdatetimepicker', 'ojs/ojselectcombobox',
    'jquery-calendar-picker', 'ojs/ojvalidationgroup'],
        function (oj, ko, $, app, commonUtil, services) {
            /**
             * The view model for the main content view template
             */
            function JobContentViewModel() {
                var self = this;
                var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
                self.isHaveFuture = ko.observable();
                self.dateMessage = ko.observable();
                self.errorAdd = ko.observable();
                self.successEdit = ko.observable();
                self.currentStepValue = ko.observable('stp1');
                self.returnToPerviousPage = ko.observable();
                self.confirmationCancelPage = ko.observable();
                self.checkProcessSucessOrNot = ko.observable();
                self.currentStepValueText = function () {


                    if (self.currentStepValue() === 'stp2' && self.serviceType() != 'VIEW') {
                        self.isDisabled(true);
                        self.addBtnVisible(true);
                        self.nextBtnVisible(false);
                        $('#datePickerStartDate').calendarsPicker('disable');
                        $('#datePickerEndDate').calendarsPicker('disable');
                    } else if (self.serviceType() == 'VIEW') {
                        self.nextBtnVisible(false);
                        self.addBtnVisible(false);
                        self.isDisabled(true);
                        $('#datePickerStartDate').calendarsPicker('disable');
                        $('#datePickerEndDate').calendarsPicker('disable');
                    } else if (self.serviceType() == 'CORRECTION') {
                        $('#datePickerStartDate').calendarsPicker('disable');
                        $('#datePickerEndDate').calendarsPicker('disable');
                    } else {
                        self.isDisabled(false);
                        self.addBtnVisible(false);
                        self.nextBtnVisible(true);
                        $('#datePickerStartDate').calendarsPicker('enable');
                        $('#datePickerEndDate').calendarsPicker('enable');
                    }
                    return self.jobTitel();
                };
                self.previousStep = function () {
                    var prev = document.getElementById("train").getPreviousSelectableStep();
                    if (prev !== null)
                        self.currentStepValue(prev);
                };

                //start next validation 
                self.nextStep = function () {
                    var tracker = document.getElementById("tracker");

                    if (tracker.valid === "valid") {

                        //  var dates = $('#' + $('#datePicker').val()).calendarsPicker('getDate'); 
                        function isValidDate(dateString)
                        {

                            // First check for the pattern
                            if (!/^\d{4}\/\d{1,2}\/\d{1,2}$/.test(dateString)) {
                                if (!/^\d{4}\-\d{1,2}\-\d{1,2}$/.test(dateString)) {
                                    $.notify(self.dateMessage());
                                    return false;
                                }
                                return true;
                            }

                            // Parse the date parts to integers
                            var parts = dateString.split("/");
                            var month = parseInt(parts[1], 10);
                            var year = parseInt(parts[0], 10);
                            var day = parseInt(parts[2], 10);


                            // Check the ranges of month and year
                            if (year < 1300 || year > 3000 || month == 0 || month > 12) {
                                $.notify(self.dateMessage());
                                return false;
                            }

                            return true;


                        }
                        ;
                        function datevalidtion(d, d2) {

                            if (d >= d2) {
                                $.notify(self.dateMessage());
                                return false;
                            }
                            return true;
                        }
                        ;

                        var s1 = self.ministryJobModle.startDate();
                        var d = Date.parse(s1);
                        var s2 = self.ministryJobModle.endDate();
                        var d2 = Date.parse(s2);



                        var x;
                        function authinticate(data) {
                            if (data.count == "0" && (self.serviceType() == "ADD" || self.serviceType() == "EDIT" || self.serviceType() == "Update_Change_Insert" || self.serviceType() == "update_override" || self.serviceType() == "CORRECTION")) {
                                x = true;
                                self.ministryJobModle.jobName(self.ministryJobModle.jobClassificationType() + "." + self.ministryJobModle.generalGroup() + "." + self.ministryJobModle.groupType() + "." + self.ministryJobModle.seriesCategory());
//                    self.ministryJobModle.code(self.ministryJobModle.jobClassificationType() + "." + self.ministryJobModle.generalGroup() + "." + self.ministryJobModle.groupType() + "." + self.ministryJobModle.seriesCategory());
                            } else {
                                x = false;
                            }
                        }
                        function failCbFn(xhr) {
                            $.notify(self.errorAdd());
                        }
                        if (oj.Router.rootInstance.retrieve() === 'add') {
//                self.ministryJobModle.code(self.ministryJobModle.jobClassificationType() + "." + self.ministryJobModle.generalGroup() + "." + self.ministryJobModle.groupType() + "." + self.ministryJobModle.seriesCategory());
                        }

                        if (!self.ministryJobModle.endDate()) {

                            if (isValidDate((s1))) {
                                services.searcheGeneric(commonUtil.jobContUrl, self.ministryJobModle).then(authinticate, failCbFn);

                                if (!x) {
                                    $.notify(self.errorAdd());
                                    return;
                                } else {
                                    var next = document.getElementById("train").getNextSelectableStep();
                                    if (next !== null)
                                        self.currentStepValue(next);
                                }


                            }

                        } else {

                            if (isValidDate(s1) && isValidDate(s2) && datevalidtion(d, d2)) {
                                services.searcheGeneric(commonUtil.jobContUrl, self.ministryJobModle).then(authinticate, failCbFn);

                                if (!x) {
                                    $.notify(self.errorAdd());
                                    return;
                                } else {
                                    var next = document.getElementById("train").getNextSelectableStep();
                                    if (next !== null)
                                        self.currentStepValue(next);
                                }

                            }
                            ;
                        }


                    } else
                    {
                        tracker.showMessages();
                    }

                };

                //end next validation
                self.stopSelectListener = function (event, ui) {
                    var tracker = document.getElementById("tracker");
                    var x;
                    function authinticate(data) {
                        var count = data.count;
                        if (count == "0") {
                            x = true;
                        } else {
                            x = false;
                        }
                    }
                    function failCbFn(xhr) {
                        self.messages([{
                                severity: 'error',
                                summary: 'Error to check Combination between 4 Segments.',
                                detail: '' + xhr,
                                autoTimeout: 0
                            }
                        ]);
                    }
                    if (oj.Router.rootInstance.retrieve() === 'add') {
//                self.ministryJobModle.code(self.ministryJobModle.jobClassificationType() + "." + self.ministryJobModle.generalGroup() + "." + self.ministryJobModle.groupType() + "." + self.ministryJobModle.seriesCategory());
                    }
                    services.searcheGeneric(commonUtil.jobContUrl, self.ministryJobModle).then(authinticate, failCbFn);
                    if (tracker.valid != "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        event.preventDefault();
                        return;
                    } else if (!x) {
                        self.messages([{
                                severity: 'error',
                                summary: 'The Combination between 4 Segments should not be Duplicate.',
                                detail: '',
                                autoTimeout: 0
                            }
                        ]);
                        event.preventDefault();
                        return;
                    }
                };
                self.cancelAction = function () {
                    if (oj.Router.rootInstance._navHistory.length > 1) {
                        oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                    }
                    oj.Router.rootInstance.go('searchJob');

                };
                self.canceld = function () {
                    document.querySelector("#cancelDialog").close();

                };
                self.canceldUpdateCorrection = function () {
                    document.querySelector("#editDialog").close();

                };
                self.oprationServiceRecord = function () {
                    function authinticate(data) {
                        $.notify(self.checkProcessSucessOrNot());
                        self.reset();
                        if (oj.Router.rootInstance._navHistory.length > 1) {
                            oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                        } else {
                            oj.Router.rootInstance.go('home');
                        }
                    }
                    var jsonData = ko.toJSON(self.ministryJobModle);
                    services.jobs(jsonData, self.serviceType()).then(authinticate, self.failCbFn);
                };

                self.commitRecordUpdateOverride = function () {
                    function authinticate(data) {

                        $.notify(self.checkProcessSucessOrNot());
                        self.reset();
                        if (oj.Router.rootInstance._navHistory.length > 1) {
                            oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                        } else {
                            oj.Router.rootInstance.go('home');
                        }
                    }
                    var jsonData = ko.toJSON(self.ministryJobModle);
                    var serviceType = 'UPDATE_OVERRIDE';
                    services.jobs(jsonData, serviceType).then(authinticate, self.failCbFn);
                };
                self.commitRecord = function (data, event) {
                    if (!self.disableSubmit()) {
                        app.loading(true);
                        self.disableSubmit(true);
                        self.oprationServiceRecord();
                        return true;
                    }
                };
                self.submitButton = function () {
                    if (self.isHaveFuture() > 0) {
                        document.querySelector("#editDialog").open();
                    } else {
                        document.querySelector("#yesNoDialog").open();
                    }

                };
                self.cancelDialog = function () {
                    document.querySelector("#cancelDialog").open();

                }
                self.cancelButton = function () {
                    document.querySelector("#yesNoDialog").close();
                };
                self.failCbFn = function (xhr) {
                    self.messages([{
                            severity: 'error',
                            summary: 'Error to ' + oj.Router.rootInstance.retrieve() + ' data',
                            detail: '',
                            autoTimeout: 0
                        }
                    ]);
                };
                self.commitDraft = function (data, event) {
                    return true;
                };
                self.reset = function () {
                    clearContent();
                };
                function clearContent() {
                }

                self.jobCatagoryList = ko.observableArray(app.globalJobCatagory());
                self.generalGroupList = ko.observableArray(app.globalGeneralGroup());
                self.generalTypeList = ko.observableArray(app.globalGroupType());
                self.jobNameList = ko.observableArray(app.globalJobName());
                self.jobsNames = ko.observableArray(app.globalJobsNames());
                self.disableSubmit = ko.observable(false);
                self.endDate = ko.observable();
                self.startDate = ko.observable();
                self.jobType = ko.observable();
                self.generalGroup = ko.observable();
                self.jobTypeGroup = ko.observable();
                self.seriesCategories = ko.observable();
                self.jobName = ko.observable();
                self.addJob = ko.observable();
                self.updateJob = ko.observable();
                self.viewJob = ko.observable();
                self.jobTitel = ko.observable();
                self.trainView = ko.observable();
                self.confirmMessage = ko.observable();
                self.addMessage = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.pervious = ko.observable();
                self.next = ko.observable();
                self.cancel = ko.observable();
                self.submit = ko.observable();
                self.create = ko.observable();
                self.review = ko.observable();
                self.ok = ko.observable();
                self.oprationMessage = ko.observable();
                self.addMassageLbl = ko.observable();
                self.editMassageLbl = ko.observable();
                self.placeholder = ko.observable();
                self.date1 = ko.observable(oj.IntlConverterUtils.dateToLocalIso(new Date()));

                self.serviceType = ko.observable();
                self.ministryJobModle = ko.observable();
                self.groupValid = ko.observable();
                self.jnVisibility = ko.observable();
                self.jnVisibility = ko.observable();
                self.trainVisibility = ko.observable();
                self.tracker = ko.observable();
                self.isDisabled = ko.observable(false);
                self.nextBtnVisible = ko.observable(true);
                self.addBtnVisible = ko.observable(false);
                self.columnArray = ko.observableArray();
                self.isRequired = ko.observable(true);
                self.stepArray = ko.observableArray([]);
                self.messages = ko.observable();
                self.endDate = ko.observable();

                self.formatDate = function (date) {
                    //var date = new Date()
                    var month = '' + (date.getMonth() + 1),
                            day = '' + date.getDate(),
                            year = date.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;

                    return [year, month, day].join('-');
                };





                self.handleAttached = function (info) {
                    app.loading(false);
                    //
                    //Hijri Calendar
                    var calendar = $.calendars.instance();
                    //   var tempDate = calendar.parseDate('mm/dd/yyyy', '10/10/1439');
                    //  tempDate = calendar.formatDate('yyyy/mm/dd', tempDate);
                    $("#datePickerStartDate").val(self.retrieve.startDate);
                    $("#datePickerEndDate").val(self.retrieve.endDate);

                    $("#datePickerEndDate").calendarsPicker({calendar: $.calendars.instance('ummalqura'),
                        onSelect: function (dates) {
                            var endDate = $('#datePickerEndDate').val();
                            self.ministryJobModle.endDate(endDate);
                            $("datePickerEndDate").prop('disabled', true);


                        }});


                    $("#datePickerStartDate").calendarsPicker({calendar: $.calendars.instance('ummalqura'),
                        onSelect: function (dates) {
                            var startDate = $('#datePickerStartDate').val();
                            self.ministryJobModle.startDate(startDate);

                        }});






                    self.retrieve = oj.Router.rootInstance.retrieve();
                    self.isRequired = ko.observable(true);
                    if (self.retrieve === 'add') {
                        self.serviceType('ADD');
                        self.oprationMessage(self.addMassageLbl());
                        self.jnVisibility(false);
                        self.trainVisibility(true);
                        self.ministryJobModle = {
                            jobId: ko.observable(0),
                            jobName: ko.observable(),
                            startDate: ko.observable(oj.IntlConverterUtils.dateToLocalIso(new Date())),
                            endDate: ko.observable(""),

                            jobClassificationType: ko.observable(),
                            generalGroup: ko.observable(),
                            groupType: ko.observable(),
                            seriesCategory: ko.observable(),

                            createdBy: ko.observable(rootViewModel.personDetails().personId),
                            creationDate: ko.observable(self.formatDate(new Date())),
                            lastUpdatedBy: ko.observable(),
                            lastUpdatedDate: ko.observable(),
                            code: ko.observable()
                        };

                    } else if (self.retrieve.type === 'update') {
                        self.serviceType('EDIT');
                        self.oprationMessage(self.editMassageLbl());
                        self.jnVisibility(true);
                        self.trainVisibility(true);
                        self.ministryJobModle = {
                            jobId: ko.observable(self.retrieve.jobId),
                            jobName: ko.observable(self.retrieve.jobName),
                            startDate: ko.observable(self.formatDate(new Date(self.retrieve.startDate))),
                            endDate: ko.observable(self.formatDate(new Date(self.retrieve.endDate))),

                            jobClassificationType: ko.observable(self.retrieve.jobClassificationType),
                            generalGroup: ko.observable(self.retrieve.generalGroup),
                            groupType: ko.observable(self.retrieve.groupType),
                            seriesCategory: ko.observable(self.retrieve.seriesCategory),

                            createdBy: ko.observable(self.retrieve.createdBy),
                            creationDate: ko.observable(self.formatDate(new Date())),
                            lastUpdatedBy: ko.observable(app.userName()),
                            lastUpdatedDate: ko.observable(oj.IntlConverterUtils.dateToLocalIso(new Date())),
                            code: ko.observable(self.retrieve.code),
                            startDateGregorian: ko.observable(self.formatDate(new Date(self.retrieve.startDateGregorian)))
                        };
                        var fullname = self.retrieve.jobCatagoryLbl + " " + self.retrieve.generalGroupLbl + " " + self.retrieve.generalTypeLbl + " " + self.retrieve.seriesCategoryLbl;

                        self.jobn = ko.observable(fullname);
                        function getFutureRecirdeCBCF(data) {
                            self.isHaveFuture(data.count);
                        }
                        function failCbFn() {

                        }
//                 self.ministryJobModle.startDate('1439-10-05');
                        if (self.ministryJobModle.startDateGregorian() == app.globalEffictiveDate()) {
                            correction();
                        }
                        self.ministryJobModle.startDate(app.globalEffictiveDate());


                        services.searcheGeneric(commonUtil.jobFutureUrl, self.ministryJobModle).then(getFutureRecirdeCBCF, failCbFn);
                        if (self.isHaveFuture() > 0) {
                            updateChangeInsert();

                        }

                    } else if (self.retrieve.type === 'view') {
                        self.serviceType('VIEW');
                        self.currentStepValue('stp2');

                        self.jnVisibility(true);
                        self.trainVisibility(false);
                        self.isDisabled(true);
                        self.addBtnVisible(false);
                        self.nextBtnVisible(false);
                        self.ministryJobModle = {
                            jobId: ko.observable(self.retrieve.jobId),
                            jobName: ko.observable(self.retrieve.jobName),
                            startDate: ko.observable(self.formatDate(new Date(self.retrieve.startDate))),
                            endDate: ko.observable(self.formatDate(new Date(self.retrieve.endDate))),

                            jobClassificationType: ko.observable(self.retrieve.jobClassificationType),
                            generalGroup: ko.observable(self.retrieve.generalGroup),
                            groupType: ko.observable(self.retrieve.groupType),
                            seriesCategory: ko.observable(self.retrieve.seriesCategory),

                            reatedBy: ko.observable(self.retrieve.createdBy),
                            creationDate: ko.observable(self.retrieve.creationDate),
                            lastUpdatedBy: ko.observable(app.userName()),
                            lastUpdatedDate: ko.observable(oj.IntlConverterUtils.dateToLocalIso(new Date())),
                            code: ko.observable(self.retrieve.code)
                        };
                        var fullname = self.retrieve.jobCatagoryLbl + " " + self.retrieve.generalGroupLbl + " " + self.retrieve.generalTypeLbl + " " + self.retrieve.seriesCategoryLbl;

                        self.jobn = ko.observable(fullname);
                    }
                    function correction() {
                        self.serviceType('CORRECTION');
                        self.oprationMessage(self.editMassageLbl());
                        self.jnVisibility(true);
                        self.trainVisibility(true);
                        $('#datePickerStartDate').calendarsPicker('disable');
                        $('#datePickerEndDate').calendarsPicker('disable');
                        self.ministryJobModle = {
                            jobId: ko.observable(self.retrieve.jobId),
                            jobName: ko.observable(self.retrieve.jobName),
                            startDate: ko.observable(self.formatDate(new Date(self.retrieve.startDate))),
                            endDate: ko.observable(self.formatDate(new Date(self.retrieve.endDate))),

                            jobClassificationType: ko.observable(self.retrieve.jobClassificationType),
                            generalGroup: ko.observable(self.retrieve.generalGroup),
                            groupType: ko.observable(self.retrieve.groupType),
                            seriesCategory: ko.observable(self.retrieve.seriesCategory),

                            createdBy: ko.observable(self.retrieve.createdBy),
                            creationDate: ko.observable(self.formatDate(new Date())),
                            lastUpdatedBy: ko.observable(app.userName()),
                            lastUpdatedDate: ko.observable(oj.IntlConverterUtils.dateToLocalIso(new Date())),
                            code: ko.observable(self.retrieve.code)
                        };

                        self.jobName(self.ministryJobModle.jobName());

                    }
                };
                ///////////////////////add function for change insert
                function updateChangeInsert() {
                    self.serviceType('Update_Change_Insert');
                    self.oprationMessage(self.editMassageLbl());
                    self.jnVisibility(true);
                    self.trainVisibility(true);
                    $('#datePickerStartDate').calendarsPicker('disable');
                    $('#datePickerEndDate').calendarsPicker('disable');
                    self.ministryJobModle = {
                        jobId: ko.observable(self.retrieve.jobId),
                        jobName: ko.observable(self.retrieve.jobName),
                        startDate: ko.observable(self.formatDate(new Date(self.retrieve.startDate))),
                        endDate: ko.observable(self.formatDate(new Date(self.retrieve.endDate))),

                        jobClassificationType: ko.observable(self.retrieve.jobClassificationType),
                        generalGroup: ko.observable(self.retrieve.generalGroup),
                        groupType: ko.observable(self.retrieve.groupType),
                        seriesCategory: ko.observable(self.retrieve.seriesCategory),

                        createdBy: ko.observable(self.retrieve.createdBy),
                        creationDate: ko.observable(self.formatDate(new Date())),
                        lastUpdatedBy: ko.observable(app.userName()),
                        lastUpdatedDate: ko.observable(oj.IntlConverterUtils.dateToLocalIso(new Date())),
                        code: ko.observable(self.retrieve.code)
                    };
                }
                //
                ///////////////////////add function for change insert
                function updateOverride() {
                    if (self.isHaveFuture() > 0) {
                        self.serviceType('update_override');
                        self.oprationMessage(self.editMassageLbl());
                        self.jnVisibility(true);
                        self.trainVisibility(true);
                        $('#datePickerStartDate').calendarsPicker('disable');
                        $('#datePickerEndDate').calendarsPicker('disable');
                        self.ministryJobModle = {
                            jobId: ko.observable(self.retrieve.jobId),
                            jobName: ko.observable(self.retrieve.jobName),
                            startDate: ko.observable(self.formatDate(new Date(self.retrieve.startDate))),
                            endDate: ko.observable(self.formatDate(new Date(self.retrieve.endDate))),

                            jobClassificationType: ko.observable(self.retrieve.jobClassificationType),
                            generalGroup: ko.observable(self.retrieve.generalGroup),
                            groupType: ko.observable(self.retrieve.groupType),
                            seriesCategory: ko.observable(self.retrieve.seriesCategory),

                            createdBy: ko.observable(self.retrieve.createdBy),
                            creationDate: ko.observable(self.formatDate(new Date())),
                            lastUpdatedBy: ko.observable(app.userName()),
                            lastUpdatedDate: ko.observable(oj.IntlConverterUtils.dateToLocalIso(new Date())),
                            code: ko.observable(self.retrieve.code)
                        };
                    }
                }
                //
                var getTranslation = oj.Translations.getTranslatedString;
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        self.stepArray([{label: self.create(), id: 'stp1'},
                            {label: self.review(), id: 'stp2'}]);
                        initTranslations();
                    }
                });
                function initTranslations() {
                    self.retrieve = oj.Router.rootInstance.retrieve();
                    if (self.retrieve === 'add') {
                        self.jobTitel(getTranslation("job.addJob"));
                    } else if (self.retrieve.type === 'update') {
                        self.jobTitel(getTranslation("job.editJob"));
                    } else if (self.retrieve.type === 'view') {
                        self.jobTitel(getTranslation("job.viewJob"));
                    }
                    self.endDate(getTranslation("job.endDate"));
                    self.startDate(getTranslation("job.startDate"));
                    self.dateMessage(getTranslation("labels.datemessage"));
                    self.errorAdd(getTranslation("job.errorAdd"));
                    self.jobType(getTranslation("job.jobType"));
                    self.generalGroup(getTranslation("job.generalGroup"));
                    self.jobTypeGroup(getTranslation("job.jobTypeGroup"));
                    self.seriesCategories(getTranslation("job.SeriesCategories"));
                    self.jobName(getTranslation("job.jobName"));
                    self.addJob(getTranslation("job.addJob"));
                    self.updateJob(getTranslation("job.updateJob"));
                    self.viewJob(getTranslation("job.viewJob"));
                    self.addMassageLbl(getTranslation("job.addMessage"));
                    self.editMassageLbl(getTranslation("job.editMessage"));

                    self.trainView(getTranslation("others.review"));

                    self.placeholder(getTranslation("labels.placeHolder"));
                    self.pervious(getTranslation("others.pervious"));
                    self.next(getTranslation("others.next"));
                    self.cancel(getTranslation("others.cancel"));
                    self.yes(getTranslation("others.yes"));
                    self.no(getTranslation("others.no"));
                    self.submit(getTranslation("others.submit"));
                    self.successEdit(getTranslation("job.successEdit"));
                    self.confirmMessage(getTranslation("labels.confirmMessage"));
                    self.create(getTranslation("labels.create"));
                    self.review(getTranslation("others.review"));
                    self.ok(getTranslation("others.ok"));
                    self.returnToPerviousPage(getTranslation("job.returnToPerviousPage"));
                    self.confirmationCancelPage(getTranslation("job.confirmationCancelPage"));
                    self.checkProcessSucessOrNot(getTranslation("job.checkProcessSucessOrNot"));
                    if (self.serviceType() === 'ADD') {
                        self.oprationMessage(self.addMassageLbl());
                    } else if (self.serviceType() === 'EDIT') {
                        self.oprationMessage(self.editMassageLbl());
                    }
                    self.stepArray([{label: self.jobTitel(), id: 'stp1'},
                        {label: self.trainView(), id: 'stp2'}
                    ]);
                }
                initTranslations();
            }
            return JobContentViewModel;
        });
