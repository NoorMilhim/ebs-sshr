/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * homeministryJob module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'util/commonhelper', 'ojs/ojknockout', 'ojs/ojarraydataprovider', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
    'ojs/ojselectcombobox', 'ojs/ojdialog', 'ojs/ojinputtext', 'jquery-calendar', 'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin',
    'jquery-calendar-picker', 'ojs/ojlabel', 'ojs/ojdatetimepicker', 'ojs/ojtimezonedata'],
        function (oj, ko, $, app, services, commonhelper, commonUtil) {

            function homeministryJobContentViewModel() {
                var self = this;

                self.columnArray = ko.observableArray();
                self.isRequired = ko.observable(true);
                self.placeholder = ko.observable("Please Select Value");
                self.dataSourceTB2 = ko.observable();
                var deptArray = [];
                self.oprationdisabled = ko.observable(true);
                self.searchdisabled = ko.observable(true);
                self.cleardisabled = ko.observable(true);
                self.checkResultSearch = ko.observable();
                self.seqaunce = ko.observable();
                self.selectedRowKey = ko.observable();
                self.selectedIndex = ko.observable();
                self.seuencelbl = ko.observable();
                self.deptObservableArray = ko.observableArray(deptArray);
                self.dataprovider = new oj.ArrayDataProvider(self.deptObservableArray, {idAttribute: 'DepartmentId'});
                self.dataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.deptObservableArray, {idAttribute: 'jobId'}));
                self.formatDate = function (date) {

                    //var date = new Date()
                    var month = '' + (date.getMonth() + 1),
                            day = '' + date.getDate(),
                            year = date.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;

                    return [year, month, day].join('-');
                };

                self.dateEffictive = ko.observable("");
                self.currentDate = ko.observable();
                self.isDisabled = ko.observable(false);



                self.testData = ko.observable([
                    {value: "value", label: "label"},
                    {value: "value", label: "label"},
                    {value: "value", label: "label"},
                    {value: "value", label: "label"},
                    {value: "value", label: "label"}
                ]);
                self.jobCatagoryList = ko.observableArray(app.globalJobCatagory());
                self.generalTypeList = ko.observableArray(app.globalGroupType());
                self.jobNameList = ko.observableArray(app.globalJobName());
                self.generalGroupList = ko.observableArray(app.globalGeneralGroup());
                self.jobsNames = ko.observableArray(app.globalJobsNames());
                self.jobList = ko.observableArray();



                self.ministryJobSearch = {
                    seriesCategory: ko.observable(),
                    qualityGroup: ko.observable(),
                    generalGroup: ko.observable(),
                    jobClassificationType: ko.observable(),
                    jobName: ko.observable()

                };
//                self.rowNumberRender = function (context) {
//                    return context.cellContext.status.rowIndex + 1;
//                };



                self.HijrahDate = function () {

                    var getHijrahDateCbFn = function (data) {

                        self.currentDate(data);
                        ;

                    };
                    var failCbFn = function () {
                    };

                    var serivename = commonUtil.getDate;
                    services.getGeneric(serivename).then(getHijrahDateCbFn, failCbFn);
                };



                self.searchPerson = function () {
                    app.loading(true);
                    var searcheJobCbFn = function (data) {
                        app.loading(false);
                        if (data.length !== 0) {
                            app.loading(false);
                            self.deptObservableArray(data);
//                                    localStorage.setItem("names", JSON.stringify(data));


                            for (var i = 0; i < data.length; i++) {
                                //jobCatagoryList		
                                //data[i].jobCatagoryLbl = searchArray(parseInt( data[i].jobClassificationType),self.jobCatagoryList());		
                                data[i].sequence = i + 1;
                                data[i].jobCatagoryLbl = searchArray(data[i].jobClassificationType, self.jobCatagoryList());
                                data[i].generalTypeLbl = searchArray(data[i].groupType, self.generalTypeList());
                                data[i].generalGroupLbl = searchArray(data[i].generalGroup, self.generalGroupList());
                                data[i].seriesCategoryLbl = searchArray(data[i].seriesCategory, self.jobNameList());
                                //                        rowNumberRender=i+1;		

                            }
                            //self.dataprovider = new oj.ArrayDataProvider(self.deptObservableArray, {idAttribute: 'jobId'});  		



                        } else {

                            $.notify(self.checkResultSearch(), "error");
                            self.deptObservableArray([]);

                        }
                    };
                    var failCbFn = function () {
                        $.notify(self.checkResultSearch(), "error");
                    };
                    services.searcheGeneric(commonhelper.jobHisoryUrl, self.ministryJobSearch).then(searcheJobCbFn, failCbFn);

                };


                self.allJobs = function () {
                    var allJobsCbFn = function (data) {
                        if (data.length !== 0) {
                            self.deptObservableArray(data);
                            for (var i = 0; i < data.length; i++) {
                                //jobCatagoryList
                                //data[i].jobCatagoryLbl = searchArray(parseInt( data[i].jobClassificationType),self.jobCatagoryList());
                                data[i].sequence = i + 1;
                                data[i].jobCatagoryLbl = searchArray(data[i].jobClassificationType, self.jobCatagoryList());
                                data[i].generalTypeLbl = searchArray(data[i].groupType, self.generalTypeList());
                                data[i].generalGroupLbl = searchArray(data[i].generalGroup, self.generalGroupList());
                                data[i].seriesCategoryLbl = searchArray(data[i].seriesCategory, self.jobNameList());
//                        rowNumberRender=i+1;
                                self.cleardisabled(false);
                            }
                            //self.dataprovider = new oj.ArrayDataProvider(self.deptObservableArray, {idAttribute: 'jobId'});  
                        } else {
                            self.deptObservableArray([]);
                            $.notify(self.checkResultSearch(), "error");

                        }
                    };
                    var failCbFn = function () {
                        $.notify(self.checkResultSearch(), "error");
                    };

                    services.getGeneric(commonhelper.getAllJobs, {}).then(allJobsCbFn, failCbFn);



                };

                self.reset = function () {
                    clearContent();
                    self.deptObservableArray([]);
                };
                function clearContent() {
                    app.loading(true);
                    self.ministryJobSearch.seriesCategory("");
                    self.ministryJobSearch.qualityGroup("");
                    self.ministryJobSearch.generalGroup("");
                    self.ministryJobSearch.jobClassificationType("");
                    self.ministryJobSearch.jobName("");
                    self.searchdisabled(true);
                    self.cleardisabled(true);
                    app.loading(false);
                }
                self.tableSelectionListener = function (event) {
                    var data = event.detail;

                    var currentRow = data.currentRow;
                    self.oprationdisabled(false);
                    self.selectedRowKey(currentRow['rowKey']);
                    self.selectedIndex(currentRow['rowIndex']);
                };

                self.jobNameChangedHandler = function (event, data) {
                    if (event.detail.value != "")
                    {
                        self.searchdisabled(false);
                        self.cleardisabled(false);
                    } else
                    {
                        self.searchdisabled(true);
                        self.cleardisabled(true);
                    }

                };

                self.groupChangedHandler = function (event, data) {
                    if (event.detail.value != "")
                    {
                        self.searchdisabled(false);
                        self.cleardisabled(false);
                    } else
                    {
                        self.searchdisabled(true);
                        self.cleardisabled(true);
                    }
                };

                self.jobtypeChangedHandler = function (event, data) {
                    if (event.detail.value != "")
                    {
                        self.searchdisabled(false);
                        self.cleardisabled(false);
                    } else
                    {
                        self.searchdisabled(true);
                        self.cleardisabled(true);
                    }
                };

                self.grouptypeChangedHandler = function (event, data) {
                    if (event.detail.value != "")
                    {
                        self.searchdisabled(false);
                        self.cleardisabled(false);
                    } else
                    {
                        self.searchdisabled(true);
                        self.cleardisabled(true);
                    }
                };

                self.seriesChangedHandler = function (event, data) {
                    if (event.detail.value != "")
                    {
                        self.searchdisabled(false);
                        self.cleardisabled(false);
                    } else
                    {
                        self.searchdisabled(true);
                        self.cleardisabled(true);
                    }
                };


                self.addJob = function () {
                    oj.Router.rootInstance.store('add');
                    oj.Router.rootInstance.go('job');
                };
                self.updateJob = function () {
                    ////oj.Router.rootInstance.store('update');
                    self.deptObservableArray()[self.selectedIndex()].type = "update";
                    oj.Router.rootInstance.store(self.deptObservableArray()[self.selectedIndex()]);
                    oj.Router.rootInstance.go('job');
                };
                self.viewJob = function () {
                    self.deptObservableArray()[self.selectedIndex()].type = "view";
                    oj.Router.rootInstance.store(self.deptObservableArray()[self.selectedIndex()]);
                    oj.Router.rootInstance.go('job');
                };
                self.correctionJob = function () {
                    self.deptObservableArray()[self.selectedIndex()].type = "CORRECTION";
                    oj.Router.rootInstance.store(self.deptObservableArray()[self.selectedIndex()]);
                    oj.Router.rootInstance.go('job');
                };
                self.historyDetails = ko.observable();
                ////----------------------------Effective Date Section--------------------
                self.closeCancelDialog = function () {
                    $("#effictiveDateDialog").ojDialog("close");
                };
                self.openEffectiveDate = function () {
                    document.querySelector("#effictiveDateDialog").open();


                };
                self.closeDialogDate = function () {
                    $("#effictiveDateDialog").ojDialog("close");
                    if (self.dateEffictive()) {
                        var x = self.dateEffictive();
                        app.globalEffictiveDate(x);
                    } else {
                        self.dateEffictive("");
                        app.globalEffictiveDate(self.currentDate());
                    }
                };
                self.restEffectiveDate = function () {
                    self.dateEffictive("");
                    self.currentDate("");
                };
                app.globalEffictiveDate(self.currentDate());
                //--------------------------End Effective Date Section--------------------
                //---------History Section ----------------------------------------
                self.closeDialog = function () {
                    $("#modalDialog1").ojDialog("close");
                };




                self.openDialog = function () {
                    var jobHistoryCBF = function (data) {
                        var count = 0;
                        for (var i = 0; i < data.length; i++) {
                            //jobCatagoryList
                            //data[i].jobCatagoryLbl = searchArray(parseInt( data[i].jobClassificationType),self.jobCatagoryList());
                            data[i].sequence = i + 1;
                            data[i].jobCatagoryLbl = searchArray(data[i].jobClassificationType, self.jobCatagoryList());
                            data[i].generalTypeLbl = searchArray(data[i].groupType, self.generalTypeList());
                            data[i].generalGroupLbl = searchArray(data[i].generalGroup, self.generalGroupList());
                            data[i].seriesCategoryLbl = searchArray(data[i].seriesCategory, self.jobNameList());
                            data[i].seq = count += 1;
                        }
                        self.dataSourceTB2(new oj.ArrayTableDataSource(data));
                    };
                    services.getGeneric(commonhelper.jobHisoryUrl + self.deptObservableArray()[self.selectedIndex()].jobName, {}).then(jobHistoryCBF, self.failCbFn);
                    document.querySelector("#modalDialog1").open();
                };
                //-----------------------End History Section -------------------------------------
                //------------------------Valid Grade Section ----------------------------------
                self.validGradeAction = function () {
                    oj.Router.rootInstance.store(self.deptObservableArray()[self.selectedIndex()]);
                    oj.Router.rootInstance.go('validGradeSummary');
                };
                //--------------------End Of Section ----------------------------------------------
                //------------------Section For Additional Details -----------------------------
                self.detailsOfJob = function () {
                    oj.Router.rootInstance.store(self.deptObservableArray()[self.selectedIndex()]);
                    oj.Router.rootInstance.go('additionalDetailsSummary');


                };
                //-------------------------End Of Secttion 
                self.handleAttached = function (info) {
                    app.loading(false);

                    self.HijrahDate();
                    $("#datePickerStartDate2").calendarsPicker({calendar: $.calendars.instance('ummalqura'),
                        onSelect: function (dates) {
                            var endDate = $('#datePickerStartDate2').val();
                        }});
                    $("#datePickerStartDate1").calendarsPicker({calendar: $.calendars.instance('ummalqura'),
                        onSelect: function (dates) {
                            var startDate = $('#datePickerStartDate1').val();
                        }});
                    app.globalEffictiveDate(self.currentDate());
                };
                //----------------------------get all job name---------------
                var jobNames = function (data) {
                    for (var i = 0; i < data.length; i++) {
                        //Deployed
                        data[i].JOBCATAGORYLBL = searchArray(Number(data[i].jobClassificationType), self.jobCatagoryList());
                        data[i].GENERALTYPELBL = searchArray(Number(data[i].groupType), self.generalTypeList());
                        data[i].GENERALGROUPLBL = searchArray(Number(data[i].generalGroup), self.generalGroupList());
                        data[i].SERIESCATEGORYLBL = searchArray(Number(data[i].seriesCategory), self.jobNameList());
                        ///Local
//                        data[i].JOBCATAGORYLBL = searchArray(data[i].jobClassificationType, self.jobCatagoryList());
//                        data[i].GENERALTYPELBL = searchArray(data[i].groupType, self.generalTypeList());
//                        data[i].GENERALGROUPLBL = searchArray(data[i].generalGroup, self.generalGroupList());
//                        data[i].SERIESCATEGORYLBL = searchArray(data[i].seriesCategory, self.jobNameList());

                        self.jobsNames.push({"value": data[i].jobName, "label": data[i].JOBCATAGORYLBL + "." + data[i].GENERALTYPELBL + "." + data[i].GENERALGROUPLBL + "." + data[i].SERIESCATEGORYLBL});
                    }
                };
                services.getGeneric(commonhelper.getAllJobs, {}).then(jobNames, self.failCbFn);
                //-----------------------------
                //---------------------Translate Section ----------------------------------------
                self.ok = ko.observable();
                self.startDateLbl = ko.observable();
                self.endDateLbl = ko.observable();
                self.jobTypeLbl = ko.observable();
                self.generalGroupLbl = ko.observable();
                self.jobTypeGroupLbl = ko.observable();
                self.SeriesCategories = ko.observable();
                self.addLbl = ko.observable();
                self.editLbl = ko.observable();
                self.additionalDetailsLbl = ko.observable();
                self.alljobs = ko.observable();
                self.historyLbl = ko.observable();
                self.validGradeLbl = ko.observable();
                self.correctionLbl = ko.observable();
                self.search = ko.observable();
                self.clear = ko.observable();
                self.view = ko.observable();
                self.validGrade = ko.observable();
                self.ministryJob = ko.observable();
                self.effectiveDate = ko.observable();
                self.todayDay = ko.observable();
                self.cancel = ko.observable();
                self.resetLabel = ko.observable();
                self.jobName = ko.observable();
                var getTranslation = oj.Translations.getTranslatedString;
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();

                    }

                });
                function initTranslations() {

                    self.ok(getTranslation("others.ok"));
                    self.addLbl(getTranslation("others.add"));
                    self.editLbl(getTranslation("others.edit"));
                    self.validGradeLbl(getTranslation("others.validGrade"));
                    self.additionalDetailsLbl(getTranslation("others.additonalDetail"));
                    self.historyLbl(getTranslation("others.history"));
                    self.startDateLbl(getTranslation("job.startDate"));
                    self.endDateLbl(getTranslation("job.endDate"));
                    self.historyDetails(getTranslation("job.historyDetails"))
                    self.jobTypeLbl(getTranslation("job.jobType"));
                    self.generalGroupLbl(getTranslation("job.generalGroup"));
                    self.jobTypeGroupLbl(getTranslation("job.jobTypeGroup"));
                    self.SeriesCategories(getTranslation("job.SeriesCategories"));
                    self.seqaunce(getTranslation("labels.Sequence"));
                    self.correctionLbl(getTranslation("others.correction"));
                    self.search(getTranslation("others.search"));
                    self.clear(getTranslation("others.clear"));
                    self.view(getTranslation("others.view"));
                    self.validGrade(getTranslation("others.validGrade"));
                    self.ministryJob(getTranslation("job.ministryJob"));
                    self.effectiveDate(getTranslation("others.effectiveDate"));
                    self.todayDay(getTranslation("others.todayDay"));
                    self.cancel(getTranslation("others.cancel"));
                    self.resetLabel(getTranslation("login.resetLabel"));
                    self.jobName(getTranslation("job.jobName"));
                    self.seuencelbl(getTranslation("common.sequence"));
                    self.alljobs(getTranslation("job.allJobs"))
                    self.checkResultSearch(getTranslation("common.checkResultSearch"));
                    self.columnArray([
//                        {
//                            "headerText": "rowNumber", "field": "rowNumberRender"
//                         },
//                        {
//                           "headerText": self.seuencelbl(), "field": "sequence"
//                        } ,                     
                        {
                            "headerText": self.startDateLbl(), "field": "startDate"
                        },
                        {
                            "headerText": self.endDateLbl(), "field": "endDate"
                        },
                        {
                            "headerText": self.jobTypeLbl(), "field": "jobCatagoryLbl"
                        },
                        {
                            "headerText": self.generalGroupLbl(), "field": "generalGroupLbl"
                        },
                        {
                            "headerText": self.jobTypeGroupLbl(), "field": "generalTypeLbl"
                        },
                        {
                            "headerText": self.SeriesCategories(), "field": "seriesCategoryLbl"
                        }
                    ]);
                }
                initTranslations();




            }

            return homeministryJobContentViewModel;
        });
