/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * homeministryJob module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojknockout', 'ojs/ojarraydataprovider',
    'ojs/ojselectcombobox', 'ojs/ojdialog'],
        function (oj, ko, $, app, services, commonhelper) {

            function summaryValidGrade() {
                var self = this;
                self.columnArray = ko.observableArray();
                self.columnArray2 = ko.observableArray();
                self.placeholder = ko.observable("Please Select Value");
                self.dataSourceTB2 = ko.observable();
                var deptArray = [];
                self.oprationdisabled = ko.observable(true);
                self.seqaunce = ko.observable();
                self.selectedRowKey = ko.observable();
                self.fullname = ko.observable();
                self.jobNameLbl = ko.observable();
                self.effStartDate = ko.observable();
                self.effEndDate = ko.observable();
                self.selectedIndex = ko.observable();
                self.deptObservableArray = ko.observableArray(deptArray);//validGradeOprationScreen
                self.nextPage = ko.observable('validGradeOprationScreen');
                self.gradesArr = ko.observableArray(app.globalHrGrades());
                self.positionNameArr = ko.observableArray(app.globalPositionsName());
                self.sequenceLbl = ko.observableArray();
                self.isDisable = ko.observable(true);
                var jobObj;
                var jobObj2;
                self.dataprovider = new oj.ArrayDataProvider(self.deptObservableArray, {idAttribute: 'DepartmentId'});
                function clearContent() {

                }
                self.tableSelectionListener = function (event) {
                    var data = event.detail;
                    var currentRow = data.currentRow;
                    self.oprationdisabled(false);
                    self.selectedRowKey(currentRow['rowKey']);
                    self.selectedIndex(currentRow['rowIndex']);
                    self.isDisable(false);
                };

                self.addValidGrade = function () {

                    jobObj.type = "ADD";
                    oj.Router.rootInstance.store(jobObj);
                    oj.Router.rootInstance.go(self.nextPage());
                };
                self.updateValidGrade = function () {
                    self.deptObservableArray()[self.selectedIndex()].type = "EDIT";
                    self.deptObservableArray()[self.selectedIndex()].xx = "xx";
                    oj.Router.rootInstance.store(self.deptObservableArray()[self.selectedIndex()]);
                    oj.Router.rootInstance.go(self.nextPage());
                };
                self.backAction = function () {
                    app.loading(true);
                    oj.Router.rootInstance.go('searchJob');

                };
                self.handleAttached = function () {
                    app.loading(false);
                    //jobName

                    jobObj = oj.Router.rootInstance.retrieve();

                    var fullname = jobObj.jobCatagoryLbl + " " + jobObj.generalGroupLbl + " " + jobObj.generalTypeLbl + " " + jobObj.seriesCategoryLbl;
                    self.fullname(fullname);
                    var effStartDate = jobObj.startDate;
                    self.effStartDate(effStartDate);
                    var effEndDate = jobObj.endDate;
                    self.effEndDate(effEndDate);

                    var jobarray = [{JobName: self.fullname(), EffictiveStartDate: self.effStartDate(), EffictiveEndDate: self.effEndDate()}];
                    self.dataprovider2 = new oj.ArrayDataProvider(jobarray, {idAttribute: 'DepartmentId'});
                    var jobName = jobObj.jobName;

                    self.nextPage('validGradeOprationScreen');
                    var getValidGradeCBF = function (data) {
                        var Data = data;
                        var count = 0;
                        if (data) {
                            for (var i = 0; i < data.length; i++) {
                                data[i].sequenceLbl = i + 1;
                                data[i].gradeLbl = searchArray(data[i].grade, self.gradesArr());
                                data[i].positionLbl = searchArray(data[i].positionName, self.positionNameArr());
                                data[i].seq = count += 1;

                            }
                            self.deptObservableArray(Data);
                        }

                    };
                    services.getGeneric(commonhelper.validGradeUrl + jobName, {}).then(getValidGradeCBF, self.failCbFn);

                };

                //---------------------Translate Section ----------------------------------------
                self.ok = ko.observable();
                self.startDateLbl = ko.observable();
                self.endDateLbl = ko.observable();
                self.addLbl = ko.observable();
                self.editLbl = ko.observable();
                self.gradeLbl = ko.observable();
                self.postionLbl = ko.observable();
                self.validGradeLbl = ko.observable();
                self.backLbl = ko.observable();
                var getTranslation = oj.Translations.getTranslatedString;
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });
                function initTranslations() {
                    self.backLbl(getTranslation("others.back"));
                    self.ok(getTranslation("others.ok"));
                    self.seqaunce(getTranslation("labels.Sequence"));
                    self.addLbl(getTranslation("others.add"));
                    self.jobNameLbl(getTranslation("common.job"));
                    self.editLbl(getTranslation("others.edit"));
                    self.validGradeLbl(getTranslation("others.validGrade"));
                    self.startDateLbl(getTranslation("job.startDate"));
                    self.endDateLbl(getTranslation("job.endDate"));
                    self.postionLbl(getTranslation("position.positionName"));
                    self.gradeLbl(getTranslation("common.grade"));
                    self.sequenceLbl(getTranslation("common.sequence"));
                    self.columnArray([
                        {
                            "headerText": self.sequenceLbl(), "field": "sequenceLbl"
                        },
                        {
                            "headerText": self.gradeLbl(), "field": "gradeLbl"
                        }
                        ,
                        {
                            "headerText": self.startDateLbl(), "field": "startDate"
                        },
                        {
                            "headerText": self.endDateLbl(), "field": "endDate"
                        },
                        {
                            "headerText": self.postionLbl(), "field": "positionLbl"
                        }
                    ]);
                    self.columnArray2([
                        {
                            "headerText": self.jobNameLbl(), "field": "JobName",

                        },
                        {
                            "headerText": self.startDateLbl(), "field": "EffictiveStartDate"
                        },
                        {
                            "headerText": self.endDateLbl(), "field": "EffictiveEndDate"
                        }
                    ]);
                }
                initTranslations();




            }

            return summaryValidGrade;
        });
