/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * homeministryJob module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper',
    'ojs/ojknockout', 'ojs/ojtrain', 'ojs/ojbutton',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker',
    'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup', 'ojs/ojcheckboxset', 'ojs/ojradioset',
    'ojs/ojlabel', 'ojs/ojgauge', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojmessage', 'jquery-calendar',
    'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin',
    'ojs/ojtimezonedata', 'ojs/ojdatetimepicker', 'ojs/ojselectcombobox',
    'jquery-calendar-picker', 'ojs/ojvalidationgroup'],
        function (oj, ko, $, app, services, commonhelper) {

            function validGradeOprationScreen() {
                var self = this;
                var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
                self.columnArray = ko.observableArray();
                self.tracker = ko.observable();
                self.dateMessage = ko.observable();
                self.groupValid = ko.observable();
                self.placeholder = ko.observable("Please Select Value");
                self.dataSourceTB2 = ko.observable();
                var deptArray = [];
                self.isDisabled = ko.observable(false);
                self.selctedStruct = ko.observable();
                self.isRequired = ko.observable(true);
                self.currentStepValue = ko.observable('stp1');
                self.addBtnVisible = ko.observable(false);
                self.nextBtnVisible = ko.observable(true);
                self.pageMode = ko.observable();
                self.oprationMessage = ko.observable();
                self.disableSubmit = ko.observable(false);
                self.gradesArr = ko.observableArray(app.globalHrGrades());
                self.positionNameArr = ko.observableArray(app.globalPositionsName());
                var Url;
                var jobObj;
                self.stepArray = ko.observableArray([{label: "XXXX", id: 'stp1'},
                    {label: "YYYY", id: 'stp2'}]);

                self.backAction = function () {
                    jobObj.jobName = jobObj.jobCode;
                    oj.Router.rootInstance.store(jobObj);
                    if (oj.Router.rootInstance._navHistory.length > 1) {
                        app.loading(true);
                        oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                    }

                };
                self.formatDate = function (date) {
                    //var date = new Date()
                    var month = '' + (date.getMonth() + 1),
                            day = '' + date.getDate(),
                            year = date.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;

                    return [year, month, day].join('-');
                };
                self.validGradeModel = {
                    startDate: ko.observable(),
                    endDate: ko.observable(),
                    grade: ko.observable(),
                    effectiveStartDate: ko.observable(),
                    effectiveEndDate: ko.observable(),
                    id: ko.observable(),
                    TransActionType: ko.observable(), //Edit View or Add 
                    flag: ko.observable('Y'), //Y or N
                    createdBy: ko.observable(),
                    jobCode: ko.observable(),
                    creationDate: ko.observable(self.formatDate(new Date())),
                    updatedBy: ko.observable(),
                    positionName: ko.observable()

                };

                self.handleAttached = function () {
                    app.loading(false);
                    jobObj = oj.Router.rootInstance.retrieve();
                    if (app.xx() == "N") {
                        $("#datePickerEndDate").calendarsPicker({calendar: $.calendars.instance('ummalqura'),
                            onSelect: function (dates) {
                                var endDate = $('#datePickerEndDate').val();
                                self.validGradeModel.endDate(endDate);
                            }});
                        $("#datePickerStartDate").calendarsPicker({calendar: $.calendars.instance('ummalqura'),
                            onSelect: function (dates) {
                                var startDate = $('#datePickerStartDate').val();
                                self.validGradeModel.startDate(startDate);
                            }});
                        app.xx("Y");
                    }
                    initTranslations();
                    if (jobObj.type == 'ADD') {
                        self.pageMode('ADD');
                        self.validGrade(self.validGradeAddLbl());
                        self.oprationMessage(self.addMassageLbl());
                        self.validGradeModel.jobCode(jobObj.jobName);
                        self.validGradeModel.effectiveStartDate(jobObj.startDate);
                        self.validGradeModel.effectiveEndDate(jobObj.endDate);
                        self.validGradeModel.createdBy(rootViewModel.personDetails().personId);
                    } else if (jobObj.type == 'EDIT') {
                        self.pageMode('EDIT');

                        $("#datePickerStartDate").val(jobObj.startDate);
                        $("#datePickerEndDate").val(jobObj.endDate);
                        self.validGrade(self.validGradeEditLbl());
                        self.validGradeModel.createdBy(rootViewModel.personDetails().personId);
                        self.oprationMessage(self.editMassageLbl());
                        self.validGradeModel.jobCode(jobObj.jobCode);
                        self.validGradeModel.updatedBy(rootViewModel.personDetails().personId);
                        self.validGradeModel.startDate(self.formatDate(new Date(jobObj.startDate)));
                        self.validGradeModel.effectiveStartDate(self.formatDate(new Date(jobObj.effectiveStartDate)));
                        self.validGradeModel.effectiveEndDate(jobObj.effectiveEndDate);
                        self.validGradeModel.endDate(self.formatDate(new Date(jobObj.endDate)));
                        self.validGradeModel.id(jobObj.id);
                        self.validGradeModel.grade(jobObj.grade);
                        self.validGradeModel.positionName(jobObj.positionName);
                    }

                };


                //-------------This Function For Update Train Step To previousStep------------------
                self.previousStep = function () {
                    var prev = document.getElementById("train").getPreviousSelectableStep();
                    if (prev != null)
                        self.currentStepValue(prev);
                };
                self.nextStep = function ()
                {
                    function isValidDate(dateString)
                    {

                        // First check for the pattern
                        if (!/^\d{4}\/\d{1,2}\/\d{1,2}$/.test(dateString)) {
                            if (!/^\d{4}\-\d{1,2}\-\d{1,2}$/.test(dateString)) {
                                $.notify(self.dateMessage());
                                return false;
                            }
                            return true;
                        }

                        // Parse the date parts to integers
                        var parts = dateString.split("/");
                        var month = parseInt(parts[1], 10);
                        var year = parseInt(parts[0], 10);
                        var day = parseInt(parts[2], 10);


                        // Check the ranges of month and year
                        if (year < 1300 || year > 3000 || month == 0 || month > 12) {
                            $.notify(self.dateMessage());
                            return false;
                        }

                        return true;


                    }
                    ;
                    function datevalidtion(d, d2) {
                        if (d >= d2) {
                            $.notify(self.dateMessage());
                            return false;
                        }
                        return true
                    }
                    ;

                    var s1 = self.validGradeModel.startDate();
                    var d = Date.parse(s1);
                    var s2 = self.validGradeModel.endDate();
                    var d2 = Date.parse(s2);


                    if (isValidDate(s1) && isValidDate(s2) && datevalidtion(d, d2)) {
                        var tracker = document.getElementById("tracker");
                        if (tracker.valid != "valid")
                        {
                            tracker.showMessages();
                            tracker.focusOn("@firstInvalidShown");
                            return;
                        }
                        var next = document.getElementById("train").getNextSelectableStep();
                        if (next != null)
                            self.currentStepValue(next);
                    }
                    ;
                };
                self.currentStepValueText = function () {
                    if (self.currentStepValue() == 'stp2') {
                        self.isDisabled(true);
                        self.addBtnVisible(true);
                        self.nextBtnVisible(false);
                        $('#datePickerStartDate').calendarsPicker('disable');
                        $('#datePickerEndDate').calendarsPicker('disable');
                    } else {
                        self.isDisabled(false);
                        self.addBtnVisible(false);
                        self.nextBtnVisible(true);
                        $('#datePickerStartDate').calendarsPicker('enable');
                        $('#datePickerEndDate').calendarsPicker('enable');
                    }

                    return self.validGrade();
                };
                //------------------End Of Section -------------------------------------------------
                //------------------This Section For Dialog Action (Open ,Close)
                self.submitButton = function () {
                    document.querySelector("#yesNoDialog").open();
                };
                self.cancelButton = function () {
                    document.querySelector("#yesNoDialog").close();
                };
                //------------------End Of Section ----------------------------------------------
                //-------------This Function To Stop Train Action If Action Are Not Valid -------
                self.stopSelectListener = function (event, ui) {
                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        event.preventDefault();
                        return;
                    }
                };
                //-------------------End Of Function ---------------------------------------------



                //------------------ This Section For Call Web Service For Insert Or Update ---------
                function oprationServiceRecord() {
                    var jsonData = ko.toJSON(self.validGradeModel);
                    var oprationCBF = function (data) {
                        var Data = data;
                        self.disableSubmit(false);
                        if (oj.Router.rootInstance._navHistory.length > 1) {
                            oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                        } else {
                            oj.Router.rootInstance.go('home');
                        }
                    };
                    //   services.addGeneric(commonhelper.validGradeUrl + self.pageMode(), jsonData).then(oprationCBF, self.failCbFn); 
                    services.addGeneric(commonhelper.validGradeUrl + self.pageMode(), jsonData).then(oprationCBF, self.failCbFn);
                }
                ;
                self.commitRecord = function (data, event) {
                    if (!self.disableSubmit()) {
                        app.loading(true);
                        self.disableSubmit(true);
                        oprationServiceRecord();
                        return true;
                    }
                };
                //-------------------End Of Section ------------------------------------------------
                //---------------------Translate Section ----------------------------------------
                self.ok = ko.observable();
                self.startDateLbl = ko.observable();
                self.endDateLbl = ko.observable();
                self.addLbl = ko.observable();
                self.editLbl = ko.observable();
                self.gradeLbl = ko.observable();
                self.validGradeLbl = ko.observable();
                self.structureNameLbl = ko.observable();
                self.backLbl = ko.observable();
                self.nextLbl = ko.observable();
                self.submitLbl = ko.observable();
                self.placeholder = ko.observable("Please Select Value");
                self.supervisorLbl = ko.observable();
                self.qualificationNameLbl = ko.observable();
                self.experienceYearsLbl = ko.observable();
                self.amountLbl = ko.observable();
                self.cancel = ko.observable();
                self.create = ko.observable();
                self.review = ko.observable();
                self.validGradeAddLbl = ko.observable();
                self.validGradeEditLbl = ko.observable();
                self.addMassageLbl = ko.observable();
                self.editMassageLbl = ko.observable();
                self.confirmMessage = ko.observable();
                self.validGrade = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.positionName = ko.observable();
                var getTranslation = oj.Translations.getTranslatedString;
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        self.stepArray([{label: self.create(), id: 'stp1'},
                            {label: self.review(), id: 'stp2'}]);
                        initTranslations();
                    }
                });
                function initTranslations() {
                    self.create(getTranslation("labels.create"));
                    self.review(getTranslation("others.review"));
                    self.structureNameLbl(getTranslation("additionalDetails.structureName"));
                    self.backLbl(getTranslation("others.back"));
                    self.nextLbl(getTranslation("others.next"));
                    self.submitLbl(getTranslation("others.submit"));
                    self.ok(getTranslation("others.ok"));
                    self.addLbl(getTranslation("others.add"));
                    self.editLbl(getTranslation("others.edit"));
                    self.validGradeLbl(getTranslation("others.validGrade"));
                    self.startDateLbl(getTranslation("job.startDate"));
                    self.endDateLbl(getTranslation("job.endDate"));
                    self.gradeLbl(getTranslation("common.grade"));
                    self.cancel(getTranslation("others.cancel"));
                    self.validGradeAddLbl(getTranslation("validGrade.validGradeAdd"));
                    self.dateMessage(getTranslation("labels.datemessage"));
                    self.validGradeEditLbl(getTranslation("validGrade.validGradeEdit"));
                    self.addMassageLbl(getTranslation("validGrade.addMessage"));
                    self.editMassageLbl(getTranslation("validGrade.editMessage"));
                    self.confirmMessage(getTranslation("labels.confirmMessage"));
                    self.yes(getTranslation("others.yes"));
                    self.no(getTranslation("others.no"));
                    self.positionName(getTranslation("position.positionName"));
                    self.validGrade(getTranslation("validGrade.validGradeAdd"));
                    self.stepArray([{label: self.create(), id: 'stp1'},
                        {label: self.review(), id: 'stp2'}]);
                }
                initTranslations();




            }

            return validGradeOprationScreen;
        });
