define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper',
    'ojs/ojknockout', 'ojs/ojarraydataprovider', 'ojs/ojdatetimepicker', 'ojs/ojtimezonedata',
    'ojs/ojselectcombobox', 'ojs/ojdialog', 'ojs/ojtrain', 'ojs/ojvalidationgroup'],
        function (oj, ko, $, app, services, commonhelper) {

            function elementEntryAddViewModel() {
                var self = this;
                var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
                var countValid = "";
                var max_order = 0;
                var positionObj;
                var getTranslation = oj.Translations.getTranslatedString;
                self.absenceStatusLbl = ko.observable();
                self.eitCodeErrMsg = ko.observable();
                self.absenceErrMsg = ko.observable();
                self.absenceTypeLbl = ko.observable();
                self.unknownErrMsg = ko.observable();
                self.employerLbl = ko.observable();
                self.startDateLbl = ko.observable();
                self.sourceSystemOwnerLbl = ko.observable();
                self.endDateLbl = ko.observable();
                self.personNumberLbl = ko.observable();
                self.elementNameLbl = ko.observable();
                self.recurringEntryLbl = ko.observable();
                self.sourceSystemIDLbl = ko.observable();
                self.legislativeDataGroupNameLbl = ko.observable();
                self.creatorTypeLbl = ko.observable();
                self.inputValueLbl = ko.observable();
                self.typeOfActionLbl = ko.observable();
                self.oprationMessage = ko.observable();
                self.validationMessage = ko.observable();
                self.tracker = ko.observable();
                self.groupValid = ko.observable();
                self.placeholder = ko.observable("Please select a value");
                self.isVisibleSpecialCase = ko.observable(false);
                self.isDisabled = ko.observable(false);
                self.isRequired = ko.observable(true);
                self.currentStepValue = ko.observable('stp1');
                self.addBtnVisible = ko.observable(false);
                self.nextBtnVisible = ko.observable(true);
                self.pageMode = ko.observable();
                self.disableSubmit = ko.observable(false);
                self.messages = ko.observable();
                self.errorMsg = ko.observable();
                self.ok = ko.observable();
                self.addLbl = ko.observable();
                self.editLbl = ko.observable();
                self.gradeLbl = ko.observable();
                self.validGradeLbl = ko.observable();
                self.structureNameLbl = ko.observable();
                self.backLbl = ko.observable();
                self.nextLbl = ko.observable();
                self.submitLbl = ko.observable();
                self.eitCodeLbl = ko.observable();
                self.cancel = ko.observable();
                self.create = ko.observable();
                self.review = ko.observable();
                self.workNatureAllowanceScreenAddLbl = ko.observable();
                self.workNatureAllowanceScreenEditLbl = ko.observable();
                self.addMessageLbl = ko.observable();
                self.validMessageLbl = ko.observable();
                self.editMessageLbl = ko.observable();
                self.confirmMessage = ko.observable();
                self.clothesAllowance = ko.observable();
                self.approvalOrderLbl = ko.observable();
                self.approvalTypeLbl = ko.observable();
                self.roleNameLbl = ko.observable();
                self.specialCaseLbl = ko.observable();
                self.notificationTypeLbl = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.trainVisible = ko.observable(true);
                self.backActionBtnVisible = ko.observable(true);
                self.validNotificationTypeLbl = ko.observable();
                self.lastNotification = ko.observable();
                self.EitNameArr = ko.observableArray([]);
                self.inputValues = ko.observableArray([]);
                self.typeOfActionArr = ko.observableArray([]);
                self.recurringEntryArr = ko.observableArray([]);
                self.personNumberArr = ko.observableArray([]);
                self.eitSegmentArr = ko.observableArray([]);
                self.personDetailArr = ko.observableArray([]);
                self.eitAndPersonArr = ko.observableArray([]);
                self.parameterNameArr = ko.observableArray([]);
                self.elementNameArr = ko.observableArray([]);
                self.elementDetailsArr = ko.observableArray([]);
                self.summaryObservableArray = ko.observableArray([]);
                self.absenceTypeArr = ko.observableArray([]);
                self.personNumberArr.push({
                    value: rootViewModel.personDetails().personId,
                    label: rootViewModel.personDetails().personId
                });
               self.effictiveStartDateLbl=ko.observable();
               self.elementReasonLbl=ko.observable();
                self.stepArray = ko.observableArray([{label: "XXXX", id: 'stp1'},
                    {label: "YYYY", id: 'stp2'}]);

                self.validationModel = {
                    eid_code: ko.observable()
                };

                self.failCbFn = function (err) {
                    self.messages([{
                            severity: 'error',
                            summary: self.errorMsg(),
                            autoTimeout: 5000
                        }]);
                };

                function findElement(arr, propName, propValue) {
                    for (var i = 0; i < arr.length; i++)
                        if (arr[i][propName] == propValue)
                            return arr[i];
                }
                self.absenceType = function(){
                    var getAbsenceTypeCBFN = function(data){
                        for(var i =0 ;i<data.length;i++){
                            self.absenceTypeArr.push({
                                value: data[i].NAME ,
                                label: data[i].NAME 
                            });
                        }
                    }
                    var absenceTypeReportPaylod = {"reportName": "AbsenceTtpeReport"};
                    services.getGenericReport(absenceTypeReportPaylod).then(getAbsenceTypeCBFN, self.failCbFn);
                };

                self.formatDate = function (date) {
                    var month = '' + (date.getMonth() + 1),
                            day = '' + date.getDate(),
                            year = date.getFullYear();
                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;
                    return [year, month, day].join('-');
                };

                var elementNameReportFn = function () {
                    var getElementNameReport = function (data) {
                        self.elementNameArr([]);
                        for (var index = 0; index < data.length; index++) {
                            self.elementNameArr.push({
                                value: data[index].BASE_ELEMENT_NAME,
                                type: data[index].PROCESSING_TYPE,
                                label: data[index].BASE_ELEMENT_NAME
                            });
                        }
                    };
                    var elementNamePaylod = {"reportName": "ElementEntry"};
                    services.getGenericReport(elementNamePaylod).then(getElementNameReport, self.failCbFn);
                };

                var elementDetailsReportFn = function () {
                    var getElementDetailsReport = function (data) {
                        self.inputValues([]);
                        for (var index = 0; index < data.length; index++) {
                            self.inputValues.push({
                                inputValueLbl: data[index].INPUTNAME,
                                inputEitValue: ''
                            });
                        }
                    };
                    var elementNamePaylod = {"reportName": "ElementDetails_Report", "ElementName": self.approvalSetupModel.elementName()};
                    services.getGenericReport(elementNamePaylod).then(getElementDetailsReport, self.failCbFn);
                };


                self.approvalSetupModel = {
                    id: ko.observable(),
                    eitCode: ko.observable(),
                    absenceStatus: ko.observable('SUBMITTED'),
                    absenceType: ko.observable(''),
                    employer: ko.observable(''),
                    startDate: ko.observable(''),
                    endDate: ko.observable(''),
                    sourceSystemOwner: ko.observable('PaaS'),
                    personNumber: ko.observable(rootViewModel.personDetails().personNumber),
                    elementName: ko.observable(''),
                    recurringEntry: ko.observable(''),
                    sourceSystemId: ko.observable(''),
                    legislativeDatagroupName: ko.observable('SA Legislative Data Group'),
                    creatorType: ko.observable('F'),
                    typeOfAction: ko.observable(''),
                    inputValue: ko.observable(''),
                    createdBy: ko.observable(""),
                    creationDate: ko.observable(self.formatDate(new Date())),
                    reason: ko.observable(""),
                    effivtiveStartDate: ko.observable("")
                };

                self.getElementEntries = function () {
                    var getAllElementsCBF = function (data) {
                        if (data.length !== 0) {
                            self.summaryObservableArray(data);
                        } else {
                            self.messages([{
                                    severity: 'error',
                                    summary: self.unknownErrMsg(),
                                    autoTimeout: 5000
                                }]);
                        }
                    };
                    var failCbFn = function () {
                        self.messages([{
                                severity: 'error',
                                summary: self.unknownErrMsg(),
                                autoTimeout: 5000
                            }]);
                    };
                    services.getGeneric(commonhelper.allElementEntries, {}).then(getAllElementsCBF, failCbFn);
                };

                self.backAction = function () {
                    if (oj.Router.rootInstance._navHistory.length > 1) {
                        app.loading(true);
                        oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                    }
                };

                self.handleAttached = function () {
                    self.absenceType();
                    app.loading(false);
                    initTranslations();
                    var arr = JSON.parse(sessionStorage.eitName).map(e => ({label: e.DESCRIPTIVE_FLEX_CONTEXT_NAME, value: e.DESCRIPTIVE_FLEX_CONTEXT_CODE}));
                    self.EitNameArr(arr);
                    elementNameReportFn();
                    self.getElementEntries();
                    positionObj = oj.Router.rootInstance.retrieve();
                    if (positionObj.type == 'ADD') {
                        self.pageMode('ADD');
                        self.oprationMessage(self.addMessageLbl());
                        self.validationMessage(self.validMessageLbl());
                        self.approvalSetupModel.createdBy(rootViewModel.personDetails().personId);
                    } else if (positionObj.type == 'EDIT') {
                        self.pageMode('EDIT');
                        self.connected();
                        self.approvalSetupModel.createdBy(rootViewModel.personDetails().personId);
                        self.oprationMessage(self.editMessageLbl());
                    }
                };

                self.previousStep = function () {
                    var prev = document.getElementById("train").getPreviousSelectableStep();
                    if (prev != null)
                        self.currentStepValue(prev);
                };

                self.nextStep = function () {
                    
                    //General Validation
                    if( self.pageMode()!='EDIT'){
                                            for (var i = 0; i < self.summaryObservableArray().length; i++) {
                        if (self.summaryObservableArray()[i].personNumber === rootViewModel.personDetails().personNumber) {
                            if (self.approvalSetupModel.eitCode() == self.summaryObservableArray()[i].eitCode &&
                                    self.approvalSetupModel.elementName() == self.summaryObservableArray()[i].elementName) {
                                self.messages([{
                                        severity: 'error',
                                        summary: self.eitCodeErrMsg(),
                                        autoTimeout: 5000
                                    }]);
                                return;
                            }
                            if (self.summaryObservableArray()[i].typeOfAction === 'Absence' &&
                                    self.approvalSetupModel.typeOfAction() === 'Absence') {
                                self.messages([{
                                        severity: 'error',
                                        summary: self.absenceErrMsg(),
                                        autoTimeout: 5000
                                    }]);
                                return;
                            }
                        }
                    }
                    }

                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid") {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        return;
                    }
                    for (var i = 0; i < self.summaryObservableArray().length; i++) {

                    }
                    var next = document.getElementById("train").getNextSelectableStep();
                    if (next != null) {
                        self.globalSearch();
                        if (parseInt(countValid) > 0) {
                        } else {
                            var flag = true;
                            if (flag) {
                                self.currentStepValue(next);
                                self.currentStepValueText();
                            }
                        }
                    }
                };

                self.currentStepValueText = function () {
                    if (self.currentStepValue() == 'stp2') {                        
                        self.addBtnVisible(false);
                        self.addBtnVisible(true);                     
                        self.isDisabled(true);                        
                        self.addBtnVisible(true);
                        self.nextBtnVisible(false);
                    } else {
                        self.isDisabled(false);
                        self.addBtnVisible(false);
                        if(self.pageMode==='VIEW'){
                            self.nextBtnVisible(false);
                        }else{
                            self.nextBtnVisible(true);
                        }
                        
                    }
                    return self.clothesAllowance();
                };

                self.typeOfActionChangedHandler = function () {
                    self.approvalSetupModel.absenceType('');
                    self.approvalSetupModel.employer('');
                    self.approvalSetupModel.startDate('');
                    self.approvalSetupModel.endDate('');
                    self.approvalSetupModel.elementName('');
                    self.approvalSetupModel.recurringEntry('');
                    self.approvalSetupModel.sourceSystemId('');
                    self.approvalSetupModel.inputValue('');
                    self.approvalSetupModel.effivtiveStartDate('');
                    self.approvalSetupModel.reason('');
                    self.inputValues([]);
                };

                self.elementNameChangedHandler = function () {
                    var currentElementNameType = findElement(self.elementNameArr(), "value", self.approvalSetupModel.elementName());
                    self.approvalSetupModel.recurringEntry(currentElementNameType["type"]);
                    elementDetailsReportFn();
                };

                self.eitCodeChangedHandler = function () {
                    self.personDetailArr([]);
                    self.eitSegmentArr([]);
                    self.eitAndPersonArr([]);
                    var tempObject = Object.keys(rootViewModel.personDetails());
                    for (var i = 0; i < tempObject.length - 1; i++) {
                        self.personDetailArr.push({
                            value: tempObject[i],
                            label: tempObject[i]
                        });
                        self.eitAndPersonArr.push({
                            value: tempObject[i],
                            label: tempObject[i]
                        });
                    }

                    var segmentTempObject = app.getEITSegments(self.approvalSetupModel.eitCode());
                    for (var i = 0; i < segmentTempObject.length; i++) {
                        self.eitSegmentArr.push({
                            value: segmentTempObject[i].DESCRIPTION,
                            label: segmentTempObject[i].FORM_LEFT_PROMPT
                        });
                       
                        self.eitAndPersonArr.push({
                            value: segmentTempObject[i].DESCRIPTION,
                            label: segmentTempObject[i].FORM_LEFT_PROMPT
                        });
                    }
                      self.eitSegmentArr.push({
                            value: "Transaction_ID",
                            label:"Transaction ID"
                        });
                    
                };

                self.submitButton = function () {
                    document.querySelector("#yesNoDialog").open();
                };
                self.cancelButton = function () {
                    document.querySelector("#yesNoDialog").close();
                };
                self.cancelButtonDialog = function () {
                    document.querySelector("#NoDialog").close();
                    document.querySelector("#yesNoDialog").close();
                };

                //-------------This Function To Stop Train Action If Action Are Not Valid -------
                self.stopSelectListener = function (event, ui) {
                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid") {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        event.preventDefault();
                        return;
                    }
                };

                //------------------ This Section For Call Web Service For Insert Or Update ---------
                function oprationServiceRecord() {
                    self.approvalSetupModel.inputValue(JSON.stringify(self.inputValues()));
                    var jsonData = ko.toJSON(self.approvalSetupModel);
                    if (parseInt(countValid) > 0) {
                        document.querySelector("#yesNoDialog").close();
                        var popup = document.querySelector('#NoDialog');
                        popup.open();
                        self.isDisabled(false);
                    } else {
                        var getValidGradeCBF = function (data) {
                            self.disableSubmit(false);
                            if (oj.Router.rootInstance._navHistory.length > 1) {
                                oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                            } else {
                                oj.Router.rootInstance.go('elementEntry');
                            }
                        };
                        services.addGeneric(commonhelper.elementEntry + self.pageMode(), jsonData).then(getValidGradeCBF, self.failCbFn);
                    }
                }

                self.commitRecord = function (data, event) {
                    if (!self.disableSubmit()) {
                        self.disableSubmit(true);
                        oprationServiceRecord();
                        return true;
                    }
                };

                self.globalSearch = function () {
                    var eit = self.approvalSetupModel.eitCode();
                    self.validationModel.eid_code(eit);
                    var getValidGradeCBF = function (data) {
                        countValid = data.count;
                    };
                    services.searcheGeneric(commonhelper.approvalSetupUrl + commonhelper.validationSetupUrl, self.validationModel).then(getValidGradeCBF);
                };

                self.connected = function () {
                    var elementEntryEditedArray = JSON.parse(localStorage.getItem("elementEntryEditedArray"));
                    var segmentTempObject = app.getEITSegments(elementEntryEditedArray.eitCode);
                    var elementEntryinputValue = JSON.parse(elementEntryEditedArray.inputValue);
                    console.log(elementEntryEditedArray);
                    self.inputValues([]);
                    self.personDetailArr([]);
                    self.eitSegmentArr([]);
                    self.eitAndPersonArr([]);

                    for (var i = 0; i < segmentTempObject.length; i++) {
                        self.eitSegmentArr.push({
                            value: segmentTempObject[i].DESCRIPTION,
                            label: segmentTempObject[i].FORM_LEFT_PROMPT
                        });
                        self.eitAndPersonArr.push({
                            value: segmentTempObject[i].DESCRIPTION,
                            label: segmentTempObject[i].FORM_LEFT_PROMPT
                        });
                    }
                     self.eitSegmentArr.push({
                            value: "Transaction_ID",
                            label:"Transaction ID"
                        });

                    for (var index = 0; index < elementEntryinputValue.length; index++) {
                        self.inputValues.push({
                            inputValueLbl: elementEntryinputValue[index].inputValueLbl,
                            inputEitValue: elementEntryinputValue[index].inputEitValue
                        });
                    }
                           
                    self.approvalSetupModel.id(elementEntryEditedArray.id);
                    self.approvalSetupModel.eitCode(elementEntryEditedArray.eitCode);
                    self.approvalSetupModel.absenceStatus(elementEntryEditedArray.absenceStatus);
                    self.approvalSetupModel.absenceType(elementEntryEditedArray.absenceType);
                    self.approvalSetupModel.employer(elementEntryEditedArray.employer);
                    self.approvalSetupModel.startDate(elementEntryEditedArray.startDate);
                    self.approvalSetupModel.endDate(elementEntryEditedArray.endDate);
                    self.approvalSetupModel.sourceSystemOwner(elementEntryEditedArray.sourceSystemOwner);
                    self.approvalSetupModel.personNumber(elementEntryEditedArray.personNumber);
                    self.approvalSetupModel.elementName(elementEntryEditedArray.elementName);
                    self.approvalSetupModel.recurringEntry(elementEntryEditedArray.recurringEntry);
                    self.approvalSetupModel.sourceSystemId(elementEntryEditedArray.sourceSystemId);
                    self.approvalSetupModel.legislativeDatagroupName(elementEntryEditedArray.legislativeDatagroupName);
                    self.approvalSetupModel.creatorType(elementEntryEditedArray.creatorType);
                    self.approvalSetupModel.typeOfAction(elementEntryEditedArray.typeOfAction);
                    self.approvalSetupModel.creationDate(elementEntryEditedArray.creationDate);
                    self.approvalSetupModel.effivtiveStartDate(elementEntryEditedArray.effivtiveStartDate);
                    self.approvalSetupModel.reason(elementEntryEditedArray.reason);
                    
                };

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        self.stepArray([{label: self.create(), id: 'stp1'},
                            {label: self.review(), id: 'stp2'}]);
                        initTranslations();
                    }
                });

                function initTranslations() {
                    self.effictiveStartDateLbl(getTranslation("elementEntry.effictiveStartDate"));
                    self.elementReasonLbl(getTranslation("elementEntry.elementResson"));
                    self.typeOfActionArr(app.getPaaSLookup('Type_of_Action'));
                    self.recurringEntryArr(app.getPaaSLookup('Recurring_Entry'));
                    self.absenceStatusLbl(getTranslation("elementEntry.absenceStatus"));
                    self.absenceTypeLbl(getTranslation("elementEntry.absenceType"));
                    self.eitCodeErrMsg(getTranslation("others.eitCodeErrMsg"));
                    self.absenceErrMsg(getTranslation("others.absenceErrMsg"));
                    self.employerLbl(getTranslation("elementEntry.employer"));
                    self.startDateLbl(getTranslation("labels.startdate"));
                    self.endDateLbl(getTranslation("labels.enddate"));
                    self.personNumberLbl(getTranslation("others.personNumber"));
                    self.elementNameLbl(getTranslation("labels.elementName"));
                    self.recurringEntryLbl(getTranslation("elementEntry.recurringEntry"));
                    self.sourceSystemIDLbl(getTranslation("elementEntry.sourceSystemID"));
                    self.sourceSystemOwnerLbl(getTranslation("elementEntry.sourceSystemOwner"));
                    self.legislativeDataGroupNameLbl(getTranslation("elementEntry.legislativeDataGroupName"));
                    self.creatorTypeLbl(getTranslation("elementEntry.creatorType"));
                    self.inputValueLbl(getTranslation("elementEntry.inputValue"));
                    self.typeOfActionLbl(getTranslation("validation.typeOFAction"));
                    self.create(getTranslation("labels.create"));
                    self.review(getTranslation("others.review"));
                    self.structureNameLbl(getTranslation("additionalDetails.structureName"));
                    self.backLbl(getTranslation("others.back"));
                    self.nextLbl(getTranslation("others.next"));
                    self.submitLbl(getTranslation("others.submit"));
                    self.ok(getTranslation("others.ok"));
                    self.addLbl(getTranslation("others.add"));
                    self.editLbl(getTranslation("others.edit"));
                    self.validGradeLbl(getTranslation("others.validGrade"));
                    self.gradeLbl(getTranslation("job.grade"));
                    self.eitCodeLbl(getTranslation("approvalScreen.eitCode"));
                    self.cancel(getTranslation("others.cancel"));
                    self.workNatureAllowanceScreenAddLbl(getTranslation("approvalScreen.ApprovalAdd"));
                    self.workNatureAllowanceScreenEditLbl(getTranslation("approvalScreen.ApprovalEdit"));
                    self.addMessageLbl(getTranslation("elementEntry.addElementEntry"));
                    self.validMessageLbl(getTranslation("workNatureAllowance.validMessage"));
                    self.editMessageLbl(getTranslation("elementEntry.editElementEntry"));
                    self.confirmMessage(getTranslation("labels.confirmMessage"));
                    self.yes(getTranslation("others.yes"));
                    self.no(getTranslation("others.no"));
                    self.unknownErrMsg(getTranslation("labels.unknownError"));
                    self.errorMsg(getTranslation("labels.datemessage"));
                    self.approvalOrderLbl(getTranslation("approvalScreen.aprovalOrder"));
                    self.approvalTypeLbl(getTranslation("approvalScreen.approvalType"));
                    self.roleNameLbl(getTranslation("approvalScreen.roleName"));
                    self.specialCaseLbl(getTranslation("approvalScreen.specialCase"));
                    self.notificationTypeLbl(getTranslation("approvalScreen.notificationType"));
                    self.placeholder(getTranslation("labels.placeHolder"));
                    self.validNotificationTypeLbl(getTranslation("workNatureAllowance.notificationMessage"));
                    self.stepArray([{label: self.create(), id: 'stp1'},
                        {label: self.review(), id: 'stp2'}]);
                }

            }

            return elementEntryAddViewModel;
        }
);