/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * homeministryJob module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper',
    'ojs/ojknockout', 'ojs/ojarraydataprovider', 'ojs/ojdatetimepicker', 'ojs/ojtimezonedata', 'jquery-calendar', 'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin',
    'jquery-calendar-picker', 'ojs/ojselectcombobox', 'ojs/ojdialog', 'ojs/ojtrain', 'ojs/ojvalidationgroup'],
        function (oj, ko, $, app, services, commonhelper) {

            function workNatureAllowanceOprationScreen() {
                var self = this;
                var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
                self.columnArray = ko.observableArray();
                self.tracker = ko.observable();
                self.groupValid = ko.observable();
                self.placeholder = ko.observable("Please Select Value");
                self.dataSourceTB2 = ko.observable();
                var deptArray = [];
                self.isDisabled = ko.observable(false);
                self.selctedStruct = ko.observable();
                self.isRequired = ko.observable(true);
                self.currentStepValue = ko.observable('stp1');
                self.addBtnVisible = ko.observable(false);
                self.nextBtnVisible = ko.observable(true);
                self.pageMode = ko.observable();
                self.supervisorHRCYesNoList = rootViewModel.globalHRCYesNo();
                self.oprationMessage = ko.observable();
                self.disableSubmit = ko.observable(false);
                self.formatDate = function (date) {
                    //var date = new Date()
                    var month = '' + (date.getMonth() + 1),
                            day = '' + date.getDate(),
                            year = date.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;

                    return [year, month, day].join('-');
                };

                var Url;
                var positionObj;
                self.stepArray = ko.observableArray([{label: "XXXX", id: 'stp1'},
                    {label: "YYYY", id: 'stp2'}]);//workNatureAllowance
                self.workNatureAllowanceModel = {
                    startDate: ko.observable(oj.IntlConverterUtils.dateToLocalIso(new Date())),
                    endDate: ko.observable(),
                    percentage: ko.observable(),
                    id: ko.observable(),
                    TransActionType: ko.observable(), //Edit View or Add 
                    flag: ko.observable('Y'), //Y or N
                    createdBy: ko.observable(),
                    positionCode: ko.observable(),
                    creationDate: ko.observable(self.formatDate(new Date())),
                    updatedBy: ko.observable(),
                    code: ko.observable()

                };

                self.backAction = function () {
                    if (oj.Router.rootInstance._navHistory.length > 1) {
                        app.loading(true);
                        oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                    }
                };

                self.handleAttached = function () {
                    app.loading(false);
                    initTranslations();
                    positionObj = oj.Router.rootInstance.retrieve();
                    if (positionObj.type == 'ADD') {
                        //hijri date
                        var calendar = $.calendars.instance();
                        var tempDate = calendar.parseDate('mm/dd/yyyy', '');
                        tempDate = calendar.formatDate('yyyy/mm/dd', tempDate);
                        $("#datePickerStartDate").val(tempDate);
                        $("#datePickerEndDate").val(tempDate);
                        $("#datePickerEndDate").calendarsPicker({calendar: $.calendars.instance('ummalqura'),
                            onSelect: function (dates) {
                                var endDate = $('#datePickerEndDate').val();
                                self.workNatureAllowanceModel.endDate(endDate);
                            }});
                        $("#datePickerStartDate").calendarsPicker({calendar: $.calendars.instance('ummalqura'),
                            onSelect: function (dates) {
                                var startDate = $('#datePickerStartDate').val();
                                self.workNatureAllowanceModel.startDate(startDate);
                            }});
                        //end of hijri date
                        self.pageMode('ADD');
                        self.clothesAllowance(self.workNatureAllowanceScreenAddLbl());
                        self.oprationMessage(self.addMassageLbl());
                        self.workNatureAllowanceModel.positionCode(positionObj.code);
                        self.workNatureAllowanceModel.createdBy(rootViewModel.personDetails().personId);
                    } else if (positionObj.type == 'EDIT') {
                        //hijri date
                        $("#datePickerStartDate").val(positionObj.startDate);
                        $("#datePickerEndDate").val(positionObj.endDate);
                        $("#datePickerEndDate").calendarsPicker({calendar: $.calendars.instance('ummalqura'),
                            onSelect: function (dates) {
                                var endDate = $('#datePickerEndDate').val();
                                self.workNatureAllowanceModel.endDate(endDate);
                            }});
                        $("#datePickerStartDate").calendarsPicker({calendar: $.calendars.instance('ummalqura'),
                            onSelect: function (dates) {
                                var startDate = $('#datePickerStartDate').val();
                                self.workNatureAllowanceModel.startDate(startDate);
                            }});

                        //
                        self.pageMode('EDIT');
                        self.clothesAllowance(self.workNatureAllowanceScreenEditLbl());
                        self.workNatureAllowanceModel.createdBy(rootViewModel.personDetails().personId);
                        self.oprationMessage(self.editMassageLbl());
                        self.workNatureAllowanceModel.positionCode(positionObj.positionCode);
                        self.workNatureAllowanceModel.updatedBy(rootViewModel.personDetails().personId);
                        self.workNatureAllowanceModel.startDate(self.formatDate(new Date(positionObj.startDate)));
                        self.workNatureAllowanceModel.percentage(parseInt(positionObj.percentage));
                        self.workNatureAllowanceModel.id(positionObj.id);
                        self.workNatureAllowanceModel.code(positionObj.code);
                        if (positionObj.endDate) {
                            self.workNatureAllowanceModel.endDate(self.formatDate(new Date(positionObj.endDate)));
                        }

                    }

                };

                //-------------This Function For Update Train Step To previousStep------------------
                self.previousStep = function () {
                    var prev = document.getElementById("train").getPreviousSelectableStep();
                    if (prev != null)
                        self.currentStepValue(prev);
                };
                self.nextStep = function ()
                {
                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        return;
                    }
                    var next = document.getElementById("train").getNextSelectableStep();
                    if (next != null)
                        self.currentStepValue(next);
                };

                self.currentStepValueText = function () {
                    if (self.currentStepValue() == 'stp2') {
                        self.isDisabled(true);
                        self.addBtnVisible(true);
                        self.nextBtnVisible(false);
                        $('#datePickerStartDate').calendarsPicker('disable');
                        $('#datePickerEndDate').calendarsPicker('disable');

                    } else {
                        self.isDisabled(false);
                        self.addBtnVisible(false);
                        self.nextBtnVisible(true);
                        $('#datePickerStartDate').calendarsPicker('enable');
                        $('#datePickerEndDate').calendarsPicker('enable');
                    }

                    return self.clothesAllowance();
                };
                //------------------End Of Section -------------------------------------------------
                //------------------This Section For Dialog Action (Open ,Close)
                self.submitButton = function () {
                    document.querySelector("#yesNoDialog").open();
                };
                self.cancelButton = function () {
                    document.querySelector("#yesNoDialog").close();
                };
                //------------------End Of Section ----------------------------------------------
                //-------------This Function To Stop Train Action If Action Are Not Valid -------
                self.stopSelectListener = function (event, ui) {
                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        event.preventDefault();
                        return;
                    }
                };
                //-------------------End Of Function ---------------------------------------------
                //------------------ This Section For Call Web Service For Insert Or Update ---------
                function oprationServiceRecord() {
                    var jsonData = ko.toJSON(self.workNatureAllowanceModel);
                    var getValidGradeCBF = function (data) {
                        var Data = data;
                        self.disableSubmit(false);
                        if (oj.Router.rootInstance._navHistory.length > 1) {
                            oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                        } else {
                            oj.Router.rootInstance.go('home');
                        }
                    };
                    services.addGeneric(commonhelper.workNatureAllowanceUrl + self.pageMode(), jsonData).then(getValidGradeCBF, self.failCbFn);
                }
                ;
                self.commitRecord = function (data, event) {
                    if (!self.disableSubmit()) {
                        app.loading(true);
                        self.disableSubmit(true);
                        oprationServiceRecord();
                        return true;
                    }
                };
                //-------------------End Of Section ------------------------------------------------
                //---------------------Translate Section ----------------------------------------
                self.ok = ko.observable();
                self.startDateLbl = ko.observable();
                self.endDateLbl = ko.observable();
                self.addLbl = ko.observable();
                self.editLbl = ko.observable();
                self.gradeLbl = ko.observable();
                self.validGradeLbl = ko.observable();
                self.structureNameLbl = ko.observable();
                self.backLbl = ko.observable();
                self.nextLbl = ko.observable();
                self.submitLbl = ko.observable();
                self.placeholder = ko.observable("Please Select Value");
                self.percentageLbl = ko.observable();
                self.qualificationNameLbl = ko.observable();
                self.experienceYearsLbl = ko.observable();

                self.cancel = ko.observable();
                self.create = ko.observable();
                self.review = ko.observable();
                self.workNatureAllowanceScreenAddLbl = ko.observable();
                self.workNatureAllowanceScreenEditLbl = ko.observable();
                self.addMassageLbl = ko.observable();
                self.editMassageLbl = ko.observable();
                self.confirmMessage = ko.observable();
                self.clothesAllowance = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                var getTranslation = oj.Translations.getTranslatedString;
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        self.stepArray([{label: self.create(), id: 'stp1'},
                            {label: self.review(), id: 'stp2'}]);
                        initTranslations();
                    }
                });
                function initTranslations() {
                    self.create(getTranslation("labels.create"));
                    self.review(getTranslation("others.review"));
                    self.structureNameLbl(getTranslation("additionalDetails.structureName"));
                    self.backLbl(getTranslation("others.back"));
                    self.nextLbl(getTranslation("others.next"));
                    self.submitLbl(getTranslation("others.submit"));
                    self.ok(getTranslation("others.ok"));
                    self.addLbl(getTranslation("others.add"));
                    self.editLbl(getTranslation("others.edit"));
                    self.validGradeLbl(getTranslation("others.validGrade"));
                    self.startDateLbl(getTranslation("job.startDate"));
                    self.endDateLbl(getTranslation("job.endDate"));
                    self.gradeLbl(getTranslation("job.grade"));
                    self.percentageLbl(getTranslation("position.percentage"));
                    self.cancel(getTranslation("others.cancel"));
                    self.workNatureAllowanceScreenAddLbl(getTranslation("workNatureAllowance.workNatureAllowanceAdd"));
                    self.workNatureAllowanceScreenEditLbl(getTranslation("workNatureAllowance.workNatureAllowanceEdit"));
                    self.addMassageLbl(getTranslation("workNatureAllowance.addMessage"));
                    self.editMassageLbl(getTranslation("workNatureAllowance.editMessage"));
                    self.confirmMessage(getTranslation("labels.confirmMessage"));
                    self.yes(getTranslation("others.yes"));
                    self.no(getTranslation("others.no"));
                    self.stepArray([{label: self.create(), id: 'stp1'},
                        {label: self.review(), id: 'stp2'}]);
                }
                initTranslations();




            }

            return workNatureAllowanceOprationScreen;
        });
