/**
 * 
 * @param {type} oj
 * @param {type} ko
 * @param {type} $
 * @param {type} app
 * @param {type} commonUtil
 * @param {type} services
 * @param {type} translatehelper
 * @returns {JobL#13.JobContentViewModel}
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojknockout', 'ojs/ojtrain', 'ojs/ojbutton',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker',
    'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup', 'ojs/ojcheckboxset', 'ojs/ojradioset',
    'ojs/ojlabel', 'ojs/ojgauge', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojmessage', 'jquery-calendar',
    'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin',
    'jquery-calendar-picker', 'ojs/ojvalidationgroup', 'ojs/ojmessages', 'ojs/ojmessage', 'ojs/ojarraydataprovider'],
        function (oj, ko, $, app, commonUtil, services) {
            /**
             * The view model for the main content view template
             */
            function JobContentViewModel() {
                var self = this;
                var operationType;
                self.disableSubmit = ko.observable(false);
                self.disableSubmitEdit = ko.observable(false);
                self.endDate = ko.observable();
                self.reportName = ko.observable();
                self.Parameter1 = ko.observable();
                self.Parameter2 = ko.observable();
                self.Parameter3 = ko.observable();
                self.Parameter4 = ko.observable();
                self.Parameter5 = ko.observable();
                self.createReport = ko.observable();
                self.reportNameVal = ko.observable("");
                self.Parameter1Val = ko.observable("");
                self.Parameter2Val = ko.observable("");
                self.Parameter3Val = ko.observable("");
                self.Parameter4Val = ko.observable("");
                self.Parameter5Val = ko.observable("");
                self.reportTypeVal = ko.observable("");
                self.reportTitleLabl = ko.observable();
                self.reportTypelbl = ko.observable();
                self.reportTypeArr=ko.observableArray();
                 self.trainVisible = ko.observable(true);
                self.addJob = ko.observable();
                self.updateJob = ko.observable();
                self.viewJob = ko.observable();
                self.jobTitel = ko.observable();
                self.trainView = ko.observable();
                self.confirmMessage = ko.observable();
                self.addMessage = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.pervious = ko.observable();
                self.next = ko.observable();
                self.cancel = ko.observable();
                self.submit = ko.observable();
                self.create = ko.observable();
                self.review = ko.observable();
                self.ok = ko.observable();
                self.oprationMessage = ko.observable();
                self.addMassageLbl = ko.observable();
                self.editMassageLbl = ko.observable();
                self.placeholder = ko.observable();
                self.date1 = ko.observable(oj.IntlConverterUtils.dateToLocalIso(new Date()));

                self.serviceType = ko.observable();
                self.ministryJobModle = ko.observable();
                self.groupValid = ko.observable();
                self.jnVisibility = ko.observable();
                self.jnVisibility = ko.observable();
                self.trainVisibility = ko.observable();
                self.tracker = ko.observable();
                self.nextBtnVisible = ko.observable(true);
                self.addBtnVisible = ko.observable(false);
                self.columnArray = ko.observableArray();
                self.isRequired = ko.observable(true);
                self.stepArray = ko.observableArray([]);
                self.messages = ko.observable();
                self.errormessage = ko.observable();
                self.errormessagecheckInDatabase = ko.observable();
                self.validationMessage = ko.observable();
                self.oprationMessageUpdate = ko.observable();
                self.reportNamelbl = ko.observable();
                self.fristKeyValueLBl = ko.observable();
                self.eitSegmentArr = ko.observableArray();
                self.eitSegmentArr = ko.observableArray();
                self.operationArr  = ko.observableArray();
                self.checkvalidation=ko.observable(); 
                
                self.isDisabled=ko.observable(false);
                    
              
                

                self.currentStepValue = ko.observable('stp1');
                self.hidecancel = ko.observable(true);
                self.hidepervious = ko.observable(false);
                self.hidenext = ko.observable(true);
                self.hidesubmit = ko.observable(false);
                self.hideUpdatesubmit = ko.observable(false);
                self.messages = ko.observable();
               // new define 
               self.EitCodeArr=ko.observableArray();
               self.fristKeyArr=ko.observableArray();
               self.EitCodeArr(app.globalEitNameReport());
              //new method 
                var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
              
          


              // 
               self.validationModle = ko.observable();
               self.validationModle = {
                    eitCodeVal: ko.observable(),
                    fristKeyVal: ko.observable(),
                    fristKeyvalue:ko.observable(),
                    eitSegmentVal:ko.observable(),
                    operation:ko.observable(),
                    secondtKeyVal:ko.observable(),
                    secondtKeyValue:ko.observable(),
                    secondEitSegmentVal:ko.observable(),
                    approvalCode:ko.observable()
                  
                };
                self.structureChangedHandler = function (event, data) {
                     self.eitSegmentArr([]);
                    var tempObject = app.getEITSegments(self.validationModle.eitCodeVal());
                    for (var i = 0; i < tempObject.length; i++) {
                        self.eitSegmentArr.push({
                            value: tempObject[i].DESCRIPTION,
                            label: tempObject[i].DESCRIPTION
                        });
                    }
                };
  
                self.previousStep = function () {
                    self.hidecancel(true);
                    self.hidepervious(false);
                    self.hidesubmit(false);
                    self.hideUpdatesubmit(false);
                    self.hidenext(true);
                    self.isDisabled(false);
                    $(".disable-element").prop('disabled', false);
                    self.currentStepValue('stp1');

                };

                  self.nextStep = function () {
                          var tracker = document.getElementById("tracker");
                      
                        if (tracker.valid !== "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        return;
                    }
                    else {
                        
                        if(self.serviceType()==='ADD'){
                           self.getApprovalCount();                         
                         
                        }else if(self.serviceType()==='update'){
                            self.hidesubmit(false);
                            self.hideUpdatesubmit(true);
                            self.isDisabled(true);
                            self.hidepervious(true);
                            self.hidenext(false);
                            $(".disable-element").prop('disabled', false);
                            self.currentStepValue('stp2');
                        }                 
                    } 
                
                   
                     
                  };


                self.disableInput = function () {
                    self.hidecancel(false);
                    self.hidepervious(true);
                    self.hidenext(false);

                    $(".disable-element").prop('disabled', true);
                    self.currentStepValue('stp2');
                };

                self.cancelAction = function () {

                    oj.Router.rootInstance.go('ApprovalConditionSummary');

                };


                self.submitButton = function () {
                    document.querySelector("#yesNoDialog").open();

                };
                self.commitRecordupdate = function () {
                    self.updateApprovalCondition();
                    self.disableSubmitEdit(true);

                };
                self.cancelButtonupdate = function () {
                    document.querySelector("#yesNoDialogUpdate").close();

                };
            
          
               self.submitUpdateButton=function(){
                  document.querySelector("#yesNoDialogUpdate").open();  
               };
              self.getApprovalCount=function(){
                 var ApprovalConditionCbFn=function(data){
               
                      if(data=="0"){
                           self.hideUpdatesubmit(false);
                            self.hidesubmit(true);
                            self.isDisabled(true);
                            self.hidepervious(true);
                            self.hidenext(false);
                            $(".disable-element").prop('disabled', false);
                            self.currentStepValue('stp2');
                          
                      }else{
                          $.notify(self.checkvalidation(), "error");  
                      }
                  };
                services.getGeneric(commonUtil.getCountApprovalCondition+self.validationModle.approvalCode()).then(ApprovalConditionCbFn, app.failCbFn);     
              };

             self.updateApprovalCondition=function(){
                 var payload={
                    "eitCode":self.validationModle.eitCodeVal(),
                   "firstKeyType":self.validationModle.fristKeyVal(),  
                   "firstKeyValue":self.validationModle.fristKeyvalue(),  
                   "firstEitSegment":self.validationModle.eitSegmentVal(),  
                   "operation":self.validationModle.operation(),  
                   "secondKeyType":self.validationModle.secondtKeyVal(),  
                   "secondKeyValue":self.validationModle.secondtKeyValue(),
                   "secondEitSegment":self.validationModle.secondEitSegmentVal(),
                   "approvalCode":self.validationModle.approvalCode(),
                   "id":self.retrieve.id
                   
                 };
                 var ApprovalConditionCbFn=function(data){
                      document.querySelector("#yesNoDialogUpdate").close();  
                      oj.Router.rootInstance.go('ApprovalConditionSummary');
                 };
                 
                 
                    services.addGeneric(commonUtil.updateApprovalCondition, JSON.stringify(payload)).then(ApprovalConditionCbFn, app.failCbFn);  
                 
             };   

            
             self.addApprovalCondition=function(){
                 
                 var payload={
                    "eitCode":self.validationModle.eitCodeVal(),
                   "firstKeyType":self.validationModle.fristKeyVal(),  
                   "firstKeyValue":self.validationModle.fristKeyvalue(),  
                   "firstEitSegment":self.validationModle.eitSegmentVal(),  
                   "operation":self.validationModle.operation(),  
                   "secondKeyType":self.validationModle.secondtKeyVal(),  
                   "secondKeyValue":self.validationModle.secondtKeyValue(),
                   "secondEitSegment":self.validationModle.secondEitSegmentVal(),
                   "approvalCode":self.validationModle.approvalCode()
                   
                 };
                 var ApprovalConditionCbFn=function(data){
                      document.querySelector("#yesNoDialog").close();  
                      oj.Router.rootInstance.go('ApprovalConditionSummary');
                 };
                 
                 
                    services.addGeneric(commonUtil.addApprovalCondition, JSON.stringify(payload)).then(ApprovalConditionCbFn, app.failCbFn);
             };

                self.commitRecord = function () {
                    
                    self.addApprovalCondition();
                    self.disableSubmit(true);
                };


                self.cancelButton = function () {

                    document.querySelector("#yesNoDialog").close();
                };

                self.commitDraft = function (data, event) {
                    return true;
                };
                self.stopSelectListener = function () {
                    var tracker = document.getElementById("tracker");

                    if (tracker.valid !== "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        event.preventDefault();
                        return;
                    }

                };
                self.currentStepValueText = function () {
                    if (self.currentStepValue() == 'stp2') {
                        $(".disable-element").prop('disabled', true);
                    } else {
                        $(".disable-element").prop('disabled', false);
                    }

                };

                self.handleAttached = function (info) {
                    
                   
                    app.loading(false);
                      
                     self.retrieve = oj.Router.rootInstance.retrieve();
                     
                    if (self.retrieve === 'ADD') {
                        self.serviceType('ADD');
                        self.hidecancel(true);
                        self.hidepervious(false);
                        self.hidenext(true);
                        self.hidesubmit(false);
                        $(".disable-element").prop('disabled', false);
                        self.currentStepValue('stp1');


                    } else if (self.retrieve.type === 'view'){
                        
                 self.validationModle = {
                            eitCodeVal: ko.observable(self.retrieve.eitCode),
                            fristKeyVal: ko.observable(self.retrieve.firstKeyType),
                            fristKeyvalue: ko.observable(self.retrieve.firstKeyValue),
                            eitSegmentVal: ko.observable(self.retrieve.firstEitSegment),
                            operation: ko.observable(self.retrieve.operation),
                            secondtKeyVal: ko.observable(self.retrieve.secondKeyType),
                            secondtKeyValue: ko.observable(self.retrieve.secondKeyValue),
                            secondEitSegmentVal: ko.observable(self.retrieve.secondEitSegment),
                            approvalCode: ko.observable(self.retrieve.approvalCode)

                        }; 
                    
                    
                      self.isDisabled(true);
                      self.hidenext(false);
                      self.trainVisible(false);
                      self.currentStepValue('stp2');
                    }else if (self.retrieve.type === 'update') {
                        self.serviceType('update');

                        self.validationModle = {
                            eitCodeVal: ko.observable(self.retrieve.eitCode),
                            fristKeyVal: ko.observable(self.retrieve.firstKeyType),
                            fristKeyvalue: ko.observable(self.retrieve.firstKeyValue),
                            eitSegmentVal: ko.observable(self.retrieve.firstEitSegment),
                            operation: ko.observable(self.retrieve.operation),
                            secondtKeyVal: ko.observable(self.retrieve.secondKeyType),
                            secondtKeyValue: ko.observable(self.retrieve.secondKeyValue),
                            secondEitSegmentVal: ko.observable(self.retrieve.secondEitSegment),
                            approvalCode: ko.observable(self.retrieve.approvalCode)

                        };         
                    }
                     self.eitSegmentArr([]);
                        var tempObject = app.getEITSegments(self.validationModle.eitCodeVal());
                        for (var i = 0; i < tempObject.length; i++) {
                            self.eitSegmentArr.push({
                                value: tempObject[i].DESCRIPTION,
                                label: tempObject[i].DESCRIPTION
                            });
                        }
                };


                 // define label
                 self.roleNamelbl=ko.observable();
                 self.firstKeyTypeLbl=ko.observable();
                 self.firstKeyValueLbl=ko.observable();
                 self.personalInfromationLbl=ko.observable();
                 self.operationLbl=ko.observable();
                 self.secondKeyValueLbl = ko.observable();
                 self.secondpersonalInfromationLbl = ko.observable();
                 self.errorMassageEnLbl = ko.observable();
                 self.errorMassageArLbl = ko.observable(); 
                 self.arabicErrorMessageLbl = ko.observable(); 
                 self.fristKeyTypelbl = ko.observable(); 
                 self.fristKeyValueelbl = ko.observable(); 
                 self.eitSegmentLbl = ko.observable();
                 self.operationLbl = ko.observable();
                 self.secondKeyTypelabel = ko.observable();
                 self.secondKeyValueelbl = ko.observable();
                 self.secondEitSegmentbl = ko.observable();
                 self.approvalCodeLbl = ko.observable();
                 
                 
              
                 self.EitCodeLBl=ko.observable();
              
              
                var getTranslation = oj.Translations.getTranslatedString;
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });
                function initTranslations() {
                     self.fristKeyArr(app.getPaaSLookup('ApprovalConditionFristKey'));
                     self.operationArr(app.getPaaSLookup('OPERATION'));
                     self.reportTypeArr(app.getPaaSLookup('reportType'));
                       self.retrieve = oj.Router.rootInstance.retrieve();
                    if (self.retrieve === 'ADD') {
                        self.reportTitleLabl(getTranslation("report.addApprovalCondition"));

                    } else if (self.retrieve.type === 'view') {
                        self.reportTitleLabl(getTranslation("report.viewApprovalCondition"));
                    } else if (self.retrieve.type === 'update') {
                        self.reportTitleLabl(getTranslation("report.editApprovalCondition"));
                    }
            
                    self.reportTypelbl(getTranslation("report.reportType"));
                    self.errormessagecheckInDatabase(getTranslation("report.errormessagecheckInDatabase"));
                    self.oprationMessage(getTranslation("report.oprationMessage"));
                    self.confirmMessage(getTranslation("report.confirmMessage"));
                    self.oprationMessageUpdate(getTranslation("report.oprationMessageUpdate"));
                    self.yes(getTranslation("report.yes"));
                    self.no(getTranslation("report.no"));
                    self.addJob(getTranslation("job.addJob"));
                    self.updateJob(getTranslation("job.updateJob"));
                    self.viewJob(getTranslation("job.viewJob"));
                    self.addMassageLbl(getTranslation("job.addMessage"));
                    self.editMassageLbl(getTranslation("job.editMessage"));

                    self.trainView(getTranslation("others.review"));

                    self.pervious(getTranslation("others.pervious"));
                    self.next(getTranslation("others.next"));
                    self.cancel(getTranslation("others.cancel"));

                    self.submit(getTranslation("others.submit"));

                    self.create(getTranslation("labels.create"));
                    self.review(getTranslation("others.review"));
                    self.ok(getTranslation("others.ok"));
                   // new for Role 
                   self.roleNamelbl(getTranslation("report.roleName"));
                   self.firstKeyTypeLbl(getTranslation("validation.firstKeyType"));
                   self.firstKeyValueLbl(getTranslation("validation.firstKeyValue"));
                   self.personalInfromationLbl(getTranslation("validation.personalInfromation"));
                   self.operationLbl(getTranslation("validation.operation"));
                    self.secondKeyValueLbl(getTranslation("validation.secondKeyValue"));
                    self.secondpersonalInfromationLbl(getTranslation("validation.secondpersonalInfromation"));
                    self.errorMassageEnLbl(getTranslation("validation.errorMassageEn"));
                    self.arabicErrorMessageLbl(getTranslation("validation.arabicErrorMessage"));
                    self.errorMassageArLbl(getTranslation("validation.errorMassageAr"));
                    self.placeholder(getTranslation("labels.placeHolder"));
                    //new approval Condition
                    self.EitCodeLBl(getTranslation("labels.EitCodeLBl"));
                    self.fristKeyValueLBl(getTranslation("labels.EitCodeLBl"));
                    self.fristKeyTypelbl(getTranslation("report.fristKeyType"));
                    self.fristKeyValueelbl(getTranslation("validation.firstKeyValue"));
                    self.eitSegmentLbl(getTranslation("report.fristEitSegmentLbl"));
                    self.operationLbl(getTranslation("validation.operation"));
                    self.secondKeyTypelabel(getTranslation("validation.secondKeyType"));
                    self.secondKeyValueelbl(getTranslation("validation.secondKeyValue"));
                    self.secondEitSegmentbl(getTranslation("report.secondEitSegmentbl"));
                    self.approvalCodeLbl(getTranslation("report.approvalCode"));
                     self.createReport(getTranslation("report.create"));
                     self.checkvalidation(getTranslation("report.ApprovalCodeValidation"));
                   

                    self.stepArray([{label: self.createReport(), id: 'stp1'},
                        {label: self.trainView(), id: 'stp2'}
                    ]);
                }
                initTranslations();
            }
            return JobContentViewModel;
        });
