/**
 * @license
 * Copyright (c) 2014, 2018, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your about ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper',
    'ojs/ojknockout', 'ojs/ojarraydataprovider', 'ojs/ojdatetimepicker', 'ojs/ojtimezonedata',
    'ojs/ojselectcombobox', 'ojs/ojdialog', 'ojs/ojtrain', 'ojs/ojvalidationgroup'],
        function (oj, ko, $, app, services, commonhelper) {

            function AboutViewModel() {
                var self = this;
                var Typs = ['date', 'date', 'number'];
                self.ministryPositionbSearch = {
                    taxonomicCode: ko.observable(),
                    ranked: ko.observable(),
                    postionNumber: ko.observable(11),
                    managing: ko.observable(),
                    positionName: ko.observable(),

                };
                var Keys = ['startDate', 'endDate', 'percentage', 'id', 'flag', 'createdBy', 'positionCode', 'creationDate',
                    'updatedBy'];
                var VALUES = ['', '', 500, 1, 'Y', '123456789', '88', '10-10-2010',
                    'updatedBy'];
                self.dataArray = [{value: 'option1', label: 'Option 1'},
                    {value: 'option2', label: 'Option 2', disabled: true},
                    {value: 'option3', label: 'Option 3'}];
                var key = 'dynamicKey';
                var key2 = 'dynamicKey2';
                self.o = {};
                self.handleAttached = function (info) {



//        var getEit = function (data) {
//        };
//        services.getEIT('xx').then(getEit, app.failCbFn);
                    var x1 = 'select  PERSON_ID  value , FULL_NAME label from PER_PERSON_NAMES_F where 1=1 And ROWNUM <= 5';

                    var getDynamicReport = function (data) {
                    };
                    var ggg = function (data) {
                    };
                    services.getEitNameReportReport(app.lang).then(ggg, app.failCbFn);
                    services.getAbsenceReport().then(getDynamicReport, app.failCbFn);
                    for (var i = 0; i < Keys.length; i++) {
                        self.o[Keys[i]] = ko.observable(VALUES[i]);
                    }
                    for (var i = 0; i < Typs.length; i++) {
                        // self.o[Keys[i]] =ko.observable();//div
                        var c = self.o[Keys[i]];
                        var zzz = 'o.' + Keys[i];
                        //Add Dynamic----------------------
                        var input = document.createElement("div");
                        input.setAttribute('class', 'oj-flex-item');
                        var parent = document.getElementById("xx");
                        parent.appendChild(input);
                        //----------------End 
//             var input = document.createElement("label");
//           input.setAttribute('data-bind =text', 'start Date');
//           var parent = document.getElementById("xx");
//           parent.appendChild(input);
                        if (Typs[i] == "text") {
                            var input = document.createElement("oj-input-text");
                            input.setAttribute('disabled', 'false');
                            input.setAttribute('value', '{{o.date}}');//ministryPositionbSearch.postionNumber         
                            var parent = document.getElementById("xx");
                            parent.appendChild(input);
                        } else if (Typs[i] == "number") {
                            var input = document.createElement("oj-input-number");
                            input.setAttribute('disabled', 'false');
                            input.setAttribute('value', '{{' + zzz + '}}');
                            var parent = document.getElementById("xx");
                            parent.appendChild(input);
                        } else if (Typs[i] == "list") {
                            var input = document.createElement("oj-select-one");
                            input.setAttribute('disabled', 'false');
                            input.setAttribute('value', '{{ministryPositionbSearch.managing}}');
                            input.setAttribute('options', '{{dataArray}}');
                            var parent = document.getElementById("xx");
                            parent.appendChild(input);
                        } else if (Typs[i] == "date") {
                            var input = document.createElement("oj-input-date");
                            input.setAttribute('disabled', 'false');
                            input.setAttribute('value', '{{' + zzz + '}}');
                            input.setAttribute('required', 'true');
                            var parent = document.getElementById("xx");
                            parent.appendChild(input);
                        }
                    }

                };
                var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
                self.columnArray = ko.observableArray();
                self.tracker = ko.observable();
                self.groupValid = ko.observable();
                self.placeholder = ko.observable("Please Select Value");
                self.dataSourceTB2 = ko.observable();
                var deptArray = [];
                self.isDisabled = ko.observable(false);
                self.selctedStruct = ko.observable();
                self.isRequired = ko.observable(true);
                self.currentStepValue = ko.observable('stp1');
                self.addBtnVisible = ko.observable(false);
                self.nextBtnVisible = ko.observable(true);
                self.pageMode = ko.observable();
                self.supervisorHRCYesNoList = rootViewModel.globalHRCYesNo();
                self.oprationMessage = ko.observable();
                self.disableSubmit = ko.observable(false);

                var Url;
                var positionObj;
                self.stepArray = ko.observableArray([{label: "XXXX", id: 'stp1'},
                    {label: "YYYY", id: 'stp2'}]);//workNatureAllowance
                self.workNatureAllowanceModel = {
                    startDate: ko.observable(oj.IntlConverterUtils.dateToLocalIso(new Date())),
                    endDate: ko.observable(),
                    percentage: ko.observable(),
                    id: ko.observable(),
                    TransActionType: ko.observable(), //Edit View or Add 
                    flag: ko.observable('Y'), //Y or N
                    createdBy: ko.observable(),
                    positionCode: ko.observable(),
                    creationDate: ko.observable('2010-10-10'),
                    updatedBy: ko.observable()

                };

                self.backAction = function () {
                    if (oj.Router.rootInstance._navHistory.length > 1) {
                        app.loading(true);
                        oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                    }
                };
                self.formatDate = function (date) {
                    //var date = new Date()
                    var month = '' + (date.getMonth() + 1),
                            day = '' + date.getDate(),
                            year = date.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;

                    return [year, month, day].join('-');
                };
//                self.handleAttached = function () {
//                    initTranslations();
//                    positionObj = oj.Router.rootInstance.retrieve();
//                    if (positionObj.type == 'ADD') {
//                        self.pageMode('ADD');
//                        self.clothesAllowance(self.workNatureAllowanceScreenAddLbl());
//                        self.oprationMessage(self.addMassageLbl());
//                        self.workNatureAllowanceModel.positionCode(positionObj.code);
//                        self.workNatureAllowanceModel.createdBy(rootViewModel.personDetails().personId());
//                    } else if (positionObj.type == 'EDIT') {
//                        self.pageMode('EDIT');
//                        self.clothesAllowance(self.workNatureAllowanceScreenEditLbl());
//                        self.workNatureAllowanceModel.createdBy(rootViewModel.personDetails().personId());
//                        self.oprationMessage(self.editMassageLbl());
//                        self.workNatureAllowanceModel.positionCode(positionObj.positionCode);
//                        self.workNatureAllowanceModel.updatedBy(rootViewModel.personDetails().personId());
//                        self.workNatureAllowanceModel.startDate(self.formatDate(new Date (positionObj.startDate)));
//                        self.workNatureAllowanceModel.percentage(parseInt( positionObj.percentage));
//                        self.workNatureAllowanceModel.id(positionObj.id);
//                        if(positionObj.endDate){
//                           self.workNatureAllowanceModel.endDate(self.formatDate(new Date (positionObj.endDate)));
//                        }
//                    }
//
//                };

                //-------------This Function For Update Train Step To previousStep------------------
                self.previousStep = function () {
                    var prev = document.getElementById("train").getPreviousSelectableStep();
                    if (prev != null)
                        self.currentStepValue(prev);
                };
                self.nextStep = function ()
                {
                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        return;
                    }
                    var next = document.getElementById("train").getNextSelectableStep();
                    if (next != null)
                        self.currentStepValue(next);

                };

                self.currentStepValueText = function () {
                    if (self.currentStepValue() == 'stp2') {
                        self.isDisabled(true);
                        self.addBtnVisible(true);
                        self.nextBtnVisible(false);

                    } else {
                        self.isDisabled(false);
                        self.addBtnVisible(false);
                        self.nextBtnVisible(true);
                    }

                    return self.clothesAllowance();
                };
                //------------------End Of Section -------------------------------------------------
                //------------------This Section For Dialog Action (Open ,Close)
                self.submitButton = function () {
                    document.querySelector("#yesNoDialog").open();
                };
                self.cancelButton = function () {
                    document.querySelector("#yesNoDialog").close();
                };
                //------------------End Of Section ----------------------------------------------
                //-------------This Function To Stop Train Action If Action Are Not Valid -------
                self.stopSelectListener = function (event, ui) {
                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        event.preventDefault();
                        return;
                    }
                };
                //-------------------End Of Function ---------------------------------------------
                //------------------ This Section For Call Web Service For Insert Or Update ---------
                function oprationServiceRecord() {
                    var jsonData = ko.toJSON(self.o);
                    var getValidGradeCBF = function (data) {
                        var Data = data;
                        self.disableSubmit(false);
                        if (oj.Router.rootInstance._navHistory.length > 1) {
                            oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                        } else {
                            oj.Router.rootInstance.go('home');
                        }
                    };
                    // services.addGeneric(commonhelper.workNatureAllowanceUrl+'ADD', jsonData).then(getValidGradeCBF, self.failCbFn);
                }
                ;
                self.commitRecord = function (data, event) {
                    if (!self.disableSubmit()) {
                        self.disableSubmit(true);
                        oprationServiceRecord();
                        return true;
                    }
                };
                //-------------------End Of Section ------------------------------------------------
                //---------------------Translate Section ----------------------------------------
                self.ok = ko.observable();
                self.startDateLbl = ko.observable();
                self.endDateLbl = ko.observable();
                self.addLbl = ko.observable();
                self.editLbl = ko.observable();
                self.gradeLbl = ko.observable();
                self.validGradeLbl = ko.observable();
                self.structureNameLbl = ko.observable();
                self.backLbl = ko.observable();
                self.nextLbl = ko.observable();
                self.submitLbl = ko.observable();
                self.placeholder = ko.observable("Please Select Value");
                self.percentageLbl = ko.observable();
                self.qualificationNameLbl = ko.observable();
                self.experienceYearsLbl = ko.observable();

                self.cancel = ko.observable();
                self.create = ko.observable();
                self.review = ko.observable();
                self.workNatureAllowanceScreenAddLbl = ko.observable();
                self.workNatureAllowanceScreenEditLbl = ko.observable();
                self.addMassageLbl = ko.observable();
                self.editMassageLbl = ko.observable();
                self.confirmMessage = ko.observable();
                self.clothesAllowance = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                var getTranslation = oj.Translations.getTranslatedString;
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        self.stepArray([{label: self.create(), id: 'stp1'},
                            {label: self.review(), id: 'stp2'}]);
                        initTranslations();
                    }
                });
                function initTranslations() {
                    self.create(getTranslation("labels.create"));
                    self.review(getTranslation("others.review"));
                    self.structureNameLbl(getTranslation("additionalDetails.structureName"));
                    self.backLbl(getTranslation("others.back"));
                    self.nextLbl(getTranslation("others.next"));
                    self.submitLbl(getTranslation("others.submit"));
                    self.ok(getTranslation("others.ok"));
                    self.addLbl(getTranslation("others.add"));
                    self.editLbl(getTranslation("others.edit"));
                    self.validGradeLbl(getTranslation("others.validGrade"));
                    self.startDateLbl(getTranslation("job.startDate"));
                    self.endDateLbl(getTranslation("job.endDate"));
                    self.gradeLbl(getTranslation("job.grade"));
                    self.percentageLbl(getTranslation("position.percentage"));
                    self.cancel(getTranslation("others.cancel"));
                    self.workNatureAllowanceScreenAddLbl(getTranslation("workNatureAllowance.workNatureAllowanceAdd"));
                    self.workNatureAllowanceScreenEditLbl(getTranslation("workNatureAllowance.workNatureAllowanceEdit"));
                    self.addMassageLbl(getTranslation("workNatureAllowance.addMessage"));
                    self.editMassageLbl(getTranslation("workNatureAllowance.editMessage"));
                    self.confirmMessage(getTranslation("labels.confirmMessage"));
                    self.yes(getTranslation("others.yes"));
                    self.no(getTranslation("others.no"));
                    self.stepArray([{label: self.create(), id: 'stp1'},
                        {label: self.review(), id: 'stp2'}]);
                }
                initTranslations();



            }

            return new AboutViewModel();
        }
);
