/**
 * 
 * @param {type} oj
 * @param {type} ko
 * @param {type} $
 * @param {type} app
 * @param {type} commonUtil
 * @param {type} services
 * @param {type} translatehelper
 * @returns {JobL#13.JobContentViewModel}
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojknockout', 'ojs/ojtrain', 'ojs/ojbutton',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker',
    'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup', 'ojs/ojcheckboxset', 'ojs/ojradioset',
    'ojs/ojlabel', 'ojs/ojgauge', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojmessage', 'jquery-calendar',
    'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin',
    'jquery-calendar-picker', 'ojs/ojvalidationgroup'],
        function (oj, ko, $, app, commonUtil, services) {



            /**
             * The view model for the main content view template
             */
            function JobContentViewModel() {
                var self = this;
                // var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
//       self.handleAttached = function (info) {};


                self.handleAttached = function (info) {
                    app.loading(false);
                    self.getLocale = function () {
                        return oj.Config.getLocale();

                    };


                    if (self.getLocale() === "ar") {
                        $('.approval-card-body').removeClass('app-crd-bdy-border-en');
                        $('.approval-card-body').addClass('app-crd-bdy-border-ar');
                        $('.app-crd-type').removeClass('app-card-for-en');
                        $('.app-crd-type').addClass('app-card-for-ar');
                        $('.blockquote-footer').removeClass('app-card-for-en');
                        $('.blockquote-footer').addClass('app-card-for-ar');

                    } else {
                        $('.approval-card-body').removeClass('app-crd-bdy-border-ar');
                        $('.approval-card-body').addClass('app-crd-bdy-border-en');
                        $('.app-crd-type').removeClass('app-card-for-ar');
                        $('.app-crd-type').addClass('app-card-for-en');
                        $('.blockquote-footer').removeClass('app-card-for-ar');
                        $('.blockquote-footer').addClass('app-card-for-en');
                    }


                };

            }
            return JobContentViewModel;
        });
