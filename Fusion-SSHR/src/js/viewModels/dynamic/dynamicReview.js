/**
 * @license
 * Copyright (c) 2014, 2018, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your about ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper',
    'config/buildScreen', 'ojs/ojknockout', 'ojs/ojarraydataprovider', 'ojs/ojdatetimepicker', 'ojs/ojtimezonedata', 'ojs/ojbutton',
    'ojs/ojselectcombobox', 'ojs/ojdialog', 'ojs/ojtrain', 'ojs/ojvalidationgroup', 'ojs/ojformlayout', , 'ojs/ojselectcombobox', 'ojs/ojtimezonedata', 'ojs/ojdatetimepicker','ojs/ojfilepicker','ojs/ojinputtext'],
        function (oj, ko, $, app, services, commonhelper, buildScreen) {

            function dynamicOperationScreenViewModel() {
                var self = this;
                self.lang = "";
                var Typs = ['date', 'date', 'number'];
                self.pageMode = ko.observable();
                var Keys = [];
                var PaaSKeys = [];
                self.isLastApprover = ko.observable(false);
                var disabeld = [];
                var VALUES = [];
                self.dataArray = [{value: 'zz', label: 'zz'}];
                self.validationCheck = ko.observable(false);
                self.currentValueText = ko.observable();
                self.rejectDialogBody = ko.observable();
                self.valueText = ko.observable();
                self.url;
                self.actionModelsArr = [];
                self.reqestedID;
                self.workFlowApprovalId=ko.observable();
                var tempObject;
                var approvalManagmant;
                self.reqeustedPersonNumber;
                var key = 'dynamicKey';
                var key2 = 'dynamicKey2';
                self.isDisabledx = {};
                var ArrKeys = [];
                self.Arrs = {};
                self.model = {};
                self.PaaSmodel = {};
                self.approvlModel = {};
                var globalRowData;
                self.actionAvilable = ko.observable(true);
                self.getPaaSEitesValues = function () {
                    var getPaasEitsValuesCBFN = function (data) {
                        globalRowData=data;
                        //console.log(data);
                        self.reqeustedPersonNumber = data.person_number;
                        var jsonData = JSON.parse(data.eit);
                        var values = JSON.parse(jsonData);
                        for (var i = 0; i < Object.keys(self.model).length; i++) {
                            for (var j = 0; j < Object.keys(values).length; j++) {

                                if (Object.keys(self.model)[i] == Object.keys(values)[j]) {

                                    self.model[Object.keys(self.model)[i]](values[Object.keys(values)[j]]);
                                    //For Set Dependent List 
                                    if (tempObject[i].value_SET_TYPE == "X" && tempObject[i + 1].value_SET_TYPE == "Y") {
                                        var getDependentReportCBCF = function (data) {
                                            var tempObject2 = data;
                                            var arr = tempObject2;
                                            var arrays = [];
                                            if (arr.length) {
                                                for (var ind = 0; ind < arr.length; ind++) {
                                                    arrays.push({"value": arr[ind].FLEX_value, "label": arr[ind].value_DESCRIPTION})
                                                }
                                            } else {
                                                arrays.push({"value": arr.FLEX_value, "label": arr.value_DESCRIPTION});
                                            }
                                            self.Arrs[tempObject[i + 1].FLEX_value_SET_NAME + tempObject[i + 1].DESCRIPTION + "Arr"](arrays);
                                        }
                                        var xx = {"reportName": "DependentReport", "valueSet": tempObject[i + 1].FLEX_value_SET_NAME, "parent": self.model[Object.keys(self.model)[i]]()};
                                        services.getGenericReport(xx).then(getDependentReportCBCF, app.failCbFn);
                                    }
                                    //End Of Set Dependent Value Set 

                                }


                            }
//                     if (Object.keys(self.model)[i]==Object.keys(values)[i]){                        
//                          self.model[Object.keys(self.model)[i]](values[Object.keys(values)[i]]);
//                     }
                        }

                        self.url = data.url;
                    }
                    //console.log(self.retrieve);
                    services.getGeneric("PeopleExtra/" + self.retrieve.TRS_ID).then(getPaasEitsValuesCBFN, app.failCbFn);
                }
                //-----------get attchment-------------------//
                self.getAttachment=function(){
                    self.koArray([]);
                      var attachmentView = function (data) {
                   // console.log(data)
                    for (var i = 0; i < data.length; i++) {

//                                    var baseStr64 = data[i].attachment;
//                                    drawAttachment2(i, baseStr64);
                        self.koArray.push({
                            "id": data[i].id,
                            "name": data[i].name,
                            "data": data[i].attachment
                        });

                    }

                    // imgElem.setAttribute('src', baseStr64);
                };
                services.getGeneric("attachment/" + self.retrieve.TRS_ID).then(attachmentView, app.failCbFn);
            }
                //-----------get attchment-------------------//
                self.handleAttached = function (info) {
                    self.rejectReason(" ");
                    app.loading(false);
                    self.disableSubmit(false);
                     ArrKeys = [];  
                    self.retrieve = oj.Router.rootInstance.retrieve();    
                    console.log(self.retrieve);
                    self.workFlowApprovalId(self.retrieve.id);
                    //MSG_Body_Lbl
                    self.headerLbl(self.retrieve.MSG_Body_Lbl+' / '+self.retrieve.person_number);
                    self.EitCode = rootViewModel.reviewNotiType();
                    self.reqestedID = self.retrieve.person_id;
                     approvalManagmant = [];
                    
                     initTranslations();
                    getElementEntryByEitCode();
                    if (self.retrieve.TYPE == "FYI") {
                    }
                    self.valueText("Person Number :"+self.retrieve.person_number);
                    self.currentValueText()
                    self.currentStepValue('stp1');
                    //self.prevPram= oj.Router.rootInstance.retrieve();
                    
                    //self.pageMode(self.prevPram.pageMode);
                    self.selectedPage(self.pageMode());
                    
                    self.isLastApprover(rootViewModel.isLastApprover(self.retrieve.TRS_ID, self.EitCode,false));
                    
                    self.approvalType = self.retrieve.receiver_type;
                    self.roleID = self.retrieve.roleId;
                    var getApprovalManagmantCBFN = function (data) {

                        approvalManagmant = data;
                    }
                    if (self.approvalType == "POSITION") {
                        services.getGeneric("approvalManagement/search/" + self.EitCode + "/" + self.approvalType + "/" + self.roleID, {}).then(getApprovalManagmantCBFN, app.failCbFn);
                    } else
                    {
                        services.getGeneric("approvalManagement/search/" + self.EitCode + "/" + self.approvalType, {}).then(getApprovalManagmantCBFN, app.failCbFn);
                    }
                    var getEit = function (data) {
                        tempObject = data;
                        Keys = [];
                        var Iterater = 0;
                        disabeld=[];
                        for (var i = 0; i < tempObject.length; i++) {
                            if (tempObject[i].DEFAULT_value && tempObject[i].DEFAULT_TYPE == 'C' && tempObject[i].DEFAULT_value != "PAAS") {
                                var actionModel = {
                                    reportName: tempObject[i].DEFAULT_value,
                                    whenChangeValue: [],
                                    segmentName: Keys[i]
                                };

                                actionModel.whenChangeValue = searchArrayContainValue(tempObject[i].DEFAULT_value, app.globalEITDefultValueParamaterLookup());
                                if (actionModel.whenChangeValue[0]) {
                                    actionModel.whenChangeValue = actionModel.whenChangeValue[0].split(',');
                                }
                                self.actionModelsArr.push(actionModel);

                                for (var index = 0; index < actionModel.whenChangeValue.length; index++) {

                                    var reportPaylod = {"reportName": tempObject[i].DEFAULT_value};
                                    for (var index = 0; index < actionModel.whenChangeValue.length; index++) {
                                        if (Object.keys(self.model).indexOf(actionModel.whenChangeValue[index]) != -1) {
                                            reportPaylod[actionModel.whenChangeValue[index]] = self.model[actionModel.whenChangeValue[index]]();
                                        } else {
                                            reportPaylod[actionModel.whenChangeValue[index]] = app.personDetails()[actionModel.whenChangeValue[index]];
                                        }
                                    }
                                    var getReportValidationCBCF = function (dataSaaSQuery) {
                                        var resultOfQueryObj = dataSaaSQuery;
                                        self.model[Keys[i]](resultOfQueryObj[ Object.keys(dataSaaSQuery)[0]]);
                                    }
                                    services.getGenericReport(reportPaylod).then(getReportValidationCBCF, app.failCbFn);


                                }




                            }
                            //Build Model 
                            Keys.push(tempObject[i].DESCRIPTION);
                            self.model[Keys[i]] = ko.observable();
                            PaaSKeys.push(tempObject[i].APPLICATION_COLUMN_NAME);
                            self.PaaSmodel[PaaSKeys[i]] = ko.observable(self.model[Keys[i]]());
                            //end OF Build Model 
                            if ((tempObject[i].FLEX_value_SET_NAME != "100 Character No Validation") &&
                                    (tempObject[i].FLEX_value_SET_NAME != "10 Number") &&
                                    (tempObject[i].FLEX_value_SET_NAME != "HRC_STANDARD_DATE") &&
                                    (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_EIT_ATTACHMENTS") &&
                                    (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_PAAS_NUMBER") &&
                                    (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_PAAS_DATE")&&
                                    (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_PAAS_TIME")
                                    ) {
                                if (tempObject[i].value_SET_TYPE == "X" || tempObject[i].value_SET_TYPE == "I") {
                                    //  self.ishide(false);
                                    ArrKeys.push(tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr");
                                    self.Arrs[ArrKeys[Iterater]] = ko.observableArray();
                                    var getReportCBCF = function (data) {
                                        var tempObject2 = data;
                                        var arr = tempObject2;
                                        var arrays = [];
                                        for (var x = 0; x < arr.length; x++) {
                                            arrays.push({"value": arr[x].FLEX_value, "label": arr[x].value_DESCRIPTION})
                                        }
                                        self.Arrs[ArrKeys[Iterater]](arrays);


                                    };
                                    var lang = '';
                                    if (localStorage.getItem("selectedLanguage") == 'ar') {
                                        lang = "AR";
                                    } else {
                                        lang = "US";
                                    }
                                    var xx = {"reportName": "inDependentReport", "valueSet": tempObject[i].FLEX_value_SET_NAME,"langCode":lang};
                                    services.getGenericReportNotAsync(xx).then(getReportCBCF, app.failCbFn);
                                    Iterater = Iterater + 1;
                                } else if (tempObject[i].value_SET_TYPE == "Y") {
                                    //         self.ishide(false);
                                    ArrKeys.push(tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr");
                                    self.Arrs[ArrKeys[Iterater]] = ko.observableArray();

                                    Iterater = Iterater + 1;

                                } else if (tempObject[i].value_SET_TYPE == "F") {
                                    //    self.ishide(false);
                                    ArrKeys.push(tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr");
                                    self.Arrs[ArrKeys[Iterater]] = ko.observableArray();
                                    var getDynamicReport = function (data) {
                                        var val = "value";
                                        var lbl = "label";
                                        var q;
                                        var tempObject = data;
                                        q = "select " + tempObject.ID_COL + " " + val + " ,  " + tempObject.value_COL + "  " + lbl
                                                + " From " + tempObject.APPLICATION_TABLE_NAME + "  " + tempObject.ADDITIONAL_WHERE_CLAUSE;


                                        var str = q;
                                        var flex = ":";
                                        var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));
                                        var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf('#'));
                                        var is_HaveFlex = true;
                                        if (str.indexOf(':') == -1) {
                                            is_HaveFlex = false;
                                        }
                                        while (is_HaveFlex) {
                                            var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));
                                            var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf('#'));
                                            actionModel.whenChangeValue.push(ModelName);
                                            str = str.replace(strWitoutFlex, self.reqestedID);
                                            if (str.indexOf(":") == -1) {
                                                is_HaveFlex = false;
                                            }
                                        }



                                        if (str.includes('undefined')) {
                                        } else {
                                            //Here Must Call Report Fpr This Perpus 
                                            var getArrCBCF = function (data) {
                                                var tempObject2 = data;
                                                var arr = tempObject2;
                                                if (tempObject2.length) {
                                                    self.Arrs[ArrKeys[Iterater]](arr);
                                                } else {
                                                    self.Arrs[ArrKeys[Iterater]]([arr])
                                                }
                                            };

                                            services.getDynamicReport(str).then(getArrCBCF, app.failCbFn);

                                        }



                                    };
                                    services.getQueryForListReport(tempObject[i].FLEX_value_SET_NAME).then(getDynamicReport, app.failCbFn);
                                    Iterater = Iterater + 1;
                               } else if (tempObject[i].FLEX_value_SET_NAME == "XXX_HR_DYNAMIC_COLUMN") {

                                    ArrKeys.push(tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr");
                                    self.Arrs[ArrKeys[ArrKeys.length-1]] = ko.observableArray();
                                    var reportName = searchArray(tempObject[i].DESCRIPTION, app.globalEITReportLookup());
                                    var getReportCBCF = function (data) {
                                        var tempObject2 = data;
                                        var arr = tempObject2;
                                        if (tempObject2.length) {
                                            self.Arrs[ArrKeys[Iterater]](arr);
                                        } else {
                                            self.Arrs[ArrKeys[Iterater]]([arr]);
                                        }
                                          Iterater = Iterater + 1;
                                    }
                                    var lang = '';
                                if (localStorage.getItem("selectedLanguage") == 'ar') {
                                    lang = "AR";
                                } else {
                                    lang = "US";
                                }
                                    var reportPaylad = {"reportName": reportName, "personId": self.reqestedID,"langCode":lang};

                                    services.getGenericReportNotAsync(reportPaylad).then(getReportCBCF, app.failCbFn);
                                    

                                }



                            }

                            //Build Disabeld Model 
                            if (approvalManagmant.length != 0) {
                           for (var j = 0; j < approvalManagmant.length; j++) {
//
                               if (tempObject[i].DESCRIPTION == approvalManagmant[j].segementName) {
                                   if (approvalManagmant[j].enable == "true") {
                                       disabeld.push("isDisabled" +i);
                                   } else if (approvalManagmant[j].enable == "false"){
                                       disabeld.push("isDisabled" + tempObject[i].DESCRIPTION);
                                   }
                                   self.isDisabledx[disabeld[i]] = ko.observable(true);
//                                    console.log(tempObject[i].DESCRIPTION ,approvalManagmant[j].enable);
                                   if (approvalManagmant[j].hide == "true")
                                   {
                                       tempObject[i].DISPLAY_FLAG = 'N';
                                   } else if (approvalManagmant[j].hide == "false")
                                   {
                                       tempObject[i].DISPLAY_FLAG = 'Y';
                                   }
                                   if (approvalManagmant[j].required == "true") {
                                       tempObject[i].REQUIRED_FLAG = 'Y';
                                   } else if (approvalManagmant[j].required == "false") {
                                       tempObject[i].REQUIRED_FLAG = 'N';
                                   }
                               } else {
                                   if (tempObject[i].ENABLED_FLAG == "Y") {
                                       self.isDisabledx[disabeld[i]] = ko.observable(true);
                                   } else if (tempObject[i].ENABLED_FLAG == "N") {
                                       self.isDisabledx[disabeld[i]] = ko.observable(true);
                                   }
                               }
                           }
                       } else {
//                            console.log(tempObject[i])
                           disabeld.push("isDisabled" + tempObject[i].DESCRIPTION);
                           if (tempObject[i].ENABLED_FLAG == "Y") {
                               self.isDisabledx[disabeld[i]] = ko.observable(true);
//                                console.log("y")
                           } else if (tempObject[i].ENABLED_FLAG == "N") {
                               self.isDisabledx[disabeld[i]] = ko.observable(true);
//                                console.log("n")
                           }
                       }
                       //End Of Disabeld Model
                       





                            Typs.push(tempObject[i].FLEX_value_SET_NAME);
                        }
                        buildScreen.buildScreen(tempObject, "xx", self.model, self.isDisabledx, self.dataArray);
                        // self.actionAvilable(true);
                        //  self.setToDisabeld();
                        self.getPaaSEitesValues();
                        self.getAttachment();
                    };
                    if (app.getLocale() == 'ar') {
                        self.lang = "AR";
                    } else {
                        self.lang = "US";
                    }
                    services.getEIT(self.EitCode, self.lang).then(getEit, app.failCbFn);
                };
                self.setModelValue = function () {
                    for (var i = 0; i < Object.keys(self.model).length; i++) {
                        for (var j = 0; Object.keys(self.prevPram).length; j++) {
                            if (Object.keys(self.model)[i] == Object.keys(self.prevPram)[j]) {
                                // This For Set Value In Observable From Prev Page 
                                //self.prevPram[Object.keys(self.prevPram)[j]] (Value We Set in Observable )
//                   // self.model[Object.keys(self.model)[i]] Observable Are Link To Screen 
                                self.model[Object.keys(self.model)[i]](self.prevPram[Object.keys(self.prevPram)[j]]);
                                break;
                            }
                        }
                    }
                    self.model.PersonExtraInfoId = self.prevPram.PersonExtraInfoId;
                    self.model.PersonId = self.prevPram.PersonId;
                };




                self.segmentChangedHandler = function (event, data) {
                    if (event.path[0].attributes.id) {
                        var id = event.path[0].attributes.id.nodeValue;
                        var res = id.substr(6, id.length);
                        var modelArr = Object.keys(self.model);
                        var realIndex = modelArr.indexOf(res);
                        var indexOfModel = modelArr.indexOf(res) + 1;
                        //  value_SET_TYPE
                        if (tempObject[indexOfModel].value_SET_TYPE == "Y") {

                            var getReportCBCF = function (data) {
                                var tempObject2 = data;
                                var arr = tempObject2;
                                var arrays = [];
                                if (arr.length) {
                                    for (var ind = 0; ind < arr.length; ind++) {
                                        arrays.push({"value": arr[ind].FLEX_value, "label": arr[ind].value_DESCRIPTION})
                                    }
                                } else {
                                    arrays.push({"value": arr.FLEX_value, "label": arr.value_DESCRIPTION});
                                }
                                //DESCRIPTION
                                self.Arrs[tempObject[indexOfModel].FLEX_value_SET_NAME + tempObject[indexOfModel].DESCRIPTION + "Arr"](arrays);


                            };
                            var xx = {"reportName": "DependentReport", "valueSet": tempObject[indexOfModel].FLEX_value_SET_NAME, "parent": self.model[modelArr[realIndex]]()};
                            services.getGenericReport(xx).then(getReportCBCF, app.failCbFn);
                        }

                    }


                }
self.dateConverter = ko.observable(oj.Validation.converterFactory(oj.ConverterFactory.CONVERTER_TYPE_DATETIME).createConverter({
                   pattern: 'dd/MM/yyyy'
               }));


                //----------------------------Standerd Screen -------------------------------------------
                var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
                self.columnArray = ko.observableArray();
                self.tracker = ko.observable();
                self.groupValid = ko.observable();
                self.placeholder = ko.observable("Please Select Value");
                self.dataSourceTB2 = ko.observable();
                var deptArray = [];
                self.isDisabled = ko.observable(false);
                self.selctedStruct = ko.observable();
                self.isRequired = ko.observable(true);
                self.currentStepValue = ko.observable('stp1');
                self.addBtnVisible = ko.observable(false);
                self.nextBtnVisible = ko.observable(true);





                self.headerLbl = ko.observable('Noor');
                self.isShown = ko.observable(true);




                self.supervisorHRCYesNoList = rootViewModel.globalHRCYesNo();
                self.oprationMessage = ko.observable();
                self.disableSubmit = ko.observable(false);

                var Url;
                var positionObj;
                self.stepArray = ko.observableArray([{label: "XXXX", id: 'stp1'},
                    {label: "YYYY", id: 'stp2'}]);//workNatureAllowance

                self.backAction = function () {
                    if (oj.Router.rootInstance._navHistory.length > 1) {
                        app.loading(true);
                        oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                    }
                };
                self.formatDate = function (date) {
                    //var date = new Date()
                    var month = '' + (date.getMonth() + 1),
                            day = '' + date.getDate(),
                            year = date.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;

                    return [year, month, day].join('-');
                };
            //-------------this Function for check  length ----------------------  
            self.lengthValidator = ko.computed(function () {
                    return [{
                            type: 'length',
                            options: {
                                min: 0, max: 100,
                                countBy: 'codeUnit',
                                hint: {
                                    inRange: 'value must have at least 0  characters but not more than 100'
                                },
                                messageSummary: {
                                    tooLong: ' Too many characters',
                                    tooShort: ' Too few characters'
                                },
                                messageDetail: {
                                    tooLong: 'Number of characters is too high. Enter at most 100 characters',
                                    tooShort: ' Number of characters is too low. Enter at least 0 characters.'
                                }
                            }
                        }];
                });
                //----------------------------End------------------


                //-------------This Function For Update Train Step To previousStep------------------


                self.setToDisabeld = function () {
                    for (var i = 0; i < Object.keys(self.isDisabledx).length; i++) {
                        self.isDisabledx[Object.keys(self.isDisabledx)[i]](true);
                    }
                };
                self.setToDefultDisabeld = function () {
                    for (var i = 0; i < Object.keys(self.isDisabledx).length; i++) {
                        self.isDisabledx[Object.keys(self.isDisabledx)[i]](false);
                    }
                };
                self.currentStepValueText = function () {
                    if (self.currentStepValue() == 'stp2') {
                        self.isDisabled(true);
                        self.addBtnVisible(true);
                        self.nextBtnVisible(false);
                        self.setToDisabeld();

                    } else {
                        self.isDisabled(false);
                        self.addBtnVisible(false);
                        self.nextBtnVisible(true);
                        self.setToDefultDisabeld();
                    }

                    return self.selectedPage();
                };
                //------------------End Of Section -------------------------------------------------
                //------------------This Section For Dialog Action (Open ,Close)
                self.submitButton = function () {
                    document.querySelector("#yesNoDialog").open();
                };
                self.cancelButton = function () {
                    document.querySelector("#yesNoDialog").close();
                };
                //------------------End Of Section ----------------------------------------------
                //-------------------This Function For Reject -------------------------
                self.rejectButton = function () {
                   document.querySelector("#rejectDialog").open();
                   var getValidGradeCBF=function(data){
                       console.log(data)
                       if(!data==="[]"){
//                       console.log(data[0].rejectConfirmMessageArVal);
                       if(localStorage.getItem("selectedLanguage") == 'ar'){
                         self.rejectDialogBody(data[0].rejectConfirmMessageArVal);
                        }else{
                             self.rejectDialogBody(data[0].rejectConfirmMessageVal);
                        }
                    }else{
                         if(localStorage.getItem("selectedLanguage") == 'ar'){
                             self.rejectDialogBody("هل انت متاكد من الرفض؟");

                        }else{
                       self.rejectDialogBody("Are your sure to reject?");

                        }
                        
                    }
                       
                   };
                    services.getGeneric(commonhelper.getSelfService + self.EitCode, {}).then(getValidGradeCBF, app.failCbFnfailCbFn);
                    
                };
                self.rejectCancelButton = function () {
                    document.querySelector("#rejectDialog").close();
                };
                //-------------------End Of Reject --------------------------------
                //
                self.cancelRewaardRequst = function () {
                    //  rootViewModel.updateNotificaiton(self.notiId());
                    oj.Router.rootInstance.go('notificationScreen');

                    return true;
                };
                //
                //
                //
                self.rejectRewaardRequst = function (data, event) {
                    var headers = {
                        "MSG_TITLE": self.retrieve.MSG_TITLE,
                        "MSG_BODY": self.retrieve.MSG_BODY,
                        "TRS_ID": self.retrieve.TRS_ID,
                        "PERSON_ID": rootViewModel.personDetails().personId,
                        "RESPONSE_CODE": "REJECTED",
                        "ssType": self.retrieve.SELF_TYPE,
                        "rejectReason":self.rejectReason()  ,
                        "workflowId":self.workFlowApprovalId()
                    };

                    var approvalActionCBFN = function (data) {
                        oj.Router.rootInstance.go('notificationScreen');
                    }
                    services.workflowAction("workflowApproval/", headers).then( app.failCbFn,approvalActionCBFN)

                };
                //
                self.approveRequst = function (data, event) {
                   
                   
                    self.disableSubmit(true);
                    var headers = {
                        "MSG_TITLE": self.retrieve.MSG_TITLE,
                        "MSG_BODY": self.retrieve.MSG_BODY,
                        "TRS_ID": self.retrieve.TRS_ID,
                        "PERSON_ID": rootViewModel.personDetails().personId,
                        "RESPONSE_CODE": "APPROVED",
                        "ssType": self.retrieve.SELF_TYPE ,
                        "PERSON_NAME":self.retrieve.personName,
                        "rejectReason":self.rejectReason(),
                        "workflowId":self.workFlowApprovalId()
                    };
                    var approvalActionCBFN = function (data) {
                        //     
                        if (approvalManagmant.length != 0){
                                   oprationServiceRecord(); 
                               }
                        if (self.isLastApprover()) {
                            oprationServiceRecord();
                            var updateStatusCBFN =function (data){
                               
                                
                            };
                            
                             services.getGenericAsync("PeopleExtra/updateStatus?id="+self.retrieve.TRS_ID, {}).then(updateStatusCBFN, app.failCbFn);
                            
                                 
                            
                            
                            
                            
                           if(self.retrieve.SELF_TYPE =='XXX_HR_PERSON_PAY_METHOD'){
                              // self.updateBankFile();
                           }
                           //XXX_HR_PERSON_PAY_METHOD
                           
                           //Update Bank File 
                           //
                           //
                            self.model.url = self.url;
                            var jsonData = {model:self.model,TRS_ID:self.retrieve.TRS_ID,eitCode:self.EitCode}
                            var supmitEFFCBFN = function (data) {
                                var Data = data;
                                   
                                // self.disableSubmit(false);
                                if (oj.Router.rootInstance._navHistory.length > 1) {
                                    oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                                } else {
                                    oj.Router.rootInstance.go('home');
                                }

                            };

                            services.addGeneric("eff/" + "ADD"+"/"+self.retrieve.person_number,ko.toJSON(jsonData)).then(supmitEFFCBFN, app.failCbFn);
                           
//                            services.addGeneric("eff/" + "ADD"+"/"+self.retrieve.person_number, jsonData).then(supmitEFFCBFN, app.failCbFn);

                            if (self.elementEntryArr().length > 0) {
                                for (var i = 0; i < self.elementEntryArr().length; i++) {
                                    self.elementEntryArr()[i].inputValue =  JSON.parse(self.elementEntryArr()[i].inputValue) ;
                                    if (self.elementEntryArr()[i].typeOfAction == "Absence") {
                                        var absenseModel = {
                                            "personNumber": self.reqeustedPersonNumber,
                                            "employer": "HHA",
                                            "absenceType": self.elementEntryArr()[i].absenceType,
                                            "startDate": self.model[self.elementEntryArr()[i].startDate](),
                                            "endDate": self.model[self.elementEntryArr()[i].endDate](),
                                            "absenceStatusCd": self.elementEntryArr()[i].absenceStatus,
                                            "startDateDuration": 1,
                                            "endDateDuration":1
                                        };
                                        var supmitAbsenceCBFN = function (data) {

                                        }

                                        services.addGeneric("absence/", absenseModel).then(supmitAbsenceCBFN, app.failCbFn);
                                    } else {
                                        for(var inputIndex = 0 ; inputIndex<self.elementEntryArr()[i].inputValue.length;inputIndex ++ ){
                                           
                                            if (self.elementEntryArr()[i].inputValue[inputIndex].inputEitValue != "Transaction_ID") {
                                                if (self.elementEntryArr()[i].inputValue[inputIndex].inputEitValue) {
                                                    self.elementEntryArr()[i].inputValue[inputIndex].inputEitValue = self.model[self.elementEntryArr()[i].inputValue[inputIndex].inputEitValue ]();
                                                } else {
                                                    self.elementEntryArr()[i].inputValue[inputIndex].inputEitValue = "";
                                                }
                                            }else{
                                                 self.elementEntryArr()[i].inputValue[inputIndex].inputEitValue = "P"+self.retrieve.TRS_ID;
                                            }
                                           
                                           
                                        }
                                       // console.log(self.model[self.elementEntryArr()[i].sourceSystemId]());
                                        var effivtiveStartDate ;
                                        var reason; 
                                        
                                        
                                        
                                        if(self.elementEntryArr()[i].effivtiveStartDate){
                                          effivtiveStartDate =  self.model[self.elementEntryArr()[i].effivtiveStartDate]() ;
                                           //effivtiveStartDate =  self.elementEntryArr()[i].effivtiveStartDate ;
                                           console.log(effivtiveStartDate);
                                            if (!effivtiveStartDate){
                                               console.log("Here No Effictive ");
                                              effivtiveStartDate= callFailReport( self.elementEntryArr()[i].eitCode,self.elementEntryArr()[i].effivtiveStartDate)
                                           }
                                        }else{
                                            effivtiveStartDate = "%EffectiveStartDate%";
                                        }
                                        if(self.elementEntryArr()[i].reason){
                                          reason =  self.model[self.elementEntryArr()[i].reason]() ; 
                                           //effivtiveStartDate =  self.elementEntryArr()[i].effivtiveStartDate ;
                                        }else{
                                            reason = "";
                                        }
                                        var jsonBody = {
                                            personNumber: self.reqeustedPersonNumber,
                                            elementName: self.elementEntryArr()[i].elementName,
                                            legislativeDataGroupName: self.elementEntryArr()[i].legislativeDatagroupName,
                                            assignmentNumber: "E" + self.reqeustedPersonNumber,
                                            entryType: "E",
                                            creatorType: self.elementEntryArr()[i].creatorType,
                                            sourceSystemId:"PaaS_"+self.retrieve.TRS_ID,
                                            trsId: self.retrieve.TRS_ID,
                                            SSType: self.elementEntryArr()[i].recurringEntry,
                                            eitCode: self.elementEntryArr()[i].eitCode,
                                            inputValues: self.elementEntryArr()[i].inputValue,
                                            effivtiveStartDate:effivtiveStartDate,
                                            reason:reason
                                            
                                        };
                                        var submitElement = function (data1) {
                                        };
                                        console.log("jsonBody",jsonBody);
                                        services.submitElementEntry(JSON.stringify(jsonBody)).then(submitElement, app.failCbFn);
                                    }
                                }
                            }
                        } else {
                            if (oj.Router.rootInstance._navHistory.length > 1) {
                                oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                            } else {
                                oj.Router.rootInstance.go('home');
                            }
                        }
                        document.querySelector("#yesNoDialog").close();
                        // oj.Router.rootInstance.go('notificationScreen');
                    }
                    //services.workflowAction("workflowApproval/", headers).then(approvalActionCBFN, app.failCbFn)
                    services.workflowAction("workflowApproval/", headers).then( app.failCbFn,approvalActionCBFN)
                };
                //------------------ This Section For Call Web Service For Insert Or Update ---------

                //----------This Function To Set Url Call 
                self.setUrl = function (EitCode) {
                    return app.personDetails().employeeURL[EitCode];
                };
                 self.buildModelWithLabel = function () {
                    var jsonDataLbl = self.model;
                    for (var i = 0; i < tempObject.length; i++) {
                        if ((tempObject[i].FLEX_value_SET_NAME != "100 Character No Validation") &&
                                (tempObject[i].FLEX_value_SET_NAME != "10 Number") &&
                                (tempObject[i].FLEX_value_SET_NAME != "HRC_NUMBER_10") &&
                                (tempObject[i].FLEX_value_SET_NAME != "HRC_STANDARD_DATE") &&
                                (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_EIT_ATTACHMENTS") &&
                                (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_PAAS_NUMBER") &&
                                (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_PAAS_DATE") &&
                                (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_PAAS_TIME") &&
                                (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_PAAS_VIEW_REPORT")
                                ) {

                            jsonDataLbl[tempObject[i].DESCRIPTION + "Lbl"] = searchArray(self.model[tempObject[i].DESCRIPTION](), self.Arrs[tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr"]());

                        }
                    }
                    jsonDataLbl = ko.toJSON(self.model);

                    return jsonDataLbl;
                };
                function oprationServiceRecord() {
                    if (Object.keys(self.model).indexOf("approvedBy") != -1) {
                       self.model.approvedBy=ko.observable(rootViewModel.personDetails().personNumber);
                    } else {
                        //No Approved By Segment
                    }
                    
                    var jsonData = ko.toJSON(self.model);
                    // var cx = JSON.stringify(jsonData);
//                    self.model.url = self.setUrl(self.EitCode);
                    self.approvlModel.code = self.EitCode;
                    if(self.isLastApprover()){
                         self.approvlModel.status = "APPROVED";
                    }else{
                          self.approvlModel.status = "PENDING_APPROVED";
                    }
                    self.approvlModel.person_number = self.retrieve.person_number;
                    self.approvlModel.person_id = self.retrieve.person_id;
                    self.approvlModel.created_by = self.retrieve.person_id;
//                    self.approvlModel.managerName = self.personDetails().managerName;
//                    self.approvlModel.managerOfManagerName = self.personDetails().managerOfMnagerName;
                    //self.approvlModel.creation_date = self.personDetails().personId;
                    self.approvlModel.line_manager = globalRowData.line_manager;
                    self.approvlModel.manage_of_manager =globalRowData.manage_of_manager;
//                    self.approvlModel.url = self.setUrl(self.EitCode);
                    self.approvlModel.eit = JSON.stringify(jsonData);
                   // self.approvlModel.personName = ;
                   // self.approvlModel.nationalIdentity = self.personDetails().nationalId;
//                    self.approvlModel.eitLbl = JSON.stringify(self.buildModelWithLabel());
                    self.approvlModel.eit_name =self.retrieve.SELF_TYPE;
                    self.approvlModel.id= self.retrieve.TRS_ID;
        
                    
                    for (var eitModelIndex = 0; eitModelIndex < tempObject.length; eitModelIndex++) {
                        console.log(self.model);
                        console.log(self.model[tempObject[eitModelIndex].DESCRIPTION]());
                        self.approvlModel[tempObject[eitModelIndex].APPLICATION_COLUMN_NAME] = self.model[tempObject[eitModelIndex].DESCRIPTION]();
                    }
                    console.log(self.approvlModel);
                   
                    
                    var jsonData = ko.toJSON(self.approvlModel);
                    console.log(jsonData);
                    var getUpdateExtraInformationCBFN = function (data) {            
                        //
						console.log("Done");
                    };
                
                   var approvalCondetion = app.getApprovalCondetion( self.EitCode,self.model) ; 
                    services.addGeneric("PeopleExtra/createOrUpdateEIT/" + 'EDIT'+'/'+approvalCondetion, jsonData).then(getUpdateExtraInformationCBFN, app.failCbFn);
                };
                self.elementEntryArr = ko.observableArray([]);
                function getElementEntryByEitCode() {
                    var EitCode = self.EitCode;
                    var getValidGradeCBF = function (data) {
                        
                        self.elementEntryArr(data);

                    
                    };

                    services.getGeneric("elementEntrySetup/getElementEntry/" + EitCode).then(getValidGradeCBF, self.failCbFn);
                }
                ;
                //----------------------------Attachment------------------------------//
                self.resumeAttachmentLbl = ko.observable();
                self.viewAttachmentBtnVisible = ko.observable();
                self.attachmentBtnVisible = ko.observable();
                self.removeBtnLbl = ko.observable();
                self.viewLbl = ko.observable();
                self.AttachmentsBtnVisible = ko.observable();
                self.UploadFileLbl = ko.observable();
                self.fileUploadStatusLbl = ko.observable();
                self.dataProviderAttachment = ko.observable();
                self.selectedItemsAttachment = ko.observable();
                var dataFiles = {};
                self.koArray = ko.observableArray([]);
                //create file upload data provider
                self.dataProviderAttachment = new oj.ArrayDataProvider(self.koArray, {keyAttributes: 'id'});
                self.attachmentId = 0;
                self.selectedItemsAttachment = ko.observableArray([]);
                self.attachmentViewerData = ko.observable();
                self.acceptArr = ko.pureComputed(function () {
                    var accept = self.acceptStr();
                    return accept ? accept.split(",") : [];
                }, self);
                self.attachmentSelectListener = function (event) {
                    var files = event.detail.files;
                    if (files.length > 0) {

                        //add the new files at the beginning of the list

                        for (var i = 0; i < files.length; i++) {
                            function getBase64(file) {
                                return new Promise((resolve, reject) => {
                                    const reader = new FileReader();
                                    reader.readAsDataURL(file);
                                    reader.onload = () => resolve(reader.result);
                                    reader.onerror = error => reject(error);
                                });
                            }
                            dataFiles.name = files[i].name;
                            dataFiles.id = i + 1;
                            getBase64(files[i]).then(function (data) {
                                dataFiles.data = (data);
                                self.attachmentId = self.attachmentId + 1;
                                dataFiles.id = (self.attachmentId);
//                        self.koArray.push(dataFiles);

                                self.koArray.push({
                                    "id": dataFiles.id,
                                    "name": dataFiles.name,
                                    "SS_id": self.eit_id(),
                                    "data": dataFiles.data


                                });


                               
//                        self.approvalSetupModel.resumeAttachment(dataFiles.data);
                            }
                            );
                        }
                    }

                }
                self.removeSelectedAttachment = function (event) {
                    $.each(self.selectedItemsAttachment(), function (index, value)
                    {
                        self.koArray.remove(function (item)
                        {
                            return (item.id == value);
                        });
                    });
                }

                self.openAttachmentViewer = function ()
                {
    
                    $.each(self.selectedItemsAttachment(), function (index, value)
                    {
                        var data = self.koArray().find(e=>e.id == value);
                        if(data && data.name){
                            var ext = data.name.substring(data.name.lastIndexOf('.')+1, data.name.length) || data.name;
                            if( ext == 'pdf'){
                                app.pdfViewer(data.data,"pdf");
                            }else{
                                app.pdfViewer(data.data,"img");
                            }
                        }
                    });
                };
                //----------------------------Attachment------------------------------//
                
                
                
                
                self.commitRecord = function (data, event) {
                    if (!self.disableSubmit()) {
                        self.disableSubmit(true);
//                        oprationServiceRecord();
                        return true;
                    }
                };
                //-------------------End Of Section ------------------------------------------------
                //-------------------Leave Page -----------------------------
                self.handleDetached = function (info) {
                    oj.Router.rootInstance.store(self.EitCode);
                    self.isDisabledx = {};
                    if (self.retrieve.SELF_TYPE != 'XXX_HR_PERSON_PAY_METHOD') {
                       self.model = {};
                    }
                    //self.model = {};
                };
                //Update Bank File Function //
                self.updateBankFile=function(){
                    var bankFileJsonBody = {
                        "bankName": self.model.bank(),
                        "bankBranchName": self.model.bankBranch(),
                        "accountNumber": self.model.newAccountNumber(),
                        "IBAN": self.model.iban(),
                        "trsId": self.retrieve.TRS_ID,
                        "personNumber": self.reqeustedPersonNumber,
                        "newBankName": self.model.bank(),
                        "newBankBranchName": self.model.bankBranch(),
                        "newAccountNumber": self.model.newAccountNumber()
                    };
                    var submitBankFile= function(data){
                        var createPersonalPaymentMethodJsonBody = {"legislativeDataGroupName": "SA Legislative Data Group",
                            "assignmentNumber": "E"+self.reqeustedPersonNumber,
                            "personalPaymentMethodCode": "Bank Transfer",
                            "effectiveStartDate": self.model.personPayEffectiveStartDate().replace('-','/').replace('-','/'),
                            "paymentAmountType": "P",
                            "processingOrder": "1",
                            "organizationPaymentMethodCode": "Bank Transfer",
                            "percentage": "100",
                            "bankName": self.model.bank(),
                            "bankBranchName": self.model.bankBranch(),
                            "bankAccountNumber": self.model.newAccountNumber(),
                            "personNumber": self.reqeustedPersonNumber
                            ,"trsId": self.retrieve.TRS_ID
                        }
                        var createPersonalPaymentMethodCBFN = function(data){
                            self.model = {};
                        }
                      services.submitHDLFile("createPersonalPaymentMethod",JSON.stringify(createPersonalPaymentMethodJsonBody)).then(createPersonalPaymentMethodCBFN, app.failCbFn);  
                    }
                  services.submitHDLFile("createBankFile",JSON.stringify(bankFileJsonBody)).then(submitBankFile, app.failCbFn);
                };
                //End Of Update Bank File ------------------
                //---------------------Translate Section ----------------------------------------
                self.ok = ko.observable();
                self.startDateLbl = ko.observable();
                self.endDateLbl = ko.observable();
                self.addLbl = ko.observable();
                self.editLbl = ko.observable();
                self.gradeLbl = ko.observable();
                self.validGradeLbl = ko.observable();
                self.structureNameLbl = ko.observable();
                self.backLbl = ko.observable();
                self.nextLbl = ko.observable();
                self.submitLbl = ko.observable();
                self.placeholder = ko.observable("Please Select Value");
                self.percentageLbl = ko.observable();
                self.qualificationNameLbl = ko.observable();
                self.experienceYearsLbl = ko.observable();

                self.cancel = ko.observable();
                self.create = ko.observable();
                self.review = ko.observable();
                self.workNatureAllowanceScreenAddLbl = ko.observable();
                self.workNatureAllowanceScreenEditLbl = ko.observable();
                self.addMassageLbl = ko.observable();
                self.editMassageLbl = ko.observable();
                self.confirmMessage = ko.observable();
                self.selectedPage = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.approveMessage = ko.observable();
                self.approve = ko.observable();
                self.reject = ko.observable();
                self.back = ko.observable();
                self.rejectReason = ko.observable();
                self.rejectMessage = ko.observable();
                var getTranslation = oj.Translations.getTranslatedString;
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        self.stepArray([{label: self.create(), id: 'stp1'},
                            {label: self.review(), id: 'stp2'}]);
                        initTranslations();
                    }
                });
                function initTranslations() {
                    self.create(getTranslation("labels.create"));
                    self.review(getTranslation("others.review"));
                    self.structureNameLbl(getTranslation("additionalDetails.structureName"));
                    self.backLbl(getTranslation("others.back"));
                    self.nextLbl(getTranslation("others.next"));
                    self.submitLbl(getTranslation("others.submit"));
                    self.ok(getTranslation("others.ok"));
                    self.addLbl(getTranslation("others.add"));
                    self.editLbl(getTranslation("others.edit"));
                    self.validGradeLbl(getTranslation("others.validGrade"));
                    self.gradeLbl(getTranslation("job.grade"));
                    self.percentageLbl(getTranslation("position.percentage"));
                    self.cancel(getTranslation("others.cancel"));
                    self.yes(getTranslation("others.yes"));
                    self.no(getTranslation("others.no"));
                    self.approve(getTranslation("others.approve"));
                    self.reject(getTranslation("others.reject"));
                    self.back(getTranslation("others.back"));
                    self.approveMessage(getTranslation("others.approvalMSG") );
                    self.rejectMessage(getTranslation("others.rejectMSG"));
                    self.viewLbl(getTranslation("common.viewLbl"));
                    self.UploadFileLbl(getTranslation("common.UploadFileLbl"));
                    self.removeBtnLbl(getTranslation("common.removeBtnLbl"));
                    self.fileUploadStatusLbl(getTranslation("common.fileUploadStatusLbl"));
                    self.stepArray([{label: self.create(), id: 'stp1'},
                        {label: self.review(), id: 'stp2'}]);
                }
                
                function callFailReport(eitCode, segmentName){
                    //this for call report only 
                    console.log(eitCode);
                    console.log(segmentName);
                    var res;
                 var paylod =    getSegmentDetails(eitCode, segmentName);
                 console.log(paylod);
                    var getReportValidationCBCF = function ( dataSaaSQuery) {
                        var resultOfQueryObj = dataSaaSQuery;
                        console.log(resultOfQueryObj);
                       res=  (resultOfQueryObj[ Object.keys(dataSaaSQuery)[0]]);
                         
                    };
                 services.getGenericReportNotAsync(paylod).then(getReportValidationCBCF, app.failCbFn);
                 return res;
                }
                function getSegmentDetails(eitCode, segmentName){
                    //this function for get segment Details 
                   var allSegment=  app.getEITSegments(eitCode);
                   var paylod={};
                   var reportName; 
                   for (var i = 0; i < allSegment.length; i++){
                       if (allSegment[i].DESCRIPTION==segmentName){
                        reportName= tempObject[i].IN_FIELD_HELP_TEXT ;
                        var pram=searchArrayContainValue(tempObject[i].IN_FIELD_HELP_TEXT, app.globalEITDefultValueParamaterLookup());                       
                       }      
                   }
                  paylod= {"reportName": reportName};
                    for (var zz = 0; zz < pram.length; zz++) {
                        if (Object.keys(self.model).indexOf(pram[zz]) != -1) {
                            if (self.model[pram[zz]]()) {
                                paylod[pram[zz]] = self.model[pram[zz]]();
                            }

                        } else {
                            if (app.personDetails()[pram[zz]]) {
                                if(pram[zz]=='personId'){
                                    paylod[pram[zz]] =  self.approvlModel.person_id;
                                }else{
                                    console.log(pram[zz]);
                                }
                                
                            }

                        }
                    }
                   
                   return  paylod ;
                    
                }



            }

            return new dynamicOperationScreenViewModel();
        }
);
