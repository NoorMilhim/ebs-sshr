/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * homeministryJob module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojknockout', 'ojs/ojarraydataprovider',
    'ojs/ojselectcombobox','ojs/ojtable',
    'ojs/ojdialog'], function (oj, ko, $, app, services, commonhelper) {

    function positionSummaryAdditionalDetails() {
        var self = this;
        self.columnArray = ko.observableArray();
        self.placeholder = ko.observable();
        self.dataSourceTB2 = ko.observable();
        var deptArray = [];
        self.selctedStruct = ko.observable();
        self.isRequired = ko.observable(true);
        self.oprationdisabled = ko.observable(true);
        self.selectedRowKey = ko.observable();
        self.selectedIndex = ko.observable();
        self.nextPage = ko.observable();
        self.backActionLbl = ko.observable();
        self.deptObservableArray = ko.observableArray(deptArray);
        var Url;
        var positionObj;
        self.checkResultSearch = ko.observable();
        self.dataprovider = new oj.ArrayTableDataSource(self.deptObservableArray, {idAttribute: 'id'});
        function clearContent() {

        }
//        self.EitName = ko.observable([
//            {value: "XXX_ABS_CREATE_DECREE", label: "XXX_ABS_CREATE_DECREE"},
//            {value: "XXX_EMP_MINISTRY_JOB", label: "XXX_EMP_MINISTRY_JOB"},
//            {value: "XXX_ABS_UPDATE_DECREE", label: "XXX_ABS_UPDATE_DECREE"},
//            {value: "XXX_ABS_DELETE_DECREE", label: "XXX_ABS_DELETE_DECREE"}
//        ]);

        // Call EIT Code
        self.EitName = ko.observableArray([]);


        self.tableSelectionListener = function (event) {
            var data = event.detail;
            var currentRow = data.currentRow;
            self.oprationdisabled(false);
//            self.selectedRowKey(currentRow['rowKey']);
           if(currentRow!==null){
            self.selectedIndex(currentRow['rowIndex']);}

        };

        self.addAdditionalDetails = function () {
            app.loading(true);
            positionObj = {}
            positionObj.type = "ADD";
            //positionObj.type="xx";

            oj.Router.rootInstance.store(positionObj);
            oj.Router.rootInstance.go('approvalOperationScreen');
        };
        self.updateAdditionalDetails = function () {
            if (self.deptObservableArray()[self.selectedIndex()] != null) {
                app.loading(true);
                self.deptObservableArray()[self.selectedIndex()].type = "EDIT";
                oj.Router.rootInstance.store(self.deptObservableArray()[self.selectedIndex()]);
                oj.Router.rootInstance.go("approvalOperationScreen");
            } else
            {
                $.notify(self.ValidationMessage(), "error");
            }
        };
        self.deleteAdditionalDetails = function () {
            if (self.deptObservableArray()[self.selectedIndex()] != null) {
                document.querySelector("#yesNoDialog").open();
            } else
            {
                $.notify(self.ValidationMessage(), "error");
            }

        };
        self.validationModel = {
            id: ko.observable(),
            eit_code: ko.observable(),
            approval_order: ko.observable()


        };
        self.getall=function(eit_code){
            
             var positionCode = self.selctedStruct();
            Url = commonhelper.approvalSetupUrl;
//            if (self.selctedStruct() == "Work_Nature_Allowance") {
//                
//                //self.nextPage('workNatureAllowanceOprationScreen');
//            }
            var getValidGradeCBF = function (data) {
//                var Data = data;
                self.deptObservableArray([]);
                if (data.length !== 0) {
//                    self.deptObservableArray(Data);
                     self.deptObservableArray([]);
                       for (var i = 0; i < data.length; i++) {
                            var obj = {};
                               obj.id= data[i].id;
                              for (var j=0;j<app.globalEitNameReport().length;j++){                                
                              if(app.globalEitNameReport()[j].value==data[i].eitCode){  
                                  obj.eitCodeLbl  = app.globalEitNameReport()[j].label;
                                }
                             }     
                               obj.eitCode= data[i].eitCode;
                               obj.approvalType= data[i].approvalType;
                               obj.approvalOrder= data[i].approvalOrder;
                               obj.notificationType= data[i].notificationType;
                               if(data[i].approvalType=="ROLES"){
                                   for (var x=0;x<app.rolesOption().length;x++){
                                       if(app.rolesOption()[x].value==data[i].roleName){
                                         obj.roleNameLbl=app.rolesOption()[x].label;  
                                       }
                                   }
                                   
                               }else if(data[i].approvalType=="POSITION"){
                                  for (var k=0;k<app.roleOptionType().length;k++){
                                      if(app.roleOptionType()[k].value==data[i].roleName){
                                         obj.roleNameLbl=app.roleOptionType()[k].label;  
                                       } 
                                  } 
                               }
                               obj.roleName= data[i].roleName;
                               obj.specialCase= data[i].specialCase;
                               obj.createdBy= data[i].createdBy;
                               obj.creationDate= data[i].creationDate;
                               obj.updatedDate= data[i].updatedDate;
                               obj.managerLeval= data[i].managerLeval;
                               obj.approval= data[i].approval;
                               obj.position= data[i].position;
                               obj.approvalCode= data[i].approvalCode;
                               self.deptObservableArray.push(obj);
                       };
                } else {
                    $.notify(self.checkResultSearch(), "error");
                }
            };
            var failCbFn = function () {
                $.notify(self.checkResultSearch(), "error");
            };
            services.getGeneric(Url + positionCode, {}).then(getValidGradeCBF, failCbFn);
   
            
        }
        
        

        self.globalDelete = function () {
          
           
           var element = document.getElementById('table');
            var currentRow = element.currentRow;
            self.deptObservableArray()[currentRow['rowIndex']];     
            var id = self.deptObservableArray()[currentRow['rowIndex']].id;
            var eit_code = self.deptObservableArray()[currentRow['rowIndex']].eitCode;
            var approval_order = self.deptObservableArray()[currentRow['rowIndex']].approvalOrder;
            var jsonData = ko.toJSON(self.validationModel);
            var approval_code=self.deptObservableArray()[currentRow['rowIndex']].approvalCode;
            //services.addGeneric(commonhelper.approvalSetupUrl + "delapproval/", jsonData);
            var getdeleteCBF=function (){
              // self.structureChangedHandler2(eit_code); 
            };
             
            services.searcheGeneric(commonhelper.approvalSetupUrl + "delapproval/" + self.deptObservableArray()[currentRow['rowIndex']].id + "/" + eit_code + "/" +approval_code +"/"+approval_order).then(getdeleteCBF, app.failCbFn);
            // location.reload();
           
        };
        self.backAction = function () {
            app.loading(true);
            oj.Router.rootInstance.go('searchPosition');
        };
        self.handleAttached = function () {
            app.loading(false);
            // positionObj = oj.Router.rootInstance.retrieve();
            var arr = [{value: "XXX_HR_PROBATION_PERFORMANCE",
                    label: self.EmployeeProbationaryPeriodEvaluationLBL()}];
            var totalArr = [];
            totalArr = arr.concat(app.globalEitNameReport());
            self.EitName(totalArr);


        };
        //------------------Option Change Section ----------------------------
        /*
         * Tyis Function Load Date From Web Servise Dependant On Selected Structure Name 
         * And Set The Next Page To Direct So When Crate The Your Pape Set The True Value 
         * in Var self.nextPage 
         * 
         */
        self.structureChangedHandler = function (event, data) {
           self.getall(self.selctedStruct());
        };
        //------------------------End Of Section ----------------------------
            
               
               
       
        //---------------------------Function For Build Table-----------------------------
        self.buildTable = function () {
            //if (self.selctedStruct() == "Work_Nature_Allowance") {
            self.columnArray([

                {
                    "headerText": self.eitNameLbl(), "field": "eitCodeLbl"
                },
                {
                    "headerText": self.approvalTypeLbl(), "field": "approvalType"
                },
                {
                    "headerText": self.approvalOrderLbl(), "field": "approvalOrder"
                },
                {
                    "headerText": self.notificationTypeLbl(), "field": "notificationType"
                },
                {
                    "headerText": self.roleNameLbl(), "field": "roleNameLbl"
                },
                {
                    "headerText": self.specialCaseLbl(), "field": "specialCase"
                }

            ]);
            //}
        };
        //---------------------------End Of Function ---------------------------------
        //--------------------------History Section --------------------------------- 
        self.historyDetails = ko.observable();
        self.closeDialog = function () {
            $("#modalDialog1").ojDialog("close");
        };

        //--------------------------End Of Section ----------------------------------
        self.commitRecord = function (data, event) {
            self.globalDelete();
            self.getall(self.selctedStruct());
            document.querySelector("#yesNoDialog").close();
        };

        self.cancelButton = function () {
            document.querySelector("#yesNoDialog").close();
        };

        self.cancelButtonValid = function () {
            document.querySelector("#ValidationDialog").close();
        };
        self.backAction=function(){
                   oj.Router.rootInstance.go('setup');  
                };
        //---------------------Translate Section ----------------------------------------
        self.ok = ko.observable();
        self.startDateLbl = ko.observable();
        self.endDateLbl = ko.observable();
        self.addLbl = ko.observable();
        self.editLbl = ko.observable();
        self.deleteLbl = ko.observable();
        self.gradeLbl = ko.observable();
        self.validGradeLbl = ko.observable();
        self.eitNameLbl = ko.observable();
        self.backLbl = ko.observable();
        self.placeholder = ko.observable();
        self.percentageLbl = ko.observable();
        self.qualificationNameLbl = ko.observable();
        self.experienceYearsLbl = ko.observable();
        self.amountLbl = ko.observable();
        self.historyLbl = ko.observable();
        self.addtionalDetails = ko.observable();
        self.approvalTypeLbl = ko.observable();
        self.roleNameLbl = ko.observable();
        self.specialCaseLbl = ko.observable();
        self.notificationTypeLbl = ko.observable();
        self.approvalOrderLbl = ko.observable();
        self.approvalSetupLbl = ko.observable();
        self.yes = ko.observable();
        self.no = ko.observable();
        self.confirmMessage = ko.observable();
        self.oprationMessage = ko.observable();
        self.ValidationMessage = ko.observable();
        self.approvalCode = ko.observable();
        self.EmployeeProbationaryPeriodEvaluationLBL=ko.observable();
        var getTranslation = oj.Translations.getTranslatedString;
        self.refreshView = ko.computed(function () {
            if (app.refreshViewForLanguage()) {
                initTranslations();
            }
        });

        function initTranslations() {
            self.eitNameLbl(getTranslation("approvalScreen.eitCode"));
            self.backLbl(getTranslation("others.back"));
            self.ok(getTranslation("others.ok"));
            self.addLbl(getTranslation("others.add"));
            self.editLbl(getTranslation("others.edit"));
            self.deleteLbl(getTranslation("others.delete"));
            self.validGradeLbl(getTranslation("others.validGrade"));
            self.startDateLbl(getTranslation("job.startDate"));
            self.endDateLbl(getTranslation("job.endDate"));
            self.gradeLbl(getTranslation("job.grade"));
            self.percentageLbl(getTranslation("position.percentage"));
            self.qualificationNameLbl(getTranslation("additionalDetails.qualificationName"));
            self.amountLbl(getTranslation("additionalDetails.amount"));
            self.experienceYearsLbl(getTranslation("additionalDetails.experienceYears"));
            self.historyLbl(getTranslation("others.history"));
            self.addtionalDetails(getTranslation("position.positionSummaryAdditionalDetails"));
            self.approvalTypeLbl(getTranslation("approvalScreen.approvalType"));
            self.roleNameLbl(getTranslation("approvalScreen.roleName"));
            self.specialCaseLbl(getTranslation("approvalScreen.specialCase"));
            self.notificationTypeLbl(getTranslation("approvalScreen.notificationType"));
            self.approvalSetupLbl(getTranslation("position.approvalSetup"));
            self.confirmMessage(getTranslation("approvalScreen.confirmMessage"));
            self.oprationMessage(getTranslation("approvalScreen.oprationMessage"));
            self.yes(getTranslation("others.yes"));
            self.no(getTranslation("others.no"));
            self.placeholder(getTranslation("labels.placeHolder"));
            self.ValidationMessage(getTranslation("labels.ValidationMessage"));
            self.approvalOrderLbl(getTranslation("approvalScreen.aprovalOrder"));
            self.checkResultSearch(getTranslation("common.checkResultSearch"));
            self.backActionLbl(getTranslation("approvalScreen.backAction"));
            self.approvalCode(getTranslation("approvalScreen.approvalCode"));
            self.EmployeeProbationaryPeriodEvaluationLBL(getTranslation("approvalScreen.EmployeeProbationaryPeriodEvaluation"));
            self.columnArray([
                {
                    "headerText": self.eitNameLbl(), "field": "eitCodeLbl"
                },
                {
                    "headerText": self.approvalTypeLbl(), "field": "approvalType"
                },
                {
                    "headerText": self.approvalCode(), "field": "approvalCode"
                },  
                {
                    "headerText": self.approvalOrderLbl(), "field": "approvalOrder"
                },
                {
                    "headerText": self.notificationTypeLbl(), "field": "notificationType"
                },
                {
                    "headerText": self.roleNameLbl(), "field": "roleNameLbl"
                },
                {
                    "headerText": self.specialCaseLbl(), "field": "specialCase"
                }
            ]);
        }
        initTranslations();
    }
    return positionSummaryAdditionalDetails;
});
