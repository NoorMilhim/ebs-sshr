/**
 * 
 * @param {type} oj
 * @param {type} ko
 * @param {type} $
 * @param {type} app
 * @param {type} commonUtil
 * @param {type} services
 * @param {type} translatehelper
 * @returns {JobL#13.JobContentViewModel}
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojknockout', 'ojs/ojtrain', 'ojs/ojbutton',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker',
    'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup', 'ojs/ojcheckboxset', 'ojs/ojradioset',
    'ojs/ojlabel', 'ojs/ojgauge', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojmessage', 'jquery-calendar',
    'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin',
    'jquery-calendar-picker', 'ojs/ojvalidationgroup', 'ojs/ojmessages', 'ojs/ojmessage', 'ojs/ojarraydataprovider'],
        function (oj, ko, $, app, commonUtil, services) {
            /**
             * The view model for the main content view template
             */
            function JobContentViewModel() {
                var self = this;
                var operationType;
                self.disableSubmit = ko.observable(false);
                self.disableSubmitEdit = ko.observable(false);
                self.endDate = ko.observable();
                self.reportName = ko.observable();
                self.Parameter1 = ko.observable();
                self.Parameter2 = ko.observable();
                self.Parameter3 = ko.observable();
                self.Parameter4 = ko.observable();
                self.Parameter5 = ko.observable();
                self.createReport = ko.observable();
                self.reportNameVal = ko.observable("");
                self.Parameter1Val = ko.observable("");
                self.Parameter2Val = ko.observable("");
                self.Parameter3Val = ko.observable("");
                self.Parameter4Val = ko.observable("");
                self.Parameter5Val = ko.observable("");
                self.reportTypeVal = ko.observable("");
                self.reportTitleLabl = ko.observable();
                self.reportTypelbl = ko.observable();
                 self.reportTypeArr=ko.observableArray();

                self.addJob = ko.observable();
                self.updateJob = ko.observable();
                self.viewJob = ko.observable();
                self.jobTitel = ko.observable();
                self.trainView = ko.observable();
                self.confirmMessage = ko.observable();
                self.addMessage = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.pervious = ko.observable();
                self.next = ko.observable();
                self.cancel = ko.observable();
                self.submit = ko.observable();
                self.create = ko.observable();
                self.review = ko.observable();
                self.ok = ko.observable();
                self.oprationMessage = ko.observable();
                self.addMassageLbl = ko.observable();
                self.editMassageLbl = ko.observable();
                self.placeholder = ko.observable();
                self.date1 = ko.observable(oj.IntlConverterUtils.dateToLocalIso(new Date()));

                self.serviceType = ko.observable();
                self.trainVisible = ko.observable(true);
                self.ministryJobModle = ko.observable();
                self.groupValid = ko.observable();
                self.jnVisibility = ko.observable();
                self.jnVisibility = ko.observable();
                self.trainVisibility = ko.observable();
                self.tracker = ko.observable();
                self.nextBtnVisible = ko.observable(true);
                self.addBtnVisible = ko.observable(false);
                self.columnArray = ko.observableArray();
                self.isRequired = ko.observable(true);
                self.stepArray = ko.observableArray([]);
                self.messages = ko.observable();
                self.errormessage = ko.observable();
                self.errormessagecheckInDatabase = ko.observable();
                self.validationMessage = ko.observable();
                self.oprationMessageUpdate = ko.observable();
                self.reportNamelbl = ko.observable();
         
              
                

                self.currentStepValue = ko.observable('stp1');
                self.hidecancel = ko.observable(true);
                self.hidepervious = ko.observable(false);
                self.hidenext = ko.observable(true);
                self.hidesubmit = ko.observable(false);
                self.hideUpdatesubmit = ko.observable(false);
                self.messages = ko.observable();
               // new define 
                self.roleNameVal=ko.observable();
                self.roleNameArr=ko.observableArray();
                self.fristKeyTypeVal=ko.observable();
                self.fristKeyTypeArr=ko.observableArray();
                self.SecondKeyTypeArr=ko.observableArray();
                self.personalInfromationArr=ko.observableArray();
                self.firstKeyValue=ko.observable();
                self.operation=ko.observable();
                self.personalInfromationVal=ko.observable();
                self.SecondfristKeyTypeVal=ko.observable();
                self.secondKeyValue=ko.observable();
                self.secondPersonalInfromationVal=ko.observable();
                self.errorMassageEn=ko.observable();
                self.errorMassageAR=ko.observable();
                self.isDisabled = ko.observable(false);
                self.serviceType=ko.observable();
                self.operationArr=ko.observableArray();

              //new method 
                var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
              self.getRoleName=function(){
                  var getRoleNameCbFn=function(data){
                     
                      
                         $.each(data, function (index) {

                                self.roleNameArr.push({
                                    id: data[index].id,
                                    value: data[index].roleName+","+ data[index].id,
                                    label: data[index].roleName
                                });
                            });
                      
                      
                      
                  };
                  
                  services.getReportName(commonUtil.getRoleName).then(getRoleNameCbFn, app.failCbFn);  
              };
             self.getPersonDetailsCode = function () {
                 var tempObject = Object.keys(rootViewModel.personDetails());
                   for (var i = 0; i < tempObject.length - 1; i++) {
                             self.personalInfromationArr.push({
                                    value: tempObject[i],
                                    label: tempObject[i]
                                });

                   }
             };


              // 
               self.validationModle = ko.observable();
               self.validationModle = {
                    roleNameVal: ko.observable(),
                    fristKeyTypeVal: ko.observable(),
                    firstKeyValue: ko.observable(),
                    personalInfromationVal: ko.observable(),
                    operation: ko.observable(),
                    SecondfristKeyTypeVal: ko.observable(),
                    secondKeyValue: ko.observable(),
                    secondPersonalInfromationVal: ko.observable(),
                    errorMassageEn: ko.observable(),
                    errorMassageAR: ko.observable(),
                    
                };


               self.currentStepValueText = function () {
                    if (self.currentStepValue() == 'stp2') {
                         $(".disable-element").prop('disabled', true);
                   } else {
                        $(".disable-element").prop('disabled', false);
                    }

                };
              
                self.previousStep = function () {
                    self.hidecancel(true);
                    self.hidepervious(false);
                    self.hidesubmit(false);
                    self.hideUpdatesubmit(false);
                    self.hidenext(true);
                    $(".disable-element").prop('disabled', false);
                    self.currentStepValue('stp1');

                };

                  self.nextStep = function () {
                    var tracker = document.getElementById("tracker");
                      
                        if (tracker.valid !== "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        return;
                    }
                    else {
                        
                        self.hidesubmit(true);
                        self.isDisabled(true);
                        self.hidepervious(true)
                        self.hidenext(false);
                        self.disableInput();
                    } 
         
                  };


                self.disableInput = function () {
                    self.hidecancel(true);
                    self.hidepervious(true);
                    self.hidenext(false);

                    $(".disable-element").prop('disabled', true);
                    self.currentStepValue('stp2');
                };

                self.cancelAction = function () {

                    oj.Router.rootInstance.go('roleSetupValidationSummary');

                };


                self.submitButton = function () {
                    document.querySelector("#yesNoDialog").open();

                };
                self.commitRecordupdate = function () {
                    self.callReport();
                    self.disableSubmitEdit(true);

                };
                self.cancelButtonupdate = function () {
                    document.querySelector("#yesNoDialogUpdate").close();

                };
                self.submitUpdateButton = function () {
                    document.querySelector("#yesNoDialogUpdate").open();
//                    operationType = 'EDIT';
//                    self.callReport(operationType);
                };

                self.getReportName = function () {

                    var getReportCbFn = function (data) {
                        if (data === "") {

                            self.disableInput();
                            self.hidesubmit(true);

                        } else {

//                            self.messages([{
//                                    severity: 'error',
//                                    summary: self.errormessagecheckInDatabase(),
//                                    autoTimeout: 0
//                                }
//                            ]);
//                            $.notify(self.errormessagecheckInDatabase(), "error");
                            self.messages([{
                                    severity: 'error',
                                    summary: self.errormessagecheckInDatabase(),
//                                    autoTimeout: 5000

                                }
                            ]);

                        }

                    };
                    var failCbFn = function () {
                    };
                    var serviceName = commonUtil.getReportName + self.reportNameVal();
                    services.getReportName(serviceName).then(getReportCbFn, failCbFn);
                };


                self.getReportNameToCheckUpdate = function () {

                    var getReportCbFn = function (data) {
                        if (data === "") {

                            self.disableInput();
                            self.hideUpdatesubmit(true);

                        } else {

//                            self.messages([{
//                                    severity: 'error',
//                                    summary: self.errormessagecheckInDatabase(),
//                                    autoTimeout: 0
//                                }
//                            ]);
                            $.notify(self.errormessagecheckInDatabase(), "error");
                        }

                    };
                    var failCbFn = function () {
                    };
                    var serviceName = commonUtil.getReportName + oj.Router.rootInstance.retrieve().data.report_Name[0] + "/" + self.reportNameVal();
                    services.getReportName(serviceName).then(getReportCbFn, failCbFn);
                };




                self.restInput = function () {
                    self.reportNameVal("");
                    self.Parameter1Val("");
                    self.Parameter2Val("");
                    self.Parameter3Val("");
                    self.Parameter4Val("");
                    self.Parameter5Val("");
                };



                self.callReport = function () {
                      var jsonData = ko.toJSON(self.validationModle);
                     
                    var id;
                    var operationType;
                    if (self.serviceType() === 'ADD')
                    {
                        operationType='ADD';
                        id = 0;
                    } else if (self.serviceType() === 'update')
                    {
                           operationType='EDIT'; 
                        id = self.retrieve.id;
                    }
                    
                    var str = self.validationModle.roleNameVal();
                    var commaIndex = str.indexOf(",");
                    var lastIndex = str.length;
                    var roleName = str.substring(0, commaIndex);
                    var roleId = str.substring(commaIndex + 1, lastIndex);
                    
                    
                    var payload = {
                    "roleid":roleId,
                    "rolename": roleName,
                    "staticValue": self.validationModle.firstKeyValue(),
                    "personInfromation": self.validationModle.personalInfromationVal(),
                    "operation": self.validationModle.operation(),
                     "id":id
                    };
                
                    var createReportCbFn = function (data) {
                        oj.Router.rootInstance.go('roleSetupValidationSummary');
                    };
                    

                    services.addReport(commonUtil.addOrUpdateRoleValidation + operationType, JSON.stringify(payload)).then(createReportCbFn, app.failCbFn);
                };


                self.commitRecord = function () {
                    
                    self.callReport();
                    self.disableSubmit(true);
                };


                self.cancelButton = function () {

                    document.querySelector("#yesNoDialog").close();
                };

                self.commitDraft = function (data, event) {
                    return true;
                };
                self.stopSelectListener = function () {
                    var tracker = document.getElementById("tracker");

                    if (tracker.valid !== "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        event.preventDefault();
                        return;
                    }

                };

                self.handleAttached = function (info) {
                    self.getRoleName();
                    self.getPersonDetailsCode();
                    app.loading(false);
                      
                     self.retrieve = oj.Router.rootInstance.retrieve();
                     
                    if (self.retrieve === 'ADD') {
                        self.serviceType('ADD');
                        self.hidecancel(true);
                        self.hidepervious(false);
                        self.hidenext(true);
                        self.hidesubmit(false);
                        $(".disable-element").prop('disabled', false);
                        self.currentStepValue('stp1');


                    } else if (self.retrieve.type === 'view'){
                        
                        
                   self.validationModle = {
                    roleNameVal: ko.observable(self.retrieve.roleName),
                    firstKeyValue: ko.observable(self.retrieve.secondKeyValue),
                    personalInfromationVal: ko.observable(self.retrieve.fristPersonalInfromation),
                    operation: ko.observable(self.retrieve.operation)
                    
                    };  
                    
                      self.trainVisible(false);
                      self.isDisabled(true);
                      self.hidenext(false);
                      self.currentStepValue('stp2');
                    }else if (self.retrieve.type === 'update') {
                         self.serviceType('update');
                        
                           self.validationModle = {
                            roleNameVal: ko.observable(self.retrieve.roleName),
                            firstKeyValue: ko.observable(self.retrieve.secondKeyValue),
                            personalInfromationVal: ko.observable(self.retrieve.fristPersonalInfromation),
                            operation: ko.observable(self.retrieve.operation)

                    };  
                        
                    }
                };


                 // define label
                 self.roleNamelbl=ko.observable();
                 self.firstKeyTypeLbl=ko.observable();
                 self.firstKeyValueLbl=ko.observable();
                 self.personalInfromationLbl=ko.observable();
                 self.operationLbl=ko.observable();
                 self.secondKeyTypeLbl = ko.observable();
                 self.secondKeyValueLbl = ko.observable();
                 self.secondpersonalInfromationLbl = ko.observable();
                 self.errorMassageEnLbl = ko.observable();
                 self.errorMassageArLbl = ko.observable(); 
                 self.arabicErrorMessageLbl = ko.observable(); 
              
              
                var getTranslation = oj.Translations.getTranslatedString;
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });
                function initTranslations() {
                     self.fristKeyTypeArr(app.getPaaSLookup('ROLE_FIRST_KEY_TYPE'));
                     self.SecondKeyTypeArr(app.getPaaSLookup('ROLE_FIRST_KEY_TYPE'));
                      self.operationArr(app.getPaaSLookup('OPERATION'));
                     
                    
                     self.reportTypeArr(app.getPaaSLookup('reportType'));
                       self.retrieve = oj.Router.rootInstance.retrieve();
                    if (self.retrieve === 'ADD') {
                        self.reportTitleLabl(getTranslation("report.addRoleSetupValidation"));

                    } else if (self.retrieve.type === 'view') {
                        self.reportTitleLabl(getTranslation("report.viewRoleSetupValidation"));
                    } else if (self.retrieve.type === 'update') {
                        self.reportTitleLabl(getTranslation("report.editRoleSetupValidation"));
                    }
                    self.reportName(getTranslation("report.reportName"));
                    self.Parameter1(getTranslation("report.Parameter1"));
                    self.Parameter2(getTranslation("report.Parameter2"));
                    self.Parameter3(getTranslation("report.Parameter3"));
                    self.Parameter4(getTranslation("report.Parameter4"));
                    self.Parameter5(getTranslation("report.Parameter5"));
                    self.createReport(getTranslation("report.createReport"));
                    self.errormessage(getTranslation("report.errormessage"));
                    self.createReport(getTranslation("report.create"));
                    self.reportNamelbl(getTranslation("report.reportNamelbl"));
                
                    self.reportTypelbl(getTranslation("report.reportType"));
                    self.errormessagecheckInDatabase(getTranslation("report.errormessagecheckInDatabase"));
                    self.oprationMessage(getTranslation("report.oprationMessage"));
                    self.confirmMessage(getTranslation("report.confirmMessage"));
                    self.oprationMessageUpdate(getTranslation("report.oprationMessageUpdate"));
                    self.yes(getTranslation("report.yes"));
                    self.no(getTranslation("report.no"));
                    self.addJob(getTranslation("job.addJob"));
                    self.updateJob(getTranslation("job.updateJob"));
                    self.viewJob(getTranslation("job.viewJob"));
                    self.addMassageLbl(getTranslation("job.addMessage"));
                    self.editMassageLbl(getTranslation("job.editMessage"));

                    self.trainView(getTranslation("others.review"));

                    self.pervious(getTranslation("others.pervious"));
                    self.next(getTranslation("others.next"));
                    self.cancel(getTranslation("others.cancel"));

                    self.submit(getTranslation("others.submit"));

                    self.create(getTranslation("labels.create"));
                    self.review(getTranslation("others.review"));
                    self.ok(getTranslation("others.ok"));
                   // new for Role 
                   self.roleNamelbl(getTranslation("report.roleName"));
                   self.firstKeyTypeLbl(getTranslation("validation.firstKeyType"));
                   self.firstKeyValueLbl(getTranslation("validation.firstKeyValue"));
                   self.personalInfromationLbl(getTranslation("validation.personalInfromation"));
                   self.operationLbl(getTranslation("validation.operation"));
                   self.secondKeyTypeLbl(getTranslation("validation.secondKeyType"));
                    self.secondKeyValueLbl(getTranslation("validation.secondKeyValue"));
                    self.secondpersonalInfromationLbl(getTranslation("validation.secondpersonalInfromation"));
                    self.errorMassageEnLbl(getTranslation("validation.errorMassageEn"));
                    self.arabicErrorMessageLbl(getTranslation("validation.arabicErrorMessage"));
                    self.errorMassageArLbl(getTranslation("validation.errorMassageAr"));
                    self.placeholder(getTranslation("labels.placeHolder"));

                    self.stepArray([{label: self.createReport(), id: 'stp1'},
                        {label: self.trainView(), id: 'stp2'}
                    ]);
                }
                initTranslations();
            }
            return JobContentViewModel;
        });
