/**
 * 
 * @param {type} oj
 * @param {type} ko
 * @param {type} $
 * @param {type} app
 * @param {type} commonUtil
 * @param {type} services
 * @param {type} translatehelper
 * @returns {JobL#13.JobContentViewModel}
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojknockout', 'ojs/ojtrain', 'ojs/ojbutton', 'ojs/ojarraydataprovider', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker','ojs/ojtable',
    'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup', 'ojs/ojcheckboxset', 'ojs/ojradioset',
    'ojs/ojlabel', 'ojs/ojgauge', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojmessage', 'jquery-calendar',
    'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin',
    'jquery-calendar-picker', 'ojs/ojvalidationgroup', 'ojs/ojarraydataprovider'],
        function (oj, ko, $, app, commonUtil, services) {
            /**
             * The view model for the main content view template
             */
            function JobContentViewModel() {
                var self = this;
                self.columnArray = ko.observableArray();
                self.addSummaryLbl = ko.observable();
                self.editSummaryLbl = ko.observable();
                self.viewSummaryLbl = ko.observable();
                self.reportNameLbl = ko.observable();
                self.parameter1Lbl = ko.observable();
                self.parameter2Lbl = ko.observable();
                self.parameter3Lbl = ko.observable();
                self.parameter4Lbl = ko.observable();
                self.parameter5Lbl = ko.observable();
                self.reportTypelbl = ko.observable();
                self.selectedIndex = ko.observable();
                self.backActionLbl = ko.observable();
                self.DeleteSummaryLbl = ko.observable();
                self.confirmMessage = ko.observable();
                self.oprationMessage = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                var deptArray = [];
                self.summaryObservableArray = ko.observableArray(deptArray);
                self.dataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.summaryObservableArray, {idAttribute: 'id'}));
                self.isDisable = ko.observable(true);
                self.dataSource;
                self.checkResultSearch = ko.observable();
                self.search = ko.observable();
                self.clear = ko.observable();
                 self.tableSelectionListener = function () {
                   self.isDisable(false);
//                   var element = document.getElementById('table');
//                   var currentRow = element.currentRow;
//                   self.selectedIndex(currentRow['rowIndex']);


                };

                self.btnAddSummary = function () {
                    app.loading(true);
                    oj.Router.rootInstance.store('ADD');
                    oj.Router.rootInstance.go('roleSetupValidationOperation');
                };
                self.btnEditSummary = function () {
                    app.loading(true);
                    var element = document.getElementById('table');
                    var currentRow = element.currentRow;
                    self.summaryObservableArray()[currentRow['rowIndex']].type = "update";
                    oj.Router.rootInstance.store(self.summaryObservableArray()[currentRow['rowIndex']]);
                    oj.Router.rootInstance.go('roleSetupValidationOperation');

                };
                self.btnViewSummary = function () {
                    app.loading(true);
                    var element = document.getElementById('table');
                    var currentRow = element.currentRow;
                    self.summaryObservableArray()[currentRow['rowIndex']].type = "view";
                    oj.Router.rootInstance.store(self.summaryObservableArray()[currentRow['rowIndex']]);
                    oj.Router.rootInstance.go('roleSetupValidationOperation');

                };
                self.btnDeleteSummary=function(){
                  document.querySelector("#yesNoDialog").open();
                };
                 self.commitRecordDelete=function(){
                     var element = document.getElementById('table');
                    var currentRow = element.currentRow;
                   var id = self.summaryObservableArray()[currentRow['rowIndex']].id;
                   var deleteReportCBF=function(data){
                       self.getSummary();
                       document.querySelector("#yesNoDialog").close();
                   };
                   
                  services.getGeneric("RoleSetupValidation/deleteRoleSetUpValidation/" + id, {}).then(deleteReportCBF, app.failCbFn);
                };
                 self.cancelButtonDelete=function(){
                  document.querySelector("#yesNoDialog").close();
                };
                self.roleSearch = {
                    rolename: ko.observable()
                };
             
                self.getSummary = function () {

                    var getReportCbFn = function (data) {
                        self.summaryObservableArray([]);
                        if (data.length !== 0)
                        {

                            $.each(data, function (index) {

                                self.summaryObservableArray.push({
                                    id: data[index].id,
                                    roleid: data[index].roleid,
                                    roleName: data[index].rolename,
                                    fristPersonalInfromation: data[index].personInfromation,
                                    operation: data[index].operation,
                                    secondKeyValue: data[index].staticValue
                                    

                                });
                            });
                        } else {
                            $.notify(self.checkResultSearch(), "error");
                        }



                    };
                    var failCbFn = function () {
                        $.notify(self.checkResultSearch(), "error");
                    };
                    var serviceName = commonUtil.getRoleSetupValidation;
                    services.getAllSummary(serviceName).then(getReportCbFn, failCbFn);
                };
                self.backAction = function () {
                    oj.Router.rootInstance.go('setup');
                };

         

                self.handleAttached = function (info) {
                    app.loading(false);
                    self.getSummary();
                 
                };

                self.rolenamelbl=ko.observable();
                self.fristKeyTypeLbl=ko.observable();
                self.fristKeyValueLbl=ko.observable();
                self.fristPersonalInformationLbl=ko.observable();
                self.operationLbl=ko.observable();
                self.secondKeyTypeLbl=ko.observable();
                self.secondKeyValuelbl=ko.observable();
                self.secondPersonalInfromationlbl=ko.observable();
                self.secondKeyValueLabel=ko.observable();


                var getTranslation = oj.Translations.getTranslatedString;

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();

                    }

                });
                
                self.searchPerson = function () {

                    if (!self.roleSearch.rolename()) {

                        self.getSummary();
                    } else {
                        self.summaryObservableArray([]);
                        self.searchRoleName();

                    }
                };
                
                self.reset = function () {
                    clearContent();
                };
                function clearContent() {
                    self.roleSearch.rolename("");
                    self.summaryObservableArray([]);
                }
                self.searchRoleName = function () {
                    var jsonData = ko.toJSON(self.roleSearch);
                    var roleNameCodeCbFn = function (data) {

                        if (data.length !== 0)
                        {

                             $.each(data, function (index) {

                                self.summaryObservableArray.push({
                                    id: data[index].id,
                                    roleid: data[index].roleid,
                                    roleName: data[index].rolename,
                                    fristPersonalInfromation: data[index].personInfromation,
                                    operation: data[index].operation,
                                    secondKeyValue: data[index].staticValue
                                    

                                });
                            });
                        } else {
                            $.notify(self.checkResultSearch(), "error");
                        }
                    };
                   
                    var failCbFn = function () {
                    };
                    services.addGeneric("RoleSetupValidation/search", jsonData).then(roleNameCodeCbFn, failCbFn);
                };
                function initTranslations() {
                    
                    self.addSummaryLbl(getTranslation("report.addSummary"));
                    self.editSummaryLbl(getTranslation("report.editSummary"));
                    self.viewSummaryLbl(getTranslation("report.viewSummary"));
                   
                  self.rolenamelbl(getTranslation("report.roleName"));
                    self.fristKeyTypeLbl(getTranslation("report.fristKeyType"));
                    self.fristKeyValueLbl(getTranslation("report.fristKeyValue"));
                    self.fristPersonalInformationLbl(getTranslation("report.fristPersonalInformation"));
                    self.operationLbl(getTranslation("report.operation"));
                    self.secondKeyTypeLbl(getTranslation("report.secondKeyType"));
                    self.secondKeyValuelbl(getTranslation("report.secondKeyValue"));
                    self.secondKeyValueLabel(getTranslation("validation.secondKeyValue"));
                    self.secondPersonalInfromationlbl(getTranslation("report.secondPersonalInfromation"));
                    self.backActionLbl(getTranslation("approvalScreen.backAction"));
                    self.checkResultSearch(getTranslation("common.checkResultSearch"));  
                    self.DeleteSummaryLbl(getTranslation("others.delete"));
                    self.confirmMessage(getTranslation("report.confirmMessage"));
                    self.oprationMessage(getTranslation("labels.message"));
                    self.no(getTranslation("others.no"));
                    self.yes(getTranslation("others.yes"));
                    self.search(getTranslation("others.search"));
                    self.clear(getTranslation("others.clear"));

                    self.columnArray([
                        {
                            "headerText": self.rolenamelbl(), "field": "roleName"
                        },
                        {
                            "headerText": self.fristPersonalInformationLbl(), "field": "fristPersonalInfromation"
                        },
                        {
                            "headerText": self.operationLbl(), "field": "operation"
                        },
                        {
                            "headerText": self.secondKeyValueLabel(), "field": "secondKeyValue"
                        }
                    ]);
                }
                initTranslations();
            }
            return JobContentViewModel;
        });
