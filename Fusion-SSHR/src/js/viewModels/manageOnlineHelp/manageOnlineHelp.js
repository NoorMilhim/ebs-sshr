/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * homeministryJob module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper',
    'ojs/ojknockout', 'ojs/ojarraydataprovider', 'ojs/ojdatetimepicker', 'ojs/ojtimezonedata',
    'ojs/ojselectcombobox', 'ojs/ojdialog', 'ojs/ojtrain', 'ojs/ojvalidationgroup', 'ojs/ojinputtext', 
    'ojs/ojlabel', 'ojs/ojfilepicker','ojs/ojmessages', 'ojs/ojmessage'],
        function (oj, ko, $, app, services, commonhelper) {

            function manageOnlineHelpOprationScreen() {
                var self = this;
                var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
                self.columnArray = ko.observableArray();
                self.tracker = ko.observable();
                self.groupValid = ko.observable();
                self.placeholder = ko.observable("Please Select Value");
                self.dataSourceTB2 = ko.observable();
                self.isVisibleRoleName = ko.observable(false);
                self.isVisibleSpecialCase = ko.observable(false);
                var deptArray = [];
                self.isDisabled = ko.observable(false);
                self.selctedStruct = ko.observable();
                self.isRequired = ko.observable(true);
                self.currentStepValue = ko.observable('stp1');
                self.addBtnVisible = ko.observable(false);
                self.nextBtnVisible = ko.observable(true);
                self.pageMode = ko.observable();
                self.supervisorHRCYesNoList = rootViewModel.globalHRCYesNo();
                self.oprationMessage = ko.observable();
                self.oprationEditMessage = ko.observable();
                self.approval = ko.observable();
                self.disableSubmit = ko.observable(false);
                self.xx = ko.observable();
                self.isRequiredRoleName = ko.observable(false);
                self.isRequiredSpecialCase = ko.observable(false);
                self.roleOptionType = ko.observableArray([]);
                self.rolesOption = ko.observableArray([]);
                self.approvalCodeArr = ko.observableArray([]);
                self.historyDetails = ko.observable();
                var deptArray = [];
                self.disableBtnSubmit = ko.observable(false);
                self.isExist = ko.observable();
                self.deptObservableArray = ko.observableArray(deptArray);
                self.isVisibleRoles = ko.observable(false);
                self.isRequiredRoles = ko.observable(false);
                self.isExist = ko.observable();
                var countValid = "";
                //-------------------- icon attachment -----------------------//
                self.attachmentLbl = ko.observable();
                self.UploadFileLbl = ko.observable();
                self.removeBtnLbl = ko.observable();
                self.viewLbl = ko.observable();
                var dataFiles = {};
                self.koArray = ko.observableArray([]);
                self.dataProviderAttachment = new oj.ArrayDataProvider(self.koArray, {keyAttributes: 'id'});
                self.attachmentId = 0;
                self.selectedItemsAttachment = ko.observableArray([]);
                self.attachmentViewerData = ko.observable();
                self.validateAttachment = ko.observable();
                //--------------- define messages notification ---------------//
                self.messages = ko.observableArray();
                self.messagesDataProvider = new oj.ArrayDataProvider(self.messages);
                self.position =
                        {
                            "my": {"vertical": "top", "horizontal": "end"},
                            "at": {"vertical": "top", "horizontal": "end"},
                            "of": "window"
                        };
                self.closeMessageHandler = function (event) {
                    self.messages.remove(event.detail.message);
                };
                //------------------------------------------------------------//
                // Call self Service
                self.EitNameArr = ko.observableArray([]);
                self.staticRequestArr=ko.observableArray();
    
   
                var positionObj;
                self.stepArray = ko.observableArray([{label: "XXXX", id: 'stp1'},
                    {label: "YYYY", id: 'stp2'}]);//workNatureAllowance
               
                self.manageSetupModel = {
                    id: ko.observable(),
                    selfSerivce: ko.observable(),  
                    descriptionEn: ko.observable(""),
                    descriptionAr: ko.observable(""),
                    selfServDraftMessageAr: ko.observable(""),
                    selfServDraftMessageEn: ko.observable(""),
                    selfServConfMessageEn: ko.observable(""),
                    selfServConfMessageAr: ko.observable(""),
                    rejectConfirmMessageVal: ko.observable(""),
                    rejectConfirmMessageArVal: ko.observable(""),
                    iconData:ko.observable(),
                    iconName:ko.observable(),
                    iconId:ko.observable()

                };

                self.backAction = function () {
                    if (oj.Router.rootInstance._navHistory.length > 1) {
                        app.loading(true);
                        oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                    }
                };
 
                self.handleAttached = function () {
                    app.loading(false);
                    initTranslations();
                     var arr=[{value: "XXX_RECRUITMENT_REQUEST",
                                label: self.EmployeeProbationaryPeriodEvaluationLBL()}];
                        var totalArr = [];
                    totalArr = arr.concat(app.globalEitNameReport());
                    self.EitNameArr(totalArr);
                    positionObj = oj.Router.rootInstance.retrieve();

                     

                    if (positionObj.type == 'ADD') {

                        self.pageMode('ADD');
                         self.clothesAllowance(self.addManageLbl());
                        self.validationMessage(self.manageAdd());
                        
                    } else if (positionObj.type == 'EDIT') {
                        self.pageMode('EDIT');
                        
                        self.connected();
                        self.clothesAllowance(self.editManageLbl());
                        self.validationMessage(self.editMassageLbl());
                        self.manageSetupModel.id=positionObj.id;
                  

                    }

                };

                //-------------This Function For Update Train Step To previousStep------------------
                self.previousStep = function () {
                    var prev = document.getElementById("train").getPreviousSelectableStep();
                    if (prev != null)
                        self.currentStepValue(prev);
                };
                self.nextStep = function ()
                {

                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        return;
                    }
                    var next = document.getElementById("train").getNextSelectableStep();
                    if (next != null)
                    {
                        if(self.koArray().length <= 0){
                            self.messages.push({
                                severity: 'error',
                                summary: self.validateAttachment(),
                                autoTimeout: 0
                            });
                            return;
                        }else{
                          if (parseInt(countValid) > 0)
                        {
                            //var popup = document.querySelector('#NoDialog');
                            $.notify(self.validMessageLbl(), "error");
                            //popup.open();                             
                        } else
                        {
                           var flag = true;
  
                            if (flag)
                            {

                                self.currentStepValue(next);

                                self.currentStepValueText();
                            }
                        }  
                        }
                        
                    }

                };
                self.currentStepValueText = function () {
                    if (self.currentStepValue() == 'stp2') {
                        self.isDisabled(true);
                        self.addBtnVisible(true);
                        self.nextBtnVisible(false);


                    } else {
                        self.isDisabled(false);
                        self.addBtnVisible(false);
                        self.nextBtnVisible(true);

                    }

                    return self.clothesAllowance();
                };
                //------------------End Of Section -------------------------------------------------
                        
                //------------------This Section For Dialog Action (Open ,Close)
                self.submitButton = function () {
                    document.querySelector("#yesNoDialog").open();
                };
                self.cancelButton = function () {
                    document.querySelector("#yesNoDialog").close();
                };
                self.cancelButtonDialog = function () {
                    document.querySelector("#NoDialog").close();
                    document.querySelector("#yesNoDialog").close();
                };
                //------------------End Of Section ----------------------------------------------
                
                //-------------This Function To Stop Train Action If Action Are Not Valid -------
                self.stopSelectListener = function (event, ui) {
                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        event.preventDefault();
                        return;
                    }
                    if (self.koArray().length <= 0) {
                        self.messages.push({
                            severity: 'error',
                            summary: self.validateAttachment(),
                            autoTimeout: 0
                        });
                        event.preventDefault();
                        return;
                    }
                };
                //-------------------End Of Function ---------------------------------------------
                //------------------ This Section For Call Web Service For Insert Or Update ---------
                function oprationServiceRecord() {
                    self.manageSetupModel.iconData(self.koArray()[0].data);
                    self.manageSetupModel.iconName(self.koArray()[0].name);
                    var jsonData = ko.toJSON(self.manageSetupModel);
                   
                    if (parseInt(countValid) > 0)
                    {
                        document.querySelector("#yesNoDialog").close();
                        var popup = document.querySelector('#NoDialog');
                        popup.open();

                        self.isDisabled(false);

                       } else
                       {
                        var getValidGradeCBF = function (data) {


                            var Data = data;
                            self.disableSubmit(false);
                            if (oj.Router.rootInstance._navHistory.length > 1) {
                                oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                            } else {
                                oj.Router.rootInstance.go('manageOnlinHelpSummary');
                            }
                        };
                                    
                       if (positionObj.type === 'ADD') 
                       {
                           
                           var getValidGradeCBF = function (data) {
                               
                               if(data === '0'){
                                  
                                   return ;
                               }
   
                                self.deptObservableArray(data);
                                if (oj.Router.rootInstance._navHistory.length > 1) {
                                oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                                 } 
                                 else {
                                oj.Router.rootInstance.go('manageOnlinHelpSummary');
                                 }
                                
                                };
                                 var failCbFn = function () {
                                $.notify(self.isExist(), "error");
                                 document.querySelector("#yesNoDialog").close();
                                 };
                           
                             services.addGeneric(commonhelper.addSelfService +self.pageMode(), jsonData).then(getValidGradeCBF, failCbFn);

                         }
                       else{
                            services.addGeneric(commonhelper.addSelfService +self.pageMode(), jsonData).then(getValidGradeCBF, failCbFn);
                       }
    
                    }
                };
                self.commitRecord = function (data, event) {
                    if (!self.disableSubmit()) {
                        self.disableSubmit(true);
                        oprationServiceRecord();
                        return true;
                    }
                };

                //-------------------------Start Load Funcation------------------------------
   
                self.connected = function () {
                    self.manageSetupModel.selfSerivce(oj.Router.rootInstance.retrieve().selfSerivce);
                    self.manageSetupModel.descriptionAr(oj.Router.rootInstance.retrieve().descriptionAr);
                    self.manageSetupModel.descriptionEn(oj.Router.rootInstance.retrieve().descriptionEn);
                    self.manageSetupModel.selfServDraftMessageAr(oj.Router.rootInstance.retrieve().selfServDraftMessageAr);
                    self.manageSetupModel.selfServDraftMessageEn(oj.Router.rootInstance.retrieve().selfServDraftMessageEn);
                    self.manageSetupModel.selfServConfMessageAr(oj.Router.rootInstance.retrieve().selfServConfMessageAr);
                    self.manageSetupModel.selfServConfMessageEn(oj.Router.rootInstance.retrieve().selfServConfMessageEn);
                    self.manageSetupModel.rejectConfirmMessageVal(oj.Router.rootInstance.retrieve().rejectConfirmMessageVal);
                    self.manageSetupModel.rejectConfirmMessageArVal(oj.Router.rootInstance.retrieve().rejectConfirmMessageArVal);
                    
                    var getIcons = function (data) {
                        if(data.length>0){
                         self.koArray.push({
                            "name": data[0].NAME,
                            "data": data[0].ICONS
                        });
                        self.manageSetupModel.iconId(data[0].ID);
                        }
                        
                    };
                    var failCbFn = function () {};
                    services.getGeneric(commonhelper.iconSetupUrl + self.manageSetupModel.selfSerivce(), {}).then(getIcons, failCbFn);

                };
                //-------------------------End Load Function---------------------------------
                  
                  
                //---------------------Translate Section ----------------------------------------
                self.ok = ko.observable();
                self.startDateLbl = ko.observable();
                self.endDateLbl = ko.observable();
                self.addLbl = ko.observable();
                self.editLbl = ko.observable();
                self.gradeLbl = ko.observable();
                self.validGradeLbl = ko.observable();
                self.selfServDraftMessageArLbl = ko.observable();
                self.selfServDraftMessageEnLbl = ko.observable();
                self.structureNameLbl = ko.observable();
                self.backLbl = ko.observable();
                self.nextLbl = ko.observable();
                self.submitLbl = ko.observable();
                self.placeholder = ko.observable();
                self.eitCodeLbl = ko.observable();
                self.selfService = ko.observable();
                self.descriptionArLbl = ko.observable();
                self.descriptionEnLbl = ko.observable();
                self.selfServiceConfMessageArLbl = ko.observable();
                self.selfServiceConfMessageEnLbl = ko.observable();

                self.EmployeeProbationaryPeriodEvaluationLBL=ko.observable();

                self.cancel = ko.observable();
                self.create = ko.observable();
                self.review = ko.observable();
                self.workNatureAllowanceScreenAddLbl = ko.observable();
                self.workNatureAllowanceScreenEditLbl = ko.observable();
                self.manageAdd = ko.observable();
                self.addManageLbl = ko.observable();
                self.editManageLbl = ko.observable();
                self.addMassageLbl = ko.observable();
                self.validMessageLbl = ko.observable();
                self.editMassageLbl = ko.observable();
                self.validationMessage = ko.observable();
                self.confirmMessage = ko.observable();
                self.clothesAllowance = ko.observable();
                self.roleNameLbl = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.validNotificationTypeLbl = ko.observable();
                self.RejectConfirmMessageEnLbl = ko.observable();
                self.RejectConfirmMessageArLbl = ko.observable();
                self.rolesLbl = ko.observable();
                var getTranslation = oj.Translations.getTranslatedString;
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        self.stepArray([{label: self.create(), id: 'stp1'},
                            {label: self.review(), id: 'stp2'}]);
                        initTranslations();
                    }
                });
                //-------------------------- icon attachment selection ---------------------//
                self.attachmentSelectListener = function (event) {
                    var files = event.detail.files;
                    if (files.length > 0) {

                        //add the new files at the beginning of the list

                        for (var i = 0; i < files.length; i++) {
                            function getBase64(file) {
                                return new Promise((resolve, reject) => {
                                    const reader = new FileReader();
                                    reader.readAsDataURL(file);
                                    reader.onload = () => resolve(reader.result);
                                    reader.onerror = error => reject(error);
                                });
                            }
                            dataFiles.name = files[i].name;
                            dataFiles.id = i + 1;
                            getBase64(files[i]).then(function (data) {
                                dataFiles.data = (data);
                                self.attachmentId = self.attachmentId + 1;
                                dataFiles.id = (self.attachmentId);
                                self.koArray([]);
                                self.koArray.push({
                                    "name": dataFiles.name,
                                    "data": dataFiles.data,
                                    "id": dataFiles.id
                                });
                            });
                        }
                    }

                };
                self.removeSelectedAttachment = function (event) {
                    self.koArray([]);
                };
                self.openAttachmentViewer = function (){
                    $.each(self.selectedItemsAttachment(), function (index, value)
                    {
                        $.each(self.koArray(), function (indexInner, valueInner)
                        {
                                self.attachmentViewerData(valueInner.data);
                                document.getElementById('attachmentDialog').open();
                        });
                    });
                };
                self.closeAttachmentViewer = function (event) {
                    self.attachmentViewerData('');
                    document.getElementById('attachmentDialog').close();
                };
                //--------------------------------------------------------------//
                function initTranslations() {
                    self.create(getTranslation("labels.create"));
                    self.review(getTranslation("others.review"));
                    self.structureNameLbl(getTranslation("additionalDetails.structureName"));
                    self.backLbl(getTranslation("others.back"));
                    self.nextLbl(getTranslation("others.next"));
                    self.submitLbl(getTranslation("others.submit"));
                    self.ok(getTranslation("others.ok"));
                    self.addLbl(getTranslation("others.add"));
                    self.editLbl(getTranslation("others.edit"));
                    self.validGradeLbl(getTranslation("others.validGrade"));
                    self.selfServDraftMessageArLbl(getTranslation("manageOnlineHelp.selfServDraftMessageArLbl"));
                    self.selfServDraftMessageEnLbl(getTranslation("manageOnlineHelp.selfServDraftMessageEnLbl"));
                    self.eitCodeLbl(getTranslation("approvalScreen.eitCode"));
                    self.selfService(getTranslation("manageOnlineHelp.selfSerivce"));
                    self.descriptionArLbl(getTranslation("manageOnlineHelp.descriptionArLbl"));
                    self.descriptionEnLbl(getTranslation("manageOnlineHelp.descriptionEnLbl"));
                    self.selfServiceConfMessageArLbl(getTranslation("manageOnlineHelp.selfServiceConfMessageArLbl"));
                    self.selfServiceConfMessageEnLbl(getTranslation("manageOnlineHelp.selfServiceConfMessageEnLbl"));
                    self.cancel(getTranslation("others.cancel"));
                    self.workNatureAllowanceScreenAddLbl(getTranslation("manageOnlineHelp.manageAdd"));
                    self.workNatureAllowanceScreenEditLbl(getTranslation("approvalScreen.ApprovalEdit"));
                    self.manageAdd(getTranslation("manageOnlineHelp.manageAdd"));
                    self.addManageLbl(getTranslation("manageOnlineHelp.addManageLbl"));
                    self.addManageLbl(getTranslation("manageOnlineHelp.addManageLbl"));
                    self.editManageLbl(getTranslation("manageOnlineHelp.editManageLbl"));
                    self.editMassageLbl(getTranslation("manageOnlineHelp.manageEdit"));
                    self.confirmMessage(getTranslation("labels.confirmMessage"));
                    self.oprationEditMessage(getTranslation("manageOnlineHelp.oprationEditMessage"));
                    self.yes(getTranslation("others.yes"));
                    self.no(getTranslation("others.no"));
                    self.roleNameLbl(getTranslation("approvalScreen.roleName"));
                    self.placeholder(getTranslation("labels.placeHolder"));
                    self.validNotificationTypeLbl(getTranslation("workNatureAllowance.notificationMessage"));
                    self.rolesLbl(getTranslation("approvalScreen.roles"));
                    self.EmployeeProbationaryPeriodEvaluationLBL(getTranslation("approvalScreen.EmployeeProbationaryPeriodEvaluation"));
                    self.isExist(getTranslation("common.isExist"));
                    self.RejectConfirmMessageEnLbl(getTranslation("common.RejectConfirmMessageEnLbl"));
                    self.RejectConfirmMessageArLbl(getTranslation("common.RejectConfirmMessageArLbl"));
                    self.attachmentLbl(getTranslation("iconSetup.attachmentLbl"));
                    self.UploadFileLbl(getTranslation("common.UploadFileLbl"));
                    self.removeBtnLbl(getTranslation("common.removeBtnLbl"));
                    self.viewLbl(getTranslation("common.viewLbl")); 
                    self.validateAttachment(getTranslation("iconSetup.validateAttachment"));
                    self.stepArray([{label: self.create(), id: 'stp1'},
                        {label: self.review(), id: 'stp2'}]);
                }
                initTranslations();

            }

            return manageOnlineHelpOprationScreen;
        });
