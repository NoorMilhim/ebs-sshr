/**
 * 
 * @param {type} oj
 * @param {type} ko
 * @param {type} $
 * @param {type} app
 * @param {type} commonUtil
 * @param {type} services
 * @param {type} translatehelper
 * @returns {JobL#13.JobContentViewModel}
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojknockout', 'ojs/ojtrain', 'ojs/ojbutton', 'ojs/ojarraydataprovider', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker', 'ojs/ojtable',
    'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup', 'ojs/ojcheckboxset', 'ojs/ojradioset',
    'ojs/ojlabel', 'ojs/ojgauge', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojmessage', 'jquery-calendar',
    'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin',
    'jquery-calendar-picker', 'ojs/ojvalidationgroup', 'ojs/ojarraydataprovider', 'xlsx'],
        function (oj, ko, $, app, commonUtil, services) {
            /**
             * The view model for the main content view template
             */
            function JobContentViewModel() {
                var self = this;
                self.columnArray = ko.observableArray();
                self.addSummaryLbl = ko.observable();
                self.editSummaryLbl = ko.observable();
                self.viewSummaryLbl = ko.observable();
                self.reportNameLbl = ko.observable();
                self.parameter1Lbl = ko.observable();
                self.parameter2Lbl = ko.observable();
                self.parameter3Lbl = ko.observable();
                self.parameter4Lbl = ko.observable();
                self.parameter5Lbl = ko.observable();
                self.reportTypelbl = ko.observable();
                self.groupValid = ko.observable();
                self.Start_dateLbl = ko.observable();
                self.End_dateLbl = ko.observable();
                self.start_dateval = ko.observable();
                self.end_dateval = ko.observable();
                self.viewSearch = ko.observable(false);
                self.viewExport = ko.observable(false);
                self.titleValue = ko.observable();

                var deptArray = [];
                self.summaryObservableArray = ko.observableArray(deptArray);
                self.dataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.summaryObservableArray, {idAttribute: 'personNumber'}));
                self.isDisable = ko.observable(true);
                self.dataSource;
                self.checkResultSearch = ko.observable();
                self.dateConverter = ko.observable(oj.Validation.converterFactory(oj.ConverterFactory.CONVERTER_TYPE_DATETIME).createConverter({
                    pattern: 'dd/MM/yyyy'
                }));




                self.tableSelectionListener = function () {
                    self.isDisable(false);

                };

                self.getSummary = function () {
                    self.summaryObservableArray([]);
                    var getReportCbFn = function (data) {
                        if (data.length !== 0)
                        {

                            $.each(data, function (index) {

                                self.summaryObservableArray.push({
                                    seq: index + 1,
                                    punch_date: data[index].punch_date,
                                    personNumber: data[index].personNumber,
                                    personName: data[index].personName,
                                    sr_number: data[index].sr_number,
                                    todaytDate: data[index].todaytDate,
                                    deviceName: data[index].deviceName,
                                    perviousDatePunchOut: data[index].perviousDatePunchOut,
                                    perviousDateDate: data[index].perviousDateDate,
                                    perviousDatePunchIn: data[index].perviousDatePunchIn

                                });
                            });
                        } else {
                            $.notify(self.checkResultSearch(), "error");
                        }



                    };
                    var failCbFn = function () {
                        $.notify(self.checkResultSearch(), "error");
                    };
                    var serviceName = "EmployeeTimeAttandance/getAllEmployeeTimeAttandance";
                    services.getAllSummary(serviceName).then(getReportCbFn, failCbFn);
                };



//////////////////////////////

                self.checkforInsertdate = ko.observable();
                self.reportName = ko.observable();
                self.personNumberVal = ko.observable("");
                self.punch_dateval = ko.observable("");

                self.search = ko.observable();
                self.clear = ko.observable();


                self.reportSearch = {
                    reportname: ko.observable()

                };
                var el = document.getElementById("searchName");


                self.searchPerson = function () {
                    self.summaryObservableArray([]);

                    if (!self.punch_dateval()) {

                        $.notify(self.checkforInsertdate(), "error");
                    } else {

                        self.searchReportName();

                    }
                };

                self.reset = function () {
                    clearContent();
                };
                function clearContent() {
                    self.personNumberVal("");
                    self.punch_dateval("");
                    self.summaryObservableArray([]);
                }

                self.checkResultSearch = ko.observable();
                self.searchReportName = function () {
                    self.summaryObservableArray([]);
                    var payload = {

                        "punch_date": self.punch_dateval(),
                        "personNumber": self.personNumberVal(),
                        "startDate": self.start_dateval(),
                        "endDate": self.end_dateval(),
                        "employeeNumber": app.personDetails().personNumber

                    };
                    var reportNameCodeCbFn = function (data) {




                        if (data.length !== 0)
                        {

                            $.each(data, function (index) {

                                self.summaryObservableArray.push({
                                    seq: index + 1,
                                    punch_date: data[index].punch_date,
                                    personNumber: data[index].personNumber,
                                    personName: data[index].personName,
                                    sr_number: data[index].sr_number,
                                    todaytDate: data[index].todaytDate,
                                    deviceName: data[index].deviceName,
                                    perviousDatePunchOut: data[index].perviousDatePunchOut,
                                    perviousDateDate: data[index].perviousDateDate,
                                    perviousDatePunchIn: data[index].perviousDatePunchIn

                                });
                            });
                        } else {
                            $.notify(self.checkResultSearch(), "error");
                        }
                    };
                    var failCbFn = function () {
                    };



                    var serviceName = "EmployeeTimeAttandance/getAllEmployeeTimeAttandanceByPersonNumber/";
                    services.addGeneric(serviceName, JSON.stringify(payload)).then(reportNameCodeCbFn, failCbFn);
                };


                self.searchDependOnRoleBtn = function () {
                    var payload = {
                        "id":500,//act as size for paging
                        "startDate": self.start_dateval(),
                        "endDate": self.end_dateval()
                    };

                    var reportNameCodeCbFn = function (data) {
                        $("#searchDependOnRoleBtn").removeClass("loading");
                        if(data && data.message == 'Done'){
                            var arr = data.data;
                            try{  arr = JSON.parse(arr);  }catch(e){};
                            if(arr.length > 0){
                        // app.loading(false);
                     //console.log(data);
//                        var header = ['todaytDate', 'sr_number', 'punch_date', 'perviousDatePunchOut', 'perviousDatePunchIn', 'perviousDateDate', 'personNumber', 'personName', 'deviceName'];
                        var header = ['sr_number','personNumber',  'personName','todaytDate', 'punch_date', 'deviceName', 'perviousDateDate', 'perviousDatePunchIn', 'perviousDatePunchOut'];
                       var sheetData = [header, ...arr.map(e => {
                                var valus = [];
//                                 Object.values(e)
                                header.forEach(head => {
                                    valus.push(e[head] || "");
                                })
                              //  console.log(valus.length)
                                return valus;
                            })];



                     //   console.log(sheetData)
                        var filename = "TimeAttendance";
                        const book = XLSX.utils.book_new();
                        const sheet = XLSX.utils.aoa_to_sheet(sheetData);
                        XLSX.utils.book_append_sheet(book, sheet, 'TimeAttendance');
                        XLSX.writeFile(book, `${filename}.xls`);


                        //                        console.log(data);
//                        var link = document.createElement("a");
//                        document.body.appendChild(link);
//                        link.setAttribute("type", "hidden");
//                        link.href = "data:text/plain;base64," + data;
//                        link.download = "TimeAttendance.xls";
//                        link.click();
//                        
//                        document.body.removeChild(link);

                        }else{
                            app.messages.push({ severity: "error", summary: "No Records found", autoTimeout: 0 });//default time out
                        }
                    }else{
                        app.messages.push({ severity: "error", summary: data.data, autoTimeout: 0 });//default time out
                    }
                    };
                    var failCbFn = function () {
                        $("#searchDependOnRoleBtn").removeClass("loading");
                        // app.loading(false);
                       // console.log("Field to Download the file")
                    };

                    // app.loading(true);
                    $("#searchDependOnRoleBtn").addClass("loading");
                    var serviceName = "EmployeeTimeAttandance/getAllEmployeeTimeAttandance/";
                    services.addGeneric(serviceName, JSON.stringify(payload)).then(reportNameCodeCbFn, failCbFn);
                };


                self.handleExportExcel = function () {
                    var payload = {

                        "punch_date": self.punch_dateval(),
                        "personNumber": self.personNumberVal(),
                        "startDate": self.start_dateval(),
                        "endDate": self.end_dateval(),
                        "employeeNumber": app.personDetails().personNumber
                    };

                    var reportNameCodeCbFn = function (data) {
                       // console.log(data);
                        var link = document.createElement("a");
                        document.body.appendChild(link);
                        link.setAttribute("type", "hidden");
                        link.href = "data:text/plain;base64," + data;
                        link.download = "TimeAttendance.xls";
                        link.click();
                        document.body.removeChild(link);
                    };
                    var failCbFn = function () {
                       // console.log("Field to Download the file")
                    };



                    var serviceName = "EmployeeTimeAttandance/getAllEmployeeTimeAttandanceByPersonNumberExcelSheet/";
                    services.addGeneric(serviceName, JSON.stringify(payload)).then(reportNameCodeCbFn, failCbFn);
                };



                ////////////////////


                self.handleAttached = function (info) {
                    app.loading(false);
                    //HHA_PAAS_OTL_OFFICER_CUSTOM
//                    console.log(sessionStorage.getItem("userRole"));
                    var userRoles;
                    if (JSON.parse(sessionStorage.getItem("userRole"))) {
                        userRoles = JSON.parse(sessionStorage.getItem("userRole"));
                        for (var i = 0; i < userRoles.length; i++) {
                            if (userRoles[i].ROLE_NAME == "HHA PAAS OTL Officer Custom") {
                                self.viewExport(true);
                            } else if (userRoles[i].ROLE_NAME == "Application Administrator") {
                                self.viewSearch(true);
                            }
                        }
                    }

                    // userRoles[i].ROLE_NAME

                };

                self.punch_dateLbl = ko.observable();
                self.personNumberLbl = ko.observable();
                self.personNameLbl = ko.observable();
                self.sr_numberLbl = ko.observable();
                self.todaytDateLbl = ko.observable();
                self.deviceNameLbl = ko.observable();
                self.perviousDatePunchOutLbl = ko.observable();
                self.perviousDateDateLbl = ko.observable();
                self.perviousDatePunchInLbl = ko.observable();
                self.searchDependOnRoleLbl = ko.observable();
                self.exportToExcelLbl = ko.observable();


                var getTranslation = oj.Translations.getTranslatedString;

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();

                    }

                });
                function initTranslations() {
                    self.search(getTranslation("others.search"));
                    self.clear(getTranslation("others.clear"));
                    self.addSummaryLbl(getTranslation("report.addSummary"));
                    self.editSummaryLbl(getTranslation("report.editSummary"));
                    self.viewSummaryLbl(getTranslation("report.viewSummary"));
                    self.reportNameLbl(getTranslation("report.reportName"));
                    self.parameter1Lbl(getTranslation("report.Parameter1"));
                    self.parameter2Lbl(getTranslation("report.Parameter2"));
                    self.parameter3Lbl(getTranslation("report.Parameter3"));
                    self.parameter4Lbl(getTranslation("report.Parameter4"));
                    self.parameter5Lbl(getTranslation("report.Parameter5"));
                    self.reportTypelbl(getTranslation("report.reportType"));
                    self.checkResultSearch(getTranslation("common.checkResultSearch"));

                    self.punch_dateLbl(getTranslation("common.punch_dateLbl"));
                    self.personNumberLbl(getTranslation("common.personNumberLbl"));
                    self.personNameLbl(getTranslation("common.personNameLbl"));
                    self.sr_numberLbl(getTranslation("common.sr_numberLbl"));
                    self.todaytDateLbl(getTranslation("common.todaytDateLbl"));
                    self.deviceNameLbl(getTranslation("common.deviceNameLbl"));
                    self.perviousDatePunchOutLbl(getTranslation("common.perviousDatePunchOutLbl"));
                    self.perviousDateDateLbl(getTranslation("common.perviousDateDateLbl"));
                    self.perviousDatePunchInLbl(getTranslation("common.perviousDatePunchInLbl"));
                    self.checkforInsertdate(getTranslation("common.checkforInsertdate"));
                    self.Start_dateLbl(getTranslation("common.Start_dateLbl"));
                    self.End_dateLbl(getTranslation("common.End_dateLbl"));
                    self.searchDependOnRoleLbl(getTranslation("common.searchDependOnRole"));
                    self.exportToExcelLbl(getTranslation("common.exportToExcel"));
                    self.titleValue(getTranslation("common.titleValue"));




                    self.columnArray([

                        {
                            "headerText": "#SR", "field": "seq"
                        },

                        {
                            "headerText": self.personNumberLbl(), "field": "personNumber"
                        },
                        {
                            "headerText": self.personNameLbl(), "field": "personName"
                        },
                        {
                            "headerText": self.punch_dateLbl(), "field": "punch_date"
                        },
                        {
                            "headerText": "#SR", "field": "sr_number"
                        },
                        {
                            "headerText": self.todaytDateLbl(), "field": "todaytDate"
                        },
                        {
                            "headerText": self.deviceNameLbl(), "field": "deviceName"
                        },
                        {
                            "headerText": self.perviousDateDateLbl(), "field": "perviousDateDate"
                        },
                        {
                            "headerText": self.perviousDatePunchInLbl(), "field": "perviousDatePunchIn"
                        },
                        {
                            "headerText": self.perviousDatePunchOutLbl(), "field": "perviousDatePunchOut"
                        }
                    ]);
                }
                initTranslations();
            }
            return JobContentViewModel;
        });
