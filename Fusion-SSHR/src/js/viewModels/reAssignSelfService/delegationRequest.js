/**
 * 
 * @param {type} oj
 * @param {type} ko
 * @param {type} $
 * @param {type} app
 * @param {type} commonUtil
 * @param {type} services
 * @param {type} translatehelper
 * @returns {JobL#13.JobContentViewModel}
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojknockout', 'ojs/ojtrain', 'ojs/ojbutton', 'ojs/ojarraydataprovider', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker','ojs/ojtable',
    'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup', 'ojs/ojcheckboxset', 'ojs/ojradioset',
    'ojs/ojlabel', 'ojs/ojgauge', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojmessage', 'jquery-calendar',
    'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin',
    'jquery-calendar-picker', 'ojs/ojvalidationgroup', 'ojs/ojarraydataprovider'],
        function (oj, ko, $, app, commonUtil, services) {
            /**
             * The view model for the main content view template
             */
            function JobContentViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
                var deptArray = [];
                var pesonId = app.personDetails().personId;
                var globalCodeTOSelfServices;
                
                self.columnArray = ko.observableArray();
                self.Employeedata = ko.observableArray(deptArray);
                self.dataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.Employeedata, {idAttribute: 'nationalId'}));
                self.isDisable = ko.observable(true);
                self.dataSource;
                self.roelsArray = [];
                self.selfServices = ko.observableArray([]);
                self.editDisabled = ko.observable(true);
                self.delegationRequest = ko.observable();
                self.EffectiveStartDateLBl = ko.observable();      
                self.EffectiveEnddateLBL = ko.observable();
                self.personNumberLbl = ko.observable();
                self.searchLbl = ko.observable();
                
                self.nationalId = ko.observable();
                self.effictiveDate = ko.observable();
                self.searchLbl = ko.observable();
                self.clearLbl = ko.observable();
                self.personNumberLbl = ko.observable();
                self.employeeNmaeLbl = ko.observable();
                self.NationalIdLbl = ko.observable();
                self.departmentLbl = ko.observable();
                self.positionLbl = ko.observable();
                self.locationLbl = ko.observable();
                self.actionLbl = ko.observable();
                self.goTo = ko.observable();
                self.insertValue = ko.observable();
          
             
                
                // Start Search Method    
                function buildValueChange(valueParam) {
                    var valueObj = {};
                    valueObj.value = valueParam.value;
                    return valueObj;
                }

                self.PersonChangeHandler = function (event) {
                    var valueObj = buildValueChange(event['detail']);
                    globalCodeTOSelfServices = valueObj.value;
                };
//                


                var getPersonDetailsCbFn = function (data) {  
                    self.Employeedata.push({
                                    assignmentName: data.assignmentName, assignmentProjectedEndDate: data.assignmentProjectedEndDate, assignmentStatus: data.assignmentStatus,
                                    assignmentStatusTypeId: data.assignmentStatusTypeId, carAllowance: data.carAllowance, citizenshipLegislationCode: data.citizenshipLegislationCode, nationalId: (data.nationalId === 'null' || data.nationalId === null) ? '' : data.nationalId,
                                    city: data.city, country: data.country, dateOfBirth: data.dateOfBirth, department: data.department, displayName: data.displayName, personNumber: data.personNumber,
                                    email: data.email, employeeCertificate: data.employeeCertificate, employeeLocation: data.employeeLocation, fax: data.fax, faxExt: data.faxExt,
                                    grade: data.grade, gradeId: data.gradeId, hireDate: data.hireDate, housingType: data.housingType,
                                    jobId: data.jobId, jobName: data.jobName, legalEntityId: data.legalEntityId, managerId: data.managerId, managerName: data.managerName, managerOfManager: data.managerOfManager,
                                    managerOfMnagerName: data.managerOfMnagerName, managerType: data.managerType, maritalStatus: data.maritalStatus, ministryJobForEmployee: data.ministryJobForEmployee, mobileAllowance: data.mobileAllowance, mobileNumber: data.mobileNumber,
                                    organizationName: data.organizationName, overTimePaymentUrl: data.overTimePaymentUrl, overTimeURL: data.overTimeURL, peopleGroup: data.peopleGroup, personId: data.personId, picBase64: data.picBase64,
                                    positionName: data.positionName, probationPeriodEndDate: data.probationPeriodEndDate, projectedStartDate: data.projectedStartDate, region: data.region, salaryAmount: data.salaryAmount, transportationAlowance: data.transportationAlowance,
                                    workPhone: data.workPhone, workPhoneExt: data.workPhoneExt, employeeURL: data.employeeURL
                     });
              
                };

                self.SearchBTn = function () {
                      self.Employeedata([]);
                        services.searchEmployees(self.searchModel.name(), self.searchModel.nationalId(), self.searchModel.personNumberVal(), self.searchModel.managerId(), self.searchModel.effectiveDate()).then(getPersonDetailsCbFn, app.failCbFn); 
               };


       


                

              
                self.searchModel = ko.observable();
                self.searchModel = {
                    EffectiveStartDateVal: ko.observable(""),
                    EffectiveEnddateVal: ko.observable(""),
                    personNumberVal: ko.observable(""),
                    //old
                    name: ko.observable(""),
                    PersonNumber: ko.observable(""),
                    generalGroup: ko.observable(""),
                    nationalId: ko.observable(""),
                    effectiveDate: ko.observable(""),
                    selfServiceType: ko.observable(""),
                    managerId: ko.observable("")
                };

                self.tableSelectionListener = function (event) {
                    self.editDisabled(false);
                };

                self.handleAttached = function (info) {
                   app.loading(false);
                   
                   
                };
                self.handleDetached = function (info) {
                   
                };
                //---------------------------Translate Section---------------------------------------------

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });

                function initTranslations() {
                    self.delegationRequest(getTranslation("pages.delegationRequest"));
                    self.EffectiveStartDateLBl(getTranslation("common.EffectiveStartDate"));
                    self.EffectiveEnddateLBL(getTranslation("common.EffectiveEnddate"));
                    self.personNumberLbl(getTranslation("common.personNumberLbl"));
                    self.searchLbl(getTranslation("login.Search"));
                    
                    self.nationalId(getTranslation("special.nationalId"));
                    self.effictiveDate(getTranslation("special.effictiveDate"));
                    self.searchLbl(getTranslation("special.search"));
                    self.clearLbl(getTranslation("special.clear"));
                    self.personNumberLbl(getTranslation("special.personNumberTh"));
                    self.employeeNmaeLbl(getTranslation("special.employeeNmaeTh"));
                    self.NationalIdLbl(getTranslation("special.NationalIdTh"));
                    self.departmentLbl(getTranslation("special.departmentTh"));
                    self.positionLbl(getTranslation("special.positionTh"));
                    self.locationLbl(getTranslation("special.locationTh"));
                    self.actionLbl(getTranslation("special.actionTh"));
                    self.goTo(getTranslation("special.goTo"));
                    self.insertValue(getTranslation("special.goTo"));

                    self.columnArray([
                        {
                            "headerText": self.personNumberLbl(), "field": "personNumber"
                        },
                        {
                            "headerText": self.employeeNmaeLbl(), "field": "displayName"
                        },
                        {
                            "headerText": self.NationalIdLbl(), "field": "nationalId"
                        },
                        {
                            "headerText": self.departmentLbl(), "field": "department"
                        },
                        {
                            "headerText": self.positionLbl(), "field": "positionName"
                        },
                        {
                            "headerText": self.locationLbl(), "field": "employeeLocation"
                        }
                    ]);
                }
                initTranslations();
            }
            return JobContentViewModel;
        });
