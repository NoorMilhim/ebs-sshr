/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * reassignRequestOperation module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper',
    'ojs/ojknockout', 'ojs/ojarraydataprovider', 'ojs/ojdatetimepicker', 'ojs/ojtimezonedata',
    'ojs/ojselectcombobox', 'ojs/ojdialog', 'ojs/ojtrain', 'ojs/ojvalidationgroup'],
        function (oj, ko, $, app, services, commonhelper) {
    /**
     * The view model for the main content view template
     */
    function reassignRequestOperationContentViewModel() {
        var self = this;
        
        var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
                self.columnArray = ko.observableArray();
                self.tracker = ko.observable();
                self.groupValid = ko.observable();
                self.placeholder = ko.observable("Please Select Value");
                self.dataSourceTB2 = ko.observable();
                self.isVisibleRoleName = ko.observable(false);
                self.isVisibleSpecialCase = ko.observable(false);
                var deptArray = [];
                self.isDisabled = ko.observable(false);
                self.selctedStruct = ko.observable();
                self.isRequired = ko.observable(true);
                self.currentStepValue = ko.observable('stp1');
                self.addBtnVisible = ko.observable(false);
                self.nextBtnVisible = ko.observable(true);
                self.pageMode = ko.observable();
                self.supervisorHRCYesNoList = rootViewModel.globalHRCYesNo();
                self.oprationMessage = ko.observable();
                self.validationMessage = ko.observable();
                self.disableSubmit = ko.observable(false);
                self.xx = ko.observable();
                self.isRequiredRoleName = ko.observable(false);
                self.isRequiredSpecialCase = ko.observable(false);
                self.roleOptionType = ko.observableArray([]);
                self.rolesOption = ko.observableArray([]);
                self.isVisibleRoles = ko.observable(false);
                self.isRequiredRoles = ko.observable(false);
                self.roleIdMessageLbl = ko.observable();
                var countValid = "";
                var max_order = 0;
                self.lastNotification = ko.observable();
                self.reasonforReassignLbl = ko.observable();
                self.formatDate = function (date) {
                    //var date = new Date()
                    var month = '' + (date.getMonth() + 1),
                            day = '' + date.getDate(),
                            year = date.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;

                    return [year, month, day].join('-');
                };

                //  EIT Code
                self.EitNameArr = ko.observableArray([]);
                self.staticRequestArr=ko.observableArray();
                 var arr=[];
      



                self.approvalTypeArr = ko.observable([]);
                self.notificationTypeArr = ko.observable([]);
                var Url;
                var positionObj;
                self.stepArray = ko.observableArray([{label: "XXXX", id: 'stp1'},
                    {label: "YYYY", id: 'stp2'}]);//workNatureAllowance
                self.approvalSetupModel = {
                    eitCode: ko.observable(),
                    approvalOrder: ko.observable(),
                    approvalType: ko.observable(),
                    id: ko.observable(),
                    roleName: ko.observable(), //Edit View or Add 
                    specialCase: ko.observable(''), //Y or N
                    notificationType: ko.observable(),
                    createdBy: ko.observable("123"),
                    creationDate: ko.observable(self.formatDate(new Date())),
                    updatedBy: ko.observable(),
                    updatedDate: ko.observable()

                };

                self.validationModel = {
                    eid_code: ko.observable(),
                    role_name: ko.observable(),
                    approval_type: ko.observable(),
                    notification_type: ko.observable()

                };
                
                self.approvalListModel = {
                    id: ko.observable(),
                    rolrType: ko.observable(),
                    roleId: ko.observable(),
                    responseCode: ko.observable(),
                    serviceType: ko.observable(),
                    reasonforReassign: ko.observable()
                };
                

                self.backAction = function () {
                    if (oj.Router.rootInstance._navHistory.length > 1) {
                        app.loading(true);
                        oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                    }
                };
               

                self.handleAttached = function () {
                    app.loading(false);
                    initTranslations();
                    self.roleOptionType(app.rolesOption());
                    var arr = [{value: "XXX_HR_PROBATION_PERFORMANCE",
                            label: "Employee Probationary Period Evaluation"}];
                    var totalArr = [];
                    totalArr = arr.concat(app.globalEitNameReport());


                    self.EitNameArr(totalArr);

                    positionObj = oj.Router.rootInstance.retrieve();



                    if (positionObj.type == 'ADD') {

                        self.pageMode('ADD');
                        self.clothesAllowance(self.workNatureAllowanceScreenAddLbl());

                        self.oprationMessage(self.addMassageLbl());
                        self.validationMessage(self.validMessageLbl());
                        self.approvalSetupModel.createdBy(rootViewModel.personDetails().personId);
                    } else if (positionObj.type == 'EDIT') {
                        
                        self.pageMode('EDIT');
                        if (oj.Router.rootInstance.retrieve().approvalType == "POSITION")
                        {
                            self.isVisibleRoleName(true);
                            self.isRequiredRoleName(true);
                        }
                        if (oj.Router.rootInstance.retrieve().approvalType == "Special_Case")
                        {
                            self.isVisibleSpecialCase(true);
                            self.isRequiredSpecialCase(true);
                        }
                        if (oj.Router.rootInstance.retrieve().approvalType == "ROLES")
                        {
                            self.isVisibleRoles(true);
                            self.isRequiredRoles(true);
                        }

                        self.connected();
                        self.clothesAllowance(self.workNatureAllowanceScreenEditLbl());
                        self.approvalSetupModel.createdBy(rootViewModel.personDetails().personId);
                        self.oprationMessage(self.editMassageLbl());
                        self.approvalSetupModel.positionCode = positionObj.positionCode;
                        //self.approvalSetupModel.updatedBy(rootViewModel.personDetails().personId());
                        //self.approvalSetupModel.startDate = self.formatDate(new Date (positionObj.startDate));
                        // self.approvalSetupModel.percentage = (parseInt( positionObj.percentage));
                        self.approvalSetupModel.id=positionObj.id;
                        self.approvalSetupModel.code = positionObj.code;


                    }

                };

                //-------------This Function For Update Train Step To previousStep------------------
                function isEmpty(val) {
            return (val === undefined || val == null || val.length <= 0) ? true : false;
        }
                self.previousStep = function () {
                    var prev = document.getElementById("train").getPreviousSelectableStep();
                    if (prev != null)
                        self.currentStepValue(prev);
                };
                self.nextStep = function ()
                {

                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        return;
                    }
                    var next = document.getElementById("train").getNextSelectableStep();
                    if (next != null)
                    {
                            
                        if (isEmpty(self.approvalListModel.roleId()))
                        {
                            $.notify(self.roleIdMessageLbl(), "error");                             
                        } else
                       {
                            var flag = true;
                            if (flag)
                            {

                                self.currentStepValue(next);

                                self.currentStepValueText();
                            }
                        }
                    }

                };

                self.currentStepValueText = function () {
                    if (self.currentStepValue() == 'stp2') {
                        self.isDisabled(true);
                        self.addBtnVisible(true);
                        self.nextBtnVisible(false);


                    } else {
                        self.isDisabled(false);
                        self.addBtnVisible(false);
                        self.nextBtnVisible(true);

                    }

                    return self.clothesAllowance();
                };
                //------------------End Of Section -------------------------------------------------
                // ---------This Section For Change Handler --------------------
                self.approvalTypeChangedHandler = function (event, data) {
                    if (self.approvalSetupModel.approvalType() == "POSITION") {
                        self.isVisibleRoleName(true);
                        self.isRequiredRoleName(true);
                    } else {
                        self.isVisibleRoleName(false);
                        self.isRequiredRoleName(false);
                        self.approvalSetupModel.roleName("");
                    }
                    if (self.approvalSetupModel.approvalType() == "Special_Case") {
                        self.isVisibleSpecialCase(true);
                        self.isRequiredSpecialCase(true);
                    } else {
                        self.isVisibleSpecialCase(false);
                        self.isRequiredSpecialCase(false);
                        self.approvalSetupModel.specialCase("");
                    }
                    if (self.approvalSetupModel.approvalType() == "ROLES") {
                        self.isVisibleRoles(true);
                        self.isRequiredRoles(true);
                    } else {
                        self.isVisibleRoles(false);
                        self.isRequiredRoles(false);
                        self.approvalSetupModel.roleName('');
                    }
                    if(self.approvalSetupModel.approvalType() == "LINE_MANAGER" || self.approvalSetupModel.approvalType() == "LINE_MANAGER+1"){
                        self.searchPerson();
                    }
                    else{
                        
                    }
                    if(self.approvalSetupModel.approvalType() == "EMP"){
                        self.approvalListModel.roleId(rootViewModel.personDetails().personId);
                    }

                };
                
                self.roleNameChangedHandler = function (event, data) {
                    if(!isEmpty(event.detail.value)){
                    self.approvalListModel.roleId(event.detail.value);
                }
                    
                };

                self.eitCodeChangedHandler = function (event, data) {
                    self.getNotificationType();
                    self.MaxApprovalOrder();
                    self.approvalSetupModel.notificationType("");

                    self.approvalSetupModel.approvalOrder(self.xx());

                };
                // ---------End Of Change Handler -----------------
                //------------------This Section For Dialog Action (Open ,Close)
                self.submitButton = function () {
                    //self.globalSearch();
                    document.querySelector("#yesNoDialog").open();
                };
                self.cancelButton = function () {
                    document.querySelector("#yesNoDialog").close();
                };
                self.cancelButtonDialog = function () {
                    document.querySelector("#NoDialog").close();
                    document.querySelector("#yesNoDialog").close();
                    //location.reload();
                };
                //------------------End Of Section ----------------------------------------------
                //-------------This Function To Stop Train Action If Action Are Not Valid -------
                self.stopSelectListener = function (event, ui) {
                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        event.preventDefault();
                        return;
                    }
                };
                //-------------------End Of Function ---------------------------------------------
                //------------------ This Section For Call Web Service For Insert Or Update ---------
                function oprationServiceRecord() {
                    self.approvalListModel.id(self.approvalSetupModel.id);
                    self.approvalListModel.rolrType(self.approvalSetupModel.approvalType);
                    self.approvalListModel.serviceType(self.approvalSetupModel.eitCode);
                    
                    var jsonData = ko.toJSON(self.approvalListModel);
                    if (parseInt(countValid) > 0)
                    {
                        document.querySelector("#yesNoDialog").close();
                        var popup = document.querySelector('#NoDialog');
                        popup.open();

                        self.isDisabled(false);

                    } else
                    {
                        var getValidGradeCBF = function (data) {


                            var Data = data;
                            self.disableSubmit(false);
                            if (oj.Router.rootInstance._navHistory.length > 1) {
                                oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                            } else {
                                oj.Router.rootInstance.go('reassignRequestScreen');
                            }
                        };
                        services.addGeneric("reassignRequest/updateApprovalList", jsonData).then(getValidGradeCBF, self.failCbFn);
                    }
                }
                ;
                self.commitRecord = function (data, event) {
                    if (!self.disableSubmit()) {
                        self.disableSubmit(true);
                        oprationServiceRecord();
                        return true;
                    }
                };
                self.globalSearch = function () {
                    var eit = self.approvalSetupModel.eitCode();
                    var role = self.approvalSetupModel.roleName();
                    var approval = self.approvalSetupModel.approvalType();
                    var notifucation = self.approvalSetupModel.notificationType();
                    self.validationModel.eid_code(eit);
                    self.validationModel.role_name(role);
                    self.validationModel.approval_type(approval);
                    self.validationModel.notification_type(notifucation);
                    var jsonData = ko.toJSON(self.validationModel);
                    var getValidGradeCBF = function (data) {
                        countValid = data.count;
//                        self.validationModel.eitCode(self.approvalSetupModel.eitCode());
//                        self.validationModel.approvalType(self.approvalSetupModel.approvalType());
//                        self.validationModel.notificationType(self.approvalSetupModel.notificationType());

                    };

                    services.searcheGeneric(commonhelper.approvalSetupUrl + commonhelper.validationSetupUrl, self.validationModel).then(getValidGradeCBF);
                };
                self.searchModel = ko.observable();
                self.searchModel = {
                    name: ko.observable(""),
                    PersonNumber: ko.observable(""),
                    generalGroup: ko.observable(""),
                    nationalId: ko.observable(""),
                    effectiveDate: ko.observable(""),
                    selfServiceType: ko.observable(""),
                    managerId: ko.observable("")
                };
                
                self.searchPerson = function () {
                    var getPersonDetailsCbFn = function (data) {
                        
                        if(self.approvalSetupModel.approvalType() == "LINE_MANAGER"){
                        self.approvalListModel.roleId(data.managerId);
                    }
                    else if(self.approvalSetupModel.approvalType() == "LINE_MANAGER+1"){
                        self.approvalListModel.roleId(data.managerOfManager);
                    }
                        
                };
                services.searchEmployees(self.searchModel.name(), self.searchModel.nationalId(), self.searchModel.PersonNumber(), self.searchModel.managerId(), self.searchModel.effectiveDate()).then(getPersonDetailsCbFn, app.failCbFn);         

                };



                self.MaxApprovalOrder = function () {

                    var eit_COde = self.approvalSetupModel.eitCode();

                    var getMaxCBF = function (data) {
                        //  var tempObject = jQuery.parseJSON(data);
                        max_order = data.count + 1;
                        self.xx(max_order);
                        return data.count;


                    };
                    services.getGeneric(commonhelper.approvalSetupUrl + "maxorder/" + eit_COde).then(getMaxCBF, app.failCbFn);
                };


                self.getNotificationType = function () {
                    var eit_COde = self.approvalSetupModel.eitCode();
                    var getNotificationTypeCBF = function (data) {
                        self.lastNotification(data.notificationType);
                        var xxxx = data.notificationType;

                    };
                    services.getGeneric(commonhelper.approvalSetupUrl + "notificationtype/" + eit_COde).then(getNotificationTypeCBF, app.failCbFn);
                };

                self.notificationTypeChangedHandler = function (event, data) {
                    if (self.lastNotification() === "FYI" && positionObj.type === 'ADD')
                    {
                        if (self.approvalSetupModel.notificationType() === "FYA") {
                            $.notify(self.validNotificationTypeLbl(), "error");
                            self.approvalSetupModel.notificationType("");
                        }
                    } else
                    {
                        self.approvalSetupModel.notificationType();
                    }

                };
                
                                self.getRoles = function () {
  
                      var reportPaylod = {"reportName": "ROLES"};
                      var getRolesCBCF = function (data) {
                          for (i = 0; i < data.length; i++) {
                              self.rolesOption.push({
                                  value: data[i].ROLE_ID,
                                  label: data[i].ROLE_NAME
                              });
                          }
                      };
                      services.getGenericReport(reportPaylod).then(getRolesCBCF, app.failCbFn);
                  };

                //-------------------------Start Load Funcation------------------------------

                self.connected = function () {
                    self.approvalSetupModel.eitCode(oj.Router.rootInstance.retrieve().serviceType);
                    //self.approvalSetupModel.approvalOrder(parseInt(oj.Router.rootInstance.retrieve().approvalOrder));
                    self.approvalSetupModel.approvalType(oj.Router.rootInstance.retrieve().rolrType);
                    //self.approvalSetupModel.roleName(oj.Router.rootInstance.retrieve().roleName);
                    //self.approvalSetupModel.specialCase(oj.Router.rootInstance.retrieve().specialCase);
                    self.approvalSetupModel.notificationType(oj.Router.rootInstance.retrieve().notificationType);
                    self.approvalSetupModel.id(oj.Router.rootInstance.retrieve().id);
                    self.approvalListModel.responseCode(oj.Router.rootInstance.retrieve().transActionId);
                    self.searchModel.PersonNumber(oj.Router.rootInstance.retrieve().personNumber);

                };
                //-------------------------End Load Function---------------------------------

                //-------------------End Of Section ------------------------------------------------
                //---------------------Translate Section ----------------------------------------
                self.ok = ko.observable();
                self.startDateLbl = ko.observable();
                self.endDateLbl = ko.observable();
                self.addLbl = ko.observable();
                self.editLbl = ko.observable();
                self.gradeLbl = ko.observable();
                self.validGradeLbl = ko.observable();
                self.structureNameLbl = ko.observable();
                self.backLbl = ko.observable();
                self.nextLbl = ko.observable();
                self.submitLbl = ko.observable();
                self.placeholder = ko.observable();
                self.eitCodeLbl = ko.observable();


                self.cancel = ko.observable();
                self.create = ko.observable();
                self.review = ko.observable();
                self.workNatureAllowanceScreenAddLbl = ko.observable();
                self.workNatureAllowanceScreenEditLbl = ko.observable();
                self.addMassageLbl = ko.observable();
                self.validMessageLbl = ko.observable();
                self.editMassageLbl = ko.observable();
                self.confirmMessage = ko.observable();
                self.clothesAllowance = ko.observable();
                self.approvalOrderLbl = ko.observable();
                self.approvalTypeLbl = ko.observable();
                self.roleNameLbl = ko.observable();
                self.specialCaseLbl = ko.observable();
                self.notificationTypeLbl = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.validNotificationTypeLbl = ko.observable();
                self.rolesLbl = ko.observable();
                var getTranslation = oj.Translations.getTranslatedString;
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        self.stepArray([{label: self.create(), id: 'stp1'},
                            {label: self.review(), id: 'stp2'}]);
                        initTranslations();
                    }
                });
                function initTranslations() {
                    self.notificationTypeArr([]);
                    self.notificationTypeArr(app.getPaaSLookup('NOTIFICATION_TYPE'));
                    self.approvalTypeArr([]);
                    self.approvalTypeArr(app.getPaaSLookup('APPROVAL_TYPE'));
                    self.create(getTranslation("labels.create"));
                    self.review(getTranslation("others.review"));
                    self.structureNameLbl(getTranslation("additionalDetails.structureName"));
                    self.backLbl(getTranslation("others.back"));
                    self.nextLbl(getTranslation("others.next"));
                    self.submitLbl(getTranslation("others.submit"));
                    self.ok(getTranslation("others.ok"));
                    self.addLbl(getTranslation("others.add"));
                    self.editLbl(getTranslation("others.edit"));
                    self.validGradeLbl(getTranslation("others.validGrade"));
                    self.startDateLbl(getTranslation("job.startDate"));
                    self.endDateLbl(getTranslation("job.endDate"));
                    self.gradeLbl(getTranslation("job.grade"));
                    self.eitCodeLbl(getTranslation("approvalScreen.eitCode"));
                    self.cancel(getTranslation("others.cancel"));
                    self.workNatureAllowanceScreenAddLbl(getTranslation("approvalScreen.ApprovalAdd"));
                    self.workNatureAllowanceScreenEditLbl(getTranslation("approvalScreen.ApprovalEdit"));
                    self.addMassageLbl(getTranslation("approvalScreen.addMessage"));
                    self.validMessageLbl(getTranslation("workNatureAllowance.validMessage"));
                    self.editMassageLbl(getTranslation("approvalScreen.editMessage"));
                    self.confirmMessage(getTranslation("labels.confirmMessage"));
                    self.yes(getTranslation("others.yes"));
                    self.no(getTranslation("others.no"));
                    self.approvalOrderLbl(getTranslation("approvalScreen.aprovalOrder"));
                    self.approvalTypeLbl(getTranslation("approvalScreen.approvalType"));
                    self.roleNameLbl(getTranslation("approvalScreen.roleName"));
                    self.specialCaseLbl(getTranslation("approvalScreen.specialCase"));
                    self.notificationTypeLbl(getTranslation("approvalScreen.notificationType"));
                    self.placeholder(getTranslation("labels.placeHolder"));
                    self.validNotificationTypeLbl(getTranslation("workNatureAllowance.notificationMessage"));
                    self.rolesLbl(getTranslation("approvalScreen.roles"));
                    self.reasonforReassignLbl(getTranslation("approvalScreen.reasonForReassign"));
                    self.roleIdMessageLbl(getTranslation("approvalScreen.roleIdMessage"));
                    self.stepArray([{label: self.create(), id: 'stp1'},
                        {label: self.review(), id: 'stp2'}]);
                }
                initTranslations();
                self.getRoles();
    }
    
    return reassignRequestOperationContentViewModel;
});
