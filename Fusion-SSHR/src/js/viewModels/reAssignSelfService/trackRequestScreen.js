/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * reassignScreen module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'config/buildScreen', 'ojs/ojarraydataprovider', 'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
    'ojs/ojknockout-validation', 'ojs/ojlabel','ojs/ojtable', 'ojs/ojtable'],
        function (oj, ko, $, app, commonUtil, services, buildScreen, ArrayDataProvider) {
            /**
             * The view model for the main content view template
             */
            function reassignScreenContentViewModel() {
                var self = this;

                // Start Declare All Observable
                var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
                var getTranslation = oj.Translations.getTranslatedString;
                self.messages = ko.observable();
                self.trackRequestLbl = ko.observable();
                self.oprationdisabled = ko.observable(true);
                self.deleteLbl = ko.observable();
                self.unknownErrMsg = ko.observable();
                self.columnArray = ko.observableArray([]);
                self.selectedRowKey = ko.observable();
                self.selectedIndex = ko.observable();
                self.statusLbl = ko.observable();
                self.summaryObservableArray = ko.observableArray([]);
                self.eitNameLbl = ko.observable();
                self.personNameLbl = ko.observable();
                self.personNumberLbl = ko.observable();
                self.roleNameLbl = ko.observable();
                self.confirmMessage = ko.observable();
                self.deleteMessageLbl = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.errorMessage=ko.observable();
                self.creationDate = ko.observable();
                self.statusDataLbl = ko.observable();
                self.getAllLbl = ko.observable();
                self.personNumberLbl = ko.observable();
                self.search= ko.observable();
                self.clearLBL= ko.observable();
                self.Approval_list = ko.observable();
                self.positionName = ko.observable();
                self.ok = ko.observable();
                self.dataSource = new ko.observable(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.summaryObservableArray, {idAttribute: 'id'})));
                // End Declare All Observable

                // Start Function Handle attached And handle Detached Run On open Screen and Close Screen
                self.handleAttached = function (info) {
                    app.loading(false);
                    self.roleOptionType(app.rolesOption());
                 //   console.log(app.personDetails());
                };
                self.roleOptionType=ko.observableArray();

                
                self.handleDetached = function (info) {

                };
                self.getAllSelfService = function () {
                      self.getAllSelfServiceRequest();
                };
                 self.searchModel = {

                    person_number: ko.observable()
                };
                self.BtnSearch = function () {
                    var jsonData = ko.toJSON(self.searchModel);
                    self.summaryObservableArray([]);
                    var getAllSelfServiceCBF = function (data) {
                        self.summaryObservableArray([]);

                        for (var i = 0; i < data.length; i++) {
                            if (data[i].status === "PENDING_APPROVED") {
                                self.statusDataLbl("Pending Approved");
                            }else if(data[i].status === "APPROVED"){
                                self.statusDataLbl(" Approved");
                            }else if(data[i].status === "DRAFT"){
                                self.statusDataLbl("Draft");
                            }
                            var obj = {};
                            obj.id = data[i].id;
                            obj.created_by = data[i].creation_date;
                            obj.person_id = data[i].person_id;
                            obj.status = data[i].status;
                            obj.reference_num = data[i].reference_num;
                            obj.eit_name = data[i].eit_name;
                            obj.person_number = data[i].person_number;
                            for (var j=0;j<app.globalEitNameReport().length;j++){                                
                              if(app.globalEitNameReport()[j].value==data[i].code){  
                              obj.code  = app.globalEitNameReport()[j].label;
                               }
                             }
                            obj.line_manager = data[i].line_manager;
                            obj.personName = data[i].personName;
                            obj.statusDataLbl = self.statusDataLbl();
                            self.summaryObservableArray.push(obj);
                        }
                    };
                    var failCbFn = function () {
                        self.messages([{
                                severity: 'error',
                                summary: self.unknownErrMsg(),
                                autoTimeout: 5000
                            }]);
                    };
                    services.addGeneric("reassignRequest/getPendingAndApprovedByPersonNumber", jsonData).then(getAllSelfServiceCBF, failCbFn)
                };
                self.BtnReset = function () {
                     self.searchModel.person_number("");
                     self.summaryObservableArray([]);
                };
                // End Function Handle attached And handle Detached Run On open Screen and Close Screen

                // Start Funcation Calling for Select row from Table
                self.tableSelectionListener = function (event) {
                    var data = event.detail;
                    var currentRow = data.currentRow;
                    if (currentRow != null) {
                        self.selectedRowKey(currentRow['rowKey']);
                        self.selectedIndex(currentRow['rowIndex']);
                        self.oprationdisabled(false);
                    }
                    else
                    {
                        self.oprationdisabled(true);
                    }
                };
                // End Funcation Calling for Select row from Table

                // Start Function Get All Eit Self Service
                self.getAllSelfServiceRequest = function () {
                   
                    self.summaryObservableArray([]);
                    var getAllSelfServiceCBF = function (data) {
                        self.summaryObservableArray([]);

                        for (var i = 0; i < data.length; i++) {
                            console.log(data[i].status)
                            if (data[i].status === "PENDING_APPROVED") {
                                self.statusDataLbl("Pending Approved");
                            }else if(data[i].status === "APPROVED"){
                                self.statusDataLbl("Approved");
                             }else if(data[i].status == "DRAFT"){
                                self.statusDataLbl("Draft");
                            }
                            var obj = {};
                            obj.id = data[i].id;
                            obj.created_by = data[i].creation_date;
                            obj.person_id = data[i].person_id;
                            obj.status = data[i].status;
                            obj.reference_num = data[i].reference_num;
                            obj.eit_name = data[i].eit_name;
                            obj.person_number = data[i].person_number;
                             for (var j=0;j<app.globalEitNameReport().length;j++){                                
                              if(app.globalEitNameReport()[j].value==data[i].code){  
                              obj.code  = app.globalEitNameReport()[j].label;
                               }
                             }
                            obj.line_manager = data[i].line_manager;
                            obj.personName = data[i].personName;
                            obj.statusDataLbl = self.statusDataLbl();
                            self.summaryObservableArray.push(obj);
                        }
                    };
                    var failCbFn = function () {
                        self.messages([{
                                severity: 'error',
                                summary: self.unknownErrMsg(),
                                autoTimeout: 5000
                            }]);
                    };
                    services.getGeneric("reassignRequest/trackRequest", {}).then(getAllSelfServiceCBF, failCbFn);
                };
                // End Function Get All Eit Self Service

                // Start Function Delete Eit Self Service By ID
                self.deleteSelfServiceRequest = function () {
                    var id = self.summaryObservableArray()[self.selectedIndex()].id;
                        var deleteSelfServiceCBF = function (data) {
                            console.log(data);
                            self.getAllSelfServiceRequest();
                            document.querySelector("#NoDialog").close();

                        };
                        var failCbFn = function () {
                            self.messages([{
                                    severity: 'error',
                                    summary: self.unknownErrMsg(),
                                    autoTimeout: 5000
                                }]);
                        };
                        services.getGeneric("reassignRequest/delete/" + id, {}).then(deleteSelfServiceCBF, failCbFn);
                    
                };
                // End Function Delete Eit Self Service By ID

                // Start Button Open And Close Popup
                self.btnDeleteSelfServiceRequest = function () {
                   
                    var checkStatus = self.summaryObservableArray()[self.selectedIndex()].status;
                    if (checkStatus === "APPROVED") {
                        $.notify(self.errorMessage(), "error");
                        document.querySelector("#NoDialog").close();
                    } else if (checkStatus === "PENDING_APPROVED") {
                        document.querySelector("#NoDialog").open();
                    }  else if (checkStatus === "DRAFT") {
                        document.querySelector("#NoDialog").open();
                    }

                };

                self.cancelButtonDialog = function () {
                    document.querySelector("#NoDialog").close();
                };
                // End Button Open And Close Popup

                // Start Button Submit Delete Eit Self Service
                self.SubmitButtonDialog = function () {
                    self.deleteSelfServiceRequest();
                };
                // End Button Submit Delete Eit Self Service

                   //Start function
                   
        self.rolrType = ko.observableArray([]);
        self.notificationType = ko.observableArray([]);
        self.responseCode = ko.observableArray([]);
        self.responseDate = ko.observableArray([]);
        self.approvalResponseArr = ko.observableArray([]);
        self.datacrad = ko.observableArray([]);
        self.workglow_Approval = async function () {
             
            self.oprationdisabled(true);
            $(".approval-btn").addClass("loading");
            self.datacrad([]);
            var eitcode = self.EitCode;

            var getValidGradeCBF = function (data) {
                if (data.length != 0) {
                    var aprrovalStatus = "PENDING";
                   
                    for (var i = 0; i < data.length; i++) {
                        var notificationStatus, bodyCardStatus, cardStatus;
                        if (data[i].notificationType === "FYA") {
                            notificationStatus = 'app-type-a';
                        } else {
                            cardStatus = 'badge-secondary';
                            notificationStatus = 'app-type-i';
                            bodyCardStatus = 'app-crd-bdy-border-ntre';
                        }
                        if (!data[i].responseCode) {
                            cardStatus = 'badge-warning';
                            bodyCardStatus = 'app-crd-bdy-border-pen';
                            data[i].responseDate = '';
                        } else if (data[i].responseCode === 'APPROVED') {
                            cardStatus = 'badge-success';
                            bodyCardStatus = 'app-crd-bdy-border-suc';
                        } else {

                        }
                        if (data[i].rolrType === "EMP") {
//                            data[i].rolrType= app.personDetails().displayName
                            if (!data[i].responseCode) {
                                self.datacrad.push(
                                        {personName: data[i].personName,
                                            FYA: data[i].notificationType,
                                            Approved: aprrovalStatus,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus
                                        },
                                        );
                            } else {
                                self.datacrad.push(
                                        {personName: data[i].personName,
                                            FYA: data[i].notificationType,
                                            Approved: data[i].responseCode,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus
                                        },
                                        );
                            }

                        } else if (data[i].rolrType == "LINE_MANAGER") {
//                            data[i].rolrType= app.personDetails().managerName;
                            if (!data[i].responseCode) {
                                self.datacrad.push(
                                        {personName: data[i].personName,
                                            FYA: data[i].notificationType,
                                            Approved: aprrovalStatus,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus
                                        }, );
                            } else {
                                self.datacrad.push(
                                        {personName: data[i].personName,
                                            FYA: data[i].notificationType,
                                            Approved: data[i].responseCode,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus
                                        }, );

                            }

                        } else if (data[i].rolrType == "LINE_MANAGER+1") {
//                               data[i].rolrType= app.personDetails().managerOfMnagerName;
                            if (!data[i].responseCode) {
                                self.datacrad.push(
                                        {personName: data[i].personName,
                                            FYA: data[i].notificationType,
                                            Approved: aprrovalStatus,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus
                                        }, );
                            } else {
                                self.datacrad.push(
                                        {personName: data[i].personName,
                                            FYA: data[i].notificationType,
                                            Approved: data[i].responseCode,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus
                                        }, );


                            }

                        } else if (data[i].rolrType == "POSITION") {
                        
//                               data[i].rolrType= app.personDetails().managerOfMnagerName;
                            for (var j = 0; j < self.roleOptionType().length; j++) {
                                if (self.roleOptionType()[j].value == data[i].roleId)
                                {                                 
                                    self.positionName(self.roleOptionType()[j].label);
                                }
                            }
                            if (!data[i].responseCode) {
                                self.datacrad.push(
                                        {personName: self.positionName(),
                                            FYA: data[i].notificationType,
                                            Approved: aprrovalStatus,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus
                                        }, );
                            } else {
                                self.datacrad.push(
                                        {personName: self.positionName(),
                                            FYA: data[i].notificationType,
                                            Approved: data[i].responseCode,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus
                                        }, );


                            }

                        }else if (data[i].rolrType == "ROLES") {
//                               data[i].rolrType= app.personDetails().managerOfMnagerName;

                            for (var j = 0; j < rootViewModel.allRoles().length; j++) {
                                if (rootViewModel.allRoles()[j].ROLE_ID == data[i].roleId)
                                {
                                
                                    self.positionName(rootViewModel.allRoles()[j].ROLE_NAME);
                                }
                            }
                            if (!data[i].responseCode) {
                                self.datacrad.push(
                                        {personName: self.positionName(),
                                            FYA: data[i].notificationType,
                                            Approved: aprrovalStatus,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus
                                        }, );
                            } else {
                                self.datacrad.push(
                                        {personName: self.positionName(),
                                            FYA: data[i].notificationType,
                                            Approved: data[i].responseCode,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus
                                        }, );


                            }

                        }else if (data[i].rolrType == "ROLES") {
//                               data[i].rolrType= app.personDetails().managerOfMnagerName;

                            for (var j = 0; j < rootViewModel.allRoles().length; j++) {
                                if (rootViewModel.allRoles()[j].value == data[i].roleId)
                                {
                                    self.positionName(rootViewModel.allRoles()[j].label);
                                }
                            }
                            if (data[i].responseCode === null) {
                                self.datacrad.push(
                                        {personName: self.positionName(),
                                            FYA: data[i].notificationType,
                                            Approved: aprrovalStatus,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus
                                        }, );
                            } else {
                                self.datacrad.push(
                                        {personName: self.positionName(),
                                            FYA: data[i].notificationType,
                                            Approved: data[i].responseCode,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus
                                        }, );


                            }

                        }


//                       self.notificationType.push(data[i].notificationType);
//                       self.responseCode.push(data[i].responseCode);
//                       self.responseDate.push(data[i].responseDate);  

                        //  data[i].approvalType = searchArray(data[i].approvalType, self.approvalType());
                        //  data[i].roleName = searchArray(data[i].roleName, self.roleName());

                    }
                }
                else
                {
                         self.datacrad.push(
                                        {personName: 'There is no approval list to this request',
                                            FYA: '',
                                            Approved: '',
                                            date: '',
                                            status: '',
                                            bodyStatus: '',
                                            appCardStatus: ''
                                        }, );
                }
//                    var Data = data;
//          
//                self.dataSourceTB2(new oj.ArrayTableDataSource(Data));

            };
            // ... leave firstName and lastName unchanged ...

            //self.lbl = ko.observable(self.approvaltype());
               var element = document.getElementById('table');
               var currentRow = element.currentRow;
              
               var type_id=self.summaryObservableArray()[currentRow['rowIndex']].id;
               var eitcode;
                 for (var j=0;j<app.globalEitNameReport().length;j++){                                
                              if(app.globalEitNameReport()[j].label==self.summaryObservableArray()[currentRow['rowIndex']].code){  
                                   eitcode=app.globalEitNameReport()[j].value;
                               }
                  }    
            
            
            await services.getGenericAsync("approvalList/approvaltype/" + eitcode + "/" + type_id).then(getValidGradeCBF, app.failCbFn);
            self.oprationdisabled(false);
            $(".approval-btn").removeClass("loading");
            document.querySelector("#modalDialog1").open();
        };

        self.roleOptionType = ko.observableArray([]);
                   //End Function
          self.closeDialog = function () {
            $("#modalDialog1").ojDialog("close");
        };


                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }

                });
                

                function initTranslations() {
                    //New
                    self.trackRequestLbl(getTranslation("pages.trackRequest"));

                    self.deleteLbl(getTranslation("others.delete"));
                    self.unknownErrMsg(getTranslation("labels.ValidationMessage"));
                    self.statusLbl(getTranslation("labels.status"));
                    self.eitNameLbl(getTranslation("approvalScreen.eitCode"));
                    self.personNameLbl(getTranslation("common.personNameLbl"));
                    self.personNumberLbl(getTranslation("common.person_number1Lbl"));
                    self.roleNameLbl(getTranslation("approvalScreen.roleName"));
                    self.confirmMessage(getTranslation("labels.confirmMessage"));
                    self.deleteMessageLbl(getTranslation("labels.deleteSelfServiceMessage"));
                    self.no(getTranslation("others.no"));
                    self.yes(getTranslation("others.yes"));
                    self.errorMessage(getTranslation("others.errorMessage"));
                    self.creationDate(getTranslation("notification.creationDate"));
                    self.getAllLbl(getTranslation("others.getAll"));
                    self.personNumberLbl(getTranslation("report.personNumber"));
                    self.search(getTranslation("lookUp.Search"));
                    self.clearLBL(getTranslation("special.clear"));
                    self.Approval_list(getTranslation("labels.Approval_list"));
                    self.ok(getTranslation("others.ok"));
                    self.columnArray([
                        {
                            "headerText": self.eitNameLbl(), "field": "code"
                        },
                        {
                            "headerText": self.personNameLbl(), "field": "personName"
                        },
                        {
                            "headerText": self.personNumberLbl(), "field": "person_number"
                        },
//                        {
//                            "headerText": self.roleNameLbl(), "field": "line_manager"
//                        },
                        {
                            "headerText": self.statusLbl(), "field": "statusDataLbl"
                        },
                        {
                            "headerText": self.creationDate(), "field": "created_by"
                        }
                    ]);

                }
                initTranslations();
            }

            return reassignScreenContentViewModel;
        });
