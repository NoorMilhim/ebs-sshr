/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * reassignRequestScreen module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper', 'config/services',
    'ojs/ojtable',
    'config/buildScreen', 'ojs/ojarraydataprovider', 'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
    'ojs/ojknockout-validation', 'ojs/ojlabel', 'ojs/ojtable'],
        function (oj, ko, $, app, commonUtil, services, buildScreen, ArrayDataProvider) {
            /**
             * The view model for the main content view template
             */
            function reassignRequestScreenContentViewModel() {
                var self = this;

                // Start Declare All Observable
                var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
                var getTranslation = oj.Translations.getTranslatedString;
                self.messages = ko.observable();
                self.reassignRequestLbl = ko.observable();
                self.oprationdisabled = ko.observable(true);
                self.oprationdisabledApprovalList = ko.observable(true);
                self.reassignLbl = ko.observable();
                self.unknownErrMsg = ko.observable();
                self.columnArray = ko.observableArray([]);
                self.columnArrayApprovalList = ko.observableArray([]);
                self.selectedRowKey = ko.observable();
                self.selectedIndex = ko.observable();
                self.statusLbl = ko.observable();
                self.summaryObservableArray = ko.observableArray([]);
                self.approvalListArray = ko.observableArray([]);
                self.eitNameLbl = ko.observable();
                self.personNameLbl = ko.observable();
                self.personNumberLbl = ko.observable();
                self.roleNameLbl = ko.observable();
                self.confirmMessage = ko.observable();
                self.deleteMessageLbl = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.statusDataLbl = ko.observable();
                self.searchLbl = ko.observable();
                self.clearLbl = ko.observable();
                self.notificationTypeLbl = ko.observable();
                var positionObj;
                self.personNumberSelected = ko.observable();
                self.creationDate = ko.observable();
                self.dataSource = new ko.observable(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.summaryObservableArray, {idAttribute: 'id'})));
                self.dataSourceApprovalList = new ko.observable(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.approvalListArray, {idAttribute: 'id'})));
                // End Declare All Observable

                // Start Declare Model               
                self.searchModel = {

                    person_number: ko.observable()
                };
                //End Declare Model

                // Start Function Handle attached And handle Detached Run On open Screen and Close Screen
                self.handleAttached = function (info) {
                    app.loading(false);
                };
                self.handleDetached = function (info) {

                };
                // End Function Handle attached And handle Detached Run On open Screen and Close Screen

                // Start Funcation Calling for Select row from Table
                self.tableSelectionListener = function (event) {
                    var data = event.detail;
                    var currentRow = data.currentRow;
                    if (currentRow != null) {
                        self.selectedRowKey(currentRow['rowKey']);
                        self.selectedIndex(currentRow['rowIndex']);
                        self.personNumberSelected(self.summaryObservableArray()[self.selectedIndex()].person_number);
                        self.oprationdisabled(false);
                    }
                };
                // End Funcation Calling for Select row from Table

                // Start Funcation Calling for Select row from Table Approval List
                self.tableApprovalListSelectionListener = function (event) {
                    var data = event.detail;
                    var currentRow = data.currentRow;
                    if (currentRow != null) {
                        self.selectedRowKey(currentRow['rowKey']);
                        self.selectedIndex(currentRow['rowIndex']);
                        self.personNumberSelected(self.summaryObservableArray()[self.selectedIndex()].person_number);
                        self.oprationdisabledApprovalList(false);
                    }
                };
                // End Funcation Calling for Select row from Table Approval List

                // Start Function Get All Eit Self Service
                self.getSelfServiceRequestByPersonNumber = function () {
                    var jsonData = ko.toJSON(self.searchModel);
                    self.summaryObservableArray([]);
                    var getSelfServiceRequestByPersonNumberCBF = function (data) {
                        self.summaryObservableArray([]);

                        for (var i = 0; i < data.length; i++) {
                            if (data[i].status === "PENDING_APPROVED") {
                                self.statusDataLbl("Pending Approved");
                            }
                            var obj = {};
                            obj.id = data[i].id;
                            obj.created_by = data[i].creation_date;
                            obj.person_id = data[i].person_id;
                            obj.status = data[i].status;
                            obj.reference_num = data[i].reference_num;
                            obj.eit_name = data[i].eit_name;
                            obj.person_number = data[i].person_number;
                            obj.codeService = data[i].code;
                             for (var j=0;j<app.globalEitNameReport().length;j++){                                
                              if(app.globalEitNameReport()[j].value==data[i].code){  
                              obj.code  = app.globalEitNameReport()[j].label;
                               }
                             }
                            
                            obj.line_manager = data[i].line_manager;
                            obj.personName = data[i].personName;
                            obj.statusDataLbl = self.statusDataLbl();
                            self.summaryObservableArray.push(obj);
                        }
                        
                    };
                    var failCbFn = function () {
                        self.messages([{
                                severity: 'error',
                                summary: self.unknownErrMsg(),
                                autoTimeout: 5000
                            }]);
                    };
                    services.addGeneric("reassignRequest/getEit", jsonData).then(getSelfServiceRequestByPersonNumberCBF, failCbFn);
                };
                // End Function Get All Eit Self Service

                self.searchPerson = function () {
                    self.getSelfServiceRequestByPersonNumber();

                };

                self.reset = function () {
                    clearContent();
                    self.summaryObservableArray([]);
                };

                function clearContent() {

                    self.searchModel.person_number("");
                    self.oprationdisabled(true);

                }

                // Start Button Open And Close Popup
                self.btnReAssignSelfServiceRequest = function () {
                    //document.querySelector("#NoDialog").open();
                    self.workflow_Approval();
                };

                self.cancelButtonDialog = function () {
                    document.querySelector("#NoDialog").close();
                };
                // End Button Open And Close Popup

                // Start Button Submit Delete Eit Self Service
                self.SubmitButtonDialog = function () {
                    self.approvalListArray()[self.selectedIndex()].type = "EDIT";
                    self.approvalListArray()[self.selectedIndex()].personNumber = self.personNumberSelected();
                    oj.Router.rootInstance.store(self.approvalListArray()[self.selectedIndex()]);
                    oj.Router.rootInstance.go('reassignRequestOperation');
                };
                // End Button Submit Delete Eit Self Service

                self.workflow_Approval = function () {
                    var eitcode = self.summaryObservableArray()[self.selectedIndex()].codeService;
                    var id = self.summaryObservableArray()[self.selectedIndex()].id;

                    var getValidGradeCBF = function (data) {
                        self.approvalListArray([]);

                        for (var i = 0; i < data.length; i++) {
                            if (data[i].status === "PENDING_APPROVED") {
                                self.statusDataLbl("Pending Approved");
                            }
                            var obj = {};
                            obj.id = data[i].id;
                            obj.notificationType = data[i].notificationType;
                            obj.responseDate = data[i].responseDate;
                            obj.roleId = data[i].roleId;
                            obj.rolrType = data[i].rolrType;
//                            obj.serviceType = data[i].serviceType;
                               for (var j=0;j<app.globalEitNameReport().length;j++){                                
                              if(app.globalEitNameReport()[j].value==data[i].serviceType){  
                              obj.serviceType  = app.globalEitNameReport()[j].label;
                               }
                             }
                            obj.stepLeval = data[i].stepLeval;
                            obj.transActionId = data[i].transActionId;
                            self.approvalListArray.push(obj);
                        }
                    };

                    services.getGeneric("reassignRequest/approvaltype/" + eitcode + "/" + id).then(getValidGradeCBF, app.failCbFn);
                    document.querySelector("#NoDialog").open();
                };

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }

                });

                function initTranslations() {
                    //New
                    self.reassignRequestLbl(getTranslation("pages.reassignRequest"));

                    self.reassignLbl(getTranslation("pages.reassignRequest"));
                    self.unknownErrMsg(getTranslation("labels.ValidationMessage"));
                    self.statusLbl(getTranslation("labels.status"));
                    self.eitNameLbl(getTranslation("approvalScreen.eitCode"));
                    self.personNameLbl(getTranslation("common.personNameLbl"));
                    self.personNumberLbl(getTranslation("common.person_number1Lbl"));
                    self.roleNameLbl(getTranslation("approvalScreen.roleName"));
                    self.confirmMessage(getTranslation("labels.confirmMessage"));
                    self.deleteMessageLbl(getTranslation("labels.deleteSelfServiceMessage"));
                    self.no(getTranslation("others.no"));
                    self.yes(getTranslation("others.yes"));
                    self.searchLbl(getTranslation("special.search"));
                    self.clearLbl(getTranslation("special.clear"));
                    self.notificationTypeLbl(getTranslation("approvalScreen.notificationType"));
                    self.creationDate(getTranslation("notification.creationDate"));
                    self.columnArray([
                        {
                            "headerText": self.eitNameLbl(), "field": "code"
                        },
                        {
                            "headerText": self.personNameLbl(), "field": "personName"
                        },
                        {
                            "headerText": self.personNumberLbl(), "field": "person_number"
                        },
//                        {
//                            "headerText": self.roleNameLbl(), "field": "line_manager"
//                        },
                        {
                            "headerText": self.statusLbl(), "field": "statusDataLbl"
                        },
                        {
                            "headerText": self.creationDate(), "field": "created_by"
                        }
                    ]);

                    self.columnArrayApprovalList([
                        {
                            "headerText": self.eitNameLbl(), "field": "serviceType"
                        },
                        {
                            "headerText": self.roleNameLbl(), "field": "rolrType"
                        },
                        {
                            "headerText": self.personNumberLbl(), "field": "roleId"
                        },
                        {
                            "headerText": self.notificationTypeLbl(), "field": "notificationType"
                        }
                    ]);

                }
                initTranslations();
            }

            return reassignRequestScreenContentViewModel;
        });
