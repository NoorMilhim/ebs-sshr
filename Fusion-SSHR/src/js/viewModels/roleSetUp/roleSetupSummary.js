/**
 * 
 * @param {type} oj
 * @param {type} ko
 * @param {type} $
 * @param {type} app
 * @param {type} commonUtil
 * @param {type} services
 * @param {type} translatehelper
 * @returns {JobL#13.JobContentViewModel}
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojknockout', 'ojs/ojtrain', 'ojs/ojbutton', 'ojs/ojarraydataprovider', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker','ojs/ojtable',
    'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup', 'ojs/ojcheckboxset', 'ojs/ojradioset',
    'ojs/ojlabel', 'ojs/ojgauge', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojmessage', 'jquery-calendar',
    'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin',
    'jquery-calendar-picker', 'ojs/ojvalidationgroup', 'ojs/ojarraydataprovider'],
        function (oj, ko, $, app, commonUtil, services) {
            /**
             * The view model for the main content view template
             */
            function JobContentViewModel() {
                var self = this;
                self.columnArray = ko.observableArray();
                self.addSummaryLbl = ko.observable();
                self.editSummaryLbl = ko.observable();
                self.viewSummaryLbl = ko.observable();
                self.reportNameLbl = ko.observable();
                self.parameter1Lbl = ko.observable();
                self.parameter2Lbl = ko.observable();
                self.parameter3Lbl = ko.observable();
                self.parameter4Lbl = ko.observable();
                self.parameter5Lbl = ko.observable();
                self.reportTypelbl = ko.observable();
                self.backActionLbl = ko.observable();
                self.DeleteSummaryLbl = ko.observable();
                self.confirmMessage = ko.observable();
                self.oprationMessage = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                var deptArray = [];
                self.summaryObservableArray = ko.observableArray(deptArray);
                self.dataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.summaryObservableArray, {idAttribute: 'roleId'}));
                self.isDisable = ko.observable(true);
                self.dataSource;
                self.checkResultSearch = ko.observable();
                      var arr=[];
                       var str;
                     
                self.getAllEITCode=function(roleId){
                    
                           
                    var ROLeEitCodeCbFn=function(data){
                        for(var i=0;i<data.length;i++){                          
                       
                         
                            arr.push(data[i].eitCodes);
                        }
                         str=JSON.stringify(arr);
                        self.isDisable(false);
                    };
                       
                    
                    var ObjRoleName={"roleid":roleId};
                     services.addGeneric(commonUtil.getALLALLowanceRoleEmployee , JSON.stringify(ObjRoleName)).then(ROLeEitCodeCbFn, app.failCbFn);
                    
                    
                };
                 self.selectedRowKey = ko.observable();
                self.selectedIndex = ko.observable();
                self.tableSelectionListener = function (event) {

                    var data = event.detail;
                    var currentRow = data.currentRow;
                    if (currentRow) {
                        var roleId = self.summaryObservableArray()[currentRow['rowIndex']].roleId;
                        self.getAllEITCode(roleId);
                    }
                };
                self.btnAddSummary = function () {
                    app.loading(true);
                    oj.Router.rootInstance.store('ADD');
                    oj.Router.rootInstance.go('roleSetupOperation');
                };
                self.btnEditSummary = function () {
                    app.loading(true);
                    var element = document.getElementById('table');
                    var currentRow = element.currentRow;
                    self.summaryObservableArray()[currentRow['rowIndex']].type = "update";
                    self.summaryObservableArray()[currentRow['rowIndex']].eitCode=arr;
                    oj.Router.rootInstance.store(self.summaryObservableArray()[currentRow['rowIndex']]);
                    oj.Router.rootInstance.go('roleSetupOperation');
                };
                self.btnViewSummary = function () {
                    app.loading(true);
                    var element = document.getElementById('table');
                    var currentRow = element.currentRow;
                    self.summaryObservableArray()[currentRow['rowIndex']].type = "view";
                    var roleId= self.summaryObservableArray()[currentRow['rowIndex']].roleId ;
                    
                    self.summaryObservableArray()[currentRow['rowIndex']].eitCode=arr;
                    
                    oj.Router.rootInstance.store(self.summaryObservableArray()[currentRow['rowIndex']]);
                    oj.Router.rootInstance.go('roleSetupOperation');
                };
                 self.btnDeleteSummary=function(){
                  document.querySelector("#yesNoDialog").open();
                };
                 self.commitRecordDelete=function(){
                     var element = document.getElementById('table');
                    var currentRow = element.currentRow;
                   var id = self.summaryObservableArray()[currentRow['rowIndex']].roleId;
                   var deleteReportCBF=function(data){
                       self.getSummary();
                       document.querySelector("#yesNoDialog").close();
                   };
                   
                  services.getGeneric("RoleSetup/deleteRoleSetUp/" + id, {}).then(deleteReportCBF, app.failCbFn);
                };
                 self.cancelButtonDelete=function(){
                  document.querySelector("#yesNoDialog").close();
                };
               

                self.getSummary = function () {
                  self.summaryObservableArray([]);
                    var getReportCbFn = function (data) {
                        if (data.length !== 0)
                        {

                            $.each(data, function (index) {

                                self.summaryObservableArray.push({
                                   
                                    roleDescription: data[index].roleDescription,
                                    roleName: data[index].roleName,
                                    roleId: data[index].roleId

                                });
                            });
                        } else {
                            $.notify(self.checkResultSearch(), "error");
                        }



                    };
                    var failCbFn = function () {
                        $.notify(self.checkResultSearch(), "error");
                    };
                    var serviceName = commonUtil.getAllRoleSetup;
                    services.getAllSummary(serviceName).then(getReportCbFn, failCbFn);
                };


                self.reportName = ko.observable();
                self.search = ko.observable();
                self.clear = ko.observable();


                self.reportSearch = {
                    reportname: ko.observable()

                };
                self.roleSearch = {
                    roleName: ko.observable()
                };
                var el = document.getElementById("searchName");


                self.searchPerson = function () {

                    if (!self.roleSearch.roleName()) {

                        self.getSummary();
                    } else {
                        self.summaryObservableArray([]);
                        self.searchRoleName();

                    }
                };
                self.backAction = function () {
                    oj.Router.rootInstance.go('setup');
                };
                

                self.handleAttached = function (info) {
                    app.loading(false);
                    self.getSummary();
                
                };
                
                self.reset = function () {
                    clearContent();
                };
                function clearContent() {
                    self.roleSearch.roleName("");
                    self.summaryObservableArray([]);
                }
                
                self.searchRoleName = function () {
                    var jsonData = ko.toJSON(self.roleSearch);
                    var roleNameCodeCbFn = function (data) {

                        if (data.length !== 0)
                        {

                             $.each(data, function (index) {

                                self.summaryObservableArray.push({
                                   
                                    roleDescription: data[index].roleDescription,
                                    roleName: data[index].roleName,
                                    roleId: data[index].roleId

                                });
                            });
                        } else {
                            $.notify(self.checkResultSearch(), "error");
                        }
                    };
                   
                    var failCbFn = function () {
                    };
                    services.addGeneric("RoleSetup/search", jsonData).then(roleNameCodeCbFn, failCbFn);
                };



                 self.roleNamelbl=ko.observable();
                 self.roleDescriptionlbl=ko.observable();
                var getTranslation = oj.Translations.getTranslatedString;

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();

                    }

                });
                function initTranslations() {
                    self.search(getTranslation("others.search"));
                    self.clear(getTranslation("others.clear"));
                    self.addSummaryLbl(getTranslation("report.addSummary"));
                    self.editSummaryLbl(getTranslation("report.editSummary"));
                    self.viewSummaryLbl(getTranslation("report.viewSummary"));
                    self.reportNameLbl(getTranslation("report.reportName"));
                    self.parameter1Lbl(getTranslation("report.Parameter1"));
                    self.parameter2Lbl(getTranslation("report.Parameter2"));
                    self.parameter3Lbl(getTranslation("report.Parameter3"));
                    self.parameter4Lbl(getTranslation("report.Parameter4"));
                    self.parameter5Lbl(getTranslation("report.Parameter5"));
                    self.reportTypelbl(getTranslation("report.reportType"));
                    self.checkResultSearch(getTranslation("common.checkResultSearch"));
                    self.roleNamelbl(getTranslation("report.roleName"));
                    self.roleDescriptionlbl(getTranslation("report.roleDescription"));
                    self.backActionLbl(getTranslation("approvalScreen.backAction"));
                    self.DeleteSummaryLbl(getTranslation("others.delete"));
                    self.confirmMessage(getTranslation("report.confirmMessage"));
                    self.oprationMessage(getTranslation("labels.message"));
                    self.no(getTranslation("others.no"));
                    self.yes(getTranslation("others.yes"));
                    self.columnArray([
                        {
                            "headerText": self.roleNamelbl(), "field": "roleName"
                        },
                        {
                            "headerText": self.roleDescriptionlbl(), "field": "roleDescription"
                        }
                    ]);
                }
                initTranslations();
            }
            return JobContentViewModel;
        });
