/**
 * 
 * @param {type} oj
 * @param {type} ko
 * @param {type} $
 * @param {type} app
 * @param {type} commonUtil
 * @param {type} services
 * @param {type} translatehelper
 * @returns {JobL#13.JobContentViewModel}
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojknockout', 'ojs/ojtrain', 'ojs/ojbutton',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker',
    'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup', 'ojs/ojcheckboxset', 'ojs/ojradioset',
    'ojs/ojlabel', 'ojs/ojgauge', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojmessage', 'jquery-calendar',
    'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin',
    'jquery-calendar-picker', 'ojs/ojvalidationgroup', 'ojs/ojmessages', 'ojs/ojmessage', 'ojs/ojarraydataprovider'],
        function (oj, ko, $, app, commonUtil, services) {
            /**
             * The view model for the main content view template
             */
            function JobContentViewModel() {
                var self = this;
                var operationType;
                self.disableSubmit = ko.observable(false);
                self.disableSubmitEdit = ko.observable(false);
                self.endDate = ko.observable();
                self.reportName = ko.observable();
                self.Parameter1 = ko.observable();
                self.Parameter2 = ko.observable();
                self.Parameter3 = ko.observable();
                self.Parameter4 = ko.observable();
                self.Parameter5 = ko.observable();
                self.createReport = ko.observable();
                self.reportNameVal = ko.observable("");
                self.Parameter1Val = ko.observable("");
                self.Parameter2Val = ko.observable("");
                self.Parameter3Val = ko.observable("");
                self.Parameter4Val = ko.observable("");
                self.Parameter5Val = ko.observable("");
                self.reportTypeVal = ko.observable("");
                self.reportTitleLabl = ko.observable();
                self.reportTypelbl = ko.observable();
                self.trainVisible = ko.observable(true);
                 self.reportTypeArr=ko.observableArray();

                self.addJob = ko.observable();
                self.updateJob = ko.observable();
                self.viewJob = ko.observable();
                self.jobTitel = ko.observable();
                self.trainView = ko.observable();
                self.confirmMessage = ko.observable();
                self.addMessage = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.pervious = ko.observable();
                self.next = ko.observable();
                self.cancel = ko.observable();
                self.submit = ko.observable();
                self.create = ko.observable();
                self.review = ko.observable();
                self.ok = ko.observable();
                self.oprationMessage = ko.observable();
                self.addMassageLbl = ko.observable();
                self.editMassageLbl = ko.observable();
                self.placeholder = ko.observable();
                self.date1 = ko.observable(oj.IntlConverterUtils.dateToLocalIso(new Date()));

                self.serviceType = ko.observable();
                self.ministryJobModle = ko.observable();
                self.groupValid = ko.observable();
                self.jnVisibility = ko.observable();
                self.jnVisibility = ko.observable();
                self.trainVisibility = ko.observable();
                self.tracker = ko.observable();
                self.isDisabled = ko.observable(false);
                self.nextBtnVisible = ko.observable(true);
                self.addBtnVisible = ko.observable(false);
                self.columnArray = ko.observableArray();
                self.isRequired = ko.observable(true);
                self.stepArray = ko.observableArray([]);
                self.messages = ko.observable();
                self.errormessage = ko.observable();
                self.errormessagecheckInDatabase = ko.observable();
                self.validationMessage = ko.observable();
                self.oprationMessageUpdate = ko.observable();
                self.reportNamelbl = ko.observable();
                self.Parameter1lbl = ko.observable();
                self.Parameter2lbl = ko.observable();
                self.Parameter3lbl = ko.observable();
                self.Parameter4lbl = ko.observable();
                self.Parameter5lbl = ko.observable();
              
                

                self.currentStepValue = ko.observable('stp1');
                self.hidecancel = ko.observable(true);
                self.hidepervious = ko.observable(false);
                self.hidenext = ko.observable(true);
                self.hidesubmit = ko.observable(false);
                self.hideUpdatesubmit = ko.observable(false);
                self.messages = ko.observable();

                      //define new
                      
                  self.RoleNameVal=ko.observable();
                  self.RoleDescriptionLblVal=ko.observable();
                  self.EITCodeArr=ko.observableArray();
                  self.eitCodeVal=ko.observable();
                  self.EITCodeArr(app.globalEitNameReport());
                  self.actionType=ko.observable();
             

             







                
                self.addRolesetup=function (){
                      
                       var str=JSON.stringify(self.validationModle.eitCodeVal());
                       var res1 = str.replace("[", "");
                       var res2= res1.replace("]", "");
                       
                      var payload = {
                       "roleName":self.validationModle.RoleNameVal(),
                       "roleDescription": self.validationModle.RoleDescriptionLblVal(),
                       "eitCode": self.validationModle.eitCodeVal()
                   
                     };
                     
                     
               
               
                   
                   var createroleSetupCbFn=function(data){
                     
                       oj.Router.rootInstance.go('roleSetupSummary');
                   };
                   
                    
                  services.addGeneric(commonUtil.roleSetup , JSON.stringify(payload)).then(createroleSetupCbFn, app.failcb);
                };
                
                
                
                
                
                self.updateRolesetup=function (){
                      
                      
                  
                      var payload = {
                       "roleName":self.validationModle.RoleNameVal(),
                       "roleDescription": self.validationModle.RoleDescriptionLblVal(),
                       "eitCode": self.validationModle.eitCodeVal(),
                       "id":self.retrieve.id,
                       "roleId":self.retrieve.roleId
                   
                     };
                   
                    
                    
                   var createroleSetupCbFn=function(data){
                      
                       oj.Router.rootInstance.go('roleSetupSummary');
                   };
                   
                    
                  services.addGeneric(commonUtil.roleSetupUpdate , JSON.stringify(payload)).then(createroleSetupCbFn, app.failcb);
                };
                
                
                
                self.previousStep = function () {
                    self.hidecancel(true);
                    self.hidepervious(false);
                    self.hidesubmit(false);
                    self.hideUpdatesubmit(false);
                    self.hidenext(true);
                    self.isDisabled(true);
                    $(".disable-element").prop('disabled', false);
                    self.currentStepValue('stp1');

                };

                self.nextStep = function () {
                         var tracker = document.getElementById("tracker");
                      
                        if (tracker.valid !== "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        return;
                    }
                    else {
                        
                         self.hidesubmit(true);
                         self.isDisabled(true);
                         self.hidepervious(true)
                         self.hidenext(false);
                         $(".disable-element").prop('disabled', true);
                         self.currentStepValue('stp2');
                    } 
                   

                };

                 self.validationModle = ko.observable();
                 self.validationModle = {
                    RoleNameVal: ko.observable(),
                    RoleDescriptionLblVal: ko.observable(),
                    eitCodeVal: ko.observable()
                    
                 };









                self.disableInput = function () {
                    self.hidecancel(false);
                    self.hidepervious(true);
                    self.hidenext(false);

                    $(".disable-element").prop('disabled', true);
                    self.currentStepValue('stp2');
                };

                self.cancelAction = function () {

                    oj.Router.rootInstance.go('roleSetupSummary');

                };


                self.submitButton = function () {
                    document.querySelector("#yesNoDialog").open();

                };
                self.commitRecordupdate = function () {
                    operationType = 'EDIT';
                    self.callReport(operationType);
                    self.disableSubmitEdit(true);

                };
                self.cancelButtonupdate = function () {
                    document.querySelector("#yesNoDialogUpdate").close();

                };
                self.submitUpdateButton = function () {
                    document.querySelector("#yesNoDialogUpdate").open();
//                    operationType = 'EDIT';
//                    self.callReport(operationType);
                };

                self.getReportName = function () {

                    var getReportCbFn = function (data) {
                        if (data === "") {

                            self.disableInput();
                            self.hidesubmit(true);

                        } else {

//                            self.messages([{
//                                    severity: 'error',
//                                    summary: self.errormessagecheckInDatabase(),
//                                    autoTimeout: 0
//                                }
//                            ]);
//                            $.notify(self.errormessagecheckInDatabase(), "error");
                            self.messages([{
                                    severity: 'error',
                                    summary: self.errormessagecheckInDatabase(),
//                                    autoTimeout: 5000

                                }
                            ]);

                        }

                    };
                    var failCbFn = function () {
                    };
                    var serviceName = commonUtil.getReportName + self.reportNameVal();
                    services.getReportName(serviceName).then(getReportCbFn, failCbFn);
                };


                self.getReportNameToCheckUpdate = function () {

                    var getReportCbFn = function (data) {
                        if (data === "") {

                            self.disableInput();
                            self.hideUpdatesubmit(true);

                        } else {

//                            self.messages([{
//                                    severity: 'error',
//                                    summary: self.errormessagecheckInDatabase(),
//                                    autoTimeout: 0
//                                }
//                            ]);
                            $.notify(self.errormessagecheckInDatabase(), "error");
                        }

                    };
                    var failCbFn = function () {
                    };
                    var serviceName = commonUtil.getReportName + oj.Router.rootInstance.retrieve().data.report_Name[0] + "/" + self.reportNameVal();
                    services.getReportName(serviceName).then(getReportCbFn, failCbFn);
                };




                self.restInput = function () {
                    self.reportNameVal("");
                    self.Parameter1Val("");
                    self.Parameter2Val("");
                    self.Parameter3Val("");
                    self.Parameter4Val("");
                    self.Parameter5Val("");
                };



                self.callReport = function (operationType) {
                    var reportSummaryId;
                    if (operationType === 'ADD')
                    {
                        reportSummaryId = 0;
                    } else if (operationType === 'EDIT')
                    {
                        reportSummaryId = oj.Router.rootInstance.retrieve().data.id[0];
                    }
                    var payload = {

                        "report_Name": self.reportNameVal(),
                        "parameter1": self.Parameter1Val(),
                        "parameter2": self.Parameter2Val(),
                        "parameter3": self.Parameter3Val(),
                        "parameter4": self.Parameter4Val(),
                        "parameter5": self.Parameter5Val(),
                        "reportTypeVal": self.reportTypeVal(),
                        "id": reportSummaryId

                    };

                    var createReportCbFn = function (data) {
                        oj.Router.rootInstance.go('getSummary');
                    };
                    var failCbFn = function () {
                    };

                    services.addReport(commonUtil.addReportUrl + operationType, JSON.stringify(payload)).then(createReportCbFn, failCbFn);
                };


                self.commitRecord = function () {
                   if(self.actionType()==='ADD'){
                       
                       self.addRolesetup();
                   }else if(self.actionType()==='update'){
                      
                       self.updateRolesetup();
                   }
                    self.disableSubmit(true);
                };


                self.cancelButton = function () {

                    document.querySelector("#yesNoDialog").close();
                };

                self.commitDraft = function (data, event) {
                    return true;
                };
                self.stopSelectListener = function () {
                    var tracker = document.getElementById("tracker");

                    if (tracker.valid !== "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        event.preventDefault();
                        return;
                    }

                };

                self.handleAttached = function (info) {
                    app.loading(false);
                     
                    
                   
                    self.retrieve = oj.Router.rootInstance.retrieve();
                  
                   
                    if (self.retrieve === 'ADD') {
                          
                        self.actionType('ADD');
                        self.hidecancel(true);
                        self.hidepervious(false);
                        self.hidenext(true);
                        self.hidesubmit(false);
                        $(".disable-element").prop('disabled', false);
                        self.currentStepValue('stp1');
                        self.servicetype=ko.observable();  

                    } else if (self.retrieve.type === 'view')
                    {
                     
                      var eitcode=self.retrieve.eitCode;       
                     self.validationModle = {
                      RoleNameVal: ko.observable(self.retrieve.roleName),
                      RoleDescriptionLblVal: ko.observable(self.retrieve.roleDescription),
                      eitCodeVal: ko.observableArray(eitcode)
                    
                    }; 
                    
                     self.EITCodeArr(app.globalEitNameReport());
                        self.hidecancel(true);
                        self.hidepervious(false);
                        self.hidenext(false);
                        self.hidesubmit(false);
                        $(".disable-element").prop('disabled', true);
                        self.currentStepValue('stp2');
                        self.trainVisible(false);
                    } else if (self.retrieve.type === 'update') {
                           self.actionType('update');
                         var eitcode=self.retrieve.eitCode;  
                         
                           
                           self.validationModle = {
                           RoleNameVal: ko.observable(self.retrieve.roleName),
                           RoleDescriptionLblVal: ko.observable(self.retrieve.roleDescription),
                           eitCodeVal: ko.observableArray(eitcode)
                    
                           };  
                    
                    }

                };
               
                self.currentStepValueText = function () {
                    if (self.currentStepValue() == 'stp2') {
                         $(".disable-element").prop('disabled', true);
                   } else {
                        $(".disable-element").prop('disabled', false);
                    }

                };
               
               

               //define new label 
               self.RoleNameLbl=ko.observable();
               self.RoleDescriptionLbl=ko.observable();
               self.EitCodeLbl=ko.observable();
               self.roleSetupLbl=ko.observable();
              

                var getTranslation = oj.Translations.getTranslatedString;
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });
                
                
                
                function initTranslations() {
                     self.retrieve = oj.Router.rootInstance.retrieve();
                     
                     self.reportTypeArr(app.getPaaSLookup('reportType'));
                    if (self.retrieve === 'ADD') {
                       
                        self.reportTitleLabl(getTranslation("report.addroleSetup"));

                    } else if (self.retrieve.type === 'view') {
                          
                        self.reportTitleLabl(getTranslation("report.viewroleSetup"));
                    } else if (self.retrieve.type === 'update') {
                        
                        self.reportTitleLabl(getTranslation("report.editroleSetup"));
                    }
                    self.reportName(getTranslation("report.reportName"));
                    self.Parameter1(getTranslation("report.Parameter1"));
                    self.Parameter2(getTranslation("report.Parameter2"));
                    self.Parameter3(getTranslation("report.Parameter3"));
                    self.Parameter4(getTranslation("report.Parameter4"));
                    self.Parameter5(getTranslation("report.Parameter5"));
                    self.createReport(getTranslation("report.createReport"));
                    self.errormessage(getTranslation("report.errormessage"));
                    self.createReport(getTranslation("report.create"));
                    self.reportNamelbl(getTranslation("report.reportNamelbl"));
                    self.Parameter1lbl(getTranslation("report.Parameter1lbl"));
                    self.Parameter2lbl(getTranslation("report.Parameter2lbl"));
                    self.Parameter3lbl(getTranslation("report.Parameter3lbl"));
                    self.Parameter4lbl(getTranslation("report.Parameter4lbl"));
                    self.Parameter5lbl(getTranslation("report.Parameter5lbl"));
                    self.reportTypelbl(getTranslation("report.reportType"));
                    self.errormessagecheckInDatabase(getTranslation("report.errormessagecheckInDatabase"));
                    self.oprationMessage(getTranslation("report.oprationMessage"));
                    self.confirmMessage(getTranslation("report.confirmMessage"));
                    self.oprationMessageUpdate(getTranslation("report.oprationMessageUpdate"));
                    self.yes(getTranslation("report.yes"));
                    self.no(getTranslation("report.no"));
                    self.addJob(getTranslation("job.addJob"));
                    self.updateJob(getTranslation("job.updateJob"));
                    self.viewJob(getTranslation("job.viewJob"));
                    self.addMassageLbl(getTranslation("job.addMessage"));
                    self.editMassageLbl(getTranslation("job.editMessage"));

                    self.trainView(getTranslation("others.review"));

                    self.placeholder(getTranslation("labels.placeHolder"));
                    self.pervious(getTranslation("others.pervious"));
                    self.next(getTranslation("others.next"));
                    self.cancel(getTranslation("others.cancel"));

                    self.submit(getTranslation("others.submit"));

                    self.create(getTranslation("labels.create"));
                    self.review(getTranslation("others.review"));
                    self.ok(getTranslation("others.ok"));
                    
                    
                    self.RoleNameLbl(getTranslation("report.RoleName"));
                    self.RoleDescriptionLbl(getTranslation("report.RoleDescription"));
                    self.EitCodeLbl(getTranslation("report.EitCode"));


                    self.stepArray([{label: self.createReport(), id: 'stp1'},
                        {label: self.trainView(), id: 'stp2'}
                    ]);
                }
                initTranslations();
            }
            return JobContentViewModel;
        });
