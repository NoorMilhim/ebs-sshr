define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'ojs/ojtrain', 'config/services',
    'util/commonhelper', 'knockout-mapping', 'ojs/ojmodel'], function (oj, ko, $, app, ojtrain, services, commonhelper, km) {
        function bulkMenuModule() {
            var self = this;
            self.icons = ko.observableArray([]);
            isChecking = false;
            ko.computed(function () {

                if (app.refreshViewForLanguage()) {
                    initTranslations();
                }
                if (app.isGettingMaster())
                    isChecking = true;
                if (isChecking && !app.isGettingMaster()) {
                    updateLabels();
                    isChecking = false;
                }
            });

            function updateLabels() {
                var allEits = JSON.parse(sessionStorage.eitName);
                var x = self.icons();
                x.forEach(ee => {
                    ee.label = allEits.find(e => e.DESCRIPTIVE_FLEX_CONTEXT_CODE == ee.code).DESCRIPTIVE_FLEX_CONTEXT_NAME
                });
                self.icons([]);
                setTimeout(_ => {
                    self.icons(x);
                }, 10)
            }

            self.handleAttached = function (info) {
                app.loading(false);
                services.getGeneric("bulk/menu").then(data => {
                    var arr = JSON.parse(data);
                    var IconBGSetG = ['_RBGCG_0', '_RBGCG_1', '_RBGCG_2', '_RBGCG_3', '_RBGCG_4', '_RBGCG_5', '_RBGCG_6', '_RBGCG_7', '_RBGCG_8', '_RBGCG_9'];
                    var allEits = JSON.parse(sessionStorage.eitName);
                    arr = arr.map((code, index) => {
                        var o = {
                            label: allEits.find(e => e.DESCRIPTIVE_FLEX_CONTEXT_CODE == code).DESCRIPTIVE_FLEX_CONTEXT_NAME,
                            iconType: app.getEITIcons(code, "ImageIcone"),
                            visible: true,
                            IconVarBG: IconBGSetG[index % IconBGSetG.length],//to prevent out of bound exception
                            code: code
                        }

                        return o;
                    })
                    self.icons(arr);
                    if (arr.length == 0) {
                        app.confirmMsg(app.translate('bulksetup.empty_menu'), go => {
                            if (go) {
                                oj.Router.rootInstance.go('bulkSetup');
                            }
                        }, app.translate('bulksetup.add_to_menu'), app.translate('bulksetup.later'));
                    }
                }, error => {
                    console.log(error);
                })

                initTranslations();
            };

            iconNavigation = function (name, code) {
                app.loading(true);
                oj.Router.rootInstance.store({ code: code, trans_name: name });
                oj.Router.rootInstance.go('bulkOperations');
                return true;
            };

            function initTranslations() {
            }
        }
        return new bulkMenuModule();
    });
