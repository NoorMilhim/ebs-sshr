define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper','config/services'],
        function (oj, ko, $, app,commonUtil, services) {

            function ApprovePageViewModel() {
                //vars
                var self = this;
                self.data=ko.observable();
                self.lastapp = ko.observable();
                var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
                self.personID=ko.observable();
                self.transactionId=ko.observable();
                self.approveType = ko.observable();
                self.selfType = ko.observable();
                var Keys = [];
                var PaaSKeys = [];
                var tempObject;
                var ArrKeys = [];
                var Typs = ['date', 'date', 'number'];
                 self.model = {};
                 self.PaaSmodel = {};
                 self.Arrs = {};
                self.url=ko.observable();
                self.userRoles = ko.observableArray([]);
                self.elementEntryArr = ko.observableArray([]);
                self.reqeustedPersonNumber=ko.observable();
                
                

                self.handleAttached = function (info) {
                    app.loading(true);
                    app.formVisible(false);
                    console.log("data");
                    var s=location.search.substring(location.search.indexOf('!'), location.search.length);
                    console.log(s);
                    var parameters =s.substring(1).split('@');
                    var temp = parameters[0].split("=");
                    var TYPE  = unescape(temp[1]);
                    temp = parameters[1].split("=");
                    var TRS_ID   = unescape(temp[1]);
                   
                   if(TYPE=='approve'){
                        self.approveType("APPROVED");
                   }else{
                       self.approveType("REJECTED");
                   }
                   var searchModel = {
                        "TRS_ID":TRS_ID,
                        "Approve_Type":self.approveType()
                    };
                    services.searcheGeneric2(commonUtil.mailNotificationSearch, searchModel);

                    
                     
                };
                setTimeout(function () {
                        window.opener = self;
                        window.close(); 
                    }, 5000); 
                var getNotificationCBF = function (data) {
                    window.opener = self;
                    window.close();  
//                    if(data){
//                         console.log(data);
//                         console.log(self.approveType());
//                        var headers = {
//                        "MSG_TITLE": data.MSG_TITLE,
//                        "MSG_BODY": data.MSG_BODY,
//                        "TRS_ID": data.REQUEST_ID,
//                        "PERSON_ID": data.RECEIVER_ID,
//                        "RESPONSE_CODE": self.approveType(),
//                        "ssType": data.SELF_TYPE ,
//                        "PERSON_NAME":data.PERSON_NAME
//                    };
//                    self.personID(data.RECEIVER_ID);
//                    self.transactionId(data.REQUEST_ID);
//                    self.selfType(data.SELF_TYPE);
//                    self.getUserRoles();
//                   // self.getPaaSEitesValues();
//                    getElementEntryByEitCode();
//                    self.approvedAction();
//                    isLastApprover(data.REQUEST_ID, data.SELF_TYPE);
//                    
//                    console.log(headers);
//                    if(self.approveType()=='APPROVED'){
//                        services.workflowAction("workflowApproval/", headers).then(failfunc, approvalActionCBFN);
//                    }else{
//                     services.workflowAction("workflowApproval/", headers).then(app.failCbFn, rejectCBFN); 
//                    }
//                    
//                    } else {
////                        window.opener = self;
////                        window.close();
//                    }
//                    self.data(data);
                    
                };
                
                 var rejectCBFN = function (data) {
                       window.opener = self;
                        window.close();   
                    };
                
                var failfunc = function (fail) {
                    window.opener = self;
                    window.close(); 
                };
                var approvalActionCBFN = function (data) {
                    console.log("approvalActionCBFN")
                    console.log(self.lastapp());
                        if (self.lastapp()) {
                            var updateStatusCBFN =function (data){
//                            window.opener = self;
//                        window.close();
                            };
                            console.log(self.transactionId());
                             services.getGenericAsync("PeopleExtra/updateStatus?id="+self.transactionId(), {}).then(updateStatusCBFN, app.failCbFn);
                              
                           if(self.selfType()=='XXX_HR_PERSON_PAY_METHOD'){
                               self.updateBankFile();
                           }
//                           //XXX_HR_PERSON_PAY_METHOD
//                           
//                           //Update Bank File 
//                           //
//                           //
                            self.model.url = self.url();
                            var jsonData = ko.toJSON(self.model);
                            console.log(jsonData)
                            var supmitEFFCBFN = function (data) {
                            var Data = data;
//                            window.opener = self;
//                            window.close();       
                            };
                            services.addGeneric("eff/" + "ADD/"+self.transactionId(), jsonData).then(supmitEFFCBFN, app.failCbFn);
//                           
                            if (self.elementEntryArr().length > 0) {
                                for (var i = 0; i < self.elementEntryArr().length; i++) {
                                    self.elementEntryArr()[i].inputValue =  JSON.parse(self.elementEntryArr()[i].inputValue) ;
                                    if (self.elementEntryArr()[i].typeOfAction == "Absence") {
                                        var absenseModel = {
                                            "personNumber": self.reqeustedPersonNumber(),
                                            "employer": "HHA",
                                            "absenceType": self.model[self.elementEntryArr()[i].absenceType](),
                                            "startDate": self.model[self.elementEntryArr()[i].startDate](),
                                            "endDate": self.model[self.elementEntryArr()[i].endDate](),
                                            "absenceStatusCd": self.elementEntryArr()[i].absenceStatus,
                                            "startDateDuration": 1
                                        };
                                        var supmitAbsenceCBFN = function (data) {

                                        }

                                        services.addGeneric("absence/", absenseModel).then(supmitAbsenceCBFN, app.failCbFn);
                                    } 
                                    else {
                                        for(var inputIndex = 0 ; inputIndex<self.elementEntryArr()[i].inputValue.length;inputIndex ++ ){
                                           
                                            if (self.elementEntryArr()[i].inputValue[inputIndex].inputEitValue != "Transaction_ID") {
                                                if (self.elementEntryArr()[i].inputValue[inputIndex].inputEitValue) {
                                                    self.elementEntryArr()[i].inputValue[inputIndex].inputEitValue = self.model[self.elementEntryArr()[i].inputValue[inputIndex].inputEitValue ]();
                                                } else {
                                                    self.elementEntryArr()[i].inputValue[inputIndex].inputEitValue = "";
                                                }
                                            }else{
                                                 self.elementEntryArr()[i].inputValue[inputIndex].inputEitValue = "P"+self.transactionId();
                                            }
                                           
                                           
                                        }
                                       // console.log(self.model[self.elementEntryArr()[i].sourceSystemId]());
                                        var effivtiveStartDate ;
                                        var reason; 
                                        if(self.elementEntryArr()[i].effivtiveStartDate){
                                          effivtiveStartDate =  self.model[self.elementEntryArr()[i].effivtiveStartDate]() ;
                                          
                                          console.log(self.model);
                                           //effivtiveStartDate =  self.elementEntryArr()[i].effivtiveStartDate ;
                                        }else{
                                            effivtiveStartDate = "%EffectiveStartDate%";
                                        }
                                        if(self.elementEntryArr()[i].reason){
                                          reason =  self.model[self.elementEntryArr()[i].reason]() ; 
                                           //effivtiveStartDate =  self.elementEntryArr()[i].effivtiveStartDate ;
                                        }else{
                                            reason = "";
                                        }
                                        var jsonBody = {
                                            personNumber: self.reqeustedPersonNumber(),
                                            elementName: self.elementEntryArr()[i].elementName,
                                            legislativeDataGroupName: self.elementEntryArr()[i].legislativeDatagroupName,
                                            assignmentNumber: "E" + self.reqeustedPersonNumber(),
                                            entryType: "E",
                                            creatorType: self.elementEntryArr()[i].creatorType,
                                            sourceSystemId:"PaaS_"+self.transactionId(),
                                            trsId: self.transactionId(),
                                            SSType: self.elementEntryArr()[i].recurringEntry,
                                            eitCode: self.elementEntryArr()[i].eitCode,
                                            inputValues: self.elementEntryArr()[i].inputValue,
                                            effivtiveStartDate:effivtiveStartDate,
                                            reason:reason
                                            
                                        };
                                        var submitElement = function (data1) {
//                                        window.opener = self;
//                                        window.close(); 
                                            
                                        };

                                        services.submitElementEntry(JSON.stringify(jsonBody)).then(submitElement, app.failCbFn);
                                    }
                                }
                            } else {
//                            window.opener = self;
//                            window.close();   
                        }
//                        
                        }
                        else {
                            console.log("else");
//                        window.opener = self;
//                        window.close();   
                        }
                       
                    };
                
                //Update Bank File Function //
                self.updateBankFile=function(){
                    var bankFileJsonBody = {
                        "bankName": self.model.bank(),
                        "bankBranchName": self.model.bankBranch(),
                        "accountNumber": self.model.newAccountNumber(),
                        "IBAN": self.model.iban(),
                        "trsId": self.transactionId(),
                        "personNumber": self.reqeustedPersonNumber(),
                        "newBankName": self.model.bank(),
                        "newBankBranchName": self.model.bankBranch(),
                        "newAccountNumber": self.model.newAccountNumber()
                    };
                    var submitBankFile= function(data){
                        var createPersonalPaymentMethodJsonBody = {"legislativeDataGroupName": "SA Legislative Data Group",
                            "assignmentNumber": "E"+self.reqeustedPersonNumber(),
                            "personalPaymentMethodCode": "Bank Transfer",
                            "effectiveStartDate": self.model.personPayEffectiveStartDate().replace('-','/').replace('-','/'),
                            "paymentAmountType": "P",
                            "processingOrder": "1",
                            "organizationPaymentMethodCode": "Bank Transfer",
                            "percentage": "100",
                            "bankName": self.model.bank(),
                            "bankBranchName": self.model.bankBranch(),
                            "bankAccountNumber": self.model.newAccountNumber(),
                            "personNumber": self.reqeustedPersonNumber()
                            ,"trsId": self.transactionId()
                        };
                        var createPersonalPaymentMethodCBFN = function(data){
                            self.model = {};
                        };
                      services.submitHDLFile("createPersonalPaymentMethod",JSON.stringify(createPersonalPaymentMethodJsonBody)).then(createPersonalPaymentMethodCBFN, app.failCbFn);  
                    };
                  services.submitHDLFile("createBankFile",JSON.stringify(bankFileJsonBody)).then(submitBankFile, app.failCbFn);
                };
                
                
                self.getPaaSEitesValues = function () {
                    var getPaasEitsValuesCBFN = function (data) {
                        
                        console.log("get paas eit");
                        console.log(data); 
                        self.url(data.url);
                        self.reqeustedPersonNumber(data.person_number);
                        var jsonData = JSON.parse(data.eit);
                        var values = JSON.parse(jsonData);
                        console.log(values);
                        console.log(self.model);
                        console.log(Object.keys(self.model).length);
                        console.log(Object.keys(values).length);
                        for (var i = 0; i < Object.keys(self.model).length; i++) {
                            for (var j = 0; j < Object.keys(values).length; j++) {
                            console.log("inside for");
                            
                                if (Object.keys(self.model)[i] == Object.keys(values)[j]) {
                                    console.log("inside if");

                                    self.model[Object.keys(self.model)[i]](values[Object.keys(values)[j]]);
                                    //For Set Dependent List 
                                    if (tempObject[i].value_SET_TYPE == "X" && tempObject[i + 1].value_SET_TYPE == "Y") {
                                        var getDependentReportCBCF = function (data) {
                                            var tempObject2 = data;
                                            var arr = tempObject2;
                                            var arrays = [];
                                            if (arr.length) {
                                                for (var ind = 0; ind < arr.length; ind++) {
                                                    arrays.push({"value": arr[ind].FLEX_value, "label": arr[ind].value_DESCRIPTION})
                                                }
                                            } else {
                                                arrays.push({"value": arr.FLEX_value, "label": arr.value_DESCRIPTION});
                                            }
                                            self.Arrs[tempObject[i + 1].FLEX_value_SET_NAME + tempObject[i + 1].DESCRIPTION + "Arr"](arrays);
                                        }
                                        var xx = {"reportName": "DependentReport", "valueSet": tempObject[i + 1].FLEX_value_SET_NAME, "parent": self.model[Object.keys(self.model)[i]]()};
                                        services.getGenericReport(xx).then(getDependentReportCBCF, app.failCbFn);
                                    }
                                    //End Of Set Dependent Value Set 

                                }


                            }
//                     if (Object.keys(self.model)[i]==Object.keys(values)[i]){                        
//                          self.model[Object.keys(self.model)[i]](values[Object.keys(values)[i]]);
//                     }
                        }
                    }
                    services.getGenericAsync("PeopleExtra/" + self.transactionId()).then(getPaasEitsValuesCBFN, app.failCbFn);
                };
                
                function isLastApprover (transationId, ss_type) {
                    var roleType, roleId;
                    var isLast;
                    var responseCode ; 
                    var getLastApprove = function (data) {
                        if (data.length) {
                            console.log(data);
                            roleType = data[0].rolrType;
                            roleId = data[0].roleId;
                            responseCode =data[0].responseCode;
                            if (roleType == "POSITION" && roleId == self.personID()) {

                                self.lastapp(true);
                                
                            } else if ((roleType == "LINE_MANAGER" || roleType == "LINE_MANAGER+1") && roleId == self.personID()) {

                                self.lastapp(true);
                                 
                            }else if(roleType == "ROLES"){
                                for(var i = 0;i<self.userRoles().length;i++){
                                    if(self.userRoles()[i].roleId==roleId &&responseCode ){
                                       self.lastapp(true);
                                       
                                        break;
                                    }
                                }
                            
                            }else if(roleType == "JOB_LEVEL" && roleId == self.personID()){
                                self.lastapp(true);
                            }else if(roleType == "AOR" && roleId == self.personID()){
                                self.lastapp(true);
                            }
                        } else {
                         self.lastapp(fasle);
                           
                        }
                    console.log(self.lastapp());
                    };


                    services.getGeneric3(commonUtil.isLastStepApproval + transationId + "/" + ss_type).then(getLastApprove, self.failCbFn);
                    return isLast;
                };
                
               
                self.getUserRoles = function () {
                    var reportPaylod = {"reportName": "USER_ROLES", "personId": self.personID()};
                    var getRolesCBCF = function (data) {
                        for (i = 0; i < data.length; i++) {
                            self.userRoles.push({
                                roleId: data[i].ROLE_ID
                            });
                        }
                    };
                    services.getGenericReportAsync(reportPaylod).then(getRolesCBCF, self.failCbFn); 
                };
                
                function getElementEntryByEitCode() {
                    var EitCode = self.selfType();
                    var getValidGradeCBF = function (data) {
                        console.log("element entry data");
                        console.log(data);
                        self.elementEntryArr(data);

                    
                    };

                    services.getGenericAsync("elementEntrySetup/getElementEntry/" + EitCode).then(getValidGradeCBF, self.failCbFn);
                };
                
                self.approvedAction = function (info) {
                    self.reqestedID = self.personID();

                   
                    var getEit = function (data) {
                        console.log("get eit");
                        console.log(data);
                        tempObject = data;
                        Keys = [];
                        var Iterater = 0;
                        for (var i = 0; i < tempObject.length; i++) {
                            if (tempObject[i].DEFAULT_value && tempObject[i].DEFAULT_TYPE == 'C' && tempObject[i].DEFAULT_value != "PAAS") {
                                var actionModel = {
                                    reportName: tempObject[i].DEFAULT_value,
                                    whenChangeValue: [],
                                    segmentName: Keys[i]
                                };

                                actionModel.whenChangeValue = searchArrayContainValue(tempObject[i].DEFAULT_value, app.globalEITDefultValueParamaterLookup());
                                if (actionModel.whenChangeValue[0]) {
                                    actionModel.whenChangeValue = actionModel.whenChangeValue[0].split(',');
                                }
                                self.actionModelsArr.push(actionModel);

                                for (var index = 0; index < actionModel.whenChangeValue.length; index++) {

                                    var reportPaylod = {"reportName": tempObject[i].DEFAULT_value};
                                    
                                    for (var index = 0; index < actionModel.whenChangeValue.length; index++) {
                                        if (Object.keys(self.model).indexOf(actionModel.whenChangeValue[index]) != -1) {
                                            reportPaylod[actionModel.whenChangeValue[index]] = self.model[actionModel.whenChangeValue[index]]();
                                        } else {
                                            reportPaylod[actionModel.whenChangeValue[index]] = app.personDetails()[actionModel.whenChangeValue[index]];
                                        }
                                    }
                                    var getReportValidationCBCF = function (dataSaaSQuery) {
                                        var resultOfQueryObj = dataSaaSQuery;
                                        self.model[Keys[i]](resultOfQueryObj[ Object.keys(dataSaaSQuery)[0]]);
                                        
                                        console.log(self.model);
                                    }
                                    
                                    console.log("load report");
                                    console.log(reportPaylod);
                                    services.getGenericReport(reportPaylod).then(getReportValidationCBCF, app.failCbFn);


                                }




                            }
                            //Build Model 
                            Keys.push(tempObject[i].DESCRIPTION);
                            self.model[Keys[i]] = ko.observable();
                            PaaSKeys.push(tempObject[i].APPLICATION_COLUMN_NAME);
                            self.PaaSmodel[PaaSKeys[i]] = ko.observable(self.model[Keys[i]]());
                            //end OF Build Model 
                            if ((tempObject[i].FLEX_value_SET_NAME != "100 Character No Validation") &&
                                    (tempObject[i].FLEX_value_SET_NAME != "10 Number") &&
                                    (tempObject[i].FLEX_value_SET_NAME != "HRC_STANDARD_DATE") &&
                                    (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_EIT_ATTACHMENTS") &&
                                    (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_PAAS_NUMBER") &&
                                    (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_PAAS_DATE")
                                    ) {
                                if (tempObject[i].value_SET_TYPE == "X" || tempObject[i].value_SET_TYPE == "I") {
                                    //  self.ishide(false);
                                    ArrKeys.push(tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr");
                                    self.Arrs[ArrKeys[Iterater]] = ko.observableArray();
                                    var getReportCBCF = function (data) {
                                        var tempObject2 = data;
                                        var arr = tempObject2;
                                        var arrays = [];
                                        for (var x = 0; x < arr.length; x++) {
                                            arrays.push({"value": arr[x].FLEX_value, "label": arr[x].value_DESCRIPTION})
                                        }
                                        self.Arrs[ArrKeys[Iterater]](arrays);


                                    };
                                    var xx = {"reportName": "inDependentReport", "valueSet": tempObject[i].FLEX_value_SET_NAME};
                                    services.getGenericReport(xx).then(getReportCBCF, app.failCbFn);
                                    Iterater = Iterater + 1;
                                } else if (tempObject[i].value_SET_TYPE == "Y") {
                                    //         self.ishide(false);
                                    ArrKeys.push(tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr");
                                    self.Arrs[ArrKeys[Iterater]] = ko.observableArray();

                                    Iterater = Iterater + 1;

                                } else if (tempObject[i].value_SET_TYPE == "F") {
                                    //    self.ishide(false);
                                    ArrKeys.push(tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr");
                                    self.Arrs[ArrKeys[Iterater]] = ko.observableArray();
                                    var getDynamicReport = function (data) {
                                        var val = "value";
                                        var lbl = "label";
                                        var q;
                                        var tempObject = data;
                                        q = "select " + tempObject.ID_COL + " " + val + " ,  " + tempObject.value_COL + "  " + lbl
                                                + " From " + tempObject.APPLICATION_TABLE_NAME + "  " + tempObject.ADDITIONAL_WHERE_CLAUSE;


                                        var str = q;
                                        var flex = ":";
                                        var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));
                                        var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf('#'));
                                        var is_HaveFlex = true;
                                        if (str.indexOf(':') == -1) {
                                            is_HaveFlex = false;
                                        }
                                        while (is_HaveFlex) {
                                            var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));
                                            var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf('#'));
                                            actionModel.whenChangeValue.push(ModelName);
                                            str = str.replace(strWitoutFlex, self.reqestedID);
                                            if (str.indexOf(":") == -1) {
                                                is_HaveFlex = false;
                                            }
                                        }



                                        if (str.includes('undefined')) {
                                        } else {
                                            //Here Must Call Report Fpr This Perpus 
                                            var getArrCBCF = function (data) {
                                                var tempObject2 = data;
                                                var arr = tempObject2;
                                                if (tempObject2.length) {
                                                    self.Arrs[ArrKeys[Iterater]](arr);
                                                } else {
                                                    self.Arrs[ArrKeys[Iterater]]([arr])
                                                }
                                            };

                                            services.getDynamicReport(str).then(getArrCBCF, app.failCbFn);

                                        }



                                    };
                                    services.getQueryForListReport(tempObject[i].FLEX_value_SET_NAME).then(getDynamicReport, app.failCbFn);
                                    Iterater = Iterater + 1;
                                } else if (tempObject[i].FLEX_value_SET_NAME == "XXX_HR_DYNAMIC_COLUMN") {

                                    ArrKeys.push(tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr");
                                    self.Arrs[ArrKeys[Iterater]] = ko.observableArray();
                                    var reportName = searchArray(tempObject[i].DESCRIPTION, app.globalEITReportLookup());
                                    var getReportCBCF = function (data) {
                                        var tempObject2 = data;
                                        var arr = tempObject2;
                                        if (tempObject2.length) {
                                            self.Arrs[ArrKeys[Iterater]](arr);
                                        } else {
                                            self.Arrs[ArrKeys[Iterater]]([arr])
                                        }

                                    }
                                    var reportPaylad = {"reportName": reportName, "personId": self.reqestedID};

                                    services.getGenericReport(reportPaylad).then(getReportCBCF, app.failCbFn);
                                    Iterater = Iterater + 1;

                                }



                            }
                            Typs.push(tempObject[i].FLEX_value_SET_NAME);
                        }
                        //buildScreen.buildScreen(tempObject, "xx", self.model, self.isDisabledx, self.dataArray);
                        // self.actionAvilable(true);
                        //  self.setToDisabeld();
                        self.getPaaSEitesValues();
//                        self.getAttachment();
                    };
                    services.getEITasync(self.selfType(), 'US').then(getEit, app.failCbFn);
                };
            }

            return new ApprovePageViewModel();
        }
);



