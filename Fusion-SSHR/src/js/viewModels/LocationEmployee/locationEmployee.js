/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * locationEmployee module
 */
define(['ojs/ojcore', 'knockout', 'appController', 'config/services', 'ojs/ojarraydataprovider',
    'ojs/ojradioset', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojtable', 'ojs/ojknockout-validation',
    'ojs/ojvalidationgroup', 'ojs/ojdialog'
], function (oj, ko, app, services, ArrayDataProvider) {
    /*
     * The view model for the main content view template
     */
    function locationEmployeeContentViewModel() {
        var self = this;

        self.val = ko.observable();
        self.locationVal = ko.observable();
        self.date = ko.observable();
        self.submit = ko.observable();
        self.submitEmp = ko.observable();
        self.save = ko.observable();
        self.Sequance = ko.observable();
        self.personNumber = ko.observable();
        self.locationNameLBL = ko.observable();
        self.SequanceLbl = ko.observable();
        self.datelbl = ko.observable();
        self.submitlbl = ko.observable();
        self.byEmployeeLbl = ko.observable();
        self.bylocationLbl = ko.observable();
        self.saveDataLbl = ko.observable();
        self.personNumberLbl = ko.observable();
        self.clearLbl = ko.observable();
        self.personNumberLbl = ko.observable();
        self.yeslbl = ko.observable();
        self.nolbl = ko.observable();
        self.areyouSure = ko.observable();
        self.locationValtest = ko.observable();
        self.arrLocation = ko.observableArray();
        self.personLocation = ko.observable();
        self.arrPerson = ko.observable();
        self.data2 = ko.observableArray([]);
        self.columnArray = ko.observableArray([]);
        self.personVal = ko.observable();
        self.personNumberVal = ko.observable("");
        self.arrperson = ko.observableArray();
        self.groupValid = ko.observable();
        self.locationValue = ko.observable();
        self.dateValue = ko.observable();
        self.confirmMessage = ko.observable();
        self.oprationMessage = ko.observable();
        self.isRequired = ko.observable(true);
        self.placeholder = ko.observable("Please Select Value");
        self.stepArray = ko.observableArray([]);
        self.currentStepValue = ko.observable('stp1');
        self.createReport = ko.observable();
        self.trainView = ko.observable();
        self.next = ko.observable();
        self.hidenext = ko.observable(true);
        self.oneChecked = ko.observable();
        self.hideSubmitLocation = ko.observable(false);
        self.hideSubmitEmp = ko.observable(false);
        self.hidepervious = ko.observable(false);
        self.backBtnLbl = ko.observable();
        self.pervious = ko.observable();
        self.Startdatelbl = ko.observable();
        self.Enddatelbl = ko.observable();
        self.Enddatelbl = ko.observable();
        self.startDateValue = ko.observable();
        self.endDateValue = ko.observable();
        self.compareLocationOrEmployee=ko.observable("one");

        self.data = ko.observableArray([]);
        self.dataprovider = new oj.ArrayDataProvider(self.data2, {keyAttributes: 'sequance'});



        var getTranslation = oj.Translations.getTranslatedString;


        self.handleAttached = function () {

            app.loading(false);
            self.locationValtest('one');
            self.getAllLocationtest();
//             self.getAllEmptest();

        };
        
        
        self.stopSelectListener = function () {
            
            
        };

        self.submitEmpClick = function () {

            var tracker = document.getElementById("tracker");
            if (tracker.valid === "valid") {
                document.querySelector("#yesNoDialog2").open();
            } else {
                tracker.showMessages();
            }

        };




        self.backAction = function () {

            oj.Router.rootInstance.go('locationSummary');

        };

        self.cancelButtonAddLocation = function () {

            document.querySelector("#yesNoDialog").close();

        };
        self.cancelButtonAddEmp = function () {
            document.querySelector("#yesNoDialog2").close();
        };

        self.commitRecordAddLocation = function () {

            var payload = {
                personNumber: "",
                sartDate:self.startDateValue(),
                endDate:self.endDateValue(),
                personId: app.personDetails().personId,
                locationName: self.locationValue(),
                type: "by location"
            };


            var getlocationCbFn = function (data) {
                // console.log(data);
                oj.Router.rootInstance.go('locationSummary');

            };

            var serviceName = "locationEmp/Add/location";
            services.addGeneric(serviceName, payload).then(getlocationCbFn, app.failCbFn);
            document.querySelector("#yesNoDialog").open();
                 oj.Router.rootInstance.go('locationSummary');
        };

        self.commitRecordAddEmp = function () {


            var payload = {
                personNumberArr: self.data2(),
                datee: self.date(),
                personId: app.personDetails().personId,
                locationName: "",
                type: "by PersonNumber"
            };
       
            var getlocationCbFn = function (data) {
                //console.log(data);
            };

            var serviceName = "locationEmp/Add/personNumber";
            services.addGeneric(serviceName, payload).then(getlocationCbFn, app.failCbFn);
            document.querySelector("#yesNoDialog2").open();
            oj.Router.rootInstance.go('locationSummary');

        };

        self.saveByEmployeeClick = function () {

            self.data2.push({
                sequance: self.data2().length + 1,
                personNumber: self.personVal(),
                sartDate:self.startDateValue(),
                endDate:self.endDateValue()
                //date:self.date()

            });
            self.personVal("");
            self.date("");
        };

        self.checked = function (e) {
//            alert(e.detail.value);
            $(".disable-element").prop('disabled', false);
            self.hidenext(true);
            self.hideSubmitLocation(false);
            self.compareLocationOrEmployee(e.detail.value);
        };

        function clearContent() {
            self.data2([]);
            self.personVal("");
            self.date("");

        }
        ;

        self.clearClick = function () {
            clearContent();
        };


        self.getAllLocationtest = function () {
            var LocationCbFn = function (data) {
                
                $.each(data, function (index) {

                    self.arrLocation.push({
                        value: data[index].LOCATION_CODE,
                        label: data[index].LOCATION_NAME
                    });
                });
            };

            services.getLocation().then(LocationCbFn, app.failCbFn);
        };


        self.nextStep = function () {
            var tracker = document.getElementById("tracker");
            if (self.compareLocationOrEmployee() === "one") {
                if (tracker.valid === "valid") {
                    self.selectNextStep();
                } else {
                    tracker.showMessages();
                }
            } else if (self.compareLocationOrEmployee() === "two") {
                if (self.data2().length > 0) {
                    self.selectNextStep();
                } else {
                    tracker.showMessages();
                }
            };
        };
        
// if (tracker.valid === "valid") {
        self.selectNextStep=function(){
            
             $(".disable-element").prop('disabled', true);
                self.hidenext(false);
                self.hideSubmitLocation(true);
                self.hideSubmitEmp(true);
                self.hidepervious(true);
                self.currentStepValue('stp2');
            
        };


        self.previousStep = function () {
            
            $(".disable-element").prop('disabled', false);
            self.hidenext(true);
            self.currentStepValue('stp1');  
            self.hidepervious(false);
        };
        self.selectedChangedListener = function (event) {
            var data = event.detail;
            var currentRow = data.currentRow;
            var checkArray = self.data()[[currentRow['rowKey']]];

        };

        self.submitLocationClick = function () {
            var tracker = document.getElementById("tracker");
            if (tracker.valid === "valid") {
                document.querySelector("#yesNoDialog").open();
            } else {
                tracker.showMessages();
            }
        };

        var getTranslation = oj.Translations.getTranslatedString;
        // console.log(getTranslation);
        self.refreshView = ko.computed(function () {
            if (app.refreshViewForLanguage()) {
                initTranslations();
            }
        });


        //---------------------Translate Section ----------------------------------------


        function initTranslations() {
            self.confirmMessage(getTranslation("report.confirmMessage"));
            self.SequanceLbl(getTranslation("common.Sequance"));
            self.yeslbl(getTranslation("common.yeslbl"));
            self.nolbl(getTranslation("common.nolbl"));
            self.nolbl(getTranslation("common.nolbl"));
            self.datelbl(getTranslation("common.datelbl"));
            self.submitlbl(getTranslation("common.submitlbl"));
            self.locationNameLBL(getTranslation("common.locationName"));
            self.bylocationLbl(getTranslation("common.bylocationLbl"));
            self.byEmployeeLbl(getTranslation("common.byEmployeeLbl"));
            self.clearLbl(getTranslation("common.clearLbl"));
            self.saveDataLbl(getTranslation("common.saveDataLbl"));
            self.personNumberLbl(getTranslation("common.personNumberLbl"));
            self.areyouSure(getTranslation("common.areyouSure"));
            self.trainView(getTranslation("others.review"));
            self.createReport(getTranslation("report.create"));
            self.next(getTranslation("others.next"));
            self.backBtnLbl(getTranslation("common.backBtnLbl"));
            self.backBtnLbl(getTranslation("common.backBtnLbl"));
            self.pervious(getTranslation("others.pervious"));
            self.Startdatelbl(getTranslation("others.Startdate"));
            self.Enddatelbl(getTranslation("others.Enddate"));
            self.columnArray([

                {
                    "headerText": self.SequanceLbl(),
                    "field": "sequance"
                },

                {"headerText": self.personNumberLbl(),
                    "field": "personNumber"}


            ]);
            self.stepArray([{label: self.createReport(), id: 'stp1'},
                {label: self.trainView(), id: 'stp2'}
            ]);
        }

        initTranslations();

    }

    return locationEmployeeContentViewModel;
});
