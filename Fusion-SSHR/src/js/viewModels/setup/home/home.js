define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'ojs/ojtrain', 'config/services',
    'util/commonhelper', 'knockout-mapping', 'ojs/ojmodel', 'ojs/ojknockout',
    'ojs/ojknockout-model', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojselectcombobox',
    'ojs/ojdatetimepicker', 'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup',
    'ojs/ojknockout-validation', 'ojs/ojfilmstrip', 'ojs/ojpagingcontrol', 'ojs/ojcheckboxset',
    'ojs/ojradioset', 'ojs/ojlabel', 'jquery-calendar', 'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin', 'jquery-calendar-picker',
    'ojs/ojinputtext', 'ojs/ojavatar'],
        function (oj, ko, $, app, ojtrain, services, commonhelper, km) {

            function homeViewModel() {

                var self = this;
              self.valueofsharethouts = ko.observable("Share thoughts with colleagues..");
                self.agreement = ko.observableArray();
                self.currentColor = ko.observable("red");
                var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
                var getTranslation = oj.Translations.getTranslatedString;
                self.empName = ko.observable();
                self.nam = ko.observable();
                self.jobLbl = ko.observable();
                self.positionLbl = ko.observable();
                self.reportlbl = ko.observable();
                self.validationLbl = ko.observable();
                self.Lookuplbl = ko.observable();
                self.approvalManagementLbl = ko.observable();
                self.elementEntryLbl = ko.observable();
                self.roleSetupSummaryLbl = ko.observable();
                self.roleSetupValidationSummaryLbl = ko.observable();
                self.ApprovalConditionSummaryLbl = ko.observable();
                self.manageOnlineHelpSummaryLbl = ko.observable();
                self.countPending = ko.observable();
                self.countApproved = ko.observable();
                self.countRejected = ko.observable();
                self.MailSummaryLbl = ko.observable();
                self.bulkSetupLbl = ko.observable();
                self.timeCardLbl = ko.observable();
                self.iconSetupSummaryLbl = ko.observable();
                self.logScreenSummaryLbl = ko.observable();
                self.image = ko.observable();  
                self.avatarSize = ko.observable("md");
                self.firstName = ko.observable();
                self.lastName = ko.observable();
                self.initials = ko.observable();
                self.LeftMenu = ko.observable(); 
                self.locationEmployeeLbl=ko.observable();    
                self.error = function () {
                    app.hasErrorOccured(true);
                    app.router.go("error");
                };



                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
//                app.showPreloader();
                        initTranslations();
//                app.hidePreloader();
                    }
                });
                self.icons = ko.observableArray([]);

                self.computeIconsArray = ko.computed(function () {
                    if (rootViewModel.personDetails()) {
                        self.firstName(app.personDetails().firstName);
                    self.lastName(app.personDetails().lastName);
                    self.initials(oj.IntlConverterUtils.getInitials(self.firstName(),self.lastName()));
                        self.icons([
                            {
                                label: 'b', value: 'demo-icon-circle-b', router: 'approvalSummary', iconType: 'fa fa-briefcase', text: self.jobLbl(), visible: true, code: "Job"
                            },
                            {
                                label: 'b', value: 'demo-icon-circle-b', router: 'getSummary', iconType: 'fa fa-file-pdf-o', text: self.reportlbl(), visible: true, code: "Report"
                            },
                            {
                                label: 'b', value: 'demo-icon-circle-b', router: 'validationScreen', iconType: 'fa fa-file-pdf-o', text: self.validationLbl(), visible: true, code: "Validation"
                            },
                            {
                                label: 'b', value: 'demo-icon-circle-b', router: 'getPassLookup', iconType: 'fa fa-file-pdf-o', text: self.Lookuplbl(), visible: true, code: "Lookup"
                            },
                            {
                                label: 'b', value: 'demo-icon-circle-b', router: 'approvalManagmentSummary', iconType: 'fa fa-file-pdf-o', text: self.approvalManagementLbl(), visible: true, code: "approvalManagmentSummary"
                            },
                            {
                                label: 'b', value: 'demo-icon-circle-b', router: 'elementEntry', iconType: 'fa fa-file-pdf-o', text: self.elementEntryLbl(), visible: true, code: "elementEntry"
                            },
                            {
                                label: 'b', value: 'demo-icon-circle-b', router: 'roleSetupSummary', iconType: 'fa fa-file-pdf-o', text: self.roleSetupSummaryLbl(), visible: true, code: "roleSetupSummary"
                            },
                             {
                                label: 'b', value: 'demo-icon-circle-b', router: 'roleSetupValidationSummary', iconType: 'fa fa-file-pdf-o', text: self.roleSetupValidationSummaryLbl(), visible: true, code: "roleSetupValidationSummary"
                            },
                            {
                                label: 'b', value: 'demo-icon-circle-b', router: 'ApprovalConditionSummary', iconType: 'fa fa-file-pdf-o', text: self.ApprovalConditionSummaryLbl(), visible: true, code: "ApprovalConditionSummary"
                            },
{
                                label: 'b', value: 'demo-icon-circle-b', router: 'mailSummary', iconType: 'fa fa-file-pdf-o', text: self.MailSummaryLbl(), visible: true, code: "MailSummary"
},
                            {
                                label: 'b', value: 'demo-icon-circle-b', router: 'manageOnlineHelpSummary', iconType: 'fa fa-file-pdf-o', text: self.manageOnlineHelpSummaryLbl(), visible: true, code: "manageOnlineHelpSummary"
                            },
//                            {
//                               label: 'b', value: 'demo-icon-circle-c', router: 'iconSetupSummary', iconType: 'fa fa-fonticons', text: self.iconSetupSummaryLbl(), visible: true, code: "iconsSetupSummary"
//                            },
                            {
                               label: 'b', value: 'demo-icon-circle-c', router: 'employeeTimeAttandanceSearch', iconType: 'fa fa-fonticons', text: "Employee Time Attandance Search", visible: true, code: "employeeTimeAttandanceSearch"
                               
                            },
                            {
                               label: 'b', value: 'demo-icon-circle-c', router: 'logScreen', iconType: 'fa fa-fonticons', text: self.logScreenSummaryLbl(), visible: true, code: "logScreen"
                            },
                            {
                               label: 'b', value:'demo-icon-circle-b', router: 'locationSummary', iconType: 'fa fa-file-pdf-o', text: self.locationEmployeeLbl(), visible: true, code: "locationEmployee"

                            }
                            ,{
                                label: 'b', value: 'demo-icon-circle-c', router: 'bulkSetup', iconType: 'fa fa-file', text: self.bulkSetupLbl(), visible: true, code: ""
                             }
                             ,{
                                label: 'b', value: 'demo-icon-circle-c', router: 'timeCard', iconType: 'fa fa-fonticons', text: self.timeCardLbl(), visible: true, code: ""
                             }, {
                                label: 'b', value: 'demo-icon-circle-b', router: 'attachElement', iconType: 'fa fa-file-pdf-o', text: "Attach Element", visible: true, code: ""
                            }


                        ]);


                    }
                });

                self.pagingModel = null;

                self.loading = function () {
                    app.loading(true);
                };
                
                self.iconNavigation = function (router, code) {
                    app.loading(true);
                    oj.Router.rootInstance.store(code);
                    oj.Router.rootInstance.go(router);
                    return true;
                };

                getPagingModel = function () {
                    if (!self.pagingModel) {
                        var filmStrip = $("#filmStrip");
                        var pagingModel = filmStrip.ojFilmStrip("getPagingModel");
                        self.pagingModel = pagingModel;
                    }
                    return self.pagingModel;
                };
                self.handleAttached = function (info) {
                    self.LeftMenu(app.hideLeftMenu());
                    //console.log( self.LeftMenu());

                    app.loading(false);
                    initTranslations();
                    if (app.conutEitRequest()) {
                            self.countPending(app.conutEitRequest().countPendingApproved);
                            self.countApproved(app.conutEitRequest().countApproved);
                            self.countRejected(app.conutEitRequest().countRejected);
                        }
                     if(app.personDetails().picBase64===undefined ||app.personDetails().picBase64 ===null || app.personDetails().picBase64 ===""){
                        self.image('');
                   }else{
                       self.image("data:image/png;base64," + app.personDetails().picBase64);
                         
                   }    
                        
                        
                };

                function initTranslations() {
                    // function to add translations for the home page module
                    if (rootViewModel.personDetails())
                        self.empName(rootViewModel.personDetails().displayName);
                    self.nam(getTranslation("common.name"));
                    self.jobLbl(getTranslation("pages.approval"));
                    self.positionLbl(getTranslation("pages.position"));
                    self.reportlbl(getTranslation("report.summaryReport"));
                    self.validationLbl(getTranslation("pages.validation"));
                    self.Lookuplbl(getTranslation("pages.Lookup"));
                    self.approvalManagementLbl(getTranslation("pages.approvalManagment"));
                    self.elementEntryLbl(getTranslation("pages.elementEntry"));
                    self.roleSetupSummaryLbl(getTranslation("pages.roleSetupSummary"));
                    self.roleSetupValidationSummaryLbl(getTranslation("pages.roleSetupValidationSummary"));
                    self.ApprovalConditionSummaryLbl(getTranslation("pages.ApprovalConditionSummary"));
                    self.manageOnlineHelpSummaryLbl(getTranslation("pages.manageOnlineHelpSummary"));
                    self.MailSummaryLbl(getTranslation("pages.mailSummary"));
                    self.bulkSetupLbl(getTranslation("pages.bulkSetup"));
                    self.timeCardLbl(getTranslation("pages.timeCard"));
                    self.iconSetupSummaryLbl(getTranslation("pages.iconSetupSummary"));
                   self.locationEmployeeLbl(getTranslation("pages.locationEmployee"));
                   self.logScreenSummaryLbl(getTranslation("pages.logScreenSummary"));
                }

            }

            return new homeViewModel();
        });
