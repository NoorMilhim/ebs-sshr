/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * homeministryJob module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojknockout', 'ojs/ojarraydataprovider', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
    'ojs/ojselectcombobox', 'ojs/ojdialog', 'ojs/ojinputtext','ojs/ojtable', 'ojs/ojlabel', 'ojs/ojdatetimepicker', 'ojs/ojtimezonedata'],
        function (oj, ko, $, app, services, commonhelper) {

            function homeministryJobContentViewModel() {
                var self = this;
//var xx;

                self.id = ko.observable();
                self.checkResultSearch = ko.observable();
                self.backActionLbl = ko.observable();
                self.callSearchEITCode = function () {
                 
                    var EITCodeCbFn = function (data) {
                        //xx = data;
                      self.deptObservableArray([]);
                        if (data.length !== 0) {
//                            self.deptObservableArray(data);
//                  $.each(data, function (index) {
//
//                                self.deptObservableArray.push({
//                                    id: data[index].id,
//                                    eitCode: data[index].eitCode,
//                                    firstKeyType: data[index].firstKeyType,
//                                    firstKeyValue: data[index].firstKeyValue,
//                                    applicationValueFamily: data[index].applicationValueFamily,
//                                    applicationKey: data[index].applicationKey,
//                                    saaSSource: data[index].saaSSource,
//                                    reportName: data[index].reportName,
//                                    parameter1Name: data[index].parameter1Name,
//                                    parameter2Name: data[index].parameter2Name,
//                                    parameter3Name: data[index].parameter3Name,
//                                    parameter4Name: data[index].parameter4Name,
//                                    parameter5Name: data[index].parameter5Name,
//                                    saaSQuery: data[index].saaSQuery,
//                                    paaSQuery: data[index].paaSQuery,
//                                    operation: data[index].operation,
//                                    secondKeyType: data[index].secondKeyType,
//                                    secondKeyValue: data[index].secondKeyValue,
//                                    secondapplicationValueFamily: data[index].secondapplicationValueFamily,
//                                    secondapplicationKey: data[index].secondapplicationKey,
//                                    secondsaaSSource: data[index].secondsaaSSource,
//                                    secondreportName: data[index].secondreportName,
//                                    secondparameter1Name: data[index].secondparameter1Name,
//                                    secondparameter2Name: data[index].secondparameter2Name,
//                                    secondparameter3Name: data[index].secondparameter3Name,
//                                    secondparameter4Name: data[index].secondparameter4Name,
//                                    secondparameter5Name: data[index].secondparameter5Name,
//                                    secondsaaSQuery: data[index].secondsaaSQuery,
//                                    secondpaaSQuery: data[index].secondpaaSQuery,
//                                    typeOFAction: data[index].typeOFAction,
//                                    errorMassageEn: data[index].errorMassageEn,
//                                    errorMassageAr: data[index].errorMassageAr,
//                                    setValueOfEIT: data[index].setValueOfEIT,
//                                    value: data[index].value,
//                                    saveResultToValidationsValues: data[index].saveResultToValidationsValues,
//                                    validationValuesName: data[index].validationValuesName,
//                                    actiontype: data[index].actiontype,
//                                    eitSegment: data[index].eitSegment,
//                                    newValue: data[index].newValue,
//                                    typeOfValue: data[index].typeOfValue,
//                                    typeOfFristValue: data[index].typeOfFristValue,
//                                    fristValues: data[index].fristValues,
//                                    secondOperation: data[index].secondOperation,
//                                    typeOfSecondValue: data[index].typeOfSecondValue,
//                                    secondValue: data[index].secondValue,
//                                   
//                                   
//
//                                });
//                            });
                            
                             for (var i = 0; i < data.length; i++) {
                            var obj = {};
                               obj.id= data[i].id;
                                 for (var j=0;j<app.globalEitNameReport().length;j++){                                
                                    if(app.globalEitNameReport()[j].value==data[i].eitCode){  
                                       obj.eitCodeLBL  = app.globalEitNameReport()[j].label;
                                       }
                                    }       
                                    obj.eitCode =data[i].eitCode;
                                    obj.firstKeyType= data[i].firstKeyType,
                                    obj.firstKeyValue= data[i].firstKeyValue,
                                    obj.applicationValueFamily= data[i].applicationValueFamily,
                                    obj.applicationKey= data[i].applicationKey,
                                    obj.saaSSource= data[i].saaSSource,
                                    obj.reportName= data[i].reportName,
                                    obj.parameter1Name= data[i].parameter1Name,
                                    obj.parameter2Name= data[i].parameter2Name,
                                    obj.parameter3Name= data[i].parameter3Name,
                                    obj.parameter4Name= data[i].parameter4Name,
                                    obj.parameter5Name= data[i].parameter5Name,
                                    obj.saaSQuery= data[i].saaSQuery,
                                    obj.paaSQuery= data[i].paaSQuery,
                                    obj.operation= data[i].operation,
                                    obj.secondKeyType= data[i].secondKeyType,
                                    obj.secondKeyValue= data[i].secondKeyValue,
                                    obj.secondapplicationValueFamily= data[i].secondapplicationValueFamily,
                                    obj.secondapplicationKey= data[i].secondapplicationKey,
                                    obj.secondsaaSSource= data[i].secondsaaSSource,
                                    obj.secondreportName= data[i].secondreportName,
                                    obj.secondparameter1Name= data[i].secondparameter1Name,
                                    obj.secondparameter2Name= data[i].secondparameter2Name,
                                    obj.secondparameter3Name= data[i].secondparameter3Name,
                                    obj.secondparameter4Name= data[i].secondparameter4Name,
                                    obj.secondparameter5Name= data[i].secondparameter5Name,
                                    obj.secondsaaSQuery= data[i].secondsaaSQuery,
                                    obj.secondpaaSQuery= data[i].secondpaaSQuery,
                                    obj.typeOFAction= data[i].typeOFAction,
                                    obj.errorMassageEn= data[i].errorMassageEn,
                                    obj.errorMassageAr= data[i].errorMassageAr,
                                    obj.setValueOfEIT= data[i].setValueOfEIT,
                                    obj.value= data[i].value,
                                    obj.saveResultToValidationsValues= data[i].saveResultToValidationsValues,
                                    obj.validationValuesName= data[i].validationValuesName,
                                    obj.actiontype= data[i].actiontype,
                                    obj.eitSegment= data[i].eitSegment,
                                    obj.newValue= data[i].newValue,
                                    obj.typeOfValue= data[i].typeOfValue,
                                    obj.typeOfFristValue= data[i].typeOfFristValue,
                                    obj.fristValues= data[i].fristValues,
                                    obj.secondOperation= data[i].secondOperation,
                                    obj.typeOfSecondValue= data[i].typeOfSecondValue,
                                    obj.secondValue= data[i].secondValue,
                                   self.deptObservableArray.push(obj);
                                 };
                            
                            
                            
                            

                            //self.dataprovider = new oj.ArrayDataProvider(self.deptObservableArray, {idAttribute: 'jobId'});  
                        } else {
                            $.notify(self.checkResultSearch(), "error");
                            self.deptObservableArray([]);
                        }
                    };
                    var failCbFn = function () {
                        $.notify(self.checkResultSearch(), "error");
                    };
                    var service = "validation/search";
                    services.searcheGeneric(service, self.validationSearch).then(EITCodeCbFn, failCbFn);
                };



                // Call EIT Code
                self.EITCodeArr = ko.observableArray([]);

                // Call Delete Web service
                
                self.deleteRow = function (id) {
                       
                    var deleteRowCbFn = function (data) {
                        self.callSearchEITCode();
                    };
                    var failCbFn = function () {
                         self.callSearchEITCode();
                    };

                    var serviceName = "validation/delete/" + id;
                    services.getGeneric(serviceName, "").then(deleteRowCbFn, failCbFn);
                };


                self.commitDelete = function () {
                    self.id(self.deptObservableArray()[self.selectedIndex()].id);
                    self.deleteRow(self.id());

                    document.querySelector("#yesNoDialog").close();
                };
                self.cancelButton = function () {

                    document.querySelector("#yesNoDialog").close();
                };



                self.columnArray = ko.observableArray();
                self.isRequired = ko.observable(true);
                self.placeholder = ko.observable("Please Select Value");
                // self.dataSourceTB2 = ko.observable();
                var deptArray = [];
                self.oprationdisabled = ko.observable(true);
                self.searchdisabled = ko.observable(true);
                self.selectedRowKey = ko.observable();
                self.selectedIndex = ko.observable();
                self.deptObservableArray = ko.observableArray(deptArray);
                self.dataprovider = new oj.ArrayDataProvider(self.deptObservableArray, {idAttribute: 'DepartmentId'});
                self.dataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.deptObservableArray, {idAttribute: 'id'}));

                self.isDisabled = ko.observable(false);


                self.validationSearch = {
                    eitCode: ko.observable()

                };

                self.searchPerson = function () {
                    var searcheJobCbFn = function (data) {
                    };

                    self.callSearchEITCode();
                };

                self.reset = function () {
                    clearContent();
                };
                function clearContent() {
                    self.validationSearch.eitCode("");
                    self.deptObservableArray([]);
                }
                self.tableSelectionListener = function (event) {
                    var data = event.detail;
                    var currentRow = data.currentRow;
                    if(currentRow){
                    self.oprationdisabled(false);
                    self.selectedRowKey(currentRow['rowKey']);
                    self.selectedIndex(currentRow['rowIndex']);
                    }
                };

                self.addJob = function () {
                    app.loading(true);
                    oj.Router.rootInstance.store('add');
                    oj.Router.rootInstance.go('validation');
                };
                self.updateJob = function () {
                    app.loading(true);
                    ////oj.Router.rootInstance.store('update');
                    self.deptObservableArray()[self.selectedIndex()].type = "update";
                    oj.Router.rootInstance.store(self.deptObservableArray()[self.selectedIndex()]);
                    oj.Router.rootInstance.go('validation');
                };
                self.viewJob = function () {
                    app.loading(true);
                    self.deptObservableArray()[self.selectedIndex()].type = "view";
                    oj.Router.rootInstance.store(self.deptObservableArray()[self.selectedIndex()]);
                    oj.Router.rootInstance.go('validation');
                };

                self.deleteValidation = function () {
                    // oj.Router.rootInstance.store(self.deptObservableArray()[self.selectedIndex()]);
                    // self.retrieve = oj.Router.rootInstance.retrieve();
                    // self.id(self.retrieve.id);
                    document.querySelector('#yesNoDialog').open();
                };

                self.handlerChange = function () {
                    if (!self.validationSearch.eitCode()) {
                        self.searchdisabled(true);
                    } else {
                        self.searchdisabled(false);
                    }

                };


                self.handleAttached = function (info) {
                    app.loading(false);
                    var arr = JSON.parse(sessionStorage.eitName).map(e => ({label: e.DESCRIPTIVE_FLEX_CONTEXT_NAME, value: e.DESCRIPTIVE_FLEX_CONTEXT_CODE}));
                    self.EITCodeArr(arr);
                };
                
                self.backAction = function () {
                    oj.Router.rootInstance.go('setup');
                };

                //-----------------------------
                //---------------------Translate Section ----------------------------------------
                self.confirmMessage = ko.observable();
                self.oprationMessage = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.ok = ko.observable();
                self.startDateLbl = ko.observable();
                self.endDateLbl = ko.observable();
                self.eitCodeLbl = ko.observable();
                self.firstKeyTypeLbl = ko.observable();
                self.operationLbl = ko.observable();
                self.secondKeyTypeLbl = ko.observable();
                self.errorMassageENLbl = ko.observable();
                self.errorMassageARLbl = ko.observable();
                self.addLbl = ko.observable();
                self.editLbl = ko.observable();
                self.additionalDetailsLbl = ko.observable();
                self.historyLbl = ko.observable();
                self.validGradeLbl = ko.observable();
                self.correctionLbl = ko.observable();
                self.search = ko.observable();
                self.clear = ko.observable();
                self.view = ko.observable();
                self.delet = ko.observable();
                self.validGrade = ko.observable();
                self.validation = ko.observable();
                self.effectiveDate = ko.observable();
                self.todayDay = ko.observable();
                self.cancel = ko.observable();
                self.resetLabel = ko.observable();
                var getTranslation = oj.Translations.getTranslatedString;
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();

                    }

                });
                function initTranslations() {

                    self.confirmMessage(getTranslation("labels.confirmMessage"));
                    self.oprationMessage(getTranslation("validation.deleteValidationMessage"));
                    self.yes(getTranslation("others.yes"));
                    self.no(getTranslation("others.no"));
                    self.ok(getTranslation("others.ok"));
                    self.addLbl(getTranslation("others.add"));
                    self.editLbl(getTranslation("others.edit"));
                    self.validGradeLbl(getTranslation("others.validGrade"));
                    self.additionalDetailsLbl(getTranslation("others.additonalDetail"));
                    self.historyLbl(getTranslation("others.history"));
                    self.startDateLbl(getTranslation("job.startDate"));
                    self.endDateLbl(getTranslation("job.endDate"));
                    self.correctionLbl(getTranslation("others.correction"));
                    self.search(getTranslation("others.search"));
                    self.clear(getTranslation("others.clear"));
                    self.view(getTranslation("others.view"));
                    self.delet(getTranslation("others.delete"));
                    self.validGrade(getTranslation("others.validGrade"));
                    self.validation(getTranslation("validation.validation"));
                    self.effectiveDate(getTranslation("others.effectiveDate"));
                    self.todayDay(getTranslation("others.todayDay"));
                    self.cancel(getTranslation("others.cancel"));
                    self.resetLabel(getTranslation("login.resetLabel"));
                    self.eitCodeLbl(getTranslation("validation.eitCode"));
                    self.firstKeyTypeLbl(getTranslation("validation.firstKeyType"));
                    self.operationLbl(getTranslation("validation.operation"));
                    self.secondKeyTypeLbl(getTranslation("validation.secondKeyType"));
                    self.errorMassageENLbl(getTranslation("validation.errorMassageEN"));
                    self.errorMassageARLbl(getTranslation("validation.errorMassageAR"));
                    self.checkResultSearch(getTranslation("common.checkResultSearch"));
                    self.backActionLbl(getTranslation("approvalScreen.backAction"));

                    self.columnArray([
                        {
                            "headerText": self.eitCodeLbl(), "field": "eitCodeLBL"
                        },
                        {
                            "headerText": self.firstKeyTypeLbl(), "field": "firstKeyType"
                        },
                        {
                            "headerText": self.operationLbl(), "field": "operation"
                        },
                        {
                            "headerText": self.secondKeyTypeLbl(), "field": "secondKeyType"
                        },
                        {
                            "headerText": self.errorMassageENLbl(), "field": "errorMassageEn"
                        },
                        {
                            "headerText": self.errorMassageARLbl(), "field": "errorMassageAr"
                        }
                    ]);
                }
                initTranslations();




            }

            return homeministryJobContentViewModel;
        });
